/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : perumdamtkr_website

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-09-23 16:28:33
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `sys_resources_details`
-- ----------------------------
DROP TABLE IF EXISTS `sys_resources_details`;
CREATE TABLE `sys_resources_details` (
  `id` char(36) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `resource_id` char(36) NOT NULL,
  `field` varchar(100) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `active_status` tinyint(1) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `annotation` text DEFAULT NULL,
  `menu_code` varchar(50) DEFAULT NULL,
  `menu_order` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sys_resources_details
-- ----------------------------
INSERT INTO `sys_resources_details` VALUES ('55f14002-7734-4a9d-bfa7-12f8c689773d', 'pre_text', '55f13a19-b194-4a2d-8e06-1780c689773d', 'pre_text', '<span class=\"fa fa-user fa-fw\"></span> ', '1', 'pre_text', 'pre_text', 'html_tag', '0', '2015-09-10 15:32:02', '2015-09-11 21:55:14');
INSERT INTO `sys_resources_details` VALUES ('55f14270-6eec-490a-a32b-12f8c689773d', 'json_url', '55f13a19-b194-4a2d-8e06-1780c689773d', 'json_url', '{\"plugin\":\"sysadmin\",\"controller\":\"sysadmin_app\", \"action\":\"my_profile\"}', '1', 'json_url', 'json_url', 'json_url', '4', '2015-09-10 15:42:24', '2015-09-11 21:55:45');
INSERT INTO `sys_resources_details` VALUES ('55f186a4-d798-4655-87c0-14fcc689773d', 'pre_text', '55f18533-ce20-410d-a0cc-14fcc689773d', 'pre_text', '<span class=\"fa fa-key fa-fw\"></span> ', '1', 'pre_text', 'pre_text', 'html_tag', '0', '2015-09-10 20:33:24', '2015-09-11 22:02:27');
INSERT INTO `sys_resources_details` VALUES ('55f187fd-61f0-471a-9cbd-080cc689773d', 'json_url', '55f18533-ce20-410d-a0cc-14fcc689773d', 'json_url', '{\"plugin\":\"sysadmin\",\"controller\":\"sysadmin_app\", \"action\":\"change_password\"}', '1', 'json_url', 'json_url', 'json_url', '3', '2015-09-10 20:39:09', '2015-09-11 22:04:47');
INSERT INTO `sys_resources_details` VALUES ('55f18d9f-ebf4-4379-b598-14fcc689773d', 'pre_text', '55f189d5-50d8-4fba-bfbe-080cc689773d', 'pre_text', '<span class=\"fa fa-sign-out fa-fw\"></span> ', '1', 'pre_text', 'pre_text', 'html_tag', '0', '2015-09-10 21:03:11', '2015-09-11 22:01:08');
INSERT INTO `sys_resources_details` VALUES ('55f18e36-2ae8-402e-ab80-14fcc689773d', 'json_url', '55f189d5-50d8-4fba-bfbe-080cc689773d', 'json_url', '{\"plugin\":\"sysadmin\",\"controller\":\"sysadmin_app\", \"action\":\"signout\"}', '1', 'json_url', 'json_url', 'json_url', '3', '2015-09-10 21:05:42', '2015-09-11 22:01:46');
INSERT INTO `sys_resources_details` VALUES ('55f18f63-1ec4-4558-947f-080cc689773d', 'pre_text', '55f18ee4-df80-46e7-ab90-14fcc689773d', 'pre_text', '<span class=\"fa fa-cogs fa-fw\"></span> ', '1', 'pre_text', 'pre_text', 'html_tag', '0', '2015-09-10 21:10:43', '2015-09-11 22:05:08');
INSERT INTO `sys_resources_details` VALUES ('55f190fb-7c80-4c6e-a24f-080cc689773d', 'pre_text', '55f19097-962c-48ba-bb24-080cc689773d', 'pre_text', '<span class=\"fa fa-question fa-fw\"></span> ', '1', 'pre_text', 'pre_text', 'html_tag', '0', '2015-09-10 21:17:31', '2015-09-11 22:05:34');
INSERT INTO `sys_resources_details` VALUES ('55f1914a-d720-4d72-ad7e-080cc689773d', 'json_url', '55f19097-962c-48ba-bb24-080cc689773d', 'json_url', '{\"plugin\":\"sysadmin\",\"controller\":\"sysadmin_app\", \"action\":\"help\"}', '1', 'json_url', 'json_url', 'json_url', '3', '2015-09-10 21:18:50', '2015-09-11 22:06:00');
INSERT INTO `sys_resources_details` VALUES ('55f191d1-da60-4b5c-97d5-14fcc689773d', 'json_url', '55f18ee4-df80-46e7-ab90-14fcc689773d', 'json_url', '{\"plugin\":\"sysadmin\",\"controller\":\"sysadmin_app\", \"action\":\"user_preferences\"}', '1', 'json_url', 'json_url', 'json_url', '3', '2015-09-10 21:21:05', '2015-09-11 22:06:16');
INSERT INTO `sys_resources_details` VALUES ('55f1c759-2b8c-4b4d-a098-1770c689773d', 'pre_text', '55f1c6b6-5e68-4e13-9ad8-1770c689773d', 'pre_text', '<span class=\"fa fa-cogs fa-fw\"></span> ', '1', 'pre_text', 'pre_text', 'html_tag', '0', '2015-09-11 01:09:29', '2015-09-11 01:09:29');
INSERT INTO `sys_resources_details` VALUES ('55f222fd-da14-40ae-89bb-14a0c689773d', 'pre_text', '55f1ca15-17a8-4d1d-8fa7-177cc689773d', 'pre_text', '<span class=\"fa fa-user fa-fw\"></span>', '1', 'pre_text', 'pre_text', 'html_tag', '0', '2015-09-11 07:40:29', '2015-09-11 09:02:33');
INSERT INTO `sys_resources_details` VALUES ('55f22367-db90-4e7b-b9fe-14a0c689773d', 'json_url', '55f2226b-3e64-4760-979f-1710c689773d', 'json_url', '{\"plugin\":\"sysadmin\",\"controller\":\"users\", \"action\":\"index\"}', '1', 'json_url', 'json_url', 'json_url', '2', '2015-09-11 07:42:15', '2015-09-11 07:42:15');
INSERT INTO `sys_resources_details` VALUES ('55f236b1-e364-402b-a0f3-14a0c689773d', 'json_url', '55f23411-2c50-450e-b047-1710c689773d', 'json_url', '{\"plugin\":\"sysadmin\",\"controller\":\"roles\", \"action\":\"index\"}', '1', 'json_url', 'json_url', 'json_url', '2', '2015-09-11 09:04:33', '2015-09-11 09:04:33');
INSERT INTO `sys_resources_details` VALUES ('55f2eaf2-b160-4118-9e5d-1968c689773d', 'json_url', '55f2e9f0-a130-4f1c-b798-1a40c689773d', 'json_url', '{\"plugin\":\"sysadmin\", \"controller\":\"privileges\", \"action\":\"index\"}', '1', 'json_url', 'json_url', 'json_url', '0', '2015-09-11 21:53:38', '2015-09-11 21:53:38');
INSERT INTO `sys_resources_details` VALUES ('55f2f6ef-c42c-49e6-84cb-1144c689773d', 'pre_text', '55f23ffc-f8a0-49b7-a8c1-14a0c689773d', 'pre_text', '<span class=\"fa fa-object-group fa-fw\"></span> ', '1', 'pre_text', 'pre_text', 'html_tag', '0', '2015-09-11 22:44:47', '2015-09-11 22:44:47');
INSERT INTO `sys_resources_details` VALUES ('55fe6d5d-1de4-44bf-a052-1168c689773d', 'json_url', '55f1c6b6-5e68-4e13-9ad8-1770c689773d', 'json_url', '{\"plugin\":\"sysadmin\",\"controller\":\"sysadmin_app\", \"action\":\"index\"}', '1', '', '', 'json_url', null, '2015-09-20 15:25:01', '2015-09-20 15:25:01');
INSERT INTO `sys_resources_details` VALUES ('56161d49-36b0-4cdd-89a1-1974c689773d', 'json_url', '56161cca-d96c-4104-b967-04ccc689773d', 'json_url', '{\"plugin\":\"sysadmin\",\"controller\":\"users\", \"action\":\"user_activation\"}', '1', '', '', '', null, '2015-10-08 14:37:45', '2015-10-08 14:37:45');
INSERT INTO `sys_resources_details` VALUES ('56168684-86e0-4493-b55e-11ccc689773d', 'protected_user_id', '5614c64d-c154-4f7f-addb-1aa0c689773d', 'user_id', '55f285b8-2b4c-4558-89b9X0984c689773d', '1', '', '', 'protected_user_id', null, '2015-10-08 22:06:44', '2015-10-08 22:07:38');
INSERT INTO `sys_resources_details` VALUES ('562079b7-1b54-4bdb-8f12-1394c689773d', 'protected_role_id', '56204d3d-f25c-4938-851d-1394c689773d', 'role_id', '55f27924-fef8-4337-bf8eX1760c689773d', '1', '', '', 'protected_role_id', null, '2015-10-16 11:14:47', '2015-10-16 11:15:16');
INSERT INTO `sys_resources_details` VALUES ('5629ccbc-6078-42fd-a0bf-1ac8c689773d', 'privileges_management_permission', '5629cbf0-ae58-4372-bb50-1ac8c689773d', 'json_url', '{\"plugin\":\"sysadmin\",\"controller\":\"roles\", \"action\":\"privileges_management\"}', '1', '', '', '', null, '2015-10-23 12:59:24', '2015-10-23 13:00:51');
INSERT INTO `sys_resources_details` VALUES ('565bf148-a410-4d96-bad3-1b24c689773d', 'pre_text', '565bf014-6b60-42d9-8a32-0ee4c689773d', 'pre_text', '<span class=\"fa fa-pen-square fa-fw\"></span>', '1', 'pre_text', '', 'pre_text', '0', '2015-11-30 13:48:40', '2015-12-10 14:55:28');
INSERT INTO `sys_resources_details` VALUES ('565bf2e7-6278-4e6a-932a-1b24c689773d', 'syscm_content_pre_text', '565bf24c-6ed4-4309-babe-18a0c689773d', 'pre_text', '<span class=\"fa fa-pen-square fa-fw\"></span>', '1', 'pre_text', 'Contents Management (syscm_content) :: pre_text', 'syscm_content_pre_text', '0', '2015-11-30 13:55:35', '2015-12-10 14:56:20');
INSERT INTO `sys_resources_details` VALUES ('565bf3a5-4c84-4a3a-8ea7-1b24c689773d', 'syscm_content_json_url', '565bf24c-6ed4-4309-babe-18a0c689773d', 'json_url', '{\"plugin\":\"syscm\",\"controller\":\"syscm_contents\", \"action\":\"index\"}', '1', 'json_url', 'Contents Management (syscm_content) :: syscm_content_json_url', 'syscm_content_json_url', '1', '2015-11-30 13:58:45', '2015-11-30 13:58:45');
INSERT INTO `sys_resources_details` VALUES ('565bf92d-0d00-4aa7-bc3f-0d80c689773d', 'syscm_json_url', '565bf014-6b60-42d9-8a32-0ee4c689773d', 'json_url', '{\"plugin\":\"syscm\",\"controller\":\"syscm\", \"action\":\"index\"}', '1', 'json_url', 'syscm_json_url', '', '1', '2015-11-30 14:22:21', '2015-11-30 14:23:05');
INSERT INTO `sys_resources_details` VALUES ('565c01cc-4cc8-499c-a45a-18a0c689773d', 'pre_text', '565c01b5-323c-455f-815a-12dcc689773d', 'pre_text', '<span class=\"fa fa-th-list fa-fw\"></span>', '1', 'pre_text', '', '', null, '2015-11-30 14:59:08', '2015-12-12 09:08:05');
INSERT INTO `sys_resources_details` VALUES ('565c01f2-cecc-46e1-b9f2-12dcc689773d', 'json_url', '565c01b5-323c-455f-815a-12dcc689773d', 'json_url', '{\"plugin\":\"syscm\",\"controller\":\"syscm_categories\", \"action\":\"index\"}', '1', '', '', '', null, '2015-11-30 14:59:46', '2015-11-30 14:59:46');
INSERT INTO `sys_resources_details` VALUES ('5661b4f2-2860-48af-bec1-111cded5206b', 'pre_text', '5661a58f-3dc0-4784-9ef3-06d8c689773d', 'pre_text', '<span class=\"fa fa-bars fa-fw\"></span>', '1', '', '', '', null, '2015-12-04 22:44:50', '2015-12-04 22:44:50');
INSERT INTO `sys_resources_details` VALUES ('5661b518-a3d4-4da3-9f83-111cded5206b', 'json_url', '5661a58f-3dc0-4784-9ef3-06d8c689773d', 'json_url', '{\"plugin\":\"syscm\",\"controller\":\"syscm_menus\",\"action\":\"index\"}', '1', '', '', '', null, '2015-12-04 22:45:28', '2015-12-04 22:45:28');
INSERT INTO `sys_resources_details` VALUES ('5663070a-9b18-45fc-a23a-1960ded5206b', 'pre_text', '5663063c-1a78-4470-b454-1964ded5206b', 'pre_text', '<span class=\"fa fa-gear fa-fw\"></span>', '1', '', '', '', null, '2015-12-05 22:47:22', '2015-12-05 22:47:22');
INSERT INTO `sys_resources_details` VALUES ('56630764-9904-4b22-aa25-1964ded5206b', 'json_url', '5663063c-1a78-4470-b454-1964ded5206b', 'json_url', '{\"plugin\":\"syscm\",\"controller\":\"syscm_widgets\",\"action\":\"index\"}', '1', '', '', '', null, '2015-12-05 22:48:52', '2015-12-05 22:48:52');
INSERT INTO `sys_resources_details` VALUES ('566309e1-fe08-4dde-aac6-15e4ded5206b', 'pre_text', '56630937-1bac-4c0b-862d-15e4ded5206b', 'pre_text', '<span class=\"fa fa-list fa-fw\"></span>', '1', '', '', '', null, '2015-12-05 22:59:29', '2015-12-05 22:59:29');
INSERT INTO `sys_resources_details` VALUES ('56630a00-4984-4faf-8a5b-0d7cded5206b', 'json_url', '56630937-1bac-4c0b-862d-15e4ded5206b', 'json_url', '{\"plugin\":\"syscm\",\"controller\":\"syscm_layouts\",\"action\":\"index\"}', '1', '', '', '', null, '2015-12-05 23:00:00', '2015-12-05 23:00:00');
INSERT INTO `sys_resources_details` VALUES ('566b8226-be8c-4e73-b8e1-08b8ded5206b', 'pre_text', '566b81c7-e024-4167-ab08-08b8ded5206b', 'pre_text', '<span class=\"fa fa-file-image fa-fw\"></span>', '1', '', '', '', null, '2015-12-12 09:10:46', '2015-12-12 09:14:10');
INSERT INTO `sys_resources_details` VALUES ('566b8259-a51c-4679-baf9-0534ded5206b', 'json_url', '566b81c7-e024-4167-ab08-08b8ded5206b', 'json_url', '{\"plugin\":\"syscm\",\"controller\":\"syscm_media\", \"action\":\"index\"}', '1', '', '', '', null, '2015-12-12 09:11:37', '2015-12-12 09:11:37');
INSERT INTO `sys_resources_details` VALUES ('5670fcbd-c624-4143-bee0-4bcec0b9c32c', 'pre_text', '5670fc7b-a634-4224-84e1-4205c0b9c32c', 'pre_text', '<span class=\"fa fa-users fa-fw\"></span> ', '1', '', '', '', null, '2015-12-15 23:55:09', '2015-12-15 23:55:09');
INSERT INTO `sys_resources_details` VALUES ('5670fd7b-00e0-4f56-9763-43edc0b9c32c', 'pre_text', '5670fd65-dd88-4bb1-804b-442ac0b9c32c', 'pre_text', '<span class=\"fa fa-users fa-fw\"></span>', '1', '', '', '', null, '2015-12-15 23:58:19', '2015-12-15 23:58:19');
INSERT INTO `sys_resources_details` VALUES ('5670fda3-95c4-47aa-9020-4826c0b9c32c', 'json_url', '5670fd65-dd88-4bb1-804b-442ac0b9c32c', 'json_url', '{\"plugin\":\"syscm\",\"controller\":\"syscm_cust\", \"action\":\"index\"}', '1', '', '', '', null, '2015-12-15 23:58:59', '2016-01-03 05:13:17');
INSERT INTO `sys_resources_details` VALUES ('5675bc9d-7b84-4ff9-8ef2-49fdc0b9c32c', 'pre_text', '5675bc53-4d98-40fc-93ad-4e1fc0b9c32c', 'pre_text', '<span class=\"fa fa-comments fa-fw\"></span>', '1', '', '', '', null, '2015-12-19 14:22:53', '2015-12-19 14:22:53');
INSERT INTO `sys_resources_details` VALUES ('5675bda4-c000-410d-bc6d-4fbdc0b9c32c', 'pre_text', '5675bd7c-3534-4406-9579-46dac0b9c32c', 'pre_text', '<span class=\"fa fa-comments fa-fw\"></span>', '1', '', '', '', '0', '2015-12-19 14:27:16', '2016-02-08 13:53:38');
INSERT INTO `sys_resources_details` VALUES ('5675bdc5-23d8-4bdd-a649-4296c0b9c32c', 'json_url', '5675bd7c-3534-4406-9579-46dac0b9c32c', 'json_url', '{\"plugin\":\"syscm\",\"controller\":\"syscm_feedback\",\"action\":\"index\"}', '1', '', '', '', null, '2015-12-19 14:27:49', '2015-12-19 14:27:49');
INSERT INTO `sys_resources_details` VALUES ('568a67df-c198-47b6-95df-f826c0b9c32c', 'pre_text', '568a676e-d860-49b7-915f-ecefc0b9c32c', 'pre_text', '<span class=\"fa fa-money-bill fa-fw\"></span>', '1', '', '', '', null, '2016-01-04 06:38:55', '2016-01-04 06:38:55');
INSERT INTO `sys_resources_details` VALUES ('568a67fd-509c-4058-8fce-fba7c0b9c32c', 'json_url', '568a676e-d860-49b7-915f-ecefc0b9c32c', 'json_url', '{\"plugin\":\"syscm\",\"controller\":\"syscm_cust_bill\", \"action\":\"index\"}', '1', '', '', '', null, '2016-01-04 06:39:25', '2016-01-04 06:39:25');
INSERT INTO `sys_resources_details` VALUES ('56a36107-c6d8-40d8-88e0-0fe0ded5206b', 'pre_text', '56a36074-7264-401e-96e6-06c4ded5206b', 'pre_text', '<span class=\"fa fa-comment fa-fw\"></span>', '1', '', '', '', null, '2016-01-23 18:16:23', '2016-01-23 18:16:23');
INSERT INTO `sys_resources_details` VALUES ('56a36134-4f88-466a-a30b-167cded5206b', 'json_url', '56a36074-7264-401e-96e6-06c4ded5206b', 'json_url', '{\"plugin\":\"syscm\",\"controller\":\"syscm_cust_comment\",\"action\":\"index\"}', '1', '', '', '', null, '2016-01-23 18:17:08', '2016-01-23 18:17:08');
INSERT INTO `sys_resources_details` VALUES ('56b513ad-e0b4-4f53-83ac-425b671fe029', 'json_url', '56b50667-3094-4d83-b55d-8b00671fe029', 'json_url', '{\"plugin\":\"sysadmin\",\"controller\":\"publish_content\",\"action\":\"index\"}', '1', '', '', '', null, '2016-02-06 04:27:09', '2016-02-06 04:27:09');
INSERT INTO `sys_resources_details` VALUES ('56b839c1-1354-490d-bc5f-d212671fe029', 'pre_text', '56b8392f-2e10-445a-a4f7-c9b5671fe029', 'pre_text', '<span class=\"fa fa-check-square fa-fw\"></span>', '1', '', '', '', null, '2016-02-08 13:46:25', '2016-02-08 13:46:25');
INSERT INTO `sys_resources_details` VALUES ('56b839d6-cf24-47c3-82bd-d367671fe029', 'json_url', '56b8392f-2e10-445a-a4f7-c9b5671fe029', 'json_url', '{\"plugin\":\"syscm\",\"controller\":\"syscm_cust_reg\",\"action\":\"index\"}', '1', '', '', '', null, '2016-02-08 13:46:46', '2016-02-08 13:46:46');
INSERT INTO `sys_resources_details` VALUES ('5f6ac636-e260-4736-b67f-1804ded5206b', 'pre_text', '565bf24c-6ed4-4309-bxxx-18a0c689773d', 'pre_text', '<span class=\"fa fa-check-square fa-fw\"></span>', '1', '', '', '', null, '2020-09-23 03:51:18', '2020-09-23 03:51:18');
INSERT INTO `sys_resources_details` VALUES ('5f6ac674-827c-4157-af06-1804ded5206b', 'json_url', '565bf24c-6ed4-4309-bxxx-18a0c689773d', 'json_url', '{\"plugin\":\"syscm\",\"controller\":\"syscm_galery\",\"action\":\"index\"}', '1', '', '', '', null, '2020-09-23 03:52:20', '2020-09-23 03:52:20');
INSERT INTO `sys_resources_details` VALUES ('5f6b0a35-c7d0-4deb-b498-0804ded5206b', 'pre_text', '5f6b09f4-f52c-4b3e-b485-2790ded5206b', 'pre_text', '<span class=\"fa fa-check-square fa-fw\"></span>', '1', '', '', '', null, '2020-09-23 08:41:25', '2020-09-23 08:41:25');
INSERT INTO `sys_resources_details` VALUES ('5f6b0a79-7b54-4c08-936d-2790ded5206b', 'json_url', '5f6b09f4-f52c-4b3e-b485-2790ded5206b', 'json_url', '{\"plugin\":\"syscm\",\"controller\":\"syscm_konsultasi\",\"action\":\"index\"}', '1', '', '', '', null, '2020-09-23 08:42:33', '2020-09-23 08:42:33');
INSERT INTO `sys_resources_details` VALUES ('5f6b11ab-8ab8-4eb7-996f-0804ded5206b', 'pre_text', '5f6b1174-9f98-4258-a56e-2790ded5206b', 'pre_text', '<span class=\"fa fa-check-square  fa-fw\"></span>', '1', '', '', '', null, '2020-09-23 09:13:15', '2020-09-23 09:13:15');
INSERT INTO `sys_resources_details` VALUES ('5f6b11ec-e0c0-4b36-b920-1f1cded5206b', 'json_url', '5f6b1174-9f98-4258-a56e-2790ded5206b', 'json_url', '{\"plugin\":\"syscm\",\"controller\":\"syscm_register\",\"action\":\"index\"}', '1', '', '', '', null, '2020-09-23 09:14:20', '2020-09-23 09:14:20');
