/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : perumdamtkr_website

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-10-05 17:13:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `sys_resources`
-- ----------------------------
DROP TABLE IF EXISTS `sys_resources`;
CREATE TABLE `sys_resources` (
  `id` char(36) NOT NULL,
  `name` varchar(100) NOT NULL,
  `resources_group_id` char(36) NOT NULL,
  `resources_type_id` char(36) NOT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  `active_status` tinyint(1) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `annotation` text DEFAULT NULL,
  `menu_code` varchar(50) DEFAULT NULL,
  `menu_order` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sys_resources
-- ----------------------------
INSERT INTO `sys_resources` VALUES ('55f13a19-b194-4a2d-8e06-1780c689773d', 'prf_mnu_my_profile', '55f13793-ded4-4205-9d35-12f8c689773d', '55f13727-c648-4084-ad69-1780c689773d', null, '1', '2', '1', 'My Profile', 'My Profile', 'users_allowed', '0', '2015-09-10 15:06:49', '2015-09-11 21:56:31');
INSERT INTO `sys_resources` VALUES ('55f18533-ce20-410d-a0cc-14fcc689773d', 'prf_mnu_change_password', '55f13793-ded4-4205-9d35-12f8c689773d', '55f13727-c648-4084-ad69-1780c689773d', null, '3', '4', '1', 'Change Password', 'Profile Menu change_password', 'users_allowed', '1', '2015-09-10 20:27:15', '2015-09-11 22:03:09');
INSERT INTO `sys_resources` VALUES ('55f188ef-ed9c-404d-82f3-14fcc689773d', 'sep_0', '55f13793-ded4-4205-9d35-12f8c689773d', '55f13e51-d9b8-4ec9-9de4-1780c689773d', null, '5', '6', '1', '', '', 'sep_0', '2', '2015-09-10 20:43:11', '2015-09-11 21:59:34');
INSERT INTO `sys_resources` VALUES ('55f189aa-9764-4b82-8b94-080cc689773d', 'sep_1', '55f13793-ded4-4205-9d35-12f8c689773d', '55f13e51-d9b8-4ec9-9de4-1780c689773d', null, '7', '8', '1', '', '', 'sep_1', '4', '2015-09-10 20:46:18', '2015-09-11 21:59:59');
INSERT INTO `sys_resources` VALUES ('55f189d5-50d8-4fba-bfbe-080cc689773d', 'prf_mnu_signout', '55f13793-ded4-4205-9d35-12f8c689773d', '55f13727-c648-4084-ad69-1780c689773d', null, '9', '10', '1', 'Sign Out', 'prf_mnu_signout', 'users_allowed', '3', '2015-09-10 20:47:01', '2015-09-11 22:00:36');
INSERT INTO `sys_resources` VALUES ('55f18ee4-df80-46e7-ab90-14fcc689773d', 'prf_mnu_preves', '55f13793-ded4-4205-9d35-12f8c689773d', '55f13727-c648-4084-ad69-1780c689773d', null, '11', '12', '0', 'Preferences', 'anchor_user_preves', 'users_allowed', '5', '2015-09-10 21:08:36', '2015-09-11 22:03:55');
INSERT INTO `sys_resources` VALUES ('55f19097-962c-48ba-bb24-080cc689773d', 'prf_mnu_help', '55f13793-ded4-4205-9d35-12f8c689773d', '55f13727-c648-4084-ad69-1780c689773d', null, '13', '14', '1', 'Help', 'prf_mnu_help', 'users_allowed', '6', '2015-09-10 21:15:51', '2015-09-11 22:04:21');
INSERT INTO `sys_resources` VALUES ('55f1c6b6-5e68-4e13-9ad8-1770c689773d', 'sysmenu_root', '55f136d6-f8d4-4111-b2e8-1780c689773d', '55f13727-c648-4084-ad69-1780c689773d', null, '15', '26', '1', 'System Administration', 'sysmenu_root', 'sysmenu_root', '99', '2015-09-11 01:06:46', '2015-09-11 10:08:30');
INSERT INTO `sys_resources` VALUES ('55f1ca15-17a8-4d1d-8fa7-177cc689773d', 'sysmenu_usermgmt', '55f136d6-f8d4-4111-b2e8-1780c689773d', '55f13727-c648-4084-ad69-1780c689773d', '55f1c6b6-5e68-4e13-9ad8-1770c689773d', '16', '21', '1', 'Users Management', 'sysmenu_usermgmt', 'sysmenu_usermgmt', '0', '2015-09-11 01:21:09', '2015-09-11 01:21:09');
INSERT INTO `sys_resources` VALUES ('55f2226b-3e64-4760-979f-1710c689773d', 'sysmenu_users', '55f136d6-f8d4-4111-b2e8-1780c689773d', '55f13727-c648-4084-ad69-1780c689773d', '55f1ca15-17a8-4d1d-8fa7-177cc689773d', '17', '18', '1', 'Users', 'sysmenu_users', 'sysmenu_users', '0', '2015-09-11 07:38:03', '2015-09-11 07:38:03');
INSERT INTO `sys_resources` VALUES ('55f23411-2c50-450e-b047-1710c689773d', 'sysmenu_roles', '55f136d6-f8d4-4111-b2e8-1780c689773d', '55f13727-c648-4084-ad69-1780c689773d', '55f1ca15-17a8-4d1d-8fa7-177cc689773d', '19', '20', '1', 'Roles', 'sysmenu_roles', 'sysmenu_roles', '1', '2015-09-11 08:53:21', '2015-09-11 08:53:21');
INSERT INTO `sys_resources` VALUES ('55f23ffc-f8a0-49b7-a8c1-14a0c689773d', 'sysmenu_resmgmt', '55f136d6-f8d4-4111-b2e8-1780c689773d', '55f13727-c648-4084-ad69-1780c689773d', '55f1c6b6-5e68-4e13-9ad8-1770c689773d', '22', '25', '1', 'Resource Management', 'Sysadmin Application Menu Groups\r\nfor Resource Management ', 'sysmenu_resmgmt', '1', '2015-09-11 09:44:12', '2015-09-11 09:44:12');
INSERT INTO `sys_resources` VALUES ('55f2e9f0-a130-4f1c-b798-1a40c689773d', 'sysmenu_priv', '55f136d6-f8d4-4111-b2e8-1780c689773d', '55f13727-c648-4084-ad69-1780c689773d', '55f23ffc-f8a0-49b7-a8c1-14a0c689773d', '23', '24', '1', 'Resources Privileges', 'Resources Privileges & Application Features', 'sysmenu_priv', '0', '2015-09-11 21:49:20', '2015-11-30 15:10:30');
INSERT INTO `sys_resources` VALUES ('5614c64d-c154-4f7f-addb-1aa0c689773d', 'protected_user', '5614c682-f5dc-4012-8d2e-1324c689773d', '5614c513-5484-453e-8e92-1324c689773d', null, '27', '28', '1', 'Protected Users Group', '', 'protected_user', '0', '2015-10-07 14:14:21', '2015-10-07 14:15:39');
INSERT INTO `sys_resources` VALUES ('56161cca-d96c-4104-b967-04ccc689773d', 'user_activation_permission', '5614c682-f5dc-4012-8d2e-1324c689773d', '56161c25-b730-4b68-ad56-1304c689773d', null, '29', '30', '1', '', '', '', null, '2015-10-08 14:35:38', '2015-11-27 21:30:17');
INSERT INTO `sys_resources` VALUES ('56204d3d-f25c-4938-851d-1394c689773d', 'protected_role', '5614c682-f5dc-4012-8d2e-1324c689773d', '5614c513-5484-453e-8e92-1324c689773d', null, '31', '32', '1', 'Protected Group', '', 'protected_role', null, '2015-10-16 08:05:01', '2015-10-16 08:05:58');
INSERT INTO `sys_resources` VALUES ('5629cbf0-ae58-4372-bb50-1ac8c689773d', 'privileges_management_permission', '5614c682-f5dc-4012-8d2e-1324c689773d', '56161c25-b730-4b68-ad56-1304c689773d', null, '33', '34', '1', '', '', '', null, '2015-10-23 12:56:00', '2015-10-23 13:00:04');
INSERT INTO `sys_resources` VALUES ('565bf014-6b60-42d9-8a32-0ee4c689773d', 'syscm', '55f136d6-f8d4-4111-b2e8-1780c689773d', '55f13727-c648-4084-ad69-1780c689773d', null, '35', '58', '1', 'Content Management', 'Content Management Menu', 'syscm', '1', '2015-11-30 13:43:32', '2015-12-04 21:35:44');
INSERT INTO `sys_resources` VALUES ('565bf24c-6ed4-4309-babe-18a0c689773d', 'syscm_content', '55f136d6-f8d4-4111-b2e8-1780c689773d', '55f13727-c648-4084-ad69-1780c689773d', '565bf014-6b60-42d9-8a32-0ee4c689773d', '36', '37', '1', 'Contents', 'Content Management :: Content', 'syscm_content', '0', '2015-11-30 13:53:00', '2015-12-04 21:36:24');
INSERT INTO `sys_resources` VALUES ('565bf24c-6ed4-4309-bxxx-18a0c689773d', 'syscm_galery', '55f136d6-f8d4-4111-b2e8-1780c689773d', '55f13727-c648-4084-ad69-1780c689773d', '565bf014-6b60-42d9-8a32-0ee4c689773d', '38', '39', '1', 'Content Galery', 'Content Management Content Galery', 'syscm_galery', '5', '2020-09-23 10:42:15', '2020-09-23 03:55:04');
INSERT INTO `sys_resources` VALUES ('565c01b5-323c-455f-815a-12dcc689773d', 'syscm_category', '55f136d6-f8d4-4111-b2e8-1780c689773d', '55f13727-c648-4084-ad69-1780c689773d', '565bf014-6b60-42d9-8a32-0ee4c689773d', '40', '41', '1', 'Categories', 'Syscm Content Categories', 'syscm_category', '2', '2015-11-30 14:58:45', '2015-12-19 10:53:11');
INSERT INTO `sys_resources` VALUES ('5661a58f-3dc0-4784-9ef3-06d8c689773d', 'syscm_menu', '55f136d6-f8d4-4111-b2e8-1780c689773d', '55f13727-c648-4084-ad69-1780c689773d', '565bf014-6b60-42d9-8a32-0ee4c689773d', '42', '43', '1', 'Menus', 'Syscm Menu', 'syscm_menu', '3', '2015-12-04 21:39:11', '2015-12-19 10:52:08');
INSERT INTO `sys_resources` VALUES ('5663063c-1a78-4470-b454-1964ded5206b', 'syscm_widget', '55f136d6-f8d4-4111-b2e8-1780c689773d', '55f13727-c648-4084-ad69-1780c689773d', '565bf014-6b60-42d9-8a32-0ee4c689773d', '44', '45', '1', 'Widgets', '', 'syscm_widget', '3', '2015-12-05 22:43:56', '2015-12-05 22:44:06');
INSERT INTO `sys_resources` VALUES ('56630937-1bac-4c0b-862d-15e4ded5206b', 'syscm_layout', '55f136d6-f8d4-4111-b2e8-1780c689773d', '55f13727-c648-4084-ad69-1780c689773d', '565bf014-6b60-42d9-8a32-0ee4c689773d', '46', '47', '1', 'Layouts', '', 'syscm_layout', '4', '2015-12-05 22:56:39', '2015-12-05 22:56:46');
INSERT INTO `sys_resources` VALUES ('566b81c7-e024-4167-ab08-08b8ded5206b', 'syscm_media', '55f136d6-f8d4-4111-b2e8-1780c689773d', '55f13727-c648-4084-ad69-1780c689773d', '565bf014-6b60-42d9-8a32-0ee4c689773d', '48', '49', '1', 'Media', '', 'syscm_media', '1', '2015-12-12 09:09:11', '2015-12-19 10:52:55');
INSERT INTO `sys_resources` VALUES ('5670fc7b-a634-4224-84e1-4205c0b9c32c', 'syscm_customers', '55f136d6-f8d4-4111-b2e8-1780c689773d', '55f13727-c648-4084-ad69-1780c689773d', null, '59', '74', '1', 'Customers', '', 'syscm_cust', '3', '2015-12-15 23:54:03', '2016-01-03 05:20:02');
INSERT INTO `sys_resources` VALUES ('5670fd65-dd88-4bb1-804b-442ac0b9c32c', 'syscm_cust_list', '55f136d6-f8d4-4111-b2e8-1780c689773d', '55f13727-c648-4084-ad69-1780c689773d', '5670fc7b-a634-4224-84e1-4205c0b9c32c', '60', '61', '1', 'Customers List', '', 'syscm_cust_list', '1', '2015-12-15 23:57:57', '2016-02-08 14:25:20');
INSERT INTO `sys_resources` VALUES ('5675bc53-4d98-40fc-93ad-4e1fc0b9c32c', 'syscm_feedback', '55f136d6-f8d4-4111-b2e8-1780c689773d', '55f13727-c648-4084-ad69-1780c689773d', '5670fc7b-a634-4224-84e1-4205c0b9c32c', '62', '63', '0', 'Customer Feedback', '', 'syscm_feedback', '2', '2015-12-19 14:21:39', '2016-02-08 13:56:46');
INSERT INTO `sys_resources` VALUES ('5675bd7c-3534-4406-9579-46dac0b9c32c', 'syscm_feedback_list', '55f136d6-f8d4-4111-b2e8-1780c689773d', '55f13727-c648-4084-ad69-1780c689773d', '5670fc7b-a634-4224-84e1-4205c0b9c32c', '64', '65', '1', 'Customer Feedback', '', 'syscm_feedback_list', '3', '2015-12-19 14:26:36', '2016-02-08 14:26:55');
INSERT INTO `sys_resources` VALUES ('568a676e-d860-49b7-915f-ecefc0b9c32c', 'syscm_cust_bill', '55f136d6-f8d4-4111-b2e8-1780c689773d', '55f13727-c648-4084-ad69-1780c689773d', '5670fc7b-a634-4224-84e1-4205c0b9c32c', '66', '67', '1', 'Customers Billing', '', 'syscm_cust_bill', '2', '2016-01-04 06:37:02', '2016-02-08 14:26:32');
INSERT INTO `sys_resources` VALUES ('56a36074-7264-401e-96e6-06c4ded5206b', 'syscm_cust_comment', '55f136d6-f8d4-4111-b2e8-1780c689773d', '55f13727-c648-4084-ad69-1780c689773d', '5670fc7b-a634-4224-84e1-4205c0b9c32c', '68', '69', '1', 'Visitor Comments', '', 'syscm_cust_comment', '4', '2016-01-23 18:13:56', '2016-02-08 14:27:19');
INSERT INTO `sys_resources` VALUES ('56b50667-3094-4d83-b55d-8b00671fe029', 'publish_content', '55f136d6-f8d4-4111-b2e8-1780c689773d', '56161c25-b730-4b68-ad56-1304c689773d', null, '75', '76', '1', 'Publisher', '', '', null, '2016-02-06 03:30:31', '2016-02-06 03:30:31');
INSERT INTO `sys_resources` VALUES ('56b8392f-2e10-445a-a4f7-c9b5671fe029', 'syscm_cust_reg', '55f136d6-f8d4-4111-b2e8-1780c689773d', '55f13727-c648-4084-ad69-1780c689773d', '5670fc7b-a634-4224-84e1-4205c0b9c32c', '70', '71', '1', 'Customer Registration (Legacy)', '', 'syscm_cust_reg', '0', '2016-02-08 13:43:59', '2020-09-23 09:11:09');
INSERT INTO `sys_resources` VALUES ('5f6b09f4-f52c-4b3e-b485-2790ded5206b', 'syscm_konsultasi', '55f136d6-f8d4-4111-b2e8-1780c689773d', '55f13727-c648-4084-ad69-1780c689773d', '565bf014-6b60-42d9-8a32-0ee4c689773d', '50', '51', '1', 'Pojok Konsultasi', '', 'syscm_konsultasi', '6', '2020-09-23 08:40:20', '2020-09-23 08:40:46');
INSERT INTO `sys_resources` VALUES ('5f6b1174-9f98-4258-a56e-2790ded5206b', 'syscm_cust_reg_new', '55f136d6-f8d4-4111-b2e8-1780c689773d', '55f13727-c648-4084-ad69-1780c689773d', '5670fc7b-a634-4224-84e1-4205c0b9c32c', '72', '73', '1', 'Customer Registration ', '', 'syscm_cust_reg_new', '0', '2020-09-23 09:12:20', '2020-09-23 09:12:20');
INSERT INTO `sys_resources` VALUES ('5f7aeedd-e2d4-4374-a22a-2944ded5206b', 'syscm_homepage_banner', '55f136d6-f8d4-4111-b2e8-1780c689773d', '55f13727-c648-4084-ad69-1780c689773d', '565bf014-6b60-42d9-8a32-0ee4c689773d', '52', '53', '1', 'Homepage Banner', '', 'syscm_homepage_banner', '7', '2020-10-05 10:01:01', '2020-10-05 10:01:01');
INSERT INTO `sys_resources` VALUES ('5f7af07b-ede0-46b5-9df8-2944ded5206b', 'syscm_running_text', '55f136d6-f8d4-4111-b2e8-1780c689773d', '55f13727-c648-4084-ad69-1780c689773d', '565bf014-6b60-42d9-8a32-0ee4c689773d', '54', '55', '1', 'Running Text', '', 'syscm_running_text', '8', '2020-10-05 10:07:55', '2020-10-05 10:07:55');
INSERT INTO `sys_resources` VALUES ('5f7af124-4dc0-43fb-84af-2944ded5206b', 'syscm_info_perumdam', '55f136d6-f8d4-4111-b2e8-1780c689773d', '55f13727-c648-4084-ad69-1780c689773d', '565bf014-6b60-42d9-8a32-0ee4c689773d', '56', '57', '1', 'Info PERUMDAM', '', 'syscm_info_perumdam', '9', '2020-10-05 10:10:44', '2020-10-05 10:10:44');

-- ----------------------------
-- Table structure for `sys_resources_details`
-- ----------------------------
DROP TABLE IF EXISTS `sys_resources_details`;
CREATE TABLE `sys_resources_details` (
  `id` char(36) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `resource_id` char(36) NOT NULL,
  `field` varchar(100) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `active_status` tinyint(1) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `annotation` text DEFAULT NULL,
  `menu_code` varchar(50) DEFAULT NULL,
  `menu_order` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sys_resources_details
-- ----------------------------
INSERT INTO `sys_resources_details` VALUES ('55f14002-7734-4a9d-bfa7-12f8c689773d', 'pre_text', '55f13a19-b194-4a2d-8e06-1780c689773d', 'pre_text', '<span class=\"fa fa-user fa-fw\"></span> ', '1', 'pre_text', 'pre_text', 'html_tag', '0', '2015-09-10 15:32:02', '2015-09-11 21:55:14');
INSERT INTO `sys_resources_details` VALUES ('55f14270-6eec-490a-a32b-12f8c689773d', 'json_url', '55f13a19-b194-4a2d-8e06-1780c689773d', 'json_url', '{\"plugin\":\"sysadmin\",\"controller\":\"sysadmin_app\", \"action\":\"my_profile\"}', '1', 'json_url', 'json_url', 'json_url', '4', '2015-09-10 15:42:24', '2015-09-11 21:55:45');
INSERT INTO `sys_resources_details` VALUES ('55f186a4-d798-4655-87c0-14fcc689773d', 'pre_text', '55f18533-ce20-410d-a0cc-14fcc689773d', 'pre_text', '<span class=\"fa fa-key fa-fw\"></span> ', '1', 'pre_text', 'pre_text', 'html_tag', '0', '2015-09-10 20:33:24', '2015-09-11 22:02:27');
INSERT INTO `sys_resources_details` VALUES ('55f187fd-61f0-471a-9cbd-080cc689773d', 'json_url', '55f18533-ce20-410d-a0cc-14fcc689773d', 'json_url', '{\"plugin\":\"sysadmin\",\"controller\":\"sysadmin_app\", \"action\":\"change_password\"}', '1', 'json_url', 'json_url', 'json_url', '3', '2015-09-10 20:39:09', '2015-09-11 22:04:47');
INSERT INTO `sys_resources_details` VALUES ('55f18d9f-ebf4-4379-b598-14fcc689773d', 'pre_text', '55f189d5-50d8-4fba-bfbe-080cc689773d', 'pre_text', '<span class=\"fa fa-sign-out fa-fw\"></span> ', '1', 'pre_text', 'pre_text', 'html_tag', '0', '2015-09-10 21:03:11', '2015-09-11 22:01:08');
INSERT INTO `sys_resources_details` VALUES ('55f18e36-2ae8-402e-ab80-14fcc689773d', 'json_url', '55f189d5-50d8-4fba-bfbe-080cc689773d', 'json_url', '{\"plugin\":\"sysadmin\",\"controller\":\"sysadmin_app\", \"action\":\"signout\"}', '1', 'json_url', 'json_url', 'json_url', '3', '2015-09-10 21:05:42', '2015-09-11 22:01:46');
INSERT INTO `sys_resources_details` VALUES ('55f18f63-1ec4-4558-947f-080cc689773d', 'pre_text', '55f18ee4-df80-46e7-ab90-14fcc689773d', 'pre_text', '<span class=\"fa fa-cogs fa-fw\"></span> ', '1', 'pre_text', 'pre_text', 'html_tag', '0', '2015-09-10 21:10:43', '2015-09-11 22:05:08');
INSERT INTO `sys_resources_details` VALUES ('55f190fb-7c80-4c6e-a24f-080cc689773d', 'pre_text', '55f19097-962c-48ba-bb24-080cc689773d', 'pre_text', '<span class=\"fa fa-question fa-fw\"></span> ', '1', 'pre_text', 'pre_text', 'html_tag', '0', '2015-09-10 21:17:31', '2015-09-11 22:05:34');
INSERT INTO `sys_resources_details` VALUES ('55f1914a-d720-4d72-ad7e-080cc689773d', 'json_url', '55f19097-962c-48ba-bb24-080cc689773d', 'json_url', '{\"plugin\":\"sysadmin\",\"controller\":\"sysadmin_app\", \"action\":\"help\"}', '1', 'json_url', 'json_url', 'json_url', '3', '2015-09-10 21:18:50', '2015-09-11 22:06:00');
INSERT INTO `sys_resources_details` VALUES ('55f191d1-da60-4b5c-97d5-14fcc689773d', 'json_url', '55f18ee4-df80-46e7-ab90-14fcc689773d', 'json_url', '{\"plugin\":\"sysadmin\",\"controller\":\"sysadmin_app\", \"action\":\"user_preferences\"}', '1', 'json_url', 'json_url', 'json_url', '3', '2015-09-10 21:21:05', '2015-09-11 22:06:16');
INSERT INTO `sys_resources_details` VALUES ('55f1c759-2b8c-4b4d-a098-1770c689773d', 'pre_text', '55f1c6b6-5e68-4e13-9ad8-1770c689773d', 'pre_text', '<span class=\"fa fa-cogs fa-fw\"></span> ', '1', 'pre_text', 'pre_text', 'html_tag', '0', '2015-09-11 01:09:29', '2015-09-11 01:09:29');
INSERT INTO `sys_resources_details` VALUES ('55f222fd-da14-40ae-89bb-14a0c689773d', 'pre_text', '55f1ca15-17a8-4d1d-8fa7-177cc689773d', 'pre_text', '<span class=\"fa fa-user fa-fw\"></span>', '1', 'pre_text', 'pre_text', 'html_tag', '0', '2015-09-11 07:40:29', '2015-09-11 09:02:33');
INSERT INTO `sys_resources_details` VALUES ('55f22367-db90-4e7b-b9fe-14a0c689773d', 'json_url', '55f2226b-3e64-4760-979f-1710c689773d', 'json_url', '{\"plugin\":\"sysadmin\",\"controller\":\"users\", \"action\":\"index\"}', '1', 'json_url', 'json_url', 'json_url', '2', '2015-09-11 07:42:15', '2015-09-11 07:42:15');
INSERT INTO `sys_resources_details` VALUES ('55f236b1-e364-402b-a0f3-14a0c689773d', 'json_url', '55f23411-2c50-450e-b047-1710c689773d', 'json_url', '{\"plugin\":\"sysadmin\",\"controller\":\"roles\", \"action\":\"index\"}', '1', 'json_url', 'json_url', 'json_url', '2', '2015-09-11 09:04:33', '2015-09-11 09:04:33');
INSERT INTO `sys_resources_details` VALUES ('55f2eaf2-b160-4118-9e5d-1968c689773d', 'json_url', '55f2e9f0-a130-4f1c-b798-1a40c689773d', 'json_url', '{\"plugin\":\"sysadmin\", \"controller\":\"privileges\", \"action\":\"index\"}', '1', 'json_url', 'json_url', 'json_url', '0', '2015-09-11 21:53:38', '2015-09-11 21:53:38');
INSERT INTO `sys_resources_details` VALUES ('55f2f6ef-c42c-49e6-84cb-1144c689773d', 'pre_text', '55f23ffc-f8a0-49b7-a8c1-14a0c689773d', 'pre_text', '<span class=\"fa fa-object-group fa-fw\"></span> ', '1', 'pre_text', 'pre_text', 'html_tag', '0', '2015-09-11 22:44:47', '2015-09-11 22:44:47');
INSERT INTO `sys_resources_details` VALUES ('55fe6d5d-1de4-44bf-a052-1168c689773d', 'json_url', '55f1c6b6-5e68-4e13-9ad8-1770c689773d', 'json_url', '{\"plugin\":\"sysadmin\",\"controller\":\"sysadmin_app\", \"action\":\"index\"}', '1', '', '', 'json_url', null, '2015-09-20 15:25:01', '2015-09-20 15:25:01');
INSERT INTO `sys_resources_details` VALUES ('56161d49-36b0-4cdd-89a1-1974c689773d', 'json_url', '56161cca-d96c-4104-b967-04ccc689773d', 'json_url', '{\"plugin\":\"sysadmin\",\"controller\":\"users\", \"action\":\"user_activation\"}', '1', '', '', '', null, '2015-10-08 14:37:45', '2015-10-08 14:37:45');
INSERT INTO `sys_resources_details` VALUES ('56168684-86e0-4493-b55e-11ccc689773d', 'protected_user_id', '5614c64d-c154-4f7f-addb-1aa0c689773d', 'user_id', '55f285b8-2b4c-4558-89b9X0984c689773d', '1', '', '', 'protected_user_id', null, '2015-10-08 22:06:44', '2015-10-08 22:07:38');
INSERT INTO `sys_resources_details` VALUES ('562079b7-1b54-4bdb-8f12-1394c689773d', 'protected_role_id', '56204d3d-f25c-4938-851d-1394c689773d', 'role_id', '55f27924-fef8-4337-bf8eX1760c689773d', '1', '', '', 'protected_role_id', null, '2015-10-16 11:14:47', '2015-10-16 11:15:16');
INSERT INTO `sys_resources_details` VALUES ('5629ccbc-6078-42fd-a0bf-1ac8c689773d', 'privileges_management_permission', '5629cbf0-ae58-4372-bb50-1ac8c689773d', 'json_url', '{\"plugin\":\"sysadmin\",\"controller\":\"roles\", \"action\":\"privileges_management\"}', '1', '', '', '', null, '2015-10-23 12:59:24', '2015-10-23 13:00:51');
INSERT INTO `sys_resources_details` VALUES ('565bf148-a410-4d96-bad3-1b24c689773d', 'pre_text', '565bf014-6b60-42d9-8a32-0ee4c689773d', 'pre_text', '<span class=\"fa fa-pen-square fa-fw\"></span>', '1', 'pre_text', '', 'pre_text', '0', '2015-11-30 13:48:40', '2015-12-10 14:55:28');
INSERT INTO `sys_resources_details` VALUES ('565bf2e7-6278-4e6a-932a-1b24c689773d', 'syscm_content_pre_text', '565bf24c-6ed4-4309-babe-18a0c689773d', 'pre_text', '<span class=\"fa fa-pen-square fa-fw\"></span>', '1', 'pre_text', 'Contents Management (syscm_content) :: pre_text', 'syscm_content_pre_text', '0', '2015-11-30 13:55:35', '2015-12-10 14:56:20');
INSERT INTO `sys_resources_details` VALUES ('565bf3a5-4c84-4a3a-8ea7-1b24c689773d', 'syscm_content_json_url', '565bf24c-6ed4-4309-babe-18a0c689773d', 'json_url', '{\"plugin\":\"syscm\",\"controller\":\"syscm_contents\", \"action\":\"index\"}', '1', 'json_url', 'Contents Management (syscm_content) :: syscm_content_json_url', 'syscm_content_json_url', '1', '2015-11-30 13:58:45', '2015-11-30 13:58:45');
INSERT INTO `sys_resources_details` VALUES ('565bf92d-0d00-4aa7-bc3f-0d80c689773d', 'syscm_json_url', '565bf014-6b60-42d9-8a32-0ee4c689773d', 'json_url', '{\"plugin\":\"syscm\",\"controller\":\"syscm\", \"action\":\"index\"}', '1', 'json_url', 'syscm_json_url', '', '1', '2015-11-30 14:22:21', '2015-11-30 14:23:05');
INSERT INTO `sys_resources_details` VALUES ('565c01cc-4cc8-499c-a45a-18a0c689773d', 'pre_text', '565c01b5-323c-455f-815a-12dcc689773d', 'pre_text', '<span class=\"fa fa-th-list fa-fw\"></span>', '1', 'pre_text', '', '', null, '2015-11-30 14:59:08', '2015-12-12 09:08:05');
INSERT INTO `sys_resources_details` VALUES ('565c01f2-cecc-46e1-b9f2-12dcc689773d', 'json_url', '565c01b5-323c-455f-815a-12dcc689773d', 'json_url', '{\"plugin\":\"syscm\",\"controller\":\"syscm_categories\", \"action\":\"index\"}', '1', '', '', '', null, '2015-11-30 14:59:46', '2015-11-30 14:59:46');
INSERT INTO `sys_resources_details` VALUES ('5661b4f2-2860-48af-bec1-111cded5206b', 'pre_text', '5661a58f-3dc0-4784-9ef3-06d8c689773d', 'pre_text', '<span class=\"fa fa-bars fa-fw\"></span>', '1', '', '', '', null, '2015-12-04 22:44:50', '2015-12-04 22:44:50');
INSERT INTO `sys_resources_details` VALUES ('5661b518-a3d4-4da3-9f83-111cded5206b', 'json_url', '5661a58f-3dc0-4784-9ef3-06d8c689773d', 'json_url', '{\"plugin\":\"syscm\",\"controller\":\"syscm_menus\",\"action\":\"index\"}', '1', '', '', '', null, '2015-12-04 22:45:28', '2015-12-04 22:45:28');
INSERT INTO `sys_resources_details` VALUES ('5663070a-9b18-45fc-a23a-1960ded5206b', 'pre_text', '5663063c-1a78-4470-b454-1964ded5206b', 'pre_text', '<span class=\"fa fa-gear fa-fw\"></span>', '1', '', '', '', null, '2015-12-05 22:47:22', '2015-12-05 22:47:22');
INSERT INTO `sys_resources_details` VALUES ('56630764-9904-4b22-aa25-1964ded5206b', 'json_url', '5663063c-1a78-4470-b454-1964ded5206b', 'json_url', '{\"plugin\":\"syscm\",\"controller\":\"syscm_widgets\",\"action\":\"index\"}', '1', '', '', '', null, '2015-12-05 22:48:52', '2015-12-05 22:48:52');
INSERT INTO `sys_resources_details` VALUES ('566309e1-fe08-4dde-aac6-15e4ded5206b', 'pre_text', '56630937-1bac-4c0b-862d-15e4ded5206b', 'pre_text', '<span class=\"fa fa-list fa-fw\"></span>', '1', '', '', '', null, '2015-12-05 22:59:29', '2015-12-05 22:59:29');
INSERT INTO `sys_resources_details` VALUES ('56630a00-4984-4faf-8a5b-0d7cded5206b', 'json_url', '56630937-1bac-4c0b-862d-15e4ded5206b', 'json_url', '{\"plugin\":\"syscm\",\"controller\":\"syscm_layouts\",\"action\":\"index\"}', '1', '', '', '', null, '2015-12-05 23:00:00', '2015-12-05 23:00:00');
INSERT INTO `sys_resources_details` VALUES ('566b8226-be8c-4e73-b8e1-08b8ded5206b', 'pre_text', '566b81c7-e024-4167-ab08-08b8ded5206b', 'pre_text', '<span class=\"fa fa-file-image fa-fw\"></span>', '1', '', '', '', null, '2015-12-12 09:10:46', '2015-12-12 09:14:10');
INSERT INTO `sys_resources_details` VALUES ('566b8259-a51c-4679-baf9-0534ded5206b', 'json_url', '566b81c7-e024-4167-ab08-08b8ded5206b', 'json_url', '{\"plugin\":\"syscm\",\"controller\":\"syscm_media\", \"action\":\"index\"}', '1', '', '', '', null, '2015-12-12 09:11:37', '2015-12-12 09:11:37');
INSERT INTO `sys_resources_details` VALUES ('5670fcbd-c624-4143-bee0-4bcec0b9c32c', 'pre_text', '5670fc7b-a634-4224-84e1-4205c0b9c32c', 'pre_text', '<span class=\"fa fa-users fa-fw\"></span> ', '1', '', '', '', null, '2015-12-15 23:55:09', '2015-12-15 23:55:09');
INSERT INTO `sys_resources_details` VALUES ('5670fd7b-00e0-4f56-9763-43edc0b9c32c', 'pre_text', '5670fd65-dd88-4bb1-804b-442ac0b9c32c', 'pre_text', '<span class=\"fa fa-users fa-fw\"></span>', '1', '', '', '', null, '2015-12-15 23:58:19', '2015-12-15 23:58:19');
INSERT INTO `sys_resources_details` VALUES ('5670fda3-95c4-47aa-9020-4826c0b9c32c', 'json_url', '5670fd65-dd88-4bb1-804b-442ac0b9c32c', 'json_url', '{\"plugin\":\"syscm\",\"controller\":\"syscm_cust\", \"action\":\"index\"}', '1', '', '', '', null, '2015-12-15 23:58:59', '2016-01-03 05:13:17');
INSERT INTO `sys_resources_details` VALUES ('5675bc9d-7b84-4ff9-8ef2-49fdc0b9c32c', 'pre_text', '5675bc53-4d98-40fc-93ad-4e1fc0b9c32c', 'pre_text', '<span class=\"fa fa-comments fa-fw\"></span>', '1', '', '', '', null, '2015-12-19 14:22:53', '2015-12-19 14:22:53');
INSERT INTO `sys_resources_details` VALUES ('5675bda4-c000-410d-bc6d-4fbdc0b9c32c', 'pre_text', '5675bd7c-3534-4406-9579-46dac0b9c32c', 'pre_text', '<span class=\"fa fa-comments fa-fw\"></span>', '1', '', '', '', '0', '2015-12-19 14:27:16', '2016-02-08 13:53:38');
INSERT INTO `sys_resources_details` VALUES ('5675bdc5-23d8-4bdd-a649-4296c0b9c32c', 'json_url', '5675bd7c-3534-4406-9579-46dac0b9c32c', 'json_url', '{\"plugin\":\"syscm\",\"controller\":\"syscm_feedback\",\"action\":\"index\"}', '1', '', '', '', null, '2015-12-19 14:27:49', '2015-12-19 14:27:49');
INSERT INTO `sys_resources_details` VALUES ('568a67df-c198-47b6-95df-f826c0b9c32c', 'pre_text', '568a676e-d860-49b7-915f-ecefc0b9c32c', 'pre_text', '<span class=\"fa fa-money-bill fa-fw\"></span>', '1', '', '', '', null, '2016-01-04 06:38:55', '2016-01-04 06:38:55');
INSERT INTO `sys_resources_details` VALUES ('568a67fd-509c-4058-8fce-fba7c0b9c32c', 'json_url', '568a676e-d860-49b7-915f-ecefc0b9c32c', 'json_url', '{\"plugin\":\"syscm\",\"controller\":\"syscm_cust_bill\", \"action\":\"index\"}', '1', '', '', '', null, '2016-01-04 06:39:25', '2016-01-04 06:39:25');
INSERT INTO `sys_resources_details` VALUES ('56a36107-c6d8-40d8-88e0-0fe0ded5206b', 'pre_text', '56a36074-7264-401e-96e6-06c4ded5206b', 'pre_text', '<span class=\"fa fa-comment fa-fw\"></span>', '1', '', '', '', null, '2016-01-23 18:16:23', '2016-01-23 18:16:23');
INSERT INTO `sys_resources_details` VALUES ('56a36134-4f88-466a-a30b-167cded5206b', 'json_url', '56a36074-7264-401e-96e6-06c4ded5206b', 'json_url', '{\"plugin\":\"syscm\",\"controller\":\"syscm_cust_comment\",\"action\":\"index\"}', '1', '', '', '', null, '2016-01-23 18:17:08', '2016-01-23 18:17:08');
INSERT INTO `sys_resources_details` VALUES ('56b513ad-e0b4-4f53-83ac-425b671fe029', 'json_url', '56b50667-3094-4d83-b55d-8b00671fe029', 'json_url', '{\"plugin\":\"sysadmin\",\"controller\":\"publish_content\",\"action\":\"index\"}', '1', '', '', '', null, '2016-02-06 04:27:09', '2016-02-06 04:27:09');
INSERT INTO `sys_resources_details` VALUES ('56b839c1-1354-490d-bc5f-d212671fe029', 'pre_text', '56b8392f-2e10-445a-a4f7-c9b5671fe029', 'pre_text', '<span class=\"fa fa-check-square fa-fw\"></span>', '1', '', '', '', null, '2016-02-08 13:46:25', '2016-02-08 13:46:25');
INSERT INTO `sys_resources_details` VALUES ('56b839d6-cf24-47c3-82bd-d367671fe029', 'json_url', '56b8392f-2e10-445a-a4f7-c9b5671fe029', 'json_url', '{\"plugin\":\"syscm\",\"controller\":\"syscm_cust_reg\",\"action\":\"index\"}', '1', '', '', '', null, '2016-02-08 13:46:46', '2016-02-08 13:46:46');
INSERT INTO `sys_resources_details` VALUES ('5f6ac636-e260-4736-b67f-1804ded5206b', 'pre_text', '565bf24c-6ed4-4309-bxxx-18a0c689773d', 'pre_text', '<span class=\"fa fa-check-square fa-fw\"></span>', '1', '', '', '', null, '2020-09-23 03:51:18', '2020-09-23 03:51:18');
INSERT INTO `sys_resources_details` VALUES ('5f6ac674-827c-4157-af06-1804ded5206b', 'json_url', '565bf24c-6ed4-4309-bxxx-18a0c689773d', 'json_url', '{\"plugin\":\"syscm\",\"controller\":\"syscm_galery\",\"action\":\"index\"}', '1', '', '', '', null, '2020-09-23 03:52:20', '2020-09-23 03:52:20');
INSERT INTO `sys_resources_details` VALUES ('5f6b0a35-c7d0-4deb-b498-0804ded5206b', 'pre_text', '5f6b09f4-f52c-4b3e-b485-2790ded5206b', 'pre_text', '<span class=\"fa fa-check-square fa-fw\"></span>', '1', '', '', '', null, '2020-09-23 08:41:25', '2020-09-23 08:41:25');
INSERT INTO `sys_resources_details` VALUES ('5f6b0a79-7b54-4c08-936d-2790ded5206b', 'json_url', '5f6b09f4-f52c-4b3e-b485-2790ded5206b', 'json_url', '{\"plugin\":\"syscm\",\"controller\":\"syscm_konsultasi\",\"action\":\"index\"}', '1', '', '', '', null, '2020-09-23 08:42:33', '2020-09-23 08:42:33');
INSERT INTO `sys_resources_details` VALUES ('5f6b11ab-8ab8-4eb7-996f-0804ded5206b', 'pre_text', '5f6b1174-9f98-4258-a56e-2790ded5206b', 'pre_text', '<span class=\"fa fa-check-square  fa-fw\"></span>', '1', '', '', '', null, '2020-09-23 09:13:15', '2020-09-23 09:13:15');
INSERT INTO `sys_resources_details` VALUES ('5f6b11ec-e0c0-4b36-b920-1f1cded5206b', 'json_url', '5f6b1174-9f98-4258-a56e-2790ded5206b', 'json_url', '{\"plugin\":\"syscm\",\"controller\":\"syscm_register\",\"action\":\"index\"}', '1', '', '', '', null, '2020-09-23 09:14:20', '2020-09-23 09:14:20');
INSERT INTO `sys_resources_details` VALUES ('5f7aef0c-9d18-4de0-bceb-2fdcded5206b', 'pre_text', '5f7aeedd-e2d4-4374-a22a-2944ded5206b', 'pre_text', '<span class=\"fa fa-check-square fa-fw\"></span>', '1', '', '', '', null, '2020-10-05 10:01:48', '2020-10-05 10:01:48');
INSERT INTO `sys_resources_details` VALUES ('5f7aef33-dcf8-439c-a110-2fdcded5206b', 'json_url', '5f7aeedd-e2d4-4374-a22a-2944ded5206b', 'json_url', '{\"plugin\":\"syscm\",\"controller\":\"syscm_banner\",\"action\":\"index\"}', '1', '', '', '', null, '2020-10-05 10:02:27', '2020-10-05 10:02:27');
INSERT INTO `sys_resources_details` VALUES ('5f7af08b-ce80-47d9-b524-2fdcded5206b', 'pre_text', '5f7af07b-ede0-46b5-9df8-2944ded5206b', 'pre_text', '<span class=\"fa fa-check-square fa-fw\"></span>', '1', '', '', '', null, '2020-10-05 10:08:11', '2020-10-05 10:08:11');
INSERT INTO `sys_resources_details` VALUES ('5f7af0a0-0900-48e5-9c8e-2fdcded5206b', 'json_url', '5f7af07b-ede0-46b5-9df8-2944ded5206b', 'json_url', '{\"plugin\":\"syscm\",\"controller\":\"syscm_running_text\",\"action\":\"index\"}', '1', '', '', '', null, '2020-10-05 10:08:32', '2020-10-05 10:08:32');
INSERT INTO `sys_resources_details` VALUES ('5f7af132-dd08-4f29-9f97-2fdcded5206b', 'pre_text', '5f7af124-4dc0-43fb-84af-2944ded5206b', 'pre_text', '<span class=\"fa fa-check-square fa-fw\"></span>', '1', '', '', '', null, '2020-10-05 10:10:58', '2020-10-05 10:10:58');
INSERT INTO `sys_resources_details` VALUES ('5f7af147-bd0c-4298-ba68-2fdcded5206b', 'json_url', '5f7af124-4dc0-43fb-84af-2944ded5206b', 'json_url', '{\"plugin\":\"syscm\",\"controller\":\"syscm_info_perumdam\",\"action\":\"index\"}', '1', '', '', '', null, '2020-10-05 10:11:19', '2020-10-05 10:11:19');

-- ----------------------------
-- Table structure for `sys_resources_groups`
-- ----------------------------
DROP TABLE IF EXISTS `sys_resources_groups`;
CREATE TABLE `sys_resources_groups` (
  `id` char(36) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `active_status` tinyint(1) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `annotation` text DEFAULT NULL,
  `menu_code` varchar(50) DEFAULT NULL,
  `menu_order` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sys_resources_groups
-- ----------------------------
INSERT INTO `sys_resources_groups` VALUES ('55f136d6-f8d4-4111-b2e8-1780c689773d', 'sysadmin_app_menu', '1', 'Sysadmin Application Menu', 'Sysadmin Application Menu', 'sysadmin_app_menu', '0', '2015-09-10 14:52:54', '2015-09-10 14:52:54');
INSERT INTO `sys_resources_groups` VALUES ('55f13793-ded4-4205-9d35-12f8c689773d', 'sysadmin_profile_menu', '1', 'Sysadmin Profile Menu', 'Sysadmin Profile Menu', 'sysadmin_profile_menu', '1', '2015-09-10 14:56:03', '2015-09-10 14:56:03');
INSERT INTO `sys_resources_groups` VALUES ('5614c682-f5dc-4012-8d2e-1324c689773d', 'sysadmin_users', '1', 'Sysadmin Users', '', 'sysadmin_users', '0', '2015-10-07 14:15:13', '2015-10-07 14:15:13');

-- ----------------------------
-- Table structure for `sys_resources_proceedings`
-- ----------------------------
DROP TABLE IF EXISTS `sys_resources_proceedings`;
CREATE TABLE `sys_resources_proceedings` (
  `id` char(36) NOT NULL,
  `resource_id` char(36) NOT NULL,
  `proceeding_id` char(36) NOT NULL,
  `active_status` tinyint(1) DEFAULT 1,
  `menu_code` varchar(50) DEFAULT NULL,
  `menu_order` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sys_resources_proceedings
-- ----------------------------
INSERT INTO `sys_resources_proceedings` VALUES ('55f1c6b6-c304-47cc-8116-1770c689773d', '55f1c6b6-5e68-4e13-9ad8-1770c689773d', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('55f1ca15-a1e8-44e3-bde6-177cc689773d', '55f1ca15-17a8-4d1d-8fa7-177cc689773d', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('55f2226b-4af8-4962-9a73-1710c689773d', '55f2226b-3e64-4760-979f-1710c689773d', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('55f2226b-6bc8-4ccc-96af-1710c689773d', '55f2226b-3e64-4760-979f-1710c689773d', '55f1353a-800c-4370-930b-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('55f2226b-84c8-4c5d-8a67-1710c689773d', '55f2226b-3e64-4760-979f-1710c689773d', '55f13558-1a0c-4885-9584-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('55f2226b-9bd4-404f-ae3b-1710c689773d', '55f2226b-3e64-4760-979f-1710c689773d', '55f13590-fd38-4fa0-a5ed-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('55f23411-2e98-4b73-ab8c-1710c689773d', '55f23411-2c50-450e-b047-1710c689773d', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('55f23411-5288-4b51-97a8-1710c689773d', '55f23411-2c50-450e-b047-1710c689773d', '55f1353a-800c-4370-930b-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('55f23411-6c50-44c1-855a-1710c689773d', '55f23411-2c50-450e-b047-1710c689773d', '55f13558-1a0c-4885-9584-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('55f23411-8424-4677-a821-1710c689773d', '55f23411-2c50-450e-b047-1710c689773d', '55f13590-fd38-4fa0-a5ed-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('55f23ffc-99c8-4ceb-9f25-14a0c689773d', '55f23ffc-f8a0-49b7-a8c1-14a0c689773d', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('55f2e9f0-66ec-4b3e-929c-1a40c689773d', '55f2e9f0-a130-4f1c-b798-1a40c689773d', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('55f2e9f0-894c-4eef-88c3-1a40c689773d', '55f2e9f0-a130-4f1c-b798-1a40c689773d', '55f1353a-800c-4370-930b-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('55f2e9f0-a184-4208-a606-1a40c689773d', '55f2e9f0-a130-4f1c-b798-1a40c689773d', '55f13558-1a0c-4885-9584-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('55f2e9f0-b8f4-4792-a509-1a40c689773d', '55f2e9f0-a130-4f1c-b798-1a40c689773d', '55f13590-fd38-4fa0-a5ed-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('55f2e9f0-d064-4f79-999b-1a40c689773d', '55f2e9f0-a130-4f1c-b798-1a40c689773d', '55f135cf-bae0-48ae-9d67-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('55fda9c3-07c8-4afe-b95d-18c0c689773d', '55f2226b-3e64-4760-979f-1710c689773d', '55f135cf-bae0-48ae-9d67-12f8c689773d', '1', '', null, '2015-09-20 01:30:27', '2015-09-20 01:30:27');
INSERT INTO `sys_resources_proceedings` VALUES ('56161cca-5080-436c-958c-04ccc689773d', '56161cca-d96c-4104-b967-04ccc689773d', '55f135ed-e970-47c3-9d0e-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5624ffee-39b4-48e4-9039-174cc689773d', '55f23411-2c50-450e-b047-1710c689773d', '55f135cf-bae0-48ae-9d67-12f8c689773d', '1', '', null, '2015-10-19 21:36:30', '2015-10-19 21:36:30');
INSERT INTO `sys_resources_proceedings` VALUES ('5629cbf0-1838-43b5-b372-1ac8c689773d', '5629cbf0-ae58-4372-bb50-1ac8c689773d', '55f13558-1a0c-4885-9584-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5629cbf0-30d4-4a7e-8ab5-1ac8c689773d', '5629cbf0-ae58-4372-bb50-1ac8c689773d', '55f13590-fd38-4fa0-a5ed-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5629cbf0-48a8-427f-8ffc-1ac8c689773d', '5629cbf0-ae58-4372-bb50-1ac8c689773d', '55f135cf-bae0-48ae-9d67-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5629cbf0-da1c-41a9-bccc-1ac8c689773d', '5629cbf0-ae58-4372-bb50-1ac8c689773d', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5629cbf0-fe0c-4241-9d88-1ac8c689773d', '5629cbf0-ae58-4372-bb50-1ac8c689773d', '55f1353a-800c-4370-930b-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('565bf0f9-9a90-463f-ae98-14b4c689773d', '565bf014-6b60-42d9-8a32-0ee4c689773d', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, '2015-11-30 13:47:21', '2015-11-30 13:47:21');
INSERT INTO `sys_resources_proceedings` VALUES ('565bf303-8528-4bcc-be48-14b4c689773d', '565bf24c-6ed4-4309-babe-18a0c689773d', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, '2015-11-30 13:56:03', '2015-11-30 13:56:03');
INSERT INTO `sys_resources_proceedings` VALUES ('565bf308-f5f0-474a-b25f-1b24c689773d', '565bf24c-6ed4-4309-babe-18a0c689773d', '55f1353a-800c-4370-930b-12f8c689773d', '1', null, null, '2015-11-30 13:56:08', '2015-11-30 13:56:08');
INSERT INTO `sys_resources_proceedings` VALUES ('565bf30e-1e0c-457d-91d7-0ee4c689773d', '565bf24c-6ed4-4309-babe-18a0c689773d', '55f13558-1a0c-4885-9584-1780c689773d', '1', null, null, '2015-11-30 13:56:14', '2015-11-30 13:56:14');
INSERT INTO `sys_resources_proceedings` VALUES ('565bf313-9c88-4602-9687-0d80c689773d', '565bf24c-6ed4-4309-babe-18a0c689773d', '55f13590-fd38-4fa0-a5ed-1780c689773d', '1', null, null, '2015-11-30 13:56:19', '2015-11-30 13:56:19');
INSERT INTO `sys_resources_proceedings` VALUES ('565bf318-555c-433d-a4ef-12dcc689773d', '565bf24c-6ed4-4309-babe-18a0c689773d', '55f135cf-bae0-48ae-9d67-12f8c689773d', '1', null, null, '2015-11-30 13:56:24', '2015-11-30 13:56:24');
INSERT INTO `sys_resources_proceedings` VALUES ('565c0210-534c-4fc3-b712-0d80c689773d', '565c01b5-323c-455f-815a-12dcc689773d', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, '2015-11-30 15:00:16', '2015-11-30 15:00:16');
INSERT INTO `sys_resources_proceedings` VALUES ('565c0217-d5e4-4f0c-9e9c-12dcc689773d', '565c01b5-323c-455f-815a-12dcc689773d', '55f1353a-800c-4370-930b-12f8c689773d', '1', null, null, '2015-11-30 15:00:23', '2015-11-30 15:00:23');
INSERT INTO `sys_resources_proceedings` VALUES ('565c021d-e730-4dcf-afdf-056cc689773d', '565c01b5-323c-455f-815a-12dcc689773d', '55f13558-1a0c-4885-9584-1780c689773d', '1', null, null, '2015-11-30 15:00:29', '2015-11-30 15:00:29');
INSERT INTO `sys_resources_proceedings` VALUES ('565c0223-ae5c-4297-a849-18a0c689773d', '565c01b5-323c-455f-815a-12dcc689773d', '55f13590-fd38-4fa0-a5ed-1780c689773d', '1', null, null, '2015-11-30 15:00:35', '2015-11-30 15:00:35');
INSERT INTO `sys_resources_proceedings` VALUES ('565c0229-f414-44f9-98ac-14b4c689773d', '565c01b5-323c-455f-815a-12dcc689773d', '55f135cf-bae0-48ae-9d67-12f8c689773d', '1', null, null, '2015-11-30 15:00:41', '2015-11-30 15:00:41');
INSERT INTO `sys_resources_proceedings` VALUES ('5661a59a-2f08-4e7a-8372-17e0c689773d', '5661a58f-3dc0-4784-9ef3-06d8c689773d', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5661a59a-6cc0-4989-b814-17e0c689773d', '5661a58f-3dc0-4784-9ef3-06d8c689773d', '55f1353a-800c-4370-930b-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5661a59a-9948-4912-aff8-17e0c689773d', '5661a58f-3dc0-4784-9ef3-06d8c689773d', '55f13558-1a0c-4885-9584-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5661a59a-c508-4520-8044-17e0c689773d', '5661a58f-3dc0-4784-9ef3-06d8c689773d', '55f13590-fd38-4fa0-a5ed-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5661a59a-ef9c-42de-a8f1-17e0c689773d', '5661a58f-3dc0-4784-9ef3-06d8c689773d', '55f135cf-bae0-48ae-9d67-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('56630646-3498-408a-acf6-0d7cded5206b', '5663063c-1a78-4470-b454-1964ded5206b', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('56630646-6cd8-42be-9f06-0d7cded5206b', '5663063c-1a78-4470-b454-1964ded5206b', '55f1353a-800c-4370-930b-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('56630646-9834-4a31-beaa-0d7cded5206b', '5663063c-1a78-4470-b454-1964ded5206b', '55f13558-1a0c-4885-9584-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('56630646-c19c-4f44-a044-0d7cded5206b', '5663063c-1a78-4470-b454-1964ded5206b', '55f13590-fd38-4fa0-a5ed-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('56630646-ebcc-42ad-9092-0d7cded5206b', '5663063c-1a78-4470-b454-1964ded5206b', '55f135cf-bae0-48ae-9d67-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5663093e-388c-4c3b-9677-1960ded5206b', '56630937-1bac-4c0b-862d-15e4ded5206b', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5663093e-5da8-4920-9d10-1960ded5206b', '56630937-1bac-4c0b-862d-15e4ded5206b', '55f1353a-800c-4370-930b-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5663093e-7770-4103-b098-1960ded5206b', '56630937-1bac-4c0b-862d-15e4ded5206b', '55f13558-1a0c-4885-9584-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5663093e-8ee0-43fc-9972-1960ded5206b', '56630937-1bac-4c0b-862d-15e4ded5206b', '55f13590-fd38-4fa0-a5ed-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5663093e-a5ec-463c-a8a2-1960ded5206b', '56630937-1bac-4c0b-862d-15e4ded5206b', '55f135cf-bae0-48ae-9d67-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('566b81d5-0574-4673-af04-0534ded5206b', '566b81c7-e024-4167-ab08-08b8ded5206b', '55f13558-1a0c-4885-9584-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('566b81d5-1d48-4a77-8df8-0534ded5206b', '566b81c7-e024-4167-ab08-08b8ded5206b', '55f13590-fd38-4fa0-a5ed-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('566b81d5-33f0-45ab-b00c-0534ded5206b', '566b81c7-e024-4167-ab08-08b8ded5206b', '55f135cf-bae0-48ae-9d67-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('566b81d5-c9b0-46ce-aef0-0534ded5206b', '566b81c7-e024-4167-ab08-08b8ded5206b', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('566b81d5-ecd8-4c0b-a6d3-0534ded5206b', '566b81c7-e024-4167-ab08-08b8ded5206b', '55f1353a-800c-4370-930b-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5670fd13-8520-40d2-af4f-4abcc0b9c32c', '5670fc7b-a634-4224-84e1-4205c0b9c32c', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5670fdb2-ab9c-43ab-a321-496ac0b9c32c', '5670fd65-dd88-4bb1-804b-442ac0b9c32c', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5670fdb2-c8e8-4ed8-9ca2-4471c0b9c32c', '5670fd65-dd88-4bb1-804b-442ac0b9c32c', '55f1353a-800c-4370-930b-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5670fdb2-d11c-4018-94c5-491cc0b9c32c', '5670fd65-dd88-4bb1-804b-442ac0b9c32c', '55f13558-1a0c-4885-9584-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5670fdb2-d824-4e29-867e-4c5bc0b9c32c', '5670fd65-dd88-4bb1-804b-442ac0b9c32c', '55f13590-fd38-4fa0-a5ed-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5670fdb2-df2c-4577-99ad-4d13c0b9c32c', '5670fd65-dd88-4bb1-804b-442ac0b9c32c', '55f135cf-bae0-48ae-9d67-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5675bcbe-b704-45e9-abe0-4500c0b9c32c', '5675bc53-4d98-40fc-93ad-4e1fc0b9c32c', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5675bdd1-5380-4987-bf8f-48d5c0b9c32c', '5675bd7c-3534-4406-9579-46dac0b9c32c', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5675bdd1-6c1c-4083-9ab0-499fc0b9c32c', '5675bd7c-3534-4406-9579-46dac0b9c32c', '55f1353a-800c-4370-930b-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5675bdd1-7324-4fa6-b4ea-4b76c0b9c32c', '5675bd7c-3534-4406-9579-46dac0b9c32c', '55f13558-1a0c-4885-9584-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5675bdd1-79c8-4b70-97b2-49d3c0b9c32c', '5675bd7c-3534-4406-9579-46dac0b9c32c', '55f13590-fd38-4fa0-a5ed-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5675bdd1-8008-4767-b71c-4467c0b9c32c', '5675bd7c-3534-4406-9579-46dac0b9c32c', '55f135cf-bae0-48ae-9d67-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('568a6a85-274c-4ffa-8f16-42dfc0b9c32c', '568a676e-d860-49b7-915f-ecefc0b9c32c', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, '2016-01-04 06:50:13', '2016-01-04 06:50:13');
INSERT INTO `sys_resources_proceedings` VALUES ('568a6a8d-82a4-4622-b551-41f6c0b9c32c', '568a676e-d860-49b7-915f-ecefc0b9c32c', '55f1353a-800c-4370-930b-12f8c689773d', '1', null, null, '2016-01-04 06:50:21', '2016-01-04 06:50:21');
INSERT INTO `sys_resources_proceedings` VALUES ('568a6a94-a640-499e-9caf-4ad6c0b9c32c', '568a676e-d860-49b7-915f-ecefc0b9c32c', '55f13558-1a0c-4885-9584-1780c689773d', '1', null, null, '2016-01-04 06:50:28', '2016-01-04 06:50:28');
INSERT INTO `sys_resources_proceedings` VALUES ('568a6a9c-5438-4f47-8db5-477bc0b9c32c', '568a676e-d860-49b7-915f-ecefc0b9c32c', '55f13590-fd38-4fa0-a5ed-1780c689773d', '1', null, null, '2016-01-04 06:50:36', '2016-01-04 06:50:36');
INSERT INTO `sys_resources_proceedings` VALUES ('568a6aa4-78b4-4e85-8911-4d71c0b9c32c', '568a676e-d860-49b7-915f-ecefc0b9c32c', '55f135cf-bae0-48ae-9d67-12f8c689773d', '1', null, null, '2016-01-04 06:50:44', '2016-01-04 06:50:44');
INSERT INTO `sys_resources_proceedings` VALUES ('56a360b6-6a2c-4f27-ab37-0384ded5206b', '56a36074-7264-401e-96e6-06c4ded5206b', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, '2016-01-23 18:15:02', '2016-01-23 18:15:02');
INSERT INTO `sys_resources_proceedings` VALUES ('56a36157-083c-46b3-a781-06c4ded5206b', '56a36074-7264-401e-96e6-06c4ded5206b', '55f13558-1a0c-4885-9584-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('56a36157-083c-49a7-827b-06c4ded5206b', '56a36074-7264-401e-96e6-06c4ded5206b', '55f13590-fd38-4fa0-a5ed-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('56a36157-083c-4b9f-ad03-06c4ded5206b', '56a36074-7264-401e-96e6-06c4ded5206b', '55f1353a-800c-4370-930b-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('56a36157-8edc-431c-af72-06c4ded5206b', '56a36074-7264-401e-96e6-06c4ded5206b', '55f135cf-bae0-48ae-9d67-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('56b506ad-3f28-4e31-b92d-91ae671fe029', '56b50667-3094-4d83-b55d-8b00671fe029', '55f135ed-e970-47c3-9d0e-1780c689773d', '1', null, null, '2016-02-06 03:31:41', '2016-02-06 03:31:41');
INSERT INTO `sys_resources_proceedings` VALUES ('56b8393d-0b48-46f8-aa62-ca7e671fe029', '56b8392f-2e10-445a-a4f7-c9b5671fe029', '55f1353a-800c-4370-930b-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('56b8393d-1250-4d2f-b35e-ca7e671fe029', '56b8392f-2e10-445a-a4f7-c9b5671fe029', '55f13558-1a0c-4885-9584-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('56b8393d-1958-4ba2-a5c3-ca7e671fe029', '56b8392f-2e10-445a-a4f7-c9b5671fe029', '55f13590-fd38-4fa0-a5ed-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('56b8393d-1ffc-4eb3-91f5-ca7e671fe029', '56b8392f-2e10-445a-a4f7-c9b5671fe029', '55f135cf-bae0-48ae-9d67-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('56b8393d-ea14-40cd-af28-ca7e671fe029', '56b8392f-2e10-445a-a4f7-c9b5671fe029', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5f6ac4b5-e088-4ad9-bf5f-0804ded5206b', '565bf24c-6ed4-4309-bxxx-18a0c689773d', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, '2020-09-23 03:44:53', '2020-09-23 03:44:53');
INSERT INTO `sys_resources_proceedings` VALUES ('5f6ac4ba-898c-44bb-a6c3-2790ded5206b', '565bf24c-6ed4-4309-bxxx-18a0c689773d', '55f1353a-800c-4370-930b-12f8c689773d', '1', null, null, '2020-09-23 03:44:58', '2020-09-23 03:44:58');
INSERT INTO `sys_resources_proceedings` VALUES ('5f6ac4be-214c-44e6-99d7-1804ded5206b', '565bf24c-6ed4-4309-bxxx-18a0c689773d', '55f13558-1a0c-4885-9584-1780c689773d', '1', null, null, '2020-09-23 03:45:02', '2020-09-23 03:45:02');
INSERT INTO `sys_resources_proceedings` VALUES ('5f6ac4c2-f52c-4824-8afe-0804ded5206b', '565bf24c-6ed4-4309-bxxx-18a0c689773d', '55f13590-fd38-4fa0-a5ed-1780c689773d', '1', null, null, '2020-09-23 03:45:06', '2020-09-23 03:45:06');
INSERT INTO `sys_resources_proceedings` VALUES ('5f6ac4c8-b8dc-442f-abdd-2790ded5206b', '565bf24c-6ed4-4309-bxxx-18a0c689773d', '55f135cf-bae0-48ae-9d67-12f8c689773d', '1', null, null, '2020-09-23 03:45:12', '2020-09-23 03:45:12');
INSERT INTO `sys_resources_proceedings` VALUES ('5f6ac4cd-f830-40f8-a646-1804ded5206b', '565bf24c-6ed4-4309-bxxx-18a0c689773d', '55f135ed-e970-47c3-9d0e-1780c689773d', null, null, null, '2020-09-23 03:45:17', '2020-09-23 03:46:17');
INSERT INTO `sys_resources_proceedings` VALUES ('5f6b0a0e-08e0-45d5-b029-2790ded5206b', '5f6b09f4-f52c-4b3e-b485-2790ded5206b', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5f6b0a0e-1434-46c1-a8c5-2790ded5206b', '5f6b09f4-f52c-4b3e-b485-2790ded5206b', '55f1353a-800c-4370-930b-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5f6b0a0e-1948-4a14-b45d-2790ded5206b', '5f6b09f4-f52c-4b3e-b485-2790ded5206b', '55f13558-1a0c-4885-9584-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5f6b0a0e-1d94-490c-96cd-2790ded5206b', '5f6b09f4-f52c-4b3e-b485-2790ded5206b', '55f13590-fd38-4fa0-a5ed-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5f6b0a0e-21e0-4589-90b3-2790ded5206b', '5f6b09f4-f52c-4b3e-b485-2790ded5206b', '55f135cf-bae0-48ae-9d67-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5f6b120f-6168-4c0d-a098-22bcded5206b', '5f6b1174-9f98-4258-a56e-2790ded5206b', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, '2020-09-23 09:14:55', '2020-09-23 09:14:55');
INSERT INTO `sys_resources_proceedings` VALUES ('5f6b1215-94a0-493b-b6a1-22bcded5206b', '5f6b1174-9f98-4258-a56e-2790ded5206b', '55f1353a-800c-4370-930b-12f8c689773d', '1', null, null, '2020-09-23 09:15:01', '2020-09-23 09:15:01');
INSERT INTO `sys_resources_proceedings` VALUES ('5f6b121a-d930-4ee5-9036-22bcded5206b', '5f6b1174-9f98-4258-a56e-2790ded5206b', '55f13558-1a0c-4885-9584-1780c689773d', '1', null, null, '2020-09-23 09:15:06', '2020-09-23 09:15:06');
INSERT INTO `sys_resources_proceedings` VALUES ('5f6b1220-ceb0-4017-9b3b-22bcded5206b', '5f6b1174-9f98-4258-a56e-2790ded5206b', '55f13590-fd38-4fa0-a5ed-1780c689773d', '1', null, null, '2020-09-23 09:15:12', '2020-09-23 09:15:12');
INSERT INTO `sys_resources_proceedings` VALUES ('5f6b1225-bdd0-48cc-aa12-22bcded5206b', '5f6b1174-9f98-4258-a56e-2790ded5206b', '55f135cf-bae0-48ae-9d67-12f8c689773d', '1', null, null, '2020-09-23 09:15:17', '2020-09-23 09:15:17');
INSERT INTO `sys_resources_proceedings` VALUES ('5f7aefd8-3d10-4020-a95a-2944ded5206b', '5f7aeedd-e2d4-4374-a22a-2944ded5206b', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, '2020-10-05 10:05:12', '2020-10-05 10:05:12');
INSERT INTO `sys_resources_proceedings` VALUES ('5f7aefe1-6388-4a7d-88ce-1840ded5206b', '5f7aeedd-e2d4-4374-a22a-2944ded5206b', '55f1353a-800c-4370-930b-12f8c689773d', '1', null, null, '2020-10-05 10:05:21', '2020-10-05 10:05:21');
INSERT INTO `sys_resources_proceedings` VALUES ('5f7aefe6-c5e4-4064-835d-2fdcded5206b', '5f7aeedd-e2d4-4374-a22a-2944ded5206b', '55f13558-1a0c-4885-9584-1780c689773d', '1', null, null, '2020-10-05 10:05:26', '2020-10-05 10:05:26');
INSERT INTO `sys_resources_proceedings` VALUES ('5f7aefec-4c34-4e8e-b8c9-2944ded5206b', '5f7aeedd-e2d4-4374-a22a-2944ded5206b', '55f13590-fd38-4fa0-a5ed-1780c689773d', '1', null, null, '2020-10-05 10:05:32', '2020-10-05 10:05:32');
INSERT INTO `sys_resources_proceedings` VALUES ('5f7aeff1-3530-433b-8b84-1840ded5206b', '5f7aeedd-e2d4-4374-a22a-2944ded5206b', '55f135cf-bae0-48ae-9d67-12f8c689773d', '1', null, null, '2020-10-05 10:05:37', '2020-10-05 10:05:37');
INSERT INTO `sys_resources_proceedings` VALUES ('5f7af0cf-4964-4c75-bf33-2fdcded5206b', '5f7af07b-ede0-46b5-9df8-2944ded5206b', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, '2020-10-05 10:09:19', '2020-10-05 10:09:19');
INSERT INTO `sys_resources_proceedings` VALUES ('5f7af0d3-859c-4fde-b979-2944ded5206b', '5f7af07b-ede0-46b5-9df8-2944ded5206b', '55f1353a-800c-4370-930b-12f8c689773d', '1', null, null, '2020-10-05 10:09:23', '2020-10-05 10:09:23');
INSERT INTO `sys_resources_proceedings` VALUES ('5f7af0d8-0f34-429b-9268-1840ded5206b', '5f7af07b-ede0-46b5-9df8-2944ded5206b', '55f13558-1a0c-4885-9584-1780c689773d', '1', null, null, '2020-10-05 10:09:28', '2020-10-05 10:09:28');
INSERT INTO `sys_resources_proceedings` VALUES ('5f7af0dd-9c30-4659-b13a-2fdcded5206b', '5f7af07b-ede0-46b5-9df8-2944ded5206b', '55f13590-fd38-4fa0-a5ed-1780c689773d', '1', null, null, '2020-10-05 10:09:33', '2020-10-05 10:09:33');
INSERT INTO `sys_resources_proceedings` VALUES ('5f7af0e1-5428-4d94-bafc-2944ded5206b', '5f7af07b-ede0-46b5-9df8-2944ded5206b', '55f135cf-bae0-48ae-9d67-12f8c689773d', '1', null, null, '2020-10-05 10:09:37', '2020-10-05 10:09:37');
INSERT INTO `sys_resources_proceedings` VALUES ('5f7af16f-8c90-4b1c-b6c4-2fdcded5206b', '5f7af124-4dc0-43fb-84af-2944ded5206b', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, '2020-10-05 10:11:59', '2020-10-05 10:11:59');
INSERT INTO `sys_resources_proceedings` VALUES ('5f7af174-d744-446c-ada3-2944ded5206b', '5f7af124-4dc0-43fb-84af-2944ded5206b', '55f1353a-800c-4370-930b-12f8c689773d', '1', null, null, '2020-10-05 10:12:04', '2020-10-05 10:12:04');
INSERT INTO `sys_resources_proceedings` VALUES ('5f7af178-778c-4114-998b-1840ded5206b', '5f7af124-4dc0-43fb-84af-2944ded5206b', '55f13558-1a0c-4885-9584-1780c689773d', '1', null, null, '2020-10-05 10:12:08', '2020-10-05 10:12:08');
INSERT INTO `sys_resources_proceedings` VALUES ('5f7af17d-90e4-4616-bc5b-2fdcded5206b', '5f7af124-4dc0-43fb-84af-2944ded5206b', '55f13590-fd38-4fa0-a5ed-1780c689773d', '1', null, null, '2020-10-05 10:12:13', '2020-10-05 10:12:13');
INSERT INTO `sys_resources_proceedings` VALUES ('5f7af181-2dc0-41ab-a154-2944ded5206b', '5f7af124-4dc0-43fb-84af-2944ded5206b', '55f135cf-bae0-48ae-9d67-12f8c689773d', '1', null, null, '2020-10-05 10:12:17', '2020-10-05 10:12:17');

-- ----------------------------
-- Table structure for `sys_roles`
-- ----------------------------
DROP TABLE IF EXISTS `sys_roles`;
CREATE TABLE `sys_roles` (
  `id` char(36) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `active_status` tinyint(1) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `annotation` text DEFAULT NULL,
  `menu_code` varchar(50) DEFAULT NULL,
  `menu_order` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sys_roles
-- ----------------------------
INSERT INTO `sys_roles` VALUES ('55f27924-fef8-4337-bf8e-1760c689773d', 'Sysadmin', '1', 'System Administrator', 'System Administrator', 'sa', '0', '2015-09-11 13:48:04', '2015-09-11 13:48:04');
INSERT INTO `sys_roles` VALUES ('5653c30f-c7ac-4678-9eb1-160cc689773d', 'Demo Group', '1', 'Demo Group', 'Demo User Group', 'demo', null, '2015-11-24 08:53:19', '2015-11-25 11:03:28');
INSERT INTO `sys_roles` VALUES ('5661a5e8-aa48-4b99-bd92-17e0c689773d', 'Admin', '1', 'Admin', 'Admin', 'adm_demo', null, '2015-12-04 21:40:40', '2016-02-06 05:08:07');
INSERT INTO `sys_roles` VALUES ('56b5068c-cc38-43b4-b690-9016671fe029', 'Publisher', '1', 'Publisher', '', 'publisher', null, '2016-02-06 03:31:08', '2016-02-06 03:31:08');
INSERT INTO `sys_roles` VALUES ('56e7ed0a-08dc-45c2-8b6b-41c1671fe029', 'Kontak Center', '1', 'Kontak Center', 'Kontak Center', 'contact_center', null, '2016-03-15 18:07:54', '2016-03-15 18:07:54');
INSERT INTO `sys_roles` VALUES ('56e7f02e-276c-430f-960d-8bab671fe029', 'editor', '1', 'editor', 'editor', 'editor', null, '2016-03-15 18:21:18', '2016-03-15 18:21:18');
INSERT INTO `sys_roles` VALUES ('5e9d5c0c-0a70-4cdc-94a4-2b0ca747c35f', 'viewer', '1', 'View Data Registrasi Pelanggan', 'View Data Registrasi Pelanggan', 'view', null, '2020-04-20 08:23:40', '2020-04-20 08:23:40');

-- ----------------------------
-- Table structure for `sys_roles_preferences`
-- ----------------------------
DROP TABLE IF EXISTS `sys_roles_preferences`;
CREATE TABLE `sys_roles_preferences` (
  `id` char(36) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `role_id` char(36) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `active_status` tinyint(1) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `annotation` text DEFAULT NULL,
  `menu_code` varchar(50) DEFAULT NULL,
  `menu_order` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sys_roles_preferences
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_roles_proceedings`
-- ----------------------------
DROP TABLE IF EXISTS `sys_roles_proceedings`;
CREATE TABLE `sys_roles_proceedings` (
  `id` char(36) NOT NULL,
  `role_id` char(36) NOT NULL,
  `resources_proceeding_id` char(36) NOT NULL,
  `active_status` tinyint(1) DEFAULT NULL,
  `menu_code` varchar(50) DEFAULT NULL,
  `menu_order` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sys_roles_proceedings
-- ----------------------------
INSERT INTO `sys_roles_proceedings` VALUES ('08d9046c-0c78-4fbd-a5cd-9dddcfacce4b', '55f27924-fef8-4337-bf8e-1760c689773d', '5f7aefe6-c5e4-4064-835d-2fdcded5206b', '1', null, null, '2020-10-05 12:05:45', '2020-10-05 12:05:45');
INSERT INTO `sys_roles_proceedings` VALUES ('0bd198e1-edc6-4fde-ab23-22f694235ba6', '55f27924-fef8-4337-bf8e-1760c689773d', '5f7af0d8-0f34-429b-9268-1840ded5206b', '1', null, null, '2020-10-05 12:10:00', '2020-10-05 12:10:00');
INSERT INTO `sys_roles_proceedings` VALUES ('1e72c434-09ce-438b-b230-c10d230fa01f', '55f27924-fef8-4337-bf8e-1760c689773d', '5f6ac4ba-898c-44bb-a6c3-2790ded5206b', '1', null, null, '2020-09-23 04:32:40', '2020-09-23 04:32:40');
INSERT INTO `sys_roles_proceedings` VALUES ('201b25aa-fede-4fcb-98a4-48105ff5bf88', '55f27924-fef8-4337-bf8e-1760c689773d', '5f6ac4be-214c-44e6-99d7-1804ded5206b', '1', null, null, '2020-09-23 04:32:40', '2020-09-23 04:32:40');
INSERT INTO `sys_roles_proceedings` VALUES ('29bda048-c85c-46de-9624-6d01db3bcb31', '55f27924-fef8-4337-bf8e-1760c689773d', '5f7af0dd-9c30-4659-b13a-2fdcded5206b', '1', null, null, '2020-10-05 12:10:00', '2020-10-05 12:10:00');
INSERT INTO `sys_roles_proceedings` VALUES ('2d2113ae-8feb-480b-b261-e033927ec872', '55f27924-fef8-4337-bf8e-1760c689773d', '5f6b0a0e-1d94-490c-96cd-2790ded5206b', '1', null, null, '2020-09-23 08:44:46', '2020-09-23 08:44:46');
INSERT INTO `sys_roles_proceedings` VALUES ('2d3b5530-9540-4c4c-a7a2-cc6d18d7890f', '55f27924-fef8-4337-bf8e-1760c689773d', '5f7aefd8-3d10-4020-a95a-2944ded5206b', '1', null, null, '2020-10-05 12:05:45', '2020-10-05 12:05:45');
INSERT INTO `sys_roles_proceedings` VALUES ('2da6ae2d-ef76-4cd4-a44e-93f321c252bc', '55f27924-fef8-4337-bf8e-1760c689773d', '5f7af0cf-4964-4c75-bf33-2fdcded5206b', '1', null, null, '2020-10-05 12:10:00', '2020-10-05 12:10:00');
INSERT INTO `sys_roles_proceedings` VALUES ('320ab5db-ab88-42b7-9e5e-7e14f7a87f92', '55f27924-fef8-4337-bf8e-1760c689773d', '5f6ac4c8-b8dc-442f-abdd-2790ded5206b', '1', null, null, '2020-09-23 04:32:40', '2020-09-23 04:32:40');
INSERT INTO `sys_roles_proceedings` VALUES ('3a2f9cff-4022-4b12-a098-7a6e756cfb5a', '55f27924-fef8-4337-bf8e-1760c689773d', '5f6ac4cd-f830-40f8-a646-1804ded5206b', '1', null, null, '2020-09-23 04:32:40', '2020-09-23 04:32:40');
INSERT INTO `sys_roles_proceedings` VALUES ('558a6498-a9d0-443c-ba67-b5f8cb2d14a4', '55f27924-fef8-4337-bf8e-1760c689773d', '5f6b1225-bdd0-48cc-aa12-22bcded5206b', '1', null, null, '2020-09-23 09:17:24', '2020-09-23 09:17:24');
INSERT INTO `sys_roles_proceedings` VALUES ('55f99c1a-43b8-43e4-848c-0f88c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f1c6b6-c304-47cc-8116-1770c689773d', '1', '', null, '2015-09-16 23:43:06', '2015-09-16 23:43:06');
INSERT INTO `sys_roles_proceedings` VALUES ('55fad9e4-0aac-49cd-965d-1438c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2226b-4af8-4962-9a73-1710c689773d', '1', '', null, '2015-09-17 22:19:00', '2015-09-17 22:19:00');
INSERT INTO `sys_roles_proceedings` VALUES ('55fbc346-b3d8-4d03-a1f4-106cc689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f23411-2e98-4b73-ab8c-1710c689773d', '1', '', null, '2015-09-18 14:54:46', '2015-09-18 14:54:46');
INSERT INTO `sys_roles_proceedings` VALUES ('55fcda5f-9fc8-48b5-84ed-0108c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f1ca15-a1e8-44e3-bde6-177cc689773d', '1', '', null, '2015-09-19 10:45:35', '2015-09-19 10:45:35');
INSERT INTO `sys_roles_proceedings` VALUES ('55fce104-1a48-4ab6-ab84-0108c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2f73d-deb0-46d0-9b3a-051cc689773d', '1', '', null, '2015-09-19 11:13:56', '2015-09-19 11:13:56');
INSERT INTO `sys_roles_proceedings` VALUES ('55fce12b-95e8-4f77-8722-05e0c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f23ffc-99c8-4ceb-9f25-14a0c689773d', '1', '', null, '2015-09-19 11:14:35', '2015-09-19 11:14:35');
INSERT INTO `sys_roles_proceedings` VALUES ('55fce17d-b6b0-4943-afc7-03c0c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2226b-6bc8-4ccc-96af-1710c689773d', '1', '', null, '2015-09-19 11:15:57', '2015-09-19 11:15:57');
INSERT INTO `sys_roles_proceedings` VALUES ('55fcf63e-39cc-47b0-bfa8-05e0c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2226b-84c8-4c5d-8a67-1710c689773d', '1', '', null, '2015-09-19 12:44:30', '2015-09-19 12:44:30');
INSERT INTO `sys_roles_proceedings` VALUES ('55fd1a5f-7464-4a3c-9f07-03c0c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2226b-9bd4-404f-ae3b-1710c689773d', '1', '', null, '2015-09-19 15:18:39', '2015-09-19 15:18:39');
INSERT INTO `sys_roles_proceedings` VALUES ('55fda9de-3294-4c38-b08e-1e58c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55fda9c3-07c8-4afe-b95d-18c0c689773d', '1', '', null, '2015-09-20 01:30:54', '2015-09-20 01:30:54');
INSERT INTO `sys_roles_proceedings` VALUES ('5616233e-2de4-4474-8fa7-16f8c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '56161cca-5080-436c-958c-04ccc689773d', '1', '', null, '2015-10-08 15:03:10', '2015-10-08 15:03:10');
INSERT INTO `sys_roles_proceedings` VALUES ('5624ffaf-696c-4763-ac8d-1480c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f23411-5288-4b51-97a8-1710c689773d', '1', '', null, '2015-10-19 21:35:27', '2015-10-19 21:35:27');
INSERT INTO `sys_roles_proceedings` VALUES ('5624ffc6-c580-46de-9a72-174cc689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f23411-6c50-44c1-855a-1710c689773d', '1', '', null, '2015-10-19 21:35:50', '2015-10-19 21:35:50');
INSERT INTO `sys_roles_proceedings` VALUES ('5624ffcd-49e0-4f83-a21b-1480c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f23411-8424-4677-a821-1710c689773d', '1', '', null, '2015-10-19 21:35:57', '2015-10-19 21:35:57');
INSERT INTO `sys_roles_proceedings` VALUES ('5624fffa-29dc-4c52-a05b-1480c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '5624ffee-39b4-48e4-9039-174cc689773d', '1', '', null, '2015-10-19 21:36:42', '2015-10-19 21:36:42');
INSERT INTO `sys_roles_proceedings` VALUES ('5629cd84-42ac-47b5-b65f-1ac8c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2e9f0-66ec-4b3e-929c-1a40c689773d', '1', '', null, '2015-10-23 13:02:44', '2015-10-23 13:02:44');
INSERT INTO `sys_roles_proceedings` VALUES ('5629cd8f-dca0-412b-adc4-1bbcc689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2e9f0-894c-4eef-88c3-1a40c689773d', '1', '', null, '2015-10-23 13:02:55', '2015-10-23 13:02:55');
INSERT INTO `sys_roles_proceedings` VALUES ('5629cd98-e42c-4828-a2f5-1ac8c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2e9f0-a184-4208-a606-1a40c689773d', '1', '', null, '2015-10-23 13:03:04', '2015-10-23 13:03:04');
INSERT INTO `sys_roles_proceedings` VALUES ('5629cda5-49d0-4e48-8894-1bbcc689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2e9f0-b8f4-4792-a509-1a40c689773d', '1', '', null, '2015-10-23 13:03:17', '2015-10-23 13:03:17');
INSERT INTO `sys_roles_proceedings` VALUES ('5629cdb0-4660-4abf-a30b-1ac8c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2e9f0-d064-4f79-999b-1a40c689773d', '1', '', null, '2015-10-23 13:03:28', '2015-10-23 13:03:28');
INSERT INTO `sys_roles_proceedings` VALUES ('5629ce06-2480-46c6-af44-0f28c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '5629cbf0-da1c-41a9-bccc-1ac8c689773d', '1', '', null, '2015-10-23 13:04:54', '2015-10-23 13:04:54');
INSERT INTO `sys_roles_proceedings` VALUES ('5629ce15-6a88-4454-a2aa-08f0c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '5629cbf0-fe0c-4241-9d88-1ac8c689773d', '1', '', null, '2015-10-23 13:05:09', '2015-10-23 13:05:09');
INSERT INTO `sys_roles_proceedings` VALUES ('5629ce26-8704-49d4-a585-0f28c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '5629cbf0-1838-43b5-b372-1ac8c689773d', '1', '', null, '2015-10-23 13:05:26', '2015-10-23 13:05:26');
INSERT INTO `sys_roles_proceedings` VALUES ('5629ce30-9530-4528-b4ca-08f0c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '5629cbf0-30d4-4a7e-8ab5-1ac8c689773d', '1', '', null, '2015-10-23 13:05:36', '2015-10-23 13:05:36');
INSERT INTO `sys_roles_proceedings` VALUES ('5629ce3a-7c60-47ed-b920-0f28c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '5629cbf0-48a8-427f-8ffc-1ac8c689773d', '1', '', null, '2015-10-23 13:05:46', '2015-10-23 13:05:46');
INSERT INTO `sys_roles_proceedings` VALUES ('56552b9e-abb4-432b-8866-1afcc689773d', '5653c30f-c7ac-4678-9eb1-160cc689773d', '55f1c6b6-c304-47cc-8116-1770c689773d', null, null, null, '2015-11-25 10:31:42', '2015-11-25 10:36:19');
INSERT INTO `sys_roles_proceedings` VALUES ('5655314c-f4f4-44b8-b958-0ffcc689773d', '5653c30f-c7ac-4678-9eb1-160cc689773d', '55f2e9f0-66ec-4b3e-929c-1a40c689773d', '1', null, null, '2015-11-25 10:55:56', '2015-11-25 10:56:55');
INSERT INTO `sys_roles_proceedings` VALUES ('565807ed-3e10-418a-901c-1b18c689773d', '5653c30f-c7ac-4678-9eb1-160cc689773d', '55f2e9f0-894c-4eef-88c3-1a40c689773d', '1', null, null, '2015-11-27 14:36:13', '2015-11-27 14:36:13');
INSERT INTO `sys_roles_proceedings` VALUES ('56580824-34b4-4469-be36-15e8c689773d', '5653c30f-c7ac-4678-9eb1-160cc689773d', '55f2e9f0-a184-4208-a606-1a40c689773d', '1', null, null, '2015-11-27 14:37:08', '2015-11-27 14:37:08');
INSERT INTO `sys_roles_proceedings` VALUES ('565bf0b4-ab5c-4e47-b46a-12dcc689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2fafc-a31c-4898-92ad-1234c689773d', '1', null, null, '2015-11-30 13:46:12', '2015-11-30 13:46:12');
INSERT INTO `sys_roles_proceedings` VALUES ('565bf0c4-2de0-4297-81df-056cc689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2fafc-c388-4b93-88f8-1234c689773d', '1', null, null, '2015-11-30 13:46:28', '2015-11-30 13:46:28');
INSERT INTO `sys_roles_proceedings` VALUES ('565bf0c9-af0c-4320-a36d-18a0c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2fafc-dcec-45cc-bc18-1234c689773d', '1', null, null, '2015-11-30 13:46:33', '2015-11-30 13:46:33');
INSERT INTO `sys_roles_proceedings` VALUES ('565bf0ce-3578-496d-96f6-14b4c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2fafc-f524-46cc-bb2a-1234c689773d', '1', null, null, '2015-11-30 13:46:38', '2015-11-30 13:46:38');
INSERT INTO `sys_roles_proceedings` VALUES ('565bf0d4-056c-405b-8b0a-1b24c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2fafc-0c30-4331-9be0-1234c689773d', '1', null, null, '2015-11-30 13:46:44', '2015-11-30 13:46:44');
INSERT INTO `sys_roles_proceedings` VALUES ('565bf0d8-39ec-4f90-a15a-0ee4c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2f73d-4a80-450b-aaa3-051cc689773d', '1', null, null, '2015-11-30 13:46:48', '2015-11-30 13:46:48');
INSERT INTO `sys_roles_proceedings` VALUES ('565bf0dd-66bc-4dfb-8a87-0d80c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2f73d-32ac-4eda-b56e-051cc689773d', '1', null, null, '2015-11-30 13:46:53', '2015-11-30 13:46:53');
INSERT INTO `sys_roles_proceedings` VALUES ('565bf0e1-5d2c-4e2b-ac97-12dcc689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2f73d-1b3c-4db1-8cd8-051cc689773d', '1', null, null, '2015-11-30 13:46:57', '2015-11-30 13:46:57');
INSERT INTO `sys_roles_proceedings` VALUES ('565bf0e4-af88-464c-b93b-056cc689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2f73d-01d8-4512-ac89-051cc689773d', '1', null, null, '2015-11-30 13:47:00', '2015-11-30 13:47:00');
INSERT INTO `sys_roles_proceedings` VALUES ('565bf107-f2c8-4e05-a296-0ee4c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '565bf0f9-9a90-463f-ae98-14b4c689773d', '1', null, null, '2015-11-30 13:47:35', '2015-11-30 13:47:35');
INSERT INTO `sys_roles_proceedings` VALUES ('565bf325-d1ec-444f-b743-18a0c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '565bf303-8528-4bcc-be48-14b4c689773d', '1', null, null, '2015-11-30 13:56:37', '2015-11-30 13:56:37');
INSERT INTO `sys_roles_proceedings` VALUES ('565bf32a-3d84-4134-b968-14b4c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '565bf308-f5f0-474a-b25f-1b24c689773d', '1', null, null, '2015-11-30 13:56:42', '2015-11-30 13:56:42');
INSERT INTO `sys_roles_proceedings` VALUES ('565bf32f-7f24-432b-8767-1b24c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '565bf30e-1e0c-457d-91d7-0ee4c689773d', '1', null, null, '2015-11-30 13:56:47', '2015-11-30 13:56:47');
INSERT INTO `sys_roles_proceedings` VALUES ('565bf334-6e3c-4cd6-aca4-0ee4c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '565bf313-9c88-4602-9687-0d80c689773d', '1', null, null, '2015-11-30 13:56:52', '2015-11-30 13:56:52');
INSERT INTO `sys_roles_proceedings` VALUES ('565bf339-73b8-44c1-a57a-0d80c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '565bf318-555c-433d-a4ef-12dcc689773d', '1', null, null, '2015-11-30 13:56:57', '2015-11-30 13:56:57');
INSERT INTO `sys_roles_proceedings` VALUES ('565c0239-b38c-4272-84f4-0ee4c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '565c0210-534c-4fc3-b712-0d80c689773d', '1', null, null, '2015-11-30 15:00:57', '2015-11-30 15:00:57');
INSERT INTO `sys_roles_proceedings` VALUES ('565c023e-d9d8-4369-bf8d-0d80c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '565c0217-d5e4-4f0c-9e9c-12dcc689773d', '1', null, null, '2015-11-30 15:01:02', '2015-11-30 15:01:02');
INSERT INTO `sys_roles_proceedings` VALUES ('565c0243-94e4-47dd-a14b-12dcc689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '565c021d-e730-4dcf-afdf-056cc689773d', '1', null, null, '2015-11-30 15:01:07', '2015-11-30 15:01:07');
INSERT INTO `sys_roles_proceedings` VALUES ('565c0248-bc64-4259-9d3e-056cc689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '565c0223-ae5c-4297-a849-18a0c689773d', '1', null, null, '2015-11-30 15:01:12', '2015-11-30 15:01:12');
INSERT INTO `sys_roles_proceedings` VALUES ('565c024d-e854-46eb-bac3-18a0c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '565c0229-f414-44f9-98ac-14b4c689773d', '1', null, null, '2015-11-30 15:01:17', '2015-11-30 15:01:17');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a5fa-5044-437d-8ebe-17e0c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '55f1c6b6-c304-47cc-8116-1770c689773d', '1', null, null, '2015-12-04 21:40:58', '2015-12-04 21:40:58');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a5fe-2574-47c9-bcf6-111cc689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '55f1ca15-a1e8-44e3-bde6-177cc689773d', '1', null, null, '2015-12-04 21:41:02', '2015-12-04 21:41:02');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a602-fc50-4d69-9aa8-06d8c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '55f2226b-4af8-4962-9a73-1710c689773d', '1', null, null, '2015-12-04 21:41:06', '2015-12-04 21:41:06');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a605-6134-4b3b-8b8d-17e0c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '55f2226b-6bc8-4ccc-96af-1710c689773d', '1', null, null, '2015-12-04 21:41:09', '2015-12-04 21:41:09');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a609-cda4-41b3-921e-111cc689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '55f2226b-84c8-4c5d-8a67-1710c689773d', '1', null, null, '2015-12-04 21:41:13', '2015-12-04 21:41:13');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a60c-d338-41fa-a55f-06d8c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '55f2226b-9bd4-404f-ae3b-1710c689773d', '1', null, null, '2015-12-04 21:41:16', '2015-12-04 21:41:16');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a612-feb0-49b0-a5c9-17e0c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '55fda9c3-07c8-4afe-b95d-18c0c689773d', '1', null, null, '2015-12-04 21:41:22', '2015-12-04 21:41:22');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a617-cde0-4cb0-8eb5-111cc689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '55f23411-2e98-4b73-ab8c-1710c689773d', '1', null, null, '2015-12-04 21:41:27', '2015-12-04 21:41:27');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a61b-45ec-4438-bf9b-06d8c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '55f23411-5288-4b51-97a8-1710c689773d', '1', null, null, '2015-12-04 21:41:31', '2015-12-04 21:41:31');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a61e-c820-4ee5-821f-17e0c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '55f23411-6c50-44c1-855a-1710c689773d', '1', null, null, '2015-12-04 21:41:34', '2015-12-04 21:41:34');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a622-39d0-4e47-8135-111cc689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '55f23411-8424-4677-a821-1710c689773d', '1', null, null, '2015-12-04 21:41:38', '2015-12-04 21:41:38');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a625-143c-4177-ae72-06d8c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '5624ffee-39b4-48e4-9039-174cc689773d', '1', null, null, '2015-12-04 21:41:41', '2015-12-04 21:41:41');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a62e-d6d4-4fd7-a213-17e0c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '56161cca-5080-436c-958c-04ccc689773d', '1', null, null, '2015-12-04 21:41:50', '2015-12-04 21:41:50');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a635-c0f4-410d-8c01-111cc689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '5629cbf0-da1c-41a9-bccc-1ac8c689773d', '1', null, null, '2015-12-04 21:41:57', '2015-12-04 21:41:57');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a638-887c-4351-b196-06d8c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '5629cbf0-fe0c-4241-9d88-1ac8c689773d', '1', null, null, '2015-12-04 21:42:00', '2015-12-04 21:42:00');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a63c-c5a4-4b72-8e3e-17e0c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '5629cbf0-1838-43b5-b372-1ac8c689773d', '1', null, null, '2015-12-04 21:42:04', '2015-12-04 21:42:04');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a640-f890-4fb3-ae6a-111cc689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '5629cbf0-30d4-4a7e-8ab5-1ac8c689773d', '1', null, null, '2015-12-04 21:42:08', '2015-12-04 21:42:08');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a644-0ad8-4af5-99fe-06d8c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '5629cbf0-48a8-427f-8ffc-1ac8c689773d', '1', null, null, '2015-12-04 21:42:12', '2015-12-04 21:42:12');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a64c-5a14-40c2-add5-17e0c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '565bf0f9-9a90-463f-ae98-14b4c689773d', '1', null, null, '2015-12-04 21:42:20', '2015-12-06 00:49:40');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a652-8444-443f-8232-111cc689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '565bf303-8528-4bcc-be48-14b4c689773d', '1', null, null, '2015-12-04 21:42:26', '2015-12-04 21:42:26');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a657-c154-490d-8653-06d8c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '565bf308-f5f0-474a-b25f-1b24c689773d', '1', null, null, '2015-12-04 21:42:31', '2015-12-04 21:42:31');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a65c-1230-4e0f-b2ca-17e0c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '565bf30e-1e0c-457d-91d7-0ee4c689773d', '1', null, null, '2015-12-04 21:42:36', '2015-12-04 21:42:36');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a661-a714-4cd9-bf76-111cc689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '565bf313-9c88-4602-9687-0d80c689773d', '1', null, null, '2015-12-04 21:42:41', '2015-12-04 21:42:41');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a666-2494-40ed-85ed-06d8c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '565bf318-555c-433d-a4ef-12dcc689773d', '1', null, null, '2015-12-04 21:42:46', '2015-12-04 21:42:46');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a66b-fc9c-4c6e-961e-17e0c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '565c0210-534c-4fc3-b712-0d80c689773d', '1', null, null, '2015-12-04 21:42:51', '2015-12-04 21:42:51');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a670-3d84-41ec-8b9b-111cc689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '565c0217-d5e4-4f0c-9e9c-12dcc689773d', '1', null, null, '2015-12-04 21:42:56', '2015-12-04 21:42:56');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a676-ba84-40c5-846e-06d8c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '565c021d-e730-4dcf-afdf-056cc689773d', '1', null, null, '2015-12-04 21:43:02', '2015-12-04 21:43:02');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a67b-f0a4-4220-a1a6-17e0c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '565c0223-ae5c-4297-a849-18a0c689773d', '1', null, null, '2015-12-04 21:43:07', '2015-12-04 21:43:07');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a67f-a93c-44af-85fb-111cc689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '565c0229-f414-44f9-98ac-14b4c689773d', '1', null, null, '2015-12-04 21:43:11', '2015-12-04 21:43:11');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a684-09b4-4ea7-b029-06d8c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '5661a59a-2f08-4e7a-8372-17e0c689773d', '1', null, null, '2015-12-04 21:43:16', '2015-12-04 21:43:16');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a688-3380-435a-add6-17e0c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '5661a59a-6cc0-4989-b814-17e0c689773d', '1', null, null, '2015-12-04 21:43:20', '2015-12-04 21:43:20');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a68d-f1a4-40a0-b39a-111cc689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '5661a59a-9948-4912-aff8-17e0c689773d', '1', null, null, '2015-12-04 21:43:25', '2015-12-04 21:43:25');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a691-803c-45fe-bc16-06d8c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '5661a59a-c508-4520-8044-17e0c689773d', '1', null, null, '2015-12-04 21:43:29', '2015-12-04 21:43:29');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a696-8614-460a-b531-17e0c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '5661a59a-ef9c-42de-a8f1-17e0c689773d', '1', null, null, '2015-12-04 21:43:34', '2015-12-04 21:43:34');
INSERT INTO `sys_roles_proceedings` VALUES ('5661b4ac-b788-4468-9782-17e0ded5206b', '55f27924-fef8-4337-bf8e-1760c689773d', '5661a59a-2f08-4e7a-8372-17e0c689773d', '1', null, null, '2015-12-04 22:43:40', '2015-12-04 22:43:40');
INSERT INTO `sys_roles_proceedings` VALUES ('5661b4b1-e524-49c6-aa7f-111cded5206b', '55f27924-fef8-4337-bf8e-1760c689773d', '5661a59a-6cc0-4989-b814-17e0c689773d', '1', null, null, '2015-12-04 22:43:45', '2015-12-04 22:43:45');
INSERT INTO `sys_roles_proceedings` VALUES ('5661b4b5-8d9c-4967-ba04-06d8ded5206b', '55f27924-fef8-4337-bf8e-1760c689773d', '5661a59a-9948-4912-aff8-17e0c689773d', '1', null, null, '2015-12-04 22:43:49', '2015-12-04 22:43:49');
INSERT INTO `sys_roles_proceedings` VALUES ('5661b4bb-af58-4431-af45-17e0ded5206b', '55f27924-fef8-4337-bf8e-1760c689773d', '5661a59a-c508-4520-8044-17e0c689773d', '1', null, null, '2015-12-04 22:43:55', '2015-12-04 22:43:55');
INSERT INTO `sys_roles_proceedings` VALUES ('5661b4c0-6078-45f2-b223-111cded5206b', '55f27924-fef8-4337-bf8e-1760c689773d', '5661a59a-ef9c-42de-a8f1-17e0c689773d', '1', null, null, '2015-12-04 22:44:00', '2015-12-05 03:01:29');
INSERT INTO `sys_roles_proceedings` VALUES ('566b826d-8c20-4747-ab2c-08b8ded5206b', '55f27924-fef8-4337-bf8e-1760c689773d', '566b81d5-c9b0-46ce-aef0-0534ded5206b', '1', null, null, '2015-12-12 09:11:57', '2015-12-12 09:11:57');
INSERT INTO `sys_roles_proceedings` VALUES ('566b8272-eb48-4046-82ff-0534ded5206b', '55f27924-fef8-4337-bf8e-1760c689773d', '566b81d5-ecd8-4c0b-a6d3-0534ded5206b', '1', null, null, '2015-12-12 09:12:02', '2015-12-12 09:12:02');
INSERT INTO `sys_roles_proceedings` VALUES ('566b8279-0058-41a8-aadb-15b8ded5206b', '55f27924-fef8-4337-bf8e-1760c689773d', '566b81d5-0574-4673-af04-0534ded5206b', '1', null, null, '2015-12-12 09:12:09', '2015-12-12 09:12:09');
INSERT INTO `sys_roles_proceedings` VALUES ('566b827f-8864-4f7d-a25d-08b8ded5206b', '55f27924-fef8-4337-bf8e-1760c689773d', '566b81d5-1d48-4a77-8df8-0534ded5206b', '1', null, null, '2015-12-12 09:12:15', '2015-12-12 09:12:15');
INSERT INTO `sys_roles_proceedings` VALUES ('566b8285-c584-42e1-80c2-0534ded5206b', '55f27924-fef8-4337-bf8e-1760c689773d', '566b81d5-33f0-45ab-b00c-0534ded5206b', '1', null, null, '2015-12-12 09:12:21', '2015-12-12 09:12:21');
INSERT INTO `sys_roles_proceedings` VALUES ('566b82a3-f034-46c4-843e-08b8ded5206b', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '566b81d5-c9b0-46ce-aef0-0534ded5206b', '1', null, null, '2015-12-12 09:12:51', '2015-12-12 09:12:51');
INSERT INTO `sys_roles_proceedings` VALUES ('566b82aa-5b84-49df-af07-0534ded5206b', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '566b81d5-ecd8-4c0b-a6d3-0534ded5206b', '1', null, null, '2015-12-12 09:12:58', '2015-12-12 09:12:58');
INSERT INTO `sys_roles_proceedings` VALUES ('566b82af-0270-4043-8c14-15b8ded5206b', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '566b81d5-0574-4673-af04-0534ded5206b', '1', null, null, '2015-12-12 09:13:03', '2015-12-12 09:13:03');
INSERT INTO `sys_roles_proceedings` VALUES ('566b82b5-f044-4ddc-8163-08b8ded5206b', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '566b81d5-1d48-4a77-8df8-0534ded5206b', '1', null, null, '2015-12-12 09:13:09', '2015-12-12 09:13:09');
INSERT INTO `sys_roles_proceedings` VALUES ('566b82ba-4b68-450d-a053-0534ded5206b', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '566b81d5-33f0-45ab-b00c-0534ded5206b', '1', null, null, '2015-12-12 09:13:14', '2015-12-12 09:13:14');
INSERT INTO `sys_roles_proceedings` VALUES ('5670fd2a-78a0-4d48-8b49-4d12c0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '5670fd13-8520-40d2-af4f-4abcc0b9c32c', '1', null, null, '2015-12-15 23:56:58', '2015-12-15 23:56:58');
INSERT INTO `sys_roles_proceedings` VALUES ('5670fdcb-0410-42b0-b35b-4360c0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '5670fdb2-ab9c-43ab-a321-496ac0b9c32c', '1', null, null, '2015-12-15 23:59:39', '2015-12-15 23:59:39');
INSERT INTO `sys_roles_proceedings` VALUES ('5670fdd2-a074-4fb6-ab7e-4825c0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '5670fdb2-c8e8-4ed8-9ca2-4471c0b9c32c', '1', null, null, '2015-12-15 23:59:46', '2015-12-15 23:59:46');
INSERT INTO `sys_roles_proceedings` VALUES ('5670fddc-5d0c-4260-9f2f-4830c0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '5670fdb2-d11c-4018-94c5-491cc0b9c32c', '1', null, null, '2015-12-15 23:59:56', '2015-12-15 23:59:56');
INSERT INTO `sys_roles_proceedings` VALUES ('5670fde4-e61c-4cd7-a20c-44ecc0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '5670fdb2-d824-4e29-867e-4c5bc0b9c32c', '1', null, null, '2015-12-16 00:00:04', '2015-12-16 00:00:19');
INSERT INTO `sys_roles_proceedings` VALUES ('5670fe03-91e4-4997-bdf1-49dbc0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '5670fdb2-df2c-4577-99ad-4d13c0b9c32c', '1', null, null, '2015-12-16 00:00:35', '2015-12-16 00:00:35');
INSERT INTO `sys_roles_proceedings` VALUES ('5675bccb-af00-4f4c-9972-494cc0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '5675bcbe-b704-45e9-abe0-4500c0b9c32c', null, null, null, '2015-12-19 14:23:39', '2016-02-08 13:54:47');
INSERT INTO `sys_roles_proceedings` VALUES ('5675bdde-44c0-4085-b830-4eacc0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '5675bdd1-5380-4987-bf8f-48d5c0b9c32c', '1', null, null, '2015-12-19 14:28:14', '2015-12-19 14:28:14');
INSERT INTO `sys_roles_proceedings` VALUES ('5675bde3-c14c-4070-9f44-42d7c0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '5675bdd1-6c1c-4083-9ab0-499fc0b9c32c', '1', null, null, '2015-12-19 14:28:19', '2015-12-19 14:28:19');
INSERT INTO `sys_roles_proceedings` VALUES ('5675bde9-64f8-4ae4-924a-4b53c0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '5675bdd1-7324-4fa6-b4ea-4b76c0b9c32c', '1', null, null, '2015-12-19 14:28:25', '2015-12-19 14:28:25');
INSERT INTO `sys_roles_proceedings` VALUES ('5675bdee-c934-4fb9-83b3-46d7c0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '5675bdd1-79c8-4b70-97b2-49d3c0b9c32c', '1', null, null, '2015-12-19 14:28:30', '2015-12-19 14:28:30');
INSERT INTO `sys_roles_proceedings` VALUES ('5675bdf3-a260-4dff-ab94-4113c0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '5675bdd1-8008-4767-b71c-4467c0b9c32c', '1', null, null, '2015-12-19 14:28:35', '2015-12-19 14:28:35');
INSERT INTO `sys_roles_proceedings` VALUES ('568a681f-ccf0-465d-817d-ff02c0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '568a6809-81b4-4d60-bdfa-fce9c0b9c32c', '1', null, null, '2016-01-04 06:39:59', '2016-01-04 06:39:59');
INSERT INTO `sys_roles_proceedings` VALUES ('568a6825-1cec-4c97-96c7-481cc0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '568a6809-9e9c-47b4-a2dd-fce9c0b9c32c', '1', null, null, '2016-01-04 06:40:05', '2016-01-04 06:40:05');
INSERT INTO `sys_roles_proceedings` VALUES ('568a6846-84f8-4685-a43a-4672c0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '568a6809-a5a4-4050-97ef-fce9c0b9c32c', '1', null, null, '2016-01-04 06:40:38', '2016-01-04 06:40:38');
INSERT INTO `sys_roles_proceedings` VALUES ('568a684d-b1fc-4043-b51b-4915c0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '568a6809-abe4-4ee0-998b-fce9c0b9c32c', '1', null, null, '2016-01-04 06:40:45', '2016-01-04 06:40:45');
INSERT INTO `sys_roles_proceedings` VALUES ('568a6854-2b9c-4305-971d-42acc0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '568a6809-b224-4d48-b627-fce9c0b9c32c', '1', null, null, '2016-01-04 06:40:52', '2016-01-04 06:40:52');
INSERT INTO `sys_roles_proceedings` VALUES ('568a6b9c-81e0-4a4f-ab44-4c0fc0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '568a6a85-274c-4ffa-8f16-42dfc0b9c32c', '1', null, null, '2016-01-04 06:54:52', '2016-01-04 06:54:52');
INSERT INTO `sys_roles_proceedings` VALUES ('568a6ba3-1fb4-49e1-b681-419dc0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '568a6a8d-82a4-4622-b551-41f6c0b9c32c', '1', null, null, '2016-01-04 06:54:59', '2016-01-04 06:54:59');
INSERT INTO `sys_roles_proceedings` VALUES ('568a6bab-1ef4-4976-a7c9-4726c0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '568a6a94-a640-499e-9caf-4ad6c0b9c32c', '1', null, null, '2016-01-04 06:55:07', '2016-01-04 06:55:07');
INSERT INTO `sys_roles_proceedings` VALUES ('568a6bb2-006c-40ad-8cbe-46fdc0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '568a6a9c-5438-4f47-8db5-477bc0b9c32c', '1', null, null, '2016-01-04 06:55:14', '2016-01-04 06:55:14');
INSERT INTO `sys_roles_proceedings` VALUES ('568a6bb9-2114-4e82-a436-491fc0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '568a6aa4-78b4-4e85-8911-4d71c0b9c32c', '1', null, null, '2016-01-04 06:55:21', '2016-01-04 06:55:21');
INSERT INTO `sys_roles_proceedings` VALUES ('56a3617d-0df8-4ece-8918-12c8ded5206b', '55f27924-fef8-4337-bf8e-1760c689773d', '56a360b6-6a2c-4f27-ab37-0384ded5206b', '1', null, null, '2016-01-23 18:18:21', '2016-01-23 18:18:21');
INSERT INTO `sys_roles_proceedings` VALUES ('56a36184-7260-4161-818c-0fe0ded5206b', '55f27924-fef8-4337-bf8e-1760c689773d', '56a36157-083c-4b9f-ad03-06c4ded5206b', '1', null, null, '2016-01-23 18:18:28', '2016-01-23 18:18:28');
INSERT INTO `sys_roles_proceedings` VALUES ('56a3618b-50a4-4955-99c7-14f0ded5206b', '55f27924-fef8-4337-bf8e-1760c689773d', '56a36157-083c-46b3-a781-06c4ded5206b', '1', null, null, '2016-01-23 18:18:35', '2016-01-23 18:18:35');
INSERT INTO `sys_roles_proceedings` VALUES ('56a36192-947c-45d5-bd60-1584ded5206b', '55f27924-fef8-4337-bf8e-1760c689773d', '56a36157-083c-49a7-827b-06c4ded5206b', '1', null, null, '2016-01-23 18:18:42', '2016-01-23 18:18:42');
INSERT INTO `sys_roles_proceedings` VALUES ('56a36198-49c8-4e9b-bf0c-0384ded5206b', '55f27924-fef8-4337-bf8e-1760c689773d', '56a36157-8edc-431c-af72-06c4ded5206b', '1', null, null, '2016-01-23 18:18:48', '2016-01-23 18:18:48');
INSERT INTO `sys_roles_proceedings` VALUES ('56b506d9-bce0-405a-a8cf-93e6671fe029', '56b5068c-cc38-43b4-b690-9016671fe029', '56b506ad-3f28-4e31-b92d-91ae671fe029', '1', null, null, '2016-02-06 03:32:25', '2016-02-06 03:32:25');
INSERT INTO `sys_roles_proceedings` VALUES ('56b506ee-18a8-449f-b5c0-98a5671fe029', '56b5068c-cc38-43b4-b690-9016671fe029', '565bf303-8528-4bcc-be48-14b4c689773d', '1', null, null, '2016-02-06 03:32:46', '2016-02-06 03:32:46');
INSERT INTO `sys_roles_proceedings` VALUES ('56b506f6-0740-4b90-99d1-98c4671fe029', '56b5068c-cc38-43b4-b690-9016671fe029', '565bf308-f5f0-474a-b25f-1b24c689773d', '1', null, null, '2016-02-06 03:32:54', '2016-02-06 03:32:54');
INSERT INTO `sys_roles_proceedings` VALUES ('56b506fc-f82c-42f5-b279-98e7671fe029', '56b5068c-cc38-43b4-b690-9016671fe029', '565bf30e-1e0c-457d-91d7-0ee4c689773d', '1', null, null, '2016-02-06 03:33:00', '2016-02-06 03:33:00');
INSERT INTO `sys_roles_proceedings` VALUES ('56b50703-6870-4164-8942-9904671fe029', '56b5068c-cc38-43b4-b690-9016671fe029', '565bf313-9c88-4602-9687-0d80c689773d', '1', null, null, '2016-02-06 03:33:07', '2016-02-06 03:33:07');
INSERT INTO `sys_roles_proceedings` VALUES ('56b50709-6b68-4114-a92b-9911671fe029', '56b5068c-cc38-43b4-b690-9016671fe029', '565bf318-555c-433d-a4ef-12dcc689773d', '1', null, null, '2016-02-06 03:33:13', '2016-02-06 03:33:13');
INSERT INTO `sys_roles_proceedings` VALUES ('56b515d8-745c-4098-a6b2-5296671fe029', '56b5068c-cc38-43b4-b690-9016671fe029', '565bf0f9-9a90-463f-ae98-14b4c689773d', '1', null, null, '2016-02-06 04:36:24', '2016-02-06 04:36:24');
INSERT INTO `sys_roles_proceedings` VALUES ('56b51cfd-323c-4115-9ee1-a362671fe029', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '56a360b6-6a2c-4f27-ab37-0384ded5206b', '1', null, null, '2016-02-06 05:06:53', '2016-02-06 05:06:53');
INSERT INTO `sys_roles_proceedings` VALUES ('56b51d02-261c-41df-b309-a36c671fe029', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '56a36157-083c-4b9f-ad03-06c4ded5206b', '1', null, null, '2016-02-06 05:06:58', '2016-02-06 05:06:58');
INSERT INTO `sys_roles_proceedings` VALUES ('56b51d08-f770-4290-8c28-a383671fe029', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '56a36157-083c-46b3-a781-06c4ded5206b', '1', null, null, '2016-02-06 05:07:04', '2016-02-06 05:07:04');
INSERT INTO `sys_roles_proceedings` VALUES ('56b51d0d-5de8-485f-b163-a3c1671fe029', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '56a36157-083c-49a7-827b-06c4ded5206b', '1', null, null, '2016-02-06 05:07:09', '2016-02-06 05:07:09');
INSERT INTO `sys_roles_proceedings` VALUES ('56b51d13-e3f4-4652-ac5d-a3df671fe029', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '56a36157-8edc-431c-af72-06c4ded5206b', '1', null, null, '2016-02-06 05:07:15', '2016-02-06 05:07:15');
INSERT INTO `sys_roles_proceedings` VALUES ('56b51d1f-0ae0-4522-9a0b-a891671fe029', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '56b506ad-3f28-4e31-b92d-91ae671fe029', '1', null, null, '2016-02-06 05:07:27', '2016-02-06 05:07:27');
INSERT INTO `sys_roles_proceedings` VALUES ('56b89a28-b95c-41ba-acf8-fb9f671fe029', '55f27924-fef8-4337-bf8e-1760c689773d', '56b8393d-ea14-40cd-af28-ca7e671fe029', '1', null, null, '2016-02-08 20:37:44', '2016-02-08 20:37:44');
INSERT INTO `sys_roles_proceedings` VALUES ('56b89a43-57c0-445a-b156-0149671fe029', '55f27924-fef8-4337-bf8e-1760c689773d', '56b8393d-0b48-46f8-aa62-ca7e671fe029', '1', null, null, '2016-02-08 20:38:11', '2016-02-08 20:38:11');
INSERT INTO `sys_roles_proceedings` VALUES ('56b89a57-afd0-43cd-9f9c-018b671fe029', '55f27924-fef8-4337-bf8e-1760c689773d', '56b8393d-1250-4d2f-b35e-ca7e671fe029', '1', null, null, '2016-02-08 20:38:31', '2016-02-08 20:38:31');
INSERT INTO `sys_roles_proceedings` VALUES ('56b89a73-ca64-482e-96f2-01d8671fe029', '55f27924-fef8-4337-bf8e-1760c689773d', '56b8393d-1ffc-4eb3-91f5-ca7e671fe029', '1', null, null, '2016-02-08 20:38:59', '2016-02-08 20:38:59');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7ed37-6490-4d7c-9bcd-4372671fe029', '56e7ed0a-08dc-45c2-8b6b-41c1671fe029', '5675bdd1-5380-4987-bf8f-48d5c0b9c32c', '1', null, null, '2016-03-15 18:08:39', '2016-03-15 18:08:39');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7ed3e-5408-4386-a937-481d671fe029', '56e7ed0a-08dc-45c2-8b6b-41c1671fe029', '5675bdd1-6c1c-4083-9ab0-499fc0b9c32c', '1', null, null, '2016-03-15 18:08:46', '2016-03-15 18:08:46');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7ed48-dba0-44fc-a651-48b4671fe029', '56e7ed0a-08dc-45c2-8b6b-41c1671fe029', '5675bdd1-7324-4fa6-b4ea-4b76c0b9c32c', '1', null, null, '2016-03-15 18:08:56', '2016-03-15 18:08:56');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7ed4f-8898-4189-a521-492f671fe029', '56e7ed0a-08dc-45c2-8b6b-41c1671fe029', '5675bdd1-79c8-4b70-97b2-49d3c0b9c32c', '1', null, null, '2016-03-15 18:09:03', '2016-03-15 18:09:03');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7ed56-6adc-4f9f-8d8d-498c671fe029', '56e7ed0a-08dc-45c2-8b6b-41c1671fe029', '5675bdd1-8008-4767-b71c-4467c0b9c32c', '1', null, null, '2016-03-15 18:09:10', '2016-03-15 18:09:10');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7ed7c-cb30-4d28-bf83-4a1f671fe029', '56e7ed0a-08dc-45c2-8b6b-41c1671fe029', '56b8393d-ea14-40cd-af28-ca7e671fe029', '1', null, null, '2016-03-15 18:09:48', '2016-03-15 18:09:48');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7ed82-d148-42aa-b67c-4a37671fe029', '56e7ed0a-08dc-45c2-8b6b-41c1671fe029', '56b8393d-0b48-46f8-aa62-ca7e671fe029', '1', null, null, '2016-03-15 18:09:54', '2016-03-15 18:09:54');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7ed87-f5e4-4f14-a333-4a3d671fe029', '56e7ed0a-08dc-45c2-8b6b-41c1671fe029', '56b8393d-1250-4d2f-b35e-ca7e671fe029', '1', null, null, '2016-03-15 18:09:59', '2016-03-15 18:09:59');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7ed8c-5908-4f99-8e82-4ac3671fe029', '56e7ed0a-08dc-45c2-8b6b-41c1671fe029', '56b8393d-1958-4ba2-a5c3-ca7e671fe029', '1', null, null, '2016-03-15 18:10:04', '2016-03-15 18:10:04');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7ed91-d020-4fba-95eb-4adf671fe029', '56e7ed0a-08dc-45c2-8b6b-41c1671fe029', '56b8393d-1ffc-4eb3-91f5-ca7e671fe029', '1', null, null, '2016-03-15 18:10:09', '2016-03-15 18:10:09');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7f0c8-26bc-4537-bfa8-991f671fe029', '56e7f02e-276c-430f-960d-8bab671fe029', '566b81d5-c9b0-46ce-aef0-0534ded5206b', '1', null, null, '2016-03-15 18:23:52', '2016-03-15 18:23:52');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7f0d0-ea50-49b3-a94a-9955671fe029', '56e7f02e-276c-430f-960d-8bab671fe029', '566b81d5-ecd8-4c0b-a6d3-0534ded5206b', '1', null, null, '2016-03-15 18:24:00', '2016-03-15 18:24:00');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7f0d9-4cf0-4a5e-ac2f-9989671fe029', '56e7f02e-276c-430f-960d-8bab671fe029', '566b81d5-0574-4673-af04-0534ded5206b', '1', null, null, '2016-03-15 18:24:09', '2016-03-15 18:24:09');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7f0e1-2164-412b-a7c1-9999671fe029', '56e7f02e-276c-430f-960d-8bab671fe029', '566b81d5-1d48-4a77-8df8-0534ded5206b', '1', null, null, '2016-03-15 18:24:17', '2016-03-15 18:24:17');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7f0e8-adfc-4ff3-9ac6-99c9671fe029', '56e7f02e-276c-430f-960d-8bab671fe029', '566b81d5-33f0-45ab-b00c-0534ded5206b', '1', null, null, '2016-03-15 18:24:24', '2016-03-15 18:24:24');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7f113-4c48-48a3-ab79-9a86671fe029', '56e7f02e-276c-430f-960d-8bab671fe029', '565bf303-8528-4bcc-be48-14b4c689773d', '1', null, null, '2016-03-15 18:25:07', '2016-03-15 18:25:07');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7f11c-1b70-4ef6-b852-9aa3671fe029', '56e7f02e-276c-430f-960d-8bab671fe029', '565bf308-f5f0-474a-b25f-1b24c689773d', '1', null, null, '2016-03-15 18:25:16', '2016-03-15 18:25:16');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7f123-5998-4d2b-b268-9abc671fe029', '56e7f02e-276c-430f-960d-8bab671fe029', '565bf30e-1e0c-457d-91d7-0ee4c689773d', '1', null, null, '2016-03-15 18:25:23', '2016-03-15 18:25:23');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7f12a-0108-4aea-a5e0-9ad4671fe029', '56e7f02e-276c-430f-960d-8bab671fe029', '565bf313-9c88-4602-9687-0d80c689773d', '1', null, null, '2016-03-15 18:25:30', '2016-03-15 18:25:30');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7f131-21bc-49c4-be03-9ae3671fe029', '56e7f02e-276c-430f-960d-8bab671fe029', '565bf318-555c-433d-a4ef-12dcc689773d', '1', null, null, '2016-03-15 18:25:37', '2016-03-15 18:25:37');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7f186-f1bc-4a4e-a08b-a1ce671fe029', '56b5068c-cc38-43b4-b690-9016671fe029', '5675bdd1-5380-4987-bf8f-48d5c0b9c32c', null, null, null, '2016-03-15 18:27:02', '2016-03-15 18:27:10');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7f196-a7ec-4cb4-b7ca-a228671fe029', '56b5068c-cc38-43b4-b690-9016671fe029', '56a360b6-6a2c-4f27-ab37-0384ded5206b', '1', null, null, '2016-03-15 18:27:18', '2016-03-15 18:27:18');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7f19f-4170-4b18-ac24-a277671fe029', '56b5068c-cc38-43b4-b690-9016671fe029', '56a36157-083c-4b9f-ad03-06c4ded5206b', '1', null, null, '2016-03-15 18:27:27', '2016-03-15 18:27:27');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7f1a8-d268-4334-ab23-a290671fe029', '56b5068c-cc38-43b4-b690-9016671fe029', '56a36157-083c-46b3-a781-06c4ded5206b', '1', null, null, '2016-03-15 18:27:36', '2016-03-15 18:27:36');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7f1b2-3a70-4a1e-8c85-a29f671fe029', '56b5068c-cc38-43b4-b690-9016671fe029', '56a36157-083c-49a7-827b-06c4ded5206b', '1', null, null, '2016-03-15 18:27:46', '2016-03-15 18:27:46');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7f1b9-c1e8-4f3c-a705-a2b9671fe029', '56b5068c-cc38-43b4-b690-9016671fe029', '56a36157-8edc-431c-af72-06c4ded5206b', '1', null, null, '2016-03-15 18:27:53', '2016-03-15 18:27:53');
INSERT INTO `sys_roles_proceedings` VALUES ('5e9d5d0e-0e90-4801-aefe-2d81a747c35f', '5e9d5c0c-0a70-4cdc-94a4-2b0ca747c35f', '5670fd13-8520-40d2-af4f-4abcc0b9c32c', '1', null, null, '2020-04-20 08:27:58', '2020-04-20 08:27:58');
INSERT INTO `sys_roles_proceedings` VALUES ('5e9d5d26-c44c-4189-b626-2d92a747c35f', '5e9d5c0c-0a70-4cdc-94a4-2b0ca747c35f', '5670fdb2-c8e8-4ed8-9ca2-4471c0b9c32c', null, null, null, '2020-04-20 08:28:22', '2020-04-20 08:30:47');
INSERT INTO `sys_roles_proceedings` VALUES ('5e9d5d2f-76bc-4a32-8621-2d80a747c35f', '5e9d5c0c-0a70-4cdc-94a4-2b0ca747c35f', '5670fdb2-ab9c-43ab-a321-496ac0b9c32c', null, null, null, '2020-04-20 08:28:31', '2020-04-20 08:30:44');
INSERT INTO `sys_roles_proceedings` VALUES ('5e9d5dca-405c-4313-900f-2e44a747c35f', '5e9d5c0c-0a70-4cdc-94a4-2b0ca747c35f', '56b8393d-ea14-40cd-af28-ca7e671fe029', '1', null, null, '2020-04-20 08:31:06', '2020-04-20 08:31:06');
INSERT INTO `sys_roles_proceedings` VALUES ('5e9d5dd0-d388-4272-a677-2e44a747c35f', '5e9d5c0c-0a70-4cdc-94a4-2b0ca747c35f', '56b8393d-0b48-46f8-aa62-ca7e671fe029', '1', null, null, '2020-04-20 08:31:12', '2020-04-20 08:31:12');
INSERT INTO `sys_roles_proceedings` VALUES ('65900104-24f1-49e7-aa0c-97529c96e53a', '55f27924-fef8-4337-bf8e-1760c689773d', '5f7af17d-90e4-4616-bc5b-2fdcded5206b', '1', null, null, '2020-10-05 12:12:35', '2020-10-05 12:12:35');
INSERT INTO `sys_roles_proceedings` VALUES ('6f3ef080-93d8-4ff3-be9f-9ccccab10f70', '55f27924-fef8-4337-bf8e-1760c689773d', '5f7af178-778c-4114-998b-1840ded5206b', '1', null, null, '2020-10-05 12:12:35', '2020-10-05 12:12:35');
INSERT INTO `sys_roles_proceedings` VALUES ('75814529-bd47-4bce-bc78-3f4791c94bb7', '55f27924-fef8-4337-bf8e-1760c689773d', '5f7af0e1-5428-4d94-bafc-2944ded5206b', '1', null, null, '2020-10-05 12:10:00', '2020-10-05 12:10:00');
INSERT INTO `sys_roles_proceedings` VALUES ('785ae0af-6afe-41d7-9331-01f6f7a2b726', '55f27924-fef8-4337-bf8e-1760c689773d', '5f6b0a0e-1434-46c1-a8c5-2790ded5206b', '1', null, null, '2020-09-23 08:44:46', '2020-09-23 08:44:46');
INSERT INTO `sys_roles_proceedings` VALUES ('80ca771c-4f45-48af-8b3c-189499bde0de', '55f27924-fef8-4337-bf8e-1760c689773d', '5f6b0a0e-08e0-45d5-b029-2790ded5206b', '1', null, null, '2020-09-23 08:44:45', '2020-09-23 08:44:45');
INSERT INTO `sys_roles_proceedings` VALUES ('8ce8faab-a6f4-429c-a4d1-c103238dd64b', '55f27924-fef8-4337-bf8e-1760c689773d', '5f6ac4b5-e088-4ad9-bf5f-0804ded5206b', '1', null, null, '2020-09-23 04:32:40', '2020-09-23 04:32:40');
INSERT INTO `sys_roles_proceedings` VALUES ('9a860867-4f1e-47dc-b16d-2efb779ca890', '55f27924-fef8-4337-bf8e-1760c689773d', '5f6b0a0e-1948-4a14-b45d-2790ded5206b', '1', null, null, '2020-09-23 08:44:46', '2020-09-23 08:44:46');
INSERT INTO `sys_roles_proceedings` VALUES ('a20218a2-e635-4cd7-b6d9-64c387a22dcd', '55f27924-fef8-4337-bf8e-1760c689773d', '5f6b1220-ceb0-4017-9b3b-22bcded5206b', '1', null, null, '2020-09-23 09:17:24', '2020-09-23 09:17:24');
INSERT INTO `sys_roles_proceedings` VALUES ('a249f3f6-34f7-4e56-818b-9f43a5b6cdef', '55f27924-fef8-4337-bf8e-1760c689773d', '5f6b1215-94a0-493b-b6a1-22bcded5206b', '1', null, null, '2020-09-23 09:17:24', '2020-09-23 09:17:24');
INSERT INTO `sys_roles_proceedings` VALUES ('afea451c-91c2-4139-b8dd-ba3a54da1610', '55f27924-fef8-4337-bf8e-1760c689773d', '5f7aefec-4c34-4e8e-b8c9-2944ded5206b', '1', null, null, '2020-10-05 12:05:45', '2020-10-05 12:05:45');
INSERT INTO `sys_roles_proceedings` VALUES ('b082e21f-a6b8-4288-951e-d8e9d0add962', '55f27924-fef8-4337-bf8e-1760c689773d', '5f7af0d3-859c-4fde-b979-2944ded5206b', '1', null, null, '2020-10-05 12:10:00', '2020-10-05 12:10:00');
INSERT INTO `sys_roles_proceedings` VALUES ('b0dea65c-5650-459c-8533-c4d6971d0de4', '55f27924-fef8-4337-bf8e-1760c689773d', '5f7aefe1-6388-4a7d-88ce-1840ded5206b', '1', null, null, '2020-10-05 12:05:45', '2020-10-05 12:05:45');
INSERT INTO `sys_roles_proceedings` VALUES ('bbbd7072-ac7c-4a33-adec-a465ff42e4f3', '55f27924-fef8-4337-bf8e-1760c689773d', '5f6b0a0e-21e0-4589-90b3-2790ded5206b', '1', null, null, '2020-09-23 08:44:46', '2020-09-23 08:44:46');
INSERT INTO `sys_roles_proceedings` VALUES ('bfb49788-f338-403d-a43a-9cc6748da6d3', '55f27924-fef8-4337-bf8e-1760c689773d', '5f7af16f-8c90-4b1c-b6c4-2fdcded5206b', '1', null, null, '2020-10-05 12:12:35', '2020-10-05 12:12:35');
INSERT INTO `sys_roles_proceedings` VALUES ('c034bb68-b697-4d9b-a1e2-b9d8fba0afdd', '55f27924-fef8-4337-bf8e-1760c689773d', '5f7af181-2dc0-41ab-a154-2944ded5206b', '1', null, null, '2020-10-05 12:12:35', '2020-10-05 12:12:35');
INSERT INTO `sys_roles_proceedings` VALUES ('c0bfc6a3-dca4-4c45-b652-73c019670415', '55f27924-fef8-4337-bf8e-1760c689773d', '5f7aeff1-3530-433b-8b84-1840ded5206b', '1', null, null, '2020-10-05 12:05:45', '2020-10-05 12:05:45');
INSERT INTO `sys_roles_proceedings` VALUES ('d0fb1e10-8277-4b9d-9ba6-bfcdefd8d8c5', '55f27924-fef8-4337-bf8e-1760c689773d', '5f6ac4c2-f52c-4824-8afe-0804ded5206b', '1', null, null, '2020-09-23 04:32:40', '2020-09-23 04:32:40');
INSERT INTO `sys_roles_proceedings` VALUES ('d4775330-8526-43d8-9d15-501ab5dc1770', '55f27924-fef8-4337-bf8e-1760c689773d', '5f6b121a-d930-4ee5-9036-22bcded5206b', '1', null, null, '2020-09-23 09:17:24', '2020-09-23 09:17:24');
INSERT INTO `sys_roles_proceedings` VALUES ('daabb633-d16c-4c72-b7a4-89fcc1b90f43', '55f27924-fef8-4337-bf8e-1760c689773d', '5f7af174-d744-446c-ada3-2944ded5206b', '1', null, null, '2020-10-05 12:12:35', '2020-10-05 12:12:35');
INSERT INTO `sys_roles_proceedings` VALUES ('f5a6639b-6497-4ef7-9a5b-8560d89a394d', '55f27924-fef8-4337-bf8e-1760c689773d', '5f6b120f-6168-4c0d-a098-22bcded5206b', '1', null, null, '2020-09-23 09:17:24', '2020-09-23 09:17:24');

-- ----------------------------
-- Table structure for `sys_users_roles`
-- ----------------------------
DROP TABLE IF EXISTS `sys_users_roles`;
CREATE TABLE `sys_users_roles` (
  `id` char(36) NOT NULL,
  `user_id` char(36) NOT NULL,
  `role_id` char(36) NOT NULL,
  `active_status` tinyint(1) DEFAULT 1,
  `menu_code` varchar(50) DEFAULT NULL,
  `menu_order` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sys_users_roles
-- ----------------------------
INSERT INTO `sys_users_roles` VALUES ('55f285b8-24e0-4968-adab-0984c689773d', '55f285b8-2b4c-4558-89b9-0984c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '1', null, null, null, null);
INSERT INTO `sys_users_roles` VALUES ('56552ff7-5530-428d-86d9-16d4c689773d', '56552ff0-99cc-485a-a890-1b0cc689773d', '5653c30f-c7ac-4678-9eb1-160cc689773d', '1', null, null, null, null);
INSERT INTO `sys_users_roles` VALUES ('5671a068-4fa4-4aed-b8e9-4005c0b9c32c', '5671a055-c814-47d5-b3df-4542c0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '1', null, null, null, null);
INSERT INTO `sys_users_roles` VALUES ('56aa2034-2954-40b9-b348-4115c0b9c32c', '5661a6b8-f538-4c73-a0bd-06d8c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '1', null, null, null, null);
INSERT INTO `sys_users_roles` VALUES ('56b2d3b5-2374-440f-b8f9-5b1f671fe029', '5694d911-316c-4842-9291-bb61c0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '1', null, null, null, null);
INSERT INTO `sys_users_roles` VALUES ('56b5072a-30d8-418f-b7df-99e5671fe029', '56a57c55-d288-4c69-98bb-49d3c0b9c32c', '56b5068c-cc38-43b4-b690-9016671fe029', '1', null, null, null, null);
INSERT INTO `sys_users_roles` VALUES ('56b51569-8444-4e6f-aec7-50ae671fe029', '56b51561-d4b8-4007-8277-509e671fe029', '56b5068c-cc38-43b4-b690-9016671fe029', '1', null, null, null, null);
INSERT INTO `sys_users_roles` VALUES ('56b953bc-ea5c-4ae5-8934-5089671fe029', '56b95376-9950-444f-bd75-4ad3671fe029', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '1', null, null, null, null);
INSERT INTO `sys_users_roles` VALUES ('56e62b59-fd24-41a6-8443-f26b671fe029', '56e62b3f-a084-4c4e-893c-f1dd671fe029', '55f27924-fef8-4337-bf8e-1760c689773d', '1', null, null, null, null);
INSERT INTO `sys_users_roles` VALUES ('56e7edc3-d0c8-4344-874a-50d5671fe029', '56c157a7-fd6c-4402-8888-714c671fe029', '56e7ed0a-08dc-45c2-8b6b-41c1671fe029', '1', null, null, null, null);
INSERT INTO `sys_users_roles` VALUES ('56e7f05c-3e64-4062-ab3c-9182671fe029', '56e7ef7f-01cc-4672-8cc2-8139671fe029', '56e7f02e-276c-430f-960d-8bab671fe029', '1', null, null, null, null);
INSERT INTO `sys_users_roles` VALUES ('56ebc3cd-ba20-40ba-a5e1-8bd2671fe029', '56ebc11a-c900-4d33-9b38-7060671fe029', '56b5068c-cc38-43b4-b690-9016671fe029', '1', null, null, null, null);
INSERT INTO `sys_users_roles` VALUES ('56ebc6f8-91b4-4d6c-8384-b2e7671fe029', '56ebc6ee-e648-4954-8e9d-b2ba671fe029', '56e7f02e-276c-430f-960d-8bab671fe029', '1', null, null, null, null);
INSERT INTO `sys_users_roles` VALUES ('57033396-6adc-4083-840f-c841671fe029', '57033380-6d14-43c5-9211-c366671fe029', '56b5068c-cc38-43b4-b690-9016671fe029', '1', null, null, null, null);
INSERT INTO `sys_users_roles` VALUES ('570333b0-e3f4-4e12-b261-c8c3671fe029', '570332c6-5b64-4059-a0ea-bbaf671fe029', '56b5068c-cc38-43b4-b690-9016671fe029', '1', null, null, null, null);
INSERT INTO `sys_users_roles` VALUES ('57033411-e6c4-48aa-89ed-cb3f671fe029', '57033407-88b0-4063-bc5a-caff671fe029', '56b5068c-cc38-43b4-b690-9016671fe029', '1', null, null, null, null);
INSERT INTO `sys_users_roles` VALUES ('5703345d-addc-4bf9-ab87-d07f671fe029', '57033452-3a7c-4b28-ade8-d056671fe029', '56e7ed0a-08dc-45c2-8b6b-41c1671fe029', '1', null, null, null, null);
INSERT INTO `sys_users_roles` VALUES ('582ad4d2-2734-4a74-b5fb-350f67f70827', '582ad4c6-9ce0-46f8-a51b-35a267f70827', '56b5068c-cc38-43b4-b690-9016671fe029', '1', null, null, null, null);
INSERT INTO `sys_users_roles` VALUES ('582ad58c-e5a4-415a-aa9c-35a267f70827', '5694d911-316c-4842-9291-bb61c0b9c32c', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '1', null, null, null, null);
INSERT INTO `sys_users_roles` VALUES ('582ad58c-ef04-4a59-ba26-35a267f70827', '5694d911-316c-4842-9291-bb61c0b9c32c', '56b5068c-cc38-43b4-b690-9016671fe029', '1', null, null, null, null);
INSERT INTO `sys_users_roles` VALUES ('58856689-3200-469e-9a66-2b8967f70827', '588565c3-bdd0-4f94-a630-2b1667f70827', '56e7ed0a-08dc-45c2-8b6b-41c1671fe029', '1', null, null, null, null);
INSERT INTO `sys_users_roles` VALUES ('58d1ec5d-f92c-4f3e-825b-36b967f70827', '58d1ec4b-248c-43aa-98a4-36b867f70827', '56e7f02e-276c-430f-960d-8bab671fe029', '1', null, null, null, null);
INSERT INTO `sys_users_roles` VALUES ('5d92fe21-e2e4-470f-8087-7cd267f70827', '57033380-6d14-43c5-9211-c366671fe029', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '1', null, null, null, null);
INSERT INTO `sys_users_roles` VALUES ('5e1be04e-422c-4453-a790-6e0b67f70827', '5e1be03d-a6e0-4ac7-8cbf-6e0d67f70827', '56b5068c-cc38-43b4-b690-9016671fe029', '1', null, null, null, null);
INSERT INTO `sys_users_roles` VALUES ('5e1be04e-580c-4bb3-b0dd-6e0b67f70827', '5e1be03d-a6e0-4ac7-8cbf-6e0d67f70827', '56e7f02e-276c-430f-960d-8bab671fe029', '1', null, null, null, null);
INSERT INTO `sys_users_roles` VALUES ('5e867fd5-4da0-491a-9e2f-7e3fa747c35f', '5e867f99-ad78-4b27-8e85-7dd0a747c35f', '55f27924-fef8-4337-bf8e-1760c689773d', '1', null, null, null, null);
INSERT INTO `sys_users_roles` VALUES ('5e9d5cb9-c584-4268-b894-2d5ea747c35f', '5e9d5caa-0e64-4422-8433-2d5ba747c35f', '5e9d5c0c-0a70-4cdc-94a4-2b0ca747c35f', '1', null, null, null, null);
INSERT INTO `sys_users_roles` VALUES ('5f11f67f-9e88-4231-ad69-56e7a747c35f', '5671a055-c814-47d5-b3df-4542c0b9c32c', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '1', null, null, null, null);
INSERT INTO `sys_users_roles` VALUES ('5f11f67f-a018-4a52-9c7f-56e7a747c35f', '5671a055-c814-47d5-b3df-4542c0b9c32c', '56b5068c-cc38-43b4-b690-9016671fe029', '1', null, null, null, null);
