/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : perumdamtkr_website

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-09-05 15:40:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `sys_pendaftaran`
-- ----------------------------
DROP TABLE IF EXISTS `sys_pendaftaran`;
CREATE TABLE `sys_pendaftaran` (
  `id` varchar(36) NOT NULL,
  `nama_pemohon` varchar(100) NOT NULL,
  `nama_pemilik` varchar(100) NOT NULL,
  `nomor_ktp` varchar(16) NOT NULL,
  `nomor_hp` varchar(14) NOT NULL,
  `nomor_telepon` varchar(14) NOT NULL,
  `email` varchar(100) NOT NULL,
  `id_prov` varchar(14) NOT NULL,
  `id_kab` varchar(14) NOT NULL,
  `id_kec` varchar(14) NOT NULL,
  `id_kel` varchar(14) NOT NULL,
  `alamat` varchar(225) NOT NULL,
  `kode_pos` varchar(10) NOT NULL,
  `tgl_buat` datetime NOT NULL,
  `tgl_edit` datetime NOT NULL,
  `status` int(1) DEFAULT NULL,
  `foto_ktp` varchar(255) NOT NULL,
  `kode_pendaftaran` varchar(100) NOT NULL,
  `kabupaten` varchar(50) DEFAULT NULL,
  `kecamatan` varchar(50) DEFAULT NULL,
  `kelurahan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nomor_ktp` (`nomor_ktp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabel pendaftaran v2';

-- ----------------------------
-- Records of sys_pendaftaran
-- ----------------------------
INSERT INTO `sys_pendaftaran` VALUES ('72dd5f4f-6b76-40d3-b935-a47bd3bf4353', 'Bambang Setiadi', 'Bambang Setiadi', '0000111122224444', '08953713088', '', 'cristminix@gmail.com', '36', '01', '014', '117', 'Komplek Kehakiman', '15820', '2020-09-05 08:31:21', '2020-09-05 08:31:21', '0', '2ca5598d21061fefc48af8457fab61f8-2019-02-27-e-ktp-orang-asing-696x464.jpg', 'WEB-20200905-0831-BAMBANG', 'Kabupaten Tangerang', 'LEGOK', 'Babakan');
