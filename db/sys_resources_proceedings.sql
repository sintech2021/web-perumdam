/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : perumdamtkr_website

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-09-23 11:39:25
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `sys_resources_proceedings`
-- ----------------------------
DROP TABLE IF EXISTS `sys_resources_proceedings`;
CREATE TABLE `sys_resources_proceedings` (
  `id` char(36) NOT NULL,
  `resource_id` char(36) NOT NULL,
  `proceeding_id` char(36) NOT NULL,
  `active_status` tinyint(1) DEFAULT 1,
  `menu_code` varchar(50) DEFAULT NULL,
  `menu_order` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sys_resources_proceedings
-- ----------------------------
INSERT INTO `sys_resources_proceedings` VALUES ('55f1c6b6-c304-47cc-8116-1770c689773d', '55f1c6b6-5e68-4e13-9ad8-1770c689773d', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('55f1ca15-a1e8-44e3-bde6-177cc689773d', '55f1ca15-17a8-4d1d-8fa7-177cc689773d', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('55f2226b-4af8-4962-9a73-1710c689773d', '55f2226b-3e64-4760-979f-1710c689773d', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('55f2226b-6bc8-4ccc-96af-1710c689773d', '55f2226b-3e64-4760-979f-1710c689773d', '55f1353a-800c-4370-930b-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('55f2226b-84c8-4c5d-8a67-1710c689773d', '55f2226b-3e64-4760-979f-1710c689773d', '55f13558-1a0c-4885-9584-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('55f2226b-9bd4-404f-ae3b-1710c689773d', '55f2226b-3e64-4760-979f-1710c689773d', '55f13590-fd38-4fa0-a5ed-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('55f23411-2e98-4b73-ab8c-1710c689773d', '55f23411-2c50-450e-b047-1710c689773d', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('55f23411-5288-4b51-97a8-1710c689773d', '55f23411-2c50-450e-b047-1710c689773d', '55f1353a-800c-4370-930b-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('55f23411-6c50-44c1-855a-1710c689773d', '55f23411-2c50-450e-b047-1710c689773d', '55f13558-1a0c-4885-9584-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('55f23411-8424-4677-a821-1710c689773d', '55f23411-2c50-450e-b047-1710c689773d', '55f13590-fd38-4fa0-a5ed-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('55f23ffc-99c8-4ceb-9f25-14a0c689773d', '55f23ffc-f8a0-49b7-a8c1-14a0c689773d', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('55f2e9f0-66ec-4b3e-929c-1a40c689773d', '55f2e9f0-a130-4f1c-b798-1a40c689773d', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('55f2e9f0-894c-4eef-88c3-1a40c689773d', '55f2e9f0-a130-4f1c-b798-1a40c689773d', '55f1353a-800c-4370-930b-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('55f2e9f0-a184-4208-a606-1a40c689773d', '55f2e9f0-a130-4f1c-b798-1a40c689773d', '55f13558-1a0c-4885-9584-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('55f2e9f0-b8f4-4792-a509-1a40c689773d', '55f2e9f0-a130-4f1c-b798-1a40c689773d', '55f13590-fd38-4fa0-a5ed-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('55f2e9f0-d064-4f79-999b-1a40c689773d', '55f2e9f0-a130-4f1c-b798-1a40c689773d', '55f135cf-bae0-48ae-9d67-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('55fda9c3-07c8-4afe-b95d-18c0c689773d', '55f2226b-3e64-4760-979f-1710c689773d', '55f135cf-bae0-48ae-9d67-12f8c689773d', '1', '', null, '2015-09-20 01:30:27', '2015-09-20 01:30:27');
INSERT INTO `sys_resources_proceedings` VALUES ('56161cca-5080-436c-958c-04ccc689773d', '56161cca-d96c-4104-b967-04ccc689773d', '55f135ed-e970-47c3-9d0e-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5624ffee-39b4-48e4-9039-174cc689773d', '55f23411-2c50-450e-b047-1710c689773d', '55f135cf-bae0-48ae-9d67-12f8c689773d', '1', '', null, '2015-10-19 21:36:30', '2015-10-19 21:36:30');
INSERT INTO `sys_resources_proceedings` VALUES ('5629cbf0-1838-43b5-b372-1ac8c689773d', '5629cbf0-ae58-4372-bb50-1ac8c689773d', '55f13558-1a0c-4885-9584-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5629cbf0-30d4-4a7e-8ab5-1ac8c689773d', '5629cbf0-ae58-4372-bb50-1ac8c689773d', '55f13590-fd38-4fa0-a5ed-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5629cbf0-48a8-427f-8ffc-1ac8c689773d', '5629cbf0-ae58-4372-bb50-1ac8c689773d', '55f135cf-bae0-48ae-9d67-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5629cbf0-da1c-41a9-bccc-1ac8c689773d', '5629cbf0-ae58-4372-bb50-1ac8c689773d', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5629cbf0-fe0c-4241-9d88-1ac8c689773d', '5629cbf0-ae58-4372-bb50-1ac8c689773d', '55f1353a-800c-4370-930b-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('565bf0f9-9a90-463f-ae98-14b4c689773d', '565bf014-6b60-42d9-8a32-0ee4c689773d', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, '2015-11-30 13:47:21', '2015-11-30 13:47:21');
INSERT INTO `sys_resources_proceedings` VALUES ('565bf303-8528-4bcc-be48-14b4c689773d', '565bf24c-6ed4-4309-babe-18a0c689773d', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, '2015-11-30 13:56:03', '2015-11-30 13:56:03');
INSERT INTO `sys_resources_proceedings` VALUES ('565bf308-f5f0-474a-b25f-1b24c689773d', '565bf24c-6ed4-4309-babe-18a0c689773d', '55f1353a-800c-4370-930b-12f8c689773d', '1', null, null, '2015-11-30 13:56:08', '2015-11-30 13:56:08');
INSERT INTO `sys_resources_proceedings` VALUES ('565bf30e-1e0c-457d-91d7-0ee4c689773d', '565bf24c-6ed4-4309-babe-18a0c689773d', '55f13558-1a0c-4885-9584-1780c689773d', '1', null, null, '2015-11-30 13:56:14', '2015-11-30 13:56:14');
INSERT INTO `sys_resources_proceedings` VALUES ('565bf313-9c88-4602-9687-0d80c689773d', '565bf24c-6ed4-4309-babe-18a0c689773d', '55f13590-fd38-4fa0-a5ed-1780c689773d', '1', null, null, '2015-11-30 13:56:19', '2015-11-30 13:56:19');
INSERT INTO `sys_resources_proceedings` VALUES ('565bf318-555c-433d-a4ef-12dcc689773d', '565bf24c-6ed4-4309-babe-18a0c689773d', '55f135cf-bae0-48ae-9d67-12f8c689773d', '1', null, null, '2015-11-30 13:56:24', '2015-11-30 13:56:24');
INSERT INTO `sys_resources_proceedings` VALUES ('565c0210-534c-4fc3-b712-0d80c689773d', '565c01b5-323c-455f-815a-12dcc689773d', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, '2015-11-30 15:00:16', '2015-11-30 15:00:16');
INSERT INTO `sys_resources_proceedings` VALUES ('565c0217-d5e4-4f0c-9e9c-12dcc689773d', '565c01b5-323c-455f-815a-12dcc689773d', '55f1353a-800c-4370-930b-12f8c689773d', '1', null, null, '2015-11-30 15:00:23', '2015-11-30 15:00:23');
INSERT INTO `sys_resources_proceedings` VALUES ('565c021d-e730-4dcf-afdf-056cc689773d', '565c01b5-323c-455f-815a-12dcc689773d', '55f13558-1a0c-4885-9584-1780c689773d', '1', null, null, '2015-11-30 15:00:29', '2015-11-30 15:00:29');
INSERT INTO `sys_resources_proceedings` VALUES ('565c0223-ae5c-4297-a849-18a0c689773d', '565c01b5-323c-455f-815a-12dcc689773d', '55f13590-fd38-4fa0-a5ed-1780c689773d', '1', null, null, '2015-11-30 15:00:35', '2015-11-30 15:00:35');
INSERT INTO `sys_resources_proceedings` VALUES ('565c0229-f414-44f9-98ac-14b4c689773d', '565c01b5-323c-455f-815a-12dcc689773d', '55f135cf-bae0-48ae-9d67-12f8c689773d', '1', null, null, '2015-11-30 15:00:41', '2015-11-30 15:00:41');
INSERT INTO `sys_resources_proceedings` VALUES ('5661a59a-2f08-4e7a-8372-17e0c689773d', '5661a58f-3dc0-4784-9ef3-06d8c689773d', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5661a59a-6cc0-4989-b814-17e0c689773d', '5661a58f-3dc0-4784-9ef3-06d8c689773d', '55f1353a-800c-4370-930b-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5661a59a-9948-4912-aff8-17e0c689773d', '5661a58f-3dc0-4784-9ef3-06d8c689773d', '55f13558-1a0c-4885-9584-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5661a59a-c508-4520-8044-17e0c689773d', '5661a58f-3dc0-4784-9ef3-06d8c689773d', '55f13590-fd38-4fa0-a5ed-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5661a59a-ef9c-42de-a8f1-17e0c689773d', '5661a58f-3dc0-4784-9ef3-06d8c689773d', '55f135cf-bae0-48ae-9d67-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('56630646-3498-408a-acf6-0d7cded5206b', '5663063c-1a78-4470-b454-1964ded5206b', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('56630646-6cd8-42be-9f06-0d7cded5206b', '5663063c-1a78-4470-b454-1964ded5206b', '55f1353a-800c-4370-930b-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('56630646-9834-4a31-beaa-0d7cded5206b', '5663063c-1a78-4470-b454-1964ded5206b', '55f13558-1a0c-4885-9584-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('56630646-c19c-4f44-a044-0d7cded5206b', '5663063c-1a78-4470-b454-1964ded5206b', '55f13590-fd38-4fa0-a5ed-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('56630646-ebcc-42ad-9092-0d7cded5206b', '5663063c-1a78-4470-b454-1964ded5206b', '55f135cf-bae0-48ae-9d67-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5663093e-388c-4c3b-9677-1960ded5206b', '56630937-1bac-4c0b-862d-15e4ded5206b', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5663093e-5da8-4920-9d10-1960ded5206b', '56630937-1bac-4c0b-862d-15e4ded5206b', '55f1353a-800c-4370-930b-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5663093e-7770-4103-b098-1960ded5206b', '56630937-1bac-4c0b-862d-15e4ded5206b', '55f13558-1a0c-4885-9584-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5663093e-8ee0-43fc-9972-1960ded5206b', '56630937-1bac-4c0b-862d-15e4ded5206b', '55f13590-fd38-4fa0-a5ed-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5663093e-a5ec-463c-a8a2-1960ded5206b', '56630937-1bac-4c0b-862d-15e4ded5206b', '55f135cf-bae0-48ae-9d67-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('566b81d5-0574-4673-af04-0534ded5206b', '566b81c7-e024-4167-ab08-08b8ded5206b', '55f13558-1a0c-4885-9584-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('566b81d5-1d48-4a77-8df8-0534ded5206b', '566b81c7-e024-4167-ab08-08b8ded5206b', '55f13590-fd38-4fa0-a5ed-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('566b81d5-33f0-45ab-b00c-0534ded5206b', '566b81c7-e024-4167-ab08-08b8ded5206b', '55f135cf-bae0-48ae-9d67-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('566b81d5-c9b0-46ce-aef0-0534ded5206b', '566b81c7-e024-4167-ab08-08b8ded5206b', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('566b81d5-ecd8-4c0b-a6d3-0534ded5206b', '566b81c7-e024-4167-ab08-08b8ded5206b', '55f1353a-800c-4370-930b-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5670fd13-8520-40d2-af4f-4abcc0b9c32c', '5670fc7b-a634-4224-84e1-4205c0b9c32c', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5670fdb2-ab9c-43ab-a321-496ac0b9c32c', '5670fd65-dd88-4bb1-804b-442ac0b9c32c', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5670fdb2-c8e8-4ed8-9ca2-4471c0b9c32c', '5670fd65-dd88-4bb1-804b-442ac0b9c32c', '55f1353a-800c-4370-930b-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5670fdb2-d11c-4018-94c5-491cc0b9c32c', '5670fd65-dd88-4bb1-804b-442ac0b9c32c', '55f13558-1a0c-4885-9584-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5670fdb2-d824-4e29-867e-4c5bc0b9c32c', '5670fd65-dd88-4bb1-804b-442ac0b9c32c', '55f13590-fd38-4fa0-a5ed-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5670fdb2-df2c-4577-99ad-4d13c0b9c32c', '5670fd65-dd88-4bb1-804b-442ac0b9c32c', '55f135cf-bae0-48ae-9d67-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5675bcbe-b704-45e9-abe0-4500c0b9c32c', '5675bc53-4d98-40fc-93ad-4e1fc0b9c32c', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5675bdd1-5380-4987-bf8f-48d5c0b9c32c', '5675bd7c-3534-4406-9579-46dac0b9c32c', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5675bdd1-6c1c-4083-9ab0-499fc0b9c32c', '5675bd7c-3534-4406-9579-46dac0b9c32c', '55f1353a-800c-4370-930b-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5675bdd1-7324-4fa6-b4ea-4b76c0b9c32c', '5675bd7c-3534-4406-9579-46dac0b9c32c', '55f13558-1a0c-4885-9584-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5675bdd1-79c8-4b70-97b2-49d3c0b9c32c', '5675bd7c-3534-4406-9579-46dac0b9c32c', '55f13590-fd38-4fa0-a5ed-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5675bdd1-8008-4767-b71c-4467c0b9c32c', '5675bd7c-3534-4406-9579-46dac0b9c32c', '55f135cf-bae0-48ae-9d67-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('568a6a85-274c-4ffa-8f16-42dfc0b9c32c', '568a676e-d860-49b7-915f-ecefc0b9c32c', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, '2016-01-04 06:50:13', '2016-01-04 06:50:13');
INSERT INTO `sys_resources_proceedings` VALUES ('568a6a8d-82a4-4622-b551-41f6c0b9c32c', '568a676e-d860-49b7-915f-ecefc0b9c32c', '55f1353a-800c-4370-930b-12f8c689773d', '1', null, null, '2016-01-04 06:50:21', '2016-01-04 06:50:21');
INSERT INTO `sys_resources_proceedings` VALUES ('568a6a94-a640-499e-9caf-4ad6c0b9c32c', '568a676e-d860-49b7-915f-ecefc0b9c32c', '55f13558-1a0c-4885-9584-1780c689773d', '1', null, null, '2016-01-04 06:50:28', '2016-01-04 06:50:28');
INSERT INTO `sys_resources_proceedings` VALUES ('568a6a9c-5438-4f47-8db5-477bc0b9c32c', '568a676e-d860-49b7-915f-ecefc0b9c32c', '55f13590-fd38-4fa0-a5ed-1780c689773d', '1', null, null, '2016-01-04 06:50:36', '2016-01-04 06:50:36');
INSERT INTO `sys_resources_proceedings` VALUES ('568a6aa4-78b4-4e85-8911-4d71c0b9c32c', '568a676e-d860-49b7-915f-ecefc0b9c32c', '55f135cf-bae0-48ae-9d67-12f8c689773d', '1', null, null, '2016-01-04 06:50:44', '2016-01-04 06:50:44');
INSERT INTO `sys_resources_proceedings` VALUES ('56a360b6-6a2c-4f27-ab37-0384ded5206b', '56a36074-7264-401e-96e6-06c4ded5206b', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, '2016-01-23 18:15:02', '2016-01-23 18:15:02');
INSERT INTO `sys_resources_proceedings` VALUES ('56a36157-083c-46b3-a781-06c4ded5206b', '56a36074-7264-401e-96e6-06c4ded5206b', '55f13558-1a0c-4885-9584-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('56a36157-083c-49a7-827b-06c4ded5206b', '56a36074-7264-401e-96e6-06c4ded5206b', '55f13590-fd38-4fa0-a5ed-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('56a36157-083c-4b9f-ad03-06c4ded5206b', '56a36074-7264-401e-96e6-06c4ded5206b', '55f1353a-800c-4370-930b-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('56a36157-8edc-431c-af72-06c4ded5206b', '56a36074-7264-401e-96e6-06c4ded5206b', '55f135cf-bae0-48ae-9d67-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('56b506ad-3f28-4e31-b92d-91ae671fe029', '56b50667-3094-4d83-b55d-8b00671fe029', '55f135ed-e970-47c3-9d0e-1780c689773d', '1', null, null, '2016-02-06 03:31:41', '2016-02-06 03:31:41');
INSERT INTO `sys_resources_proceedings` VALUES ('56b8393d-0b48-46f8-aa62-ca7e671fe029', '56b8392f-2e10-445a-a4f7-c9b5671fe029', '55f1353a-800c-4370-930b-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('56b8393d-1250-4d2f-b35e-ca7e671fe029', '56b8392f-2e10-445a-a4f7-c9b5671fe029', '55f13558-1a0c-4885-9584-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('56b8393d-1958-4ba2-a5c3-ca7e671fe029', '56b8392f-2e10-445a-a4f7-c9b5671fe029', '55f13590-fd38-4fa0-a5ed-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('56b8393d-1ffc-4eb3-91f5-ca7e671fe029', '56b8392f-2e10-445a-a4f7-c9b5671fe029', '55f135cf-bae0-48ae-9d67-12f8c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('56b8393d-ea14-40cd-af28-ca7e671fe029', '56b8392f-2e10-445a-a4f7-c9b5671fe029', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, null, null);
INSERT INTO `sys_resources_proceedings` VALUES ('5f6ac4b5-e088-4ad9-bf5f-0804ded5206b', '565bf24c-6ed4-4309-bxxx-18a0c689773d', '55f13517-6190-42ec-a445-1780c689773d', '1', null, null, '2020-09-23 03:44:53', '2020-09-23 03:44:53');
INSERT INTO `sys_resources_proceedings` VALUES ('5f6ac4ba-898c-44bb-a6c3-2790ded5206b', '565bf24c-6ed4-4309-bxxx-18a0c689773d', '55f1353a-800c-4370-930b-12f8c689773d', '1', null, null, '2020-09-23 03:44:58', '2020-09-23 03:44:58');
INSERT INTO `sys_resources_proceedings` VALUES ('5f6ac4be-214c-44e6-99d7-1804ded5206b', '565bf24c-6ed4-4309-bxxx-18a0c689773d', '55f13558-1a0c-4885-9584-1780c689773d', '1', null, null, '2020-09-23 03:45:02', '2020-09-23 03:45:02');
INSERT INTO `sys_resources_proceedings` VALUES ('5f6ac4c2-f52c-4824-8afe-0804ded5206b', '565bf24c-6ed4-4309-bxxx-18a0c689773d', '55f13590-fd38-4fa0-a5ed-1780c689773d', '1', null, null, '2020-09-23 03:45:06', '2020-09-23 03:45:06');
INSERT INTO `sys_resources_proceedings` VALUES ('5f6ac4c8-b8dc-442f-abdd-2790ded5206b', '565bf24c-6ed4-4309-bxxx-18a0c689773d', '55f135cf-bae0-48ae-9d67-12f8c689773d', '1', null, null, '2020-09-23 03:45:12', '2020-09-23 03:45:12');
INSERT INTO `sys_resources_proceedings` VALUES ('5f6ac4cd-f830-40f8-a646-1804ded5206b', '565bf24c-6ed4-4309-bxxx-18a0c689773d', '55f135ed-e970-47c3-9d0e-1780c689773d', null, null, null, '2020-09-23 03:45:17', '2020-09-23 03:46:17');
