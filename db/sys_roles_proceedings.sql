/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : perumdamtkr_website

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-09-23 11:39:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `sys_roles_proceedings`
-- ----------------------------
DROP TABLE IF EXISTS `sys_roles_proceedings`;
CREATE TABLE `sys_roles_proceedings` (
  `id` char(36) NOT NULL,
  `role_id` char(36) NOT NULL,
  `resources_proceeding_id` char(36) NOT NULL,
  `active_status` tinyint(1) DEFAULT NULL,
  `menu_code` varchar(50) DEFAULT NULL,
  `menu_order` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sys_roles_proceedings
-- ----------------------------
INSERT INTO `sys_roles_proceedings` VALUES ('1e72c434-09ce-438b-b230-c10d230fa01f', '55f27924-fef8-4337-bf8e-1760c689773d', '5f6ac4ba-898c-44bb-a6c3-2790ded5206b', '1', null, null, '2020-09-23 04:32:40', '2020-09-23 04:32:40');
INSERT INTO `sys_roles_proceedings` VALUES ('201b25aa-fede-4fcb-98a4-48105ff5bf88', '55f27924-fef8-4337-bf8e-1760c689773d', '5f6ac4be-214c-44e6-99d7-1804ded5206b', '1', null, null, '2020-09-23 04:32:40', '2020-09-23 04:32:40');
INSERT INTO `sys_roles_proceedings` VALUES ('320ab5db-ab88-42b7-9e5e-7e14f7a87f92', '55f27924-fef8-4337-bf8e-1760c689773d', '5f6ac4c8-b8dc-442f-abdd-2790ded5206b', '1', null, null, '2020-09-23 04:32:40', '2020-09-23 04:32:40');
INSERT INTO `sys_roles_proceedings` VALUES ('3a2f9cff-4022-4b12-a098-7a6e756cfb5a', '55f27924-fef8-4337-bf8e-1760c689773d', '5f6ac4cd-f830-40f8-a646-1804ded5206b', '1', null, null, '2020-09-23 04:32:40', '2020-09-23 04:32:40');
INSERT INTO `sys_roles_proceedings` VALUES ('55f99c1a-43b8-43e4-848c-0f88c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f1c6b6-c304-47cc-8116-1770c689773d', '1', '', null, '2015-09-16 23:43:06', '2015-09-16 23:43:06');
INSERT INTO `sys_roles_proceedings` VALUES ('55fad9e4-0aac-49cd-965d-1438c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2226b-4af8-4962-9a73-1710c689773d', '1', '', null, '2015-09-17 22:19:00', '2015-09-17 22:19:00');
INSERT INTO `sys_roles_proceedings` VALUES ('55fbc346-b3d8-4d03-a1f4-106cc689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f23411-2e98-4b73-ab8c-1710c689773d', '1', '', null, '2015-09-18 14:54:46', '2015-09-18 14:54:46');
INSERT INTO `sys_roles_proceedings` VALUES ('55fcda5f-9fc8-48b5-84ed-0108c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f1ca15-a1e8-44e3-bde6-177cc689773d', '1', '', null, '2015-09-19 10:45:35', '2015-09-19 10:45:35');
INSERT INTO `sys_roles_proceedings` VALUES ('55fce104-1a48-4ab6-ab84-0108c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2f73d-deb0-46d0-9b3a-051cc689773d', '1', '', null, '2015-09-19 11:13:56', '2015-09-19 11:13:56');
INSERT INTO `sys_roles_proceedings` VALUES ('55fce12b-95e8-4f77-8722-05e0c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f23ffc-99c8-4ceb-9f25-14a0c689773d', '1', '', null, '2015-09-19 11:14:35', '2015-09-19 11:14:35');
INSERT INTO `sys_roles_proceedings` VALUES ('55fce17d-b6b0-4943-afc7-03c0c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2226b-6bc8-4ccc-96af-1710c689773d', '1', '', null, '2015-09-19 11:15:57', '2015-09-19 11:15:57');
INSERT INTO `sys_roles_proceedings` VALUES ('55fcf63e-39cc-47b0-bfa8-05e0c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2226b-84c8-4c5d-8a67-1710c689773d', '1', '', null, '2015-09-19 12:44:30', '2015-09-19 12:44:30');
INSERT INTO `sys_roles_proceedings` VALUES ('55fd1a5f-7464-4a3c-9f07-03c0c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2226b-9bd4-404f-ae3b-1710c689773d', '1', '', null, '2015-09-19 15:18:39', '2015-09-19 15:18:39');
INSERT INTO `sys_roles_proceedings` VALUES ('55fda9de-3294-4c38-b08e-1e58c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55fda9c3-07c8-4afe-b95d-18c0c689773d', '1', '', null, '2015-09-20 01:30:54', '2015-09-20 01:30:54');
INSERT INTO `sys_roles_proceedings` VALUES ('5616233e-2de4-4474-8fa7-16f8c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '56161cca-5080-436c-958c-04ccc689773d', '1', '', null, '2015-10-08 15:03:10', '2015-10-08 15:03:10');
INSERT INTO `sys_roles_proceedings` VALUES ('5624ffaf-696c-4763-ac8d-1480c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f23411-5288-4b51-97a8-1710c689773d', '1', '', null, '2015-10-19 21:35:27', '2015-10-19 21:35:27');
INSERT INTO `sys_roles_proceedings` VALUES ('5624ffc6-c580-46de-9a72-174cc689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f23411-6c50-44c1-855a-1710c689773d', '1', '', null, '2015-10-19 21:35:50', '2015-10-19 21:35:50');
INSERT INTO `sys_roles_proceedings` VALUES ('5624ffcd-49e0-4f83-a21b-1480c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f23411-8424-4677-a821-1710c689773d', '1', '', null, '2015-10-19 21:35:57', '2015-10-19 21:35:57');
INSERT INTO `sys_roles_proceedings` VALUES ('5624fffa-29dc-4c52-a05b-1480c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '5624ffee-39b4-48e4-9039-174cc689773d', '1', '', null, '2015-10-19 21:36:42', '2015-10-19 21:36:42');
INSERT INTO `sys_roles_proceedings` VALUES ('5629cd84-42ac-47b5-b65f-1ac8c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2e9f0-66ec-4b3e-929c-1a40c689773d', '1', '', null, '2015-10-23 13:02:44', '2015-10-23 13:02:44');
INSERT INTO `sys_roles_proceedings` VALUES ('5629cd8f-dca0-412b-adc4-1bbcc689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2e9f0-894c-4eef-88c3-1a40c689773d', '1', '', null, '2015-10-23 13:02:55', '2015-10-23 13:02:55');
INSERT INTO `sys_roles_proceedings` VALUES ('5629cd98-e42c-4828-a2f5-1ac8c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2e9f0-a184-4208-a606-1a40c689773d', '1', '', null, '2015-10-23 13:03:04', '2015-10-23 13:03:04');
INSERT INTO `sys_roles_proceedings` VALUES ('5629cda5-49d0-4e48-8894-1bbcc689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2e9f0-b8f4-4792-a509-1a40c689773d', '1', '', null, '2015-10-23 13:03:17', '2015-10-23 13:03:17');
INSERT INTO `sys_roles_proceedings` VALUES ('5629cdb0-4660-4abf-a30b-1ac8c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2e9f0-d064-4f79-999b-1a40c689773d', '1', '', null, '2015-10-23 13:03:28', '2015-10-23 13:03:28');
INSERT INTO `sys_roles_proceedings` VALUES ('5629ce06-2480-46c6-af44-0f28c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '5629cbf0-da1c-41a9-bccc-1ac8c689773d', '1', '', null, '2015-10-23 13:04:54', '2015-10-23 13:04:54');
INSERT INTO `sys_roles_proceedings` VALUES ('5629ce15-6a88-4454-a2aa-08f0c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '5629cbf0-fe0c-4241-9d88-1ac8c689773d', '1', '', null, '2015-10-23 13:05:09', '2015-10-23 13:05:09');
INSERT INTO `sys_roles_proceedings` VALUES ('5629ce26-8704-49d4-a585-0f28c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '5629cbf0-1838-43b5-b372-1ac8c689773d', '1', '', null, '2015-10-23 13:05:26', '2015-10-23 13:05:26');
INSERT INTO `sys_roles_proceedings` VALUES ('5629ce30-9530-4528-b4ca-08f0c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '5629cbf0-30d4-4a7e-8ab5-1ac8c689773d', '1', '', null, '2015-10-23 13:05:36', '2015-10-23 13:05:36');
INSERT INTO `sys_roles_proceedings` VALUES ('5629ce3a-7c60-47ed-b920-0f28c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '5629cbf0-48a8-427f-8ffc-1ac8c689773d', '1', '', null, '2015-10-23 13:05:46', '2015-10-23 13:05:46');
INSERT INTO `sys_roles_proceedings` VALUES ('56552b9e-abb4-432b-8866-1afcc689773d', '5653c30f-c7ac-4678-9eb1-160cc689773d', '55f1c6b6-c304-47cc-8116-1770c689773d', null, null, null, '2015-11-25 10:31:42', '2015-11-25 10:36:19');
INSERT INTO `sys_roles_proceedings` VALUES ('5655314c-f4f4-44b8-b958-0ffcc689773d', '5653c30f-c7ac-4678-9eb1-160cc689773d', '55f2e9f0-66ec-4b3e-929c-1a40c689773d', '1', null, null, '2015-11-25 10:55:56', '2015-11-25 10:56:55');
INSERT INTO `sys_roles_proceedings` VALUES ('565807ed-3e10-418a-901c-1b18c689773d', '5653c30f-c7ac-4678-9eb1-160cc689773d', '55f2e9f0-894c-4eef-88c3-1a40c689773d', '1', null, null, '2015-11-27 14:36:13', '2015-11-27 14:36:13');
INSERT INTO `sys_roles_proceedings` VALUES ('56580824-34b4-4469-be36-15e8c689773d', '5653c30f-c7ac-4678-9eb1-160cc689773d', '55f2e9f0-a184-4208-a606-1a40c689773d', '1', null, null, '2015-11-27 14:37:08', '2015-11-27 14:37:08');
INSERT INTO `sys_roles_proceedings` VALUES ('565bf0b4-ab5c-4e47-b46a-12dcc689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2fafc-a31c-4898-92ad-1234c689773d', '1', null, null, '2015-11-30 13:46:12', '2015-11-30 13:46:12');
INSERT INTO `sys_roles_proceedings` VALUES ('565bf0c4-2de0-4297-81df-056cc689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2fafc-c388-4b93-88f8-1234c689773d', '1', null, null, '2015-11-30 13:46:28', '2015-11-30 13:46:28');
INSERT INTO `sys_roles_proceedings` VALUES ('565bf0c9-af0c-4320-a36d-18a0c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2fafc-dcec-45cc-bc18-1234c689773d', '1', null, null, '2015-11-30 13:46:33', '2015-11-30 13:46:33');
INSERT INTO `sys_roles_proceedings` VALUES ('565bf0ce-3578-496d-96f6-14b4c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2fafc-f524-46cc-bb2a-1234c689773d', '1', null, null, '2015-11-30 13:46:38', '2015-11-30 13:46:38');
INSERT INTO `sys_roles_proceedings` VALUES ('565bf0d4-056c-405b-8b0a-1b24c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2fafc-0c30-4331-9be0-1234c689773d', '1', null, null, '2015-11-30 13:46:44', '2015-11-30 13:46:44');
INSERT INTO `sys_roles_proceedings` VALUES ('565bf0d8-39ec-4f90-a15a-0ee4c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2f73d-4a80-450b-aaa3-051cc689773d', '1', null, null, '2015-11-30 13:46:48', '2015-11-30 13:46:48');
INSERT INTO `sys_roles_proceedings` VALUES ('565bf0dd-66bc-4dfb-8a87-0d80c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2f73d-32ac-4eda-b56e-051cc689773d', '1', null, null, '2015-11-30 13:46:53', '2015-11-30 13:46:53');
INSERT INTO `sys_roles_proceedings` VALUES ('565bf0e1-5d2c-4e2b-ac97-12dcc689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2f73d-1b3c-4db1-8cd8-051cc689773d', '1', null, null, '2015-11-30 13:46:57', '2015-11-30 13:46:57');
INSERT INTO `sys_roles_proceedings` VALUES ('565bf0e4-af88-464c-b93b-056cc689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '55f2f73d-01d8-4512-ac89-051cc689773d', '1', null, null, '2015-11-30 13:47:00', '2015-11-30 13:47:00');
INSERT INTO `sys_roles_proceedings` VALUES ('565bf107-f2c8-4e05-a296-0ee4c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '565bf0f9-9a90-463f-ae98-14b4c689773d', '1', null, null, '2015-11-30 13:47:35', '2015-11-30 13:47:35');
INSERT INTO `sys_roles_proceedings` VALUES ('565bf325-d1ec-444f-b743-18a0c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '565bf303-8528-4bcc-be48-14b4c689773d', '1', null, null, '2015-11-30 13:56:37', '2015-11-30 13:56:37');
INSERT INTO `sys_roles_proceedings` VALUES ('565bf32a-3d84-4134-b968-14b4c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '565bf308-f5f0-474a-b25f-1b24c689773d', '1', null, null, '2015-11-30 13:56:42', '2015-11-30 13:56:42');
INSERT INTO `sys_roles_proceedings` VALUES ('565bf32f-7f24-432b-8767-1b24c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '565bf30e-1e0c-457d-91d7-0ee4c689773d', '1', null, null, '2015-11-30 13:56:47', '2015-11-30 13:56:47');
INSERT INTO `sys_roles_proceedings` VALUES ('565bf334-6e3c-4cd6-aca4-0ee4c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '565bf313-9c88-4602-9687-0d80c689773d', '1', null, null, '2015-11-30 13:56:52', '2015-11-30 13:56:52');
INSERT INTO `sys_roles_proceedings` VALUES ('565bf339-73b8-44c1-a57a-0d80c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '565bf318-555c-433d-a4ef-12dcc689773d', '1', null, null, '2015-11-30 13:56:57', '2015-11-30 13:56:57');
INSERT INTO `sys_roles_proceedings` VALUES ('565c0239-b38c-4272-84f4-0ee4c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '565c0210-534c-4fc3-b712-0d80c689773d', '1', null, null, '2015-11-30 15:00:57', '2015-11-30 15:00:57');
INSERT INTO `sys_roles_proceedings` VALUES ('565c023e-d9d8-4369-bf8d-0d80c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '565c0217-d5e4-4f0c-9e9c-12dcc689773d', '1', null, null, '2015-11-30 15:01:02', '2015-11-30 15:01:02');
INSERT INTO `sys_roles_proceedings` VALUES ('565c0243-94e4-47dd-a14b-12dcc689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '565c021d-e730-4dcf-afdf-056cc689773d', '1', null, null, '2015-11-30 15:01:07', '2015-11-30 15:01:07');
INSERT INTO `sys_roles_proceedings` VALUES ('565c0248-bc64-4259-9d3e-056cc689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '565c0223-ae5c-4297-a849-18a0c689773d', '1', null, null, '2015-11-30 15:01:12', '2015-11-30 15:01:12');
INSERT INTO `sys_roles_proceedings` VALUES ('565c024d-e854-46eb-bac3-18a0c689773d', '55f27924-fef8-4337-bf8e-1760c689773d', '565c0229-f414-44f9-98ac-14b4c689773d', '1', null, null, '2015-11-30 15:01:17', '2015-11-30 15:01:17');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a5fa-5044-437d-8ebe-17e0c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '55f1c6b6-c304-47cc-8116-1770c689773d', '1', null, null, '2015-12-04 21:40:58', '2015-12-04 21:40:58');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a5fe-2574-47c9-bcf6-111cc689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '55f1ca15-a1e8-44e3-bde6-177cc689773d', '1', null, null, '2015-12-04 21:41:02', '2015-12-04 21:41:02');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a602-fc50-4d69-9aa8-06d8c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '55f2226b-4af8-4962-9a73-1710c689773d', '1', null, null, '2015-12-04 21:41:06', '2015-12-04 21:41:06');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a605-6134-4b3b-8b8d-17e0c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '55f2226b-6bc8-4ccc-96af-1710c689773d', '1', null, null, '2015-12-04 21:41:09', '2015-12-04 21:41:09');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a609-cda4-41b3-921e-111cc689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '55f2226b-84c8-4c5d-8a67-1710c689773d', '1', null, null, '2015-12-04 21:41:13', '2015-12-04 21:41:13');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a60c-d338-41fa-a55f-06d8c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '55f2226b-9bd4-404f-ae3b-1710c689773d', '1', null, null, '2015-12-04 21:41:16', '2015-12-04 21:41:16');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a612-feb0-49b0-a5c9-17e0c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '55fda9c3-07c8-4afe-b95d-18c0c689773d', '1', null, null, '2015-12-04 21:41:22', '2015-12-04 21:41:22');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a617-cde0-4cb0-8eb5-111cc689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '55f23411-2e98-4b73-ab8c-1710c689773d', '1', null, null, '2015-12-04 21:41:27', '2015-12-04 21:41:27');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a61b-45ec-4438-bf9b-06d8c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '55f23411-5288-4b51-97a8-1710c689773d', '1', null, null, '2015-12-04 21:41:31', '2015-12-04 21:41:31');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a61e-c820-4ee5-821f-17e0c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '55f23411-6c50-44c1-855a-1710c689773d', '1', null, null, '2015-12-04 21:41:34', '2015-12-04 21:41:34');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a622-39d0-4e47-8135-111cc689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '55f23411-8424-4677-a821-1710c689773d', '1', null, null, '2015-12-04 21:41:38', '2015-12-04 21:41:38');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a625-143c-4177-ae72-06d8c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '5624ffee-39b4-48e4-9039-174cc689773d', '1', null, null, '2015-12-04 21:41:41', '2015-12-04 21:41:41');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a62e-d6d4-4fd7-a213-17e0c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '56161cca-5080-436c-958c-04ccc689773d', '1', null, null, '2015-12-04 21:41:50', '2015-12-04 21:41:50');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a635-c0f4-410d-8c01-111cc689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '5629cbf0-da1c-41a9-bccc-1ac8c689773d', '1', null, null, '2015-12-04 21:41:57', '2015-12-04 21:41:57');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a638-887c-4351-b196-06d8c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '5629cbf0-fe0c-4241-9d88-1ac8c689773d', '1', null, null, '2015-12-04 21:42:00', '2015-12-04 21:42:00');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a63c-c5a4-4b72-8e3e-17e0c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '5629cbf0-1838-43b5-b372-1ac8c689773d', '1', null, null, '2015-12-04 21:42:04', '2015-12-04 21:42:04');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a640-f890-4fb3-ae6a-111cc689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '5629cbf0-30d4-4a7e-8ab5-1ac8c689773d', '1', null, null, '2015-12-04 21:42:08', '2015-12-04 21:42:08');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a644-0ad8-4af5-99fe-06d8c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '5629cbf0-48a8-427f-8ffc-1ac8c689773d', '1', null, null, '2015-12-04 21:42:12', '2015-12-04 21:42:12');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a64c-5a14-40c2-add5-17e0c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '565bf0f9-9a90-463f-ae98-14b4c689773d', '1', null, null, '2015-12-04 21:42:20', '2015-12-06 00:49:40');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a652-8444-443f-8232-111cc689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '565bf303-8528-4bcc-be48-14b4c689773d', '1', null, null, '2015-12-04 21:42:26', '2015-12-04 21:42:26');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a657-c154-490d-8653-06d8c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '565bf308-f5f0-474a-b25f-1b24c689773d', '1', null, null, '2015-12-04 21:42:31', '2015-12-04 21:42:31');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a65c-1230-4e0f-b2ca-17e0c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '565bf30e-1e0c-457d-91d7-0ee4c689773d', '1', null, null, '2015-12-04 21:42:36', '2015-12-04 21:42:36');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a661-a714-4cd9-bf76-111cc689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '565bf313-9c88-4602-9687-0d80c689773d', '1', null, null, '2015-12-04 21:42:41', '2015-12-04 21:42:41');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a666-2494-40ed-85ed-06d8c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '565bf318-555c-433d-a4ef-12dcc689773d', '1', null, null, '2015-12-04 21:42:46', '2015-12-04 21:42:46');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a66b-fc9c-4c6e-961e-17e0c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '565c0210-534c-4fc3-b712-0d80c689773d', '1', null, null, '2015-12-04 21:42:51', '2015-12-04 21:42:51');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a670-3d84-41ec-8b9b-111cc689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '565c0217-d5e4-4f0c-9e9c-12dcc689773d', '1', null, null, '2015-12-04 21:42:56', '2015-12-04 21:42:56');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a676-ba84-40c5-846e-06d8c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '565c021d-e730-4dcf-afdf-056cc689773d', '1', null, null, '2015-12-04 21:43:02', '2015-12-04 21:43:02');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a67b-f0a4-4220-a1a6-17e0c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '565c0223-ae5c-4297-a849-18a0c689773d', '1', null, null, '2015-12-04 21:43:07', '2015-12-04 21:43:07');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a67f-a93c-44af-85fb-111cc689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '565c0229-f414-44f9-98ac-14b4c689773d', '1', null, null, '2015-12-04 21:43:11', '2015-12-04 21:43:11');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a684-09b4-4ea7-b029-06d8c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '5661a59a-2f08-4e7a-8372-17e0c689773d', '1', null, null, '2015-12-04 21:43:16', '2015-12-04 21:43:16');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a688-3380-435a-add6-17e0c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '5661a59a-6cc0-4989-b814-17e0c689773d', '1', null, null, '2015-12-04 21:43:20', '2015-12-04 21:43:20');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a68d-f1a4-40a0-b39a-111cc689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '5661a59a-9948-4912-aff8-17e0c689773d', '1', null, null, '2015-12-04 21:43:25', '2015-12-04 21:43:25');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a691-803c-45fe-bc16-06d8c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '5661a59a-c508-4520-8044-17e0c689773d', '1', null, null, '2015-12-04 21:43:29', '2015-12-04 21:43:29');
INSERT INTO `sys_roles_proceedings` VALUES ('5661a696-8614-460a-b531-17e0c689773d', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '5661a59a-ef9c-42de-a8f1-17e0c689773d', '1', null, null, '2015-12-04 21:43:34', '2015-12-04 21:43:34');
INSERT INTO `sys_roles_proceedings` VALUES ('5661b4ac-b788-4468-9782-17e0ded5206b', '55f27924-fef8-4337-bf8e-1760c689773d', '5661a59a-2f08-4e7a-8372-17e0c689773d', '1', null, null, '2015-12-04 22:43:40', '2015-12-04 22:43:40');
INSERT INTO `sys_roles_proceedings` VALUES ('5661b4b1-e524-49c6-aa7f-111cded5206b', '55f27924-fef8-4337-bf8e-1760c689773d', '5661a59a-6cc0-4989-b814-17e0c689773d', '1', null, null, '2015-12-04 22:43:45', '2015-12-04 22:43:45');
INSERT INTO `sys_roles_proceedings` VALUES ('5661b4b5-8d9c-4967-ba04-06d8ded5206b', '55f27924-fef8-4337-bf8e-1760c689773d', '5661a59a-9948-4912-aff8-17e0c689773d', '1', null, null, '2015-12-04 22:43:49', '2015-12-04 22:43:49');
INSERT INTO `sys_roles_proceedings` VALUES ('5661b4bb-af58-4431-af45-17e0ded5206b', '55f27924-fef8-4337-bf8e-1760c689773d', '5661a59a-c508-4520-8044-17e0c689773d', '1', null, null, '2015-12-04 22:43:55', '2015-12-04 22:43:55');
INSERT INTO `sys_roles_proceedings` VALUES ('5661b4c0-6078-45f2-b223-111cded5206b', '55f27924-fef8-4337-bf8e-1760c689773d', '5661a59a-ef9c-42de-a8f1-17e0c689773d', '1', null, null, '2015-12-04 22:44:00', '2015-12-05 03:01:29');
INSERT INTO `sys_roles_proceedings` VALUES ('566b826d-8c20-4747-ab2c-08b8ded5206b', '55f27924-fef8-4337-bf8e-1760c689773d', '566b81d5-c9b0-46ce-aef0-0534ded5206b', '1', null, null, '2015-12-12 09:11:57', '2015-12-12 09:11:57');
INSERT INTO `sys_roles_proceedings` VALUES ('566b8272-eb48-4046-82ff-0534ded5206b', '55f27924-fef8-4337-bf8e-1760c689773d', '566b81d5-ecd8-4c0b-a6d3-0534ded5206b', '1', null, null, '2015-12-12 09:12:02', '2015-12-12 09:12:02');
INSERT INTO `sys_roles_proceedings` VALUES ('566b8279-0058-41a8-aadb-15b8ded5206b', '55f27924-fef8-4337-bf8e-1760c689773d', '566b81d5-0574-4673-af04-0534ded5206b', '1', null, null, '2015-12-12 09:12:09', '2015-12-12 09:12:09');
INSERT INTO `sys_roles_proceedings` VALUES ('566b827f-8864-4f7d-a25d-08b8ded5206b', '55f27924-fef8-4337-bf8e-1760c689773d', '566b81d5-1d48-4a77-8df8-0534ded5206b', '1', null, null, '2015-12-12 09:12:15', '2015-12-12 09:12:15');
INSERT INTO `sys_roles_proceedings` VALUES ('566b8285-c584-42e1-80c2-0534ded5206b', '55f27924-fef8-4337-bf8e-1760c689773d', '566b81d5-33f0-45ab-b00c-0534ded5206b', '1', null, null, '2015-12-12 09:12:21', '2015-12-12 09:12:21');
INSERT INTO `sys_roles_proceedings` VALUES ('566b82a3-f034-46c4-843e-08b8ded5206b', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '566b81d5-c9b0-46ce-aef0-0534ded5206b', '1', null, null, '2015-12-12 09:12:51', '2015-12-12 09:12:51');
INSERT INTO `sys_roles_proceedings` VALUES ('566b82aa-5b84-49df-af07-0534ded5206b', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '566b81d5-ecd8-4c0b-a6d3-0534ded5206b', '1', null, null, '2015-12-12 09:12:58', '2015-12-12 09:12:58');
INSERT INTO `sys_roles_proceedings` VALUES ('566b82af-0270-4043-8c14-15b8ded5206b', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '566b81d5-0574-4673-af04-0534ded5206b', '1', null, null, '2015-12-12 09:13:03', '2015-12-12 09:13:03');
INSERT INTO `sys_roles_proceedings` VALUES ('566b82b5-f044-4ddc-8163-08b8ded5206b', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '566b81d5-1d48-4a77-8df8-0534ded5206b', '1', null, null, '2015-12-12 09:13:09', '2015-12-12 09:13:09');
INSERT INTO `sys_roles_proceedings` VALUES ('566b82ba-4b68-450d-a053-0534ded5206b', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '566b81d5-33f0-45ab-b00c-0534ded5206b', '1', null, null, '2015-12-12 09:13:14', '2015-12-12 09:13:14');
INSERT INTO `sys_roles_proceedings` VALUES ('5670fd2a-78a0-4d48-8b49-4d12c0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '5670fd13-8520-40d2-af4f-4abcc0b9c32c', '1', null, null, '2015-12-15 23:56:58', '2015-12-15 23:56:58');
INSERT INTO `sys_roles_proceedings` VALUES ('5670fdcb-0410-42b0-b35b-4360c0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '5670fdb2-ab9c-43ab-a321-496ac0b9c32c', '1', null, null, '2015-12-15 23:59:39', '2015-12-15 23:59:39');
INSERT INTO `sys_roles_proceedings` VALUES ('5670fdd2-a074-4fb6-ab7e-4825c0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '5670fdb2-c8e8-4ed8-9ca2-4471c0b9c32c', '1', null, null, '2015-12-15 23:59:46', '2015-12-15 23:59:46');
INSERT INTO `sys_roles_proceedings` VALUES ('5670fddc-5d0c-4260-9f2f-4830c0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '5670fdb2-d11c-4018-94c5-491cc0b9c32c', '1', null, null, '2015-12-15 23:59:56', '2015-12-15 23:59:56');
INSERT INTO `sys_roles_proceedings` VALUES ('5670fde4-e61c-4cd7-a20c-44ecc0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '5670fdb2-d824-4e29-867e-4c5bc0b9c32c', '1', null, null, '2015-12-16 00:00:04', '2015-12-16 00:00:19');
INSERT INTO `sys_roles_proceedings` VALUES ('5670fe03-91e4-4997-bdf1-49dbc0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '5670fdb2-df2c-4577-99ad-4d13c0b9c32c', '1', null, null, '2015-12-16 00:00:35', '2015-12-16 00:00:35');
INSERT INTO `sys_roles_proceedings` VALUES ('5675bccb-af00-4f4c-9972-494cc0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '5675bcbe-b704-45e9-abe0-4500c0b9c32c', null, null, null, '2015-12-19 14:23:39', '2016-02-08 13:54:47');
INSERT INTO `sys_roles_proceedings` VALUES ('5675bdde-44c0-4085-b830-4eacc0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '5675bdd1-5380-4987-bf8f-48d5c0b9c32c', '1', null, null, '2015-12-19 14:28:14', '2015-12-19 14:28:14');
INSERT INTO `sys_roles_proceedings` VALUES ('5675bde3-c14c-4070-9f44-42d7c0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '5675bdd1-6c1c-4083-9ab0-499fc0b9c32c', '1', null, null, '2015-12-19 14:28:19', '2015-12-19 14:28:19');
INSERT INTO `sys_roles_proceedings` VALUES ('5675bde9-64f8-4ae4-924a-4b53c0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '5675bdd1-7324-4fa6-b4ea-4b76c0b9c32c', '1', null, null, '2015-12-19 14:28:25', '2015-12-19 14:28:25');
INSERT INTO `sys_roles_proceedings` VALUES ('5675bdee-c934-4fb9-83b3-46d7c0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '5675bdd1-79c8-4b70-97b2-49d3c0b9c32c', '1', null, null, '2015-12-19 14:28:30', '2015-12-19 14:28:30');
INSERT INTO `sys_roles_proceedings` VALUES ('5675bdf3-a260-4dff-ab94-4113c0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '5675bdd1-8008-4767-b71c-4467c0b9c32c', '1', null, null, '2015-12-19 14:28:35', '2015-12-19 14:28:35');
INSERT INTO `sys_roles_proceedings` VALUES ('568a681f-ccf0-465d-817d-ff02c0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '568a6809-81b4-4d60-bdfa-fce9c0b9c32c', '1', null, null, '2016-01-04 06:39:59', '2016-01-04 06:39:59');
INSERT INTO `sys_roles_proceedings` VALUES ('568a6825-1cec-4c97-96c7-481cc0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '568a6809-9e9c-47b4-a2dd-fce9c0b9c32c', '1', null, null, '2016-01-04 06:40:05', '2016-01-04 06:40:05');
INSERT INTO `sys_roles_proceedings` VALUES ('568a6846-84f8-4685-a43a-4672c0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '568a6809-a5a4-4050-97ef-fce9c0b9c32c', '1', null, null, '2016-01-04 06:40:38', '2016-01-04 06:40:38');
INSERT INTO `sys_roles_proceedings` VALUES ('568a684d-b1fc-4043-b51b-4915c0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '568a6809-abe4-4ee0-998b-fce9c0b9c32c', '1', null, null, '2016-01-04 06:40:45', '2016-01-04 06:40:45');
INSERT INTO `sys_roles_proceedings` VALUES ('568a6854-2b9c-4305-971d-42acc0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '568a6809-b224-4d48-b627-fce9c0b9c32c', '1', null, null, '2016-01-04 06:40:52', '2016-01-04 06:40:52');
INSERT INTO `sys_roles_proceedings` VALUES ('568a6b9c-81e0-4a4f-ab44-4c0fc0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '568a6a85-274c-4ffa-8f16-42dfc0b9c32c', '1', null, null, '2016-01-04 06:54:52', '2016-01-04 06:54:52');
INSERT INTO `sys_roles_proceedings` VALUES ('568a6ba3-1fb4-49e1-b681-419dc0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '568a6a8d-82a4-4622-b551-41f6c0b9c32c', '1', null, null, '2016-01-04 06:54:59', '2016-01-04 06:54:59');
INSERT INTO `sys_roles_proceedings` VALUES ('568a6bab-1ef4-4976-a7c9-4726c0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '568a6a94-a640-499e-9caf-4ad6c0b9c32c', '1', null, null, '2016-01-04 06:55:07', '2016-01-04 06:55:07');
INSERT INTO `sys_roles_proceedings` VALUES ('568a6bb2-006c-40ad-8cbe-46fdc0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '568a6a9c-5438-4f47-8db5-477bc0b9c32c', '1', null, null, '2016-01-04 06:55:14', '2016-01-04 06:55:14');
INSERT INTO `sys_roles_proceedings` VALUES ('568a6bb9-2114-4e82-a436-491fc0b9c32c', '55f27924-fef8-4337-bf8e-1760c689773d', '568a6aa4-78b4-4e85-8911-4d71c0b9c32c', '1', null, null, '2016-01-04 06:55:21', '2016-01-04 06:55:21');
INSERT INTO `sys_roles_proceedings` VALUES ('56a3617d-0df8-4ece-8918-12c8ded5206b', '55f27924-fef8-4337-bf8e-1760c689773d', '56a360b6-6a2c-4f27-ab37-0384ded5206b', '1', null, null, '2016-01-23 18:18:21', '2016-01-23 18:18:21');
INSERT INTO `sys_roles_proceedings` VALUES ('56a36184-7260-4161-818c-0fe0ded5206b', '55f27924-fef8-4337-bf8e-1760c689773d', '56a36157-083c-4b9f-ad03-06c4ded5206b', '1', null, null, '2016-01-23 18:18:28', '2016-01-23 18:18:28');
INSERT INTO `sys_roles_proceedings` VALUES ('56a3618b-50a4-4955-99c7-14f0ded5206b', '55f27924-fef8-4337-bf8e-1760c689773d', '56a36157-083c-46b3-a781-06c4ded5206b', '1', null, null, '2016-01-23 18:18:35', '2016-01-23 18:18:35');
INSERT INTO `sys_roles_proceedings` VALUES ('56a36192-947c-45d5-bd60-1584ded5206b', '55f27924-fef8-4337-bf8e-1760c689773d', '56a36157-083c-49a7-827b-06c4ded5206b', '1', null, null, '2016-01-23 18:18:42', '2016-01-23 18:18:42');
INSERT INTO `sys_roles_proceedings` VALUES ('56a36198-49c8-4e9b-bf0c-0384ded5206b', '55f27924-fef8-4337-bf8e-1760c689773d', '56a36157-8edc-431c-af72-06c4ded5206b', '1', null, null, '2016-01-23 18:18:48', '2016-01-23 18:18:48');
INSERT INTO `sys_roles_proceedings` VALUES ('56b506d9-bce0-405a-a8cf-93e6671fe029', '56b5068c-cc38-43b4-b690-9016671fe029', '56b506ad-3f28-4e31-b92d-91ae671fe029', '1', null, null, '2016-02-06 03:32:25', '2016-02-06 03:32:25');
INSERT INTO `sys_roles_proceedings` VALUES ('56b506ee-18a8-449f-b5c0-98a5671fe029', '56b5068c-cc38-43b4-b690-9016671fe029', '565bf303-8528-4bcc-be48-14b4c689773d', '1', null, null, '2016-02-06 03:32:46', '2016-02-06 03:32:46');
INSERT INTO `sys_roles_proceedings` VALUES ('56b506f6-0740-4b90-99d1-98c4671fe029', '56b5068c-cc38-43b4-b690-9016671fe029', '565bf308-f5f0-474a-b25f-1b24c689773d', '1', null, null, '2016-02-06 03:32:54', '2016-02-06 03:32:54');
INSERT INTO `sys_roles_proceedings` VALUES ('56b506fc-f82c-42f5-b279-98e7671fe029', '56b5068c-cc38-43b4-b690-9016671fe029', '565bf30e-1e0c-457d-91d7-0ee4c689773d', '1', null, null, '2016-02-06 03:33:00', '2016-02-06 03:33:00');
INSERT INTO `sys_roles_proceedings` VALUES ('56b50703-6870-4164-8942-9904671fe029', '56b5068c-cc38-43b4-b690-9016671fe029', '565bf313-9c88-4602-9687-0d80c689773d', '1', null, null, '2016-02-06 03:33:07', '2016-02-06 03:33:07');
INSERT INTO `sys_roles_proceedings` VALUES ('56b50709-6b68-4114-a92b-9911671fe029', '56b5068c-cc38-43b4-b690-9016671fe029', '565bf318-555c-433d-a4ef-12dcc689773d', '1', null, null, '2016-02-06 03:33:13', '2016-02-06 03:33:13');
INSERT INTO `sys_roles_proceedings` VALUES ('56b515d8-745c-4098-a6b2-5296671fe029', '56b5068c-cc38-43b4-b690-9016671fe029', '565bf0f9-9a90-463f-ae98-14b4c689773d', '1', null, null, '2016-02-06 04:36:24', '2016-02-06 04:36:24');
INSERT INTO `sys_roles_proceedings` VALUES ('56b51cfd-323c-4115-9ee1-a362671fe029', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '56a360b6-6a2c-4f27-ab37-0384ded5206b', '1', null, null, '2016-02-06 05:06:53', '2016-02-06 05:06:53');
INSERT INTO `sys_roles_proceedings` VALUES ('56b51d02-261c-41df-b309-a36c671fe029', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '56a36157-083c-4b9f-ad03-06c4ded5206b', '1', null, null, '2016-02-06 05:06:58', '2016-02-06 05:06:58');
INSERT INTO `sys_roles_proceedings` VALUES ('56b51d08-f770-4290-8c28-a383671fe029', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '56a36157-083c-46b3-a781-06c4ded5206b', '1', null, null, '2016-02-06 05:07:04', '2016-02-06 05:07:04');
INSERT INTO `sys_roles_proceedings` VALUES ('56b51d0d-5de8-485f-b163-a3c1671fe029', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '56a36157-083c-49a7-827b-06c4ded5206b', '1', null, null, '2016-02-06 05:07:09', '2016-02-06 05:07:09');
INSERT INTO `sys_roles_proceedings` VALUES ('56b51d13-e3f4-4652-ac5d-a3df671fe029', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '56a36157-8edc-431c-af72-06c4ded5206b', '1', null, null, '2016-02-06 05:07:15', '2016-02-06 05:07:15');
INSERT INTO `sys_roles_proceedings` VALUES ('56b51d1f-0ae0-4522-9a0b-a891671fe029', '5661a5e8-aa48-4b99-bd92-17e0c689773d', '56b506ad-3f28-4e31-b92d-91ae671fe029', '1', null, null, '2016-02-06 05:07:27', '2016-02-06 05:07:27');
INSERT INTO `sys_roles_proceedings` VALUES ('56b89a28-b95c-41ba-acf8-fb9f671fe029', '55f27924-fef8-4337-bf8e-1760c689773d', '56b8393d-ea14-40cd-af28-ca7e671fe029', '1', null, null, '2016-02-08 20:37:44', '2016-02-08 20:37:44');
INSERT INTO `sys_roles_proceedings` VALUES ('56b89a43-57c0-445a-b156-0149671fe029', '55f27924-fef8-4337-bf8e-1760c689773d', '56b8393d-0b48-46f8-aa62-ca7e671fe029', '1', null, null, '2016-02-08 20:38:11', '2016-02-08 20:38:11');
INSERT INTO `sys_roles_proceedings` VALUES ('56b89a57-afd0-43cd-9f9c-018b671fe029', '55f27924-fef8-4337-bf8e-1760c689773d', '56b8393d-1250-4d2f-b35e-ca7e671fe029', '1', null, null, '2016-02-08 20:38:31', '2016-02-08 20:38:31');
INSERT INTO `sys_roles_proceedings` VALUES ('56b89a73-ca64-482e-96f2-01d8671fe029', '55f27924-fef8-4337-bf8e-1760c689773d', '56b8393d-1ffc-4eb3-91f5-ca7e671fe029', '1', null, null, '2016-02-08 20:38:59', '2016-02-08 20:38:59');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7ed37-6490-4d7c-9bcd-4372671fe029', '56e7ed0a-08dc-45c2-8b6b-41c1671fe029', '5675bdd1-5380-4987-bf8f-48d5c0b9c32c', '1', null, null, '2016-03-15 18:08:39', '2016-03-15 18:08:39');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7ed3e-5408-4386-a937-481d671fe029', '56e7ed0a-08dc-45c2-8b6b-41c1671fe029', '5675bdd1-6c1c-4083-9ab0-499fc0b9c32c', '1', null, null, '2016-03-15 18:08:46', '2016-03-15 18:08:46');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7ed48-dba0-44fc-a651-48b4671fe029', '56e7ed0a-08dc-45c2-8b6b-41c1671fe029', '5675bdd1-7324-4fa6-b4ea-4b76c0b9c32c', '1', null, null, '2016-03-15 18:08:56', '2016-03-15 18:08:56');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7ed4f-8898-4189-a521-492f671fe029', '56e7ed0a-08dc-45c2-8b6b-41c1671fe029', '5675bdd1-79c8-4b70-97b2-49d3c0b9c32c', '1', null, null, '2016-03-15 18:09:03', '2016-03-15 18:09:03');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7ed56-6adc-4f9f-8d8d-498c671fe029', '56e7ed0a-08dc-45c2-8b6b-41c1671fe029', '5675bdd1-8008-4767-b71c-4467c0b9c32c', '1', null, null, '2016-03-15 18:09:10', '2016-03-15 18:09:10');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7ed7c-cb30-4d28-bf83-4a1f671fe029', '56e7ed0a-08dc-45c2-8b6b-41c1671fe029', '56b8393d-ea14-40cd-af28-ca7e671fe029', '1', null, null, '2016-03-15 18:09:48', '2016-03-15 18:09:48');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7ed82-d148-42aa-b67c-4a37671fe029', '56e7ed0a-08dc-45c2-8b6b-41c1671fe029', '56b8393d-0b48-46f8-aa62-ca7e671fe029', '1', null, null, '2016-03-15 18:09:54', '2016-03-15 18:09:54');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7ed87-f5e4-4f14-a333-4a3d671fe029', '56e7ed0a-08dc-45c2-8b6b-41c1671fe029', '56b8393d-1250-4d2f-b35e-ca7e671fe029', '1', null, null, '2016-03-15 18:09:59', '2016-03-15 18:09:59');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7ed8c-5908-4f99-8e82-4ac3671fe029', '56e7ed0a-08dc-45c2-8b6b-41c1671fe029', '56b8393d-1958-4ba2-a5c3-ca7e671fe029', '1', null, null, '2016-03-15 18:10:04', '2016-03-15 18:10:04');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7ed91-d020-4fba-95eb-4adf671fe029', '56e7ed0a-08dc-45c2-8b6b-41c1671fe029', '56b8393d-1ffc-4eb3-91f5-ca7e671fe029', '1', null, null, '2016-03-15 18:10:09', '2016-03-15 18:10:09');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7f0c8-26bc-4537-bfa8-991f671fe029', '56e7f02e-276c-430f-960d-8bab671fe029', '566b81d5-c9b0-46ce-aef0-0534ded5206b', '1', null, null, '2016-03-15 18:23:52', '2016-03-15 18:23:52');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7f0d0-ea50-49b3-a94a-9955671fe029', '56e7f02e-276c-430f-960d-8bab671fe029', '566b81d5-ecd8-4c0b-a6d3-0534ded5206b', '1', null, null, '2016-03-15 18:24:00', '2016-03-15 18:24:00');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7f0d9-4cf0-4a5e-ac2f-9989671fe029', '56e7f02e-276c-430f-960d-8bab671fe029', '566b81d5-0574-4673-af04-0534ded5206b', '1', null, null, '2016-03-15 18:24:09', '2016-03-15 18:24:09');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7f0e1-2164-412b-a7c1-9999671fe029', '56e7f02e-276c-430f-960d-8bab671fe029', '566b81d5-1d48-4a77-8df8-0534ded5206b', '1', null, null, '2016-03-15 18:24:17', '2016-03-15 18:24:17');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7f0e8-adfc-4ff3-9ac6-99c9671fe029', '56e7f02e-276c-430f-960d-8bab671fe029', '566b81d5-33f0-45ab-b00c-0534ded5206b', '1', null, null, '2016-03-15 18:24:24', '2016-03-15 18:24:24');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7f113-4c48-48a3-ab79-9a86671fe029', '56e7f02e-276c-430f-960d-8bab671fe029', '565bf303-8528-4bcc-be48-14b4c689773d', '1', null, null, '2016-03-15 18:25:07', '2016-03-15 18:25:07');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7f11c-1b70-4ef6-b852-9aa3671fe029', '56e7f02e-276c-430f-960d-8bab671fe029', '565bf308-f5f0-474a-b25f-1b24c689773d', '1', null, null, '2016-03-15 18:25:16', '2016-03-15 18:25:16');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7f123-5998-4d2b-b268-9abc671fe029', '56e7f02e-276c-430f-960d-8bab671fe029', '565bf30e-1e0c-457d-91d7-0ee4c689773d', '1', null, null, '2016-03-15 18:25:23', '2016-03-15 18:25:23');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7f12a-0108-4aea-a5e0-9ad4671fe029', '56e7f02e-276c-430f-960d-8bab671fe029', '565bf313-9c88-4602-9687-0d80c689773d', '1', null, null, '2016-03-15 18:25:30', '2016-03-15 18:25:30');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7f131-21bc-49c4-be03-9ae3671fe029', '56e7f02e-276c-430f-960d-8bab671fe029', '565bf318-555c-433d-a4ef-12dcc689773d', '1', null, null, '2016-03-15 18:25:37', '2016-03-15 18:25:37');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7f186-f1bc-4a4e-a08b-a1ce671fe029', '56b5068c-cc38-43b4-b690-9016671fe029', '5675bdd1-5380-4987-bf8f-48d5c0b9c32c', null, null, null, '2016-03-15 18:27:02', '2016-03-15 18:27:10');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7f196-a7ec-4cb4-b7ca-a228671fe029', '56b5068c-cc38-43b4-b690-9016671fe029', '56a360b6-6a2c-4f27-ab37-0384ded5206b', '1', null, null, '2016-03-15 18:27:18', '2016-03-15 18:27:18');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7f19f-4170-4b18-ac24-a277671fe029', '56b5068c-cc38-43b4-b690-9016671fe029', '56a36157-083c-4b9f-ad03-06c4ded5206b', '1', null, null, '2016-03-15 18:27:27', '2016-03-15 18:27:27');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7f1a8-d268-4334-ab23-a290671fe029', '56b5068c-cc38-43b4-b690-9016671fe029', '56a36157-083c-46b3-a781-06c4ded5206b', '1', null, null, '2016-03-15 18:27:36', '2016-03-15 18:27:36');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7f1b2-3a70-4a1e-8c85-a29f671fe029', '56b5068c-cc38-43b4-b690-9016671fe029', '56a36157-083c-49a7-827b-06c4ded5206b', '1', null, null, '2016-03-15 18:27:46', '2016-03-15 18:27:46');
INSERT INTO `sys_roles_proceedings` VALUES ('56e7f1b9-c1e8-4f3c-a705-a2b9671fe029', '56b5068c-cc38-43b4-b690-9016671fe029', '56a36157-8edc-431c-af72-06c4ded5206b', '1', null, null, '2016-03-15 18:27:53', '2016-03-15 18:27:53');
INSERT INTO `sys_roles_proceedings` VALUES ('5e9d5d0e-0e90-4801-aefe-2d81a747c35f', '5e9d5c0c-0a70-4cdc-94a4-2b0ca747c35f', '5670fd13-8520-40d2-af4f-4abcc0b9c32c', '1', null, null, '2020-04-20 08:27:58', '2020-04-20 08:27:58');
INSERT INTO `sys_roles_proceedings` VALUES ('5e9d5d26-c44c-4189-b626-2d92a747c35f', '5e9d5c0c-0a70-4cdc-94a4-2b0ca747c35f', '5670fdb2-c8e8-4ed8-9ca2-4471c0b9c32c', null, null, null, '2020-04-20 08:28:22', '2020-04-20 08:30:47');
INSERT INTO `sys_roles_proceedings` VALUES ('5e9d5d2f-76bc-4a32-8621-2d80a747c35f', '5e9d5c0c-0a70-4cdc-94a4-2b0ca747c35f', '5670fdb2-ab9c-43ab-a321-496ac0b9c32c', null, null, null, '2020-04-20 08:28:31', '2020-04-20 08:30:44');
INSERT INTO `sys_roles_proceedings` VALUES ('5e9d5dca-405c-4313-900f-2e44a747c35f', '5e9d5c0c-0a70-4cdc-94a4-2b0ca747c35f', '56b8393d-ea14-40cd-af28-ca7e671fe029', '1', null, null, '2020-04-20 08:31:06', '2020-04-20 08:31:06');
INSERT INTO `sys_roles_proceedings` VALUES ('5e9d5dd0-d388-4272-a677-2e44a747c35f', '5e9d5c0c-0a70-4cdc-94a4-2b0ca747c35f', '56b8393d-0b48-46f8-aa62-ca7e671fe029', '1', null, null, '2020-04-20 08:31:12', '2020-04-20 08:31:12');
INSERT INTO `sys_roles_proceedings` VALUES ('8ce8faab-a6f4-429c-a4d1-c103238dd64b', '55f27924-fef8-4337-bf8e-1760c689773d', '5f6ac4b5-e088-4ad9-bf5f-0804ded5206b', '1', null, null, '2020-09-23 04:32:40', '2020-09-23 04:32:40');
INSERT INTO `sys_roles_proceedings` VALUES ('d0fb1e10-8277-4b9d-9ba6-bfcdefd8d8c5', '55f27924-fef8-4337-bf8e-1760c689773d', '5f6ac4c2-f52c-4824-8afe-0804ded5206b', '1', null, null, '2020-09-23 04:32:40', '2020-09-23 04:32:40');
