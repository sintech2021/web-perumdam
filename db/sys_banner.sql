/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : perumdamtkr_website

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-10-05 20:05:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `sys_banner`
-- ----------------------------
DROP TABLE IF EXISTS `sys_banner`;
CREATE TABLE `sys_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(150) DEFAULT '',
  `url` varchar(225) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of sys_banner
-- ----------------------------
INSERT INTO `sys_banner` VALUES ('1', '26bwtysqqy2sk0gck4.jpg', '#');
INSERT INTO `sys_banner` VALUES ('2', '1p29oyj2c3lwgwgwgo.jpg', '#');
INSERT INTO `sys_banner` VALUES ('3', 'hj0u3oeievwwwks4wg.jpg', '#');
INSERT INTO `sys_banner` VALUES ('4', 'ftjdcpmhjc8owkc4o.jpg', '#');

-- ----------------------------
-- Table structure for `sys_info_perumdam`
-- ----------------------------
DROP TABLE IF EXISTS `sys_info_perumdam`;
CREATE TABLE `sys_info_perumdam` (
  `id` int(60) NOT NULL AUTO_INCREMENT,
  `path` varchar(225) DEFAULT '',
  `url` varchar(225) DEFAULT NULL,
  `tgl_buat` datetime DEFAULT NULL,
  `tgl_edit` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_info_perumdam
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_running_text`
-- ----------------------------
DROP TABLE IF EXISTS `sys_running_text`;
CREATE TABLE `sys_running_text` (
  `id` int(60) NOT NULL AUTO_INCREMENT,
  `content` text DEFAULT NULL,
  `tgl_buat` datetime DEFAULT NULL,
  `tgl_edit` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_running_text
-- ----------------------------
INSERT INTO `sys_running_text` VALUES ('1', '<p>Selamat datang di Website PERUMDAM Tirta Kerta raharja</p>', null, null);
