<section class="flash-wrapper">
<br>
<div id="flash-<?php echo isset($key) ? h($key) : CakeString::uuid(); ?>" class="alert alert-danger">
    <span class="fa fa-exclamation-triangle"></span> <?php echo h($message) ?>
</div>
    </section>
    