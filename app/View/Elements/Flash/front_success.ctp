<section class="flash-wrapper">
<br>
<div id="flash-<?php echo isset($key) ? h($key) : CakeString::uuid(); ?>" class="alert alert-success">
    <span class="fa fa-check-square-o"></span> <?php echo h($message) ?>
</div>
    </section>
    