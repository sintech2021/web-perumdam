<?php
App::uses('AppHelper', 'View/Helper');

class MyHelper extends AppHelper {
	public function format_dt($dt,$tt)
	{
		setlocale (LC_TIME, 'id_ID');
		$strpubdate = strtotime($dt);
        $postday    = __d('syscm', date("l", $strpubdate));
        $postdate   = date("j", $strpubdate);
        $postmonth  = __d('syscm', date("F", $strpubdate));
        $postyear   = date("Y", $strpubdate);
        $posttime   = isset($tt) ? sprintf('%s ',
                        $tt) : '';
        return $post_date  = sprintf(
                '<div class="postday">%s ' .
                '%s ' .
                '%s ' .
                '%s ' .
                '%s </div>', $postday, $postdate, $postmonth, $postyear, $posttime);
	}
}