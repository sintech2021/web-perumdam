<?php
App::uses('AppHelper', 'View/Helper');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * CakePHP ZeroCilpboardHelper
 * @author ino
 */
class ZeroClipboardHelper extends AppHelper
{

    // Take advantage of other helpers
    public $helpers  = array('Js', 'Html');
    // Check if the ZeroClipboard.js file has been added or not
    public $_script  = false;
    public $_options = array();

    public function __construct(View $View, $settings = array())
    {
        parent::__construct($View, $settings);
        if (!$this->_script) {
            // We don't want to add this every time, it's only needed once
            $this->_script = true;
            $this->Html->script('/zeroclipboard-2.2.0/dist/ZeroClipboard.min',
                    array('inline' => false));
        }
    }

    protected function _setScript($zcoptions)
    {
        $this->Html->scriptStart(array('inline' => false));
        $id = $zcoptions['_id'];
        $var = "Client" . $id;
        ?> 
document.addEventListener("DOMContentLoaded", function(event) { 
    var <?php echo $var; ?> = new ZeroClipboard( document.getElementById("zc-btn-<?php echo $id;?>") );
    <?php echo $var; ?>.on( "ready", function( readyEvent ) {
        // alert( "ZeroClipboard SWF is ready!" );
        <?php echo $var; ?>.on( "aftercopy", function( event ) {
        // `this` === `client`
        // `event.target` === the element that was clicked
            alert("Copied text to clipboard: " + event.data["text/plain"] );
        } );
    } );
});
    <?php
        $this->Html->scriptEnd();
    }

    public function buildZC($zcoptions = array())
    {
        $this->_options = $zcoptions;
        $this->_setScript($this->_options);
    }

    /*
      public function __construct(View $View, $settings = array())
      {
      parent::__construct($View, $settings);
      }

      public function beforeRender($viewFile)
      {

      }

      public function afterRender($viewFile)
      {

      }

      public function beforeLayout($viewLayout)
      {

      }

      public function afterLayout($viewLayout)
      {

      }
     */
}
