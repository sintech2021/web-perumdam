<?php
echo $this->Html->docType('html5');
$page = $this->get('page');
//die($page);
?>
<html lang="en">
    <head>
        <?php echo $this->Html->charset(); ?>
        <?php
        echo $this->Html->meta(null, null,
                array('http-equiv' => 'X-UA-Compatible', 'content' => 'IE=edge'));
        echo $this->Html->meta(null, null,
                array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1'));
        echo $this->Html->meta('description',
                Configure::read('Sysconfig.app.desc'));
        echo $this->Html->meta(null, null,
                array('name' => 'author', 'content' => Configure::read('Sysconfig.author.name')));
        echo $this->Html->meta(
                '/favicon.ico', '/favicon.ico', array('type' => 'icon')
        );
        echo $this->fetch('meta');
        ?>
        <title>
            <?php 
            
            echo isset($browser_menu_title) ? $browser_menu_title : $this->fetch('title'); ?> # <?php echo Configure::read('Sysconfig.app.name') ?>
        </title>

        <!-- Bootstrap core CSS -->

        <?php
        $css_ls = array(
            '../xcrud/plugins/jquery-ui/jquery-ui.min',

            'Sysadmin./bootstrap-3.3.5-dist/css/bootstrap.min',
            'Sysadmin./../node_modules/opensans-npm-webfont/style',
            'Sysadmin./../node_modules/@fortawesome/fontawesome-free/css/all.min',
            'dashboard',
            'Sysadmin./smartmenus-1.0.0/css/sm-core-css',
            'Sysadmin./smartmenus-1.0.0/css/sm-simple/sm-simple-clean',
            'Sysadmin./../admin.css',
        );
        if (isset($enqueued_css)) {
            $css_ls = array_merge($css_ls, $enqueued_css);
        }
        echo $this->Html->css($css_ls);
        echo $this->fetch('css');
        ?>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <?php 
         $script_ls = array(
            'Sysadmin./bootstrap-3.3.5-dist/js/html5shiv.min',
            'Sysadmin./bootstrap-3.3.5-dist/js/respond.min',
        );
         echo $this->Html->script($script_ls,
                array(
            'block' => 'scriptTop'
        ));
        echo $this->fetch('scriptTop');
        ?>
        <![endif]-->
        
        
             <?php
             
             echo $this->fetch('script');
        $script_ls = array(
            '../xcrud/plugins/jquery.min',
            '../xcrud/plugins/jquery-ui/jquery-ui.min',
            '../xcrud/plugins/bootstrap/js/bootstrap.min',
            'Sysadmin./bootstrap-3.3.5-dist/js/ie10-viewport-bug-workaround',
            'Sysadmin./smartmenus-1.0.0/jquery.smartmenus.min',
            'Sysadmin.sysScript',
        );
        if (isset($enqueued_script)) {
            $script_ls = array_merge($script_ls, $enqueued_script);
        }
        echo $this->Html->script($script_ls,
                array(
            'block' => 'scriptBottom'
        ));
      
        ?>
        <?php
echo $this->fetch('scriptBottom');
?>
    </head>
    <body>
        <?php
        echo $this->element('nav', array(), array('plugin' => 'Sysadmin'));
        ?>
        <div class="container-fluid" id="sysadmin_main_content">
            <div class="row">
                <div class="col-md-2 sidebar">
                    <?php
                    echo $this->element('sidebar', array(),
                            array('plugin' => 'Sysadmin'));
                    ?>

                </div>
                <div class="col-md-10 col-md-offset-2">
                    <?php
                    $showPageTitle = true;
                    if (isset($noPageTitle)):
                        if ($noPageTitle):
                            $showPageTitle = false;
                        endif;
                    endif;
                    if ($showPageTitle):
                        $page_title = isset($page_title) ? $page_title : $this->fetch('title');
                        ?><h1 class="page-header"><?php
                        echo Inflector::humanize(__d('sysadmin', $page_title));
                        ?></h1><?php
                    endif;
                    ?>
                    <?php echo $this->Flash->render(); ?>
                    <?php
                    if (isset($top_menu)) {
                        if ($top_menu) {
                            echo $this->element('topmenu',
                                    array('topmenuls' => $top_menu),
                                    array('plugin' => 'sysadmin'));
                        }
                    }
                    ?>
                    <?php echo $this->fetch('content'); ?>
                    <?php echo $this->element('sql_dump', array(),
                            array('plugin' => 'Sysadmin')); ?>
                </div>
            </div>
        </div>
        <div id="navbar-bottom-placeholder" class="no-print">
            <div class="navbar-fixed-bottom no-print" role="navigation">
                <div class="row no-print">
                    <div class="col-xs-1 col-xs-push-10 col-md-1 col-md-push-11 col-lg-1 col-lg-push-11 no-print">
                        <button id="backtotop" class="btn btn-sm btn-outline btn-info no-print" type="button">
                            <span class="glyphicon glyphicon-arrow-up no-print"></span></button>
                    </div>
                </div>
                
            </div>
        </div>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->


    </body>
</html>
