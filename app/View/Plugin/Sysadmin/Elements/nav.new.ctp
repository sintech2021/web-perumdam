<nav class="navbar navbar-inverse navbar-primary navbar-fixed-top" id="navbar">
    <div class="container-fluid" id="navbar-fluid">
        <div class="row">
        <div class="col-md-6">
            <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="navbar-brand collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <?php
            $home_link_text = $this->Html->image('/pub/img/perumdam-top.png', array('height'=> '64px'));
            $home_link_url  = array('plugin'     => 'sysadmin', 'controller' => 'sysadmin_app',
                'action'     => 'index');
            echo $this->Html->link($home_link_text, $home_link_url,
                    array('escape'=>false));
            ?>
            </div>
            
        </div>
        <div class="navbar-collapse collapse">
            <div class="nav navbar-nav navbar-left"> 
                <div style="flex-direction: row;padding-top: 10px; margin-left: -18px;"><h1 class="homepage_title">PERUMDAM TIRTA KERTA RAHARJA</h1><h4 class="homepage_sub_title">KABUPATEN TANGERANG</h4></div>
            </div>
            
        </div>
    </div>
        <div class="col-md-6">
            <ul class="nav navbar-nav navbar-right" style="margin-right: 0">
                <li class="dropdown">
                    <a href="#" style="font-size: 2em" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <?php
                        if (isset($profileMenu)):
                            if ($profileMenu):
                                foreach ($profileMenu as $i => $v):
                                    if (isset($v['resources_type'])):
                                        if ($v['resources_type'] == 'cake_anchor'):
                                            ?><li>
                                                <?php
                                                echo $this->SysHtml->resToCakeUrl($v);
                                                ?>
                                            </li><?php
                                        endif;
                                        if ($v['resources_type'] == 'bs_dropdown_sep'):
                                            ?><li role="separator" class="divider"></li><?php
                                        endif;
                                    endif;
                                endforeach;
                            endif;
                        endif;
                        ?>
                    </ul>
                </li>
            </ul>
        </div>    
        
            
        
    </div>
    </div>
</nav>
