<nav class="navbar navbar-inverse navbar-primary navbar-fixed-top">
    <div class="container-fluid" style="margin: 0;padding: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="navbar-brand collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar" style="margin-left:12px ">
                <?php
            $home_link_text = $this->Html->image('/top.png', array('style'=> 'height:40px'));
            $home_link_url  = array('plugin'     => 'sysadmin', 'controller' => 'sysadmin_app',
                'action'     => 'index');
            echo $this->Html->link($home_link_text, $home_link_url,
                    array('escape'=>false));
            ?>
            </div>
            
        </div>
        <div id="navbar" class="navbar-collapse collapse" style="padding: 12px">
            <ul class="nav navbar-nav navbar-left"><?php
            $home_link_text = '';//$this->Html->image('sindex/logo.png', array('height'=> '10px'));
            $home_link_text .= Configure::read('Sysconfig.app.desc');
            $home_link_url  = array('plugin'     => 'sysadmin', 'controller' => 'sysadmin_app',
                'action'     => 'index');
             $this->Html->link($home_link_text, $home_link_url,
                    array('class' => 'navbar-brand','escape'=>false));
            ?>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <?php
                        if (isset($profileMenu)):
                            if ($profileMenu):
                                foreach ($profileMenu as $i => $v):
                                    if (isset($v['resources_type'])):
                                        if ($v['resources_type'] == 'cake_anchor'):
                                            ?><li>
                                                <?php
                                                echo $this->SysHtml->resToCakeUrl($v);
                                                ?>
                                            </li><?php
                                        endif;
                                        if ($v['resources_type'] == 'bs_dropdown_sep'):
                                            ?><li role="separator" class="divider"></li><?php
                                        endif;
                                    endif;
                                endforeach;
                            endif;
                        endif;
                        ?>
                    </ul>
                </li>
            </ul>
            <?php
            echo $this->element('global-search', array(), array('plugin' => 'Sysadmin'));
            /**if (isset($global_search)):
                if ($global_search):
                    if (isset($global_search['form'])):
                        echo $global_search['form'];
                    endif;
                    ?><form class="navbar-form navbar-right " method="post">
                        <input name="_method" value="POST" type="hidden">
                        <input type="hidden" name="global_search[plugin]" value="<?php echo $global_search['plugin']; ?>">
                        <input type="hidden" name="global_search[controller]" value="<?php echo $global_search['controller']; ?>">
                        <input type="hidden" name="global_search[action]" value="<?php echo $global_search['action']; ?>">

                        <div class="row">

                            <div class="col-lg-12 col-sm-12">
                                <div class="input-group">
                                   <span class="form-group">
                                        <select class="form-control">
                                            <?php
                                            if ($global_search['allSearchKey']):
                                                foreach ($global_search['allSearchKey'] as $i => $v):
                                                    ?><option value="<?php echo $i; ?>"><?php echo $v; ?></option><?php
                                                endforeach;
                                            endif;
                                            
                                            ?>
                                            <?php
                                            if ($global_search['params']):
                                                foreach ($global_search['params'] as $i => $v):
                                                    ?><option value="<?php echo $i; ?>"<?php 
                                                    if($i==$global_search['field']):
                                                        ?> selected="selected"<?php
                                                    endif;
                                                    ?>><?php echo $v; ?></option><?php
                                                endforeach;
                                            endif;
                                            
                                            ?>
                                        </select>
                                    
                                    <input type="text" name="global_search[keyword]"  maxlength="300" class="form-control"  id="global_search_keyword"  placeholder="<?php echo __d('sysadmin',
                                                    'Search...');
                                            ?>" value="<?php h(trim($global_search['keyword'])); ?>" >
                                    </span>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" name="global_search[do]" type="submit"><span class="fa fa-search"></span></button>
                                        <button class="btn btn-default" name="global_search[clear]"  type="submit"><span class="fa fa-eraser"></span></button>
                                    </span>
                                </div><!-- /input-group -->
                            </div><!-- /.col-lg-12 -->
                        </div><!-- /.row -->

                    </form><?php
                endif;
            endif;
             * **/
             
            ?>
        </div>
    </div>
</nav>