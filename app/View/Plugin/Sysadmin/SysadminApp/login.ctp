<link href="<?=BASE_URL.'/'?>node_modules/opensans-npm-webfont/style.css" rel="stylesheet"  crossorigin="anonymous">
<link href="<?=BASE_URL.'/'?>node_modules/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet"  crossorigin="anonymous">

<div class="container">
    <div class="row-x">
        <div class="col-lg-12-x">
            
    <div class="form-signin">
        <div class="signin-heading" style="padding: 1em;">
              
                <div class="signin-logo"><?php echo $this->Html->image('../top.png');  ?></div>
            </div>
    </div>        
    <form class="form-signin jTGMge" method="post">
        
        <div class="form-group">
            <label for="username" class="sr-only"><?php echo __d('sysadmin', 'Username');?></label>
        <input type="text" maxlength="100" id="username" name="<?php echo md5($this->Session->read('AppUsrToken').'username');?>" class="form-control" placeholder="<?php echo __d('sysadmin', 'Username');?>" required autofocus>
        </div>
        <div class="form-group">
        <label for="inputPassword" class="sr-only"><?php echo __d('sysadmin', 'Password');?></label>
        <input type="password" maxlength="100" id="inputPassword" name="<?php echo md5($this->Session->read('AppUsrToken').'password');?>"  class="form-control" placeholder="<?php echo __d('sysadmin', 'Password');?>" required>
        </div>
        <!--div class="checkbox">
          <label>
            <input type="checkbox" name="rememberme" value="remember-me"> <?php echo __d('sysadmin', 'Remember me');?>
          </label>
        </div-->
        <div class="form-group">
        <?if(defined('ENABLE_G_CAPTCHA')):?>
            <?if(ENABLE_G_CAPTCHA):?>
                <?if($_SERVER['HTTP_HOST'] == 'ppsl.perumdamtkr.org'):?>
                <div class="g-recaptcha" data-sitekey="6LeyWeEUAAAAAJaunvapgbfUB3v1MSr56mc0ubru"></div>
                <?else:?>
                <div class="g-recaptcha" data-sitekey="6LecSeYUAAAAADazed6MSoJXAW53CyaHJXgZ8i79"></div>
                <?endif?>
            <?endif?>
        <?endif?>
    </div>
    <div class="form-group">
        <?php echo $this->Flash->render()?>
    </div>
    <div class="form-group">
        <button class="css-1eyj9fn" type="submit"><?php echo __d('sysadmin', 'Sign in');?></button>
      </div>
      </form>
        </div>
    </div>
    
</div> <!-- /container -->

<style>
    *{
        font-family: 'Open Sans'
    }
    .signin-logo > img{
        height: 40px;
    }
    .g-recaptcha{
        margin-bottom: 1em;
    }
.jTGMge {

  
    margin: 0px auto 24px;
    width: 400px;
    padding: 32px 40px;
    background: rgb(255, 255, 255);
    border-radius: 3px;
    box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 10px;
    box-sizing: border-box;
    color: rgb(94, 108, 132);
}
input.form-control{
    background: transparent;
    border: 0px;
    box-sizing: border-box;
   

    
    outline: none;
    width: 100%;


    background-color: rgb(250, 251, 252);
    border-color: rgb(223, 225, 230);
    border-radius: 3px;
    border-style: solid;


    font-size: 14px;
   
    line-height: 1.42857;
    max-width: 100%;

    transition: background-color 0.2s ease-in-out 0s, border-color 0.2s ease-in-out 0s;

    border-width: 2px;
    padding: 6px;

    margin: 0px auto 24px;
   
    padding: 32px 40px;
    background: rgb(255, 255, 255);
    border-radius: 3px;
    box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 10px;
    box-sizing: border-box;
    color: rgb(94, 108, 132);
}
.css-1eyj9fn {
 
    border-width: 0px;
    box-sizing: border-box;

    font-weight: 500;
    max-width: 100%;
    text-align: center;

    background: rgb(0, 82, 204);
    border-radius: 3px;
    cursor: default;
    padding: 0px 8px;
    transition: background 0.1s ease-out 0s, box-shadow 0.15s cubic-bezier(0.47, 0.03, 0.49, 1.38) 0s;

    width: 100%;
    outline: none !important;
    color: rgb(255, 255, 255) !important;
    height: 40px !important;
    line-height: 40px !important;
    font-weight: bold;
}
</style>
<script>
document.title = "Login masuk untuk melanjutkan"
</script>