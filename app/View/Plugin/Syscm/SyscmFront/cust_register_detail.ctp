<?php
$reg_content = $this->get('cust_reg_content', array());
if ($reg_content):
    
    extract($reg_content);
endif;
?><section class="wrapper style1 padding-0">
    <article class="row">
        <div class="article-content box box-override">
            <section class="col-md-12">
                <header>
                    <div class="pull-right btn-group">
                        <button class="btn btn-default" type="button" onclick="window.open('<?php
                        echo $this->Html->url(
                                array(
                                    'plugin' =>'syscm',
                                    'controller' => 'syscm_front', 
                                    'action' => 'cust_register', 
                                    'reg_code'=> $cust_reg_code,
                                    'print'=>1,
                                    ));
                        ?>', 'print-reg-frm', 'width=1014, height=758, left=10, top=10,location=0,menubar=0,resizable=0, status=0, titlebar=0, toolbar=0');"><span class="fa-stack">
  <i class="fa fa-square-o fa-stack-2x"></i>
  <i class="fa fa-print fa-stack-1x text-info"></i>
</span> <?php echo __d('syscm', 'Print Preview');?></button>
                    </div>
                    <h2><?php echo __d('syscm', 'Customer Registration Detail');
?></h2>
                </header>
                <div class="row">
                    <div class="col-md-12 well">
                        <table class="table table-striped">
                            <tr><td><?php echo __d('syscm', 'Registration Code'); ?></td><td>:</td><td><strong><?php echo isset($cust_reg_code) ? $cust_reg_code : ""; ?></strong></td></tr>
                            <tr><td><?php echo __d('syscm', 'Registration Date'); ?></td><td>:</td><td><strong><?php echo isset($cust_reg_dt) ? $cust_reg_dt : ""; ?></strong></td></tr>
                            <tr><td><?php echo __d('syscm', 'Full Name'); ?></td><td>:</td><td><strong><?php echo isset($cust_reg_name) ? $cust_reg_name : ""; ?></strong></td></tr>
                            <tr><td><?php echo __d('syscm', 'Phone Number'); ?></td><td>:</td><td><strong><?php echo isset($cust_reg_phone) ? $cust_reg_phone : ""; ?></strong></td></tr>
                            <tr><td><?php echo __d('syscm', 'Cellular Number'); ?></td><td>:</td><td><strong><?php echo isset($cust_reg_cell) ? $cust_reg_cell : ""; ?></strong></td></tr>
                            <tr><td><?php echo __d('syscm', 'Email'); ?></td><td>:</td><td><strong><?php echo isset($cust_reg_email) ? $cust_reg_email : ""; ?></strong></td></tr>
                            <tr><td><?php echo __d('syscm', 'Address'); ?></td><td>:</td><td><strong><?php echo isset($cust_reg_addr) ? $cust_reg_addr : ""; ?></strong></td></tr>
                            <tr><td><?php echo __d('syscm', 'Your IP Address'); ?></td><td>:</td><td><strong><?php echo isset($cust_reg_ip) ? $cust_reg_ip : ""; ?></strong></td></tr>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </article>
</section>
