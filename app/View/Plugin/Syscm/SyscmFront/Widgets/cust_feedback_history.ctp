<?php
$feedback_history = $this->get('cust_feedback_history', array());
if (!$feedback_history) return;
extract($feedback_history);

$feedback_respon = $this->get('cust_feedback_respon', array());
echo '<article class="well">';
//list
$list            = isset($list) ? $list : array();
if ($list):
    foreach ($list as $post):
        echo '<div class="col-md-12 mainlist">';
        echo '<section class="box style1 box-override ">';
        $post_feedback = isset($post['post_feedback']) ? sprintf('<p>%s</p>', h($post['post_feedback'])) : "";
        $post_author   = isset($post['author']) ? sprintf('<div class="meta-author">%s</div>', __d('syscm', 'Posted by : %s', $post['author'])) : "";
        $post_feedback .= $post_author;
        $post_header   = "";
        $post_date     = "";

        if (isset($post['date'])) {
            $strpubdate = strtotime($post['date']);
            $postday    = __d('syscm', date("l", $strpubdate));
            $postdate   = date("j", $strpubdate);
            $postmonth  = __d('syscm', date("F", $strpubdate));
            $postyear   = date("Y", $strpubdate);
            $posttime   = isset($post['time']) ? sprintf('%s ', $post['time']) : '';
            $post_date  = sprintf(
                    '%s ' .
                    '%s ' .
                    '%s ' .
                    '%s ' .
                    '%s', $postday, $postdate, $postmonth, $postyear, $posttime);
        }
        printf('<div class="mainlist-item"><div class="mainlist-title"><div class="row 200%%"><div class="3u"><div class="post-time">%s</div></div><div class="9u">%s</div></div></div>', $post_date, $post_feedback);

        echo '<div class="mainlist-content">';
        $cust_id = $post['scm_customer_id'];
        //debug($feedback_respon);
        if (isset($feedback_respon[$cust_id])):
            foreach ($feedback_respon[$cust_id] as $respon):
                $strrespontime = strtotime($respon['respon_time']);
                $responday     = __d('syscm', date("l", $strrespontime));
                $respondate    = date("j", $strrespontime);
                $responmonth   = __d('syscm', date("F", $strrespontime));
                $responyear    = date("Y", $strrespontime);
                $respontime    = date("H:i", $strrespontime);

                $respon_time = sprintf('%s, %s %s %s %s', $responday, $respondate, $responmonth, $responyear, $respontime);
                echo '<blockquote>';
                printf('<div class="meta-author">%s %s: <br>%s</div>', __d('syscm', 'Respon from'), $respon['responder'], $respon_time);
                printf('<p>%s</p>', h($respon['respon']));                
                echo '</blockquote>';
            endforeach;
        endif;
        echo '</div>';
        echo '<div style="clear:both;"></div>';
        echo '</section>';
        echo '</div>';

    endforeach;
    ?>
    <ul class="pagination">
        <?php
        echo $this->Paginator->prev('<span aria-hidden="true">&laquo;</span>', array(
            'escape' => false, 'tag'    => 'li'), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator'    => '', 'currentTag'   => 'a',
            'currentClass' => 'active', 'tag'          => 'li', 'first'        => 1));
        echo $this->Paginator->next('<span aria-hidden="true">&raquo;</span>', array(
            'escape' => false, 'tag'    => 'li'), null, array('class' => 'next disabled'));
        ?>
    </ul><?php
endif;
echo '</article>';
?>

