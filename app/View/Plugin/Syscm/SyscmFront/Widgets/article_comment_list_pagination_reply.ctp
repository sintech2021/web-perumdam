<?php
if (!$comment_paging): return;
endif;
extract($comment_paging);
if ($count <= $limit): return;
endif;
$total_page = ceil($count / $limit);
$pages      = range(1, $total_page);
$li_f       = '<li class="%s">%s</li>'; //li formatted
$base_link  = array(
    'plugin'     => $this->request->plugin,
    'controller' => $this->request->controller,
    'action'     => $this->request->action,
);
$passed     = $this->request->param('pass');
if ($passed):
    foreach ($passed as $val):
        $base_link[] = $val;
    endforeach;
endif;
$named                     = $this->request->param('named');
$base_link['comment_page'] = $content_page;
$base_link['reply_cid'] = $reply_cid;

?>
<ul class="pagination">
    <?php
    $first_url                 = $base_link;
    $first_url['reply_page'] = 1;
    $first_title               = '<span class="fa fa-angle-double-left "></span>';
    $first_disabled            = $comment_page == 1 ? 'disabled' : '';
    $first                     = $comment_page > 1 ? $this->Html->link($first_title, $this->Html->url($first_url, true), array(
                'escape' => false)) : $first_title;
    printf($li_f, 'prev ' . $first_disabled, $first);

    //prev
    $prev_val = $comment_page - 1;

    $prev_url                 = $base_link;
    $prev_url['reply_page'] = $prev_val;
    $prev_title               = '<span class="fa fa-angle-left "></span>';
    $prev_disabled            = $prev_val < 1 ? 'disabled' : '';
    $prev                     = $prev_val >= 1 ? $this->Html->link($prev_title, $this->Html->url($prev_url, true), array(
                'escape' => false)) : $prev_title;
    printf($li_f, ' ' . $prev_disabled, $prev);
    $truncate_pre             = false;
    $truncate_post            = false;
    $paging_between           = 5;
    foreach ($pages as $val):
        if ($val < ($comment_page - $paging_between)):
            $truncate_pre = true;
            continue;
        endif;
        if ($val > ($comment_page + $paging_between)):
            $truncate_post = true;
            continue;
        endif;
        $page_url                 = $base_link;
        $page_url['reply_page'] = $val;

        $paging_class = $comment_page == $val ? 'active' : '';
        $paging_title = $comment_page == $val ? sprintf('<a>%s</a>', $val) : $val;
        $paging_this  = $comment_page == $val ? $paging_title : $this->Html->link($paging_title, $this->Html->url($page_url, true), array(
                    'escape' => false));
        if ($truncate_pre):
            printf($li_f, '', '<a>..</a>');
            $truncate_pre = false;
        endif;
        printf($li_f, $paging_class, $paging_this);
    endforeach;
    if ($truncate_post):
        printf($li_f, '', '<a>..</a>');
    endif;
    //next
    $next_val                 = $comment_page + 1;
    $next_url                 = $base_link;
    $next_url['reply_page'] = $next_val;
    $next_title               = '<span class="fa fa-angle-right "></span>';
    $next_disabled            = $next_val > $total_page ? 'disabled' : '';
    $next                     = $next_val <= $total_page ? $this->Html->link($next_title, $this->Html->url($next_url, true), array(
                'escape' => false)) : $next_title;
    printf($li_f, ' ' . $next_disabled, $next);

    //last
    $last_url                 = $base_link;
    $last_url['reply_page'] = $total_page;
    $last_title               = '<span class="fa fa-angle-double-right "></span>';
    $last_disabled            = $comment_page == $total_page ? 'disabled' : '';
    $last                     = $comment_page != $total_page ? $this->Html->link($last_title, $this->Html->url($last_url, true), array(
                'escape' => false)) : $last_title;
    printf($li_f, 'last ' . $last_disabled, $last);
    ?>
</ul>
<div class="clear clearfix" style="clear: both;"><br></div>
<?php
//Fdebug($comment_paging);
