<?php
$empty = sprintf('<small>%s</small>', __d('syscm', 'Comments are empty. Fill in the form below to be the first.'));
if(!isset($comment)):
    echo $empty;
    return;
endif;
extract($comment);
if(!isset($ls)):
    echo $empty;
    return;
endif;
extract($ls);
if(!isset($count)):
    echo $empty;
    return;
endif;
if($count < 1 ):
    echo $empty;
    return;
endif;
    
?>
<div class="article-content box box-override">
    
        
    <?php
    foreach($comments as $i => $v):
        ?><div class="row">
            <div class="<?php echo $i % 2 != 0 ? 'odd': 'even';?>">
                <blockquote><header>
                        <div class="comment-date"><?php echo $v['post_date'];?></div>
                        <div><strong><?php echo $v['name'];?></strong> <?php echo __d('syscm', 'wrote');?>:</div>
                    </header><?php echo $v['message'];?></blockquote>
            </div>
        </div><?php
    endforeach;
    ?>
</div>