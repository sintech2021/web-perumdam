<?php
$fb_share = $this->get('fb_share', array());
if ($fb_share):
    if (isset($fb_share['meta']['og:url'])):
        ?>
 <div style="text-align:left; padding-top:3px;">
    <g:plus action="share"></g:plus>
</div>

<div className="fb-share-button" data-href="<?php echo $fb_share['meta']['og:url']; ?>" 
             data-layout="button_count">
        </div>
        <div style="text-align:left; padding-top:3px;">
            <a href="https://twitter.com/share" className="twitter-share-button"{count} data-lang="id">Tweet</a>
            <script>!function (d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
                    if (!d.getElementById(id)) {
                        js = d.createElement(s);
                        js.id = id;
                        js.src = p + '://platform.twitter.com/widgets.js';
                        fjs.parentNode.insertBefore(js, fjs);
                    }
                }(document, 'script', 'twitter-wjs');</script>
        </div>
    
        <div style="text-align:left; padding-top:3px;">
            <a data-pin-do="buttonPin" target="_blank" href="https://www.pinterest.com/pin/create/button/?url=<?php echo urlencode($fb_share['meta']['og:url']); ?>&media=https%3A%2F%2Ffarm8.staticflickr.com%2F7027%2F6851755809_df5b2051c9_z.jpg&description=Next%20stop%3A%20Pinterest"><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_gray_20.png" /></a>
        </div>
<div style="text-align:left; padding-top:3px;">
        <script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: in_ID</script>
<script type="IN/Share" data-url="<?php echo $fb_share['meta']['og:url']; ?>" data-counter="right"></script>
</div>
 
		<div className="addthis_sharing_toolbox"></div>
		<br>
		<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52f47a80539870b9" async="async"></script>


        <?php
    endif;
endif;
?>