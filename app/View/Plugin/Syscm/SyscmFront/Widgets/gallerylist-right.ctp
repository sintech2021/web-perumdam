<?php

/**
 * 
 * [title] => String,
 * [list] => 
 *      [] =>
 *          [title] => String,
 *          [content] => String,
 *          [url] => CakeUrl
 * 
 */
$sidelist = $this->get('sidelist', false);
if (!$sidelist): return;
endif;
extract($sidelist);
$title = isset($title) ? $title : "";
$fa_icon = isset($fa_icon) ? $fa_icon : "fa-image";
$elm_id = "modal".rand(9,1981);
$title = isset($url) ? $this->Html->link($title, $this->Html->url($url, true), array('class'=>'image-gallery-button sidelist-title','data-source-elm' => $elm_id)) : $title;
$list  = isset($list) ? $list : array();



echo '<div class="panel panel-primary">';
if ($title):
	echo '<div class="panel-heading">';
    echo sprintf('<h3 class="panel-title"><div class="icon"><i class="fa %s fa-fw fa-flip-horizontal""></i></div> %s</h3>', $fa_icon,$title);
	echo '</div>';
endif;
if ($list):

    printf('<div class="sss_slider" id="%s">', $elm_id);
    foreach($list as $v):
        printf('<div class="col-md-12 embed-responsive embed-responsive-4by3"  style="overflow:hidden;" >%s</div>', $v['content']);
    endforeach;
	echo '</div>';
    
endif;

echo '</div>';


echo "<br>";

?>
