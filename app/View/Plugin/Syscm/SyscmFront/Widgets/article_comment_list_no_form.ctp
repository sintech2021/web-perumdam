<?php
$empty = sprintf('<div class="article-content box box-override box-comment-list"><div class="" style="padding:1em"><div>%s</div></div></div>','Belum ada komentar.');
$comment = $this->get('syscm_comment', array());

if (!isset($comment)):
    echo $empty;
    return;
endif;
extract($comment);
if (!isset($ls)):
    echo $empty;
    return;
endif;
extract($ls);
if (!isset($count)):
    echo $empty;
    return;
endif;
if ($count < 1):
    echo $empty;
    return;
endif;
?>
<div class="article-content box box-override box-comment-list">
    <?php
    foreach ($comments as $i => $v):
        $reply_error = isset($v['reply_error']) ? $v['reply_error'] : array();
        $reply_data  = isset($v['reply_data']) ? $v['reply_data'] : array();
        extract($reply_data);
        extract($reply_error);
        ?><div class="row">
        
                <div class="col-md-12">
                    <header>
                        <span class="comment-author"><?php echo $v['name']; ?></span> 
                        <span class="comment-date"><?php echo  comment_dt($v['post_date']); ?></span>
                    
                    </header>
                    <div class="comment-content">
                    <?php echo $v['message']; ?>
                            </div>
                    <?php
                    if ($v['replies']):
                        ?>
                        
                        <?php
                        if(!isset($v['replies']['replies'])):
                            $v['replies']['replies'] = array();
                        endif;
                        
                        if ($v['replies']['replies']):
                            foreach ($v['replies']['replies'] as $irep => $vrep):
                                ?>
                                <blockquote>
                                    <header>
                                        <span class="comment-author"><?php echo $vrep['name']; ?></span>
                                        <span class="comment-date"><?php echo comment_dt($vrep['post_date']); ?></span>
                                        
                                    </header>
                                    <div class="comment-content">

                                    <?php echo $vrep['message']; ?>
                                    </div>
                                </blockquote>
                                <?php
                            endforeach;
                        endif;
                        //debug($v);
                        echo $this->element("Syscm.../SyscmFront/Widgets/article_comment_list_pagination_reply", array('comment_paging'=>$v['replies']['comment_paging']));

                    endif;
                    ?>



                    <div class="reply-toggle">
                        <a href="javascript:;" class="" onclick="comentBoxComponent.onReply(this,'<?php echo $i; ?>');">Jawab</a>
                    </div>
                </div>
                <div class="col-md-12 reply-form-cont" id="reply-form-cont-<?php echo $i; ?>" style="display:none;">
                    <section class="col-md-12 well">

                        <form method="post reply-comment-box">
                            <input type="hidden" name="_method" value="POST" />
                            <input type="hidden" name="reply_cid" value="<?php echo $v['cid']; ?>" />
                            <input type="hidden" name="comment_page" value="<?php
                            $comment_page = 1;
                            $named        = $this->request->param('named');

                            if (isset($named['comment_page'])):
                                $comment_page = $named['comment_page'];
                            endif;
                            echo $comment_page;
                            ?>" />
                            <div class="row">
                                <div class="col-md-12">
                                    <?php
                                    $reply_var = 'reply_name';
                                    ?>
                                    <input value="<?php echo isset(${$reply_var . '_data'}) ? ${$reply_var . '_data'} : ''; ?>" class="<?php echo isset(${$reply_var}) ? "form-control input-danger" : 'form-control'; ?>" name="<?php echo $reply_var; ?>" id="<?php echo $reply_var; ?>" placeholder="<?php echo __d('syscm', 'Name'); ?>" type="text" required="required">
                                    <?php
                                    echo isset(${$reply_var}['message']) ? $this->element('Syscm.front_error', array(
                                                'msg' => ${$reply_var}['message'])) : "";
                                    ?>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-xs-12">
                                    <?php
                                    $reply_var = 'reply_email';
                                    ?>
                                    <input value="<?php echo isset(${$reply_var . '_data'}) ? ${$reply_var . '_data'} : ''; ?>" class="<?php echo isset(${$reply_var}) ? "form-control input-danger" : 'form-control'; ?>" name="<?php echo $reply_var; ?>" id="<?php echo $reply_var; ?>" placeholder="<?php echo __d('syscm', 'Email'); ?>" type="email" required="required">
                                    <?php
                                    echo isset(${$reply_var}['message']) ? $this->element('Syscm.front_error', array(
                                                'msg' => ${$reply_var}['message'])) : "";
                                    ?>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <?php
                                    $reply_var = 'reply_telp';
                                    ?>
                                    <input value="<?php echo isset(${$reply_var . '_data'}) ? ${$reply_var . '_data'} : ''; ?>" class="<?php echo isset(${$reply_var}) ? "form-control input-danger" : 'form-control'; ?>" name="<?php echo $reply_var; ?>" id="<?php echo $reply_var; ?>" placeholder="<?php echo __d('syscm', 'Telp'); ?>" type="tel" required="required">
                                    <?php
                                    echo isset(${$reply_var}['message']) ? $this->element('Syscm.front_error', array(
                                                'msg' => ${$reply_var}['message'])) : "";
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <?php
                                    $reply_var = 'reply_message';
                                    ?>
                                    <textarea class="<?php echo isset(${$reply_var}) ? "form-control input-danger" : 'form-control'; ?>" name="<?php echo $reply_var; ?>" id="<?php echo $reply_var; ?>" placeholder="<?php echo __d('syscm', 'Your reply message'); ?>" rows="5"><?php echo isset(${$reply_var . '_data'}) ? ${$reply_var . '_data'} : ''; ?></textarea>
                                    <?php
                                    echo isset(${$reply_var}['message']) ? $this->element('Syscm.front_error', array(
                                                'msg' => ${$reply_var}['message'])) : "";
                                    ?>
                                </div>
                            </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div id="recaptcha_parent_container_l-<?php echo md5('#reply-form-cont-'.$i); ?>">
                            </div>    
                        </div>
                    </div>
                            <div class="row">
                        <div class="col-md-12">
                            <div class="link_separator" style="padding-top:6px;display: flex;justify-content: flex-end;">
                                <a class="link_goto_page" href="javascript:;"><?php echo __d('syscm', 'Send Reply');?></a>
                            </div>    
                        </div>
                    </div>

                        </form>
                    </section>
                </div>
            

        </div><?php
    endforeach;
    // echo $this->element("Syscm.../SyscmFront/Widgets/article_comment_list_pagination", compact('comment_paging'));
    ?>
</div>
<?php
//debug($this->session->read());
//debug($comment_paging);
//debug($comment);
?>