<?php
$article = $this->get('article', array());
if (!$article): return;
endif;
extract($article);


?>
<article>
 
                <h2 class="title-article"><?php echo $title; ?></h2>
                 
               
    <div class="article-info" style="padding: 0 1em">
    	<div class="row">
    		<div class="col-md-6" style="padding: 0">
    			<h4 class="article-author"><?=$author?></h4>
    			<h4 class="article-dt"><?=$dt?></h4>
    			 
    		</div>
    		<div class="col-md-6 share-container" style="text-align: right;">
    			<a class="share-btn fb" href="javascript:;" onclick="shereThisUrl(this)" style="margin-right: 1em"><img src="<?=BASE_URL.'/'?>/icons/fb_share.png"/></a>
    			<a class="share-btn twitter" href="javascript:;" onclick="shereThisUrl(this)"><img src="<?=BASE_URL.'/'?>/icons/twitter_share.png"/></a>
    		</div>
    	</div>
    	
    </div>         

<div style="padding:2em 0"> <hr class="featurette-divider" style="margin: 0"/></div>
     
    <div class="article-content box box-override text-justify col-md-12">
		<?php echo str_replace('col-md-8 col-xs-12','col-md-12',$content); ?>
    </div>
<div style="padding:2em 0"> <hr class="featurette-divider" style="margin: 0"/></div>

</article>