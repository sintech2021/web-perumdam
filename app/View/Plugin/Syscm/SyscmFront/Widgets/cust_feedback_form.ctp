<?php 
$feedback_data  = $this->get('cust_feedback_data', array());
$feedback_error = $this->get('cust_feedback_error', array());

if ($feedback_data):
    extract($feedback_data);
endif;
if ($feedback_error):
    extract($feedback_error);
endif;
?><h3><?php echo __d('syscm', 'Send us a feedback'); ?></h3>

                <form method="post">
                    <input type="hidden" name="_method" value="PUT" />
                    <div class="row">
                        <?php
                        
                        $var                          = 'cust_feedback_name';
                        $input_options                = array();
                        $input_options['div']         = 'col-md-12';
                        $input_options['name']        = $var;
                        $input_options['id']          = $var;
                        $input_options['placeholder'] = __d('syscm', 'Name')." *)";
                        $input_options['value']       = isset(${$var}) ? ${$var}
                                    : '';
                        $input_options['class']       = isset(${$var . '_error'})
                                    ? "form-control input-danger" : 'form-control';
                        $input_options['after']       = isset(${$var . '_error'})
                                    ? $this->element('Syscm.front_error',
                                        array('msg' => ${$var . '_error'})) : "";
                        echo $this->Form->input($var, $input_options);
                        ?>
                    </div>
                    <div class="row">
                        <?php
                        $var                          = 'cust_feedback_nosl';
                        $input_options                = array();
                        $input_options['name']        = $var;
                        $input_options['id']          = $var;
						$input_options['div']         = 'col-md-12';
                        $input_options['placeholder'] = __d('syscm',
                                'No. SL')." *)";
                        $input_options['value']       = isset(${$var}) ? ${$var}
                                    : '';
                        $input_options['class']       = isset(${$var . '_error'})
                                    ? "form-control input-danger" : 'form-control';
                        $input_options['after']       = isset(${$var . '_error'})
                                    ? $this->element('Syscm.front_error',
                                        array('msg' => ${$var . '_error'})) : "";
                        echo $this->Form->input($var, $input_options);

                        $var                          = 'cust_feedback_email';
                        $input_options                = array();
                        $input_options['type']        = 'email';
                        $input_options['name']        = $var;
                        $input_options['id']          = $var;
					    $input_options['div']         = 'col-md-12';
                        $input_options['placeholder'] = __d('syscm', 'Email')." *)";
                        $input_options['value']       = isset(${$var}) ? ${$var}
                                    : '';
                        $input_options['class']       = isset(${$var . '_error'})
                                    ? "form-control input-danger" : 'form-control';
                        $input_options['after']       = isset(${$var . '_error'})
                                    ? $this->element('Syscm.front_error',
                                        array('msg' => ${$var . '_error'})) : "";
                        echo $this->Form->input($var, $input_options);
                        ?>
                    </div>
                    <div class="row 50%">
                        <?php
                        $var                          = 'cust_feedback_phone';
                        $input_options                = array();
                        $input_options['type']        = 'tel';
						$input_options['div']         = 'col-md-12';
                        $input_options['name']        = $var;
                        $input_options['id']          = $var;
                        $input_options['placeholder'] = __d('syscm',
                                'Phone Number');
                        $input_options['required']    = false;
                        $input_options['value']       = isset(${$var}) ? ${$var}
                                    : '';
                        $input_options['class']       = isset(${$var . '_error'})
                                    ? "form-control input-danger" : 'form-control';
                        $input_options['after']       = isset(${$var . '_error'})
                                    ? $this->element('Syscm.front_error',
                                        array('msg' => ${$var . '_error'})) : "";
                        echo $this->Form->input($var, $input_options);

                        $var                          = 'cust_feedback_cell';
                        $input_options                = array();
                        $input_options['type']        = 'tel';
						$input_options['div']         = 'col-md-12';
                        $input_options['name']        = $var;
                        $input_options['id']          = $var;
                        $input_options['placeholder'] = __d('syscm',
                                'Cellular Number')." *)";
                        $input_options['value']       = isset(${$var}) ? ${$var}
                                    : '';
                        $input_options['class']       = isset(${$var . '_error'})
                                    ? "form-control input-danger" : 'form-control';
                        $input_options['after']       = isset(${$var . '_error'})
                                    ? $this->element('Syscm.front_error',
                                        array('msg' => ${$var . '_error'})) : "";
                        echo $this->Form->input($var, $input_options);
                        ?>
                    </div>

                    <div class="row">
                        <?php
                        $var                          = 'cust_feedback_msg';
                        $input_options                = array();
                        $input_options['type']        = 'textarea';
                        $input_options['div']         = 'col-md-12';
                        $input_options['name']        = $var;
                        $input_options['id']          = $var;
                        $input_options['placeholder'] = __d('syscm',
                                'Type your message here')." *)";
                        $input_options['value']       = isset(${$var}) ? ${$var}
                                    : '';
                        $input_options['class']       = isset(${$var . '_error'})
                                    ? "form-control input-danger" : 'form-control';
                        $input_options['after']       = isset(${$var . '_error'})
                                    ? $this->element('Syscm.front_error',
                                        array('msg' => ${$var . '_error'})) : "";
                        echo $this->Form->input($var, $input_options);
                        ?>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="g-recaptcha" data-sitekey="<?php echo Configure::read('Sysconfig.recaptcha.sitekey');?>"></div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <input class="12u button alt btn btn-info" value="<?php
                        echo __d('syscm', 'Send Feedback');
                        ?>" type="submit">
                        </div>
						<div class="col-md-12">*) <?php echo __d('syscm', 'Required field.');?></div>
                    </div>
                </form>