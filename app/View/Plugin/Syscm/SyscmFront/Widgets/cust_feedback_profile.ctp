<?php
$feedback_profile = $this->get('cust_feedback_profile', array());
$input_default                = array(
            'div'      => 'col-md-6 col-xs-12',
            'type'     => 'text',
            'label'    => false,
            'required' => true,
        );
        $this->Form->inputDefaults($input_default);
if ($feedback_profile) :
    ?>
    <h3><?php echo __d('syscm', 'Customer Profile'); ?></h3>
    <table class="table">
        <tr>
            <td><?php echo __d('syscm', 'Customer ID'); ?></td>
            <td><?php echo h($feedback_profile['ScmCustInfo']['text']); ?></td>
        </tr>
        <tr>
            <td><?php echo __d('syscm', 'Name'); ?></td>
            <td><?php echo h($feedback_profile['ScmCustInfo']['name']); ?></td>
        </tr>
        <tr>
            <td><?php echo __d('syscm', 'Address'); ?></td>
            <td><?php echo h($feedback_profile['ScmCustInfo']['annotation']); ?></td>
        </tr>
        <tr>
            <td><?php echo __d('syscm', 'Gol/Wil'); ?></td>
            <td><?php echo h($feedback_profile['ScmCustInfoDetail']['name']); ?>/<?php echo h($feedback_profile['ScmCustInfoDetail']['text']); ?></td>
        </tr>
    </table>

    <form method="post">
        <input type="hidden" name="_method" value="POST" />
        <div class="row">
            <div class="col-md-12">
                <button class="btn-block btn btn-danger" name="cust_feedback_clear_nosl" value="1" type="submit"><span class="fa fa-remove"></span> 
                    <?php
                    echo __d('syscm', 'Close');
                    ?></button>
            </div>
        </div>

    </form>
    <?php
    return;
endif;

$cust_feedback_check_nosl_posted = $this->get('cust_feedback_check_nosl_posted', array());
if ($cust_feedback_check_nosl_posted):
    extract($cust_feedback_check_nosl_posted);
endif;
?>
<h3><?php echo __d('syscm', 'My feedback history'); ?></h3>
<form method="post">
    <input type="hidden" name="_method" value="POST" />
    <div class="row">
        <?php
        
        $var                          = 'cust_feedback_check_nosl';
        $input_options                = array();
        $input_options['div']         = 'col-md-12';
        $input_options['name']        = $var;
        $input_options['id']          = $var;
        $input_options['placeholder'] = __d('syscm', 'Customer ID') . " *)";
        $input_options['value']       = isset(${$var}) ? ${$var} : '';
        $input_options['class']       = isset(${$var . '_error'}) ? "form-control input-danger" : 'form-control ';
        $input_options['after']       = isset(${$var . '_error'}) ? $this->element('Syscm.front_error', array(
                    'msg' => ${$var . '_error'})) : "";
        echo $this->Form->input($var, $input_options);
        ?>
    </div>

    <div class="row">
        <div class="col-md-12">
            <button class="button alt fit btn btn-info"  value="1" type="submit">
                <span class="fa fa-binoculars"></span> 
                <?php
                echo __d('syscm', 'View history');
                ?></button>

        </div>

</form>