<?php
$mainlist = $this->get('mainlist', array());

if (!$mainlist): return;
endif;
extract($mainlist);

$title    = isset($title) ? $title : "";
$subtitle = isset($subtitle) ? $subtitle : "";
echo '<article>';
if ($title || $subtitle):
    echo '<header class="">';
    printf('<h6 class="header-mainlist">%s</h6>', h($title));
    if ($subtitle):
        printf('<p>%s</p>', $subtitle);
    endif;
    echo '</header>';
endif;


//list
$list = isset($list) ? $list : array();
if ($list):
    echo '<div class="col-md-12 ">';
    foreach ($list as $post):
        
        echo '<section class="box style1 box-override mainlist">';
        $post_subtitle = isset($post['subtitle']) ? sprintf('<em><small>%s</small></em>',
                        h($post['subtitle'])) : "";
        $post_author   = isset($post['author']) ? sprintf('<div class="meta-author">%s</div>',
                        __d('syscm', 'Posted by : %s', $post['author'])) : "";
        $post_subtitle .= $post_author;
        $post_url      = isset($post['url']) ? $post['url'] : '#';
        $post_title    = isset($post['title']) ? sprintf('%s %s',
                        $this->Html->link(h($post['title']),
                                $this->Html->url($post_url, true),
                                array('style' => "text-decoration: none;border-bottom: none;line-height: 14pt;")),
                        $post_subtitle) : $post_subtitle;
        $post_header   = "";
        $post_date     = "";

        if (isset($post['date'])) {
            $strpubdate = strtotime($post['date']);
            $postday    = __d('syscm', date("l", $strpubdate));
            $postdate   = date("j", $strpubdate);
            $postmonth  = __d('syscm', date("F", $strpubdate));
            $postyear   = date("Y", $strpubdate);
            $posttime   = isset($post['time']) ? sprintf('%s ',
                            $post['time']) : '';
            $post_date  = sprintf(
                    '<div class="postday">%s ' .
                    '%s ' .
                    '%s ' .
                    '%s ' .
                    '%s </div>', $postday, $postdate, $postmonth, $postyear, $posttime);
        }

        
	
        $post_header .= sprintf('<div class="post-dates"><small class="text-primary">%s</small></div>',
                $post_date);
		$post_content = '<div class="media">';
        if (isset($post['content'])) {
            $first_img  = $this->SyscmArticle->getFirstImg($post['content'],
                    false);
            if ($first_img) {
				$post_content .='<div class="media-left">
							<a href="#">
							  '.$this->Html->image($this->Html->url($first_img,
                                true),array('class' => 'media-object img-thumbnail','style'=>'width:100px;')).'
							</a>
						  </div>';
                
            }
            $post_content .= '<div class="media-body">
							<h3 class="media-heading">'.$post_header.$post_title.'</h3>
							'.$this->SyscmArticle->excerpt($post['content']).' '.$this->Html->link(__d('syscm', 'read more'),
                    $this->Html->url($post_url, true)).'
						  </div>';
            $post_content .= '</div>';
        }
        printf('<div class="mainlist-item"><div class="mainlist-content ">%s</div></div>',
                 $post_content);
        echo '<div style="clear:both;"></div>';
        echo '</section>';
    endforeach;
    
        echo '</div>';
    ?>
	<div class="col-md-12">
    <ul class="pagination">
    <?php
    echo $this->Paginator->prev('<span aria-hidden="true">&laquo;</span>',
            array('escape' => false, 'tag' => 'li'), null,
            array('class' => 'prev disabled'));
    echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a',
        'currentClass' => 'active', 'tag' => 'li', 'first' => 1));
    echo $this->Paginator->next('<span aria-hidden="true">&raquo;</span>',
            array('escape' => false, 'tag' => 'li'), null,
            array('class' => 'next disabled'));
    ?>
    </ul>
	</div>
	<?php
endif;
echo '</article>';
?>

