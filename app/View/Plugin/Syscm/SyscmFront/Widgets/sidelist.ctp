<?php

/**
 * 
 * [title] => String,
 * [list] => 
 *      [] =>
 *          [title] => String,
 *          [content] => String,
 *          [url] => CakeUrl
 * 
 */
$sidelist = $this->get('sidelist', false);
if (!$sidelist): return;
endif;
extract($sidelist);
$title = isset($title) ? $title : "";
$title = isset($url) ? $this->Html->link($title, $this->Html->url($url, true), array('class'=>'sidelist-title')) : $title;
$list  = isset($list) ? $list : array();
echo '<section class="panel panel-primary">';

if ($title):
	echo '<div class="panel-heading">';
    echo sprintf('<h3 class="panel-title"><div class="icon"><i class="fa fa-tags fa-fw"></i></div> %s</h3>', $title);
	echo '</div>';
endif;
if ($list):
    echo '<ul class="list-group">';
    foreach ($list as $i):
        echo '<li class="list-group-item">';
   
        echo sprintf('%s', $this->Html->link(''.$i['title'], $this->Html->url($i['url'], true), array('escape'=> false)));
        echo '</li>';
    endforeach;
    echo '</ul>';
endif;
echo '</section>';
/*
  ?>

  <footer>
  <a href="#" class="button">More Random Links</a>
  </footer> <?php
 * 
 */
?>