<?php
$comment = $this->get('syscm_comment', false);
if(!$comment): return;endif;
$error = $this->get('syscm_comment_error', array());
$comment_data = $this->get('syscm_comment_data', array());
extract($comment_data);
extract($error);
?>
<div class="clear clearfix" style="clear: both;"><br></div>
<article class="content-comments comment-box">
    <header>
        <div class="row">
            <section class="col-md-12">
                <h2 class="header-comment" style="margin-bottom: 1em"><?php echo __d('syscm', 'Comments');?></h2>
				<h2 class="header-comment sub" style="margin-bottom: 0">Tulis Komentar</h2>
            </section>
        </div>
    </header>
    
    <div class="clear clearfix" style="clear: both;height: 10px"><br></div>
    <div class="article-content box box-override">
        <section class="col-md-12">
            
                <form method="post" class="comment-form">
                    <input type="hidden" name="_method" value="PUT"/>
                    <div class="row">
                        <div class="col-md-12" >
                            <input value="<?php echo isset($cust_name_data) ? $cust_name_data : '';?>" class="form-control <?php echo isset($cust_name) ? "input-danger" : '';?>" name="cust_name" id="cust_name" placeholder="<?php echo __d('syscm', 'Name');?>" type="text" required="required">
                            <?php
                            echo isset($cust_name['message']) ? $this->element('Syscm.front_error', array('msg'=> $cust_name['message'])) : "";
                            ?>
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-xs-12" style="padding-right: 1em">
                            <input value="<?php echo isset($cust_email_data) ? $cust_email_data : '';?>" class="form-control <?php echo isset($cust_email) ? "input-danger" : '';?>" name="cust_email" id="cust_id" placeholder="<?php echo __d('syscm', 'Email');?>" type="email" required="required">
                            <?php
                            echo isset($cust_email['message']) ? $this->element('Syscm.front_error', array('msg'=> $cust_email['message'])) : "";
                            ?>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <input value="<?php echo isset($cust_telp_data) ? $cust_telp_data : '';?>" class="form-control <?php echo isset($cust_telp) ? "input-danger" : '';?>" name="cust_telp" id="cust_telp" placeholder="<?php echo __d('syscm', 'Telp');?>" type="tel" required="required">
                            <?php
                            echo isset($cust_telp['message']) ? $this->element('Syscm.front_error', array('msg'=> $cust_telp['message'])) : "";
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <textarea class="form-control <?php echo isset($cust_message) ? "input-danger" : '';?>" name="cust_message" id="cust_message" placeholder="<?php echo __d('syscm', 'Your message');?>" rows="5"><?php echo isset($cust_message['data']) ? $cust_message['data'] : '';?></textarea>
                            <?php
                            echo isset($cust_message['message']) ? $this->element('Syscm.front_error', array('msg'=> $cust_message['message'])) : "";
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" id="grecaptcha_parent_container_c_cnt">
                            <?php
                            echo isset($recaptcha['message']) ? $this->element('Syscm.front_error', array('msg'=> $recaptcha['message'])) : "";
                            ?>
                        </div>
                            
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="link_separator" style="padding-top:6px;display: flex;justify-content: flex-end;">
                                <a class="link_goto_page" href="javascript:;"><?php echo __d('syscm', 'Send Comment');?></a>
                            </div>    
                        </div>
                    </div>
                    
                </form>
            </section>
    </div>
</article>
