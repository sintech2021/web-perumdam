<?php
$empty = sprintf('<small>%s</small>', __d('syscm', 'Comments are empty. Fill in the form below to be the first.'));
if (!isset($comment)):
    echo $empty;
    return;
endif;
extract($comment);
if (!isset($ls)):
    echo $empty;
    return;
endif;
extract($ls);
if (!isset($count)):
    echo $empty;
    return;
endif;
if ($count < 1):
    echo $empty;
    return;
endif;
?>
<div class="article-content box box-override">
    <?php
    foreach ($comments as $i => $v):
        $reply_error = isset($v['reply_error']) ? $v['reply_error'] : array();
        $reply_data  = isset($v['reply_data']) ? $v['reply_data'] : array();
        extract($reply_data);
        extract($reply_error);
        ?><div class="row">
            <div>
                <div class="col-md-12">
                    <header>
                        <div class="comment-date"><?php echo $v['post_date']; ?></div>
                        <div><strong><?php echo $v['name']; ?></strong> <?php echo __d('syscm', 'wrote'); ?>:</div>
                    </header>
                    <?php echo $v['message']; ?>
                    <?php
                    if ($v['replies']):
                        ?>
                        <hr>
                        <?php
                        if(!isset($v['replies']['replies'])):
                            $v['replies']['replies'] = array();
                        endif;
                        
                        if ($v['replies']['replies']):
                            foreach ($v['replies']['replies'] as $irep => $vrep):
                                ?>
                                <blockquote>
                                    <header>
                                        <div class="comment-date"><?php echo $vrep['post_date']; ?></div>
                                        <div><?php
                                        echo $vrep['account_official_text'] ? sprintf('<em>%s</em> ', $vrep['account_official_text']) : '';
                                        ?><strong><?php echo $vrep['name']; ?></strong> 
                                            <?php 
                                            
                                            echo __d('syscm', 'wrote'); 
                                            ?>:</div>
                                    </header>
                                    <?php echo $vrep['message']; ?>
                                </blockquote>
                                <?php
                            endforeach;
                        endif;
                        //debug($v);
                        echo $this->element("Syscm.../SyscmFront/Widgets/article_comment_list_pagination_reply", array('comment_paging'=>$v['replies']['comment_paging']));

                    endif;
                    ?>



                    <div class="reply-toggle">
                        <a href="#" class="" onclick="$('#reply-form-cont-<?php echo $i; ?>').toggle();
                                return false;"><span class="fa fa-reply"></span> <?php echo __d('syscm', 'write a reply'); ?></a>
                    </div>
                </div>
                <div class="reply-form-cont" id="reply-form-cont-<?php echo $i; ?>" style="display:none;">
                    <section class="col-md-12 well">

                        <form method="post">
                            <input type="hidden" name="_method" value="POST" />
                            <input type="hidden" name="reply_cid" value="<?php echo $v['cid']; ?>" />
                            <input type="hidden" name="comment_page" value="<?php
                            $comment_page = 1;
                            $named        = $this->request->param('named');

                            if (isset($named['comment_page'])):
                                $comment_page = $named['comment_page'];
                            endif;
                            echo $comment_page;
                            ?>" />
                            <div class="row">
                                <div class="col-md-12">
                                    <?php
                                    $reply_var = 'reply_name';
                                    ?>
                                    <input value="<?php echo isset(${$reply_var . '_data'}) ? ${$reply_var . '_data'} : ''; ?>" class="<?php echo isset(${$reply_var}) ? "form-control input-danger" : 'form-control'; ?>" name="<?php echo $reply_var; ?>" id="<?php echo $reply_var; ?>" placeholder="<?php echo __d('syscm', 'Name'); ?>" type="text" required="required">
                                    <?php
                                    echo isset(${$reply_var}['message']) ? $this->element('Syscm.front_error', array(
                                                'msg' => ${$reply_var}['message'])) : "";
                                    ?>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-xs-12">
                                    <?php
                                    $reply_var = 'reply_email';
                                    ?>
                                    <input value="<?php echo isset(${$reply_var . '_data'}) ? ${$reply_var . '_data'} : ''; ?>" class="<?php echo isset(${$reply_var}) ? "form-control input-danger" : 'form-control'; ?>" name="<?php echo $reply_var; ?>" id="<?php echo $reply_var; ?>" placeholder="<?php echo __d('syscm', 'Email'); ?>" type="email" required="required">
                                    <?php
                                    echo isset(${$reply_var}['message']) ? $this->element('Syscm.front_error', array(
                                                'msg' => ${$reply_var}['message'])) : "";
                                    ?>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <?php
                                    $reply_var = 'reply_telp';
                                    ?>
                                    <input value="<?php echo isset(${$reply_var . '_data'}) ? ${$reply_var . '_data'} : ''; ?>" class="<?php echo isset(${$reply_var}) ? "form-control input-danger" : 'form-control'; ?>" name="<?php echo $reply_var; ?>" id="<?php echo $reply_var; ?>" placeholder="<?php echo __d('syscm', 'Telp'); ?>" type="tel" required="required">
                                    <?php
                                    echo isset(${$reply_var}['message']) ? $this->element('Syscm.front_error', array(
                                                'msg' => ${$reply_var}['message'])) : "";
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <?php
                                    $reply_var = 'reply_message';
                                    ?>
                                    <textarea class="<?php echo isset(${$reply_var}) ? "form-control input-danger" : 'form-control'; ?>" name="<?php echo $reply_var; ?>" id="<?php echo $reply_var; ?>" placeholder="<?php echo __d('syscm', 'Your reply message'); ?>" rows="5"><?php echo isset(${$reply_var . '_data'}) ? ${$reply_var . '_data'} : ''; ?></textarea>
                                    <?php
                                    echo isset(${$reply_var}['message']) ? $this->element('Syscm.front_error', array(
                                                'msg' => ${$reply_var}['message'])) : "";
                                    ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <input class="button btn btn-info" value="<?php echo __d('syscm', 'Send Reply'); ?>" type="submit">
                                </div>
                            </div>

                        </form>
                    </section>
                </div>
            </div>

        </div><?php
    endforeach;
    // echo $this->element("Syscm.../SyscmFront/Widgets/article_comment_list_pagination", compact('comment_paging'));
    ?>
</div>
<?php
//debug($this->session->read());
//debug($comment_paging);
//debug($comment);
?>