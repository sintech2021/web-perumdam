<?php

/* <h4>Blockquote</h4>
  <blockquote>Fringilla nisl. Donec accumsan interdum nisi, quis tincidunt felis sagittis eget tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan faucibus. Vestibulum ante ipsum primis in faucibus lorem ipsum dolor sit amet nullam adipiscing eu felis.</blockquote>
 * 
 */

$list = $this->get('list', array());
if (!$list): return;
endif;
extract($list);
if (!isset($FrontContent)):return;
endif;

$title   = isset($FrontContent['text']) ? $this->Html->link($FrontContent['text'],$this->Html->url($FrontContent['name'], true)) : '';
$Content = isset($ScmContentContent) ? $ScmContentContent : array();
$content = isset($Content['value']) ? $Content['value'] : '';

$first_img  = $this->SyscmArticle->getFirstImg($content, false);
$invoke_img = "";
if ($first_img):
    $invoke_img = $this->Html->image($this->Html->url($first_img, true),
            array('class' => 'image left', 'width' => '20%'));
endif;
$content = $this->SyscmArticle->excerpt(filter_var($content, FILTER_SANITIZE_STRIPPED));
// $content .= '<br>'.$this->Html->link('... '.__d('syscm', 'read more'), $this->Html->url($FrontContent['name'], true));
printf('<div class="result-item"><h4><span class="fa fa-hand-pointer-o fa-rotate-90"></span> %s</h4>', $title);
printf('<blockquote>%s</blockquote></div>', $invoke_img . $content);
