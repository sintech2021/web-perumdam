<?php

/**
 * 
 * [title] => String,
 * [list] => 
 *      [] =>
 *          [title] => String,
 *          [alt] => String,
 *          [url] => full,
 *          [img_url] => image src,
 *          [target] => link target,
 * 
 */
$sidelist = $this->get('sidelist', false);
if (!$sidelist): return;
endif;
extract($sidelist);

$title = isset($title) ? $title : "";
$list  = isset($list) ? $list : array();
echo '<section class="box padding-0">';
echo '<div class="panel panel-primary">';
if ($title):
	echo '<div class="panel-heading">';
    echo sprintf('<h3 class="panel-title" ><div class="icon"><i class="fa %s fa-fw"></i></div> %s</h3>', $fa_icon, $title);
	echo '</div>';
endif;
if ($list):
	echo '<div class="panel-body no-padding">';
    echo '<ul class="list-group">';
    foreach ($list as $i):
        echo '<li class="list-group-item no-padding">';
        echo $this->Html->link($this->Html->image($this->Html->url($i['img_url'], true), array('class'=>'img-responsive')), $this->Html->url($i['url'], true),array('escape'=> false, 'target'=> $i['target']));
        
        echo '</li>';
    endforeach;
    echo '</ul>';
	echo '</div>';
endif;
echo '</div>';
echo '</section>';
?>