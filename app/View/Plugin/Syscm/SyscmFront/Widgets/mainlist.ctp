<?php
$mainlist = $this->get('mainlist', array());

if (!$mainlist): return;
endif;

extract($mainlist);

$title    = isset($title) ? $title : "";
$subtitle = isset($subtitle) ? $subtitle : "";
echo '<article>';
if ($title || $subtitle):
    echo '<header class="">';
    printf('<h4 class="header-mainlist">%s</h4>', h($title));
    if ($subtitle):
        printf('<p>%s</p>', $subtitle);
    endif;
    echo '</header>';
endif;


//list
$list = isset($list) ? $list : array();
if ($list):
    foreach ($list as $post):
        echo '<div class="col-md-12">';
        echo '<section class="well no-padding no-radius">';
        $post_subtitle = isset($post['subtitle']) ? sprintf('<em><small>%s</small></em>',
                        h($post['subtitle'])) : "";
        $post_author   = isset($post['author']) ? sprintf('<div class="meta-author"><small>%s</small></div>',
                        __d('syscm', 'Posted by : %s', $post['author'])) : "";
        $post_subtitle .= $post_author;
        $post_url      = isset($post['url']) ? $post['url'] : '#';
        $post_title    = isset($post['title']) ? sprintf('<h4>%s</h4>%s',
                        $this->Html->link(h($post['title']),
                                $this->Html->url($post_url, true),
                                array('style' => "text-decoration: none;border-bottom: none;line-height: 14pt;")),
                        $post_subtitle) : $post_subtitle;
        $post_header   = "";
        $post_date     = "";

        if (isset($post['date'])) {
            $strpubdate = strtotime($post['date']);
            $postday    = __d('syscm', date("l", $strpubdate));
            $postdate   = date("j", $strpubdate);
            $postmonth  = __d('syscm', date("F", $strpubdate));
            $postyear   = date("Y", $strpubdate);
            $posttime   = isset($post['time']) ? sprintf('<span class="posttime">%s</span>',
                            $post['time']) : '';
            $post_date  = sprintf(
                    '<span class="post-time">%s ' .
                    '%s ' .
                    '%s ' .
                    '%s ' .
                    '%s </span>', $postday, $postdate, $postmonth, $postyear, $posttime);
        }
        $post_header .= sprintf('<div class="row"><div class="col-md-12 padding-10"><div class="post-date"><span class="fa-stack fa-lg fa-2x post-date-calendar">
  <i class="fa fa-circle fa-stack-2x"></i>
  <i class="fa fa-calendar fa-stack-1x fa-inverse"></i>
</span>%s</div><div class="mainlist-title">%s</div></div></div>',
                $post_date, $post_title);
        $post_content = "";
        if (isset($post['content'])) {
            $first_img  = $this->SyscmArticle->getFirstImg($post['content'],
                    false);
            $invoke_img = "";
            if ($first_img) {
                $invoke_img = $this->Html->image($this->Html->url($first_img,
                                true),
                        array('class' => 'image', 'width' => '45%', 'style' => 'float:right; margin-left:10px; margin-top:10px;'));
            }
            $post_content = $invoke_img . $this->SyscmArticle->excerpt($post['content']);
            $post_content .= $this->Html->link(__d('syscm', 'read more'),
                    $this->Html->url($post_url, true));
        }
        printf('<div class="mainlist-item"><div class="mainlist-titles">%s</div><div class="mainlist-content">%s</div></div>',
                $post_header, $post_content);
        echo '<div style="clear:both;"></div>';
        echo '</section>';
        echo '</div>';
    endforeach;
    ?>
    <ul class="pagination">
    <?php
    echo $this->Paginator->prev('<span aria-hidden="true">&laquo;</span>',
            array('escape' => false, 'tag' => 'li'), null,
            array('class' => 'prev disabled'));
    echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a',
        'currentClass' => 'active', 'tag' => 'li', 'first' => 1));
    echo $this->Paginator->next('<span aria-hidden="true">&raquo;</span>',
            array('escape' => false, 'tag' => 'li'), null,
            array('class' => 'next disabled'));
    ?>
    </ul><?php
endif;
echo '</article>';
?>

