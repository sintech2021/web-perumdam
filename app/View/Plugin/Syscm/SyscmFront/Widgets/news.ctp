<?php

/**
 * 
 * [title] => String,
 * [list] => 
 *      [] =>
 *          [title] => String,
 *          [content] => String,
 *          [url] => CakeUrl
 * 
 */
$news_widget = $this->get('news_widget', false);
if (!$news_widget): return;
endif;
extract($news_widget);
$title = isset($title) ? $title : "";
$list  = isset($list) ? $list : array();
echo '<section>';
if ($title):
    echo sprintf('<h3 style="margin-bottom:5px;">%s</h3>', $title);
endif;
if ($list):
    echo '<ul class="links">';
    foreach ($list as $i):
        echo '<li style="line-height: 1.5em;">';
    $imUrl = $this->SyscmArticle->getFirstImg($i['content']);
        if ($imUrl):
            echo $this->Html->link(
                    $this->Html->image($imUrl), $this->Html->url($i['url'], true),
                    array('escape' => false, 'class' => 'image left', 'style'=> "margin: 1px;"));
        endif;
        echo sprintf('<span style="line-height:0;">%s : </span>',
                $this->Html->link($i['title'], $this->Html->url($i['url'], true)));
        $len = '50';
        $text = substr(filter_var($i['content'], FILTER_SANITIZE_STRIPPED), 0, $len);
        echo sprintf('%s',$text);
        if(strlen($i['content']) > $len):
            echo "...";
        endif;
        echo '</li>';
    endforeach;
    echo '</ul>';
endif;
echo '</section>';
/*
  ?>

  <footer>
  <a href="#" class="button">More Random Links</a>
  </footer> <?php
 * 
 */
?>