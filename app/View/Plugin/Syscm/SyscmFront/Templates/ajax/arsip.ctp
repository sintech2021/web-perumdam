<link href="<?=BASE_URL.'/'?>pub/bstreeview.min.css" rel="stylesheet"  crossorigin="anonymous">
<script src="<?=BASE_URL.'/'?>pub/bstreeview.min.js"  crossorigin="anonymous"></script>

  <script type="text/javascript">
  <?php
  	$scripts = ['Arsip.js','PerumdamtkrWebArsip.js','PageBanner.js'];//
  	$component_path = realpath(__DIR__.'/../../../../../../webroot/com');
  	$str = '';
  	foreach($scripts as $filename){
  		$str .= file_get_contents($component_path . '/' .$filename) . "\n";
  	}
  	$str .= "ReactDOM.render(<React.StrictMode> <PerumdamtkrWebArsip is_ajax={true}/> </React.StrictMode>, document.getElementById('main'));"; 

  	?>
    bable_scripts_input = <?=json_encode($str)?>;
    var input = bable_scripts_input;
	var output = Babel.transform(input, { presets:['react'],plugins: [
		["proposal-class-properties", { "loose": true }]
	]}).code;
	<? 

    $pagination = base64_encode($this->Paginator->prev('<span aria-hidden="true">&laquo;</span>',
            array('escape' => false, 'tag' => 'li'), null,
            array('class' => 'prev disabled')) . $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a',
        'currentClass' => 'active', 'tag' => 'li', 'first' => 1)) . $this->Paginator->next('<span aria-hidden="true">&raquo;</span>',
            array('escape' => false, 'tag' => 'li'), null,
            array('class' => 'next disabled'))
    );
    
    $featured_heading = $this->get('featured_heading');
    $sidelist = $this->get('sidelist');
    $mainlist = $this->get('mainlist');
    $formatted_slug = $this->get('formatted_slug');
    $article = $this->get('article');
    $syscm_contents = $this->get('syscm_contents');
    // print_r(json_encode($featured_contents));
    ?>
    app_data = {
        pagination : '<?=$pagination?>',
        menu : <?=json_encode($syscm_main_nav,JSON_PRETTY_PRINT)?>,
        featured_heading : <?=json_encode($featured_heading,JSON_PRETTY_PRINT)?>,
        sidelist : <?=json_encode($sidelist,JSON_PRETTY_PRINT)?>,
        mainlist : <?=json_encode($mainlist,JSON_PRETTY_PRINT)?>,
        article : <?=json_encode($article,JSON_PRETTY_PRINT)?>,
        syscm_contents : <?=json_encode($syscm_contents,JSON_PRETTY_PRINT)?>,
        formatted_slug : 'arsip',
    };
    // console.log(atob(app_data.pagination));
    app_data.mainlist.latest = app_data.mainlist.list.splice(0,2);
    document.title = 'Arsip Berita - PERUMDAM TKR';
	eval(output)
  </script>