<?php
$json = $this->get('list',['list'=>[]]);
$json['success'] = true;
$json['pagination'] = [
	'text'=> $this->Paginator->counter(array('format' => __('Laman {:page} dari {:pages}, tampil {:current} record dari {:count} total, dimulai dari {:start} s.d {:end}'))),
	'links' => $this->Paginator->prev(__('<<'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a')) .
               $this->Paginator->numbers(array('modulus'=>(is_mobile()?2:5),'separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1)) .
               $this->Paginator->next(__('>>'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a')) 
];
?>
<?=json_encode($json)?>