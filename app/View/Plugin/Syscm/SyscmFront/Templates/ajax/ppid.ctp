
  <script type="text/javascript">
  <?php
  	$scripts = ['CommentBox.js','PageBanner.js','RelatedContent.js', 'PerumdamtkrWebPpid.js'];//
  	$component_path = realpath(__DIR__.'/../../../../../../webroot/com');
  	$str = '';
  	foreach($scripts as $filename){
  		$str .= file_get_contents($component_path . '/' .$filename) . "\n";
  	}
    $_data = [
      't' => 'post',
      'content' => ''
    ];
    $post = '';

    if($article){
      $this->set('article',$article);
      $_data['content']  = $this->element("Syscm.../SyscmFront/Widgets/article");
      $_data['content'] .= $this->element("Syscm.../SyscmFront/Widgets/article_comment");
    }else{
      $post = "<article> <header> <h2>". __d('syscm', 'Content Unavailable.')."</h2> <p>". __d('syscm', 'Nothing to display.')."</p> </header> </article>"; 
    }
        
    $data =  json_encode($_data);
    $content_id = $article['id'];
    $comment_enabled = isset($article['comment_enabled'])?($article['comment_enabled']?1:0):0;
    $str .= <<<EOD
class SinglePost extends React.Component{   
  render(){
    let post = '$post';
    const data = $data;
    if(data.content == ''){
      data.content = post;
    }
    const content_id = '$content_id';
    let comment_enabled = $comment_enabled;
    comment_enabled = comment_enabled == 1 ? true : false;
    return (
      <div className="container">
        <div className="content content-main" style={{padding:'0'}}>
          <div className="article" dangerouslySetInnerHTML={{__html:data.content}}></div>
          <div className="article-comment-cnt">
          {comment_enabled && (<CommentBox id={content_id} ref={(element) => {window.comentBoxComponent = element}}/>)}
          </div>
        </div>
      </div>
    )
  }
}
EOD;
  	$str .= "ReactDOM.render(<React.StrictMode> <PerumdamtkrWebPpid is_ajax={true}/> </React.StrictMode>, document.getElementById('main'));"; 

  	?>
    bable_scripts_input = <?=json_encode($str)?>;
    var input = bable_scripts_input;
	var output = Babel.transform(input, { presets:['react'],plugins: [
		["proposal-class-properties", { "loose": true }]
	]}).code;
	<? 

    $featured_contents = $this->get('featured_contents');
    $sidelist_right = $this->get('sidelist_right');
    $top_mainlist = $this->get('top_mainlist');
    $article = $this->get('article');
    // print_r(json_encode($featured_contents));
    ?>
    app_data = {
        menu : <?=json_encode($syscm_main_nav,JSON_PRETTY_PRINT)?>,
        carousel : <?=json_encode($featured_contents,JSON_PRETTY_PRINT)?>,
        sidelist_right : <?=json_encode($sidelist_right,JSON_PRETTY_PRINT)?>,
        top_mainlist : <?=json_encode($mainlist,JSON_PRETTY_PRINT)?>,
        article : <?=json_encode($article,JSON_PRETTY_PRINT)?>,
        formatted_slug : '<?=$this->get('segment_key')?>',
    };
    // console.log(atob(app_data.pagination));
    document.title = 'PPID / <?=$article['title']?> - PERUMDAM TKR';
	eval(output)
  </script>