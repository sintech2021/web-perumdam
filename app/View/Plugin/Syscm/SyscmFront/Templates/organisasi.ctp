<?=$this->element("Syscm.../SyscmFront/Widgets/root_with_progress_bar");?>
 

        
        <!-- Bootstrap core CSS -->
    <link href="<?=BASE_URL.'/'?>node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"  crossorigin="anonymous">
    <link href="<?=BASE_URL.'/'?>node_modules/opensans-npm-webfont/style.css" rel="stylesheet"  crossorigin="anonymous">
    <link href="<?=BASE_URL.'/'?>node_modules/opensans-npm-webfont/style.css" rel="stylesheet"  crossorigin="anonymous">
    <link href="<?=BASE_URL.'/'?>node_modules/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet"  crossorigin="anonymous">
    <link href="<?=BASE_URL.'/'?>node_modules/plyr/dist/plyr.css" rel="stylesheet"  crossorigin="anonymous">
    <!-- Custom styles for this template -->
        <link href="<?=BASE_URL.'/'?>styles.css" rel="stylesheet">
        <link href="<?=BASE_URL.'/'?>bootstrap-overide.css" rel="stylesheet">
    
      
    
    
      <!-- Marketing messaging and featurettes
      ================================================== -->
      <!-- Wrap the rest of the page in another container to center all the content. -->
    <script type="text/babel">
      class SinglePost extends React.Component{

        render(){
          // ////////////////////////////
          let post = '';
          <?php
            $_data = [
              't' => post,
              'content' => ''
            ];
            $article = $this->get('article',array());
            if($article):
                $this->set('article',$article);
                $_data['content'] = $this->element("Syscm.../SyscmFront/Widgets/article");
            else:
            ?>
          post = (<article>
                <header>
                    <h2><?php echo __d('syscm', 'Content Unavailable.'); ?></h2>
                    <p><?php echo __d('syscm', 'Nothing to display.'); ?></p>
                </header>
            </article>)
            <?php        
            endif;
            $_data['content'] .= $this->element("Syscm.../SyscmFront/Widgets/article_comment");
            ?>
          const data = <?=json_encode($_data)?>  
          //////////////////////////////

          return (
            <div class="container">
            <div class="content" style={{padding:'6em'}}>
            <div class="article" dangerouslySetInnerHTML={{__html:data.content}}></div>
            </div>
            </div>
          )
        }
      }
       </script>  
    <script type="text/javascript">
        site_url = ()=>{
            return '<?=BASE_URL?>';
    
        }
        <? 
    
        $featured_heading = $this->get('featured_heading');
        $sidelist = $this->get('sidelist');
        $mainlist = $this->get('mainlist');
        $formatted_slug = $this->get('formatted_slug');
        $article = $this->get('article');
        $syscm_contents = $this->get('syscm_contents');
        // print_r(json_encode($featured_contents));
        ?>
        app_data = {
            // pagination : '<?=$pagination?>',
            menu : <?=json_encode($syscm_main_nav,JSON_PRETTY_PRINT)?>,
            featured_heading : <?=json_encode($featured_heading,JSON_PRETTY_PRINT)?>,
            sidelist : <?=json_encode($sidelist,JSON_PRETTY_PRINT)?>,
            top_mainlist : <?=json_encode($mainlist,JSON_PRETTY_PRINT)?>,
            article : <?=json_encode($article,JSON_PRETTY_PRINT)?>,
            syscm_contents : <?=json_encode($syscm_contents,JSON_PRETTY_PRINT)?>,
            formatted_slug : '<?=$this->get('segment_key')?>',
        };
    </script>                  
<?if(ENABLE_CDN):?>

<script src="https://code.jquery.com/jquery-3.1.0.min.js"  crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" crossorigin="anonymous"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.min.js" crossorigin="anonymous"></script> 
<script crossorigin src="https://unpkg.com/react@16/umd/react.production.min.js"></script>
<script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.production.min.js"></script>
<script crossorigin src="https://cdn.jsdelivr.net/npm/@babel/standalone@7.11.3/babel.min.js"></script>
<?else:?>
<script src="<?=BASE_URL.'/'?>jquery-3.5.1.slim.min.js"  crossorigin="anonymous"></script>
  <script src="<?=BASE_URL.'/'?>node_modules/bootstrap/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script> 
  <script src="<?=BASE_URL.'/'?>node_modules/axios/dist/axios.min.js" crossorigin="anonymous"></script> 
  <script src="<?=BASE_URL.'/'?>node_modules/react/umd/react.production.min.js" crossorigin="anonymous"></script> 
  <script src="<?=BASE_URL.'/'?>node_modules/react-dom/umd/react-dom.production.min.js" crossorigin="anonymous"></script> 
  <script crossorigin src="<?=BASE_URL.'/'?>node_modules/@babel/standalone/babel.min.js"></script>

<?endif?>
      <script  type="text/javascript" src="<?=BASE_URL.'/'?>com/app/Proxy.js"></script> 
<script  type="text/javascript" src="<?=BASE_URL.'/'?>com/app/Store.js"></script> 
      <script  type="text/babel" src="<?=BASE_URL.'/'?>com/RelatedContent.js" crossorigin="anonymous"></script> 
      <script  type="text/babel" src="<?=BASE_URL.'/'?>com/PageBanner.js" crossorigin="anonymous"></script> 
      <script  type="text/babel" src="<?=BASE_URL.'/'?>com/MyCompany.js" crossorigin="anonymous"></script> 
      <script  type="text/babel" src="<?=BASE_URL.'/'?>com/Footer.js" crossorigin="anonymous"></script> 
      <script  type="text/babel" src="<?=BASE_URL.'/'?>com/SearchBar.js" crossorigin="anonymous"></script> 
      <script  type="text/babel" src="<?=BASE_URL.'/'?>com/MenuItem.js" crossorigin="anonymous"></script> 
      <script  type="text/babel" src="<?=BASE_URL.'/'?>com/Header.js" crossorigin="anonymous"></script> 
      <script  type="text/babel" src="<?=BASE_URL.'/'?>com/PerumdamtkrWebOrganisasi.js" crossorigin="anonymous"></script> 
      <script  type="text/babel" src="<?=BASE_URL.'/'?>com/index.web.js" crossorigin="anonymous"></script> 
    
      <script type="text/babel">
        ReactDOM.render(
            <React.StrictMode>
                <PerumdamtkrWebOrganisasi/>
            </React.StrictMode>, 
        document.getElementById('root'));
      </script>