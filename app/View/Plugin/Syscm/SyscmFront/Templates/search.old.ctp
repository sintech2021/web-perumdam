<!-- Main -->
<section class="wrapper style1 padding-0">
        <div class="row">
            <div class="col-md-8 col-xs-12">
                <div id="content">

                    <!-- Content -->
                    <article>
                        <header>
                            <?php
                            $searchq = $this->get('searchq', __d('syscm', 'missing keyword...'));
                            ?>
                            <h2><?php echo __d('syscm', 'Search result for: <small>%s</small>', $searchq);?></h2>
                        <?php
                        $data_list = $this->get('data_list', array());
                        if (!$data_list):
                            ?>
                            <p><?php echo __d('syscm', 'No matching content found. Please refine your search.');?></p>
                            <?php
                            else:
                                ?><p><?php echo $this->Paginator->counter(array(
            'format' => __d('sysadmin','Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
        ));
?></p><?php
                        endif;
                        
                        ?></header><?php
                        
                        if($data_list):
                            
                            foreach ($data_list as $list):
                                $this->set('list', $list);
                                echo $this->element("Syscm.../SyscmFront/Widgets/searchlist");
                            endforeach;
                            echo $this->element('paginator', array(), array('plugin' => 'syscm')); 
                         endif; ?>
                    </article>
                   
                </div>
            </div>
            
        </div>
</section>