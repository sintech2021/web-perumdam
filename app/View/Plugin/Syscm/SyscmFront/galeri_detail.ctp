<?=$this->element("Syscm.../SyscmFront/Widgets/root_with_progress_bar");?>
 
   


    <script type="text/javascript">
    site_url = ()=>{
        return '<?=BASE_URL?>';

    }
    get_segment_key = ()=>{
   

       return '<?=$this->get('segment_key')?>'
    }
    <?
   
    $pagination = base64_encode($this->Paginator->prev('<span aria-hidden="true">&laquo;</span>',
            array('escape' => false, 'tag' => 'li'), null,
            array('class' => 'prev disabled')) . $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a',
        'currentClass' => 'active', 'tag' => 'li', 'first' => 1)) . $this->Paginator->next('<span aria-hidden="true">&raquo;</span>',
            array('escape' => false, 'tag' => 'li'), null,
            array('class' => 'next disabled'))
    );
    
    $featured_heading = $this->get('featured_heading');
    $sidelist = $this->get('sidelist');
    $mainlist = $this->get('mainlist');
    $formatted_slug = $this->get('formatted_slug');
    $article = $this->get('article');
    $syscm_contents = $this->get('syscm_contents');
    $item_id = $this->get('item_id');
    $detail = $this->get('detail');
    // print_r(json_encode($featured_contents));
    ?>
    app_data = {
        pagination : '<?=$pagination?>',
        menu : <?=json_encode($syscm_main_nav,JSON_PRETTY_PRINT)?>,
        featured_heading : <?=json_encode($featured_heading,JSON_PRETTY_PRINT)?>,
        sidelist : <?=json_encode($sidelist,JSON_PRETTY_PRINT)?>,
        mainlist : <?=json_encode($mainlist,JSON_PRETTY_PRINT)?>,
        article : <?=json_encode($article,JSON_PRETTY_PRINT)?>,
        syscm_contents : <?=json_encode($syscm_contents,JSON_PRETTY_PRINT)?>,
        formatted_slug : 'galeri_detail',
        item_id : '<?=$item_id?>',
        detail : <?=json_encode($detail)?>,
    };
    // console.log(atob(app_data.pagination));
    // app_data.mainlist.latest = app_data.mainlist.list.splice(0,2);

</script>
    <!-- Bootstrap core CSS -->
<link href="<?=BASE_URL.'/'?>node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"  crossorigin="anonymous">
<link href="<?=BASE_URL.'/'?>node_modules/opensans-npm-webfont/style.css" rel="stylesheet"  crossorigin="anonymous">
<link href="<?=BASE_URL.'/'?>node_modules/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet"  crossorigin="anonymous">
<link href="<?=BASE_URL.'/'?>pub/bstreeview.min.css" rel="stylesheet"  crossorigin="anonymous">

<!-- <link href="<?=BASE_URL.'/'?>node_modules/plyr/dist/plyr.css" rel="stylesheet"  crossorigin="anonymous"> -->
<!-- Custom styles for this template -->
    <link href="<?=BASE_URL.'/'?>styles.css" rel="stylesheet">
    <link href="<?=BASE_URL.'/'?>bootstrap-overide.css" rel="stylesheet">
<?if(ENABLE_CDN):?>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" crossorigin="anonymous"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.min.js" crossorigin="anonymous"></script> 
<script crossorigin src="https://unpkg.com/react@16/umd/react.production.min.js"></script>
<script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.production.min.js"></script>
<script crossorigin src="https://cdn.jsdelivr.net/npm/@babel/standalone@7.11.3/babel.min.js"></script>
<?else:?>
  <script src="<?=BASE_URL.'/'?>node_modules/bootstrap/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script> 
  <script src="<?=BASE_URL.'/'?>node_modules/axios/dist/axios.min.js" crossorigin="anonymous"></script> 
  <script src="<?=BASE_URL.'/'?>node_modules/react/umd/react.production.min.js" crossorigin="anonymous"></script> 
  <script src="<?=BASE_URL.'/'?>node_modules/react-dom/umd/react-dom.production.min.js" crossorigin="anonymous"></script> 
  <script crossorigin src="<?=BASE_URL.'/'?>node_modules/@babel/standalone/babel.min.js"></script>

<?endif?>
<script src="<?=BASE_URL.'/'?>pub/bstreeview.min.js"  crossorigin="anonymous"></script>
<script  type="text/javascript" src="<?=BASE_URL.'/'?>com/app/Proxy.js"></script> 
<script  type="text/javascript" src="<?=BASE_URL.'/'?>com/app/Store.js"></script>   
  <script  type="text/babel" src="<?=BASE_URL.'/'?>com/GaleriDetail.js" crossorigin="anonymous"></script> 
  <script  type="text/babel" src="<?=BASE_URL.'/'?>com/SearchBar.js" crossorigin="anonymous"></script> 
  <script  type="text/babel" src="<?=BASE_URL.'/'?>com/MenuItem.js" crossorigin="anonymous"></script> 
  <script  type="text/babel" src="<?=BASE_URL.'/'?>com/MyCompany.js" crossorigin="anonymous"></script> 
  <script  type="text/babel" src="<?=BASE_URL.'/'?>com/PageBanner.js" crossorigin="anonymous"></script> 
  <script  type="text/babel" src="<?=BASE_URL.'/'?>com/Footer.js" crossorigin="anonymous"></script> 
  <script  type="text/babel" src="<?=BASE_URL.'/'?>com/Header.js" crossorigin="anonymous"></script> 
  <script  type="text/babel" src="<?=BASE_URL.'/'?>com/PerumdamtkrWebGaleriDetail.js" crossorigin="anonymous"></script> 
  <script  type="text/babel" src="<?=BASE_URL.'/'?>com/index.web.js" crossorigin="anonymous"></script> 

  <script type="text/babel">
    ReactDOM.render(
        <React.StrictMode>
            <PerumdamtkrWebGaleriDetail/>
        </React.StrictMode>, 
    document.getElementById('root'));
  </script>