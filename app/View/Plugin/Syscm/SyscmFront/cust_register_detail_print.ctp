<?php
$reg_content = $this->get('cust_reg_content', array());
if ($reg_content):
    
    extract($reg_content);
endif;
?><section class="wrapper style1 padding-0">
    <article class="row">
        <div class="article-content box box-override">
            <section class="col-md-12">
                <header>
                    <div class="pull-right btn-group no-print">
                        <button class="btn btn-default" type="button" onclick="window.print();"><span class="fa fa-print"></span> <?php echo __d('syscm', 'Print');?></button>
                    </div>
                    <h5><?php echo __d('syscm', 'Customer Registration Detail');
?></h5>
                </header>
                <div class="row">
                    <div class="col-md-12 well">
                        <table class="table table-striped">
                            <tr><td><?php echo __d('syscm', 'Registration Code'); ?></td><td>:</td><td><strong><?php echo isset($cust_reg_code) ? $cust_reg_code : ""; ?></strong></td></tr>
                            <tr><td><?php echo __d('syscm', 'Registration Date'); ?></td><td>:</td><td><strong><?php echo isset($cust_reg_dt) ? $cust_reg_dt : ""; ?></strong></td></tr>
                            <tr><td><?php echo __d('syscm', 'Full Name'); ?></td><td>:</td><td><strong><?php echo isset($cust_reg_name) ? $cust_reg_name : ""; ?></strong></td></tr>
                            <tr><td><?php echo __d('syscm', 'Phone Number'); ?></td><td>:</td><td><strong><?php echo isset($cust_reg_phone) ? $cust_reg_phone : ""; ?></strong></td></tr>
                            <tr><td><?php echo __d('syscm', 'Cellular Number'); ?></td><td>:</td><td><strong><?php echo isset($cust_reg_cell) ? $cust_reg_cell : ""; ?></strong></td></tr>
                            <tr><td><?php echo __d('syscm', 'Email'); ?></td><td>:</td><td><strong><?php echo isset($cust_reg_email) ? $cust_reg_email : ""; ?></strong></td></tr>
                            <tr><td><?php echo __d('syscm', 'Address'); ?></td><td>:</td><td><strong><?php echo isset($cust_reg_addr) ? $cust_reg_addr : ""; ?></strong></td></tr>
                            <tr><td><?php echo __d('syscm', 'Your IP Address'); ?></td><td>:</td><td><strong><?php echo isset($cust_reg_ip) ? $cust_reg_ip : ""; ?></strong></td></tr>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </article>
</section>
<div id="footer" class="footer fit-margin padding-10 text-center">
          
                <!-- Copyright -->
                <div class="copyright">
                    &copy; <?php
                            echo Configure::read('Sysconfig.app.copy');
                            ?>&nbsp;<?php
                            echo Configure::read('Sysconfig.company.name');
                            ?>. All rights reserved</li><!--li>Design: <a href="http://html5up.net" target="_blank">HTML5 UP</a></li-->
                    
                </div>

            </div>