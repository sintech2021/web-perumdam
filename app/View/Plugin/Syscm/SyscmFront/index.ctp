<?=$this->element("Syscm.../SyscmFront/Widgets/root_with_progress_bar");?>

   


    <script type="text/javascript">
	site_url = ()=>{
		return '<?=BASE_URL?>';

	}
	<? 

	$featured_contents = $this->get('featured_contents');
  $sidelist_right = $this->get('sidelist_right');
	$sidelist = $this->get('sidelist');
	$top_mainlist = $this->get('top_mainlist');
	$homepage_popup_content = $this->get('homepage_popup_content');
	?>
	app_data = {
		menu : <?=json_encode($syscm_main_nav,JSON_PRETTY_PRINT)?>,
		carousel : <?=json_encode($featured_contents,JSON_PRETTY_PRINT)?>,
		sidelist_right : <?=json_encode($sidelist_right,JSON_PRETTY_PRINT)?>,
    top_mainlist : <?=json_encode($top_mainlist,JSON_PRETTY_PRINT)?>,
    sidelist : <?=json_encode($sidelist,JSON_PRETTY_PRINT)?>,
		homepage_popup_content : <?=json_encode($homepage_popup_content,JSON_PRETTY_PRINT)?>,
	}
</script>
<?if(ENABLE_CDN):?>
    <!-- Bootstrap core CSS -->
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet"  crossorigin="anonymous">
<link href="https://cdnjs.cloudflare.com/ajax/libs/plyr/3.6.2/plyr.css" rel="stylesheet"  crossorigin="anonymous">
<?else:?>
<link href="<?=BASE_URL.'/'?>node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"  crossorigin="anonymous">
<link href="<?=BASE_URL.'/'?>node_modules/plyr/dist/plyr.css" rel="stylesheet"  crossorigin="anonymous">
<?endif?>
<link href="<?=BASE_URL.'/'?>node_modules/opensans-npm-webfont/style.css" rel="stylesheet"  crossorigin="anonymous">
<link href="<?=BASE_URL.'/'?>node_modules/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet"  crossorigin="anonymous">
<!-- Custom styles for this template -->
    <link href="<?=BASE_URL.'/'?>carousel.css" rel="stylesheet">
    <link href="<?=BASE_URL.'/'?>bootstrap-overide.css" rel="stylesheet">
    <link href="<?=BASE_URL.'/'?>styles.css?ver=<?=gen_uuid()?>" rel="stylesheet">

<?if(!ENABLE_CDN):?>


  <script src="<?=BASE_URL.'/'?>node_modules/bootstrap/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script> 
  <script src="<?=BASE_URL.'/'?>node_modules/plyr/dist/plyr.min.js" crossorigin="anonymous"></script> 
  <script src="<?=BASE_URL.'/'?>node_modules/axios/dist/axios.min.js" crossorigin="anonymous"></script> 
  <script src="<?=BASE_URL.'/'?>pub/imagesloaded.min.js" crossorigin="anonymous"></script> 
 
  <script src="<?=BASE_URL.'/'?>node_modules/masonry-layout/dist/masonry.pkgd.min.js" crossorigin="anonymous"></script> 
  <script src="<?=BASE_URL.'/'?>node_modules/react/umd/react.production.min.js" crossorigin="anonymous"></script> 
  <script src="<?=BASE_URL.'/'?>node_modules/react-dom/umd/react-dom.production.min.js" crossorigin="anonymous"></script> 
  <script crossorigin src="<?=BASE_URL.'/'?>node_modules/@babel/standalone/babel.min.js"></script>
  <script type="text/javascript" src="<?=BASE_URL.'/'?>fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
  <script type="text/javascript" src="<?=BASE_URL.'/'?>fancybox/jquery.fancybox-1.3.4.js"></script>
  <link rel="stylesheet" type="text/css" href="<?=BASE_URL.'/'?>fancybox/jquery.fancybox-1.3.4.css" media="screen" />
<?else:?>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" crossorigin="anonymous"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/plyr/3.6.2/plyr.min.js" crossorigin="anonymous"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.min.js" crossorigin="anonymous"></script> 

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.1.8/imagesloaded.pkgd.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/masonry/4.2.2/masonry.pkgd.min.js" crossorigin="anonymous"></script> 
<script crossorigin src="https://unpkg.com/react@16/umd/react.production.min.js"></script>
<script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.production.min.js"></script>
<script crossorigin src="https://cdn.jsdelivr.net/npm/@babel/standalone@7.11.3/babel.min.js"></script>

<?endif?>
 <script  type="text/javascript" src="<?=BASE_URL.'/'?>com/app/Proxy.js"></script> 
  <script  type="text/javascript" src="<?=BASE_URL.'/'?>com/app/Store.js"></script>
  <script  type="text/babel" src="com/TentangKami.js?v=1" crossorigin="anonymous"></script> 
  <script  type="text/babel" src="com/Footer.js" crossorigin="anonymous"></script> 
  <script  type="text/babel" src="com/MyCompany.js" crossorigin="anonymous"></script> 
  <script  type="text/babel" src="com/MyGalery.js" crossorigin="anonymous"></script> 
  <script  type="text/babel" src="com/MyNews.js?v=<?=gen_uuid()?>" crossorigin="anonymous"></script> 
  <script  type="text/babel" src="com/ServiceButton.js" crossorigin="anonymous"></script> 
  <script  type="text/babel" src="com/MyCarousel.js?v=<?=gen_uuid()?>" crossorigin="anonymous"></script> 
  <script  type="text/babel" src="com/RunningText.js?v=<?=gen_uuid()?>" crossorigin="anonymous"></script> 
  <script  type="text/babel" src="com/InfoPerumdam.js?v=<?=gen_uuid()?>" crossorigin="anonymous"></script> 
  <script  type="text/babel" src="com/SearchBar.js" crossorigin="anonymous"></script> 
  <script  type="text/babel" src="com/MenuItem.js" crossorigin="anonymous"></script> 
  <script  type="text/babel" src="com/Header.js" crossorigin="anonymous"></script> 
  <script  type="text/babel" src="com/PerumdamtkrWeb.js" crossorigin="anonymous"></script> 
  <script  type="text/babel" src="com/index.web.js" crossorigin="anonymous"></script> 

  <script type="text/babel">
  	ReactDOM.render(
		<React.StrictMode>
			<PerumdamtkrWeb/>
		</React.StrictMode>, 
	document.getElementById('root'));
  </script>