<html>
	<head>
		<title><?=$this->get('page_title')?></title>
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet"  crossorigin="anonymous">
<link href="<?=BASE_URL.'/'?>node_modules/opensans-npm-webfont/style.css" rel="stylesheet"  crossorigin="anonymous">
<link href="<?=BASE_URL.'/'?>node_modules/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet"  crossorigin="anonymous">
<link href="https://cdnjs.cloudflare.com/ajax/libs/plyr/3.6.2/plyr.css" rel="stylesheet"  crossorigin="anonymous">
<!-- Custom styles for this template -->
    <link href="<?=BASE_URL.'/'?>carousel.css" rel="stylesheet">
    <link href="<?=BASE_URL.'/'?>bootstrap-overide.css" rel="stylesheet">
    <link href="<?=BASE_URL.'/'?>styles.css?ver=<?=gen_uuid()?>" rel="stylesheet">
    <link href="<?=BASE_URL.'/'?>bootstrap-overide.css" rel="stylesheet">



<script src="https://code.jquery.com/jquery-3.1.0.min.js"  crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" crossorigin="anonymous"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/plyr/3.6.2/plyr.min.js" crossorigin="anonymous"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.min.js" crossorigin="anonymous"></script> 
<script  type="text/javascript" src="<?=BASE_URL.'/'?>com/app/Proxy.js"></script> 
<script  type="text/javascript" src="<?=BASE_URL.'/'?>com/app/Store.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.1.8/imagesloaded.pkgd.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/masonry/4.2.2/masonry.pkgd.min.js" crossorigin="anonymous"></script> 
<script crossorigin src="https://unpkg.com/react@16/umd/react.production.min.js"></script>
<script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.production.min.js"></script>
<!-- <script crossorigin src="https://cdn.jsdelivr.net/npm/@babel/standalone@7.11.3/babel.min.js"></script> -->
<script crossorigin src="https://cdn.jsdelivr.net/npm/@babel/standalone@7.11.3/babel.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css" media="screen" />

  <script  type="text/babel" src="com/HomePage.js?v=<?=gen_uuid()?>" crossorigin="anonymous"></script> 
  <script  type="text/babel" src="com/TentangKami.js?v=211" crossorigin="anonymous"></script> 
  <script  type="text/babel" src="com/Footer.js" crossorigin="anonymous"></script> 
  <script  type="text/babel" src="com/MyCompany.js" crossorigin="anonymous"></script> 
  <script  type="text/babel" src="com/MyGalery.js?v=111" crossorigin="anonymous"></script> 
  <script  type="text/babel" src="com/MyNews.js?v=<?=gen_uuid()?>" crossorigin="anonymous"></script> 
  <script  type="text/babel" src="com/ServiceButton.js" crossorigin="anonymous"></script> 
  <script  type="text/babel" src="com/MyCarousel.js?v=<?=gen_uuid()?>" crossorigin="anonymous"></script> 
  <script  type="text/babel" src="com/SearchBar.js" crossorigin="anonymous"></script> 
  <script  type="text/babel" src="com/MenuItem.js" crossorigin="anonymous"></script> 
  <script  type="text/babel" src="com/Header.js" crossorigin="anonymous"></script> 
  		<script  type="text/babel" src="<?=BASE_URL.'/'?>com/index.web.js" crossorigin="anonymous"></script>
  		<script  type="text/babel" src="<?=BASE_URL.'/'?>com/index.web-v1.js" crossorigin="anonymous"></script>
  		<script type="text/javascript">
  			function site_url(){return '<?=BASE_URL.'/'?>'}
  		</script>
		 
	</head>
	<body>
<?=$this->element("Syscm.../SyscmFront/Widgets/root_with_progress_bar");?>

	</body>
</html>

<script type="text/babel">
	setTimeout(()=>{
		ReactDOM.render(
		    <React.StrictMode>
		        <Web/>
		    </React.StrictMode>, 
		document.getElementById('root'));
	},250)
</script>