<?php
App::uses('AppShell', 'Console/Command');
App::uses('CakeString', 'Utility');
class ProfilerTask extends AppShell {
    public $tasks = array('Profiler');
    
    public function getOptionParser() {
        $parser = parent::getOptionParser();
        return $parser->description('Model Profiling');
                     
    }
    public function execute() {
        echo 'Executing ....' . "\n";
        $model = ClassRegistry::init('Consultation');

        $ls = $model->findAll();

        print_r($ls);
    }
}