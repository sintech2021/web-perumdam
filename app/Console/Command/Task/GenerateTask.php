<?php
App::uses('AppShell', 'Console/Command');
App::uses('CakeString', 'Utility');
class GenerateTask extends AppShell {
    public $tasks = array('Generate');
    public function execute() {
        $method = $this->args[0];
        
        if(method_exists($this, $method)){
          $this->$method();
        }
    }
    public function getOptionParser() {
        $parser = parent::getOptionParser();
        return $parser->description('Artisan Console')
                      ->addArgument('method', array('required' => true,'help' => __('Method')))
                      ; 
        }
    public function test()
    {
      echo "This is a test";
    }
}
