<?php
App::uses('AppShell', 'Console/Command');
App::uses('CakeString', 'Utility');
class UtilTask extends AppShell {
    public $tasks = array('Generate');
    public $uses             = array(
        'Syscm.FrontModel',
        'Syscm.Pendaftaran',
        'Syscm.ScmContent',
        'Syscm.ScmContentContent',
        'Syscm.ScmContentPublished',
        'Syscm.ScmContentMeta',
        'Syscm.FrontContent',
        'Syscm.ScmCustComment',
        'Syscm.ScmCustReg',
        'Syscm.ScmContentMedia',
        'Syscm.ScmCustBill',
        'Syscm.ScmCustFeedback',
        'Syscm.ScmCustFeedbackRespon',
        'Syscm.ScmCustInfo',
        'Syscm.ScmCategory',
        'Syscm.Consultation'
    );
    public function execute() {
        $method = $this->args[0];
        
        if(method_exists($this, $method)){
          $this->$method();
        }
    }
    public function getOptionParser() {
        $parser = parent::getOptionParser();
        return $parser->description('Artisan Util')
                      ->addArgument('method', array('required' => true,'help' => __('Method')))
                      ; 
        }
    public function test()
    {
      echo "This is a test";
      $rs = $this->FrontContent->getFeaturedContents();
      print_r($rs);
    }
}
