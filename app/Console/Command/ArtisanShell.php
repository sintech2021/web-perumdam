<?php
App::uses('AppShell', 'Console/Command');
class ArtisanShell extends AppShell {         
    public $tasks = array('Generate','Util');
    public function main() {
        $this->out($this->getOptionParser()->help());
    }
    public function getOptionParser() {
        $parser = parent::getOptionParser();
        return $parser->description('Welcome to artisan')
                    ->addSubcommand('generate', array( 'help' => __('Generates anythings file'),
                                                        'parser' => $this->Generate->getOptionParser()
                                                    )); 
    }
}