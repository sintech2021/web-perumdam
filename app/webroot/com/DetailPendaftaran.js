class DetailPendaftaran extends React.Component{
  state = {
    data : null
  }
   componentWillMount(){
     this.setState({
      data:app_data.data.Pendaftaran
     })
  }
  downloadFile(data){
    document.location.href = site_url()+`/pendaftaran/print/${data.id}/${data.kode_pendaftaran}`;
  } 
  render(){
    // const enableGcaptcha = true;
    // const gCaptchaSiteKey = '6LecSeYUAAAAADazed6MSoJXAW53CyaHJXgZ8i79';
    const {data} = this.state;
    return(
      <section className="form_registrasi container detail_reg">
        <article className="row">
          <section className="col-md-12 wrp">
          <header>
          <div className="img-container">
            <img src={site_url()+`/pub/img/perumdam-top.png`} style={{width:'auto'}}/>
          </div>
          <h2 className="form-title">PERUMDAM Tirta Kerta Raharja</h2>
          </header>
          {data && (
            <div className="table_cnt">
            <table className="table">
              <tbody>
                <tr><th><label>Nomor Registrasi</label></th><td><label>{data.kode_pendaftaran}</label></td></tr>
                <tr><th><label>Nama Pemohon</label></th><td><label>{data.nama_pemohon}</label></td></tr>
                <tr><th><label>Nama Pemilik</label></th><td><label>{data.nama_pemilik}</label></td></tr>
                <tr><th><label>Nomor KTP</label></th><td><label>{data.nomor_ktp}</label></td></tr>
                <tr><th><label>Nomor HP</label></th><td><label>{data.nomor_hp}</label></td></tr>
                <tr><th><label>Email</label></th><td><label>{data.email}</label></td></tr>
                <tr><th><label>Kabupaten</label></th><td><label>{data.kabupaten}</label></td></tr>
                <tr><th><label>Kecamatan</label></th><td><label>{data.kecamatan}</label></td></tr>
                <tr><th><label>Kelurahan</label></th><td><label>{data.kelurahan}</label></td></tr>
                <tr><th><label>Alamat</label></th><td><label>{data.alamat}</label></td></tr>
                <tr><th><label>Kode Pos</label></th><td><label>{data.kode_pos}</label></td></tr>
                <tr><th><label>Foto KTP</label></th><td><label><img style={{width:370}} src={site_url()+'/uploads/pendaftaran/'+data.foto_ktp}/></label></td></tr>
              </tbody>
            </table>
            <div className="info-reg">
              <div className="warning-message">
              <span>*) Mohon Catat Nomor Registrasi Anda. Untuk Status Pendaftran senjutnya </span> 
              <a className="external-link" target="_blank" href={site_url()+`/register_check/${data.kode_pendaftaran}`}>disini</a>
              </div>
              <div className="btn-cnt">
                <button onClick={(e)=>{this.downloadFile(data)}} className="btn btn-primary">Download</button>
              </div>
            </div>
          </div>
          )}
          </section>
        </article>
      </section>
    )
  }
}