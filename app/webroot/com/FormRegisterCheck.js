class FormRegisterCheck extends React.Component{
  state = {
    gCaptchaSiteKey:'6LecSeYUAAAAADazed6MSoJXAW53CyaHJXgZ8i79',
    gRecaptcha:'',
    gRecaptchaContainer:'recaptcha_container',
    gRecaptchaPanthom:0,
    kode_pendaftaran : '',
    submitLoadingCls:'',
    isLoading: false,
    error_message:'',
    result:null,
    customer: null,
    registerStatus:''
  }
  componentWillMount(){
    setTimeout(()=>{
      if(typeof app_data.kode_pendaftaran != 'undefined'){
        this.setState({kode_pendaftaran:app_data.kode_pendaftaran});
      }
      this.updateRecaptchaContainer();
      this.loadRecaptcha();
    },1000);
  }
  loadRecaptcha(){
    var el_str  = `<div className="g-recaptcha" id="${this.state.gRecaptchaContainer}"></div>`;
    var parent = $('#recaptcha_parent_container');
    parent.html(el_str);

    grecaptcha.render(this.state.gRecaptchaContainer, {
        sitekey : this.state.gCaptchaSiteKey,
        callback : (response) =>{
          this.setState({gRecaptcha:response})
        }
    }); 
  }
  updateRecaptchaContainer(){
    let gRecaptchaPanthom = this.state.gRecaptchaPanthom + 1;
    let gRecaptchaContainer = `recaptcha_container${gRecaptchaPanthom}`
    this.setState({
      gRecaptchaContainer:gRecaptchaContainer,
      gRecaptchaPanthom: gRecaptchaPanthom
    });
  }
  onSubmitForm = async ()=>{
    const {gRecaptcha,kode_pendaftaran,isLoading} = this.state;
    if(isLoading){
      return;
    }
    this.setState({
        result : null,
        submitLoadingCls:'fa fa-spinner fa-spin',
        isLoading:true,
        error_message:''
    });
    const res = await Store.Service.do_register_check(kode_pendaftaran,gRecaptcha);
    this.updateRecaptchaContainer();
    this.loadRecaptcha();
    this.setState({
        // result : null,
        submitLoadingCls:'',
        isLoading:false
    });
    if( typeof res.success!== 'undefined'){
          if(res.success == false){
            this.setState({error_message:res.msg});
            return;
          }
          
      }
      if(res == ''){
       
        this.setState({error_message:'ID Pelanggan tidak valid.'});

        return;
      }
     
      if(res.status != 500){
        if(res.data == null){
          res.data = [];
        }
        if(res.desc == "General Error Service"){
          this.setState({error_message:res.desc,result:null})
        }
         else if(res.desc == "Kode Daftar Tidak ditemukan."){
          this.setState({error_message:res.desc,result:null})
        }
        else if(res.desc == "Customer is Not found "){
          this.setState({error_message:res.desc,result:null})
        }
        else if(res.desc == "Kode Daftar Tidak ditemukan."){
          this.setState({error_message:res.desc,result:null})
        }else{
          this.setState({result:res,resultPelangganData:res.data});
          if(typeof res.customer == 'object'){
            this.setState({customer:res.customer, registerStatus: res.desc});
          }
          setTimeout(()=>{
            // this.updatePelangganInfo();
            // this.updateDataInfo();
          },100);
        }
      }
    
    event.preventDefault();
    return false;
  }
  onFormSubmit = ()=>{
    event.preventDefault();
    return false;
  }
  onInputChange = (evt)=>{
    const el    = evt.target;
    const name  = el.name;
    let value = el.value;
    let state   = {};
    state[name] = value;
    this.setState(state);
  }
  render(){
 
    const {gCaptchaSiteKey,submitLoadingCls,error_message,customer,registerStatus} = this.state;
    const hasError = error_message != '';
    return(
      <section className="form_tagihan container">
        <article className="row">
          <section className="col-md-12 wrp">
          <header>
              <h2 className="form-title">Cek Registrasi Pelanggan</h2>
 
          </header>

          <form className="form-horizontal main-form" method="post" onSubmit={this.onSubmitForm}>
            <div className="form-group">
              <label htmlFor="kode_pendaftaran" className="control-label">Kode Registrasi</label>
              <div className="input-container">
              <input className="form-control" value={this.state.kode_pendaftaran} onChange={()=>{this.onInputChange(event)}}  name="kode_pendaftaran" id="kode_pendaftaran" placeholder="Masukkan Kode Registrasi" type="text" required="required"/>
              </div>
            </div> 

            <div className="form-group">
              <div className="input-container">
                <div className="form-error-message" style={{display:hasError?'block':'none'}}>
                <div dangerouslySetInnerHTML={{__html: error_message}} />
                </div>
              </div>
            </div>
            <div className="form-group">
              
              <div className="row" style={{padding:'1em 0'}}>
                <div className="col-md-6" id="recaptcha_parent_container">
                </div>
                <div className="col-md-6">
                  <div className="link-container" style={{textAlign:'right',marginTop:'1em'}}>
                    <a href="javascript:;" className="link_goto_page" onClick={this.onSubmitForm}><i className={submitLoadingCls}></i> Cek Status</a>
                  </div>
                  </div>
              </div>
            </div>
          
            </form>
          </section>
          {customer && (
            <section className="form_tagihan container">
           
                <article className="row">
                  <section className="col-md-12 wrp">
                    <header>
                      <h2 className="form-title">Data Pendaftaran</h2>
                    </header>
                    <div>
                        <table className="table table-hover table-web-api-result">
                          <tbody>
                            <tr><th><label>Nomor Registrasi</label></th><td><label>{customer.kdDaftar}</label></td></tr>
                            <tr><th><label>Nama Pemohon</label></th><td><label>{customer.pemohon}</label></td></tr>
                            <tr><th><label>Nama Pemilik</label></th><td><label>{customer.pemilik}</label></td></tr>
                            <tr><th><label>Nomor KTP</label></th><td><label>{customer.noKtp}</label></td></tr>
                            <tr><th><label>Nomor HP</label></th><td><label>{customer.noHp}</label></td></tr>
                            <tr><th><label>Email</label></th><td><label>{customer.email}</label></td></tr>
                            <tr><th><label>Kabupaten</label></th><td><label>{customer.kabupaten}</label></td></tr>
                            <tr><th><label>Kecamatan</label></th><td><label>{customer.kecamatan}</label></td></tr>
                            <tr><th><label>Kelurahan</label></th><td><label>{customer.kelurahan}</label></td></tr>
                            <tr><th><label>Alamat</label></th><td><label>{customer.almtPasang} RT {customer.rt}/{customer.rw} No. Rumah {customer.noRumah}</label></td></tr>
                            <tr><th><label>Kode Pos</label></th><td><label>{customer.kdPos}</label></td></tr>
                            <tr><th><label>Status Pendaftaran</label></th><td><label>{registerStatus}</label></td></tr>
                          </tbody>
                        </table>
                    </div>
                  </section>
                </article>
                
              </section>
          )}  
        </article>
      </section>
    )
  }
}
