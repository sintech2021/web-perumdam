
class NewsItem extends React.Component{
	state = {
		active_id : app_data.active_id
	}
	render(){
		const {title,mapUrl,id,fullContent} = this.props;
		const {active_id} = this.state;
		const styleDsp = {

		};

		return (
			<div id={id} className="my_news_item col-lg-12" style={{display: active_id==id?'block':'none',marginBottom:'2em'}}>
				
				<div className="news_content" style={{padding:'1em 2em'}}>
					<h4 className="my_news_title">{title}</h4>
					 <div className="map_container">
						<iframe style={{border: 0}} src={mapUrl} width="100%" height="500" frameBorder="0" allowFullScreen="yes"></iframe>
					</div>
					<div className="fullContent" dangerouslySetInnerHTML={{__html:fullContent}}></div> 
				</div>
			</div>
		)
	}
}

class News extends React.Component{
	state = {
		top_mainlist: []
	}
	componentWillMount(){
		const top_mainlist = app_data.mainlist;
		if(typeof top_mainlist != 'undefined'){
			try{
				if(top_mainlist.list.length > 0){
					this.setState({top_mainlist:top_mainlist.list})
				}
			}catch(e){
			}
			
		}
	}
	
	render(){
		const {top_mainlist} = this.state;
		return (
			 <div className="news_container container" style={{marginTop:'1.5em'}}>
				<div className="row">
				{
					top_mainlist.map((item,index)=>{
						return (
							<NewsItem title={item.title} key={index}
							mapUrl={item.annotation} id={item.content_id} fullContent={item.full_content}
						/>)
					})
				} 
				</div>
			</div> 
		)
	}
}

class LatestNews extends React.Component{
	state = {
		latest_news:[]
	}
	componentWillMount(){
		this.setState({
			latest_news:app_data.mainlist.latest
		})
	}
	onNavigateMenu = (e)=>{

		const el = $(e.target).parent()[0];
	
			bindHistoryPushState(el);
		e.preventDefault();
		return false;
	}
	render(){
		const {latest_news} = this.state;
		return (
			<div className="latest_news">
			<h4 className="item-title">Kantor Layanan</h4> 	
			<div style={{padding:'0 1.2em'}}> <hr className="featurette-divider" style={{margin:'0'}}/></div>
			

				{
					latest_news.map((row,idx)=>{
						return (
							<div className={`latets-item`} key={`latest_${idx}`}>
							 	
							 <div className="container-latest" style={{display:'flex',flexDirection:'row'}}>
								 <div className="bg-image-shadow" 
								 style={{ backgroundImage:`url('${row.bgurl}')`}}>
								</div>
								<div className="latest-info">
									<a onClick={this.onNavigateMenu} href={site_url()+`/web/layanan_informasi/kantor_layanan?active=${row.content_id}`}><h4 className="latest_title">{row.title}</h4></a>
									<p className="latest_dt">{row.content}</p>
								</div>	 
							 </div>
						
							</div>
						)
					})
				}
			</div>
		)
	}
}
 

class KantorLayanan extends React.Component{
	render(){
		// const {title,snipet,url,bgurl,by,dt} = this.props;
		// const pagination = atob(app_data.pagination);
		return (
			<div className="container">
				<div className="main_news row">
					<div className="col-md-4">
						<LatestNews/>
					</div>
					<div className="col-md-8">
						<div style={{paddingTop:'1em'}}>
						<News/>
						 </div>
					</div>
				</div>
			</div>
		)
	}
}