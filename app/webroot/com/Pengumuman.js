
class NewsItem extends React.Component{
	render(){
		const {title,snipet,url,bgurl,by,dt} = this.props;
		return (
			<div className="my_news_item col-lg-6">
				<div className="bd-placeholder-img" style={{height:223,backgroundSize:'cover',borderRadius:5,backgroundImage:`url('${bgurl}')`,backgroundRepeat:'no-repeat',backgroundColor:'#C4C4C4'}}>
					
				</div>
				<div className="news_content">
					<a href={url}><h4 className="my_news_title">{title}</h4></a>
					<ul className="my_news_info">
						<li className="by">Oleh {by} &#8226; {dt}</li>
					</ul>
					<div className="my_news_snipet">
						<div className="snipet">{snipet}</div>
						<div className="link_container">
							<a className="news_link" href={url}>Lebih lengkap <i className="fa fa-chevron-right"></i></a>
						</div>
					</div>
				</div>
			</div>
		)
	}
}

class News extends React.Component{
	static slugify(str) {
		 
	
		return slugify(str,6);
	}
	static getUrl(row){
		const slug = News.slugify(row.title);
		return site_url() + `/read/${row.id}/pengumuman/${slug}`;
	}
	state = {
		top_mainlist: []
	}
	componentWillMount(){
		const top_mainlist = app_data.mainlist;
		if(typeof top_mainlist != 'undefined'){
			try{
				if(top_mainlist.list.length > 0){
					this.setState({top_mainlist:top_mainlist.list})
				}
			}catch(e){
			}
			
		}
	}
	
	render(){
		const {top_mainlist} = this.state;
		return (
			 <div className="news_container container">
				<div className="row">
				{
					top_mainlist.map((item,index)=>{
						return (
							<NewsItem title={item.title} key={index}
							by={item.author} dt={item.dt}
							url={News.getUrl(item)} bgurl={item.bgurl}
							snipet={item.content}
						/>)
					})
				} 
				</div>
			</div> 
		)
	}
}

class LatestNews extends React.Component{
	state = {
		latest_news:[]
	}
	componentWillMount(){
		this.setState({
			latest_news:app_data.mainlist.latest
		})
	}
	render(){
		const {latest_news} = this.state;
		return (
			<div className="latest_news">
			<h4 className="item-title">Pengumuman Terbaru</h4> 	
			<div style={{padding:'0 1.2em'}}> <hr className="featurette-divider" style={{margin:'0'}}/></div>
			

				{
					latest_news.map((row,idx)=>{
						return (
							<div className="latets-item" key={`latest_${idx}`}>
							 	
							 <div className="container-latest" style={{display:'flex',flexDirection:'row'}}>
								 <div className="bg-image-shadow" 
								 style={{ backgroundImage:`url('${row.bgurl}')`}}>
								</div>
								<div className="latest-info">
									<a href={News.getUrl(row)}><h4 className="latest_title">{row.title}</h4></a>
									<p className="latest_dt">{row.dt}</p>
								</div>	 
							 </div>
						
							</div>
						)
					})
				}
			</div>
		)
	}
}
class ArchivedNews extends React.Component{
	render(){
		// const {title,snipet,url,bgurl,by,dt} = this.props;
		const news_statistics = [
			{month:'Januari', stat:'15'},
			{month:'Februari', stat:'15'},
			{month:'Maret', stat:'15'},
			{month:'April', stat:'15'},
			{month:'Mei', stat:'15'},
			{month:'Juni', stat:'15'},

		];
		return (
			<div className="archived_news">
				<h4 className="item-title">Arsip Berita</h4> 
				<div style={{padding:'0 1.5em'}}> <hr className="featurette-divider" style={{margin:'0'}}/></div>
				<ul className="archived_stat_item">
				{
					news_statistics.map((row,k)=>{
						return(
							<li>
								<div className="item-cnt row">
									<h4 className="month col-md-8">{row.month}</h4>
									<span class="badge badge-light col-md-2"><span className="badge-text">{row.stat}</span></span>
								</div>
							</li>
						)
					})
				}
				</ul>
			</div>
		)
	}
}

class Pengumuman extends React.Component{
	render(){
		// const {title,snipet,url,bgurl,by,dt} = this.props;
		const pagination = atob(app_data.pagination);
		return (
			<div className="container">
				<div className="main_news row">
					<div className="col-md-4">
						<LatestNews/>
					</div>
					<div className="col-md-8">
						<div style={{paddingTop:'5em'}}></div>
						<News/>
						<ul className="news_pagination" dangerouslySetInnerHTML={{__html:pagination}}>
						</ul>
					</div>
				</div>
			</div>
		)
	}
}