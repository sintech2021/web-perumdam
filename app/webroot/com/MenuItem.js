class MenuItem extends React.Component{
	
	state ={
		data:[]
	}
	getData = async ()=>{
		
		const data = await Store.Data.get_menu();
		this.setState({
			data : data 
		})
		setTimeout(()=>{
			$(".dropdown, .btn-group").hover(function(){
		        var dropdownMenu = $(this).children(".dropdown-menu");
		        if(dropdownMenu.is(":visible")){
		            dropdownMenu.parent().toggleClass("open");
		        }
		    });
		},250);
		
	}
	componentWillMount(){
		this.getData();
		
	}
	onNavigateMenu = (e)=>{
		const el = e.target;
		if($(el).hasClass('induk')){
		}else{
			bindHistoryPushState(el);
		}
		e.preventDefault();
		return false;
	}
	render(){
		const {data} = this.state;
		let allMenu = (<div></div>);
		try{
			if(typeof data.forEach == 'function'){
				allMenu =  data.map((row,index)=>{
					const uri = site_url()+row.ScmMenuSlug.value;
					let subMenu = null;
					let hasChild = false;
					if(row.ChildMenu.length > 0){
							hasChild=true;
						subMenu = row.ChildMenu.map((child,idx)=>{
							return ( <a key={`child-menu-${idx}`} onClick={this.onNavigateMenu} className="anak dropdown-item" href={site_url()+child.ScmMenuSlug.value}>{child.text}</a> )	
						})

					}
					//lass="nav-link dropdown-toggle"
					return (<li className={hasChild?"induk nav-item dropdown":"nav-item"} key={`menu-${row.FrontModel.id}`}><a data-toggle={hasChild?"dropdown":''} role={hasChild?"button":''} aria-haspopup="true" aria-expanded="false"  className={hasChild?"nav-link dropdown-toggle":"nav-link"}  onClick={this.onNavigateMenu}  href={uri}>{row.FrontModel.text}</a>
						{subMenu && (
								<div className="dropdown-menu">
								{subMenu}	
								</div>
						)}
						</li>
					)
				})
	 
			}
		}
		catch(e){
			
		}
		
		
		return (
			<div>
			<ul className="navbar-nav mr-auto">
			{allMenu}
			</ul>
			</div>
		);
	}
}

/*

*/