
class Galeri extends React.Component{
	state={
		list:[]
	}
	getData = async ()=>{
		const data = await Store.Data.get_galery();
		return data;
	}
	
	async componentWillMount(){
		const res = await this.getData();
		this.setState({
			list : res 
		});
		 
 
	}
	render(){
		const {list} = this.state;
 
		return (
			<div className="container">
				<div className="main_galery row">
					<div className="col-md-12">
						<div className="row">
						{
							list.map((j,i)=>{
								return(
									<div className=" col-md-4 items">
										<div className="info">
						                   	<h4>{j.title} ({j.cx})</h4>
						                </div>
						                <a className="thumbnail" href={j.url} style={{height:400,width:'100%',display:'block',backgroundSize:'cover',backgroundImage:'url('+ `${j.thumb})`}}>

						                </a>
						            </div>
								)
							})
						}
				            
						</div>
					</div>
				</div>
			</div>
		)
	}
}