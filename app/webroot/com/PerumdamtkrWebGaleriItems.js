class PerumdamtkrWebGaleriItems extends React.Component{
	
	render(){
		const {is_ajax} = this.props;
		let content = (<div></div>);
		let galery_title = 'Galeri Foto & Video';
		try{
			galery_title = app_data.galery.Galery.title;
		}catch(e){}	
		if(is_ajax){
			content = (
				<div id="galeri">

					<PageBanner/>
			<h1 className="big-title text-center" style={{padding:'1em'}}>{galery_title}</h1>

					<GaleriItems/>
				</div>
			)
		}else{
			content = (
				<div id="galeri">

					<Header/>
					<main role="main">
					<PageBanner/>
			<h1 className="big-title text-center" style={{padding:'1em'}}>{galery_title}</h1>
					
					<GaleriItems/>
					</main>
					<MyCompany/>
					<Footer/>
					
				</div>
			);
		}
		return content;
		 
	}
}