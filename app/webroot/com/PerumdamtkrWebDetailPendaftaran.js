class PerumdamtkrWebDetailPendaftaran extends React.Component{
	
	render(){
		const {is_ajax} = this.props;
		let content = (<div></div>);

		if(is_ajax){
			content = (
				<div id="app">
					<DetailPendaftaran/>
				</div>
			)
		}else{
			content = (
				<div id="app">
					<main role="main">
					<DetailPendaftaran/>
					</main>
				</div>
			);
		}
		return content; 
	}
}