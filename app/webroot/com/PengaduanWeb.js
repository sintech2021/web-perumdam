class PengaduanWeb extends React.Component{
	
	render(){
		const {is_ajax} = this.props;
		let content = (<div></div>);

		if(is_ajax){
			content = (
				<div id="app_pengaduan">
					<PageBanner/>
					<FormPengaduan/>
					<ServiceButtonSm/>
				</div>
			)
		}else{
			content = (
				<div id="app_pengaduan">
					<Header/>
					<main role="main">
					<PageBanner/>
					<FormPengaduan/>
					<ServiceButtonSm/>
					</main>
					<MyCompany/>
					<Footer/>
				</div>
			);
		}
		return content;
		 
	}
}