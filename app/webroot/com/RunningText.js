
class RunningText extends React.Component{
	state ={
		list:[]
	}
	getData = async ()=>{
		const data = await Store.Data.get_running_text();
		return data;
	}
	
	async componentWillMount(){
		const res = await this.getData();
		this.setState({
			list : res 
		});
		 
 
	}
	render(){
		const {list} = this.state;
		return (
			<div className="my_running_text">
				<div class="scrolling-limit">
				<marquee  class="scrolling" >
					{
						list.map((j,i)=>{
							return(
								
								 <p dangerouslySetInnerHTML={{__html:j.content}} key={`RunningText-${i}`}></p>
				  				
								)
						})
					}
				  </marquee>
				</div>
			</div>
		)
	}
}