class PageBanner extends React.Component{
	getTitle(){
		try{
			if(app_data.formatted_slug=='galeri_detail'){
				return app_data.detail.GaleryItem.title;
			}
			if(app_data.formatted_slug=='galeri_items'){
				return app_data.galery.Galery.title;
			}
			return app_data.article.title;
		}catch(e){
			return ''
		}
	}
	getBreadcrumbs(){
		let breadcrumb = 'Beranda ';
		try{
			app_data.breadcrumbs.forEach((row,index)=>{
				breadcrumb += `/ ${row.title}`;
			});

			return breadcrumb;
		}catch(e){
			return ''
		}
	}
	getLastBreadcrumbs(){
		try{
			return app_data.article.title;
		}catch(e){return ''}
	}
	state = {
		description : '',
		breadcrumb : '',
		bgurl:'',
		formatted_slug: '',

		config:{
			'faq':{
				bgurl : site_url() + `/pub/img/banner/perumdamtkr-depan.jpg`,
				breadcrumb : 'Beranda / Frequently Asked Questions (FAQ) '  ,
				description : this.getTitle()
			},
			'arsip' :{
				bgurl : site_url() + `/pub/img/banner/mountains.jpg`,
				breadcrumb : 'Beranda / Arsip Berita',
				description : (<p>Arsip Berita Perumdam<br/>Tirta Kerta Raharja</p>)
			},
			'/topik/berita' :{
				bgurl : site_url() + `/pub/img/banner/mountains.jpg`,
				breadcrumb : 'Beranda / Berita',
				description : (<p>Berita Terkini Perumdam<br/>Tirta Kerta Raharja</p>)
			},
			'berita' :{
				bgurl : site_url() + `/pub/img/banner/mountains.jpg`,
				breadcrumb : 'Beranda / Berita',
				description : this.getTitle()
			},
			"default" :{
				bgurl : site_url() + `/pub/img/banner/perumdamtkr-depan.jpg`,
				breadcrumb : this.getBreadcrumbs(),
				description : this.getTitle()
			},
			"galeri" :{
				bgurl : site_url() + `/pub/img/banner/perumdamtkr-depan.jpg`,
				breadcrumb : 'Beranda / Galeri Foto & Video',
				description : (<p>Galeri Foto & Video Perumdam<br/>Tirta Kerta Raharja</p>)
			},
			"pengumuman" :{
				bgurl : site_url() + `/pub/img/banner/perumdamtkr-depan.jpg`,
				breadcrumb : 'Beranda / Pengumuman',
				description : 'Pengumuman'
			},
			'galeri_detail':{
				bgurl : site_url() + `/pub/img/banner/perumdamtkr-depan.jpg`,
				breadcrumb : 'Beranda / Galeri Foto & Video',
				description : this.getTitle()
			},
			'galeri_items':{
				bgurl : site_url() + `/pub/img/banner/perumdamtkr-depan.jpg`,
				breadcrumb : 'Beranda / Galeri Foto & Video',
				description : this.getTitle()
			},
			"organisasi" :{
				bgurl : site_url() + `/pub/img/banner/perumdamtkr-depan.jpg`,
				breadcrumb : 'Beranda / Organisasi',
				description : this.getTitle()
			},
			"web.tentang_kami.jajaran_direksi":{
				bgurl : site_url() + `/pub/img/banner/all-staff.jpg`,
				breadcrumb : 'Beranda / Tentang Kami / Jajaran Direksi',
				description : this.getTitle()
			},
			"tentang_kami":{
				bgurl : site_url() + `/pub/img/banner/all-staff.jpg`,
				breadcrumb : 'Beranda / Tentang Kami / ' + this.getLastBreadcrumbs(),
				description : this.getTitle()
			},
			"layanan_informasi":{
				bgurl : site_url() + `/pub/img/banner/perumdamtkr-depan.jpg`,
				breadcrumb : 'Beranda / Layanan Informasi/ ' + this.getLastBreadcrumbs(),
				description : this.getTitle()
			},
			"laboratorium":{
				bgurl : site_url() + `/pub/img/banner/perumdamtkr-depan.jpg`,
				breadcrumb : 'Beranda / Laboratorium / ' + this.getLastBreadcrumbs(),
				description : this.getTitle()
			},
			"ppid":{
				bgurl : site_url() + `/pub/img/banner/perumdamtkr-depan.jpg`,
				breadcrumb : 'Beranda / PPID / ' + this.getLastBreadcrumbs(),
				description : this.getTitle()
			},
			"web.layanan_informasi.kantor_layanan":{
				bgurl : site_url() + `/pub/img/banner/perumdamtkr-depan.jpg`,
				breadcrumb : 'Beranda / Layanan Informasi / Kantor Layanan',
				description : this.getTitle()
			},
			'web.kontak_kami' :{
				bgurl : site_url() + `/pub/img/banner/forum-komunikasi-all.jpg`,
				breadcrumb : 'Beranda / Kontak Kami',
				description : this.getTitle()
			},
			'web.tentang_kami.struktur_organisasi':{
				bgurl : site_url() + `/pub/img/banner/meeting-in-office.jpg`,
				breadcrumb : 'Beranda / Tentang Kami / Struktur Organisasi',
				description : this.getTitle()
			},
			'pendaftaran' : {
				bgurl : site_url() + `/pub/img/banner/pen-writing.jpg`,
				breadcrumb : 'Beranda / Pendaftaran',
				description : 'Registrasi Pelanggan'

			},
			'web.pengaduan' : {
				bgurl : site_url() + `/pub/img/banner/customer-service.jpg`,
				breadcrumb : 'Beranda / Pengaduan',
				description : 'Pengaduan Pelanggan' 
			},
			'tagihan' : {
				bgurl : site_url() + `/pub/img/banner/customer-bill.jpg`,
				breadcrumb : 'Beranda / Tagihan',
				description : 'Informasi Tagihan Pelanggan'
			},
			'register_check' : {
				bgurl : site_url() + `/pub/img/banner/registrasi-check.jpg`,
				breadcrumb : 'Beranda / Check Registrasi',
				description : 'Cek Registrasi Pelanggan'
			},
			'tunggakan' : {
				bgurl : site_url() + `/pub/img/banner/tunggakan.jpg`,
				breadcrumb : 'Beranda / Tunggakan',
				description : 'Cek Tunggakan Pelanggan'
			},
			'kantor_layanan' : {
				bgurl : site_url() + `/pub/img/banner/tunggakan.jpg`,
				breadcrumb : 'Beranda / Tunggakan',
				description : 'Tunggakan Pelanggan'
			},
		}
	}
	getData(){
		const {config} = this.state;
		let formatted_slug = app_data.formatted_slug;
		if(formatted_slug.match(/register_check/)){
			formatted_slug = 'register_check';
		}
		if(typeof config[formatted_slug] !== 'undefined'){
			this.setState({formatted_slug:formatted_slug});
			this.setState(config[formatted_slug]);
		}else{
			this.setState(config.default);
		}
	}
	componentWillMount(){
	 	this.getData();
		setTimeout(()=>{
			 
		},250);
		
	}
	render(){
		const {bgurl,description,breadcrumb} = this.state;
		return (
			 <div className="container" style={{maxWidth:'100%',margin:0,padding:0,background:`transparent url(${bgurl}) no-repeat top left`}}>
				<div className="row page_banner">
						<div className="col-md-12">
						<div className="data">	
							<div className="hypen"></div>
							<div className="content_description">{description}</div>
							<div className="content_breadcrumb">{breadcrumb}</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}