class PerumdamtkrWebSearch extends React.Component{
	
	render(){
		const {is_ajax,include_banner} = this.props;
		let category = 'berita';
		let exclude_id='0';
		try{category=app_data.formatted_slug;}catch(e){}
		try{exclude_id=app_data.article.id;}catch(e){}
		let content = (<div></div>);
		let banner = (<Breadcrumbs/>);
		if(include_banner){
			banner = (<PageBanner/>);
		}
		if(is_ajax){
			content = (
				<div id="single">
					{banner}
					<SearchResult/>
					<div className="container" style={{backgroundColor:"#F9F9F9",paddingTop:'3em',maxWidth:'100%',margin:'0'}}>
					<h1 className="big-title text-center">Berita</h1>
					<div className="underline" style={{marginBottom:'1.5em'}}></div>
					<MyNews containerBg="#F9F9F9" category={category} exclude_id={exclude_id}/>
					</div>
				</div>
			)
		}else{
			content = (
				<div id="single">
					<Header/>
					<main role="main">
					{banner}
					<SearchResult/>
					<div className="container" style={{backgroundColor:"#F9F9F9",paddingTop:'3em',maxWidth:'100%',margin:'0'}}>
					<h1 className="big-title text-center">Berita</h1>
					<div className="underline" style={{marginBottom:'1.5em'}}></div>
					<MyNews containerBg="#F9F9F9" category={category} exclude_id={exclude_id}/>
					</div>
					</main>
					<MyCompany/>
					<Footer/>
					
				</div>
			)
		}
		return content;
	}
}