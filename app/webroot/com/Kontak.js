class Kontak extends React.Component{
	state={
		latlng : {
			latitude : '',
			longitude : ''
		}
	}
	getData(){

	}
	componentWillMount(){
		// this.getData()
	}
	render(){
		return(
			<div className="kontak container">

				<div className="main_container">
					<div className="kontak-box">
						<div className="row">
							<div className="col-md-4">
								<div className="cups">
									<a className="link" href="#">
										<img src={site_url()+'/pub/img/map-marker-mid.png'}/>
									</a>
									<p>Jl. Kisamaun No.204, RT.002/RW.007, Sukasari, Kec. Tangerang, Kota Tangerang, Banten 15118</p>
								</div>
							</div>
							<div className="col-md-4">
								<div className="cups">
									<a className="link" href="#">
										<img src={site_url()+'/pub/img/tel-mid.png'}/>
									</a>
									<p style={{textAlign:'center'}}>Kantor – (021) 557777 666<br/>HP – 0821 222777 666</p>
								
								</div>
							</div>
							<div className="col-md-4">
								<div className="cups">
									<a className="link" href="#">
										<img src={site_url()+'/pub/img/envelope-mid.png'}/>
									</a>
									<p style={{textAlign:'center'}}>contact@perumdamtkr.co.id</p>
								
								</div>
							</div>
						</div>
					</div> 
					<div className="map_container">
						<iframe style={{border: 0}} src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.5939444766004!2d106.6298483140156!3d-6.1850609955222975!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69fb0b6f6811b7%3A0xff18191ead72f8a9!2sPdam+%E2%80%93+Tirta+Kerta+Raharja+Kabupaten+Tangerang!5e0!3m2!1sid!2sid!4v1454256024222" width="100%" height="500" frameBorder="0" allowFullScreen="yes"></iframe>
					</div>
				</div>
			</div>
		)
	}
}