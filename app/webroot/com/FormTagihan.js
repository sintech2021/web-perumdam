class FormTagihan extends React.Component{
  state = {
    gCaptchaSiteKey:'6LecSeYUAAAAADazed6MSoJXAW53CyaHJXgZ8i79',
    gRecaptcha:'',
    gRecaptchaContainer:'recaptcha_container',
    gRecaptchaPanthom:0,
    id_pelanggan : '',
    bulan : '',
    tahun:'',
    submitLoadingCls:'',
    isLoading: false,
    bulan_list : [
      {id:'00',name:'--ALL--'},
      {id:'01',name:'Januari'},
      {id:'02',name:'Februari'},
      {id:'03',name:'Maret'},
      {id:'04',name:'April'},
      {id:'05',name:'Mei'},
      {id:'06',name:'Juni'},
      {id:'07',name:'Juli'},
      {id:'08',name:'Agustus'},
      {id:'09',name:'September'},
      {id:'10',name:'Oktober'},
      {id:'11',name:'November'},
      {id:'12',name:'Desember'},
    ],
    bulan_list_x : {
      '01':'Januari',
      '02':'Februari',
      '03':'Maret',
      '04':'April',
      '05':'Mei',
      '06':'Juni',
      '07':'Juli',
      '08':'Agustus',
      '09':'September',
      '10':'Oktober',
      '11':'November',
      '12':'Desember',
    },
    tahun_list : [
    ],
    bill_period : app_data.bill_period,
    result:null,
    resultPelangganInfo:[],
    resultPelangganData:[],
    invalidCustomer:false,
    keyData:null,
    infoTagihan:null,detailTagihan:null,finalDataAll:[],
    error_message:''

  }
  updateTahunList = ()=>{
    const {bill_period} = this.state;
    try{
      const bill_periods = bill_period.split('-');
      if(bill_periods.length==3){
        const tahun = parseInt(bill_periods[0]);
        const bulan = bill_periods[1];
        
        let tmp_tahun = tahun;
        let tmp_cx = 5;
        const tahun_list = [];
        while(tmp_cx > 0){
          const tahun_item = {
             id : tmp_tahun,
             name : tmp_tahun 
          }
          tahun_list.push(tahun_item);
          tmp_tahun--;
          tmp_cx--;
        }

        this.setState({
          tahun : tahun,
          bulan : bulan,
          tahun_list : tahun_list
        });
      }
    }catch(e){
    }
  }
  componentWillMount(){
    setTimeout(()=>{
      this.updateTahunList();
      this.updateRecaptchaContainer();
      this.loadRecaptcha();
    },1000);
  }
  loadRecaptcha(){
    var el_str  = `<div className="g-recaptcha" id="${this.state.gRecaptchaContainer}"></div>`;
    var parent = $('#recaptcha_parent_container');
    parent.html(el_str);

    grecaptcha.render(this.state.gRecaptchaContainer, {
        sitekey : this.state.gCaptchaSiteKey,
        callback : (response) =>{
          this.setState({gRecaptcha:response})
        }
    }); 
  }
  updateRecaptchaContainer(){
    let gRecaptchaPanthom = this.state.gRecaptchaPanthom + 1;
    let gRecaptchaContainer = `recaptcha_container${gRecaptchaPanthom}`
    this.setState({
      gRecaptchaContainer:gRecaptchaContainer,
      gRecaptchaPanthom: gRecaptchaPanthom
    });
  }
  onSubmitForm = async ()=>{
    if(!this.state.isLoading){
      const {gRecaptcha,id_pelanggan,bulan,tahun} = this.state;
      const id_pelanggan_clean = id_pelanggan.replace(/\W+/,'');
    

      this.setState({
        result : null,
        error_message:'',
        submitLoadingCls:'fa fa-spinner fa-spin',isLoading:true,invalidCustomer:false,keyData:`${tahun}${bulan}`});
      const res = await Store.Service.do_get_tagihan(id_pelanggan_clean, bulan, tahun, gRecaptcha);
      this.setState({submitLoadingCls:'',isLoading:false});
      this.updateRecaptchaContainer();
      this.loadRecaptcha();
      if( typeof res.success!== 'undefined'){
          if(res.success == false){
            this.setState({error_message:res.msg});
            return;
          }
          
      }
      if(res == ''){
       
        this.setState({error_message:'ID Pelanggan tidak valid.'});

        return;
      }
      if(res.status != 500){
        if(res.data == null){
          res.data = [];
        }
        if(res.desc == "General Error Service"){
          this.setState({error_message:res.desc,result:null})
        }
        else if(res.desc == "Customer is Not found "){
          this.setState({error_message:res.desc,result:null})
        }else{
          this.setState({result:res,resultPelangganData:res.data});
          setTimeout(()=>{
            this.updatePelangganInfo();
            this.updateDataInfo();
          },100);
        }
      }
      
      
      event.preventDefault();
      return false;
    }
    
  }
  onFormSubmit = ()=>{
    event.preventDefault();
    return false;
  }
  onInputChange = (evt)=>{
    const el    = evt.target;
    const name  = el.name;
    let value = el.value;
    let state   = {};
    state[name] = value.replace(/\W+/,'');
    this.setState(state);
  }
  onTahunChanged = ()=>{
    const tahun = event.target.value;
    this.setState({tahun: tahun});
  }
  onBulanChanged = ()=>{
    const bulan = event.target.value;
    this.setState({bulan: bulan});
  } 
  getTagihanBulanan(row, total){
    const bulan = row.noThnbln.toString().substr(-2);
    const tahun = row.noThnbln.toString().substr(0,4);
    const nmBulan = this.state.bulan_list_x[bulan];
    const m3 = row.m3Akhir - row.m3Awal;

    total.jmlTagihan += row.jmlTagihan;
    total.jmlCicilan += row.jmlCicilan;
    total.jmlDenda   += row.jmlDenda;
    total.jmlAdmin   += row.jmlAdmin;
    total.m3 += m3;

    let _row =  {};

      _row[`${nmBulan} ${tahun}`] = this.convertToRupiah(row.jmlTagihan);
      _row['Pemakaian'] = `${format_number(m3)} m3`;
      _row['Stand Meter'] = `${format_number(row.m3Awal)} - ${format_number(row.m3Akhir)} m3`;
      _row['Tanggal Bayar'] = row.tglBayar;
      _row['Status'] = 'Belum dibayar';
      if(typeof row.tglBayar == 'string'){
        if(row.tglBayar != ''){
          _row['Status'] = 'Sudah dibayar';
           _row['Tanggal Bayar'] = tgl_indo(row.tglBayar);
        }

      }

    return _row;
  }
  updateDataInfo(){
    const {keyData,resultPelangganData,bulan,result} = this.state;
    let finalDataAll = [];
    let total = {jmlDenda:0, jmlCicilan:0, jmlTagihan:0, jmlAdmin:0, m3:0};
    
    for(let p = 0 ; p < resultPelangganData.length;p++){
      const row = resultPelangganData[p];
      if(bulan == '00'){
        finalDataAll.push(this.getTagihanBulanan(row, total));
      }
      else{
        if(row.noThnbln==keyData){
          finalDataAll.push(this.getTagihanBulanan(row, total));
          break;
        }
      } 
    }
    
    const infoTagihan = {
      'Pemakaian' : `${format_number(total.m3)} m3`,
      'Cicilan' : this.convertToRupiah(total.jmlCicilan),
      'Total Tagihan' : this.convertToRupiah(total.jmlTagihan),
    };

    const detailTagihan = {
      'Tag. Lalu/Denda' : this.convertToRupiah(total.jmlDenda),
      'Kode Tarif' : result.pelanggan.golTarif,
      'Cicilan' : this.convertToRupiah(total.jmlCicilan),
      'Biaya Admin' : this.convertToRupiah(total.jmlAdmin),
      'Jumlah Tagihan' : this.convertToRupiah(total.jmlTagihan),

    }
    
    this.setState({infoTagihan:infoTagihan,detailTagihan:detailTagihan,finalDataAll:finalDataAll});
  }
  updatePelangganInfo(){
    const {result} = this.state;
    let resultPelangganInfo = [
      {key:'Nama',val:result.pelanggan.pemilik},
      {key:'No. Pelanggan',val:result.pelanggan.kdPelanggan},
      {key:'Alamat',val:result.pelanggan.almtPasang},
    ];
 

    this.setState({resultPelangganInfo:resultPelangganInfo});
  }
  convertToRupiah(angka)
  {
    var rupiah = '';    
    var angkarev = angka.toString().split('').reverse().join('');
    for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
    return 'Rp. '+rupiah.split('',rupiah.length-1).reverse().join('');
  }
  /**
   * Usage example:
   * alert(convertToRupiah(10000000)); -> "Rp. 10.000.000"
   */
   
  convertToAngka(rupiah)
  {
    return parseInt(rupiah.replace(/,.*|[^0-9]/g, ''), 10);
  }
  detailTagihanTag(obj,append,index){
    let tags = [];
    if(typeof append !== 'undefined'){
      if(append){
        tags.push({k:`Tagihan ${index+1}`})
      }
    }

    for(let p in obj){
      if(typeof p !== 'undefined')
        tags.push({k:p,v:obj[p]});
    }
    return tags;
  }
  render(){
 
    const {error_message,infoTagihan,detailTagihan,finalDataAll,bulan_list, tahun_list,submitLoadingCls,result, resultPelangganInfo,resultPelangganData,invalidCustomer} = this.state;
    let hasError = false;
    try{
      hasError = error_message.length > 0;
    }catch(e){
      hasError = false;
    }
    return(
      <section className="form_tagihan container">
        <article className="row">
          <section className="col-md-12 wrp">
          <header>
            <h2 className="form-title">Informasi Tagihan Pelanggan</h2>
          </header>

          <form className="form-horizontal main-form" method="post" onSubmit={this.onSubmitForm}>
            <div className="form-group">
              <label htmlFor="id_pelanggan" className="control-label">ID Pelanggan</label>
              <div className="input-container">
              <input className="form-control" value={this.state.id_pelanggan} onChange={()=>{this.onInputChange(event)}} name="id_pelanggan" id="id_pelanggan" placeholder="Masukkan ID Pelanggan" type="text" required="required"/>
              </div>
            </div> 

            <div className="form-group">
              <div className="row">
                <div className="col-md-12">
                  <label htmlFor="periode" className="control-label">Periode Tagihan</label>
                </div>
              </div>
              <div className="row">
                <div className="col-md-6">
                  <div className="selectWrapper">
                    <select className="form-control" value={this.state.bulan} onChange={this.onBulanChanged} >
                      {
                        bulan_list.map((row,index)=>{
                          return (
                            <option key={`bulan-${index}`} value={row.id}>{row.name}</option>
                          )
                        })
                      }
                    </select>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="selectWrapper">
                    <select className="form-control" value={this.state.tahun} onChange={this.onTahunChanged} >
                      {
                        tahun_list.map((row,index)=>{
                          return (
                            <option key={`tahun-${index}`} value={row.id}>{row.name}</option>
                          )
                        })
                      }
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div className="form-group">
              <div className="input-container">
                <div className="form-error-message" style={{display:hasError?'block':'none'}}>
                <div dangerouslySetInnerHTML={{__html: error_message}} />
                </div>
              </div>
            </div>
            <div className="form-group">
              
              <div className="row" style={{padding:'1em 0'}}>
                <div className="col-md-6" id="recaptcha_parent_container">
                </div>
                <div className="col-md-6">
                  <div className="link-container" style={{textAlign:'right',marginTop:'1em'}}>
                    <a href="javascript:;" className="link_goto_page" onClick={this.onSubmitForm}><i className={submitLoadingCls}></i> Cek Tagihan Saya</a>
                  </div>
                  </div>
              </div>
            </div>
          
            </form>
          </section>
          {result && (
            <section className="form_tagihan container">
           
                <article className="row">
                  <section className="col-md-12 wrp">
                    <header>
                      <h2 className="form-title">Data Tagihan</h2>
                    </header>
                    <div>
                      <div>
                        <table className="table table-hover table-web-api-result">
                          <tbody>
                            <tr>
                              <th>Nama PAM</th><th>PERUMDAM Tirta Kerta Raharja</th>
                            </tr>
                            {resultPelangganInfo.map((row,index)=>{
                              return (
                                <tr key={`pl-${index}`}>
                                  <th>{row.key}</th><td className={row.key}>{row.val}</td>
                                </tr>
                              )
                            })}
                            {
                              this.detailTagihanTag(infoTagihan).map((row,index)=>{
                                return (
                                  <tr key={`pl1-${index}`} className={slugify(row.k)}>
                                    <th>{row.k}</th><td>{row.v}</td>
                                  </tr>
                                )
                              })
                            }
                            {
                              finalDataAll.reverse().map((rowx,index)=>{
                               
                                ////
                                return this.detailTagihanTag(rowx,true,index).map((rowy,index)=>{
                                  return (
                                    <tr key={`pl3-${index}`} className={slugify(rowy.k)}>
                                      <th>{rowy.k}</th><td>{rowy.v}</td>
                                    </tr>
                                  )
                                })
                                ////  
                              })
                              
                            }
                            <tr>
                              <th colSpan={2}>Detail Tagihan</th>
                            </tr>
                            {
                              this.detailTagihanTag(detailTagihan).map((row,index)=>{
                                return (
                                  <tr key={`pl3-${index}`} className={slugify(row.k)}>
                                    <th>{row.k}</th><td>{row.v}</td>
                                  </tr>
                                )
                              })
                            }
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </section>
                </article>
                
              </section>
          )}
          {invalidCustomer && (
            <div style={{padding:'1em',color:'red'}}>ID Pelanggan tidak terdaftar.</div>
          )} 
        </article>
      </section>
    )
  }
}