class PerumdamtkrWebGaleri extends React.Component{
	
	render(){
		const {is_ajax} = this.props;
		let content = (<div></div>);

		if(is_ajax){
			content = (
				<div id="galeri">

					<PageBanner/>
			<h1 className="big-title text-center" style={{padding:'1em'}}>Galeri Foto & Video</h1>

					<Galeri/>
				</div>
			)
		}else{
			content = (
				<div id="galeri">

					<Header/>
					<main role="main">
					<PageBanner/>
			<h1 className="big-title text-center" style={{padding:'1em'}}>Galeri Foto & Video</h1>
					
					<Galeri/>
					</main>
					<MyCompany/>
					<Footer/>
					
				</div>
			);
		}
		return content;
		 
	}
}