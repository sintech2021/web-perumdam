class TagihanWeb extends React.Component{
	
	render(){
		const {is_ajax} = this.props;
		let content = (<div></div>);

		if(is_ajax){
			content = (
				<div id="app">
					<PageBanner/>
					<FormTagihan/>
					<ServiceButtonSm/>

				</div>
			)
		}else{
			content = (
				<div id="app">
					<Header/>
					<main role="main">
					<PageBanner/>
					<FormTagihan/>
					<ServiceButtonSm/>
					
					</main>
					<MyCompany/>
					<Footer/>
				</div>
			);
		}
		return content; 
	}
}