
class MyNewsItem extends React.Component{

	onNavigateMenu = (e)=>{
		const el = e.target;
		if($(el).hasClass('induk')){
		}else{
			bindHistoryPushState(el);
		}
		e.preventDefault();
		return false;
	}
	render(){
		const {title,snipet,url,bgurl,by,dt} = this.props;
		return (
			<div className="my_news_item col-md-4">
				<div className="bd-placeholder-img" style={{height:223,backgroundSize:'cover',borderRadius:5,backgroundImage:`url('${bgurl}')`,backgroundRepeat:'no-repeat',backgroundColor:'#C4C4C4'}}>
					
				</div>
				<div className="news_content">
					<a onClick={this.onNavigateMenu} href={url}><h4 className="my_news_title">{title}</h4></a>
					<ul className="my_news_info">
						<li className="by">Oleh {by} &#8226; {dt}</li>
					</ul>
					<div className="my_news_snipet">
						<div className="snipet">{snipet}</div>
						<div className="link_container">
							<a onClick={this.onNavigateMenu} className="news_link" href={url}>Lebih lengkap <i className="fa fa-chevron-right"></i></a>
						</div>
					</div>
				</div>
			</div>
		)
	}
}

class MyNews extends React.Component{
	
	 
	state = {
		top_mainlist: [],
		containerBg : '#fff',
		category:'',
		exclude_id:''
	}
	getData = async (cat,exclude_id)=>{
		let category = typeof cat != 'undefined'?cat:'berita';
		const {use_ci} = this.props;
		this.setState({category});
		// const {category} = this.state;
		const mainlist = await Store.Data.get_related_content(category,exclude_id,use_ci);
		return mainlist;
	}
	slugify(str) {
		return slugify(str,6);
	}
	getUrl(row){
		const slug = this.slugify(row.title);
		const {category} = this.state;
		return site_url() + `/read/${row.id}/${category}/${slug}`;
	}
	componentWillMount = async ()=>{
		const {category,exclude_id } = this.props;
		
		

		let top_mainlist = await this.getData(category,exclude_id);
	 
		const {containerBg } = this.props;
		if(typeof containerBg != 'undefined'){
			this.setState({containerBg})
		}
		if(typeof top_mainlist != 'undefined'){
			try{
				if(top_mainlist != null)
				if(top_mainlist.list.length > 0){
					this.setState({top_mainlist:top_mainlist.list})
				}
			}
			catch(e){

			}
		}
	}
	onNavigateMenu = (e)=>{
		const el = e.target;
		if($(el).hasClass('induk')){
		}else{
			bindHistoryPushState(el);
		}
		e.preventDefault();
		return false;
	}
	render(){
		const {top_mainlist,containerBg} = this.state;
		const {no_link} = this.props;
		const no_browse = typeof no_link != 'undefined';
		return (
			<div>
			
			<div className="my_news_container container" style={{backgroundColor:containerBg}}>
				<div className="row">
				{
					top_mainlist.map((item,index)=>{
						return (
							<MyNewsItem title={item.title} key={index}
							by={item.author} dt={item.dt}
							url={this.getUrl(item)} bgurl={item.bgurl}
							snipet={item.content}
						/>
						)
					})
				} 
				</div>
			</div>
			{
				!no_browse && (
					<div className="link_separator">
						<a onClick={this.onNavigateMenu} href={site_url()+'/topik/berita'} className="link_goto_page">Lihat Semua</a>
					</div>
				)
			}
			
			</div>
		)
	}
}
