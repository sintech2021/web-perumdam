class PerumdamtkrWebPengumuman extends React.Component{
	
	render(){
		const {is_ajax} = this.props;
		let content = (<div></div>);
		if(is_ajax){
			content = (
				<div id="pengumuman">
					<PageBanner/>
					<Pengumuman/>
				</div>
			)
		}else{
			content = (
				<div id="pengumuman">
					<Header/>
					<main role="main">
					<PageBanner/>
					<Pengumuman/>
					</main>
					<MyCompany/>
					<Footer/>
					
				</div>
			)
		}
		return content;
	}
}