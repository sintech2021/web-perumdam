
class HomePage extends React.Component{
	state ={
		history : window.history
	}
 
  componentDidMount(){
  	
  }	
  handleClick() {
    this.state.history.pushState({},"Home Page","/home");
  }
  render(){
  	return (
  		<div id="app">
				<Header/>
				<main role="main">
				<MyCarousel/>
				<h1 className="big-title text-center" >Layanan</h1>
				<div className="underline" style={{marginBottom:'1.5em'}}></div>
				<h2 className="service-description text-center" style={{padding:'1em'}}>Layanan Pelanggan PERUMDAM Tirta Kerta Raharja Kabupaten Tangerang</h2> 
				<ServiceButton/>
				<h1 className="big-title text-center">Berita</h1>
				<div className="underline" style={{marginBottom:'1.5em'}}></div>
				<MyNews/>
				<TentangKami/>
				<h1 className="big-title text-center">Galeri</h1>
				<div className="underline" style={{marginBottom:'1.5em'}}></div>
				<MyGalery/>
				</main>
				<MyCompany/>
				<Footer/>
				
			</div>
	  );
  }
  
}