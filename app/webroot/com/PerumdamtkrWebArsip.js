class PerumdamtkrWebArsip extends React.Component{
	
	render(){
		const {is_ajax} = this.props;
		let content = (<div></div>);

		if(is_ajax){
			content = (
				<div id="arsip">
					<PageBanner/>
					<Arsip/>
				</div>
			)
		}else{
			content = (
				<div id="arsip">
					<Header/>
					<main role="main">
					<PageBanner/>
					<Arsip/>
					</main>
					<MyCompany/>
					<Footer/>
					
				</div>
			);
		}
		return content;
		 
	}
}