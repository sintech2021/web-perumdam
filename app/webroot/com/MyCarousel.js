 

class MyCarousel extends React.Component{
	constructor(props){
		super(props);
		this.state ={
			data : [],
			page_list : [],
			// store index of all 1 .. total_pages
			pages : [],
			total_pages : 0,
			page_size : 7,
			active_index : 0,
			pages_maps : {}

		};
		this.updatePageListPublic = (index) => {
			this.updatePageList(index)
		}
	}
	
	onNavigateMenu = (e)=>{
		const el = e.target;
		if($(el).hasClass('induk')){
		}else{
			bindHistoryPushState(el);
		}
		e.preventDefault();
		return false;
	}
	updatePageList(index){

		if(typeof index !== 'undefined'){
			this.setState({active_index:index});
		}
		const {pages_maps} = this.state;
		setTimeout(()=>{
			const {active_index} = this.state;

			for(var j=0;j<pages_maps.length-1;j++){
				const page_list = pages_maps[j];

				if(page_list.indexOf(active_index) != -1){
					this.setState({page_list});
					break;
				} 
			}
		},250);
		
	}
	initPageMaps(){
		const {pages,page_size} = this.state;
		
		let total_pages = Math.ceil(pages.length/page_size);

		let pages_maps = [[-1]];
		for(var i = 1; i <= total_pages;i++){
			let pages_maps_items = pages.slice((i - 1) * page_size, i * page_size);
			pages_maps.push(pages_maps_items);
		}
		this.setState({pages_maps});

		setTimeout(()=>{
			this.updatePageList(0);
		},250)
	}
	 
	
	getData = async ()=>{
		const data = await Store.Data.get_caraousel();
		return data;
	}
	setActiveIndex(index, event){
 		const idx = parseInt(index) + 1;
 		$('#myCarousel').carousel(idx);
 		event.preventDefault();
 		return false;			
 	}
	async componentWillMount(){
		const res = await this.getData();
		this.setState({
			data : res 
		});
		setTimeout(()=>{
		const {data} = this.state;	
		if(typeof data !== 'undefined'){
 			let pages = [];
 			if(typeof data.forEach == 'function'){
 				data.forEach((row,index)=>{
 					pages.push(index);
 				});
 				this.setState({
 					pages:pages
 				});
 			}
 			this.initPageMaps();
 		};

		},250);

		setTimeout(()=>{
			$('#myCarousel').on('slide.bs.carousel', ()=> {
			  var cr = $('#myCarousel .carousel-item.active');
			  const index = parseInt(cr.attr('index'));	
			  var is = cr.find('.image-shadow');
			  is.height(cr.height());
			  this.updatePageList(index);

			});
		},1000);
	}
	slugify(str) {
	 
		return slugify(str,6);
	}
	getUrl(row){
		const slug = this.slugify(row.title);
		return site_url() + `/read/${row.id}/featured/${slug}`;
	}
	onImage(){
		const href = $(event.target).attr('href');
		if(href != '' && href != '#'){
			window.open(href,'_blank');
		}
	}
	render(){
		let carouselHandles = '';
		let carouselItems = '';
		let caraouselPager = '';

		const {data,page_list,active_index} = this.state;
		 
		if(typeof data.map == 'function'){
			caraouselPager = (
				<div className="caraousel-pagination">
					<ul className="pagination">
					{
						page_list.map((row,index)=>{
							const is_active = row == active_index ? 'active' : '';
							return (
								<li className={`item-${row} ${is_active}`} key={`item-${row}-${index}`}>
									<a href="#" onClick={()=>{this.setActiveIndex(row,event);}} className="pagination-item">{''}</a>
								</li>
							)
						})
					}
					</ul>
				</div>)

			carouselHandles = data.map((row,index)=>{
      			return(<li key={index} data-target="#myCarousel" data-slide-to={index} className={index==0?'active':''}></li>)

			})
			carouselItems = data.map((row,index)=>{
				const imageUrl =   row.img_url;
				return (
				<div index={index} className={index==0?"carousel-item active":"carousel-item"}  key={`caraousel-${index}`} >
		        <div className="image-container"><img onClick={this.onImage} href={row.url} src={imageUrl}/></div>
		        <div className="container">
		          <div className="carousel-caption text-center">
		            <h1><a onClick={this.onNavigateMenu} className="carousel-link" style={{color:'#fff'}} href={this.getUrl(row)}>{row.title}</a></h1>
		          </div>
		        </div>
		      </div>
      )
			})
		}
		return(
			<div className="container" style={{maxWidth:'100%',margin:0,padding:0}}>
			<div id="myCarousel" className="carousel slide" data-ride="carousel">
			    <ol className="carousel-indicators">
			      {carouselHandles}
			    </ol>
			    <div className="carousel-inner">
			       {carouselItems}
			    </div>
			    <a className="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
			      <span className="carousel-control-prev-icon" aria-hidden="true"></span>
			      <span className="sr-only">Previous</span>
			    </a>
			    <a className="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
			      <span className="carousel-control-next-icon" aria-hidden="true"></span>
			      <span className="sr-only">Next</span>
			    </a>
			  </div>
			  {caraouselPager}
			  </div>
		)
	}
}