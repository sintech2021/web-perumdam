function slugify(str,limitWordCount) {
  if(typeof str=='undefined')
    return;
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();
    
    // remove accents, swap ñ for n, etc
    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
    var to   = "aaaaeeeeiiiioooouuuunc------";
    for (var i=0, l=from.length ; i<l ; i++) {
      str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }
  
    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
      .replace(/\s+/g, '-') // collapse whitespace and replace by -
      .replace(/-+/g, '-'); // collapse dashes
    if(typeof limitWordCount != 'undefined'){
      const words = str.split('-');
      let slugs = [];
      if(limitWordCount > words.length){
        limitWordCount = words.length;
      }
      for(let i = 0 ; i < limitWordCount;i++){
        slugs.push(words[i]);
      }
      return slugs.join('-')
    }
    return str;
  }
function tgl_indo(tanggal){       
    try{
      let tgl_indo = '';
      if(typeof tanggal != 'string')
        return '';
      const bulan = ['','Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'];
      
      
      let split    = tanggal.split('-');
      if(!Array.isArray(split)){
        return '';
      }else{
        if(split.length==3){
          let bl_idx = typeof split[1] != 'undefined' ? split[1].replace(/^0/,''):0;
          let tgl = typeof split[2] != 'undefined' ? split[2].replace(/^0/,''):1;
          tgl_indo = `${tgl} ${bulan[bl_idx]} ${split[0]}`;
          return tgl_indo;
        }
        
      }
    }
    catch(e){
      return '';
    }
  }
function format_number(angka)
{
  var rupiah = '';    
  var angkarev = angka.toString().split('').reverse().join('');
  for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
  return rupiah.split('',rupiah.length-1).reverse().join('');
}

    function tweetCurrentPage()
    { window.open("https://twitter.com/share?url="+ encodeURIComponent(window.location.href)+"&text="+document.title, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false; }
function shereThisUrl(el){
  if($(el).hasClass('fb')){
      var fbpopup = window.open("https://www.facebook.com/sharer/sharer.php?u=" + window.location.href, "pop", "width=600, height=400, scrollbars=no");

  }
  if($(el).hasClass('twitter')){
    tweetCurrentPage()
      
  }
}
window.onpopstate = function(event){
    const win  = event.target;
    const href = win.location.href.replace(/([a-z_A-Z_0-9])\/\//g,'$1/');
    if(href == site_url()+'/'){
      return;
    }
    let caption = '';
    try{
    window.history.pushState({},caption,href);

  }catch(e){
  }
    Proxy.get(href).then((res)=>{
      // document.write(res);
      
      // setTimeout(()=>{
        try{
          if(res !== ''){
             $('main[role=main]').replaceWith('<main role="main" id="main"></main>');
        $('#ajax').html(res);
        setTimeout(()=>{
            window.scroll(0,findPos(document.getElementById("main")));
          },250)
          }
        }catch(e){}
       
      // },50)
    });
};
function bindHistoryPushState(el, prop, scroll) {
  if(typeof scroll =='undefined'){
    scroll = false;
  }
  if(typeof prop == 'undefined'){
    prop = 'href';
  }
  if(el.nodeName.toString() != 'A' && el.nodeName.toString() != 'DIV'){
    el = el.parentNode;
    
  }
  if(el.nodeName.toString() == 'DIV'){
      el = event.target;
      
  }
  const $el = $(el);

  if($el.attr('data-toggle') == 'dropdown'){
    return;
  }
  if( !$el.attr('bind_to_push_state') )
  {
    try{
   
    $el.click(()=>{
      let caption = '';
      let href = '';

      let target = el;
      // if(target.nodeName.toString() != 'A' && el.nodeName.toString() != 'DIV'){
      //   target = target.parentNode;

      // }
      if(el.nodeName.toString() == 'DIV'){
        target = el;
        caption = $(target).text();
        href = $(target).attr(prop);
        if(typeof href == 'string'){
           $el.attr('bind_to_push_state','yes');
          href = href.replace(/([a-z_A-Z_0-9])\/\//g,'$1/');
        }else{
          href = '';
        }
      }else{
        caption = $(target).text();
        href = $(target).attr(prop);

        if(typeof href == 'string'){
           $el.attr('bind_to_push_state','yes');
          href = href.replace(/([a-z_A-Z_0-9])\/\//g,'$1/');
        }else{
          href = '';
        }
      }
      
      if(href == ''){
        return;
      }
      window.history.pushState({},caption,href);

      Proxy.get(href).then((res)=>{
        // document.write(res);
        
        // setTimeout(()=>{
          $('main[role=main]').replaceWith('<main role="main" id="main"></main>');
          $('#ajax').html(res);

          setTimeout(()=>{
            window.scroll(0,findPos(document.getElementById("main")));
          },250)
          
          const navbar_visible = $('#navbarCollapse:visible');
          if(navbar_visible){
            $('#navbarCollapse').toggle();
          }
        // },50)
      }).catch((error)=>{
        document.location = href;
      });
      event.preventDefault();
      return false;
    });
    $el.click();
     }catch(e){
    }
  }
}
function loadPushState(href) {
  window.history.pushState({},'caption',href);

   
      Proxy.get(href).then((res)=>{
        // document.write(res);
        
        // setTimeout(()=>{
          $('main[role=main]').replaceWith('<main role="main" id="main"></main>');
          $('#ajax').html(res);

          setTimeout(()=>{
            window.scroll(0,findPos(document.getElementById("main")));
          },250)
          
          const navbar_visible = $('#navbarCollapse:visible');
          if(navbar_visible){
            $('#navbarCollapse').toggle();
          }
        // },50)
      }).catch((error)=>{
        document.location = href;
      });
}
function getFirstImg(str){
  try{
    var ele = document.createElement("div");
    ele.innerHTML = str;
    var image = ele.querySelector("img");
    return image.outerHTML;
  }catch(e){
    return '';
  }
  
}
function onNavigateMenu(e){
    const el = e.target;
    if($(el).hasClass('induk')){
    }else{
      bindHistoryPushState(el);
    }
    e.preventDefault();
    return false;
  }
//Finds y value of given object
function findPos(obj) {
    var curtop = 0;
    if (obj.offsetParent) {
        do {
            curtop += obj.offsetTop;
        } while (obj = obj.offsetParent);
    return [curtop];
    }
}

function strip_tags(originalString){
  return originalString.replace(/(<([^>]+)>)/gi, "");
}

function redirect(href,caption){
  window.history.pushState({},caption,href);

   
  Proxy.get(href).then((res)=>{

      $('main[role=main]').replaceWith('<main role="main" id="main"></main>');
      $('#ajax').html(res);

      setTimeout(()=>{
        window.scroll(0,findPos(document.getElementById("main")));
      },250)
      
      const navbar_visible = $('#navbarCollapse:visible');
      if(navbar_visible){
        $('#navbarCollapse').toggle();
      }
  }).catch((error)=>{
    document.location = href;
  });
}
$(document).ready(function(){
  let btnMobileMenu = $('button.navbar-toggler:first');
  btnMobileMenu.click(function(){
    const visible = $('#navbarCollapse:visible');
    const height  = visible.length != 0 ? '55px':'auto';
    // if(visible.length!=0){
      $('#navbarCollapse').css('height',height).toggle()
    // }
   
  })
})
var MD5 = function(d){var r = M(V(Y(X(d),8*d.length)));return r.toLowerCase()};function M(d){for(var _,m="0123456789ABCDEF",f="",r=0;r<d.length;r++)_=d.charCodeAt(r),f+=m.charAt(_>>>4&15)+m.charAt(15&_);return f}function X(d){for(var _=Array(d.length>>2),m=0;m<_.length;m++)_[m]=0;for(m=0;m<8*d.length;m+=8)_[m>>5]|=(255&d.charCodeAt(m/8))<<m%32;return _}function V(d){for(var _="",m=0;m<32*d.length;m+=8)_+=String.fromCharCode(d[m>>5]>>>m%32&255);return _}function Y(d,_){d[_>>5]|=128<<_%32,d[14+(_+64>>>9<<4)]=_;for(var m=1732584193,f=-271733879,r=-1732584194,i=271733878,n=0;n<d.length;n+=16){var h=m,t=f,g=r,e=i;f=md5_ii(f=md5_ii(f=md5_ii(f=md5_ii(f=md5_hh(f=md5_hh(f=md5_hh(f=md5_hh(f=md5_gg(f=md5_gg(f=md5_gg(f=md5_gg(f=md5_ff(f=md5_ff(f=md5_ff(f=md5_ff(f,r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+0],7,-680876936),f,r,d[n+1],12,-389564586),m,f,d[n+2],17,606105819),i,m,d[n+3],22,-1044525330),r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+4],7,-176418897),f,r,d[n+5],12,1200080426),m,f,d[n+6],17,-1473231341),i,m,d[n+7],22,-45705983),r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+8],7,1770035416),f,r,d[n+9],12,-1958414417),m,f,d[n+10],17,-42063),i,m,d[n+11],22,-1990404162),r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+12],7,1804603682),f,r,d[n+13],12,-40341101),m,f,d[n+14],17,-1502002290),i,m,d[n+15],22,1236535329),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+1],5,-165796510),f,r,d[n+6],9,-1069501632),m,f,d[n+11],14,643717713),i,m,d[n+0],20,-373897302),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+5],5,-701558691),f,r,d[n+10],9,38016083),m,f,d[n+15],14,-660478335),i,m,d[n+4],20,-405537848),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+9],5,568446438),f,r,d[n+14],9,-1019803690),m,f,d[n+3],14,-187363961),i,m,d[n+8],20,1163531501),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+13],5,-1444681467),f,r,d[n+2],9,-51403784),m,f,d[n+7],14,1735328473),i,m,d[n+12],20,-1926607734),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+5],4,-378558),f,r,d[n+8],11,-2022574463),m,f,d[n+11],16,1839030562),i,m,d[n+14],23,-35309556),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+1],4,-1530992060),f,r,d[n+4],11,1272893353),m,f,d[n+7],16,-155497632),i,m,d[n+10],23,-1094730640),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+13],4,681279174),f,r,d[n+0],11,-358537222),m,f,d[n+3],16,-722521979),i,m,d[n+6],23,76029189),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+9],4,-640364487),f,r,d[n+12],11,-421815835),m,f,d[n+15],16,530742520),i,m,d[n+2],23,-995338651),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+0],6,-198630844),f,r,d[n+7],10,1126891415),m,f,d[n+14],15,-1416354905),i,m,d[n+5],21,-57434055),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+12],6,1700485571),f,r,d[n+3],10,-1894986606),m,f,d[n+10],15,-1051523),i,m,d[n+1],21,-2054922799),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+8],6,1873313359),f,r,d[n+15],10,-30611744),m,f,d[n+6],15,-1560198380),i,m,d[n+13],21,1309151649),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+4],6,-145523070),f,r,d[n+11],10,-1120210379),m,f,d[n+2],15,718787259),i,m,d[n+9],21,-343485551),m=safe_add(m,h),f=safe_add(f,t),r=safe_add(r,g),i=safe_add(i,e)}return Array(m,f,r,i)}function md5_cmn(d,_,m,f,r,i){return safe_add(bit_rol(safe_add(safe_add(_,d),safe_add(f,i)),r),m)}function md5_ff(d,_,m,f,r,i,n){return md5_cmn(_&m|~_&f,d,_,r,i,n)}function md5_gg(d,_,m,f,r,i,n){return md5_cmn(_&f|m&~f,d,_,r,i,n)}function md5_hh(d,_,m,f,r,i,n){return md5_cmn(_^m^f,d,_,r,i,n)}function md5_ii(d,_,m,f,r,i,n){return md5_cmn(m^(_|~f),d,_,r,i,n)}function safe_add(d,_){var m=(65535&d)+(65535&_);return(d>>16)+(_>>16)+(m>>16)<<16|65535&m}function bit_rol(d,_){return d<<_|d>>>32-_}

 

class Web extends React.Component{
  
  render(){
    return (
      <div id="app">
        <HomePage/>
        <main role="main">
   
        </main>
      </div>
    )
  }
}