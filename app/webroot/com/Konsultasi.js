class Konsultasi extends React.Component{
	state={
		list: app_data.list
	}
 	onClick(){
 		const el = $(event.target);
 		const expanded = el.attr('aria-expanded') == 'true';
 		const rc = expanded ? 'fa-chevron-right' : 'fa-chevron-down';
 		const ac = !expanded ? 'fa-chevron-right' : 'fa-chevron-down';
		el.find('i.fa').removeClass(rc).addClass(ac);
	}
	render(){
			const {list} = this.state;

		return(
			<div className="konsultasi container">

			<div className="wrapper center-block">
			<h1 className="big-title text-center" style={{paddingTop:'1.5em'}}>Frequently Asked Questions (FAQ)</h1>
			  <div className="accordion" id="accordion_konsultasi">
			  {
			  	list.map((row,index)=>{
			  		return (
			  			<div className="card" key={`konsultasi-${index}`}>
					    <div className="card-header" id={`heading${index}`}>
					      <h2 className="mb-0">
					        <a onClick={this.onClick} className="btn btn-link btn-block text-left collapsed" data-toggle="collapse" data-target={`#collapse${index}`} aria-expanded="false" aria-controls={`collapse${index}`}>
					          {row.Consultation.title}
					          <i className="fa fa-chevron-right"></i>
					        </a>
					      </h2>
					    </div>
					    <div id={`collapse${index}`} className="collapse" aria-labelledby={`heading${index}`} data-parent="#accordion_konsultasi">
					      <div className="card-body" dangerouslySetInnerHTML={{__html:row.Consultation.keterangan}}>
					      </div>
					    </div>
					  </div>
			  		)
			  	})
			  }
			</div>
		</div>
		</div>
		)
	}
}