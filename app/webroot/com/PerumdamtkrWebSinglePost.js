class PerumdamtkrWebSinglePost extends React.Component{
	
	render(){
		const {is_ajax} = this.props;
		let content = (<div></div>);
			let cat = app_data.formatted_slug;
		if(is_ajax){
			content = (
				<div id="single">
					<PageBanner/>
					<SinglePost/>
					<div className="container" style={{backgroundColor:"#F9F9F9",paddingTop:'3em',maxWidth:'100%',margin:'0'}}>
					<h1 className="big-title text-center">Berita</h1>
					<div className="underline" style={{marginBottom:'1.5em'}}></div>
					<MyNews containerBg="#F9F9F9" category={cat}/>
					</div>
				</div>
			)
		}else{
			content = (
				<div id="single_post">
					<Header/>
					<main role="main">
					<PageBanner/>
					<SinglePost/>
					<div className="container" style={{backgroundColor:"#F9F9F9",paddingTop:'3em',maxWidth:'100%',margin:'0'}}>
					<h1 className="big-title text-center">Berita</h1>
					<div className="underline" style={{marginBottom:'1.5em'}}></div>
					<MyNews containerBg="#F9F9F9" category={cat}/>
					</div>
					</main>
					<MyCompany/>
					<Footer/>
				</div>
			)
		}
		return content;
		 
	}
}