class PerumdamtkrWeb extends React.Component{
	componentWillMount(){
		try{
			if(typeof app_data.homepage_popup_content == 'object'){
				if(typeof app_data.homepage_popup_content.ScmContentContent == 'object'){
					// alert(app_data.homepage_popup_content.ScmContentContent.value);
					$('#popup_modal .modal-body').html(app_data.homepage_popup_content.ScmContentContent.value)
					 $("#popup_modal").modal();
				}
			}
		}catch(e){
		}
		
	}
	render(){
		const {is_ajax} = this.props;
		let content = (<div></div>);
		if(is_ajax){
			content = (
				<div id="web">
					<MyCarousel/>
					<RunningText/>
					<h1 className="big-title text-center" style={{marginTop:'1.5em'}}>Layanan</h1>
					<div className="underline" style={{marginBottom:'1.5em'}}></div>
					<h2 className="service-description text-center" style={{padding:'1em',fontStyle:'italic'}}>Layanan Pelanggan PERUMDAM Tirta Kerta Raharja Kabupaten Tangerang</h2> 
					<ServiceButton/>
					<h1 className="big-title text-center">Info Pelanggan</h1>
					<div className="underline" style={{marginBottom:'1.5em'}}></div>
					<h2 className="service-description text-center" style={{padding:'1em',fontStyle:'italic'}}>Info Untuk Pelanggan PERUMDAM Tirta Kerta Raharja Kabupaten Tangerang</h2> 

					<InfoPerumdam/>
					<h1 className="big-title text-center">Berita</h1>
					<div className="underline" style={{marginBottom:'1.5em'}}></div>
					<h2 className="service-description text-center" style={{padding:'1em',fontStyle:'italic'}}>Berita Terkini Seputar PERUMDAM Tirta Kerta Raharja Kabupaten Tangerang</h2>

					<MyNews/>
					<TentangKami/>
					<h1 className="big-title text-center"  style={{marginTop:'1.5em'}}>Galeri</h1>
					<div className="underline" style={{marginBottom:'1.5em'}}></div>
					<MyGalery/>
				</div>
			);
		}else{
			content = (
				<div id="web">
					<Header/>
					<main role="main">
					<MyCarousel/>
					<RunningText/>
					
					<h1 className="big-title text-center" style={{marginTop:'1.5em'}}>Layanan</h1>
					<div className="underline" style={{marginBottom:'1.5em'}}></div>
					<h2 className="service-description text-center" style={{padding:'1em',fontStyle:'italic'}}>Layanan Pelanggan PERUMDAM Tirta Kerta Raharja Kabupaten Tangerang</h2> 
					<ServiceButton/>
					<h1 className="big-title text-center">Info Pelanggan</h1>
					<div className="underline" style={{marginBottom:'1.5em'}}></div>
					<h2 className="service-description text-center" style={{padding:'1em',fontStyle:'italic'}}>Info Untuk Pelanggan PERUMDAM Tirta Kerta Raharja Kabupaten Tangerang</h2> 

					<InfoPerumdam/>
					<h1 className="big-title text-center">Berita</h1>
					<div className="underline" style={{marginBottom:'1.5em'}}></div>
					<h2 className="service-description text-center" style={{padding:'1em',fontStyle:'italic'}}>Berita Terkini Seputar PERUMDAM Tirta Kerta Raharja Kabupaten Tangerang</h2>

					<MyNews/>
					<TentangKami/>
					<h1 className="big-title text-center" style={{marginTop:'1.5em'}}>Galeri</h1>
					<div className="underline" style={{marginBottom:'1.5em'}}></div>
					<MyGalery/>
					</main>
					<MyCompany/>
					<Footer/>
					
				</div>
			)
		}
		return content;
	}
}