
class InfoPerumdam extends React.Component{
	state ={
		list:[]
	}
	getData = async ()=>{
		const data = await Store.Data.get_info_perumdam();
		return data;
	}
	
	async componentWillMount(){
		const res = await this.getData();
		this.setState({
			list : res 
		});
		$('#info-perumdam').on('slide.bs.carousel', function (e) {

	    /*
	        CC 2.0 License Iatek LLC 2018
	        Attribution required
	    */
	    var $e = $(e.relatedTarget);
	    var idx = $e.index();
	    var itemsPerSlide = 5;
	    var totalItems = $('.carousel-item').length;
	    
	    if (idx >= totalItems-(itemsPerSlide-1)) {
	        var it = itemsPerSlide - (totalItems - idx);
	        for (var i=0; i<it; i++) {
	            // append slides to end
	            if (e.direction=="left") {
	                $('.carousel-item').eq(i).appendTo('.carousel-inner');
	            }
	            else {
	                $('.carousel-item').eq(0).appendTo('.carousel-inner');
	            }
	        }
	    }
	}); 
 
	}
	onImage(){
		const href = $(event.target).attr('href');
		if(href != '' && href != '#'){
			window.open(href,'_blank');
		}
	}
	render(){
		const {list} = this.state;
		return (
			<div className="info_perumdam"> 
        		<div id="info-perumdam" className="carousel slide" data-ride="carousel" style={{margin:'1em 0'}}>
        			<div className="carousel-inner row w-100 mx-auto" role="listbox" style={{height:315}}>
        				{
        					list.map((j,i)=>{
        						return(
        							<div className={"carousel-item col-12 col-sm-6 col-md-4 col-lg-3"+(i==0?" active":'')}>
										<img onClick={this.onImage} href={j.url} src={site_url()+'/uploads/info_perumdam/'+j.path} className="img-fluid mx-auto d-block" alt="img2"/>
									</div>
        						)
        					})
        				}
            			 
        			</div>
        			<a className="carousel-control-prev" href="#info-perumdam" role="button" data-slide="prev">
						<span className="carousel-control-prev-icon" aria-hidden="true"></span>
						<span className="sr-only">Previous</span>
					</a>
					<a className="carousel-control-next" href="#info-perumdam" role="button" data-slide="next">
						<span className="carousel-control-next-icon" aria-hidden="true"></span>
						<span className="sr-only">Next</span>
					</a>
        		</div>
			</div>
		)
	}
}