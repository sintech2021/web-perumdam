
class GaleriItems extends React.Component{
	state={
		list:[]
	}
	getData = async ()=>{
		const data = await Store.Data.get_galery_items(app_data.galery_id);
		return data;
	}
	
	async componentWillMount(){
		const res = await this.getData();
		this.setState({
			list : res 
		});
		 
 	

		setTimeout(()=>{
			
 			

			$(".main_galery a.thumbnail.group").fancybox({
				'transitionIn'	:	'elastic',
				'transitionOut'	:	'elastic',
				'speedIn'		:	600, 
				'speedOut'		:	200, 
				'overlayShow'	:	true
			});
		},3000);
	}
	render(){
		const {list} = this.state;
 		 
		return (
			<div className="container">
				<div className="main_galery row">
					<div className="col-md-12">
						<div className="row">
						{
							list.map((j,i)=>{
								const cls = j.tipe==1?' iframe':'';
								const href = j.tipe==1? j.url:j.path;
								return(
									<div className=" col-md-4 items">
										<div className="info">
						                   	<h4>{j.title}</h4>
						                </div>
						                <a className={`thumbnail group ${cls}`} data-fancybox="gallery" data-caption={j.title}  href={href} style={{height:400,width:'100%',display:'block',backgroundSize:'cover',backgroundImage:'url('+ `${j.thumb})`}} title={j.title}>

						                </a>
						            </div>
								)
							})
						}
				            
						</div>
					</div>
				</div>
			</div>
		)
	}
}