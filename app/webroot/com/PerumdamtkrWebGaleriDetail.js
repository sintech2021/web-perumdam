class PerumdamtkrWebGaleriDetail extends React.Component{
	
	render(){
		const {is_ajax} = this.props;
		let content = (<div></div>);
		let detail_title = 'Galeri Foto & Video';
		try{
			detail_title = app_data.detail.GaleryItem.title;
		}catch(e){}	
		if(is_ajax){
			content = (
				<div id="galeri">

					<PageBanner/>
			<h1 className="big-title text-center" style={{padding:'1em'}}>{detail_title}</h1>

					<GaleriDetail/>
				</div>
			)
		}else{
			content = (
				<div id="galeri">

					<Header/>
					<main role="main">
					<PageBanner/>
			<h1 className="big-title text-center" style={{padding:'1em'}}>{detail_title}</h1>
					
					<GaleriDetail/>
					</main>
					<MyCompany/>
					<Footer/>
					
				</div>
			);
		}
		return content;
		 
	}
}