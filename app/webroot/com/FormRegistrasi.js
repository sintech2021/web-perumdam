class FormRegistrasi extends React.Component{
  state = {
    gCaptchaSiteKey:'6LecSeYUAAAAADazed6MSoJXAW53CyaHJXgZ8i79',
    gRecaptcha:'',
    gRecaptchaContainer:'recaptcha_container',
    gRecaptchaPanthom:0,
    nama_pemohon : '',
    nama_pemilik : '',
    nomor_ktp : '',
    nomor_hp : '',
    nomor_telepon : '',
    email : '',
    alamat:'',
    // nama_pemohon : 'Bambang Setiadi',
    // nama_pemilik : 'Bambang Setiadi',
    // nomor_ktp : '0000111122224444',
    // nomor_hp : '08953713088',
    // nomor_telepon : '',
    // email : 'cristminix@gmail.com',
    // alamat:'Komplek Kehakiman',

    foto:'',
    id_prop : '',
    kode_pos : '',
    id_kab  : '',
    id_kec  : '',
    id_kel  : '',
    error_message:'',  
    foto_filename:'',
    regexp_number : /^[0-9\b]+$/,
    foto_preview : null,
    kabupaten_list:[],
    kecamatan_list :[
      {
        kdKec : '',
        kecamatan : '-- Pilih Kecamatan --'
      }
    ],
    kelurahan_list:[
      {
        kdKel : '',
        kelurahan : '-- Pilih Kelurahan --'
      }
    ],
    url_kec : `/pendaftaran/get_kec`,
    url_kab : `/pendaftaran/get_kab`,
    url_kel : `/pendaftaran/get_kel`,
    kabupaten:'',
    kecamatan:'',
    kelurahan:''
  }
  async componentWillMount(){
    const res = await Store.Service.get_kabupaten_list();
    if(res.status == 200){
      const kabupaten_list = res.data;
      kabupaten_list.unshift({
        kdKab : '',
        kabupaten : '-- Pilih Kabupaten --'
      });
      this.setState({kabupaten_list : kabupaten_list});
    }
    this.updateRecaptchaContainer();
    this.loadRecaptcha();
  }
  loadRecaptcha(){
    var el_str  = `<div className="g-recaptcha" id="${this.state.gRecaptchaContainer}"></div>`;
    var parent = $('#recaptcha_parent_container');
    parent.html(el_str);

    var captchaContainer = grecaptcha.render(this.state.gRecaptchaContainer, {
        'sitekey' : this.state.gCaptchaSiteKey,
        'callback' : (response) =>{
          this.setState({gRecaptcha:response})
        }
    }); 
  }
  onKabupatenChanged = async ()=>{
    const id_kab = event.target.value;
    const kabupaten = $(event.target).find('option:selected').text();
    this.setState({id_kab: id_kab,kabupaten:kabupaten});
    
    const res = await Store.Service.get_kecamatan_list(id_kab);
    if(res.status == 200){
      const kecamatan_list = res.data;
      kecamatan_list.unshift({
        kdKec : '',
        kecamatan : '-- Pilih Kecamatan --'
      });
      this.setState({kecamatan_list : kecamatan_list,kode_pos:'',id_kec:''});
    }

  } 
  onKecamatanChanged = async ()=>{
    const el = event.target;
    const id_kec = el.value;
    const {id_kab} = this.state;
    const kecamatan = $(el).find('option:selected').text();

    this.setState({id_kec: id_kec,kecamatan:kecamatan});
    
    const res = await Store.Service.get_kelurahan_list(id_kec,id_kab);
    if(res.status == 200){
      const kelurahan_list = res.data;
      kelurahan_list.unshift({
        kdKel : '',
        kelurahan : '-- Pilih Kelurahan --'
      });
      this.setState({kelurahan_list : kelurahan_list,kode_pos:'',id_kel:''});
    }
  }
  onKelurahanChanged = ()=>{
    const el = event.target;
    const id_kel = el.value;
    const kode_pos = $(el).find('option:selected').attr('kode_pos');
    const kelurahan = $(el).find('option:selected').text();
    
    this.setState({id_kel: id_kel,kode_pos:kode_pos,kelurahan:kelurahan});
  }
  onInputChange = (evt)=>{
    const el    = evt.target;
    const name  = el.name;
    let value = el.value;
    let state   = {};
    const number_only_fields = ['nomor_hp','nomor_ktp','nomor_telepon','kode_pos'];
    const is_number_only_field = number_only_fields.indexOf(name) != -1;

    if(is_number_only_field){
      if(value === '' || this.state.regexp_number.test(value)){
        state[name] = value;
        this.setState(state);
      }
    }else{
      state[name] = value;
      this.setState(state);
    }
  }

  onFotoChanged = ()=>{
    const valid_mimes = ['image/jpeg', 'image/pjpeg','image/jpeg', 'image/pjpeg','image/png',  'image/x-png'];
    const input    = event.target;
    if (input.files && input.files[0]) {
      try{
        const file       = input.files[0];
        const fileSize   = file.size / Math.pow(1024,2);
        const fileMime   = file.type;
        const validMime  = valid_mimes.indexOf(fileMime) != -1;
        const validSize  = fileSize <= 2;
        const fileName   = file.name;

        if(!validMime){
          alert('Jenis file yang anda pilih tidak valid, silahkan pilih berkas Gambar .jpeg atau .png');
          return;
        }
        if(!validSize){
          alert('Ukuran file melebihi batas maksimal 2MB');
          return;
        }

        var reader = new FileReader();
      
        reader.onload = (e) =>{
          this.setState({
            foto_preview : e.target.result,
            foto_filename : fileName
          });
        }
        
        reader.readAsDataURL(input.files[0]); // convert to base64 string
      }
      catch(e){
      }
      
    }
  }
  updateRecaptchaContainer(){
    let gRecaptchaPanthom = this.state.gRecaptchaPanthom;
    gRecaptchaPanthom++;
    let gRecaptchaContainer = `recaptcha_container${gRecaptchaPanthom}`
    this.setState({
      gRecaptchaContainer:gRecaptchaContainer,
      gRecaptchaPanthom: gRecaptchaPanthom
    });
  }
  resetForm = ()=>{
    this.updateRecaptchaContainer();
    this.loadRecaptcha();
    this.setState({
      gRecaptcha:'',
      nama_pemohon : '',
      nama_pemilik : '',
      nomor_ktp : '',
      nomor_hp : '',
      nomor_telepon : '',
      email : '',
      alamat:'',
      foto:'',
      // id_prop : '',
      kode_pos : '',
      id_kab  : '',
      id_kec  : '',
      id_kel  : '',
      error_message:'',  
      foto_filename:'',
      foto_preview : null,
      kabupaten:'',
      kecamatan:'',
      kelurahan:''

    });
  }
  onSelectFoto = ()=>{
    event.preventDefault();
    return false;
  }
  onSubmitForm = async ()=>{
    this.setState({error_message:''});
    const {gRecaptcha,kabupaten,kecamatan,kelurahan,nama_pemohon,nama_pemilik,nomor_ktp,nomor_hp,nomor_telepon,email,id_kab,id_kec,id_kel,alamat,kode_pos,foto_filename,foto_preview} = this.state;
    let foto = foto_preview == null? '' : foto_preview;
    const res = await Store.Service.do_register(nama_pemohon,nama_pemilik,nomor_ktp,nomor_hp,nomor_telepon,email,id_kab,id_kec,id_kel,alamat,kode_pos,foto,foto_filename,kabupaten,kecamatan,kelurahan,gRecaptcha);
    if(res.success){
      this.resetForm();
      const detail_reg_url = site_url() + `/detail_reg/${res.data.id}/${res.data.kode_pendaftaran}`;
      document.location.href = detail_reg_url;
    }else{
      const msg = res.msg;
      this.updateRecaptchaContainer();
      this.loadRecaptcha();
      
      this.setState({error_message:msg})
    }  
    event.preventDefault();
    return false;
  }
  onFormSubmit = ()=>{
    event.preventDefault();
    return false;
  }
  render(){
     
    const {gCaptchaSiteKey,foto_preview, kabupaten_list, kecamatan_list, kelurahan_list, error_message} = this.state;
    const hasError = error_message.length > 0;
    return(
      <section className="form_registrasi container">
        <article className="row">
          <section className="col-md-12 wrp">
          <header>
              <h2 className="form-title">Form Registrasi Pelanggan</h2>
 
          </header>

          <form className="form-horizontal main-form" method="post" onSubmit={this.onSubmitForm}>
            <div className="form-group">
              <label htmlFor="nama_pemohon" className="control-label">Nama Pemohon</label>
              <div className="input-container">
              <input className="form-control" value={this.state.nama_pemohon} onChange={()=>{this.onInputChange(event)}} name="nama_pemohon" id="nama_pemohon" placeholder="Ketik Nama Lengkap Pemohon disini" type="text" required="required"/>
              </div>
            </div> 

            <div className="form-group">
              <label htmlFor="nama_pemilik" className="control-label">Nama Pemilik</label>
              <div className="input-container">
                <input className="form-control" value={this.state.nama_pemilik} onChange={()=>{this.onInputChange(event)}} name="nama_pemilik" id="nama_pemilik" placeholder="Ketik Nama Lengkap Pemilik disini" type="text"/>
              </div>
            </div>
            <div className="form-group">
              <label htmlFor="nomor_ktp" className="control-label">Nomor KTP</label>
              <div className="input-container">
                <input className="form-control" value={this.state.nomor_ktp} onChange={()=>{this.onInputChange(event)}} maxLength={16} name="nomor_ktp" id="nomor_ktp" placeholder="Ketik Nomor KTP Anda disini" type="tel" required="required"/>
              </div>
            </div>
            <div className="form-group">
              <label htmlFor="nomor_hp" className="control-label">Nomor HP</label>
              <div className="input-container">
                <input className="form-control" value={this.state.nomor_hp} onChange={()=>{this.onInputChange(event)}}  maxLength={14} name="nomor_hp" id="nomor_hp" placeholder="Ketik Nomor HP Anda disini" type="tel" required="required"/>
              </div>
            </div>

            <div className="form-group">
              <label htmlFor="email" className="control-label">Email</label>
              <div className="input-container">
                    <input className="form-control" value={this.state.email} onChange={()=>{this.onInputChange(event)}} name="email" id="email" placeholder="Ketik Alamat Email Anda disini" type="email" required="required"/>
              </div>
            </div>
            <div className="form-group">
              <label htmlFor="id_kab" className="control-label">Kabupaten</label>
              <div className="input-container selectWrapper">
                    <select value={this.state.id_kab} onChange={this.onKabupatenChanged} className="form-control" name="id_kab" id="id_kab" required="required">
                    {
                      kabupaten_list.map((row,index)=>{
                        return (
                          <option key={`kabupaten-${index}`} value={row.kdKab}>{row.kabupaten}</option>
                        )
                      })
                    }
                    </select>
              </div>
            </div>
            <div className="form-group">
              <label htmlFor="id_kec" className="control-label">Kecamatan</label>
              <div className="input-container selectWrapper">
                    <select value={this.state.id_kec} onChange={this.onKecamatanChanged} className="form-control" name="id_kec" id="id_kec" required="required">
                    {
                      kecamatan_list.map((row,index)=>{
                        return (
                          <option key={`kecamatan-${index}`} value={row.kdKec}>{row.kecamatan}</option>
                        )
                      })
                    }
                    </select>
              </div>
            </div>
            <div className="form-group">
              <label htmlFor="id_kel" className="control-label">Kelurahan</label>
              <div className="input-container selectWrapper">
                    <select value={this.state.id_kel} onChange={this.onKelurahanChanged} className="form-control" name="id_kel" id="id_kel" required="required">
                    {
                      kelurahan_list.map((row,index)=>{
                        return (
                          <option key={`kelurahan-${index}`} kode_pos={row.kdPos} value={row.kdKel}>{row.kelurahan}</option>
                        )
                      })
                    }
                    </select>
              </div>
            </div>

            <div className="form-group">
              <label htmlFor="alamat" className="control-label">Alamat</label>
              <div className="input-container">
                <textarea maxLength={225} className="form-control" value={this.state.alamat} onChange={()=>{this.onInputChange(event)}}  name="alamat" id="alamat" placeholder="Ketik alamat anda disini" type="text" required="required"></textarea>
              </div>
            </div>
            
            <div className="form-group">
              <label htmlFor="kode_pos" className="control-label">Kode Pos</label>
              <div className="input-container kode_pos">
                <input className="form-control" maxLength={6} value={this.state.kode_pos} onChange={()=>{this.onInputChange(event)}}  name="kode_pos" id="kode_pos" placeholder="Kode Pos" type="text" required="required"/>
              </div>
            </div>
            <div className="form-group">
              <label htmlFor="foto" className="control-label">Foto KTP</label>
              <div className="input-container foto">
                <input className="form-control fileUpload" value={this.state.foto} onChange={this.onFotoChanged}  name="foto" id="foto" placeholder="Foto" type="file" required="required"/>
                <button className="btn btn-primary" onClick={this.onSelectFoto}><i className="fa fa-file-image">  <span className="caption">Pilih Foto</span></i></button>                
                
              </div>
              
                {
                  foto_preview && (
                    <div className="input-container foto-preview" style={{backgroundImage:`url(${foto_preview})`}}></div>
                  )
                }
              
            </div>
            <div className="form-group">
              <div className="input-container">
                <div className="form-error-message" style={{display:hasError?'block':'none'}}>
                <div dangerouslySetInnerHTML={{__html: error_message}} />
                </div>
              </div>
            </div>
            <div className="form-group">
              <div className="row" style={{padding:'1em 0'}}>
                <div className="col-md-6" id="recaptcha_parent_container"> 
                  
                </div>
                <div className="col-md-6">
                  <div className="link-container" style={{textAlign:'right',marginTop:'1em'}}>
                    <a href="javascript:;" className="link_goto_page" onClick={this.onSubmitForm}>Mendaftar</a>
                  </div>
                  </div>
              </div>
            </div>
          
            </form>
          </section>
        </article>
      </section>
    )
  }
}