class ServiceItem extends React.Component{
	onNavigateMenu = (e)=>{

		 const el = e.target;
	
			bindHistoryPushState(el);
		e.preventDefault();
		return false;
	}

	render(){
		const {cls,title,icon,url} = this.props;
		return (
			<div className="col-12 col-md service_item">
				<a onClick={this.onNavigateMenu} className="service_button" href={site_url()+`/${url}`}>
					<img className="image_icon" src={icon}/>
				</a>
				<a onClick={this.onNavigateMenu} href={site_url()+`/${url}`}><h4 className={"service_title "+cls}>{title}</h4></a>
			</div>
		)
	}
}

class ServiceButtonSm extends React.Component{
	state={
		 
	}
	render(){
		return (
			<div id="service_list" className="container" style={{padding:'0 0 0 3em'}}>
				<div className="row">
					<div className="col-md-12 text-center"><h4>Layanan Pelanggan</h4></div>
				</div>
				<div className="row">
					<ServiceItem  title="Pendaftaran Pelanggan" icon={site_url()+"/icons/register.png"} cls="register" url="pendaftaran"/>
					<ServiceItem  title="Cek Status Pendaftaran" icon={site_url()+"/icons/check.png"} cls="register_check" url="register_check"/>
					<ServiceItem  title="Info Tagihan" icon={site_url()+"/icons/info.png"} cls="tagihan" url="tagihan"/>
					<ServiceItem  title="Info Tunggakan" icon={site_url()+"/icons/tunggakan.png"} cls="tagihan" url="tunggakan"/>
					<ServiceItem  title="Pengaduan" icon={site_url()+"/icons/pengaduan.png"} cls="srv-pengaduan" url="web/pengaduan"/>
				</div>
			</div>
		)
	}
}