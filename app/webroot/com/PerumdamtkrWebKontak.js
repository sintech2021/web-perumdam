class PerumdamtkrWebKontak extends React.Component{
	
	render(){
		const {is_ajax} = this.props;
		let content = (<div></div>);
		if(is_ajax){
			content = (
				<div id="kontak">
					<PageBanner/>
					<div className="bgMask"></div>
					<Kontak/>
					<SocialWidget/>
				</div>
			);
		}else{
			content = (
				<div id="kontak">
					<Header/>
					<main role="main">
					<PageBanner/>
					<div className="bgMask"></div>
					<Kontak/>
					<SocialWidget/>
					</main>

					<MyCompany/>
					<Footer/>
				</div>
			);
		}
		return content ;
		
	}
}