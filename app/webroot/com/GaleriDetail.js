
class GaleriDetail extends React.Component{
	state={
		data : null
	}
	getData = async ()=>{
		const data = await Store.Data.get_galery_detail(app_data.item_id);
		return data;
	}
	
	async componentWillMount(){
		const res = await this.getData();
		this.setState({
			data : res 
		});
		 
 
	}
	render(){
		const {data} = this.state;
 
		return (
			<div className="container">
				<div className="main_galery row">
					<div className="col-md-12">
						<div className="row">
						{
							data &&   (
									<div className=" col-md-4 items">
										<div className="info">
						                   	<h4>{data.title}</h4>
						                </div>
						                <a className="thumbnail" href="javascript:;"style={{height:400,width:'100%',display:'block',backgroundSize:'cover',backgroundImage:'url('+ `${data.path})`}}>

						                </a>
						            </div>
								)
						 
						}
				            
						</div>
					</div>
				</div>
			</div>
		)
	}
}