class SearchBar extends React.Component{

	state ={
		off: true,
		searchq: ''
	}
	getData = ()=>{

		if(typeof app_data.searchq != 'undefined'){
			this.setState({searchq:app_data.searchq});
		}
	}
	componentWillMount(){
		this.getData()
	}
	onInputFocus=()=>{
		this.setState({off:false})
	}
	onSearch = ()=>{
		const searchq = event.target.value;
    	this.setState({searchq: searchq});
	}
	onSubmitForm = ()=>{
		const {searchq} = this.state;
		if(searchq.length <=3){
			event.preventDefault();
			return false;
		}
		
	}
	render(){
	
	
		const {off,searchq} = this.state; 
		const placeholder = off? '' : 'cari'
		return(
 			<form onSubmit={this.onSubmitForm} className="form-inline mt-2 mt-md-0 cari" method="post" action={site_url()+'/search'}>
		        <input onChange={this.onSearch} className="form-control mr-sm-2 cari"  type="text" value={searchq} name="searchq" placeholder={placeholder} aria-label="Cari"/>
		     </form>
		)
	}
}
/*
<form className="form-inline my-2 my-lg-0">
	      <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search"/>
	    </form>
*/