class PerumdamtkrWebLayananInformasi extends React.Component{
	
	render(){
		const {is_ajax} = this.props;
		let content = (<div></div>);
		let exclude_id = '';
		try{exclude_id=app_data.article.id;}catch(e){}
		if(is_ajax){
			content = (
				<div id="layanan_informasi">
					<PageBanner/>
					<div style={{marginTop:'2em'}}></div>
					<SinglePost/>
					<div className="container" style={{backgroundColor:"#F9F9F9",paddingTop:'3em',maxWidth:'100%',margin:'0'}}>
					<h1 className="big-title text-center">Konten Terkait</h1>
					<div className="underline" style={{marginBottom:'1.5em'}}></div>
					<RelatedContent containerBg="#F9F9F9" category="layanan_informasi" use_ci={true}  exclude_id={exclude_id} no_link={true}/>
					</div>
				</div>
			)
		}else{
			content = (
				<div id="layanan_informasi">
				<Header/>
				<main role="main">
				<PageBanner/>
				<div style={{marginTop:'2em'}}></div>
				<SinglePost/>
				<div className="container" style={{backgroundColor:"#F9F9F9",paddingTop:'3em',maxWidth:'100%',margin:'0'}}>
				<h1 className="big-title text-center">Konten Terkait</h1>
				<div className="underline" style={{marginBottom:'1.5em'}}></div>
				<RelatedContent containerBg="#F9F9F9" category="layanan_informasi" use_ci={true} exclude_id={exclude_id} no_link={true}/>
				</div>
				</main>
				<MyCompany/>
				<Footer/>
				
			</div>
			);
		}
		return content;
		 
	}
}