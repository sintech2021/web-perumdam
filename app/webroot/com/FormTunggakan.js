class FormTunggakan extends React.Component{
  state = {
    id_pelanggan : '',
    submitLoadingCls:'',
    isLoading: false, 
     result:null,
    resultPelangganInfo:[],
    resultPelangganData:[],
    invalidCustomer:false,
    infoTunggakan:null,detailTunggakan:null,finalDataAll:[],
    bulan_list_x : {
      '01':'Januari',
      '02':'Februari',
      '03':'Maret',
      '04':'April',
      '05':'Mei',
      '06':'Juni',
      '07':'Juli',
      '08':'Agustus',
      '09':'September',
      '10':'Oktober',
      '11':'November',
      '12':'Desember',
    },
    error_message:'',  
    gCaptchaSiteKey:'6LecSeYUAAAAADazed6MSoJXAW53CyaHJXgZ8i79',
    gRecaptcha:'',
    gRecaptchaContainer:'recaptcha_container',
    gRecaptchaPanthom:0
  }
  componentWillMount(){
    setTimeout(()=>{
      this.updateRecaptchaContainer();
      this.loadRecaptcha();
    },1000);
  }
  loadRecaptcha(){
    var el_str  = `<div className="g-recaptcha" id="${this.state.gRecaptchaContainer}"></div>`;
    var parent = $('#recaptcha_parent_container');
    parent.html(el_str);

    grecaptcha.render(this.state.gRecaptchaContainer, {
        sitekey : this.state.gCaptchaSiteKey,
        callback : (response) =>{
          this.setState({gRecaptcha:response})
        }
    }); 
  }
  updateRecaptchaContainer(){
    let gRecaptchaPanthom = this.state.gRecaptchaPanthom + 1;
    let gRecaptchaContainer = `recaptcha_container${gRecaptchaPanthom}`
    this.setState({
      gRecaptchaContainer:gRecaptchaContainer,
      gRecaptchaPanthom: gRecaptchaPanthom
    });
  }
  onSubmitForm = async ()=>{
    if(!this.state.isLoading){
      
      const {gRecaptcha,id_pelanggan} = this.state;
      const id_pelanggan_clean = id_pelanggan.replace(/\W+/,'');
      // if(id_pelanggan_clean == ''){
      //   $('#id_pelanggan').focus();
      //   return;
      // }  
      this.setState({
        result : null,
        submitLoadingCls:'fa fa-spinner fa-spin',
        isLoading:true,
        invalidCustomer:false,
        error_message:''
      });

      const res = await Store.Service.do_get_tunggakan(id_pelanggan_clean, gRecaptcha);
      this.setState({submitLoadingCls:'',isLoading:false});
      this.updateRecaptchaContainer();
      this.loadRecaptcha();
      if( typeof res.success!== 'undefined'){
          if(res.success == false){
            this.setState({error_message:res.msg});
            return;
          }
          
      }
      if(res == ''){
     
        this.setState({ error_message:'ID Pelanggan tidak valid.'});
        return;
      }
      if(res.status != 500){
        if(res.data == null){
     
          res.data = [];
 
        }
         if(res.desc == "General Error Service"){
          this.setState({error_message:res.desc,result:null})
        }
        else if(res.desc == "Customer doesn't Exists"){
          this.setState({result:null,error_message:res.desc});
          return;
        }else{
          
            this.setState({result:res,resultPelangganData:res.data});
            setTimeout(()=>{
              this.updatePelangganInfo();
              this.updateDataInfo();
            },100);
          
          
        }
        
      }
      this.setState({isLoading:false,submitLoadingCls:''})
    }
    event.preventDefault();
    return false;
  }
  onFormSubmit = ()=>{
    event.preventDefault();
    return false;
  }
  onInputChange = (evt)=>{
    const el    = evt.target;
    const name  = el.name;
    let value = el.value;
    let state   = {};
    state[name] = value;
    this.setState(state);
  }
  getTunggakanBulanan(row, total){
    const bulan = row.noThnbln.toString().substr(-2);
    const tahun = row.noThnbln.toString().substr(0,4);
    const nmBulan = this.state.bulan_list_x[bulan];
    const m3 = row.m3Akhir - row.m3Awal;
    if(row.jmlCicilan==null)
      row.jmlCicilan=0;
    total.jmlTagihan += row.jmlTagihan;
    total.jmlCicilan += row.jmlCicilan;
    total.jmlDenda   += row.jmlDenda;
    total.jmlAdmin   += row.jmlAdmin;
    total.m3 += m3;

    let _row =  {};

      _row[`${nmBulan} ${tahun}`] = this.convertToRupiah(row.jmlTagihan);
      _row['Pemakaian'] = `${format_number(m3)} m3`;
      _row['Stand Meter'] = `${format_number(row.m3Awal)} - ${format_number(row.m3Akhir)} m3`;
      _row['Tanggal SP'] = row.spTgl;
      // _row['Status'] = 'Belum dibayar';
      if(typeof row.spTgl == 'string'){
        if(row.spTgl != ''){
          // _row['Status'] = 'Sudah dibayar';
           _row['Tanggal SP'] = tgl_indo(row.spTgl);
        }

      }

    return _row;
  }
  updateDataInfo(){
    const {keyData,resultPelangganData,bulan,result} = this.state;
    let finalDataAll = [];
    let total = {jmlDenda:0, jmlCicilan:0, jmlTagihan:0,jmlAdmin:0, m3:0};
    
    for(let p = 0 ; p < resultPelangganData.length;p++){
      const row = resultPelangganData[p];
    
        finalDataAll.push(this.getTunggakanBulanan(row, total));
      
     
    }
    
    const infoTunggakan = {
      'Pemakaian' : `${format_number(total.m3)} m3`,
      'Cicilan' : this.convertToRupiah(total.jmlCicilan),
      'Total Tunggakan' : this.convertToRupiah(total.jmlTagihan),
    };

    const detailTunggakan = {
      'Tag. Lalu/Denda' : this.convertToRupiah(total.jmlDenda),
      'Kode Tarif' : result.pelanggan.golTarif,
      'Cicilan' : this.convertToRupiah(total.jmlCicilan),
      'Biaya Admin' : this.convertToRupiah(total.jmlAdmin),
      'Jumlah Tunggakan' : this.convertToRupiah(total.jmlTagihan),

    }
    
    this.setState({infoTunggakan:infoTunggakan,detailTunggakan:detailTunggakan,finalDataAll:finalDataAll});
  }
  updatePelangganInfo(){
    const {result} = this.state;
    let resultPelangganInfo = [
      {key:'Nama',val:result.pelanggan.pemilik},
      {key:'No. Pelanggan',val:result.pelanggan.kdPelanggan},
      {key:'Alamat',val:result.pelanggan.almtPasang},
    ];
 

    this.setState({resultPelangganInfo:resultPelangganInfo});
  }
  convertToRupiah(angka)
  {
    if(typeof angka=='undefined')
      angka=0;
    var rupiah = '';    
    var angkarev = angka.toString().split('').reverse().join('');
    for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
    return 'Rp. '+rupiah.split('',rupiah.length-1).reverse().join('');
  }
  /**
   * Usage example:
   * alert(convertToRupiah(10000000)); -> "Rp. 10.000.000"
   */
   
  convertToAngka(rupiah)
  {
    return parseInt(rupiah.replace(/,.*|[^0-9]/g, ''), 10);
  }
  detailTunggakanTag(obj,append,index){
    let tags = [];
    if(typeof append !== 'undefined'){
      if(append){
        tags.push({k:`Tunggakan ${index+1}`})
      }
    }

    for(let p in obj){
      if(typeof p !== 'undefined')
        tags.push({k:p,v:obj[p]});
    }
    return tags;
  }
  render(){
   const {error_message,infoTunggakan,detailTunggakan,finalDataAll,id_pelanggan,submitLoadingCls,result,resultPelangganData,resultPelangganInfo,invalidCustomer} = this.state;
    const hasError = error_message.length > 0;
    return(
      <section className="form_tagihan container">
        <article className="row">
          <section className="col-md-12 wrp">
          <header>
              <h2 className="form-title">Informasi Tunggakan Pelanggan</h2>
 
          </header>

          <form className="form-horizontal main-form" method="post" onSubmit={this.onSubmitForm}>
            <div className="form-group">
              <label htmlFor="id_pelanggan" className="control-label">ID Pelanggan</label>
              <div className="input-container">
              <input className="form-control" value={this.state.id_pelanggan} onChange={()=>{this.onInputChange(event)}} name="id_pelanggan" id="id_pelanggan" placeholder="Masukkan ID Pelanggan" type="text" required="required"/>
              </div>
            </div> 
            <div className="form-group">
              <div className="input-container">
                <div className="form-error-message" style={{display:hasError?'block':'none'}}>
                <div dangerouslySetInnerHTML={{__html: error_message}} />
                </div>
              </div>
            </div>
             
            <div className="form-group">
              
              <div className="row" style={{padding:'1em 0'}}>
                <div className="col-md-6" id="recaptcha_parent_container">
                </div>
                <div className="col-md-6">
                  <div className="link-container" style={{textAlign:'right',marginTop:'1em'}}>
                    <a href="javascript:;" className="link_goto_page" onClick={this.onSubmitForm}><i className={submitLoadingCls}></i> Cek Tunggakan Saya</a>
                  </div>
                  </div>
              </div>
            </div>
          
            </form>
          </section>
          {result && (
            <section className="form_tagihan container" style={{display:(hasError||invalidCustomer)?'none':'block'}}>
            <article className="row">
                  <section className="col-md-12 wrp">
                    <header>
                      <h2 className="form-title">Data</h2>
                    </header>
                    <div>
                      <div>
                         <table className="table table-hover table-web-api-result">
                          <tbody>
                            <tr>
                              <th>Nama PAM</th><th>PERUMDAM Tirta Kerta Raharja</th>
                            </tr>
                            {resultPelangganInfo.map((row,index)=>{
                              return (
                                <tr key={`pl-${index}`}>
                                  <th>{row.key}</th><td className={row.key}>{row.val}</td>
                                </tr>
                              )
                            })}
                            {
                              this.detailTunggakanTag(infoTunggakan).map((row,index)=>{
                                return (
                                  <tr key={`pl1-${index}`} className={slugify(row.k)}>
                                    <th>{row.k}</th><td>{row.v}</td>
                                  </tr>
                                )
                              })
                            }
                            {
                              finalDataAll.reverse().map((rowx,index)=>{
                               
                                ////
                                return this.detailTunggakanTag(rowx,true,index).map((rowy,index)=>{
                                  return (
                                    <tr key={`pl3-${index}`} className={slugify(rowy.k)}>
                                      <th>{rowy.k}</th><td>{rowy.v}</td>
                                    </tr>
                                  )
                                })
                                ////  
                              })
                              
                            }
                            <tr>
                              <th colSpan={2}>Detail Tunggakan</th>
                            </tr>
                            {
                              this.detailTunggakanTag(detailTunggakan).map((row,index)=>{
                                return (
                                  <tr key={`pl3-${index}`} className={slugify(row.k)}>
                                    <th>{row.k}</th><td>{row.v}</td>
                                  </tr>
                                )
                              })
                            }
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </section>
                </article>
           
                
              </section>
          )}
        {invalidCustomer && (
          <div style={{padding:'1em',color:'red'}}>ID Pelanggan tidak terdaftar.</div>
        )}  
        </article>
      </section>
    )
  }
}