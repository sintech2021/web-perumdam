class PerumdamtkrWebKonsultasi extends React.Component{
	
	render(){
		const {is_ajax} = this.props;
		let content = (<div></div>);
		if(is_ajax){
			content = (
				<div id="konsultasi">
					<PageBanner/>
					<Konsultasi/>
				</div>
			)
		}else{
			content = (
				<div id="konsultasi">
					<Header/>
					<main role="main">
					<PageBanner/>
					<Konsultasi/>
					</main>
					<MyCompany/>
					<Footer/>
					
				</div>
			)
		}
		return content;
	}
}