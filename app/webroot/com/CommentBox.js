class CommentBox extends React.Component{
	state={
      comments:'',
      isLoading:true,
       gCaptchaSiteKey:'6LecSeYUAAAAADazed6MSoJXAW53CyaHJXgZ8i79',
       gRecaptchaC:'',
	   gRecaptchaContainerC:'recaptcha_container_c',
	   gRecaptchaPanthomC:0,

		gRecaptcha:'',
		gRecaptchaContainer:'recaptcha_container',
		gRecaptchaPanthom:0,
		gRecaptchaL:'',
		gRecaptchaContainerL:'recaptcha_container_l',
		gRecaptchaPanthomL:0,
		fields:{
			recaptcha : 'Verifikasi',
			reply_name : 'Nama',
			reply_email:'Email',
			reply_telp:'Nomor Telepon',
			reply_message:'Pesan Balasan',
			cust_name : 'Nama',
			cust_email:'Email',
			cust_telp:'Nomor Telepon',
			cusr_message:'Komentar',
		},
		recaptchaReplies:{}
    }
  	loadRecaptchaComment(){
	    var el_str  = `<div className="g-recaptcha" id="${this.state.gRecaptchaContainerC}"></div>`;
	    var parent = $('#grecaptcha_parent_container_c_cnt');
	    parent.html(el_str);

	    grecaptcha.render(this.state.gRecaptchaContainerC, {
	        sitekey : this.state.gCaptchaSiteKey,
	        callback : (response) =>{
	          this.setState({gRecaptchaC:response})
	        }
	    }); 
	  }
	updateRecaptchaContainerComment(){
	    let gRecaptchaPanthom = this.state.gRecaptchaPanthomC + 1;
	    let gRecaptchaContainer = `recaptcha_container_c${gRecaptchaPanthom}`
	    this.setState({
	      gRecaptchaContainerC:gRecaptchaContainer,
	      gRecaptchaPanthomC: gRecaptchaPanthom
	    });
    }  
   loadRecaptchaReply(container){
   	let recaptchaReplies = Object.assign({},this.state.recaptchaReplies); 
   	var key = MD5(container);
   	let currentState = recaptchaReplies[key];
   	if(typeof currentState === 'undefined'){
   		currentState = {
   			gRecaptchaContainer: `gRecaptchaContainer-${key}`,
   			gRecaptchaPanthom:0,
   			gRecaptcha:''
   		};
   	}
   	
    var el_str  = `<div className="g-recaptcha" id="${currentState.gRecaptchaContainer}"></div>`;
    var parent = $(`#recaptcha_parent_container_l-${key}`);
    parent.html(el_str);
    grecaptcha.render(currentState.gRecaptchaContainer, {
        sitekey : this.state.gCaptchaSiteKey,
        callback : (response) =>{
          currentState.gRecaptcha = response;
          recaptchaReplies[key] = currentState;
    	  this.setState({recaptchaReplies:recaptchaReplies});
        }
    }); 
  }
  updateRecaptchaContainerReply(container){
    let recaptchaReplies = Object.assign({},this.state.recaptchaReplies); 
   	var key = MD5(container);
   	let currentState = recaptchaReplies[key];
   	if(typeof currentState === 'undefined'){
   		currentState = {
   			gRecaptchaContainer: `gRecaptchaContainer-${key}`,
   			gRecaptchaPanthom:0,
   			gRecaptcha:''
   		};
   	}
    let gRecaptchaPanthom = currentState.gRecaptchaPanthom + 1;
    let gRecaptchaContainer = `gRecaptchaContainer-${key}-${gRecaptchaPanthom}`;

    currentState.gRecaptchaPanthom = gRecaptchaPanthom;
    currentState.gRecaptchaContainer = gRecaptchaContainer;
    recaptchaReplies[key] = currentState;
    this.setState({recaptchaReplies:recaptchaReplies});
  }
    async componentWillMount(){
    setTimeout(()=>{
      	this.updateRecaptchaContainerComment();
      	this.loadRecaptchaComment();
      },1000);
      this._get_comments();
      
    }
    async _get_comments(){
    	const content_id = this.props.id;
      const res = await Store.Data.get_comments(content_id);
      this.setState({comments:res});

      setTimeout(()=>{
      	this.setState({isLoading:false});
      	this.attachCommentFormHandler();
      },100)
    }
	onReply=(el,index)=>{
		$(`#reply-form-cont-${index}`).toggle();
      	this.attachReplyFormHandler(`#reply-form-cont-${index}`);

	}
	sendReply=(el,index,formSelector)=>{
		const form = $(formSelector);

	}
	attachCommentFormHandler(){
		const $form 		= $('.comment-form:first');
		const $cust_name 	= $form.find('#cust_name');
		const $cust_email 	= $form.find('#cust_id');
		const $cust_telp 	= $form.find('#cust_telp');
		const $cust_message = $form.find('#cust_message');
		const $submit_btn 	= $form.find('.link_goto_page');

		$submit_btn.click(()=>{
			const cust_name  = $cust_name.val();
			const cust_email = $cust_email.val();
			const cust_telp  = $cust_telp.val();
			const cust_message = $cust_message.val();
			const captcha = this.state.gRecaptchaC;
      		const content_id = this.props.id;

			Store.Comment.send(cust_name, cust_email, cust_telp, cust_message, content_id,captcha).then((res)=>{
				this.clearForm($form);
				this.updateRecaptchaContainerComment();
      			this.loadRecaptchaComment();
				if(res==null){
					this._get_comments();
					return;
				}
				if(res.success){
					this._get_comments();
				}else{
					if(typeof res.message != 'undefined'){
						alert(res.message);
					}else if( typeof res.error == 'object'){
						let msg = "";
						for(let i in res.error){
							let j = res.error[i];
							let label = i;
							try{
								label = this.state.fields[i];
							}catch(e){}
							msg += label + ' '+ j.message + "\n";
						}
						alert(msg);

					}
				}
				

			}).catch((e)=>{

			})
			
		});


	}
	attachReplyFormHandler(selector){
		const _self = this;
		if($(selector).attr('hasHandler') == 'yes'){
			return;
		}else{
			$(selector).attr('hasHandler','yes');
		}
		this.updateRecaptchaContainerReply(selector);
      	this.loadRecaptchaReply(selector);

		const $form = $(selector);
		const $reply_name 	= $form.find('input[name=reply_name]');
		const $reply_email 	= $form.find('input[name=reply_email]');
		const $reply_telp 	= $form.find('input[name=reply_telp]');
		const $reply_message = $form.find('textarea[name=reply_message]');
		const $comment_page = $form.find('input[name=comment_page]');
		const $reply_cid = $form.find('input[name=reply_cid]');
		const $submit_btn 	= $form.find('.link_goto_page');

		$submit_btn.click(()=>{
			const key = MD5(selector);
			const reply_name  = $reply_name.val();
			const reply_email = $reply_email.val();
			const reply_telp  = $reply_telp.val();
			const reply_message = $reply_message.val();
			const captcha = _self.state.recaptchaReplies[key].gRecaptcha;
      		const reply_cid = $reply_cid.val();
      		const content_id = _self.props.id;
      		const comment_page = $comment_page.val();
			Store.Comment.reply(reply_name, reply_email, reply_telp, reply_message, reply_cid,content_id,comment_page,captcha).then((res)=>{
				this.updateRecaptchaContainerReply(selector);
      			this.loadRecaptchaReply(selector);
				this.clearReplyForm($form);
				if(res==null){
					this._get_comments();
					return;
				}
				if(res.success){
					this._get_comments();
				}else{
					if(typeof res.message != 'undefined'){
						alert(res.message);
					}else if( typeof res.error == 'object'){
						let msg = "";
						for(let i in res.error){
							let j = res.error[i];
							let label = i;
							try{
								label = this.state.fields[i];
							}catch(e){}
							msg += label + ' '+ j.message + "\n";
						}
						alert(msg);
					}
				}

			}).catch((e)=>{

			})
			
		});
	}
	clearForm($form){
		$form.find('#cust_name').val('');
		$form.find('#cust_id').val('');
		$form.find('#cust_telp').val('');
		$form.find('#cust_message').val('');
	}
	clearReplyForm($form){
		$form.find('input[name=reply_name]').val('');
		$form.find('input[name=reply_email]').val('');
		$form.find('input[name=reply_telp]').val('');
		$form.find('textarea[name=reply_message]').val('');
	}
	render(){
		let {styleCls,comments,isLoading} = this.state;
 
		return (
			<div className={`container article-comments ${styleCls}`} style={{marginBottom:'2em',padding:0}}>
			{isLoading && (
				<div className="row">
					<div className="col-md-12">Loading</div>
				</div>
			)}
						
					<div  dangerouslySetInnerHTML={{__html:comments}}></div>
			</div>
		)
	}
}