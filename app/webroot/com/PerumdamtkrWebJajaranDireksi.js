
class JajaranDireksi extends React.Component{
	render(){
		let rows = app_data.top_mainlist.list; 

		rows[0].content = 'H.Sofyan Sapar';
		rows[0].title = 'Direktur Utama';

		rows[1].content = 'Jabatan Kosong';
		rows[1].title = 'Direktur Umum';

		rows[2].content = 'H. Yadi Treviadi';
		rows[2].title = 'Direktur Teknik';
		return(
			<div className="jajaran_direksi" style={{marginTop:'1em'}}>
				<div className="container">
					<div className="row" style={{justifyContent:'center'}}>
						<div className="col-md-3 cnt">
							<div className="photo-frame" style={{background:`#C4C4C4 url(${rows[0].bgurl}) no-repeat top left`,backgroundSize:'cover'}}>
							
							</div>
							<div className="info">
								<a href={rows[0].url}><h4 className="nama">{rows[0].content}</h4></a>
								<p className="jabatan">{rows[0].title}</p>
							</div>
						</div>
						<div className="col-md-3 cnt ctr">
							<div className="photo-frame" style={{background:`#C4C4C4 url(${rows[1].bgurl}) no-repeat top left`,backgroundSize:'cover'}}>
							
							</div>
							<div className="info">
								<a href={rows[1].url}><h4 className="nama">{rows[1].content}</h4></a>
								<p className="jabatan">{rows[1].title}</p>
								<a className="link" href={rows[1].url}>Baca selengkapnya <i className="fa fa-long-arrow-alt-right"></i></a>
							</div>
						</div>
						<div className="col-md-3 cnt">
							<div className="photo-frame" style={{background:`#C4C4C4 url(${rows[2].bgurl}) no-repeat top left`,backgroundSize:'cover'}}>
							
							</div>
							<div className="info">
								<a href={rows[2].url}> <h4 className="nama">{rows[2].content}</h4></a>
								<p className="jabatan">{rows[2].title}</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}
class PerumdamtkrWebJajaranDireksi extends React.Component{
	render(){
		const {is_ajax} = this.props;
		let exclude_id = '';
		try{exclude_id=app_data.article.id;}catch(e){};

		let content = (<div></div>);

		if(is_ajax){
			content = (
				<div id="tentang_kami">
					<PageBanner/>
					<h1 className="big-title text-center" style={{paddingTop:'1em'}}>Jajaran Direksi</h1>
					<div className="underline" style={{marginBottom:'3em'}}></div>
					<JajaranDireksi/>
					<div className="container" style={{backgroundColor:"#F9F9F9",paddingTop:'3em',maxWidth:'100%',margin:'0'}}>
					<h1 className="big-title text-center">Konten Terkait</h1>
					<div className="underline" style={{marginBottom:'1.5em'}}></div>
					<RelatedContent containerBg="#F9F9F9" category="tentang_kami" exclude_id={exclude_id} use_ci={true}/>
					</div>
				</div>
			)
		}else{
			content = (
				<div id="app">
					<Header/>
					<main role="main">
					<PageBanner/>
					<h1 className="big-title text-center" style={{paddingTop:'1em'}}>Jajaran Direksi</h1>
					<div className="underline" style={{marginBottom:'.5em'}}></div>
					<JajaranDireksi/>
					<div className="container" style={{backgroundColor:"#F9F9F9",paddingTop:'3em',maxWidth:'100%',margin:'0'}}>
					<h1 className="big-title text-center">Konten Terkait</h1>
					<div className="underline" style={{marginBottom:'1.5em'}}></div>
					<RelatedContent containerBg="#F9F9F9" category="tentang_kami" exclude_id={exclude_id} use_ci={true}/>
					</div>
					</main>
					<MyCompany/>
					<Footer/>
				</div>
			);
		}
		return content;
		 
	}
}