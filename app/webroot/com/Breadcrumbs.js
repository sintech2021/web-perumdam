class Breadcrumbs extends React.Component{
	state ={
		styleCls:'default',
		data : typeof app_data.article_breadcrumbs != 'undefined' ?  app_data.article_breadcrumbs : []
	}
	onNavigateMenu = (e)=>{
		const el = e.target;
		if($(el).hasClass('induk')){
		}else{
			bindHistoryPushState(el);
		}
		e.preventDefault();
		return false;
	}
	render(){
		let {styleCls,data} = this.state;
		if(data == null)
			data=[]
		return (
			<div className={`row article-breadcrumbs ${styleCls}`}>
			<nav aria-label="breadcrumb">
 
			  <ol className="breadcrumb">
			 	{
			 		data.map((r,i)=>{
			 			return i==data.length-1?(
							<li className="breadcrumb-item" key={`br-${i}`}><span className={`cls-${i}`}>{r.title}</span></li>
			 			):(
							<li className="breadcrumb-item" key={`br-${i}`}><a onClick={this.onNavigateMenu} href={r.url}><span className={`cls-${i}`}>{r.title}</span></a></li>

			 			)
			 		})
			 	}
 
			 
			  </ol>
			 
			</nav>
			</div>
		)
	}
}