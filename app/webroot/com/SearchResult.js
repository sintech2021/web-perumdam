class SearchResult extends React.Component{

	state ={
		off: true,
		searchq: '',
		data_list :[],
		pagination_counter: '',
		pagination_link: '',
		regex:null,
		search_no_items_message:''

	}

	getData = ()=>{
	
		if(typeof app_data.search_no_items_message != 'undefined'){
			this.setState({search_no_items_message:app_data.search_no_items_message});
		}
		if(typeof app_data.data_list != 'undefined'){
			this.setState({data_list:app_data.data_list});
		}
		if(typeof app_data.pagination_counter != 'undefined'){
			this.setState({pagination_counter:app_data.pagination_counter});
		}
		if(typeof app_data.pagination_link != 'undefined'){
			this.setState({pagination_link:app_data.pagination_link});
		}
		if(typeof app_data.searchq != 'undefined'){
			const regexp = new RegExp('\('+app_data.searchq+')','ig');
			this.setState({searchq:app_data.searchq,regex:regexp});
		}
	}
	componentWillMount(){
		this.getData()
	}
	generateLink = (row) =>{
		const parent = row.ParentContent.name;
		const parent_id = row.ParentContent.parent_id;
		if(parent == '/web/layanan_informasi/kantor_layanan'){
			const id = row.ScmContentContent.id;
			return site_url() + `${parent}?active=${id}`;
		}
		else if(parent == '/topik/berita'){
			const id = row.ScmContentContent.scm_content_id;
			const slug = slugify(row.FrontContent.text,6);
			return site_url() + `/read/${id}/berita/${slug}`;
		}
		else if(parent_id == null){
			const name = row.FrontContent.name;
			return site_url() + `${name}`;

			// const id = row.ScmContentContent.scm_content_id;
			// const slug = slugify(row.FrontContent.text,6);
			// return site_url() + `/read/${id}/pengumuman/${slug}`;

		}
	}
	generateSnippet = (row) =>{
		const {regex} = this.state;
		const content = row.ScmContentContent.value;
		const snippet = strip_tags(content);
		const snippets = snippet.split('.');
		let real_snippet = '';

		for(let i = 0; i < snippets.length;i++){
			const paragraph = snippets[i];
			if(paragraph.match(regex)){
				real_snippet += paragraph.replace(regex,'<i>$1</i>');
			}else{
				real_snippet += '...';
			}
		}
		real_snippet = real_snippet.replace(/\.+/g,'...');
		if(real_snippet=='...'){
			real_snippet = getFirstImg(content);
		}
		if(real_snippet == ''){
			real_snippet = snippet.substr(0,250) + '...';
		}
		return real_snippet;
	}
	generateTitle = (row) =>{
		const {regex} = this.state;
		return row.FrontContent.text.replace(regex,'<i>$1</i>');
	}
	onNavigateItem = (row,evt) => {
		const parent = row.ParentContent.name;

		let el = evt.target;
		if(el.nodeName == "I"){
			el = $(el).closest('a')[0];
		}
		if($(el).hasClass('induk')){
		}else{
			bindHistoryPushState(el);
		}
		
	}
	render(){
		const {data_list,searchq,pagination_link,pagination_counter,search_no_items_message }= this.state;
		const empty_result = data_list.length == 0;
		return (
			<div className="search_result container">
			{
				!empty_result && (
					<div className="row">
						<div className="col-lg-12">
							<div dangerouslySetInnerHTML={{__html:pagination_counter}}></div>
						</div>
					</div>
				)
			}
			
			<div className="row">
				<div className="col-lg-12">
				{
					data_list.map((row,index)=>{
						return(
							<div className="items" key={`search-item-${index}`}>
								<h4 className="title">
								<a href={this.generateLink(row)} onClick={(e)=>{ this.onNavigateItem(row,e);e.preventDefault();return false;}} dangerouslySetInnerHTML={{__html:this.generateTitle(row)}} >
									 
								</a>

								</h4>
								<div className="snippet" dangerouslySetInnerHTML={{__html:this.generateSnippet(row)}}></div>
							</div>
						)
					})
				}
				</div>
			</div>
			{
				!empty_result && (
					<div className="row">
						<div className="col-lg-12">
							<div dangerouslySetInnerHTML={{__html:pagination_counter}}></div>
						</div>
					</div>
				)
			}
			{
				!empty_result && (
					<div className="row">
						<div className="col-lg-12">
							<div dangerouslySetInnerHTML={{__html:pagination_link}}></div>
						</div>
					</div>
				)
			}
			{
				empty_result && (
					<div className="row">
						<div className="col-lg-12">
							<div dangerouslySetInnerHTML={{__html:search_no_items_message}}></div>
						</div>
					</div>
				)
			}
			</div>
		);
	}
}