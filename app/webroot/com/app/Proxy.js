animation_started = false;
config = {
  onDownloadProgress: progressEvent => {
    let percentCompleted = Math.floor((progressEvent.loaded * 100) / progressEvent.total);
    // do whatever you like with the percentage complete
    // maybe dispatch an action that will update a progress bar or something
    if(percentCompleted == Infinity){
        percentCompleted = 110;
    }
    if(animation_started)
        return;
    animation_started = true;
    $({property: 0}).animate({property: 110}, {
        duration: 3000,
        step: function() {
            // $('#root').css('opacity','.5');
            var _percent = Math.round(percentCompleted);
            $('#progress').css('opacity',  "1");
            $('#progress').css('width',  _percent+"%");
            if(_percent == 110) {
                $("#progress").addClass("done");
                $('#progress').css('opacity',  "0");
                $('#root').css('opacity','1');
                animation_started = false;
            }
        },
        complete: function() {
            $('#progress').css('opacity',  "0");
            
            $('#progress').css('width',  "0%");
            // $('#root').css('opacity','1');
            animation_started = false;
            
        }
    });
  },
  responseType:'stream'
}
Proxy={
    get: (url,cbSuccess,cbError) => {
        return axios({
            method:'get',
            url: url,
            onDownloadProgress:config.onDownloadProgress,
            headers:{
                "X-Requested-With" : "XMLHttpRequest"
            }

        })
        .then((response) => {
            if(typeof cbSuccess == 'function')
                cbSuccess(response.data);
            else
                return response.data;
        })
        .catch((error) => {
            if(typeof cbError == 'function')
                cbError(error);
            // alert(error);
        });
    },
	post : (url,postData,cbSuccess,cbError,optArgs) => {
		var formData = new FormData();

		for(let key in postData){
            formData.append(key, postData[key]);
		}
		 
        try{
            if(typeof optArgs === 'object'){
                for(n in optArgs){
                    let f = optArgs[n];
                    formData.append(n,f);
                }
            }
        }catch(e){
        }
		return axios({
            method:'post',
            url: url,
            data:formData,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data', 
                "X-Requested-With" : "XMLHttpRequest"
            },
            onDownloadProgress:config.onDownloadProgress

        })
        .then((response) => {
            if(typeof cbSuccess == 'function')
                cbSuccess(response.data);
            return response.data;
        })
        .catch((error) => {
            if(typeof cbError == 'function')
                cbError(error);
            // alert(error)
        });

		
	},
	post_legacy : (url,postData,cbSuccess,cbError) => {
		var formData = new FormData();

		for(let key in postData){
            formData.append(key, postData[key]);
		}
		if(Config.debug){
		}
		return fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
                'X-API-KEY' : Config.api_key, 
                'X-APP-ID' : Config.api_appid,
                "X-Requested-With" : "XMLHttpRequest"

            },
            body: formData
        }).then((response) =>{ 
            response.json().then((res) => {
                cbSuccess(res);
            })
        })
        .catch((error) => {
            cbError(error);
        })
        .done();
	},

};