Store = {
	Service : {
		get_kabupaten_list : async () => {
			const url = site_url() + `/pendaftaran/get_kab`;
			return Proxy.get(url);
		},
		get_kecamatan_list : async (id_kab) => {
			const url = site_url() + `/pendaftaran/get_kec/${id_kab}`;
			return Proxy.get(url);
		},
		get_kelurahan_list : async (id_kec,id_kab) => {
			const url = site_url() + `/pendaftaran/get_kel/${id_kec}/${id_kab}`;
			return Proxy.get(url);
		},
		do_register  : async (nama_pemohon,nama_pemilik,nomor_ktp,nomor_hp,nomor_telepon,email,id_kab,id_kec,id_kel,alamat,kode_pos,foto,foto_filename,kabupaten,kecamatan,kelurahan,gRecaptcha) => {
			const url  = site_url() + `/pendaftaran/do_register`;
			const data = {
				nama_pemohon : nama_pemohon,
				nama_pemilik : nama_pemilik,
				nomor_hp : nomor_hp,
				nomor_ktp : nomor_ktp,
				nomor_telepon : nomor_telepon,
				email : email,
				id_kab : id_kab,
				id_kec : id_kec,
				id_kel : id_kel,
				alamat : alamat,
				kode_pos : kode_pos,
				foto : foto,
				foto_filename : foto_filename,
				kabupaten:kabupaten,
				kecamatan:kecamatan,
				kelurahan:kelurahan,
				gRecaptcha:gRecaptcha
			};
			return Proxy.post(url,data);
		},
		do_register_check: async (kode_pendaftaran,gRecaptcha)=>{
			const url  = site_url() + `/layanan/do_register_check`;
			const data = {
				kode_pendaftaran : kode_pendaftaran,
				gRecaptcha:gRecaptcha
			};
			return Proxy.post(url,data);
		},
		do_get_tagihan: async (id_pelanggan,bulan,tahun,gRecaptcha)=>{
			const url  = site_url() + `/layanan/do_get_tagihan`;
			const data = {
				id_pelanggan : id_pelanggan,
				bulan : bulan,
				tahun : tahun,
				gRecaptcha:gRecaptcha
			};
			return Proxy.post(url,data);
		},
		do_get_tunggakan: async (id_pelanggan,gRecaptcha)=>{
			const url  = site_url() + `/layanan/do_get_tunggakan`;
			const data = {
				id_pelanggan : id_pelanggan,
				gRecaptcha:gRecaptcha
			};
			return Proxy.post(url,data);
		},
		do_get_riwayat_pengaduan: async (riwayat_id_pelanggan,gRecaptcha)=>{
			const url  = site_url() + `/layanan/do_get_riwayat_pengaduan`;
			const data = {
				id_pelanggan : riwayat_id_pelanggan,
				gRecaptcha:gRecaptcha
			};
			return Proxy.post(url,data);
		},
		do_pengaduan:async(id_pelanggan,nama_lengkap,email,nomor_hp,nomor_telepon,pesan,gRecaptcha)=>{
			const url  = site_url() + `/layanan/do_pengaduan`;
			const data = {
				id_pelanggan : id_pelanggan ,
				nama_lengkap : nama_lengkap,
				email : email,
				nomor_hp : nomor_hp,
				nomor_telepon : nomor_telepon,
				pesan : pesan,
				gRecaptcha:gRecaptcha
			};
			return Proxy.post(url,data);
		},
		do_get_pengaduan_response: async(id_pengaduan)=>{
			const url  = site_url() + `/layanan/do_get_pengaduan_response`;
			const data = {
				id_pengaduan : id_pengaduan
			};
			return Proxy.post(url,data);
		}
	},
	Konten :{
		get_arsip_berita: async ()=>{
			const url  = site_url() + `/konten/get_arsip_berita`;
			return Proxy.get(url);
		},
		get_latest_gi_items: async ()=>{
			const url  = site_url() + `/konten/get_latest_gi_items`;
			return Proxy.get(url);
		},
		get_latest_gv_items: async ()=>{
			const url  = site_url() + `/konten/get_latest_gv_items`;
			return Proxy.get(url);
		},
	},
	Data :{
		get_menu : async ()=>{
			const url  = site_url() + `/data/menu`;
			return Proxy.get(url);	
		},
		get_berita : async (page)=>{
			const url  = site_url() + `/data/berita/page:${page}`;
			return Proxy.get(url);	
		},
		get_related_content: async (parent, exclude_id, use_ci)=>{
			if(typeof use_ci == 'undefined'){
				const url  = site_url() + `/data/related_content/${parent}/${exclude_id}`;
				return Proxy.get(url);	
			}else{
				const url  = site_url() + `/konten/related/${parent}/${exclude_id}`;
				return Proxy.get(url);
			}
			
		},
		get_caraousel :  async ()=>{
			const url  = site_url() + `/konten/get_caraousel`;
			return Proxy.get(url);	
		},
		get_comments :  async (content_id)=>{
			const url  = site_url() + `/data/comment?content_id=${content_id}`;
			return Proxy.get(url);	
		},
		get_running_text : async (content_id)=>{
			const url  = site_url() + `/konten/get_running_text`;
			return Proxy.get(url);	
		},
		get_info_perumdam : async (content_id)=>{
			const url  = site_url() + `/konten/get_info_perumdam`;
			return Proxy.get(url);	
		},
		get_galery : async (content_id)=>{
			const url  = site_url() + `/konten/get_galery`;
			return Proxy.get(url);	
		},
		get_galery_detail : async (content_id)=>{
			const url  = site_url() + `/konten/get_galery_detail/${content_id}`;
			return Proxy.get(url);	
		},
		get_galery_items : async (content_id)=>{
			const url  = site_url() + `/konten/get_galery_items/${content_id}`;
			return Proxy.get(url);	
		},
		get_galery_info : async (content_id)=>{
			const url  = site_url() + `/konten/get_galery_info/${content_id}`;
			return Proxy.get(url);	
		},
	},
	Comment:{
		send : async (cust_name, cust_email, cust_telp, cust_message, content_id,gRecaptcha)=>{
			const url = site_url() + `/data/send_comment`;
			const data = {
				cust_name : cust_name ,
				cust_email : cust_email,
				content_id:content_id,
				cust_telp : cust_telp,
				cust_message : cust_message,
				'g-recaptcha-response' : gRecaptcha 
			};
			return Proxy.post(url,data);	
		},
		reply : async (reply_name, reply_email, reply_telp, reply_message, reply_cid,content_id,comment_page,gRecaptcha)=>{
			const url = site_url() + `/data/reply_comment`;
			const data = {
				 reply_name : reply_name,
				 reply_email: reply_email,
				 reply_telp: reply_telp,
				 reply_message : reply_message,
				 reply_cid:reply_cid,
				 content_id:content_id,
				 comment_page:comment_page,
				'g-recaptcha-response' : gRecaptcha 
			};
			return Proxy.post(url,data);	
		}
	}

}