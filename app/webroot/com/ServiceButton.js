
class ServiceItem extends React.Component{
	onNavigateMenu = (e)=>{

		 const el = e.target;
	
			bindHistoryPushState(el);
		e.preventDefault();
		return false;
	}

	render(){
		const {cls,title,icon,url} = this.props;
		return (
			<div className="col-12 col-md service_item">
				<a onClick={this.onNavigateMenu} className="service_button" href={site_url()+`/${url}`}>
					<img className="image_icon" src={icon}/>
				</a>
				<a onClick={this.onNavigateMenu} href={site_url()+`/${url}`}><h4 className={"service_title "+cls}>{title}</h4></a>
			</div>
		)
	}
}

class ServiceButton extends React.Component{
	state={
		 
	}
	render(){
		return (
			<div id="service_list" className="container" style={{padding:'0 0 0 3em'}}>
				<div className="row">
					<ServiceItem  title="Pendaftaran Pelanggan" icon="icons/register.png" cls="register" url="pendaftaran"/>
					<ServiceItem  title="Cek Status Pendaftaran" icon="icons/check.png" cls="register_check" url="register_check"/>
					<ServiceItem  title="Info Tagihan" icon="icons/info.png" cls="tagihan" url="tagihan"/>
					<ServiceItem  title="Info Tunggakan" icon="icons/tunggakan.png" cls="tagihan" url="tunggakan"/>
					<ServiceItem  title="Pengaduan" icon="icons/pengaduan.png" cls="srv-pengaduan" url="web/pengaduan"/>
				</div>
			</div>
		)
	}
}