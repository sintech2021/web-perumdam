class FormPengaduan extends React.Component{
  state = {
    nama_lengkap : '',
    id_pelanggan : '',
    email : '',
    nomor_hp : '',
    nomor_telepon : '',
    pesan : '', 

    // nama_lengkap : 'Putra Budiman',
    // id_pelanggan : 'B332354',
    // email : 'cristminix@gmail.com',
    // nomor_hp : '0895378388684',
    // nomor_telepon : '0215636',
    // pesan : 'Woi mas PAM masi belum nyala juga??? petugas datang bilang pipa pam terlalu tinggi padahal selama ini air bisa jalan normal cuma beberapa bulan terakhir aja PAM bermasalah! Tetangga kiri kanan semua nyala dan tidak ada masalah, padahal pipa pam dipasang standar dari perumahan sama semua tinggi nya.', 

    riwayat_id_pelanggan: '',
    riwayat_nama : '',


    riwayat_list : [
      // {id:1,title:'Pengaduan Air Mati',dt:'2 Juli 2020, 10:55',kode_pengaduan:'TIX82012KK'},
    ],
    regexp_number : /^[0-9\b]+$/,
    r_pelanggan:null,
    rBtnCls:'',
    rIsLoading:false,
    rRequested:false,

    btnCls:'',
    isLoading:false,

    activeContent:'',
    activeDt:'',
    error_message:'',
    error_messageL:'',
    
    gCaptchaSiteKey:'6LecSeYUAAAAADazed6MSoJXAW53CyaHJXgZ8i79',
    gRecaptcha:'',
    gRecaptchaContainer:'recaptcha_container',
    gRecaptchaPanthom:0,
    gRecaptchaL:'',
    gRecaptchaContainerL:'recaptcha_container_l',
    gRecaptchaPanthomL:0,
    pgResponse:[]
  }
  componentWillMount(){
    setTimeout(()=>{
      this.updateRecaptchaContainer();
      this.loadRecaptcha();
      this.updateRecaptchaContainer(1);
      this.loadRecaptcha(1);
    },1000);
  }
  loadRecaptcha(is_l){
    const isel = typeof is_l != 'undefined';

   
    var container_id = 'gRecaptchaContainer';
    var key_id = 'gRecaptcha';
    if(isel){
      container_id +='L';
      key_id += 'L';
    }
    var el_str  = `<div className="g-recaptcha" id="${this.state[container_id]}"></div>`;
    var parent = $('#recaptcha_parent_container'+(isel?'_l':''));
    parent.html(el_str);
    grecaptcha.render(this.state[container_id], {
        sitekey : this.state.gCaptchaSiteKey,
        callback : (response) =>{
          let stateObj = {};
          stateObj[key_id] = response;
          this.setState(stateObj);
        }
    }); 
  }
  updateRecaptchaContainer(is_l){
    const isel = typeof is_l != 'undefined';

    let panthom_id = 'gRecaptchaPanthom';
    let container_id = 'gRecaptchaContainer';
    let container_name = 'recaptcha_container';
    if(isel){
      panthom_id +='L';
      container_id +='L';
      container_name += '_l';
    }
    let gRecaptchaPanthom = this.state[panthom_id] + 1;
    let gRecaptchaContainer = `${container_name}${gRecaptchaPanthom}`;
    let stateObj = {};
    stateObj[panthom_id] = gRecaptchaPanthom;
    stateObj[container_id] = gRecaptchaContainer;
    this.setState(stateObj);
  }
  onSubmitRiwayatForm = async ()=>{
    const {gRecaptchaL,riwayat_id_pelanggan,rIsLoading} = this.state;
    const id_pelanggan_clean = riwayat_id_pelanggan.replace(/\W+/,'');
    if(rIsLoading){
      return;
    }
    if(id_pelanggan_clean == ''){
      $('#riwayat_id_pelanggan').focus();

      return;
    }  
    this.setState({
        riwayat_list : [],
        riwayat_nama: '',
        rBtnCls:'fa fa-spinner fa-spin',
        rIsLoading:true,
        activeContent:'',
        rRequested:false,
        error_messageL:''
    });
    const res = await Store.Service.do_get_riwayat_pengaduan(riwayat_id_pelanggan,gRecaptchaL);
    this.setState({rBtnCls:'',rIsLoading:false,rRequested:true});
    
    if(res.success){
      try{
        const riwayat_list=[];
        res.data.feedback.forEach((row,index)=>{
          
          riwayat_list.push({
            id:row.id,
            title: row.title,
            kode_pengaduan: row.ticket,
            content:row.content,
            dt:row.dt
          })
        })
        this.setState({riwayat_list: riwayat_list,riwayat_nama:res.data.pelanggan.name});
      }catch(e){}
    }else{
      // alert(res.msg)
      this.setState({error_messageL:res.msg})
    }
    this.updateRecaptchaContainer(1);
    this.loadRecaptcha(1);
    event.preventDefault();
    return false;
  }
  onSubmitPengaduanForm = async ()=>{
    const {gRecaptcha,id_pelanggan,nama_lengkap,email,nomor_hp,nomor_telepon,pesan,isLoading,btnCls} = this.state;
    const id_pelanggan_clean = id_pelanggan.replace(/\W+/,'');
    if(isLoading){
      return;
    }
    // if(id_pelanggan_clean == ''){
    //   $('#id_pelanggan').focus();
      
    //   return;
    // } 
    this.setState({
        btnCls:'fa fa-spinner fa-spin',
        isLoading:true,
        error_message:''
    });
    const res = await Store.Service.do_pengaduan(id_pelanggan_clean,nama_lengkap,email,nomor_hp,nomor_telepon,pesan,gRecaptcha);
     this.setState({btnCls:'',isLoading:false});
    this.updateRecaptchaContainer();
    this.loadRecaptcha();
    if(res.success){
      alert(res.msg)
      this.resetForm()
    }else{
      if(res.msg=='Invalid Customer'){
        $('#id_pelanggan').focus();
      }
      this.setState({error_message:res.msg});

    }
    event.preventDefault();
    return false;
  }
  onFormSubmit = ()=>{
    event.preventDefault();
    return false;
  }
  onInputChange = (evt)=>{
    const el    = evt.target;
    const name  = el.name;
    let value = el.value;
    let state   = {};
    const number_only_fields = ['nomor_hp','nomor_telepon'];
    const is_number_only_field = number_only_fields.indexOf(name) != -1;

    if(is_number_only_field){
      if(value === '' || this.state.regexp_number.test(value)){
        state[name] = value;
        this.setState(state);
      }
    }else{
      state[name] = value;
      this.setState(state);
    }
  }
  resetForm =()=>{
    this.setState({
      id_pelanggan:'',
      nama_lengkap:'',
      nomor_hp: '',
      nomor_telepon:'',
      pesan:'',
      email:''
    });
  }
  displayRiwayat= async (row)=>{
    this.setState({activeContent:row.content,activeDt:row.dt});
    $('#pgModal').modal({
        show: true
    }); 
    this.setState({pgResponse:[]});
    // $('#pgModal .pengaduan_response').html('loading...');
    const res = await Store.Service.do_get_pengaduan_response(row.id);
    if(res.success){
      this.setState({pgResponse:res.data});

    }

  }
  render(){

    const {pgResponse,error_messageL,error_message,activeDt,rRequested,activeContent,riwayat_id_pelanggan,riwayat_list,riwayat_nama,kode_pengaduan,isLoading,rIsLoading,btnCls,rBtnCls} = this.state;
    const hasRiwayatList = riwayat_list.length > 0;
    const hasError = error_message.length > 0;
    const hasErrorL = error_messageL.length > 0;

    return(
      <div className="form_pengaduan container">
        <div id="pgModal" className="modal fade " role="dialog">
            <div className="modal-dialog modal-lg">

              <div className="modal-content">
                <div className="modal-header">
                 
                  <h4 className="modal-title">Detail Pengaduan</h4>
                   <button type="button" className="close" data-dismiss="modal">&times;</button>
                </div>
                <div className="modal-body">
                  <div className="pengaduan_content">
                    <p>{activeContent}</p>
                    <small className="dt_text">Di post pada <span style={{color:'#ccc'}}>{activeDt}</span></small>
                  </div>
                  <div className="pengaduan_response">
                  <h4 style={{paddingTop:'1em'}}>Respon</h4>
                  {
                     pgResponse.map((row,index)=>{
                      return (
                          <div style={{padding:'1em'}}>
                              <p>{row.value}</p>
                              <small className="dt_text">Di post pada <span style={{color:'#ccc'}}>{row.dt}</span></small>
                            
                          </div>
                        )
                     })
                  }
                  {
                    pgResponse.length == 0 &&(
                      <div style={{padding:'1em'}}>
                          <p>Belum ada respon</p>
                      </div>
                    )
                  }
                  </div>
                </div>
                <div className="modal-footer">
                  <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>

            </div>
          </div> 
        <div className="row wrp">
          <div className="col-md-4">
            <div className="riwayat_pengaduan cnt">
              <h2 className="form-title">Riwayat Pengaduan</h2>
              <form className="form-horizontal riwayat-pengaduan-form" style={{padding:'2em'}} onSubmit={this.onSubmitForm}>
                <div className="form-group">
                  <label htmlFor="riwayat_id_pelanggan" className="control-label">ID Pelanggan</label>
                  <div className="input-container">
                    <input className="form-control" value={this.state.riwayat_id_pelanggan} onChange={()=>{this.onInputChange(event)}}  name="riwayat_id_pelanggan" id="riwayat_id_pelanggan" placeholder="Masukkan ID Pelanggan" type="text" required="required"/>
                  </div>
                </div>
                <div className="form-group">
                    <div id="recaptcha_parent_container_l">
                    </div>
                </div>
                <div className="form-group">
                    <div className="link-container" style={{marginTop:'2em'}}>
                      <a href="javascript:;" className="link_goto_page" onClick={this.onSubmitRiwayatForm}><i className={rBtnCls}></i> Lihat riwayat</a>
                    </div>
                </div>
                {
                  !hasRiwayatList && rRequested && !hasErrorL &&(
                    <div className="riwayat_pengaduan_detail">
                      <div style={{padding:'0 1em'}}><label className="no-data" style={{color:'red'}}>Tidak data data</label></div>

                    </div>
                  )
                }
                {
                  hasErrorL  &&  (
                    <div style={{padding:'1em 0'}}>
                      <div className="form-error-message">{error_messageL}</div>

                    </div>
                  )
                }
                <div className="riwayat_pengaduan_detail" style={{display:hasRiwayatList?'block':'none'}}>
                  <h4 className="form-subtitle">Riwayat Pengaduan</h4>
                  <div style={{padding:0}}> <hr className="featurette-divider" style={{margin:'0'}}/></div>
                  <div className="info-pelanggan" style={{paddingTop:'1em'}}>
                    <div className="row">
                      <div className="col-sm-6">
                        <div className="form-grp"><label>Nama</label></div>
                        <div className="form-grp"><label>ID Pelanggan</label></div>
                      </div>
                      <div className="col-sm-6">
                        <div className="form-grp" style={{textAlign:'right'}}><label className="tbl-val">{riwayat_nama}</label></div>
                        <div className="form-grp" style={{textAlign:'right'}}><label className="tbl-val">{riwayat_id_pelanggan}</label></div>
                      </div>
                    </div>
                  </div>
                  <div className="list-pengaduan">
                    <table className="table">
                      <tbody>
                      {
                        riwayat_list.map((row,index)=>{
                          return (<tr>
                            <td key={`history-${index}`}>
                              <div className="pg-title-cnt">
                                <img src={site_url()+`/pub/img/dot-gray-12.png`} style={{marginRight:'12px'}}/>
                                <a href="javascript:;" className="p-title" onClick={(e)=>{this.displayRiwayat(row)}}><span className="title_pengaduan">{row.title}</span></a>
                                <div className="dt_text">{row.dt}</div>
                              </div>
                            </td>
                            <td style={{textAlign:'right'}}><span className="kode_pengaduan">{row.kode_pengaduan}</span></td>
                          </tr>)
                        })
                      }
                      
                      </tbody>
                    </table>
                  </div>
                </div>
              </form>

            </div>
          </div>
          <div className="col-md-8">
            <div className="pengaduan_form cnt">
              <h2 className="form-title">Buat Pengaduan</h2>
              <form className="form-horizontal pengaduan-form" onSubmit={this.onSubmitForm}>
                <div className="form-group">
                  <label htmlFor="nama_lengkap" className="control-label">Nama Lengkap</label>
                  <div className="input-container">
                  <input className="form-control" value={this.state.nama_lengkap} onChange={()=>{this.onInputChange(event)}}  name="nama_lengkap" id="nama_lengkap" placeholder="Masukkan Nama Lengkap" type="text" required="required"/>
                  </div>
                </div>
                <div className="form-group">
                  <label htmlFor="id_pelanggan" className="control-label">ID Pelanggan</label>
                  <div className="input-container">
                  <input className="form-control" value={this.state.id_pelanggan} onChange={()=>{this.onInputChange(event)}}  name="id_pelanggan" id="id_pelanggan" placeholder="Masukkan ID Pelanggan" type="text" required="required"/>
                  </div>
                </div> 
                <div className="form-group">
                  <label htmlFor="email" className="control-label">E-Mail</label>
                  <div className="input-container">
                  <input className="form-control" value={this.state.email} onChange={()=>{this.onInputChange(event)}}  name="email" id="email" placeholder="Masukkan Email" type="email" required="required"/>
                  </div>
                </div>
                <div className="form-group">
                  <label htmlFor="nomor_hp" className="control-label">Nomor HP</label>
                  <div className="input-container">
                    <input className="form-control" value={this.state.nomor_hp} onChange={()=>{this.onInputChange(event)}}   maxLength={14} name="nomor_hp" id="nomor_hp" placeholder="Masukkan Nomor HP" type="tel" required="required"/>
                  </div>
                </div>
                <div className="form-group">
                  <label htmlFor="nomor_telepon" className="control-label">Nomor Telepon</label>
                  <div className="input-container">
                    <input className="form-control" value={this.state.nomor_telepon} onChange={()=>{this.onInputChange(event)}}  maxLength={14} name="nomor_telepon" id="nomor_telepon" placeholder="Masukkan Nomor Telepon" type="tel" required="required"/>
                  </div>
                </div>
                <div className="form-group">
                  <label htmlFor="pesan" className="control-label">Pesan Pengaduan</label>
                  <div className="input-container">
                    <textarea className="form-control"  value={this.state.pesan} onChange={()=>{this.onInputChange(event)}} name="pesan" id="pesan" placeholder="Masukkan Pesan Pengaduan" type="text" required="required"></textarea>
                  </div>
                </div>
                <div className="form-group">
              <div className="input-container">
                <div className="form-error-message" style={{display:hasError?'block':'none'}}>
                <div dangerouslySetInnerHTML={{__html: error_message}} />
                </div>
              </div>
            </div>
                <div className="form-group">
                  <div className="row" style={{padding:'1em 0'}}>
                    <div className="col-md-6" id="recaptcha_parent_container">
                    
                    </div>
                    <div className="col-md-6">
                      <div className="link-container" style={{textAlign:'right',marginTop:'1em'}}>
                        <a href="javascript:;" className="link_goto_page" onClick={this.onSubmitPengaduanForm}><i className={btnCls}></i> Kirim Pengaduan</a>
                      </div>
                      </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}