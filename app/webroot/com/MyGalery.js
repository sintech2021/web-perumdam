class MyGalery extends React.Component{
	state ={
		image_list : [
		
		],
		result:[]
	}
	getData = async ()=>{
		const res = await Store.Konten.get_latest_gi_items();
		this.setState({result:res});
	}
	updateMasonry(){
		try{
			setTimeout(()=>{
				let container = $('.grid-item.image-div > a.group');
				if(typeof container.imagesLoaded=='function'){
					container.imagesLoaded(function () {
				        $('.grid').masonry({
						  // options...
						  itemSelector: '.grid-item',
						  // columnWidth: 200
						  isFitWidth: true
						});
				    });
				}
				
			},3000);
			
		}catch(e){
		}
		
	    return (<div></div>);
	}
	componentWillMount(){
		this.getData();

		setTimeout(()=>{
			
 			

			$("a.group").fancybox({
				'transitionIn'	:	'elastic',
				'transitionOut'	:	'elastic',
				'speedIn'		:	600, 
				'speedOut'		:	200, 
				'overlayShow'	:	true
			});
		},3000);

	
		
	}
	
	render(){
		const {result} = this.state;
		return (
			<div className="container my_galery">
				<div className="row">
					<div className="col-md-12">
				 		<div className="grid masonry-container js-masonry" style={{backgroundColor:'#F9F9F9'}}>
						 {
						 	result.map((row,index)=>{
						 		return (
						 			<div className="grid-item image-div" key={`my_galery-${index}`}>
							 			<a className="group" data-fancybox="gallery" data-caption={row.title} href={site_url()+`/uploads/galery_images/${row.path}`}>
							 				<img src={site_url()+`/konten/photo_thumb/galery_images/${btoa(row.path)}`} alt={row.title} style={{width:220}}/>
							 			</a>
						 			</div>
						 		)
						 	})
						 }
						 {
						 	this.updateMasonry()
						 }
						</div>
					</div>
				</div>
			</div>
		)
	}
}

 