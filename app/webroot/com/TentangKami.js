class TentangKami extends React.Component{
	state = {
		videoUrl : ''
	}
	getData = async ()=>{
		const res = await Store.Konten.get_latest_gv_items();
		this.setState({videoUrl:res[0].path});
	}
	componentWillMount(){
		this.getData();
		setTimeout(()=>{
			const player = new Plyr('#player');
		},2000);
	}
	onNavigateMenu = (e)=>{

		const el = $(e.target).parent()[0];

			bindHistoryPushState(el);
		e.preventDefault();
		return false;
	}
	render(){
		const {videoUrl} = this.state;
		return (
			 <div className="tentang-kami-cnt" style={{backgroundColor:'#F9F9F9'}}> 
			 <div className="container about" style={{backgroundColor:'#F9F9F9'}}> 
			    <div className="row featurette">
			      <div className="col-md-6 order-md-1">
			      	<div className="bg-video-about">
			          <div id="player" data-plyr-provider="youtube" data-plyr-embed-id={videoUrl}></div>
			      	</div>
			      </div><div className="col-md-6 order-md-2 ">
			        <h1 className="big-title text-left">Tentang Kami</h1>
					<div className="underline" style={{margin: 0}}></div>
					<div style={{paddingTop:'.5em'}}>
						<p style={{lineHeight:'26px'}}>Perusahaan Daerah Air Minum Tirta Kerta Raharja Kabupaten Tangerang ( PDAM TKR ) adalah salah satu perusahaan daerah yang dimiliki oleh Pemerintah Kabupaten Tangerang, yang bergerak dibidang Air Bersih. PDAM TKR didirikan oleh Pemerintah Kabupaten Tangerang berdasarkan Perda No. 10/HUK/1976 tanggal 13 April 1976 dengan nama Perusahaan Daerah Air Minum ( PDAM ) Kabupaten Tangerang. Berdasarkan Perda dan tanggal tersebut, maka tanggal 13 April dijadikan sebagai Hari Jadi PDAM Tirta Kerta Raharja. Yang ditetapkan berdasarkan Perda No. 10 Tahun 2008 Jo Perda No. 01 Tahun 2013. Adapun Perubahan nama PDAM Kabupaten Tangerang menjadi PDAM Tirta Kerta Raharja Kabupaten Tangerang adalah berdasarkan Perda No. 16 Tahun 2001</p>
					</div>
					<div className="link_separator">
						<a onClick={this.onNavigateMenu} href={site_url()+"/web/tentang_kami/profil_perusahaan"} className="link_goto_page">Lebih Lanjut</a>
					</div>
			      </div>
			      
			    </div>
			 </div>
			 </div>
		)
	}
}