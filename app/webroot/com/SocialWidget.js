class SocialWidget extends React.Component{
	
	render(){
		const ytUrl = 'https://www.youtube.com/channel/UCPzvUUyK7E64Lt13IbrKQ0w';
		const igUrl = 'https://www.instagram.com/perumdamtkr';
		const twitterUrl = 'https://twitter.com/perumdamtkr';
		const fbUrl = 'https://www.facebook.com/humasperumdamtkr';
		
		return(
			<div className="social_widget container">
				<div className="row">
					<div className="col-md-6">
						<div className="cups">
							<div className="underline" style={{margin:0}}></div>
							<h4 className="heading">IKUTI<br/>KAMI</h4>
							<p className="headline">Follow sosial media kami untuk mengetahui berita terbaru mengenai Perumdam Tirta Kerta Raharja.</p>
						</div>
					</div>
					<div className="col-md-6">
						<div className="row">
							<div className="col-md-6">
								<div className="cupz">
									<a className="link ig" target="_blank" href={igUrl}></a>
									<div className="username">IG:perumdamtkr</div>
								</div>
							</div>
							<div className="col-md-6">
								<div className="cupz">
									<a className="link fb" target="_blank" href={fbUrl}></a>
									<div className="username">PERUMDAM TKR</div>
								</div>
							</div>
						</div>
						<div className="row">
							<div className="col-md-6">
								<div className="cupz">
									<a className="link twitter" target="_blank" href={twitterUrl}></a>
									<div className="username">@perumdamtkr</div>

								</div>
							</div>
							<div className="col-md-6">
								<div className="cupz">
									<a className="link yt" target="_blank" href={ytUrl}></a>
									<div className="username">PERUMDAM TKR</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}