class RegisterWeb extends React.Component{
	
	render(){
		const {is_ajax} = this.props;
		let content = (<div></div>);

		if(is_ajax){
			content = (
				<div id="app">
					<PageBanner/>
					<FormRegistrasi/>
					<ServiceButtonSm/>
					
				</div>
			)
		}else{
			content = (
				<div id="app">
					<Header/>
					<main role="main">
					<PageBanner/>
					<FormRegistrasi/>
					<ServiceButtonSm/>

					</main>
					<MyCompany/>
					<Footer/>
				</div>
			);
		}
		return content; 
	}
}