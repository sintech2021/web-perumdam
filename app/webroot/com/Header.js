class Header extends React.Component{
	
	render(){
		const fbUrl ='https://www.facebook.com/humasperumdamtkr';
		const igUrl ='https://www.instagram.com/perumdamtkr';
		const twitterUrl ='https://twitter.com/perumdamtkr';
		const  captions = [
				{title : 'Beranda', uri: '',key:1},
				{title : 'Berita',uri:'',key:2},
				{title : 'Tentang Kami',uri:'',key:3},
				{title : 'Layanan Informasi',uri:'',key:4},
				{title : 'Laboratorium',uri:'',key:5},
				{title : 'Lelang',uri:'',key:6},
				{title : 'PPID' ,uri:'',key:7},
				{title : 'Kontak',uri:'',key:8},
			];
		 
		return (
			
<header className="header" style={{paddingLeft:'10px',paddingRight:'10px',paddingTop:'10px'}}>
<div className="row kop">
	<div  className="col-md-6" style={{}}>
		<div style={{flexDirection:'row',display:'flex'}}>
			<div style={{paddingRight:'1em'}}>
				<a className="homepage" href={site_url()}>
	 				<img src={site_url()+"/pub/img/perumdam-top.png"}/>
	 			</a>
	 		</div>	
	 		<div style={{flexDirection:'row'}}>
	 			<h1 className="homepage_title">PERUMDAM TIRTA KERTA RAHARJA</h1>
	 			<h4 className="homepage_sub_title">KABUPATEN TANGERANG</h4>
	 		</div>
 		</div>
 	</div>
 	<div  className="col-md-6" style={{textAlign:'right'}}>
 	
 		<ul className="social-icons">
 			<li className="fb"><a target="_blank" href={fbUrl}></a></li>
 			<li className="ig"><a target="_blank" href={igUrl}></a></li>
 			<li className="twitter"><a target="_blank" href={twitterUrl}></a></li>
 		</ul>
 	</div>
</div>
<div style={{padding:'0 1.5em'}}> <hr className="featurette-divider" style={{margin:'0'}}/></div>
<nav className="navbar navbar-dark bg-dark nav-mobile">
    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
  </nav>
  <nav className="navbar navbar-expand-md  top">

    <div className="collapse navbar-collapse" id="navbarCollapse">
       
        <MenuItem/>
     
      	<SearchBar/>
    </div>
  </nav>
</header>


		)
	}
}