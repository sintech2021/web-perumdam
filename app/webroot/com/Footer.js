class Footer extends React.Component{
	state ={
		web_links :[
			 
		],
		office_links:[
		],
	}
	getWebLinks(){
		let url = site_url() +`/data/web-link`;
		axios.get(url).then((res)=>{
			let web_links = [];
			try{
				res.data.list.forEach((row,index)=>{
					web_links.push({
						title : row.title,
						url :row.url
					});
				});
				this.setState({web_links:web_links});
			}catch(e){
			}
		});
	}
	getOfficeLinks(){
		let url = site_url() +`/data/office-service`;
		axios.get(url).then((res)=>{
			let office_links = [];
			try{
				res.data.list.forEach((row,index)=>{
					office_links.push({
						title : row.title,
						url :row.url,
						id:row.pk
					});
				});
				this.setState({office_links:office_links});
			}catch(e){
			}
		});
	}
	onNavigateMenu = (e)=>{
		const el = e.target;
		if(!el.href.match(/http(s)?/))
		return;
		
		if($(el).hasClass('induk')){
		}else{
			bindHistoryPushState(el);
		}
		e.preventDefault();
		return false;
	}
	componentWillMount(){
		
		setTimeout(()=>{
 			this.getWebLinks();	 
 			this.getOfficeLinks();	 
		},250);
	}
	render(){
			const igUrl = 'https://www.instagram.com/perumdamtkr';
		const twitterUrl = 'https://twitter.com/perumdamtkr';
		const fbUrl = 'https://www.facebook.com/humasperumdamtkr'; 

		const {web_links,office_links} = this.state;
		// const office_links = [
		// 	{title:'Kantor Wilayah Pelayanan III',url:'#'},
		// 	{title:'Kantor Cabang Serpong',url:'#'},
		// 	{title:'Kantor Cabang Tigakarsa',url:'#'},
		// 	{title:'Kantor Cabang Teluknaga',url:'#'},
		// ];
		const contact_links = [
			{icon:'map-marker.png',title:'Jl. Kisamaun No.204, RT.002/RW.007 Sukasari, Kec. Tangerang, Kota Tangerang, Banten 15118',url:site_url()+'/web/kontak_kami'},
			{icon:'tel.png',title:'Kantor – (021) 557777 666',url:'tel:+6221557777666'},
			{title:'HP – 0821 222777 666',url:'tel:+62821222777666'},
			{icon:'envelope.png',title:'contact@pdamtkr.co.id',url:'mailto:contact@pdamtkr.co.id'},
		]; 

		return (
			<footer className="container" style={{padding:'1.5em 3em'}}>
<div style={{padding:'0 0.5em 1.5em 0'}}> <hr className="featurette-divider" style={{margin:'0'}}/></div>

				<div className="row footer-items">
					<div className="col-md-4">
					<h4 className="footer-item-heading">PERUMDAM TIRTA KERTA RAHARJA</h4>
						<ul className="web-links contact">
						{
							contact_links.map((row,index)=>{
								let _style = {

								};
								if(typeof row.icon != 'undefined'){
									let _bgurl = site_url()+`/pub/img/${row.icon}`;
									_style.background=`transparent url(${_bgurl}) no-repeat top left`;
								}
								return (
									<li style={_style} key={`contact-link-item-${index}`} className="contact item"><a onClick={this.onNavigateMenu} className="link" href={row.url}>{row.title}</a></li>
								)
							})
							
						}
						</ul>
					</div>
					<div className="col-md-3">
					<h4 className="footer-item-heading">KANTOR PELAYANAN</h4>
						<ul className="web-links">
						{
							office_links.map((row,index)=>{
								return (
									<li key={`office-link-item-${index}`} className="item">
										<a onClick={this.onNavigateMenu}  href={site_url()+`/web/layanan_informasi/kantor_layanan?active=${row.id}`} className="link">{row.title}</a>
									</li>
								)
							})
							
						}
						</ul>
					</div>
					<div className="col-md-3">
						<h4 className="footer-item-heading">WEB LINK</h4>
						<ul className="web-links">
						{
							web_links.map((row,index)=>{
								return (
									<li key={`web-link-item-${index}`} className="item"><a target="_blank" className="link" href={row.url}>{row.title}</a></li>
								)
							})
							
						}
						</ul>
					</div>
					<div className="col-md-2">
						<h4 className="footer-item-heading">DOWNLOAD APPS</h4>
						<div className="app-link-container">
							<a target="_blank" href="https://play.google.com/store/apps/developer?id=PERUMDAM+TKR">
								<img src={site_url()+'/pub/img/playstore.png'}/>
							</a>
						</div>
						<ul className="social-icon-bottom">
							<li className="fb"><a  target="_blank" href={fbUrl}></a></li>
							<li className="ig"><a target="_blank" href={igUrl}></a></li>
							<li className="twitter"><a target="_blank" href={twitterUrl}></a></li>
						</ul>
					</div>
				</div>	
<div style={{padding:'1.5em 0.5em 0 0px'}}> <hr className="featurette-divider" style={{margin:'0'}}/></div>

				<div className="footer-text">
			    	<p className="text-center">All Rights Reserved by Perumdam Tirta Kerta Raharja &copy; <b>2020</b> </p>
				</div>
			</footer>
		)
	}
}