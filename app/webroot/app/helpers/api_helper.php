<?php
function curl_get_recaptcha($response){
        
    $url                    = 'https://www.google.com/recaptcha/api/siteverify';
    $parameters['secret']   = '6LecSeYUAAAAACY6dE5Coos_MGmeHZ5zHNycXmTK';
    $parameters['response'] = $response;
    $data                   = http_build_query($parameters);

    // Initialize the PHP curl agent
    $ch     = curl_init();
    curl_setopt($ch, CURLOPT_USERAGENT, "curl");
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FAILONERROR, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_VERBOSE, true);
    $result = curl_exec($ch);
    curl_close($ch);
    return json_decode($result);
}
function thumb_image($source, $destination, $width, $height)
{
    
    $imagine   = new Imagine\Gd\Imagine();
    $size      = new Imagine\Image\Box($width, $height);
    $mode      = Imagine\Image\ImageInterface::FILTER_UNDEFINED;
    $resizeimg = $imagine->open($source)
                    ->resize($size, $mode);
    $sizeR     = $resizeimg->getSize();
    $widthR    = $sizeR->getWidth();
    $heightR   = $sizeR->getHeight();

    $preserve  = $imagine->create($size);
    $startX = $startY = 0;
    if ($widthR < $width) {
        $startX = ( $width - $widthR ) / 2;
    }
    if ($heightR < $height) {
        $startY = ( $height - $heightR ) / 2;
    }
    $preserve->paste($resizeimg, new Imagine\Image\Point($startX, $startY))
        ->save($destination);
}
function format_dt($dt,$tt)
    {
        $hari = array ( 1 =>    'Senin',
                'Selasa',
                'Rabu',
                'Kamis',
                'Jumat',
                'Sabtu',
                'Minggu'
            );
            
        $bulan = array (1 =>   'Januari',
                    'Februari',
                    'Maret',
                    'April',
                    'Mei',
                    'Juni',
                    'Juli',
                    'Agustus',
                    'September',
                    'Oktober',
                    'November',
                    'Desember'
                );
        $strpubdate = strtotime($dt);
        $postday    =  date("d", $strpubdate) ;
        $postdate_l   = date("N", $strpubdate);
        $postdate   = $hari[$postdate_l];
        $postmonth_n = date('n',$strpubdate);
        $postmonth  = $bulan[$postmonth_n];//__d('syscm', date("F", $strpubdate));
        $postyear   = date("Y", $strpubdate);
        $posttime   = isset($tt) ? sprintf('%s ',
                        $tt) : '';
        return $post_date  = sprintf(
                '%s ' .
               
                '%s ' .
                '%s, ' .
                '%s ' ,  $postday,  $postmonth, $postyear, $posttime);
    }
function fix_snippet($text){
        $words = explode(' ',$text);
        array_pop($words);
        return implode(' ', $words);
    }
function get_first_img($the_content, $default = "/pdamtkr/logo.png")
    {
        $img_src     = $default;
        $startstrpos = '<img';
        $stoptstrpos = '/>';
        $src         = 'src="';

        $foundpos = strpos($the_content, $startstrpos);
        if ($foundpos):
            $next_content = substr($the_content, $foundpos);
            $foundsrc     = strpos($next_content, $src);
            $next_content = substr($next_content, $foundsrc + strlen($src));
            $foundquoted  = strpos($next_content, '"');
            $img_src      = substr($next_content, 0, $foundquoted);

        endif;
        return $img_src;
    }
function struuid($entropy)
{
    $s=uniqid("",$entropy);
    $num= hexdec(str_replace(".","",(string)$s));
    $index = '1234567890ABCDEFTXIJKLMNOPQRSGUVWHYZ';
    $base= strlen($index);
    $out = '';
        for($t = floor(log10($num) / log10($base)); $t >= 0; $t--) {
            $a = floor($num / pow($base,$t));
            $out = $out.substr($index,$a,1);
            $num = $num-($a*pow($base,$t));
        }
    return $out;
}
function is_mobile(){
    $detect = new Mobile_Detect;
    return $detect->isMobile();
}
function uuid() {   

     // Generate 128 bit random sequence
     $randmax_bits = strlen(base_convert(mt_getrandmax(), 10, 2));  // how many bits is mt_getrandmax()
     $x = '';
     while (strlen($x) < 128) {
         $maxbits = (128 - strlen($x) < $randmax_bits) ? 128 - strlen($x) :  $randmax_bits;
         $x .= str_pad(base_convert(mt_rand(0, pow(2,$maxbits)), 10, 2), $maxbits, "0", STR_PAD_LEFT);
     }

     // break into fields
     $a = array();
     $a['time_low_part'] = substr($x, 0, 32);
     $a['time_mid'] = substr($x, 32, 16);
     $a['time_hi_and_version'] = substr($x, 48, 16);
     $a['clock_seq'] = substr($x, 64, 16);
     $a['node_part'] =  substr($x, 80, 48);
    
     // Apply bit masks for "random or pseudo-random" version per RFC
     $a['time_hi_and_version'] = substr_replace($a['time_hi_and_version'], '0100', 0, 4);
     $a['clock_seq'] = substr_replace($a['clock_seq'], '10', 0, 2);

    // Format output
    return sprintf('%s-%s-%s-%s-%s',
        str_pad(base_convert($a['time_low_part'], 2, 16), 8, "0", STR_PAD_LEFT),
        str_pad(base_convert($a['time_mid'], 2, 16), 4, "0", STR_PAD_LEFT),
        str_pad(base_convert($a['time_hi_and_version'], 2, 16), 4, "0", STR_PAD_LEFT),
        str_pad(base_convert($a['clock_seq'], 2, 16), 4, "0", STR_PAD_LEFT),
        str_pad(base_convert($a['node_part'], 2, 16), 12, "0", STR_PAD_LEFT));
}
function uniqidReal($lenght = 13) {
    // uniqid gives 13 chars, but you could adjust it to your needs.
    if (function_exists("random_bytes")) {
        $bytes = random_bytes(ceil($lenght / 2));
    } elseif (function_exists("openssl_random_pseudo_bytes")) {
        $bytes = openssl_random_pseudo_bytes(ceil($lenght / 2));
    } else {
        throw new Exception("no cryptographically secure random function available");
    }
    return substr(bin2hex($bytes), 0, $lenght);
}
function gen_uuid() {
    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        // 32 bits for "time_low"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

        // 16 bits for "time_mid"
        mt_rand( 0, 0xffff ),

        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 4
        mt_rand( 0, 0x0fff ) | 0x4000,

        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
        mt_rand( 0, 0x3fff ) | 0x8000,

        // 48 bits for "node"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
    );
}
function date_format_id($mysql_date,$time=false)
{
    return date('d-m-Y'.($time?' H.i':""), strtotime($mysql_date));
}
function slugify($text)
{
  // replace non letter or digits by -
  $text = preg_replace('~[^\pL\d]+~u', '-', $text);

  // transliterate
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);

  // trim
  $text = trim($text, '-');

  // remove duplicate -
  $text = preg_replace('~-+~', '-', $text);

  // lowercase
  $text = strtolower($text);

  if (empty($text)) {
    return 'n-a';
  }

  return $text;
}
function remove_file_extension($filename){
    $base_exploded = explode(".", $filename);
    $ext   = end($base_exploded);
    $base  = basename($filename,'.'.$ext);
    return $base;
}
function slugify_filename($filename){
    $base_exploded = explode(".", $filename);
    $ext   = end($base_exploded);
    $base  = basename($filename,$ext);
    // echo $base . "\n";
    // $filenames = explode($base, $filename);
    return slugify($base).'.'.$ext;
}
function get_time_ago( $tanggal )
{
    $time = strtotime($tanggal);
    $time_difference = time() - $time;

    if( $time_difference < 1 ) { return 'beberapa detik yang lalu'; }
    $condition = array( 12 * 30 * 24 * 60 * 60 =>  'tahun',
                30 * 24 * 60 * 60       =>  'bulan',
                24 * 60 * 60            =>  'hari',
                60 * 60                 =>  'jam',
                60                      =>  'menit',
                1                       =>  'detik'
    );

    foreach( $condition as $secs => $str )
    {
        $d = $time_difference / $secs;

        if( $d >= 1 )
        {
            $t = round( $d );
            return '' . $t . ' ' . $str . ( $t > 1 ? '' : '' ) . ' yang lalu';
        }
    }
}
function dt_indo($tanggal )
{           
    $bulan = array (1 =>   'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            );
    $tm = strtotime($tanggal);
    $tanggal = date('Y-m-d', $tm);
    
    $split    = explode('-', $tanggal);
    $tgl_indo = preg_replace('/^0/', '', $split[2]) . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
    
    
    return $tgl_indo . ', '. date('H:i',$tm);
}
function my_simple_crypt($string, $action = 'e')
{
    // you may change these values to your own
    $secret_key = 'sun-gho-khong';
    $secret_iv = 'thong-sam-chong';
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash('sha256', $secret_key);
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
 
    if ($action == 'e') {
        $output = base64_encode(openssl_encrypt($string, $encrypt_method, $key, 0, $iv));
    } elseif ($action == 'd') {
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }
 
    return $output;
}

function send_email($from_address, $from_name, $to_address, $subject, $message)
{
    $ci = get_instance();
    $ci->load->library('email');
    $ci->load->config('ppsl');
    //send email to user
    // $config['useragent']      = (string) $this->cms_get_config('cms_email_useragent');
    $config['protocol']       = 'smtp';
    // $config['mailpath']       = (string) $this->cms_get_config('cms_email_mailpath');
    $config['smtp_host']      = $ci->config->item('smtp_host');
    $config['smtp_user']      = $ci->config->item('smtp_user');
    $config['smtp_pass']      = $ci->config->item('smtp_pass');
    $config['smtp_port']      = $ci->config->item('smtp_port');
    // $config['smtp_timeout']   = 30;//(integer) $this->cms_get_config('cms_email_smtp_timeout');
    // $config['wordwrap']       = (boolean) $this->cms_get_config('cms_email_wordwrap');
    // $config['wrapchars']      = (integer) $this->cms_get_config('cms_email_wrapchars');
    $config['mailtype']       = 'html';
    $config['charset']        = 'iso-8859-1';
    $config['validate']       = FALSE;
    // $config['priority']       = (integer) $this->cms_get_config('cms_email_priority');
    $config['crlf']           = "\r\n";
    $config['newline']        = "\r\n";
    $config['smtp_crypto']        = "ssl";
    // $config['bcc_batch_mode'] = (boolean) $this->cms_get_config('cms_email_bcc_batch_mode');
    // $config['bcc_batch_size'] = (integer) $this->cms_get_config('cms_email_bcc_batch_size');
   
    
    // die($message);
    $ci->email->initialize($config);
    // $ci->email->set_header("Reply-To","no-reply@perumdamtkr.com");
    $ci->email->from($from_address, $from_name);
    $ci->email->to($to_address);
    $ci->email->subject($subject);
    $ci->email->message($message);

    $success = $ci->email->send();
   
    return $success;
}
function send_email_verbose($from_address, $from_name, $to_address, $subject, $message)
{
    $ci = get_instance();
    $ci->load->library('email');
    $ci->load->config('ppsl');
    //send email to user
    // $config['useragent']      = (string) $this->cms_get_config('cms_email_useragent');
    $config['protocol']       = 'smtp';
    // $config['mailpath']       = (string) $this->cms_get_config('cms_email_mailpath');
    $config['smtp_host']      = $ci->config->item('smtp_host');
    $config['smtp_user']      = $ci->config->item('smtp_user');
    $config['smtp_pass']      = $ci->config->item('smtp_pass');
    $config['smtp_port']      = $ci->config->item('smtp_port');
    $config['smtp_timeout']   = 30;//(integer) $this->cms_get_config('cms_email_smtp_timeout');
    // $config['wordwrap']       = (boolean) $this->cms_get_config('cms_email_wordwrap');
    // $config['wrapchars']      = (integer) $this->cms_get_config('cms_email_wrapchars');
    $config['mailtype']       = 'html';
    $config['charset']        = 'iso-8859-1';
    $config['validate']       = FALSE;
    // $config['priority']       = (integer) $this->cms_get_config('cms_email_priority');
    $config['crlf']           = "\r\n";
    $config['newline']        = "\r\n";
    $config['smtp_crypto']        = "ssl";
    // $config['bcc_batch_mode'] = (boolean) $this->cms_get_config('cms_email_bcc_batch_mode');
    // $config['bcc_batch_size'] = (integer) $this->cms_get_config('cms_email_bcc_batch_size');
   
    
    // die($message);
    $ci->email->initialize($config);
    // $ci->email->set_header("Reply-To","no-reply@perumdamtkr.com");
    $ci->email->from($from_address, $from_name);
    $ci->email->to($to_address);
    $ci->email->subject($subject);
    $ci->email->message($message);
    print_r($config);
    $success = $ci->email->send();
    echo $ci->email->print_debugger();
    return $success;
}
function base64_image($path){
    $file_not_found_path = FCPATH . '/resource/images/file_not_found.png';
    if(!file_exists($path)){
        $path = $file_not_found_path;
    }
    $image_info = getimagesize($path);
    return sprintf("data:%s;base64,%s",$image_info['mime'],base64_encode(file_get_contents($path)));
}
function time_ago( $date_str )
{
    $time = strtotime($date_str);
    $time_difference = time() - $time;

    if( $time_difference < 1 ) { return 'beberapa detik yang lalu'; }
    $condition = array( 12 * 30 * 24 * 60 * 60 =>  'tahun',
                30 * 24 * 60 * 60       =>  'bulan',
                24 * 60 * 60            =>  'hari',
                60 * 60                 =>  'jam',
                60                      =>  'menit',
                1                       =>  'detik'
    );

    foreach( $condition as $secs => $str )
    {
        $d = $time_difference / $secs;

        if( $d >= 1 )
        {
            $t = round( $d );
            return '' . $t . ' ' . $str . ( $t > 1 ? '' : '' ) . ' yang lalu';
        }
    }
}


function valid_koordinat($koordinat){
    $koordinat_arr = explode(',', $koordinat);
    if(count($koordinat_arr) == 2){
        $lo = floatval($koordinat_arr[0]);
        $la = floatval($koordinat_arr[1]);

        return ['lo'=>$lo,'la'=>$la];
    }
    return false;
}

function strip_title($wordCount,$title)
{
    $title = preg_replace('/\W+/',' ', $title);

    $titles = explode(' ', $title);

    if($wordCount > count($titles)){
        $wordCount = count($titles);
    } 
    $titles_s = [];
    for($i=0;$i<$wordCount;$i++){
        $titles_s[]=$titles[$i];
    }
    $title = implode(' ', $titles_s) . ($wordCount<count($titles)?'...':'');
    return $title;
}

function comment_dt($dt){
    $hari = array ( 1 =>    'Senin',
                'Selasa',
                'Rabu',
                'Kamis',
                'Jumat',
                'Sabtu',
                'Minggu'
            );
            
        $bulan = array (1 =>   'Januari',
                    'Februari',
                    'Maret',
                    'April',
                    'Mei',
                    'Juni',
                    'Juli',
                    'Agustus',
                    'September',
                    'Oktober',
                    'November',
                    'Desember'
                );
        $strpubdate = strtotime($dt);
        $postday    =  date("d", $strpubdate) ;
        $postdate_l   = date("N", $strpubdate);
        $postdate   = $hari[$postdate_l];
        $postmonth_n = date('n',$strpubdate);
        $postmonth  = $bulan[$postmonth_n];//__d('syscm', date("F", $strpubdate));
        $postyear   = date("Y", $strpubdate);
        $posttime   = date('H:i', $strpubdate);
        return $post_date  = sprintf(
                '%s ' .
               
                '%s ' .
                '%s ' .
                'pukul %s ' ,  $postday,  $postmonth, $postyear, $posttime);
}