<?php

class Layanan extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_api_web');
		$this->load->library('form_validation');
		header('Content-Type: application/json');
	}

	// public function do_register()
	// {
	// 	# code...
	// }
	public function do_register_check()
	{
	 
        $this->form_validation->set_rules('kode_pendaftaran','Kode Registrasi','required');

		$kode_pendaftaran = $this->input->post('kode_pendaftaran');

		$data = [];
		$msg = [];
		$success = false;
		if(defined('ENABLE_G_CAPTCHA')){

            if(ENABLE_G_CAPTCHA){
               
                $recaptchaResponse = $this->input->post('gRecaptcha');
                $recaptchaCallenge = curl_get_recaptcha($recaptchaResponse);
              
                 if (!$recaptchaCallenge->success) {

                    header('Content-Type:application/json');
                    $msg="Mohon verifikasi bahwa Anda bukan robot";
                    echo json_encode(['success'=>$success,'msg'=>$msg,'data'=>$data,'recaptcha_challenge'=>true]);
                    exit();
                }
            }
        }
        if($this->form_validation->run()){
			$token = $this->m_api_web->_getToken();
			if(!empty($token)){
				$data_str = $this->m_api_web->check_status_pendaftaran($kode_pendaftaran, $token);
				$data = json_decode($data_str);
				// $success = true;
				// print_r($data);
				if(is_object($data->customer)){
					$kdKab = $data->customer->kdKab;
					$kdKec = $data->customer->kdKec;
					$kdKel = $data->customer->kdKel;

					// print_r($kdKab);
					// print_r($kdKec);
					// print_r($kdKel);
					$kabs = $this->m_api_web->get_daftar_kabupaten();
					// print_r($kabs);
					if(is_array($kabs->data)){
						foreach ($kabs->data as $kab) {
							if($kab->kdKab == $kdKab){
								$data->customer->kabupaten = $kab->kabupaten;
							}
						}
					}

					$kecs = $this->m_api_web->get_daftar_kecamatan($kdKab);
					// print_r($kecs);
					if(is_array($kecs->data)){
						foreach ($kecs->data as $kec) {
							if($kec->kdKec == $kdKec){
								$data->customer->kecamatan = $kec->kecamatan;
							}
						}
					}

					$kels = $this->m_api_web->get_daftar_kelurahan($kdKec,$kdKab);
					// print_r($kecs);
					if(is_array($kels->data)){
						foreach ($kels->data as $kel) {
							if($kel->kdKel == $kdKel){
								$data->customer->kelurahan = $kel->kelurahan;
							}
						}
					}

				}
				echo json_encode($data);
				return;
			}else{
				$msg = 'Failed to get token api server could not be reach';
			}
		}else{
			$msg = validation_errors();
		}
        echo json_encode(['data'=>$data, 'msg' => $msg, 'success'=>$success]);

	}
	public function do_get_tagihan()
	{
		$bulan = $this->input->post('bulan');
        $tahun = $this->input->post('tahun');
        $param_tahun = sprintf("%s-%s-01",$tahun,$bulan);
        $id_pelanggan = $this->input->post('id_pelanggan');
        
        $this->form_validation->set_rules('bulan','Bulan','required');
        $this->form_validation->set_rules('tahun','Tahun','required');
        $this->form_validation->set_rules('id_pelanggan','ID Pelanggan','required');
		$data = [];
		$msg = [];
		$success = false;
		if(defined('ENABLE_G_CAPTCHA')){

            if(ENABLE_G_CAPTCHA){
               
                $recaptchaResponse = $this->input->post('gRecaptcha');
                $recaptchaCallenge = curl_get_recaptcha($recaptchaResponse);
              
                 if (!$recaptchaCallenge->success) {

                    header('Content-Type:application/json');
                    $msg="Mohon verifikasi bahwa Anda bukan robot";
                    echo json_encode(['success'=>$success,'msg'=>$msg,'data'=>$data,'recaptcha_challenge'=>true]);
                    exit();
                }
            }
        }
        if($this->form_validation->run()){
			$token = $this->m_api_web->_getToken();
			if(!empty($token)){
				$info_tagihan = $this->m_api_web->get_info_tagihan($id_pelanggan, $tahun, $token);
				// $data = json_encode($info_tagihan);
				// $success = true;
				echo $info_tagihan;
				return;
			}else{
				$msg = 'Failed to get token api server could not be reach';
			}
        }else{
        	$msg = validation_errors();
        }

        echo json_encode(['data'=>$data, 'msg' => $msg, 'success'=>$success]);
	}
	public function do_get_tunggakan()
	{
	
        $id_pelanggan = $this->input->post('id_pelanggan');
        
       
        $this->form_validation->set_rules('id_pelanggan','ID Pelanggan','required');
		$data = [];
		$msg = '';
		$success = false;
		if(defined('ENABLE_G_CAPTCHA')){

            if(ENABLE_G_CAPTCHA){
               
                $recaptchaResponse = $this->input->post('gRecaptcha');
                $recaptchaCallenge = curl_get_recaptcha($recaptchaResponse);
              
                 if (!$recaptchaCallenge->success) {

                    header('Content-Type:application/json');
                    $msg="Mohon verifikasi bahwa Anda bukan robot";
                    echo json_encode(['success'=>$success,'msg'=>$msg,'data'=>$data,'recaptcha_challenge'=>true]);
                    exit();
                }
            }
        }
        if($this->form_validation->run()){
			$token = $this->m_api_web->_getToken();
			if(!empty($token)){
				$info_tagihan = $this->m_api_web->get_info_tunggakan($id_pelanggan, $token);
				// $data = json_encode($info_tagihan);
				// $success = true;
				echo $info_tagihan;
				return;
			}else{
				$msg = 'Failed to get token api server could not be reach';
			}
        }else{
        	$msg = validation_errors();
        }

        echo json_encode(['data'=>$data, 'msg' => $msg, 'success'=>$success]);
	}
	public function do_get_pengaduan_response()
	{

		$id_pelanggan = $this->input->post('id_pengaduan');
        
       $msg='';
       $success=false;
        $this->form_validation->set_rules('id_pengaduan','ID Pengaduan','required');
        if($this->form_validation->run()){
			$success=true;
			$data = $this->db->select("*,DATE(created) as dd,TIME(created) as tt")->where('field','ScmCustFeedbackRespon')
        							 ->where('scm_customer_id',$id_pelanggan)
        							 ->order_by('modified','dec')
        							 ->get('sys_scm_customers_details')
        							 ->result_array();
        	foreach ($data as &$r) {
        			 
        			$r['dt']=format_dt($r['dd'],$r['tt']);
        		}
        }else{
        	$msg = validation_errors();
        }

        echo json_encode(['data'=>$data, 'msg' => $msg, 'success'=>$success]);
		
	}
	public function do_get_riwayat_pengaduan()
	{
		$data = [];
		$msg = '';
		$success = false;

		$id_pelanggan = $this->input->post('id_pelanggan');
        $this->form_validation->set_rules('id_pelanggan','ID Pelanggan','required');
        if(defined('ENABLE_G_CAPTCHA') && $_SERVER['HTTP_HOST'] != 'localhost'){

            if(ENABLE_G_CAPTCHA){
               
                $recaptchaResponse = $this->input->post('gRecaptcha');
                $recaptchaCallenge = curl_get_recaptcha($recaptchaResponse);
              
                 if (!$recaptchaCallenge->success) {

                    header('Content-Type:application/json');
                    $msg="Mohon verifikasi bahwa Anda bukan robot";
                    echo json_encode(['success'=>$success,'msg'=>$msg,'data'=>$data,'recaptcha_challenge'=>true]);
                    exit();
                }
            }
        }
        if($this->form_validation->run()){
        	$pelanggan = $this->db->where('menu_code','CustomerList')
        						  ->where('text',$id_pelanggan)
        						  ->get('sys_scm_customers')
        						  ->row_array();
        	if(!empty($pelanggan)){
        		$feedback = $this->db->select('id,CONCAT(LEFT(annotation,20),\'...\' ) as title,ticket,annotation as content,DATE(created) as dd,TIME(created) as tt')->where('text',$id_pelanggan)
        								 ->order_by('created','desc')
        								 ->limit(10)
        								 ->get('sys_scm_feedback')
        								 ->result_array();
        										 
        		foreach ($feedback as &$feed) {
        			 
        			$feed['dt']=format_dt($feed['dd'],$feed['tt']);
        		}
	        	$data['feedback']=$feedback;
	        	// $data['response']=$response;
	        	$data['pelanggan']=$pelanggan;
	        	$success = true;
        	}					  
        	
        }else{
        	$msg = validation_errors();
        }
        echo json_encode(['data'=>$data, 'msg' => $msg, 'success'=>$success]);

	}
	public function do_pengaduan()
	{

		$this->form_validation->set_rules('id_pelanggan','ID Pelanggan','required');
		$this->form_validation->set_rules('nama_lengkap','Nama Lengkap','required');
		$this->form_validation->set_rules('email','Email','required|valid_email');
		$this->form_validation->set_rules('nomor_hp','Nomor HP','required|numeric');
		// $this->form_validation->set_rules('nomor_telepon','Nomor Telepon','required|numeric');
		$this->form_validation->set_rules('pesan','Pesan','required');

		$ticket = 'TX'.strtoupper(substr(md5(mt_rand()), 0, 8));
		$id_pelanggan  = $this->input->post('id_pelanggan');
		$nama_lengkap  = $this->input->post('nama_lengkap');
		$email         = $this->input->post('email');
		$nomor_hp      = $this->input->post('nomor_hp');
		$nomor_telepon = $this->input->post('nomor_telepon');
		$pesan = $this->input->post('pesan');

		
		// print_r($customer);

		$msg = '';
		$success = false;
		$data = [];
		$pengaduan=[];
		if(defined('ENABLE_G_CAPTCHA') && $_SERVER['HTTP_HOST'] != 'localhost'){

            if(ENABLE_G_CAPTCHA){
               
                $recaptchaResponse = $this->input->post('gRecaptcha');
                $recaptchaCallenge = curl_get_recaptcha($recaptchaResponse);
              
                 if (!$recaptchaCallenge->success) {

                    header('Content-Type:application/json');
                    $msg="Mohon verifikasi bahwa Anda bukan robot";
                    echo json_encode(['success'=>$success,'msg'=>$msg,'data'=>$data,'recaptcha_challenge'=>true]);
                    exit();
                }
            }
        }
		if($this->form_validation->run()){
			$customer = $this->_get_customer($id_pelanggan);

			if(empty($customer)){
				echo json_encode(['success'=>false, 'msg'=>'Invalid Customer', 'data'=>[] ]);
				
				return;
			}
			
			$contact = [
				'menu_code' =>'CustomerFeedback',
				'name' => $nama_lengkap,
				'text' => $id_pelanggan,
				'annotation' => $pesan,
				'created' => date('Y-m-d H:i:s'),
				'modified' => date('Y-m-d H:i:s'),
				'active_status'=> 1,
				'id'=> gen_uuid(),

			];
			$this->db->insert('sys_scm_customers', $contact);
		 
			$pengaduan = [
				'id'=> $contact['id'],
				'name' => $nama_lengkap,
				'text' => $id_pelanggan,
				'annotation' => $pesan,
				'active_status'=> 1,
				'menu_code' => 'CustomerFeedback',
				'field' => 'ScmCustFeedbackContact',
				'created' => date('Y-m-d H:i:s'),
				'modified' => date('Y-m-d H:i:s'),
				'scm_customer_id' => $contact['id'],
				'value' => $nomor_hp,
				'mobile' => $nomor_hp,
				'email' => $email,
				'phone' => $nomor_telepon,
				'ip_address' => $this->input->ip_address(),
				'ticket' => $ticket,
			];

			$this->db->insert('sys_scm_feedback', $pengaduan);
			$success = true;
			$msg = "Pengaduan berhasil dikirim";
		}else{
			$validation_errors = validation_errors();
            $msg = $validation_errors;
            $success = false; 
		}

		echo json_encode(['success'=>$success, 'msg'=>$msg, 'data'=>$pengaduan]);
	}

	function _get_customer($id_pelanggan){
		$customer = $this->db->where('text',$id_pelanggan)
							 ->where('menu_code','CustomerList')
							 ->get('sys_scm_customers')
							 ->row();
		return $customer;
	}
}