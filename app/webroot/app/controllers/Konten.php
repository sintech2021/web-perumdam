<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Konten extends CI_Controller {

	public function _get_categories()
	{
		$table = 'sys_scm_categories';
		$select = "c.id,c.name slug, c.text title,c.menu_code class";
		$categories=[];
		$_categories = $this->db->select($select)
						       ->get($table.' c')
						       ->result();
		foreach ($_categories as $row) {
			$categories[$row->id] = $row;
		}
	}
	public function get_running_text()
	{
		header('Content-Type: application/json');
 		$contents = $this->db->order_by('id','desc')
							 ->get('sys_running_text')
							 ->result_array();
	 					 
		 					 
		echo json_encode($contents);
	}
	public function get_info_perumdam()
	{
		header('Content-Type: application/json');
 		$contents = $this->db->order_by('id','desc')
							 ->get('sys_info_perumdam')
							 ->result_array();
	 					 
		 					 
		echo json_encode($contents);
	}
	public function get_caraousel()
	{
		
		header('Content-Type: application/json');
		$contents = [];
		$g = $this->db->order_by('id','desc')
							 ->get('sys_banner')
							 ->result_array();
	 					 
		foreach ($g as $r) {
			$contents[]=[
				'url'=>$r['url'],
				'img_url'=>site_url().'uploads/banner_images/'.$r['path'],
				'title'=>''
			];
		}					 
		echo json_encode($contents);
	}
	public function get_content_by_category($category_id)
	{
		$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
		$contents = $this->db->select("k.name url, k.text title,p.value time_published, p.text date_published,
									   ct.value content")
							 // ->join('sys_scm_contents_details m',"m.scm_content_id=c.scm_content_id AND m.field='ScmContentMeta'",'left')
							 ->join('sys_scm_contents_details p',"p.scm_content_id=c.scm_content_id AND p.field='ScmContentPublished'",'left')
							 ->join('sys_scm_contents_details ct',"ct.scm_content_id=c.scm_content_id AND ct.field='ScmContentContent'",'left')
							 ->join('sys_scm_contents  k',"k.id=c.scm_content_id",'left')
							 ->where('c.scm_category_id',$category_id)
							 ->get('sys_scm_contents_categories c')
							 ->result();
		die($this->db->last_query());
		return $contents;
	}

	public function get_latest_gi_items()
	{
		header('Content-Type: application/json');
		$contents = [];
		$g = $this->db->select('id')->order_by('tgl_buat','desc')
							 ->where('tipe',0)
							 ->limit(5)
							 ->get('sys_galeries')
							 ->result_array();
		foreach($g as $gi){
			$gid = $gi['id'];
			$gii = $this->db->order_by('tgl_buat','desc')
							 ->where('tipe',0)
							 ->where('galery_id',$gid)
							 ->limit(4)
							 ->get('sys_galeries_items')
							 ->result_array();
			foreach($gii as $giii){
				$contents[] = $giii;
			}
		}					 

		echo json_encode($contents);
	}
	public function get_latest_gv_items()
	{
		header('Content-Type: application/json');
		$contents = [];
		$g = $this->db->select('id')->order_by('tgl_buat','desc')
							 ->where('tipe',1)
							 ->limit(1)
							 ->get('sys_galeries')
							 ->result_array();
		foreach($g as $gi){
			$gid = $gi['id'];
			$gii = $this->db->order_by('tgl_buat','desc')
							 ->where('tipe',1)
							 ->where('galery_id',$gid)
							 ->limit(1)
							 ->get('sys_galeries_items')
							 ->result_array();
			foreach($gii as $giii){
				$contents[] = $giii;
			}
		}					 

		echo json_encode($contents);
	}
	public function get_arsip_berita()
	{
		header('Content-Type: application/json');

		$scm_category_id = '' ;
        $row = $this->db->select('id')
                        ->where('name','berita')
                        ->get('sys_scm_categories')->row();
        if(!empty($row)){
            $scm_category_id = $row->id;
            $rs = $this->db->select('count(cd.id) _cx,MONTH(cd.text) m,YEAR(cd.text) y,concat(MONTH(cd.text),YEAR(cd.text)) arsip')
                     ->from('sys_scm_contents_categories cc')
                     ->join('sys_scm_contents_details cd',"cd.scm_content_id=cc.scm_content_id AND cd.field = 'ScmContentPublished'")
                     ->order_by('YEAR(cd.text)','asc')
                     ->order_by('MONTH(cd.text)','asc')
                     ->group_by('concat(MONTH(cd.text),YEAR(cd.text))')

                     ->get()
                     ->result_array();
            $by_years = [];
            foreach ($rs as $row) {
                $y = 0+$row['y'];
                $m = 0+$row['m'];
                $cx = 0+$row['_cx'];

                if(empty($y)){
                    continue;
                }
                if(empty($m)){
                    continue;
                }
                if(!isset($by_years[$y])){
                    $by_years[$y]=[];
                }

                $by_years[$y][$m]=$cx;
            }
            echo json_encode($by_years);
            return;
        }
            echo json_encode([]);

	}
	function get_list_by_parent($name,$exclude_id){
		// $name = '/web/laboratorium';
        		$menu_id = $this->db->select('scm_menu_id')
                            ->where('value',$name)

                            ->get('sys_scm_menus_details')
                            ->row()->scm_menu_id;
        		//echo $menu_id . "\n";
        		$sub_menus = $this->db->select('m.id menu_id,d.value name,c.id id,
					dp.value creation_time,
					dp.text creation_date,

					dm.value meta_value,
					dm.text meta_text,
					dc.value content,
					c.name url,
					dp.name meta_author,
					c.text title')
				  ->from('sys_scm_menus m')
				  ->where('m.parent_id',$menu_id)
				   ->where('c.id !=',$exclude_id)
				  ->where('dp.field','ScmContentPublished')
				  ->where('dm.field','ScmContentMeta')
				  ->where('dc.field','ScmContentContent')
				  ->where('dp.active_status','1')
				  ->where('dm.active_status','1')
				  ->where('dc.active_status','1')
				  ->where('c.menu_code','content_web')
				  ->where('c.active_status','1')
				  ->join('sys_scm_menus_details d','d.scm_menu_id=m.id','left')
				  ->join('sys_scm_contents c','c.name=d.value','left')

				  ->join('sys_scm_contents_details dp','dp.scm_content_id=c.id','left')
				  ->join('sys_scm_contents_details dm','dm.scm_content_id=c.id','left')
				  ->join('sys_scm_contents_details dc','dc.scm_content_id=c.id','left')
				  ->group_by('dc.scm_content_id')
				  ->get()
				  ->result_array();
				foreach ($sub_menus as &$sub_menu) {
					$sub_menu['dt'] = format_dt($sub_menu['creation_date'],$sub_menu['creation_time']);
					$sub_menu['bgurl'] = get_first_img($sub_menu['content']);
					$sub_menu['content'] = fix_snippet(trim(str_replace('&nbsp;',' ', substr(strip_tags($sub_menu['content']),0,150)))). '...';
				}
		return $sub_menus;
	}
	public function related($name='',$exclude_id='')
	{
		header('Content-Type: application/json');
		switch ($name) {
			case 'laboratorium':
				$name = '/web/laboratorium';
        		$sub_menus = $this->get_list_by_parent($name,$exclude_id);
				echo json_encode(['list'=>$sub_menus]);
			break;
			case 'ppid':
				$name = '/web/ppid';
        		$sub_menus = $this->get_list_by_parent($name,$exclude_id);
				echo json_encode(['list'=>$sub_menus]);
			break;
			case 'tentang_kami':
				$name = '/web/tentang_kami';
        		$sub_menus = $this->get_list_by_parent($name,$exclude_id);
				echo json_encode(['list'=>$sub_menus]);
			break;
			case 'layanan_informasi':
				$name = '/web/layanan_informasi';
        		$sub_menus = $this->get_list_by_parent($name,$exclude_id);
				echo json_encode(['list'=>$sub_menus]);
			break;
			case 'organisasi':
				$name = '/web/organisasi';
        		$sub_menus = $this->get_list_by_parent($name,$exclude_id);
				echo json_encode(['list'=>$sub_menus]);
			break;
			default:
				# code...
				break;
		}
	}

	public function photo_thumb($base_="",$name64="",$width="200",$height="200")
	{
		$base = realpath(FCPATH.'/uploads');
		$base_thumbnails = realpath(FCPATH.'/uploads/_thumbnails');
		
		// die(realpath());
		$file_path = $base . (!empty($base_)?'/'.$base_.'/':'/') . base64_decode($name64);

		// die($file_path);
		if(file_exists($file_path)){
			$data = getimagesize($file_path);
	        $o_width  = $data[0];
	        $o_height = $data[1];

	        $eq    = $o_width / $width;
	        $height = $o_height/$eq;

	        // die();
	        $__end = explode('.', $file_path);
	        $_end = end($__end);
	        $ext = '.'.$_end;
	        $file = (object)array();
	        $file->width        = $data[0];
	        $file->height       = $data[1];
	        $file->filename     = basename($file_path);
	        $file->extension    = $ext;
	        $file->mimetype     = $data['mime'];

	        $cache_dir = FCPATH.'/../../app/tmp/cache/images/';
        	$image_thumb = $cache_dir . md5($file->filename) . "${width}x${height}" . $file->extension;

        	// die($image_thumb);

	        if (! is_dir($cache_dir)) {
	            mkdir($cache_dir, 0777, true);
	        }
	        $expire = 60;

        if ($expire) {
            header("Pragma: public");
            header("Cache-Control: public");
            header('Expires: ' . gmdate('D, d M Y H:i:s', time()+$expire) . ' GMT');
        }

        if (! file_exists($image_thumb) or (filemtime($image_thumb) < filemtime($image_thumb))) {
            thumb_image($file_path, $image_thumb, $width, $height);
        } 
        //elseif (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) &&
        //     (strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == filemtime($image_thumb)) &&
        //         $expire ) {
        //     // Send 304 back to browser if file has not beeb changed
        //     header('Last-Modified: '.gmdate('D, d M Y H:i:s', filemtime($image_thumb)).' GMT', true, 304);
        //     exit();
        // }
        
        header('Content-type: ' . $file->mimetype);
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($image_thumb)) . ' GMT');
        @ob_clean();
        echo file_get_contents($image_thumb);
        exit();
		}
	}
	public function thumb($encrypted_path = '', $width = 100, $height = 100, $base64 = '')
    {
        $no_image_path =  'public/assets/themes/images/no-image-2.png';
        $path =  my_simple_crypt($encrypted_path, 'd');
        // die($path);
        if (is_dir($path)) {
            $path = 'invalid';
        }
     
        if (!file_exists($path)) {
            $data = getimagesize($no_image_path);
        }

        

        $data = getimagesize($path);

        // print_r($data);

        $o_width  = $data[0];
        $o_height = $data[1];

        $eq    = $o_width / $width;
        $height = $o_height/$eq;

        // die();
        $_end = end(explode('.', $path));
        $ext = '.'.$_end;
        $file = (object)array();
        $file->width        = $data[0];
        $file->height       = $data[1];
        $file->filename     = basename($path);
        $file->extension    = $ext;
        $file->mimetype     = $data['mime'];

        $cache_dir = FCPATH.'cache/_thumbnails/';

        if (! is_dir($cache_dir)) {
            mkdir($cache_dir, 0777, true);
        }
        $image_thumb = $cache_dir . md5($file->filename) . "${width}x${height}" . $file->extension;
        $image_url = site_url().'cache/_thumbnails/'. md5($file->filename) . "${width}x${height}" . $file->extension;
        
        if (file_exists($image_thumb)) {

            if($base64=='true'){
                header('Content-Type: application/json');
                $img_size = getimagesize($image_thumb);
                $response = [
                    'success' => true,
                    'data' => base64_encode(file_get_contents($image_thumb)),
                    'mime' => $img_size['mime']
                ];
                echo json_encode($response);
                return;
            }

            redirect($image_url);
            exit();
        }
        $expire = 60;

        if ($expire) {
            header("Pragma: public");
            header("Cache-Control: public");
            header('Expires: ' . gmdate('D, d M Y H:i:s', time()+$expire) . ' GMT');
        }

        if (! file_exists($cache_path) or (filemtime($cache_path) < filemtime($cache_path))) {
            thumb_image($path, $image_thumb, $width, $height);
        } elseif (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) &&
            (strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == filemtime($image_thumb)) &&
                $expire ) {
            // Send 304 back to browser if file has not beeb changed
            header('Last-Modified: '.gmdate('D, d M Y H:i:s', filemtime($image_thumb)).' GMT', true, 304);
            exit();
        }
        
        header('Content-type: ' . $file->mimetype);
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($image_thumb)) . ' GMT');
        ob_clean();
        echo file_get_contents($image_thumb);
        exit();
    }
    public function get_galery()
    {
		header('Content-Type: application/json');

    	$galery = $this->db->select('a.*,b.path,COUNT(b.id) cx')
                            // ->where('a.tipe',0)
                            ->join('sys_galeries_items b','b.galery_id=a.id','left')
                            ->group_by('a.id')
                            ->get('sys_galeries a')
                            ->result();
        foreach ($galery as &$g) {
        	$video_id = '';
        	if($g->tipe == 1){
        		preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $g->path, $matches);
        		$video_id = $matches[1];

        	}
        	if($g->tipe==1){
        		$g->url = $g->path;
        	}
        	if(empty($g->path)){
        		$g->path = 'default.png';
        	}
        	$g->thumb = site_url() . '/konten/photo_thumb/galery_images/'.base64_encode($g->path).'/600/600';	
        	
        	$g->path = $g->tipe == 0 ? site_url().'uploads/galery_images/'.$g->path
        							 : 'https://img.youtube.com/vi/'.$video_id.'/0.jpg';

        	if($g->tipe==1){
        		$g->thumb = $g->path;
        	}
        	$g->url = site_url().'galeri/items/'.$g->id.'/'.slugify($g->title);						 
        }                    
        echo json_encode($galery);
    }
    public function get_galery_items($galery_id)
    {
		header('Content-Type: application/json');

		$galery = $this->db->where('galery_id',$galery_id)
						 ->get('sys_galeries_items')
						 ->result();
		foreach ($galery as &$g) {
        	$video_id = '';
        	if($g->tipe == 1){
        		preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $g->path, $matches);
        		$video_id = $matches[1];

        	}
        	if(empty($g->path)){
        		$g->path = 'default.png';
        	}
        	
        	$g->url = site_url().'galeri/detail/'.$g->id.'/'.slugify($g->title);	
        	$g->thumb = site_url() . '/konten/photo_thumb/galery_images/'.base64_encode($g->path).'/600/600';	
        	if($g->tipe==1){
        		$g->url = $g->path;
        	}
        	$g->path = $g->tipe == 0 ? site_url().'uploads/galery_images/'.$g->path
        							 : 'https://img.youtube.com/vi/'.$video_id.'/0.jpg';	
        	if($g->tipe==1){
        		$g->thumb = $g->path;
        	}			 
        }                    
        echo json_encode($galery);
    	
    }
    public function get_galery_info($galery_id)
    {
		header('Content-Type: application/json');

		$galery = $this->db->where('id',$galery_id)
						 ->get('sys_galeries')
						 ->row();
	                
        echo json_encode($galery);
    	
    }
    public function get_galery_detail($item_id)
    {
		header('Content-Type: application/json');

		$galery = $this->db->where('id',$item_id)
						 ->get('sys_galeries_items')
						 ->row();
	    if(!empty($galery)){
	    	if(empty($galery->path)){
        		$galery->path = 'default.png';
        	}
        	$video_id = '';
        	if($galery->tipe == 1){
        		preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $galery->path, $matches);
        		$video_id = $matches[1];

        	}
        	$galery->path = $galery->tipe == 0 ? site_url().'uploads/galery_images/'.$galery->path
        							 : 'https://img.youtube.com/vi/'.$video_id.'/0.jpg';
        	$galery->url = site_url().'galeri/detail/'.$galery->id.'/'.slugify($galery->title);
	    }            
        echo json_encode($galery);
    	
    }
}
