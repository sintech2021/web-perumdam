<?php
use Dompdf\Dompdf; 

class Pendaftaran extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_api_web');
	}
	public function get_kab($id_prov='')
	{
		header('Content-Type:application/json');

		$response =  $this->m_api_web->get_daftar_kabupaten();

		echo json_encode($response);
	}

	public function get_kec($id_kab)
	{
		header('Content-Type:application/json');

		$response =  $this->m_api_web->get_daftar_kecamatan($id_kab);

		echo json_encode($response);
	}

	public function get_kel($id_kec,$id_kab)
	{
		header('Content-Type:application/json');

		$response =  $this->m_api_web->get_daftar_kelurahan($id_kec,$id_kab);

		echo json_encode($response);
	}

	public function do_register()
	{
		$this->load->library('form_validation');

        $nama_pemohon = $this->input->post('nama_pemohon');
        $nama_pemilik = $this->input->post('nama_pemilik');
        $nomor_ktp      = $this->input->post('nomor_ktp');
        $email    = $this->input->post('email');
        $nomor_hp = $this->input->post('nomor_hp');
        $nomor_telepon = $this->input->post('nomor_telepon');
        $id_kab = $this->input->post('id_kab');
        $id_kec = $this->input->post('id_kec');
        $id_kel = $this->input->post('id_kel');
        $alamat = $this->input->post('alamat');
        $kode_pos = $this->input->post('kode_pos');
        $kabupaten = $this->input->post('kabupaten');
        $kecamatan = $this->input->post('kecamatan');
        $kelurahan = $this->input->post('kelurahan');
        $foto = $this->input->post('foto');
        $foto_filename = $this->input->post('foto_filename');
        $id_prov ='36';
        $tgl_buat =date('Y-m-d H:i:s');
        $tgl_edit =  $tgl_buat;
        $kode_pendaftaran = gen_uuid();

        $this->form_validation->set_rules('nama_pemohon','Nama Pemohon','required');
        $this->form_validation->set_rules('nama_pemilik','Nama Pemilik','required');
        $this->form_validation->set_rules('nomor_ktp','Nomor KTP','required|numeric|min_length[10]|max_length[16]');
        $this->form_validation->set_rules('nomor_hp','Nomor HP','required|numeric|min_length[10]|max_length[14]');
        $this->form_validation->set_rules('nomor_telepon','Nomor Telepon','numeric');
        $this->form_validation->set_rules('id_kab','Kabupaten','required|numeric');
        $this->form_validation->set_rules('id_kec','Kecamatan','required|numeric');
        $this->form_validation->set_rules('id_kel','Kelurahan','required|numeric');
        $this->form_validation->set_rules('kode_pos','Kode Pos','required|numeric|min_length[4]|max_length[6]');
        $this->form_validation->set_rules('email','Email','required|valid_email');
        $this->form_validation->set_rules('alamat','Alamat','required');
        $this->form_validation->set_rules('foto','Foto KTP','required');
        // $this->form_validation->set_rules('gRecaptcha','Verifikasi','required');

        $success = false;
        $msg = '';
        $data = [];
        if(defined('ENABLE_G_CAPTCHA')){
            if(ENABLE_G_CAPTCHA){
                $recaptchaResponse = $this->input->post('gRecaptcha');
                $recaptchaCallenge = curl_get_recaptcha($recaptchaResponse);
                if (!$recaptchaCallenge->success) {

                    header('Content-Type:application/json');
                    $msg="Mohon verifikasi bahwa Anda bukan robot";
                    echo json_encode(['success'=>$success,'msg'=>$msg,'data'=>$data,'recaptcha_challenge'=>true]);
                    exit();
                }
            }
        }
     
        if($this->form_validation->run()){
            $nomor_ktp_is_unique = $this->_check_nomor_ktp_unique($nomor_ktp);

            if($nomor_ktp_is_unique){
                $foto_ktp = md5(microtime()).'-'.slugify_filename($foto_filename);
                $kode_pendaftaran = $this->_generate_no_reg($nama_pemohon);
                $status = 0;
                $id = gen_uuid();

                $data = compact('id','nama_pemohon','nama_pemilik','nomor_ktp','nomor_hp','nomor_telepon','id_kab','id_kec','id_kel','kode_pos','status','foto_ktp','email','id_prov','alamat','kode_pendaftaran','tgl_buat','tgl_edit','kabupaten','kecamatan','kelurahan');


                $upload_path = FCPATH . 'uploads/pendaftaran/' .$foto_ktp;
                $upload_ok = file_put_contents($upload_path, file_get_contents($foto));
                if($upload_ok){
                    $this->db->insert('sys_pendaftaran',$data);
                }
                $success = true;
            }else{
                $success = false;
                $msg = "<p>Nomor KTP sudah terdaftar.</p>";
            }
            
        }else{
        	$msg = $validation_errors = validation_errors();
            // $msg = str_replace(['<p>','</p>'],['',""],$validation_errors);
            $row = [

            ];
            $success = false; 
        }
		header('Content-Type:application/json');

        echo json_encode(['success'=>$success,'msg'=>$msg,'data'=>$data]);
        exit();
	}
    public function _generate_no_reg($nama_pemohon)
    {

        $first_names = explode(' ',trim($nama_pemohon));
        $first_name = $first_names[0];
        return sprintf('WEB-%s-%s-%s',date('Ymd'),date('Hi'),strtoupper($first_name));
    }
    function _check_nomor_ktp_unique($nomor_ktp){
        
        $check = $this->db->select("COUNT(id) _cx")
                 ->where('nomor_ktp',$nomor_ktp)
                 ->get('sys_pendaftaran')
                 ->row()
                 ->_cx == 0;
        // die($this->db->last_query());         
        return $check;         
    }

    public function print($id,$kode_pendaftaran)
    {
        $row = $this->db->where('id',$id)->get('sys_pendaftaran')->row();
        $data = [
            'row' => $row
        ];
        // die(FCPATH);
        if(!empty($row)){
            $raw_html = $this->load->view('pendaftaran/print',$data,true);
            // die($raw_html);
            $dompdf = new Dompdf();
            $dompdf->loadHtml($raw_html);

            // (Optional) Setup the paper size and orientation
            $dompdf->setPaper('A4');

            // Render the HTML as PDF
            $dompdf->render();

            // Output the generated PDF to Browser
            $dompdf->stream($kode_pendaftaran.".pdf");
        }else{
            die('no records');
        }
    }
	
}