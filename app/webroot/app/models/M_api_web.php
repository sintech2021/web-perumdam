<?php

 
class M_api_web extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();

		$this->client = new \GuzzleHttp\Client();
        $this->end_point = $this->config->item('api_web_endpoint');
        $this->token_end_point = $this->end_point . 'authtimeout/token';
        $this->header = [
            'headers' => []
        ];
	}
    public function _setAuthHeader($token)
    {
    	$this->header['headers']['Authorization'] = $token; 
    }
    public function _getToken()
    {
    	$url = $this->token_end_point;
		$this->response = $this->client->request('GET', $url,$this->header);

        if( $this->response->getStatusCode() == 200){
            $body = $this->response->getBody();
            $obj = json_decode($body);
            return $obj->token;
        }

    	return '';
    }
    /*
	Info tagihan	POSThttps://api-web.perumdamtkr.com/tagihan
	Form data
	Key : kdPelanggan dan tahun	HEADER :
	Key : Authorization
	value: token

    */
    public function get_info_tagihan($kode_pelanggan, $tahun, $token)
    {
    	$url = $this->end_point . 'tagihan';
        $options = [
            'headers' => [
                'Authorization' => $token,
                'Accept'     => 'application/json',
            ],
            'form_params' => [
                'kdPelanggan' => $kode_pelanggan,
                'tahun' => $tahun
            ]
        ];
        // print_r($options);
        $this->response = $this->client->request('POST', $url, $options);

        if( $this->response->getStatusCode() == 200){
            $body = $this->response->getBody();
            return $body;
        }

        return '{status:500}';
    }
    /*
	Info tunggakan	POSThttps://api-web.perumdamtkr.com/tagihan_tunggakan
	Form Date kdPelanggan	HEADER :
	Key : Authorization
	value: token

    */
    public function get_info_tunggakan($kode_pelanggan, $token)
    {
    	$url = $this->end_point . 'tagihan_tunggakan';
        $options = [
            'headers' => [
                'Authorization' => $token,
                'Accept'     => 'application/json',
            ],
            'form_params' => [
                'kdPelanggan' => $kode_pelanggan
            ]
        ];
        // print_r($options);
        $this->response = $this->client->request('POST', $url, $options);

        if( $this->response->getStatusCode() == 200){
            $body = $this->response->getBody();
            return $body;
        }

        return '{status:500}';
    }
    /*
	 Pendaftaran	POSThttps://api-web.perumdamtkr.com/register
	Form Data
	pemohon:pemohon
	pemilik:pemilik
	almtPasang:alamtPasang
	noHp:097893838638
	kdPos:1
	kdProp:2
	kdKab:3
	kdKec:4
	kdKel:5
	photoName:base64photo
	email:email.com
	noKtp:01389265	HEADER :
	Key : Authorization
	value: token
	$nama_pemohon, $nama_pemilik, $alamat, $nomor_hp, $kode_pos, $id_prop, $id_kab, $id_kec, $id_kel, $foto64, $email, $nik
    */
    public function pendaftaran($form_data, $token)
    {
    	# code...
    }
    /*
	Cek Status Pendaftaran	POSThttps://api-web.perumdamtkr.com/info_pendaftaran

		Form Data
	Key : kdDaftar	HEADER :
	Key : Authorization
	value: token

    */
    public function check_status_pendaftaran($kode_pendaftaran, $token)
    {
    	$url = $this->end_point . 'info_pendaftaran';
        $options = [
            'headers' => [
                'Authorization' => $token,
                'Accept'     => 'application/json',
            ],
            'form_params' => [
                'kdDaftar' => $kode_pendaftaran
            ]
        ];
        // print_r($options);
        $this->response = $this->client->request('POST', $url, $options);

        if( $this->response->getStatusCode() == 200){
            $body = $this->response->getBody();
            return $body;
        }

        return '{status:500}';
    }
    public function get_daftar_kabupaten()
    {
    	$url = $this->end_point . 'list_kab';
		$this->response = $this->client->request('GET', $url,$this->header);

        if( $this->response->getStatusCode() == 200){
            $body = $this->response->getBody();
            return json_decode($body);
        }

    	return '';
    }
    public function get_daftar_kecamatan($id_kab)
    {
    	$url = $this->end_point . 'list_kec/?kab=' . $id_kab;
		$this->response = $this->client->request('GET', $url,$this->header);

        if( $this->response->getStatusCode() == 200){
            $body = $this->response->getBody();
            return json_decode($body);
        }

    	return '';
    }
    public function get_daftar_kelurahan($id_kec,$id_kab)
    {
    	$url = $this->end_point . 'list_kel/?kab='.$id_kab.'&kec='.$id_kec;
		$this->response = $this->client->request('GET', $url,$this->header);

        if( $this->response->getStatusCode() == 200){
            $body = $this->response->getBody();
            return json_decode($body);
        }

    	return '';
    }
}