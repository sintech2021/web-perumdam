<?php
// echo $this->Html->docType('html5');
?>
<html lang="en">
  <head>
    <!-- $this->Html->charset();  -->
    <!-- $this->Html->meta() -->
    <?php
    // echo $this->Html->meta(
                // 'favicon.png', '/pdamtkr/favicon.png', array('type' => 'icon')
        // );
    // echo $this->fetch('meta');
    ?>
    <link href="<?=BASE_URL.'/'?>node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"  crossorigin="anonymous">
<link href="<?=BASE_URL.'/'?>node_modules/opensans-npm-webfont/style.css" rel="stylesheet"  crossorigin="anonymous">
<link href="<?=BASE_URL.'/'?>node_modules/opensans-npm-webfont/style.css" rel="stylesheet"  crossorigin="anonymous">
<link href="<?=BASE_URL.'/'?>node_modules/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet"  crossorigin="anonymous">
<link href="<?=BASE_URL.'/'?>node_modules/plyr/dist/plyr.css" rel="stylesheet"  crossorigin="anonymous">
<!-- Custom styles for this template -->
    <link href="<?=BASE_URL.'/'?>styles.css" rel="stylesheet">
    <link href="<?=BASE_URL.'/'?>bootstrap-overide.css" rel="stylesheet">
    <script type="text/javascript" src="<?=BASE_URL.'/'?>jquery-3.5.1.slim.min.js"></script>
    <script type="text/javascript" src="<?=BASE_URL.'/'?>node_modules/axios/dist/axios.min.js"></script>
	<title>
	  <?php echo $this->page_title ?> 
	</title>

    <!-- Bootstrap core CSS -->
    <?php
    $css_ls = array(
        'Sysadmin./bootstrap-3.3.5-dist/css/bootstrap.min',
        'Sysadmin./font-awesome-4.4.0/css/font-awesome.min',
       # 'Sysadmin./bootstrap-3.3.5-dist/css/layout-full',
        'layout-full'
    );
    if(isset($enqueued_css)){
        $css_ls = array_merge($css_ls, $enqueued_css);
    }
    $css_ls[] = "signin";
    
    // echo $this->Html->css($css_ls);
    // echo $this->fetch('css');
    ?>
     
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <?php
      /* echo $this->Html->script('Sysadmin./bootstrap-3.3.5-dist/js/html5shiv.min'); */?>
      <?php
   /* echo $this->Html->script('Sysadmin./bootstrap-3.3.5-dist/js/respond.min');
    echo $this->fetch('script');
    */
      ?>
    <![endif]-->
    <?php
    /*
    echo $this->Html->script(
    array(
        'Sysadmin./bootstrap-3.3.5-dist/js/jquery.min', 
        'Sysadmin./bootstrap-3.3.5-dist/js/bootstrap.min', 
       
        'Sysadmin./bootstrap-3.3.5-dist/js/ie10-viewport-bug-workaround', 
    ), 
    array(
    'block' => 'scriptBottom'
    ));
    */
    ?>
    <script src='https://www.google.com/recaptcha/api.js?hl=id'></script>
  </head>

  <body>
      <!-- php echo $this->Flash->render(); -->
      <!-- php echo $this->fetch('content');  -->
      <?=$this->content?>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <?php
    // echo $this->fetch('scriptBottom');
    ?>
    
  </body>
</html>
