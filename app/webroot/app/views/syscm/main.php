<nav class="navbar bg-light navigation-bar">
<ul class="navbar-nav">
	<li class="nav-item"><a href="<?=site_url()?>">Konten</a>
		<ul class="dropdown">
			<li><a href="<?=site_url().'syscm/syscm_contents_v2/index/berita'?>">Berita</a>
			<li><a href="<?=site_url().'syscm/syscm_contents_v2/index/berita-utama'?>">Berita Utama</a>
			<li><a href="<?=site_url().'syscm/syscm_contents_v2/index/kantor-cabang'?>">Kantor Cabang/Pelayanan</a>
			<li><a href="<?=site_url().'syscm/syscm_contents_v2/index/web-link'?>">Web Link</a>
			<li><a href="<?=site_url().'syscm/syscm_contents_v2/index/web-contact'?>">Web Contact</a>
			<li><a href="<?=site_url().'syscm/syscm_contents_v2/index/mobile-apps'?>">Mobile Apps</a>
			<li><a href="<?=site_url().'syscm/syscm_contents_v2/index/pengumuman'?>">Pengumuman</a>
			<li><a href="<?=site_url().'syscm/syscm_contents_v2/index/galeri-foto'?>">Galeri Foto</a>
			<li><a href="<?=site_url().'syscm/syscm_contents_v2/index/galeri-video'?>">Galeri Video</a>
			<!--
			<li><a href="<?=site_url().'syscm/syscm_contents_v2/index/galeri-video'?>">Galeri Video</a>
			-->	
			
			<li><a href="<?=site_url().'syscm/syscm_contents_v2/index/tentang-kami'?>">Tentang Kami</a>
			<li><a href="<?=site_url().'syscm/syscm_contents_v2/index/layanan-informasi'?>">Layanan Informasi</a>
			<li><a href="<?=site_url().'syscm/syscm_contents_v2/index/laboratorium'?>">Laboratorium</a>
			<li><a href="<?=site_url().'syscm/syscm_contents_v2/index/ppid'?>">PPID</a>
			<li><a href="<?=site_url().'syscm/syscm_contents_v2/index/web'?>">Laman</a>
		</ul>
	</li>
</ul>
</nav>
<div class="content">
	<div id="_root"></div>
	<script type="text/javascript">
		axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
		$(document).ready(function(){

			$('.navigation-bar > ul > li  a').click(function(){
				var href  = this.href;
				var title = $(this).text();
				try{
					var slug  = title.toLowerCase().replace(/\W/,'_');
					if(href != '#'){
						window.history.pushState( { title : title }, slug, href );
						loadContent({ title : title }, slug, href );
					}
					console.log(href)
				}catch(e){
					console.log(e)
				}
				
				return false;
			});
			function loadContent(opt,slug,url){
				axios.get(url).then((res)=>{
					console.log(res);
					$('#_root').html(res.data);
				});
				document.title = opt.title;
				console.log(arguments);
			}
			window.onpopstate = function( event ) {
			  var data = event.state;
			  console.log(data);
			  // data references the first argument of 'pushState'
			  // do whatever showing or hiding of <div>s here
			};
		});

	</script>
</div>

<link href="<?=BASE_URL.'/'?>node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"  crossorigin="anonymous">
<link href="<?=BASE_URL.'/'?>node_modules/opensans-npm-webfont/style.css" rel="stylesheet"  crossorigin="anonymous">
<link href="<?=BASE_URL.'/'?>node_modules/opensans-npm-webfont/style.css" rel="stylesheet"  crossorigin="anonymous">
<link href="<?=BASE_URL.'/'?>node_modules/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet"  crossorigin="anonymous">
<link href="<?=BASE_URL.'/'?>node_modules/plyr/dist/plyr.css" rel="stylesheet"  crossorigin="anonymous">
<!-- Custom styles for this template -->
    <link href="<?=BASE_URL.'/'?>styles.css" rel="stylesheet">
    <link href="<?=BASE_URL.'/'?>bootstrap-overide.css" rel="stylesheet"