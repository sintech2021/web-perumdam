<div style="text-align: center;">
    <img src="pub/img/perumdam-top.png" style="width:auto;"/>
  
</div>
<h2 class="form-title" style="text-align: center;">PERUMDAM Tirta Kerta Raharja</h2>

<table class="table">
  <tbody>
    <tr><th style="text-align: left;vertical-align:top"><label>Nomor Registrasi</label></th><td><label style="padding-left:1em">: <?=$row->kode_pendaftaran?></label></td></tr>
    <tr><th style="text-align: left;vertical-align:top"><label>Nama Pemohon</label></th><td><label style="padding-left:1em">: <?=$row->nama_pemohon?></label></td></tr>
    <tr><th style="text-align: left;vertical-align:top"><label>Nama Pemilik</label></th><td><label style="padding-left:1em">: <?=$row->nama_pemilik?></label></td></tr>
    <tr><th style="text-align: left;vertical-align:top"><label>Nomor KTP</label></th><td><label style="padding-left:1em">: <?=$row->nomor_ktp?></label></td></tr>
    <tr><th style="text-align: left;vertical-align:top"><label>Nomor HP</label></th><td><label style="padding-left:1em">: <?=$row->nomor_hp?></label></td></tr>
    <tr><th style="text-align: left;vertical-align:top"><label>Email</label></th><td><label style="padding-left:1em">: <?=$row->email?></label></td></tr>
    <tr><th style="text-align: left;vertical-align:top"><label>Kabupaten</label></th><td><label style="padding-left:1em">: <?=$row->kabupaten?></label></td></tr>
    <tr><th style="text-align: left;vertical-align:top"><label>Kecamatan</label></th><td><label style="padding-left:1em">: <?=$row->kecamatan?></label></td></tr>
    <tr><th style="text-align: left;vertical-align:top"><label>Kelurahan</label></th><td><label style="padding-left:1em">: <?=$row->kelurahan?></label></td></tr>
    <tr><th style="text-align: left;vertical-align:top"><label>Alamat</label></th><td><label style="padding-left:1em">: <?=$row->alamat?></label></td></tr>
    <tr><th style="text-align: left;vertical-align:top"><label>Kode Pos</label></th><td><label style="padding-left:1em">: <?=$row->kode_pos?></label></td></tr>
    <tr><th style="text-align: left;vertical-align:top"><label>Foto KTP</label></th><td><div style="padding: 1em"><img style="width:200;height:auto;" src="<?='uploads/pendaftaran/'.$row->foto_ktp?>"/></div></td></tr>
  </tbody>
</table>
 