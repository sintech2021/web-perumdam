<?php

Configure::write('Syscm_conf.app.name', 'Syscm');
Configure::write('Syscm_conf.app.desc', 'Sys Content Management');
Configure::write('Syscm_conf.app.build', 'beta');
Configure::write('Syscm_conf.app.ver', '2.0.0');
Configure::write('Syscm_conf.app.copy', '2020');
Configure::write('Syscm_conf.author.name', 'Sintech');
Configure::write('Syscm_conf.author.email', 'sintech.ba@gmail.com');