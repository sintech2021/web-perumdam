<?php
/*Router::connect('/login', array('plugin' => 'sysadmin','controller' => 'sysadmin_app', 'action' => 'login'));
Router::connect('/sysadmin/login', array('plugin' => 'sysadmin','controller' => 'sysadmin_app', 'action' => 'login'));
Router::connect('/sysadmin', array('plugin' => 'sysadmin','controller' => 'sysadmin_app', 'action' => 'index'));
Router::connect('/sysadmin/sys', array('plugin' => 'sysadmin','controller' => 'sysadmin_app','action'=>'index'));
Router::connect('/sysadmin/sys/:action', array('plugin' => 'sysadmin','controller' => 'sysadmin_app')); */
Router::connect('/', array('plugin' => 'syscm','controller' => 'syscm_front', 'action'=> 'index'));
Router::connect('/beranda/*', array('plugin' => 'syscm','controller' => 'syscm_front', 'action'=> 'index'));
#Router::connect('/:action', array('plugin' => 'syscm','controller' => 'syscm_front'));
Router::connect('/web/feedback', array('plugin' => 'syscm','controller' => 'syscm_front', 'action'=>'cust_feedback'));
Router::connect('/web/feedback/*', array('plugin' => 'syscm','controller' => 'syscm_front', 'action'=>'cust_feedback'));
Router::connect('/web/pengaduan', array('plugin' => 'syscm','controller' => 'syscm_front', 'action'=>'cust_feedback'));
Router::connect('/web/pengaduan/*', array('plugin' => 'syscm','controller' => 'syscm_front', 'action'=>'cust_feedback'));
Router::connect('/web/*', array('plugin' => 'syscm','controller' => 'syscm_front', 'action'=>'front_pages'));
Router::connect('/topik/*', array('plugin' => 'syscm','controller' => 'syscm_front', 'action'=>'front_categories'));
Router::connect('/kategori/*', array('plugin' => 'syscm','controller' => 'syscm_front', 'action'=>'front_feat_categories'));
Router::connect('/search/*', array('plugin' => 'syscm','controller' => 'syscm_front', 'action'=>'front_search'));
Router::connect('/pendaftaran', array('plugin' => 'syscm','controller' => 'syscm_front', 'action'=>'cust_register'));
Router::connect('/pendaftaran/*', array('plugin' => 'syscm','controller' => 'syscm_front', 'action'=>'cust_register'));
Router::connect('/tagihan', array('plugin' => 'syscm','controller' => 'syscm_front', 'action'=>'cust_bill'));
Router::connect('/tagihan/*', array('plugin' => 'syscm','controller' => 'syscm_front', 'action'=>'cust_bill'));
Router::connect('/data/*', array('plugin' => 'syscm','controller' => 'syscm_front', 'action'=>'data'));
Router::connect('/register_check', array('plugin' => 'syscm','controller' => 'syscm_front', 'action'=>'register_check'));
Router::connect('/register_check/*', array('plugin' => 'syscm','controller' => 'syscm_front', 'action'=>'register_check'));
Router::connect('/tunggakan', array('plugin' => 'syscm','controller' => 'syscm_front', 'action'=>'tunggakan'));
Router::connect('/tunggakan/*', array('plugin' => 'syscm','controller' => 'syscm_front', 'action'=>'tunggakan'));

Router::connect('/pengumuman', array('plugin' => 'syscm','controller' => 'syscm_front', 'action'=>'pengumuman'));
Router::connect('/pengumuman/*', array('plugin' => 'syscm','controller' => 'syscm_front', 'action'=>'pengumuman'));
Router::connect('/read/*', array('plugin' => 'syscm','controller' => 'syscm_front', 'action'=>'read'));
Router::connect('/r/*', array('plugin' => 'syscm','controller' => 'syscm_front', 'action'=>'read'));
Router::connect('/arsip/*', array('plugin' => 'syscm','controller' => 'syscm_front', 'action'=>'arsip'));
Router::connect('/galeri/*', array('plugin' => 'syscm','controller' => 'syscm_front', 'action'=>'galeri'));
Router::connect('/faq/*', array('plugin' => 'syscm','controller' => 'syscm_front', 'action'=>'pojok_konsultasi'));
Router::connect('/detail_reg/*', array('plugin' => 'syscm','controller' => 'syscm_front', 'action'=>'detail_reg'));
// Router::connect('/kontak-kami', array('plugin' => 'syscm','controller' => 'syscm_front', 'action'=>'web/kontak_kami'));