<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('SyscmAppModel', 'Syscm.Model');

/**
 * CakePHP ScmMenuGroup
 * @author ino
 */
class ScmMenuGroup extends SyscmAppModel
{

    /**
     *
     * @var type 
     */
    public $menuGroupName = 'menu_group';
    /**
     *
     * @var type 
     */
    public $useTable     = "scm_menus";
    public $displayField = "name";
    public $validate     = array(
        'name' => array(
            'Group name can only be letters,numbers, dash and underscore' => array(
                'rule'       => 'alphaNumericDashUnderscore',
                'required'   => true,
                'allowEmpty' => false,
            ),
            'Duplicated name!' => array(
                 'rule'       => 'nameUnique',
                'required'   => true,
                'allowEmpty' => false,
            )
        ),
    );

    /**
     * Associations
     */
    
    public $hasAndBelongsToMany = array(
        'ScmMenuList' =>
            array(
                'className' => 'Syscm.ScmMenuList',
                'joinTable' => 'scm_menus_details',
                'foreignKey' => 'field',
                'associationForeignKey' => 'value',
                'unique' => true,
                'conditions' => '',
                'fields' => '',
                'order' => '',
                'limit' => '',
                'offset' => '',
                'finderQuery' => '',
                'with' => 'Syscm.ScmMenuListGroup'
            )
    );
    
    /**
     * 
     */
    public function dropStatus($id)
    {

        $result = false;
        if ($this->exists($id)) {
           
            $this->id       = $id;
            $data           = array(
                $this->alias => array(
                    'active_status' => '0'
                )
            );
            $result         = $this->save($data, false);
        }
    }

    /**
     * 
     */
    public function findActiveList()
    {
        $options = array(
            'conditions' => array(
                'ScmMenuGroup.active_status' => '1',
                'ScmMenuGroup.menu_code'            => $this->menuGroupName,
            )
        );
        $this->virtualFields = array('list_value'=> "CONCAT(text,' (', name,')')");
        $this->displayField = 'list_value';
        return $this->find('list', $options);
    }

    /**
     * 
     */
    public function findFirstById($id)
    {
        $options = array(
            'conditions' => array(
                'ScmMenuGroup.active_status' => '1',
                'ScmMenuGroup.id'            => $id,
            )
        );
        return $this->find('first', $options);
    }

    /**
     * 
     */
    public function getInvokeTopMenuEdit($id)
    {
        $plugin_alias     = 'syscm';
        $controller_alias = 'syscm_menus';

        $name = $this->field('name', array('id' => $id));

        $invoke = array(
            array(
                'perm'  => 'edit',
                'index' => '2',
                'alias' => 'edit',
                'menu'  => array(
                    'title'   => '<span class="fa fa-pencil"></span> ' . __d('sysadmin',
                            'Edit group'),
                    'url'     => array(
                        "plugin"     => $plugin_alias,
                        "controller" => $controller_alias,
                        "action"     => "edit",
                        $id),
                    'options' => array('escape' => false),
                    'active'  => false,
                ),
            ),
            array(
                'perm'  => 'delete',
                'index' => '3',
                'alias' => 'delete',
                'menu'  => array(
                    'link_type' => 'postLink',
                    'title'     => '<span class=" text-danger"><span class="fa fa-trash text-danger"></span>  ' . __d('sysadmin',
                            'Delete group') . '</span>',
                    'url'       => array(
                        "plugin"     => $plugin_alias,
                        "controller" => $controller_alias,
                        "action"     => "delete",
                        $id),
                    'options'   => array(
                        'confirm' => __d('sysadmin',
                                'Are you sure you want to delete # %s?',
                                h($name)),
                        'escape'  => false
                    ),
                    'active'    => false,
                ),
            )
        );

        return $invoke;
    }

    /**
     * 
     */
    public function setTopMenu($request, $SysAclView, $invoke)
    {
        $index    = array();
        $add      = array();
        $list     = array();
        $menu_add = array();

        $plugin_alias     = 'syscm';
        $controller_alias = 'syscm_menus';

        if ($SysAclView['browse']) {
            $index = array(
                'title'   => '<span class="fa fa-list fa-fw"></span> ' . __d('syscm',
                        'Index'),
                'url'     => array(
                    "plugin"     => $plugin_alias,
                    "controller" => $controller_alias,
                    "action"     => "index"),
                'options' => array('escape' => false),
                'active'  => false,
            );
            $list  = array(
                'title'   => '<span class="fa fa-list fa-fw"></span> ' . __d('syscm',
                        'Menu list'),
                'url'     => array(
                    "plugin"     => $plugin_alias,
                    "controller" => $controller_alias,
                    "action"     => "menu_list"),
                'options' => array('escape' => false),
                'active'  => false,
            );
        }
        if ($SysAclView['add']) {
            $add      = array(
                'title'   => '<span class="fa fa-plus fa-fw"></span> ' . __d('syscm',
                        'Add menu group'),
                'url'     => array(
                    "plugin"     => $plugin_alias,
                    "controller" => $controller_alias,
                    "action"     => "add"),
                'options' => array('escape' => false),
                'active'  => false,
            );
            $menu_add = array(
                'title'   => '<span class="fa fa-plus fa-fw"></span> ' . __d('syscm',
                        'Add menu'),
                'url'     => array(
                    "plugin"     => $plugin_alias,
                    "controller" => $controller_alias,
                    "action"     => "menu_add"),
                'options' => array('escape' => false),
                'active'  => false,
            );
        }
        switch ($request['action']) {
            case 'index':
                $index['url']       = '#';
                $index['active']    = true;
                break;
            case 'add':
                $add['url']         = '#';
                $add['active']      = true;
                break;
            case 'menu_list':
                $list['url']        = '#';
                $list['active']     = true;
                break;
            case 'menu_add':
                $menu_add['url']    = '#';
                $menu_add['active'] = true;
                break;
            default:
                #$index['url'] = '#';
                #$index['active']    = true;
                break;
        }

        $top_menu = array(
            $index,
            $add,
            $list,
            $menu_add,
        );

        if ($invoke) {
            foreach ($invoke as $i) {
                if ($SysAclView[$i['perm']]) {
                    $new = array();
                    $n   = 0;
                    foreach ($top_menu as $ii => $v) {

                        if ($request['action'] == $i['alias']) {
                            $i['menu']['url']    = '#';
                            $i['menu']['active'] = true;
                        }
                        if ($i['index'] == $n) {
                            $new[$n] = $i['menu'];
                        }
                        $new[] = $v;
                        $n++;
                    }
                    while($i['index'] >= $n){
                        $new[] = $i['menu'];
                        $n++;
                    }
                    $top_menu = $new;
                }
            }
        }

        return $top_menu;
    }

    
    /**
     * validations
     * @param type $check
     * @return type
     */
    public function alphaNumericDashUnderscore($check)
    {
        $value = array_values($check);
        $value = $value[0];

        return preg_match('|^[0-9a-zA-Z_-]*$|', $value);
    }

    /**
     * 
     */
    public function nameUnique()
    {
        $conditions = array(
            $this->alias.'.name'          => $this->data[$this->alias]['name'],
            $this->alias.'.active_status' => 1,
        );
        if (isset($this->data[$this->alias]['id'])) {
            $conditions['id !='] = $this->data[$this->alias]['id'];
        }
        $existing = $this->find('first',
                array(
            'conditions' => $conditions
        ));
        return (count($existing) == 0);
    }
}
