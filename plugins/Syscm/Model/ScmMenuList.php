<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('SyscmAppModel', 'Syscm.Model');

/**
 * CakePHP ScmMenuGroup
 * @author ino
 */
class ScmMenuList extends SyscmAppModel
{

    public $useTable     = "scm_menus";
    public $displayField = "name";
    public $alias = "ScmMenuList";
    public $menuListName = "menu_list";
    public $validate     = array(
        'name' => array(
            'Group name can only be letters,numbers, dash and underscore' => array(
                'rule'       => 'alphaNumericDashUnderscore',
                'required'   => true,
                'allowEmpty' => false,
            ),
            'Duplicated name!' => array(
                 'rule'       => 'nameUnique',
                'required'   => true,
                'allowEmpty' => false,
            )
        ),
    );

    /**
     * Associations
     */
    public $belongsTo = array(
        'ParentMenu' => array(
           
            'className'  => 'Syscm.ScmMenuList',
            'foreignKey' => 'parent_id',
            'conditions' => array(
                'ParentMenu.active_status'=> '1',
                'ParentMenu.menu_code'=> 'menu_list',
                ),
            'fields'     => '',
            'order'      => ''
        
        )
    );

    public $hasOne = array(
        'ScmMenuSlug' => array(
            'className' => 'Syscm.ScmMenuSlug',
            'foreignKey' => 'scm_menu_id',
            'conditions' => array('ScmMenuSlug.menu_code'=> 'menu_slug'),
        ),
    );
    
    public $hasAndBelongsToMany = array(
        'ScmMenuGroup' =>
            array(
                'className' => 'Syscm.ScmMenuGroup',
                'joinTable' => 'scm_menus_details',
                'foreignKey' => 'value',
                'associationForeignKey' => 'field',
                'unique' => true,
               'conditions' => '',
                'fields' => '',
                'order' => '',
                'limit' => '',
                'offset' => '',
                'finderQuery' => '',
                'with' => 'Syscm.ScmMenuListGroup'
            )
    );
    
    /**
     * 
     */
    public function dropStatus($id)
    {

        $result = false;
        if ($this->exists($id)) {
           
            $this->id       = $id;
            $data           = array(
                $this->alias => array(
                    'active_status' => '0',
                    'id' => $id,
                ),
                'ScmMenuSlug' => array(
                    'active_status' => '0',
                    'scm_menu_id' => $id,
                )
            );
            $result         = $this->save($data, array('validate'=>false));
            
        }

        return $result;
    }
/**
     * 
     */
    public function findActiveList($exclude_ids = array(), $useTree = false)
    {
        $active_status = $this->alias.'.active_status';
        $menu_code = $this->alias.'.menu_code';
        $options = array(
            'conditions' => array(
                'AND' => array(
                    $active_status => '1',
                    $menu_code           => $this->menuListName,
                    
                )
                
            )
        );
        if($exclude_ids){
            $options['conditions']['AND']['NOT'][$this->alias.'.'.$this->primaryKey] = $exclude_ids;
        }
        
        $this->virtualFields = array('list_value'=> "CONCAT(".$this->alias.".text,' (', ".$this->alias.".name,')')");
        $this->displayField = 'list_value';
        if($useTree){
            
           $this->Behaviors->load('Tree');
           $this->recover();
           return $this->generateTreeList($options['conditions']);
        }
        return $this->find('list', $options);
    }
    /**
     * 
     */
    public function findFirstById($id)
    {
        $active_status = $this->alias.'.active_status';
        $primary_key = $this->alias.'.'.$this->primaryKey;
        $options = array(
            'conditions' => array(
               $active_status  => '1',
               $primary_key    => $id,
            )
        );
        return $this->find('first', $options);
    }

    /**
     * 
     */
    public function getInvokeTopMenuEdit($id)
    {
        $plugin_alias     = 'syscm';
        $controller_alias = 'syscm_menus';

        $name = $this->field('name', array('id' => $id));

        $invoke = array(
            array(
                'perm'  => 'edit',
                'index' => '4',
                'alias' => 'menu_edit',
                'menu'  => array(
                    'title'   => '<span class="fa fa-pencil"></span> ' . __d('sysadmin',
                            'Edit menu'),
                    'url'     => array(
                        "plugin"     => $plugin_alias,
                        "controller" => $controller_alias,
                        "action"     => "menu_edit",
                        $id),
                    'options' => array('escape' => false),
                    'active'  => false,
                ),
            ),
            array(
                'perm'  => 'delete',
                'index' => '5',
                'alias' => 'menu_delete',
                'menu'  => array(
                    'link_type' => 'postLink',
                    'title'     => '<span class=" text-danger"><span class="fa fa-trash text-danger"></span>  ' . __d('sysadmin',
                            'Delete menu') . '</span>',
                    'url'       => array(
                        "plugin"     => $plugin_alias,
                        "controller" => $controller_alias,
                        "action"     => "menu_delete",
                        $id),
                    'options'   => array(
                        'confirm' => __d('sysadmin',
                                'Are you sure you want to delete # %s?',
                                h($name)),
                        'escape'  => false
                    ),
                    'active'    => false,
                ),
            )
        );

        return $invoke;
    }

    /**
     * 
     */
    public function getTreeByRs($rs){
        
        if(!$rs){
            return array();
        }
        $ids = array();
        foreach($rs as $i => $v){
            if($v[$this->alias]){
                $ids[] = $v[$this->alias][$this->primaryKey];
            }
        }
        if(!$ids){
            return array();
        }
        $this->Behaviors->load('Tree');
        $this->recover();
        return $this->generateTreeList(array($this->alias.'.'.$this->primaryKey => $ids));
        
    }
    /**
     * 
     */
    public function setTopMenu($request, $SysAclView, $invoke)
    {
        $index    = array();
        $add      = array();
        $list     = array();
        $menu_add = array();

        $plugin_alias     = 'syscm';
        $controller_alias = 'syscm_menus';

        if ($SysAclView['browse']) {
            $index = array(
                'title'   => '<span class="fa fa-list fa-fw"></span> ' . __d('sysadmin',
                        'Index'),
                'url'     => array(
                    "plugin"     => $plugin_alias,
                    "controller" => $controller_alias,
                    "action"     => "index"),
                'options' => array('escape' => false),
                'active'  => false,
            );
            $list  = array(
                'title'   => '<span class="fa fa-list fa-fw"></span> ' . __d('sysadmin',
                        'Menu list'),
                'url'     => array(
                    "plugin"     => $plugin_alias,
                    "controller" => $controller_alias,
                    "action"     => "menu_list"),
                'options' => array('escape' => false),
                'active'  => false,
            );
        }
        if ($SysAclView['add']) {
            $add      = array(
                'title'   => '<span class="fa fa-plus fa-fw"></span> ' . __d('sysadmin',
                        'Add menu group'),
                'url'     => array(
                    "plugin"     => $plugin_alias,
                    "controller" => $controller_alias,
                    "action"     => "add"),
                'options' => array('escape' => false),
                'active'  => false,
            );
            $menu_add = array(
                'title'   => '<span class="fa fa-plus fa-fw"></span> ' . __d('sysadmin',
                        'Add menu'),
                'url'     => array(
                    "plugin"     => $plugin_alias,
                    "controller" => $controller_alias,
                    "action"     => "menu_add"),
                'options' => array('escape' => false),
                'active'  => false,
            );
        }
        switch ($request['action']) {
            case 'index':
                $index['url']       = '#';
                $index['active']    = true;
                break;
            case 'add':
                $add['url']         = '#';
                $add['active']      = true;
                break;
            case 'menu_list':
                $list['url']        = '#';
                $list['active']     = true;
                break;
            case 'menu_add':
                $menu_add['url']    = '#';
                $menu_add['active'] = true;
                break;
            default:
                #$index['url'] = '#';
                #$index['active']    = true;
                break;
        }

        $top_menu = array(
            $index,
            $add,
            $list,
            $menu_add,
        );

        if ($invoke) {
            foreach ($invoke as $i) {
                if ($SysAclView[$i['perm']]) {
                    $new = array();
                    $n   = 0;
                    foreach ($top_menu as $ii => $v) {

                        if ($request['action'] == $i['alias']) {
                            $i['menu']['url']    = '#';
                            $i['menu']['active'] = true;
                        }
                        if ($i['index'] == $n) {
                            $new[$n] = $i['menu'];
                        }
                        $new[] = $v;
                        $n++;
                    }
                    $top_menu = $new;
                }
            }
        }

        return $top_menu;
    }

    
    /**
     * validations
     * @param type $check
     * @return type
     */
    public function alphaNumericDashUnderscore($check)
    {
        $value = array_values($check);
        $value = $value[0];

        return preg_match('|^[0-9a-zA-Z_-]*$|', $value);
    }

    /**
     * 
     */
    public function nameUnique()
    {
        $conditions = array(
            $this->alias.'.name'          => $this->data[$this->alias]['name'],
            $this->alias.'.active_status' => 1,
        );
        if (isset($this->data[$this->alias]['id'])) {
            $conditions[$this->alias.'.id !='] = $this->data[$this->alias]['id'];
        }
        $existing = $this->find('first',
                array(
            'conditions' => $conditions
        ));
        #debug($this->data);die();
        return (count($existing) == 0);
    }
}
