<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('SyscmAppModel', 'Syscm.Model');

/**
 * CakePHP FrontContent
 * @author ino
 */
class FrontContent extends SyscmAppModel
{
    /*
     * 
     */

    public $useTable             = "scm_contents";
    public $displayField         = "name";
    public $alias                = "FrontContent";
    public $published            = "Published";
    public $content_web          = "content_web";
    public $mainlistParentName   = "/topik/berita";
    public $featContName         = "berita_utama";
    public $announcementContName = "pengumuman";
    public $videoGalleryCatName  = "gallery_video";
    public $photoGalleryCatName  = "photo_gallery";
    public $homepagePopupCatName  = "homepage_popup";

    /**
     * Assoc
     */
    public $hasOne    = array(
        'ScmContentContent'   => array(
            'className'  => 'Syscm.ScmContentContent',
            'foreignKey' => 'scm_content_id',
            'conditions' => array(
                'ScmContentContent.active_status' => '1',
                'ScmContentContent.field'         => 'ScmContentContent',
            ),
            'fields'     => '',
        ),
        'ScmContentPublished' => array(
            'className'  => 'Syscm.ScmContentPublished',
            'foreignKey' => 'scm_content_id',
            'conditions' => array(
                'ScmContentPublished.active_status' => '1',
                'ScmContentPublished.field'         => 'ScmContentPublished',
            ),
            'fields'     => '',
        ),
        'ScmContentMeta'      => array(
            'className'  => 'Syscm.ScmContentMeta',
            'foreignKey' => 'scm_content_id',
            'conditions' => array(
                'ScmContentMeta.active_status' => '1',
                'ScmContentMeta.field'         => 'ScmContentMeta',
            ),
            'fields'     => '',
        ),
       
    );
    public $belongsTo = array(
        'ParentContent' => array(
            'className'  => 'Syscm.FrontContent',
            'foreignKey' => 'parent_id',
            'conditions' => array(
                'ParentContent.active_status' => '1',
                'ParentContent.menu_code'     => 'content_web',
            ),
            'fields'     => '',
            'order'      => ''
        )
    );
    public $hasMany   = array(
        'ChildContent' => array(
            'className'  => 'Syscm.FrontContent',
            'foreignKey' => 'parent_id',
            'conditions' => array(
                'ChildContent.active_status' => '1',
                'ChildContent.menu_code'     => 'content_web',
            ),
            'fields'     => '',
            'order'      => ''
        )
    );
    public $include_content = false;
    /**
     * 
     */
    public function includeContent($state)
    {
        $this->include_content = $state;
    }
    public function appendScmContentsCategory()
    {
        $this->hasOne['ScmContentsCategory']=  array(
            'className'  => 'Syscm.ScmContentsCategory',
            'foreignKey' => 'scm_content_id',
           
            'fields'     => '',
        );
    }
    public function findFirstByName($name, $deep_assoc = false)
    {
        $options                                                        = array();
        $options['conditions']['AND'] [$this->alias . '.active_status'] = '1';
        $options['conditions']['AND'] [$this->alias . '.name']          = $name;
        if (!$deep_assoc) {
            $this->recursive = -1;
        }
        return $this->find('first', $options);
    }

    /**
     * 
     */
    public function getContentsByCatname($cat_names = array(), $recursive = 0,$find_method = 'all',$order=array())
    {
        
        if (!$cat_names) {
            return array();
        }

        $Category = ClassRegistry::init('Syscm.ScmCategory');
        if ($recursive) {
            $Category->recursive = $recursive;
        }
        $options = array();
		if(!empty($order)){
					$options['order'] =$order ;
				}
        $options['conditions']['AND']['ScmCategory.active_status'] = '1';
        $options['conditions']['AND']['ScmCategory.name']          = $cat_names;
		
        return $Category->find($find_method, $options);
    }

    /**
     * 
     */
    public function getExcerpt($contents, $word_count = 35, $more = "...",
            $options = array())
    {
        $stripped     = filter_var($contents, FILTER_SANITIZE_STRING,
                FILTER_SANITIZE_STRIPPED);
        $words        = str_word_count($stripped, 2);
        $words_indexs = array_keys($words);

        if (isset($words_indexs[$word_count])) {
            $sub_max = $words_indexs[$word_count];
        }
        else {
            $sub_max = end($words_indexs);
            $more    = "";
        }
        return substr($stripped, 0, $sub_max) . $more;
    }

    public function getFeaturedContents()
    {
        $res = array();
         $rs  = $this->getContentsByCatname($this->featContName, 2,'all',array("modified"=>"desc"));
        if ($rs) {
            $i = count($rs);
			
            foreach ($rs as $v) {
                if (isset($v['ScmContent'])) {
					$a = count($v['ScmContent']);
                    foreach ($v['ScmContent'] as $cont) {
                        $contents = isset($cont['ScmContentContent']) ? $cont['ScmContentContent']['value']
                                    : "";

                        $res[$a] = array(
                            'url'     => $cont['name'],
                            'id'     => $cont['id'],
                            'title'   => $cont['text'],
                            'summary' => $this->getExcerpt($contents, 45),
                            'img_url' => $this->getFirstImg($contents,
                                    '/pdamtkr/banner.png'),
                        );
                        $a--;
                    }
                }
            }
        }
        return array_reverse($res);
    }

    public function getFirstImg($the_content, $default = "/pdamtkr/logo.png")
    {
        $img_src     = $default;
        $startstrpos = '<img';
        $stoptstrpos = '/>';
        $src         = 'src="';

        $foundpos = strpos($the_content, $startstrpos);
        if ($foundpos):
            $next_content = substr($the_content, $foundpos);
            $foundsrc     = strpos($next_content, $src);
            $next_content = substr($next_content, $foundsrc + strlen($src));
            $foundquoted  = strpos($next_content, '"');
            $img_src      = substr($next_content, 0, $foundquoted);

        endif;
        return $img_src;
    }

    /**
     * 
     */
    public function getFormattedArticle($syscm_contents)
    {

        extract($syscm_contents);
        $article             = array();
        $article['title']    = isset($ScmContent['text']) ? $ScmContent['text'] : "";
        $article['author']   = isset($ScmContentPublished['name']) ? $ScmContentPublished['name']
                    : "";
        $article['subtitle'] = "";
        $article['date']     = isset($ScmContentPublished['text']) ? $ScmContentPublished['text']
                    : "";
        $article['time']     = isset($ScmContentPublished['value']) ? $ScmContentPublished['value']
                    : "";

        $article['url']     = isset($ScmContent['name']) ? $ScmContent['name'] : "";
        $article['content'] = isset($ScmContentContent['value']) ? $ScmContentContent['value']: "";
        $article['id'] =   $ScmContentContent['scm_content_id'];
                    
        $article['dt']    = $this->format_dt($article['date'],$article['time']);
        return $article;
    }

    /**
     * 
     */
    public function getFormattedMainlist($rs)
    {
        $list = array();
        foreach ($rs as $i => $v) {
            $list[$i] ['url']     = $v[$this->alias]['name'];
            $list[$i] ['title']   = $v[$this->alias]['text'];
            $list[$i] ['content'] = $this->fixSnippet(trim(str_replace('&nbsp;',' ', substr(strip_tags($v['ScmContentContent']['value']),0,150)))) . '...';
            if($this->include_content)
                $list[$i] ['full_content'] = $v['ScmContentContent']['value'];

            $list[$i] ['date']    = $v['ScmContentPublished']['text'];
            $list[$i] ['time']    = $v['ScmContentPublished']['value'];
            $list[$i] ['dt']    = $this->format_dt($list[$i] ['date'],$list[$i] ['time']);
            $list[$i] ['bgurl']      = $this->getFirstImg( $v['ScmContentContent']['value']);
            $list[$i] ['annotation']      = $v['ScmContentContent']['annotation'];
            $list[$i] ['content_id']      = $v['ScmContentContent']['id'];

            $list[$i] ['author']  = $v['ScmContentPublished']['name'];
        }
        return array('list' => $list);
    }
    public function format_dt($dt,$tt)
    {
        $hari = array ( 1 =>    'Senin',
                'Selasa',
                'Rabu',
                'Kamis',
                'Jumat',
                'Sabtu',
                'Minggu'
            );
            
        $bulan = array (1 =>   'Januari',
                    'Februari',
                    'Maret',
                    'April',
                    'Mei',
                    'Juni',
                    'Juli',
                    'Agustus',
                    'September',
                    'Oktober',
                    'November',
                    'Desember'
                );
        $strpubdate = strtotime($dt);
        $postday    =  date("d", $strpubdate) ;
        $postdate_l   = date("N", $strpubdate);
        $postdate   = $hari[$postdate_l];
        $postmonth_n = date('n',$strpubdate);
        $postmonth  = $bulan[$postmonth_n];//__d('syscm', date("F", $strpubdate));
        $postyear   = date("Y", $strpubdate);
        $posttime   = isset($tt) ? sprintf('%s ',
                        $tt) : '';
        return $post_date  = sprintf(
                '%s ' .
               
                '%s ' .
                '%s, ' .
                '%s ' ,  $postday,  $postmonth, $postyear, $posttime);
    }
    function fixSnippet($text){
        $words = explode(' ',$text);
        array_pop($words);
        return implode(' ', $words);
    }
    public function getFormattedMainlist_v2($rs)
    {
      
        $list = array();
        foreach ($rs as $i => $v) {
            $list[$i] ['url']     = $v[$this->alias]['name'];
            $list[$i] ['title']   = $v[$this->alias]['text'];
            $list[$i] ['id']   = $v['ScmContentContent']['scm_content_id'];
            if($this->include_content){
                $list[$i] ['content'] = $v['ScmContentContent']['value'];
            }else{
                $list[$i] ['content'] = $this->fixSnippet(trim(str_replace('&nbsp;',' ', substr(strip_tags($v['ScmContentContent']['value']),0,150)))) . '...';
            }
            
            $list[$i] ['date']    = $v['ScmContentPublished']['text'];
            $list[$i] ['time']    = $v['ScmContentPublished']['value'];
            $list[$i] ['bgurl']    = $this->getFirstImg($v['ScmContentContent']['value']);
            $list[$i] ['dt']      = $this->format_dt($list[$i] ['date'] ,$list[$i] ['time'] );
            $list[$i] ['author']  = $v['ScmContentPublished']['name'];
        }
        return array('list' => $list);
    }
    /**
     * 
     */
    public function getAnnouncements()
    {
        $res = array();
        $rs  = $this->getContentsByCatname($this->announcementContName, 0,
                'first');
        // print_r($rs);
        // die();

        if (!$rs) {
            return $res;
        }
        if (!isset($rs['ScmContent'])) {
            return $res;
        }
        $res ['title'] = Inflector::humanize($this->announcementContName);
        $res ['url']   = Router::url(array('plugin'     => 'syscm', 'controller' => 'syscm_front',
                    'action'     => 'front_feat_categories', $this->announcementContName),
                        true);
        $res ['list']  = array();
        foreach ($rs['ScmContent'] as $i => $v) {
            $res ['list'][$i]['title'] = $v['text'];
            $res ['list'][$i]['url']   = $v['name'];
            $res ['list'][$i]['id']    = $v['id'];
        }

        return $res;
    }

    /**
     * 
     */
    public function getCatlistPaginateConditions($catname)
    {
        $conditions = array();

        $conditions['AND'][$this->alias . '.active_status'] = '1';
        $conditions['AND'][$this->alias . '.menu_code']     = $this->content_web;
        $conditions['AND']['ParentContent.name']            = $catname;

        return $conditions;
    }

    /**
     * 
     */
    public function getCatlistPaginateSettings()
    {
        return array('limit' => 10, 'order' => array(
                'ScmContentPublished.text'   => 'desc',
                'ScmContentPublished.value'  => 'desc',
                $this->alias . '.menu_order' => 'asc',
        ));
    }

    /**
     * 
     */
    public function getIframeSrc($the_content, $default = "")
    {
        $iframe_src     = $default;
        $startstrpos = '<iframe';
        $stoptstrpos = '/>';
        $src         = 'src="';

        $foundpos = strpos($the_content, $startstrpos);
        if ($foundpos){
            $next_content = substr($the_content, $foundpos);
            $foundsrc     = strpos($next_content, $src);
            $next_content = substr($next_content, $foundsrc + strlen($src));
            $foundquoted  = strpos($next_content, '"');
            $iframe_src      = substr($next_content, 0, $foundquoted);
        }
        return $iframe_src;
    }

    /**
     * 
     */
    public function getHomepagePopup()
    {
        // $res = null;
        // $content = $this->getContentsByCatname($this->homepagePopupCatName, 0,
        //         'first');

        // return $content;
        
        $sikon = [
            'ScmCategory.name' =>  'homepage_popup'
        ];
        $Category = ClassRegistry::init('Syscm.ScmCategory');
        $Category->recursive = -1;
        $category = $Category->find('first',['conditions'=>$sikon]);
        // print_r($category);
        // die();
        if(!empty($category)){
            $ScmContent = ClassRegistry::init('Syscm.ScmContent');
            $conditions = [
                'ScmContent.active_status' => 1
            ];
            $conditions['AND']['ScmContentsCategory.scm_category_id'] = $category['ScmCategory']['id'];
            $content = $ScmContent->find('first',['conditions'=>$conditions,'limit' => 1,
                'order' => 'DATE(ScmContentPublished.text) DESC']);
            return $content;
            // if(!$content){
            // return $res;
            // }
            // if(!isset($content['ScmContent'])){
            // return $res;
            // }
            // if(!isset($content['ScmContent'][0])){
            // return $res;
            // }
            // if(!isset($content['ScmContent'][0]['ScmContentContent'])){
            // return $res;
            // }
            // if(!isset($content['ScmContent'][0]['ScmContentContent']['value'])){
            // return $res;
            // }
            // $res = $content['ScmContent'][0]['ScmContentContent']['value'];

            // return $res;
        }
        return null;
    }
    public function getImageGallery_v2()
    {
        $res = array(
            'view_element' => 'gallerylist-right',
            'url'          => '#',
            'title'        => '',
            'fa_icon'        => 'fa-image',
            'list'         => array()
        );
        $ls  = $this->getContentsByCatname($this->photoGalleryCatName, 0,
                'first');
        if (!$ls) {
            return $res;
        }
        $res['title'] = $ls['ScmCategory']['text'];
        if (!isset($ls['ScmContent'])) {
            return $res;
        }

        if (!$ls['ScmContent']) {
            return $res;
        }
        foreach ($ls['ScmContent'] as $i => $v) {
            $img_src = $this->getFirstImg($v['ScmContentContent']['value'], false);
            if(!$img_src){
                continue;
            }
            // $img = sprintf('<img src="%1$s" class="image 12u" />', $img_src);
            $res['list'][$i]['content'] =  $img_src;
            $res['list'][$i]['title'] =  $v['ScmContentContent']['text'];
        }
        shuffle($res['list']);

        return $res;
    }
    /**
     * 
     */
    public function getImageGallery()
    {
        $res = array(
            'view_element' => 'gallerylist-right',
            'url'          => '#',
            'title'        => '',
            'fa_icon'        => 'fa-image',
            'list'         => array()
        );
        $ls  = $this->getContentsByCatname($this->photoGalleryCatName, 0,
                'first');
        if (!$ls) {
            return $res;
        }
        $res['title'] = $ls['ScmCategory']['text'];
        if (!isset($ls['ScmContent'])) {
            return $res;
        }

        if (!$ls['ScmContent']) {
            return $res;
        }
        foreach ($ls['ScmContent'] as $i => $v) {
            $img_src = $this->getFirstImg($v['ScmContentContent']['value'], false);
            if(!$img_src){
                continue;
            }
            $img = sprintf('<img src="%1$s" class="image 12u" />', $img_src);
            $res['list'][$i]['content'] = $img;
        }
        shuffle($res['list']);

        return $res;
    }
    /**
     * 
     */
    public function getMainlistPaginateConditions()
    {
        $conditions = array();

        $conditions['AND'][$this->alias . '.active_status'] = '1';
        $conditions['AND'][$this->alias . '.menu_code']     = $this->content_web;
        $conditions['AND']['ParentContent.name']            = $this->mainlistParentName;

        return $conditions;
    }

    public function getMainlistPaginateConditions_v2($conditions = array())
    {
        return $conditions;
    }
    public function getMainlistPaginateSettings_v2($conditions = [])
    {
        return $conditions;
    }
    /**
     * 
     */
    public function getMainlistPaginateSettings()
    {
        return array('limit' => 5, 'order' => array(
                'ScmContentPublished.text'   => 'desc',
                'ScmContentPublished.value'  => 'desc',
                $this->alias . '.menu_order' => 'desc',
        ));
    }

    /**
     * 
     */
    public function getVideoGallery()
    {
        $res = array(
            'view_element' => 'gallerylist-right',
            'url'          => '#',
            'title'        => '',
            'fa_icon'        => 'fa-film',
            'list'         => array()
        );
        $ls  = $this->getContentsByCatname($this->videoGalleryCatName, 0,
                'first');
        if (!$ls) {
            return $res;
        }
        $res['title'] = $ls['ScmCategory']['text'];
        if (!isset($ls['ScmContent'])) {
            return $res;
        }

        if (!$ls['ScmContent']) {
            return $res;
        }
        foreach ($ls['ScmContent'] as $i => $v) {
            $iframe_src = $this->getIframeSrc($v['ScmContentContent']['value']);
            $iframe = sprintf('<iframe src="%1$s" class="12u image" allowfullscreen="allowfullscreen"><a href="%1$s" target="_blank">View Video</a></iframe>', $iframe_src);
            $res['list'][$i]['content'] = $iframe;
        }
        shuffle($res['list']);

        return $res;
    }
    public function getVideoGallery_v2()
    {
        $res = array(
            'view_element' => 'gallerylist-right',
            'url'          => '#',
            'title'        => '',
            'fa_icon'        => 'fa-film',
            'list'         => array()
        );
        $ls  = $this->getContentsByCatname($this->videoGalleryCatName, 0,
                'first');
        if (!$ls) {
            return $res;
        }
        $res['title'] = $ls['ScmCategory']['text'];
        if (!isset($ls['ScmContent'])) {
            return $res;
        }

        if (!$ls['ScmContent']) {
            return $res;
        }
        foreach ($ls['ScmContent'] as $i => $v) {
            $iframe_src = $this->getIframeSrc($v['ScmContentContent']['value']);
            $iframe = sprintf('%1$s', $iframe_src);
            $res['list'][$i]['content'] = $this->getYoutubeIdFromUrl($iframe);
        }
        shuffle($res['list']);

        return $res;
    }
    /**
     * 
     */
    function getYoutubeIdFromUrl($url)
{
    $parts = parse_url($url);
    if (isset($parts['query'])) {
        parse_str($parts['query'], $qs);
        if (isset($qs['v'])) {
            return $qs['v'];
        } elseif (isset($qs['vi'])) {
            return $qs['vi'];
        }
    }
    if (isset($parts['path'])) {
        $path = explode('/', trim($parts['path'], '/'));
        return $path[count($path)-1];
    }
    return false;
}
    public function searchContent($keyword)
    {
        $options = array();
        if (!$keyword) {
            return $options;
        }

        // content: menu_code = content_web
        // Content.text
        // Content.name
        // Content.annotation
        // ContentContent.annotation
        // ContentDetail.value
        // ContentDetail.text

        $options['conditions']['AND'][$this->alias . '.active_status']          = '1';
        $options['conditions']['AND'][$this->alias . '.menu_code']              = $this->content_web;
        $options['conditions']['AND']['OR'][$this->alias . '.text LIKE ']       = sprintf('%%%s%%',
                $keyword);
        $options['conditions']['AND']['OR'][$this->alias . '.name LIKE ']       = sprintf('%%%s%%',
                $keyword);
        $options['conditions']['AND']['OR'][$this->alias . '.annotation LIKE '] = sprintf('%%%s%%',
                $keyword);
        $options['conditions']['AND']['OR']['ScmContentContent.value LIKE ']    = sprintf('%%%s%%',
                $keyword);

        #$log = $this->getDataSource()->getLog(false, false);
#debug($log);
        #debug($data);die();
        return $options;
    }

}
