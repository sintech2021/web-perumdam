<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('SyscmAppModel', 'Syscm.Model');

/**
 * CakePHP ScmMenuListGroup
 * @author ino
 */
class ScmCustInfo extends SyscmAppModel {

    /**
     *
     * @var type 
     */
    public $useTable     = 'scm_customers';
    public $regMenuCode  = 'CustomerInfo';
    public $listMenuCode = 'CustomerList';
    public $displayField = "name";
    public $excelMime    = array(
        'application/vnd.ms-excel'                                          => 'xls',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => 'xlsx',
        'application/vnd.ms-office'                                         => 'XLS',
        'application/msword'                                                => 'XLS',
    );

    /**
     * Association
     */
    public $hasOne       = array(
        'ScmCustInfoDetail' => array(
            'className'  => 'Syscm.ScmCustInfoDetail',
            'foreignKey' => 'scm_customer_id',
            'conditions' => array(
                'ScmCustInfoDetail.active_status' => '1',
                'ScmCustInfoDetail.field'         => 'ScmCustInfoDetail',
            ),
            'fields'     => '',
        ),
    );

    public function countByNoSl($nosl) {
        $options = array(
            'conditions' => array(
                'ScmCustInfo.text'            => $nosl,
                'ScmCustInfo.active_status' => '1',
                'ScmCustInfo.menu_code'     => $this->listMenuCode,
            )
        );
        return $this->find('count', $options);
    }
    /**
     * Delete
     */
    public function dropStatus($id) {

        $result = false;
        if ($this->exists($id)) {

            $this->id = $id;
            $data     = array(
                $this->alias => array(
                    'active_status' => '0'
                )
            );
            $result   = $this->save($data, false);
        }

        return $result;
    }

    public function findByNoSl($nosl) {
        $options = array(
            'conditions' => array(
                'ScmCustInfo.text'            => $nosl,
                'ScmCustInfo.active_status' => '1',
                'ScmCustInfo.menu_code'     => $this->listMenuCode,
            )
        );
        return $this->find('first', $options);
    }
    
    public function findCLById($id) {
        $options = array(
            'conditions' => array(
                'ScmCustInfo.id'            => $id,
                'ScmCustInfo.active_status' => '1',
                'ScmCustInfo.menu_code'     => $this->listMenuCode,
            )
        );
        return $this->find('first', $options);
    }

    public function getIndexPaginationSetting() {
        $settings                                = array();
        $settings['order'][$this->alias . '.text'] = 'asc';
        $settings['recursive'] = -1;
//        debug($settings);die();
        return $settings;
    }

    /**
     * invokeTopMenu
     */
    public function invokeTopMenu($request_params) {
        $invoke    = array();
        /* if (in_array($request_params['action'],
          array('album_list', 'album_list_add'))) {
          if (!isset($request_params['pass'][0])) {
          return array();
          }
          }
          if (in_array($request_params['action'],
          array('album_edit', 'album_list', 'album_list_add'))) {

          $invoke = $this->invokeTopMenuOnEdit($request_params);
          } */
        $invoke[4] = array(
            'perm'  => 'edit',
            'alias' => 'upload_cl',
            'index' => 4,
            'menu'  => array(
                'title'   => '<span class="fa fa-upload"></span> ' . __d('syscm', 'Upload Customer'),
                'url'     => array('action' => 'upload_cl'),
                'options' => array('escape' => false),
                'active'  => false,
            ),
        );
        $invoke[5] = array(
            'perm'  => 'edit',
            'alias' => 'customer_list_template',
            'index' => 5,
            'menu'  => array(
                'title'   => '<span class="fa fa-file-excel-o"></span> ' . __d('syscm', 'Customer Template'),
                'url'     => array('action' => 'customer_list_template'),
                'options' => array('escape' => false),
                'active'  => false,
            ),
        );
        ksort($invoke, SORT_NUMERIC);
        return $invoke;
    }

    public function saveCl($sheetData) {
        $res               = array();
        $res['save_error'] = __d('syscm', 'Failed to save. Please try again.');
        if (!$sheetData) {
            return $res;
        }
        unset($sheetData[1]);
        if (!$sheetData) {
            return $res;
        }


        $this->displayField                                          = 'text';
        // fetch current list
        $options                                                     = array();
        $options['conditions']['AND'][$this->alias . '.active_status'] = 1;
        $options['conditions']['AND'][$this->alias . '.menu_code']     = $this->listMenuCode;
        $current                                                     = $this->find('list', $options);
        $current_flipped                                             = array_flip($current);

        $alias                    = 'ScmCustInfoDetail';
        $CustDetail               = ClassRegistry::init('Syscm.' . $alias);
        $CustDetail->displayField = 'scm_customer_id';

        $options                                                 = array();
        $options['conditions']['AND'][$alias . '.active_status']   = 1;
        $options['conditions']['AND'][$alias . '.scm_customer_id'] = array_values($current_flipped);

        $current_detail = $CustDetail->find('list', $options);
        $detail_flipped = array_flip($current_detail);
        //./
        //prepare save based on sheet data
        $data    = array();
        /**
         * 'A' => 'H023350',
          'B' => 'I GUSTI MOH SUGIARTO',
          'C' => 'Perum. Sukatani  G 02/04',
          'D' => 'B1',
          'E' => '08',
         * 
         * 'A' => 'NoSL', ScmCustInfo.text
          'B' => 'Nama', ScmCustInfo.name
          'C' => 'Alamat', ScmCustInfo.annotation
          'D' => 'Gol', ScmCustInfoDetail.name
          'E' => 'Wil', ScmCustInfoDetail.text
         */
        $n       = 0;
        $new     = 0;
        $replace = 0;
        foreach ($sheetData as $i => $v) {
            extract($v);

            $cust                      = array();
            // check if current key available
            $new_id                    = CakeString::uuid();
            $cust['ScmCustInfo']['id'] = isset($current_flipped[$A]) ? $current_flipped[$A] : $new_id;
            if ($cust['ScmCustInfo']['id'] == $new_id) {
                $new++;
            } else {
                $replace++;
            }
            $cust['ScmCustInfo']['name']          = $B;
            $cust['ScmCustInfo']['text']          = $A;
            $cust['ScmCustInfo']['annotation']    = $C;
            $cust['ScmCustInfo']['active_status'] = 1;
            $cust['ScmCustInfo']['menu_code']     = $this->listMenuCode;

            $cust['ScmCustInfoDetail']['id']              = isset($detail_flipped[$cust['ScmCustInfo']['id']]) ? $detail_flipped[$cust['ScmCustInfo']['id']] : CakeString::uuid();
            $cust['ScmCustInfoDetail']['scm_customer_id'] = $cust['ScmCustInfo']['id'];
            $cust['ScmCustInfoDetail']['name']            = $D;
            $cust['ScmCustInfoDetail']['text']            = $E;
            $cust['ScmCustInfoDetail']['active_status']   = 1;
            $cust['ScmCustInfoDetail']['field']           = $CustDetail->fieldKeyValue;
            $data[]                                       = $cust;
        }
        $save = $this->saveAll($data, array('validate' => false, 'deep' => true));
        if ($save) {
            $res['save_error']   = false;
            $res['save_success'] = __d('syscm', 'Data upload successfully.');
            if ($new > 0) {
                $res['save_status'][0]['css_class']   = 'alert-success';
                $res['save_status'][0]['status_text'] = __d('syscm', '%d new records.', $new);
            }
            if ($replace > 0) {
                $res['save_status'][1]['css_class']   = 'alert-success';
                $res['save_status'][1]['status_text'] = __d('syscm', '%d records were replaced.', $replace);
            }
        }
        //./
        return $res;
    }

    public function validateExcelFile($request_file_data = array(), $return_file =
    false) {
        if (!$request_file_data) {
            return false;
        }
        extract($request_file_data);
        if ($size <= 0) {
            return false;
        }
        if ($error) {
            return false;
        }
        $file     = new File($tmp_name);
        $fileinfo = $file->info();

        if (!in_array($fileinfo['mime'], array_keys($this->excelMime))) {
            return false;
        }
        if (!$return_file) {
            return true;
        }

        return $file;
    }

}
