<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('SyscmAppModel', 'Syscm.Model');

/**
 * CakePHP ScmMenuListGroup
 * @author ino
 */
class ScmMenuSlug extends SyscmAppModel
{
    
    public $useTable = 'scm_menus_details';
    
    /**
     * Association
     */
    public $belongsTo = array(
        'ScmMenuList' => array(
            'className' => 'Syscm.ScmMenuList',
            'foreignKey' => 'scm_menu_id',
            'conditions' => array('menu_code'=> 'menu_slug'),
        ),
        'FrontModel' => array(
            'className' => 'Syscm.FrontModel',
            'foreignKey' => 'scm_menu_id',
            'conditions' => array('menu_code'=> 'menu_slug'),
        )
    );
    
}
