<?php

App::uses('SyscmAppModel', 'Syscm.Model');

class Consultation extends SyscmAppModel {
	 /**
     *
     * @var type 
     */
    public $useTable = 'konsultasi';
    public $displayField = "title";
}