<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('SyscmAppModel', 'Syscm.Model');

/**
 * CakePHP ScmMenuListGroup
 * @author ino
 */
class ScmContentMedia extends SyscmAppModel
{

    /**
     *
     * @var type 
     */
    public $useTable     = 'scm_contents';
    public $menuCode     = 'content_media';
    public $displayField = "name";
    public $mediaDirName = "media";
    public $mediaMime    = array(
        'image/jpeg'     => 'jpg',
        'image/png'      => 'png',
        'image/x-ms-bmp' => 'bmp',
        'image/tiff'     => 'tif',
    );

    /**
     * Association
     */
    public $hasOne              = array(
        'ScmContentContent'   => array(
            'className'  => 'Syscm.ScmContentContent',
            'foreignKey' => 'scm_content_id',
            'conditions' => array(
                'ScmContentContent.active_status' => '1',
                'ScmContentContent.field'         => 'ScmContentMedia',
            ),
            'fields'     => '',
        ),
        'ScmContentPublished' => array(
            'className'  => 'Syscm.ScmContentPublished',
            'foreignKey' => 'scm_content_id',
            'conditions' => array(
                'ScmContentPublished.active_status' => '1',
                'ScmContentPublished.field'         => 'ScmContentPublished',
            ),
            'fields'     => '',
        ),
        'ScmContentMeta'      => array(
            'className'  => 'Syscm.ScmContentMeta',
            'foreignKey' => 'scm_content_id',
            'conditions' => array(
                'ScmContentMeta.active_status' => '1',
                'ScmContentMeta.field'         => 'ScmContentMeta',
            ),
            'fields'     => '',
        ),
    );
    public $hasAndBelongsToMany = array(
        'ScmContentMediaGroup' =>
        array(
            'className'             => 'Syscm.ScmContentMediaGroup',
            'joinTable'             => 'scm_contents_categories',
            'foreignKey'            => 'scm_content_id',
            'associationForeignKey' => 'scm_category_id',
            'unique'                => 'keepExisting',
            'conditions'            => '',
            'fields'                => '',
            'order'                 => '',
            'limit'                 => '',
            'offset'                => '',
            'finderQuery'           => '',
            'with'                  => null
        )
    );
    public $belongsTo           = array(
        'ParentContent' => array(
            'className'  => 'Syscm.ScmContentMedia',
            'foreignKey' => 'parent_id',
            'conditions' => array(
                'ParentContent.active_status' => '1',
                'ParentContent.menu_code'     => 'content_media',
            ),
            'fields'     => '',
            'order'      => ''
        )
    );
    public $HasMany             = array(
        'ChildContent' => array(
            'className'  => 'Syscm.ScmContentMedia',
            'foreignKey' => 'parent_id',
            'conditions' => array(
                'ChildContent.active_status' => '1',
                'ChildContent.menu_code'     => 'content_media',
            ),
            'fields'     => '',
            'order'      => ''
        ),
        'ScmContentsCategory' => array(
            'className'  => 'Syscm.ScmContentsCategory',
            'foreignKey' => 'scm_content_id',
            
        )
    );

    /**
     * Validations
     */
    public $validate = array(
        'text'     => array(
            'notBlank' => array(
                'rule'    => 'notBlank',
                'message' => 'This value may not be left empty!'
            ),
        ),
        'name'     => array(
            'notBlank' => array(
                'rule'    => 'notBlank',
                'message' => 'This value may not be left empty!'
            ),
        ),
        'the_file' => array(
            'notBlank' => array(
                'rule'    => 'notBlank',
                'message' => 'This value may not be left empty!',
                'on'      => 'create',
            ),
        )
    );

    /**
     * Delete
     */
    public function deleteImage($id)
    {
        $media = $this->findById($id);
        if (!$media) {
            return false;
        }

        if (!isset($media['ScmContentContent'])) {
            return true;
        }

        if (!isset($media['ScmContentContent']['value'])) {
            return true;
        }

        $fn       = basename($media['ScmContentContent']['value']);
        $dirname  = WWW_ROOT . 'files' . DS . $this->mediaDirName;
        $filepath = $dirname . DS . $fn;
        $file     = new File($filepath);
        if (!$file->exists()) {
            return true;
        }

        return $file->delete();
    }

    public function dropStatus($id)
    {

        $result = false;
        if ($this->exists($id)) {

            $this->id = $id;
            $data     = array(
                $this->alias => array(
                    'active_status' => '0'
                )
            );
            $result   = $this->save($data, false);
        }

        return $result;
    }

    
    /**
     * 
     */
    public function getImageById($media_id)
    {
        $res = false;
        if(!$media_id){
            return $res;
        }
        
        $conditions = array();
        $conditions['AND'][$this->alias . '.active_status'] = '1';
        $conditions['AND'][$this->alias . '.menu_code'] = $this->menuCode;
        $conditions['AND'][$this->alias . '.name'] = $media_id;
        
        $options = array(
            'conditions' => $conditions
        );
        $this->recursive =0;
        $media = $this->find('first', $options);
        if(!$media){
            return $res;
        }
        if(!isset($media['ScmContentContent'])){
            return $res;
        }
        if(!$media['ScmContentContent']['value']){
            return $res;
        }
        return $media['ScmContentContent']['value'];
    }
    
    /**
     * 
     * @param type $id
     */
    public function getCategories()
    {
        $ScmCategory = ClassRegistry::init('Syscm.ScmCategory');
        $conditions  = array(
            'AND' => array(
                'ScmCategory.active_status' => '1',
            )
        );
        $options     = array('conditions' => $conditions);
        return $ScmCategory->find('list', $options);
    }

    /**
     * 
     * @return type
     */
    public function getPaginateConditions()
    {
        return array(
            'AND' => array(
                $this->alias . '.active_status' => '1',
                $this->alias . '.menu_code'     => $this->menuCode,
            )
        );
    }

    /**
     * 
     * @return type
     */
    public function getPaginateConditionsByAlbum($album, $exclude = false)
    {
        $ids = array();
        if ($album['ScmContentsCategory']) {
            foreach ($album['ScmContentsCategory'] as $v) {
                $ids[] = $v['scm_content_id'];
            }
        }
        
        
        $conditions = array(
            'AND' => array(
                $this->alias . '.active_status' => '1',
                $this->alias . '.menu_code'     => $this->menuCode,
            )
        );
        if ($exclude) {
            $conditions['AND']['NOT'][$this->alias . '.id'] = $ids;
        }
        else {
            $conditions['AND'][$this->alias . '.id'] = $ids;
        }
        #debug($conditions);die();
        return $conditions;
    }

    /**
     * 
     * @param type $id
     */
    public function getParents($exclude_ids = array())
    {

        $conditions = array(
            'AND' => array(
                $this->alias . '.active_status' => '1',
            )
        );
        if ($exclude_ids) {
            $conditions['AND']['NOT'][$this->alias . '.id'] = $exclude_ids;
        }
        $this->Behaviors->load('Tree');
        $this->recover();
        return $this->generateTreeList($conditions);
    }

    /**
     * Preview Document
     */
    public function invokeTopMenu($request_params)
    {
        $invoke = array();
        if (in_array($request_params['action'],
                        array('album_list', 'album_list_add'))) {
            if (!isset($request_params['pass'][0])) {
                return array();
            }
        }
        if (in_array($request_params['action'],
                        array('album_edit', 'album_list', 'album_list_add'))) {

            $invoke = $this->invokeTopMenuOnEdit($request_params);
        }
        $invoke[4] = array(
            'perm'  => 'browse',
            'alias' => 'album_index',
            'index' => 4,
            'menu'  => array(
                'title'   => '<span class="fa fa-th-list"></span> ' . __d('syscm',
                        'Album'),
                'url'     => array('action' => 'album_index'),
                'options' => array('escape' => false),
                'active'  => false,
            ),
        );
        $invoke[5] = array(
            'perm'  => 'add',
            'alias' => 'album_add',
            'index' => 5,
            'menu'  => array(
                'title'   => '<span class="fa fa-plus"></span> ' . __d('syscm',
                        'Add Album'),
                'url'     => array('action' => 'album_add'),
                'options' => array('escape' => false),
                'active'  => false,
            ),
        );
        ksort($invoke, SORT_NUMERIC);
        return $invoke;
    }

    /**
     * Preview Document
     */
    public function invokeTopMenuOnEdit($request_params)
    {


        return array(
            6 => array(
                'perm'  => 'edit',
                'alias' => 'album_edit',
                'index' => 6,
                'menu'  => array(
                    'title'   => '<span class="fa fa-th-list"></span> ' . __d('syscm',
                            'Album Edit'),
                    'url'     => array('action' => 'album_edit', $request_params['pass'][0]),
                    'options' => array('escape' => false),
                    'active'  => false,
                ),
            ),
            7 => array(
                'perm'  => 'edit',
                'alias' => 'album_list',
                'index' => 7,
                'menu'  => array(
                    'title'   => '<span class="fa fa-th-list"></span> ' . __d('syscm',
                            'Media List'),
                    'url'     => array('action' => 'album_list', $request_params['pass'][0]),
                    'options' => array('escape' => false),
                    'active'  => false,
                ),
            ),
            8 => array(
                'perm'  => 'edit',
                'alias' => 'album_list_add',
                'index' => 8,
                'menu'  => array(
                    'title'   => '<span class="fa fa-th-list"></span> ' . __d('syscm',
                            'Add Media List'),
                    'url'     => array('action' => 'album_list_add', $request_params['pass'][0]),
                    'options' => array('escape' => false),
                    'active'  => false,
                ),
            ),
            9 => array(
                'perm'  => 'delete',
                'alias' => 'album_delete',
                'index' => 9,
                'menu'  => array(
                    'link_type' => 'postLink',
                    'title'     => '<span class=" text-danger"><span class="fa fa-trash text-danger"></span>  ' . __d('syscm',
                            'Delete Album') . '</span>',
                    'url'       => array(
                        "action" => "album_delete",
                        $request_params['pass'][0]),
                    'options'   => array(
                        'confirm' => __d('sysadmin',
                                'Are you sure you want to delete this album ?'),
                        'escape'  => false
                    ),
                    'active'    => false,
                ),
            )
        );
    }

    /**
     * 
     * @param type $the_file
     */
    public function saveImageFile($the_file = array(), $current = false)
    {
        if (!$this->validateImageFile($the_file)) {
            return $current;
        }

        $dirname = WWW_ROOT . 'files' . DS . $this->mediaDirName;
        if (!is_dir($dirname)) {
            $folder = new Folder();
            $folder->create($dirname);
        }
        if ($current) {
            $current_file = new File($dirname . DS . basename($current));
            if ($current_file->exists()) {
                $current_file->delete();
            }
        }
        extract($the_file);
        $file     = new File($tmp_name);
        $name     = filter_var($name, FILTER_SANITIZE_URL);
        $basename = time() . '-' . basename($name);
        $filename = $dirname . DS . $basename;
        $urlpath  = '/files/' . $this->mediaDirName . '/' . $basename;

        if ($file->copy($filename, true)) {
            return $urlpath;
        }

        return false;
    }

    /**
     * 
     * @param type $options
     * @return boolean
     */
    public function validateImageFile($request_file_data = array())
    {
        if (!$request_file_data) {
            return false;
        }
        extract($request_file_data);
        if ($size <= 0) {
            return false;
        }
        if ($error) {
            return false;
        }
        $file     = new File($tmp_name);
        $fileinfo = $file->info();
        if (!in_array($fileinfo['mime'], array_keys($this->mediaMime))) {
            return false;
        }
        return true;
    }

}
