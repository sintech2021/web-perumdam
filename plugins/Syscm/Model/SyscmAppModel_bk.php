<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppModel', 'Model');

/**
 * CakePHP SyscmModel
 * @author ino
 */
class SyscmAppModel extends AppModel
{

    public $useDbConfig   = 'sys_';
    public $topMenuRevoke = array();

    /**
     * 
     */

    /**
     * 
     */
    public function setTopMenu($request, $SysAclView, $invoke)
    {
        $edit_mode = isset($request['pass'][0]) && in_array($request['action'],
                        array('edit', 'delete')) ? true : false;
        $index     = array();
        $add       = array();
        if ($edit_mode) {
            $edit   = array();
            $delete = array();
        }


        $plugin_alias     = 'syscm';
        $controller_alias = $request['controller'];

        if ($SysAclView['browse']) {
            $index = array(
                'title'   => '<span class="fa fa-list fa-fw"></span> ' . __d('syscm',
                        'Index'),
                'url'     => array(
                    "plugin"     => $plugin_alias,
                    "controller" => $controller_alias,
                    "action"     => "index"),
                'options' => array('escape' => false),
                'active'  => false,
            );
        }
        if ($SysAclView['add']) {
            $add = array(
                'title'   => '<span class="fa fa-plus fa-fw"></span> ' . __d('syscm',
                        'Add'),
                'url'     => array(
                    "plugin"     => $plugin_alias,
                    "controller" => $controller_alias,
                    "action"     => "add"),
                'options' => array('escape' => false),
                'active'  => false,
            );
        }
        if ($SysAclView['edit'] && $edit_mode) {
            $edit = array(
                'title'   => '<span class="fa fa-pencil"></span> ' . __d('syscm',
                        'Edit'),
                'url'     => array(
                    "plugin"     => $plugin_alias,
                    "controller" => $controller_alias,
                    "action"     => "edit",
                    $request['pass'][0]),
                'options' => array('escape' => false),
                'active'  => false,
            );
        }
        if ($SysAclView['delete'] && $edit_mode) {
            $delete = array(
                'link_type' => 'postLink',
                'title'     => '<span class=" text-danger"><span class="fa fa-trash text-danger"></span>  ' . __d('syscm',
                        'Delete') . '</span>',
                'url'       => array(
                    "plugin"     => $plugin_alias,
                    "controller" => $controller_alias,
                    "action"     => "delete",
                    $request['pass'][0]),
                'options'   => array(
                    'confirm' => __d('sysadmin',
                            'Are you sure you want to delete this record ?'),
                    'escape'  => false
                ),
                'active'    => false,
            );
        }
        switch ($request['action']) {
            case 'index':
                $index['url']    = '#';
                $index['active'] = true;
                break;
            case 'add':
                $add['url']      = '#';
                $add['active']   = true;
                break;
            case 'edit':
                $edit['url']     = '#';
                $edit['active']  = true;
                break;
            default:
                #$index['url'] = '#';
                #$index['active']    = true;
                break;
        }

        $top_menu = array();
        if (!in_array('index', $this->topMenuRevoke)) {
            $top_menu[] = $index;
        }
        if (!in_array('add', $this->topMenuRevoke)) {
            $top_menu[] = $add;
        }

        if ($edit_mode) {
            if (!in_array('edit', $this->topMenuRevoke)) {
                $top_menu[] = $edit;
            }
            if (!in_array('delete', $this->topMenuRevoke)) {
                $top_menu[] = $delete;
            }            
        }
        if ($invoke) {
            foreach ($invoke as $i) {
                if ($SysAclView[$i['perm']]) {
                    $new = array();
                    $n   = 0;
                    foreach ($top_menu as $ii => $v) {

                        if ($request['action'] == $i['alias']) {
                            $i['menu']['url']    = '#';
                            $i['menu']['active'] = true;
                        }
                        if ($i['index'] == $n) {
                            $new[$n] = $i['menu'];
                        }
                        $new[] = $v;
                        $n++;
                    }
                    while ($i['index'] >= $n) {
                        if ($n == $i['index']) {
                            $new[] = $i['menu'];
                        }

                        $n++;
                    }
                    $top_menu = $new;
                }
            }
        }

        return $top_menu;
    }

    /**
     * validations
     * @param type $check
     * @return type
     */
    public function alphaNumericDashUnderscore($check)
    {
        $value = array_values($check);
        $value = $value[0];

        return preg_match('|^[0-9a-zA-Z_-]*$|', $value);
    }

    /**
     * 
     */
    public function fieldUnique($field = "name")
    {
        $conditions = array(
            $this->alias . '.' . $field       => $this->data[$this->alias][$field],
            $this->alias . '.active_status' => 1,
        );
        if (isset($this->data[$this->alias][$this->primaryKey])) {
            $conditions[$this->alias . '.' . $this->primaryKey . ' !='] = $this->data[$this->alias][$this->primaryKey];
        }
        $existing = $this->find('first',
                array(
            'conditions' => $conditions
        ));
        return (count($existing) == 0);
    }

}
