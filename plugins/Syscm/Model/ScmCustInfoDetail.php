<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('SyscmAppModel', 'Syscm.Model');

/**
 * CakePHP ScmMenuListGroup
 * @author ino
 */
class ScmCustInfoDetail extends SyscmAppModel
{

    /**
     *
     * @var type 
     */
    public $useTable      = 'scm_customers_details';
    public $fieldKeyValue = 'ScmCustInfoDetail';
    public $displayField  = "name";

    /**
     * 
     */

    /**
     * Delete
     */
    public function dropStatus($id)
    {

        $result = false;
        if ($this->exists($id)) {

            $this->id = $id;
            $data     = array(
                $this->alias => array(
                    'active_status' => '0'
                )
            );
            $result   = $this->save($data, false);
        }

        return $result;
    }

}
