<?php

App::uses('SyscmAppModel', 'Syscm.Model');

class Galery extends SyscmAppModel {
	 /**
     *
     * @var type 
     */
    public $useTable = 'galeries';
    public $displayField = "title";
}