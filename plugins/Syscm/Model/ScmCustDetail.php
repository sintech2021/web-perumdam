<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('SyscmAppModel', 'Syscm.Model');

/**
 * CakePHP ScmMenuListGroup
 * @author ino
 */
class ScmCustDetail extends SyscmAppModel {

    /**
     *
     * @var type 
     */
    public $useTable      = 'scm_customers_details';
    public $displayField  = "name";

    /**
     * 
     */

    /**
     * Delete
     */
    public function dropStatus($id) {

        $result = false;
        if ($this->exists($id)) {

            $this->id = $id;
            $data     = array(
                $this->alias => array(
                    'active_status' => '0'
                )
            );
            $result   = $this->save($data, false);
        }

        return $result;
    }

    /**
     * Delete
     */
    public function dropStatusByCustId($cust_id) {

        $result = false;
        $data   = array(
            $this->alias.'.active_status' => '0'
        );
        $conditions = array(
            $this->alias.'.scm_customer_id' => $cust_id
        );
        $result = $this->updateAll($data, $conditions);

        return $result;
    }

}
