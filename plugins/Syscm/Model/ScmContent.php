<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('SyscmAppModel', 'Syscm.Model');

/**
 * CakePHP ScmMenuListGroup
 * @author ino
 */
class ScmContent extends SyscmAppModel
{

    /**
     *
     * @var type 
     */
    public $useTable     = 'scm_contents';
    public $menuCode     = 'content_web';
    public $displayField = "name";

    /**
     * Association
     */
    public $hasOne = array(
        'ScmContentContent'   => array(
            'className'  => 'Syscm.ScmContentContent',
            'foreignKey' => 'scm_content_id',
            'conditions' => array(
                'ScmContentContent.active_status' => '1',
                'ScmContentContent.field'         => 'ScmContentContent',
            ),
            'fields'     => '',
        ),
        'ScmContentPublished' => array(
            'className'  => 'Syscm.ScmContentPublished',
            'foreignKey' => 'scm_content_id',
            'conditions' => array(
                'ScmContentPublished.active_status' => '1',
                'ScmContentPublished.field'         => 'ScmContentPublished',
            ),
            'fields'     => '',
        ),
        'ScmContentMeta'      => array(
            'className'  => 'Syscm.ScmContentMeta',
            'foreignKey' => 'scm_content_id',
            'conditions' => array(
                'ScmContentMeta.active_status' => '1',
                'ScmContentMeta.field'         => 'ScmContentMeta',
            ),
            'fields'     => '',
        ),
        'ScmContentsCategory'   => array(
            'className'  => 'Syscm.ScmContentsCategory',
            'foreignKey' => 'scm_content_id',
            // 'conditions' => array(
            //     'ScmContentContent.active_status' => '1',
            //     'ScmContentContent.field'         => 'ScmContentContent',
            // ),
            'fields'     => '',
        ),
    );
    
    public $hasAndBelongsToMany = array(
        'ScmCategory' =>
            array(
                'className' => 'Syscm.ScmCategory',
                'joinTable' => 'scm_contents_categories',
                'foreignKey' => 'scm_content_id',
                'associationForeignKey' => 'scm_category_id',
                'unique' => 'keepExisting',
                'conditions' => '',
                'fields' => '',
                'order' => '',
                'limit' => '',
                'offset' => '',
                'finderQuery' => '',
                'with' => null
            )
    );
    
    public $belongsTo = array(
        'ParentContent' => array(
            'className'  => 'Syscm.ScmContent',
            'foreignKey' => 'parent_id',
            'conditions' => array(
                'ParentContent.active_status'=> '1',
                'ParentContent.menu_code'=> 'content_web',
                ),
            'fields'     => '',
            'order'      => ''
        
        )
    );
    
    public $hasMany = array(
        'ChildContent' => array(
            'className'  => 'Syscm.ScmContent',
            'foreignKey' => 'parent_id',
            'conditions' => array(
                'ChildContent.active_status'=> '1',
                'ChildContent.menu_code'=> 'content_web',
                ),
            'fields'     => '',
            'order'      => ''
        
        )
    );

    /**
     * Validations
     */
    public $validate = array(
        'text' => array(
            'notBlank' => array(
                'rule'    => 'notBlank',
                'message' => 'This value may not be left empty!'
            ),
        ),
        'name' => array(
            'notBlank' => array(
                'rule'    => 'notBlank',
                'message' => 'This value may not be left empty!'
            ),
        )
    );

    /**
     * Delete
     */
    public function dropStatus($id)
    {

        $result = false;
        if ($this->exists($id)) {

            $this->id = $id;
            $data     = array(
                $this->alias => array(
                    'active_status' => '0'
                )
            );
            $result   = $this->save($data, false);
        }

        return $result;
    }
    
    /**
     * 
     * @param type $id
     */
    public function getCategories()
    {
        $ScmCategory = ClassRegistry::init('Syscm.ScmCategory');
        $conditions = array(
            'AND' => array(
                'ScmCategory.active_status' => '1',
                'ScmCategory.menu_code' => $ScmCategory->menuCodeContentCategory,
            )
            
        );
        $ScmCategory->virtualFields = array('display'=> "CONCAT(ScmCategory.text, ' (', ScmCategory.name, ') ')");
        $ScmCategory->displayField = 'display';
        $options = array('conditions'=> $conditions);
        $options['order'] = array('ScmCategory.text'=>'asc');
        return $ScmCategory->find('list', $options);
    }
    
    /**
     * 
     * @param type $id
     */
    public function getParents($exclude_ids = array())
    {
        
        $conditions = array(
            'AND' => array(
                $this->alias.'.active_status' => '1',
                $this->alias.'.menu_code' => $this->menuCode,
            )
            
        );
        if($exclude_ids){
            $conditions['AND']['NOT'][$this->alias.'.id'] = $exclude_ids;
        }
        $this->Behaviors->load('Tree');
        $this->recover();
        return $this->generateTreeList($conditions);
    }
    /**
     * Preview Document
     */
    public function invokeTopMenuOnEdit($request_data){
        
        return array(
            array(
                'perm' => 'edit',
                'alias' => 'preview',
                'index' => 4,
                'menu' => array(
                   'title'   => '<span class="fa fa-search"></span> ' . __d('syscm',
                            'Preview'),
                    'url'     => $request_data[$this->alias]['name'].'/cid:'.$request_data[$this->alias]['id'].'/'.md5($request_data[$this->alias]['id'].'id').':'.md5($request_data[$this->alias]['id'].date('d')),
                    'options' => array('escape' => false, 'target'=> '_blank'),
                    'active'  => false,
                ),
                
            )
            
        );
    }
}
