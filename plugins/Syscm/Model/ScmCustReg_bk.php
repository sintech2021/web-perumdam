<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('SyscmAppModel', 'Syscm.Model');

/**
 * CakePHP ScmMenuListGroup
 * @author ino
 */
class ScmCustReg extends SyscmAppModel {

    /**
     *
     * @var type 
     */
    public $useTable     = 'scm_customers';
    public $regMenuCode  = 'CustomerRegistration';
    public $displayField = "name";
    public $topMenuRevoke = array('add');

    /**
     * Association
     */
    public $hasOne = array(
        'ScmCustRegDetail' => array(
            'className'  => 'Syscm.ScmCustRegDetail',
            'foreignKey' => 'scm_customer_id',
            'conditions' => array(
                'ScmCustRegDetail.active_status' => '1',
                'ScmCustRegDetail.field'         => 'ScmCustRegDetail',
            ),
            'fields'     => '',
        ),
    );

    /**
     * Delete
     */
    public function checkDuplicatedEmailCell($email, $cell) {
        $options = array(
            'conditions' => array(
                'AND' => array(
                    'ScmCustRegDetail.name'          => $email,
                    'ScmCustRegDetail.text'          => $cell,
                    'ScmCustRegDetail.active_status' => 1,
                    'ScmCustRegDetail.field'         => 'ScmCustRegDetail',
                )
            )
        );
        return $this->find('count', $options);
    }

    /**
     * Delete
     */
    public function checkRegCode($reg_code) {
        $options = array(
            'conditions' => array(
                'AND' => array(
                    'ScmCustReg.active_status' => 1,
                    'ScmCustReg.text'          => $reg_code
                )
            )
        );
        return $this->find('count', $options);
    }

    /**
     * Delete
     */
    public function dropStatus($id) {

        $result = false;
        if ($this->exists($id)) {

            $this->id = $id;
            $data     = array(
                $this->alias => array(
                    'active_status' => '0'
                )
            );
            $result   = $this->save($data, false);
        }

        return $result;
    }

    public function getIndexPaginationSetting() {
        
        $settings = array(
            'order' => array(
                
                $this->alias . '.menu_order' => 'asc',
                #$this->alias . '.created' => 'desc',
                $this->alias . '.text' => 'desc',
            ),
            #'limit' => 1,
        );
        return $settings;
    }

    /**
     * 
     * @param type $id
     */
    public function getParents($exclude_ids = array()) {

        $conditions = array(
            'AND' => array(
                $this->alias . '.active_status' => '1',
                $this->alias . '.menu_code'     => $this->menuCode,
            )
        );
        if ($exclude_ids) {
            $conditions['AND']['NOT'][$this->alias . '.id'] = $exclude_ids;
        }
        $this->Behaviors->load('Tree');
        $this->recover();
        return $this->generateTreeList($conditions);
    }

    /**
     * 
     */
    public function getRegByCode($reg_code) {
        $res     = array();
        $options = array(
            'conditions' => array(
                'AND' => array(
                    'ScmCustReg.active_status' => 1,
                    'ScmCustReg.text'          => $reg_code
                )
            )
        );
        $data    = $this->find('first', $options);
        if (!$data) {
            return $res;
        }
        if (!isset($data['ScmCustReg'])) {
            return $res;
        }
        $res['cust_reg_name'] = $data['ScmCustReg']['name'];
        $res['cust_reg_code'] = $data['ScmCustReg']['text'];
        $res['cust_reg_dt']   = $data['ScmCustReg']['modified'];
        if (!isset($data['ScmCustRegDetail'])) {
            return $res;
        }

        $res['cust_reg_email'] = $data['ScmCustRegDetail']['name'];
        $res['cust_reg_addr']  = $data['ScmCustRegDetail']['value'];
        $res['cust_reg_cell']  = $data['ScmCustRegDetail']['text'];
        $res['cust_reg_phone'] = $data['ScmCustRegDetail']['annotation'];
        $res['cust_reg_ip']    = $data['ScmCustRegDetail']['menu_code'];
        return $res;
    }

    /**
     * 
     */
    public function saveReg($posted_data) {
        if (!$posted_data) {
            return false;
        }
        #debug($posted_data); die();
        extract($posted_data);
        $data                                = array();
        $reg_id                              = CakeString::uuid();
        $data['ScmCustReg']['id']            = $reg_id;
        $data['ScmCustReg']['active_status'] = '1';
        $data['ScmCustReg']['menu_code']     = $this->regMenuCode;
        $data['ScmCustReg']['name']          = $cust_reg_name;
        $data['ScmCustReg']['text']          = $cust_reg_code;
        $annotation                          = array(
            'phone'     => $cust_reg_phone,
            'cellphone' => $cust_reg_cell,
            'email'     => $cust_reg_email,
            'addr'      => $cust_reg_addr,
            'ip'        => $client_ip,
        );
        $data['ScmCustReg']['annotation']    = json_encode($annotation);

        $data['ScmCustRegDetail']['scm_customer_id'] = $reg_id;
        $data['ScmCustRegDetail']['active_status']   = '1';
        $data['ScmCustRegDetail']['name']            = $cust_reg_email;
        $data['ScmCustRegDetail']['field']           = 'ScmCustRegDetail';
        $data['ScmCustRegDetail']['value']           = $cust_reg_addr;
        $data['ScmCustRegDetail']['text']            = $cust_reg_cell;
        $data['ScmCustRegDetail']['annotation']      = $cust_reg_phone;
        $data['ScmCustRegDetail']['menu_code']       = $client_ip;

        $options = array('validate' => false);
        return $this->saveAssociated($data, $options);
    }

}
