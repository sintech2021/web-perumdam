<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('SyscmAppModel', 'Syscm.Model');

/**
 * CakePHP ScmMenuListGroup
 * @author ino
 */
class ScmCustFeedbackRespon extends SyscmAppModel {

    /**
     *
     * @var type 
     */
    public $useTable      = 'scm_customers_details';
    public $fieldKeyValue = 'ScmCustFeedbackRespon';
    public $displayField  = "value";
    public $feedbackLimit  = 10;

    /**
     * 
     */

    /**
     * Delete
     */
    public function dropStatus($id) {

        $result = false;
        if ($this->exists($id)) {

            $this->id = $id;
            $data     = array(
                $this->alias => array(
                    'active_status' => '0'
                )
            );
            $result   = $this->save($data, false);
        }

        return $result;
    }

    public function findByFeedbackRs($rs, $find = 'first', $order = array()) {
        $res      = array();
        if (!$rs) return $res;
        $cust_ids = array();
        foreach ($rs as $key => $val) {
            $cust_ids = $val['ScmCustFeedback']['id'];
        }
        if (!$cust_ids) return $res;
        $options = array(
            'conditions' => array(
                $this->alias . '.scm_customer_id' => $cust_ids,
                $this->alias . '.active_status'   => '1',
                $this->alias . '.field'           => $this->fieldKeyValue,
            )
        );
        if ($order) {
            $options['order'] = $order;
        }
        return $this->find($find, $options);
    }

    public function findByFeedbackRsFormatted($rs) {
        $res = array();
        if (!$rs) return $res;
        $rs  = $this->findByFeedbackRs($rs, 'all', array($this->alias . '.created' => 'asc'));
        if (!$rs) return $res;
        foreach ($rs as $key => $val) {
            $cust_id                                  = $val[$this->alias]['scm_customer_id'];
            $res [$cust_id][$key]['responder']        = $val[$this->alias]['name'];
            $res [$cust_id][$key]['respon']           = $val[$this->alias]['value'];
            $res [$cust_id][$key]['respon_time']      = $val[$this->alias]['created'];
            $res [$cust_id][$key]['responder_update'] = $val[$this->alias]['text'];
            $res [$cust_id][$key]['respon_update']    = $val[$this->alias]['modified'];
        }
        return $res;
    }

    public function getCountResponsByCustId($custId){
        if(!$custId){
            return 0;
        }
        $options = array(
            'conditions' => array(
                'AND' => array(
                'active_status' => 1
                    )
            ),
        );
        $options['conditions']['AND']['field'] = $this->fieldKeyValue;
        $options['conditions']['AND']['scm_customer_id'] = $custId;
        return $this->find('count', $options);
    }
    public function getFeedbackResponPaginateSettings(){
        $settings = array(
            $this->alias => array(
                'order' => array('created' => 'desc'),
                'limit' => $this->feedbackLimit,
            )
                
        );
        return $settings;
    }
}
