<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('SyscmAppModel', 'Syscm.Model');

/**
 * CakePHP ScmMenuListGroup
 * @author ino
 */
class ScmContentMediaGroup extends SyscmAppModel
{

    /**
     *
     * @var type 
     */
    public $useTable     = 'scm_categories';
    public $menuCode     = 'MediaAlbum';
    public $displayField = "name";
    public $mediaDirName = "media";

    /**
     * Association
     */
    public $hasAndBelongsToMany = array(
        'ScmContentMedia' =>
        array(
            'className'             => 'Syscm.ScmContentMedia',
            'joinTable'             => 'scm_contents_categories',
            'foreignKey'            => 'scm_category_id',
            'associationForeignKey' => 'scm_content_id',
            'unique'                => 'keepExisting',
            'conditions'            => '',
            'fields'                => '',
            'order'                 => '',
            'limit'                 => '',
            'offset'                => '',
            'finderQuery'           => '',
            'with'                  => null
        )
    );
    
    public $hasMany = array(
        'ScmContentsCategory' => array(
            'className'  => 'Syscm.ScmContentsCategory',
            'foreignKey' => 'scm_category_id',
            
        )
    );

    /**
     * Validations
     */
    public $validate            = array(
        'text' => array(
            'notBlank' => array(
                'rule'    => 'notBlank',
                'message' => 'This value may not be left empty!'
            ),
        ),
        'name' => array(
            'notBlank' => array(
                'rule'    => 'notBlank',
                'message' => 'This value may not be left empty!'
            ),
        )
    );

    /**
     * Delete
     */
    public function dropStatus($id)
    {

        $result = false;
        if ($this->exists($id)) {

            $this->id = $id;
            $data     = array(
                $this->alias => array(
                    'active_status' => '0'
                )
            );
            $result   = $this->save($data, false);
            //delete assoc
            if ($result) {
                $assoc = ClassRegistry::init('Syscm.ScmContentsCategory');
                $assoc->deleteAll(array('ScmContentsCategory.scm_category_id' => $id),
                        false);
            }
        }

        return $result;
    }

    /**
     * 
     */
    public function getPaginateConditions()
    {
        return array(
            'AND' => array(
                $this->alias . '.active_status' => '1',
                $this->alias . '.menu_code'     => $this->menuCode,
            )
        );
    }

}
