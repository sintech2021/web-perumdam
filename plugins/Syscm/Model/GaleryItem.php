<?php

App::uses('SyscmAppModel', 'Syscm.Model');

class GaleryItem extends SyscmAppModel {
	 /**
     *
     * @var type 
     */
    public $useTable = 'galeries_items';
    public $displayField = "title";
}