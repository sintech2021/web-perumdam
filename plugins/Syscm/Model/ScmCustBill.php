<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('SyscmAppModel', 'Syscm.Model');

/**
 * CakePHP ScmMenuListGroup
 * @author ino
 */
class ScmCustBill extends SyscmAppModel {

    /**
     *
     * @var type 
     */
    public $useTable = 'scm_customers';
    public $billMenuCode = 'CustomerBill';
    public $displayField = "name";
    public $excelMime = array(
        'application/vnd.ms-excel' => 'xls',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => 'xlsx',
        'application/vnd.ms-office' => 'XLS',
        'application/msword' => 'XLS',
    );
    public $topMenuRevoke = array('add');

    /**
     * Association
     */
    public $hasOne = array(
        //BlnPakai 	ThnPakai 	StdAwal 	StdAkhir 	Kubikasi
        'ScmCustBillDetail' => array(
            'className' => 'Syscm.ScmCustBillDetail',
            'foreignKey' => 'scm_customer_id',
            'conditions' => array(
                'ScmCustBillDetail.active_status' => '1',
                'ScmCustBillDetail.field' => 'ScmCustBillDetail',
            ),
            'fields' => '',
        ),
        //RpAir 	RpAdm 	RpPmh 	RpTagihan
        'ScmCustBillAmount' => array(
            'className' => 'Syscm.ScmCustBillAmount',
            'foreignKey' => 'scm_customer_id',
            'conditions' => array(
                'ScmCustBillAmount.active_status' => '1',
                'ScmCustBillAmount.field' => 'ScmCustBillAmount',
            ),
            'fields' => '',
        ),
        
    );
    public $belongsTo = array(
        'ScmCustInfo' => array(
            'className' => 'Syscm.ScmCustInfo',
            'foreignKey' => 'parent_id',
            'conditions' => array(
                'ScmCustInfo.active_status' => '1',
                'ScmCustInfo.menu_code' => 'CustomerList',
            ),
        ),
        'ScmCustInfoDetail' => array(
            'className' => 'Syscm.ScmCustInfoDetail',
            'foreignKey' => false,
            'conditions' => array(
                'ScmCustInfo.active_status=1',
                "ScmCustInfoDetail.field='ScmCustInfoDetail'",
                "ScmCustInfoDetail.scm_customer_id=ScmCustBill.parent_id",
            ),
        ),
    );

    /**
     * Delete
     */
    public function dropStatus($id) {

        $result = false;
        if ($this->exists($id)) {

            $this->id = $id;
            $data = array(
                $this->alias => array(
                    'active_status' => '0'
                )
            );
            $result = $this->save($data, false);
        }

        return $result;
    }

    public function findBillByNosl($nosl, $year = null, $month=null) {
        /**
         * 'name' => '2', //month
         * 'annotation' => '2015', //year
         */
        $year = $year ? $year : date('Y');
        $month = $month ? $month : date('m');
        $month = intval($month);
        $options = array(
            'conditions' => array(
                'ScmCustBill.text' => $nosl,
                'ScmCustBill.name' => $month,
                'ScmCustBill.annotation' => $year,
                'ScmCustBill.active_status' => '1',
                'ScmCustBill.menu_code' => $this->billMenuCode,
            )
        );
        return $this->find('first', $options);
    }
    
    public function findCLById($id) {
        $options = array(
            'conditions' => array(
                'ScmCustBill.id' => $id,
                'ScmCustBill.active_status' => '1',
                'ScmCustBill.menu_code' => $this->billMenuCode,
            )
        );
        return $this->find('first', $options);
    }

    public function getIndexPaginationSetting() {
        $settings = array();
        $settings['order'][$this->alias . '.text'] = 'asc';
//        debug($settings);die();
        return $settings;
    }

    /**
     * invokeTopMenu
     */
    public function invokeTopMenu($request_params) {
        $invoke = array();
        /* if (in_array($request_params['action'],
          array('album_list', 'album_list_add'))) {
          if (!isset($request_params['pass'][0])) {
          return array();
          }
          }
          if (in_array($request_params['action'],
          array('album_edit', 'album_list', 'album_list_add'))) {

          $invoke = $this->invokeTopMenuOnEdit($request_params);
          } */
        $invoke[4] = array(
            'perm' => 'edit',
            'alias' => 'upload_cl_bill',
            'index' => 4,
            'menu' => array(
                'title' => '<span class="fa fa-upload"></span> ' . __d('syscm', 'Upload Billing'),
                'url' => array('action' => 'upload_cl_bill'),
                'options' => array('escape' => false),
                'active' => false,
            ),
        );
        $invoke[5] = array(
            'perm' => 'edit',
            'alias' => 'customer_bill_template',
            'index' => 5,
            'menu' => array(
                'title' => '<span class="fa fa-file-excel-o"></span> ' . __d('syscm', 'Billing Template'),
                'url' => array('action' => 'customer_bill_template'),
                'options' => array('escape' => false),
                'active' => false,
            ),
        );
        ksort($invoke, SORT_NUMERIC);
        return $invoke;
    }

    public function saveBil($sheetData) {
        $res = array();
        $res['save_error'] = __d('syscm', 'Failed to save. Please try again.');
        if (!$sheetData) {
            return $res;
        }
        unset($sheetData[1]);
        if (!$sheetData) {
            return $res;
        }

        // bill
        $this->virtualFields = array('nosl_bill_month' => "CONCAT(ScmCustBill.text,':',ScmCustBill.annotation, ScmCustBill.name)");
        $this->displayField = 'nosl_bill_month';
        // fetch current list
        $options = array();
        $options['conditions']['AND'][$this->alias . '.active_status'] = 1;
        $options['conditions']['AND'][$this->alias . '.menu_code'] = $this->billMenuCode;
        $current = $this->find('list', $options);
        $current_flipped = array_flip($current);

        //info
        $alias = 'ScmCustInfo';
        $CustInfo = ClassRegistry::init('Syscm.' . $alias);
        $CustInfo->displayField = 'text';

        $options = array();
        $options['conditions']['AND'][$alias . '.active_status'] = 1;
        $options['conditions']['AND'][$alias . '.menu_code'] = $CustInfo->listMenuCode;

        $current_info = $CustInfo->find('list', $options);
        $info_flipped = array_flip($current_info);
        
        //bill detail
        $alias = 'ScmCustBillDetail';
        $BillDetail = ClassRegistry::init('Syscm.' . $alias);
        $BillDetail->displayField = 'scm_customer_id';

        $options = array();
        $options['conditions']['AND'][$alias . '.active_status'] = 1;
        $options['conditions']['AND'][$alias . '.scm_customer_id'] = array_values($current_flipped);

        $current_detail = $BillDetail->find('list', $options);
        $detail_flipped = array_flip($current_detail);
        
        //amount detail
        $alias = 'ScmCustBillAmount';
        $BillAmount = ClassRegistry::init('Syscm.' . $alias);
        $BillAmount->displayField = 'scm_customer_id';

        $options = array();
        $options['conditions']['AND'][$alias . '.active_status'] = 1;
        $options['conditions']['AND'][$alias . '.scm_customer_id'] = array_values($current_flipped);

        $current_amount = $BillAmount->find('list', $options);
        $amount_flipped = array_flip($current_amount);
        //./
        //prepare save based on sheet data
        $data = array();
        /**
         * 					 	 	 
									

         * 'A' => 'H023350',NoSL
          'B' => '2',BlnPakai
          'C' => '2015',ThnPakai
          'D' => '766',StdAwal
          'E' => '773',StdAkhir
          'F' => '7',Kubikasi
          'G' => '37450',RpAir
          'H' => '13750',RpAdm
          'I' => '5000',RpPmh
          'J' => '56200',RpTagihant
         * 
         * 'A' => 'NoSL', ScmCustInfo.text ScmCustBill.text
          'B' => 'BlnPakai', ScmCustBill.name
          'C' => 'ThnPakai', ScmCustBill.annotation
          'D' => 'StdAwal', ScmCustBillDetail.name
          'E' => 'StdAkhir', ScmCustBillDetail.value
          'F' => 'Kubikasi', ScmCustBillDetail.text
          'G' => 'RpAir', ScmCustBillAmount.name
          'H' => 'RpAdm', ScmCustBillAmount.text
          'I' => 'RpPmh', ScmCustBillAmount.menu_code
          'J' => 'RpTagihant', ScmCustBillAmount.value
         */
        $n = 0;
        $info_new = 0;
        $info_replace = 0;
        $new = 0;
        $replace = 0;
        foreach ($sheetData as $i => $v) {
            extract($v);

            $cust = array();
            // check if current nosl available
            $info_new_id = CakeString::uuid();
            //info
            $cust['ScmCustInfo']['id'] = isset($info_flipped[$A]) ? $info_flipped[$A] : $info_new_id;
            if ($cust['ScmCustInfo']['id'] == $info_new_id) {
                $info_new++;
            } else {
                $info_replace++;
            }
            $cust['ScmCustInfo']['text'] = $A;
            $cust['ScmCustInfo']['active_status'] = 1;
            $cust['ScmCustInfo']['menu_code'] = $CustInfo->listMenuCode;
            
            //bill
            $new_id = CakeString::uuid();
            $nosl_bill_month = sprintf('%s:%s%s', $A, $C, $B);
            $cust['ScmCustBill']['id'] = isset($current_flipped[$nosl_bill_month]) ? $current_flipped[$nosl_bill_month] : $new_id;
            if ($cust['ScmCustBill']['id'] == $new_id) {
                $new++;
            } else {
                $replace++;
            }
            $cust['ScmCustBill']['active_status'] = 1;
            $cust['ScmCustBill']['menu_code'] = $this->billMenuCode;
            $cust['ScmCustBill']['text'] = $A;
            $cust['ScmCustBill']['name'] = $B;
            $cust['ScmCustBill']['annotation'] = $C;
            $cust['ScmCustBill']['parent_id'] = $cust['ScmCustBill']['id'];
          /*  'B' => 'BlnPakai', ScmCustBill.name
          'C' => 'ThnPakai', ScmCustBill.annotation*/
            
            //bill detail
            $alias = 'ScmCustBillDetail';
            $cust[$alias]['id'] = isset($detail_flipped[$cust['ScmCustBill']['id']]) ? $detail_flipped[$cust['ScmCustBill']['id']] : CakeString::uuid();
            $cust[$alias]['scm_customer_id'] = $cust['ScmCustBill']['id'];
            /*
              'D' => 'StdAwal', ScmCustBillDetail.name
          'E' => 'StdAkhir', ScmCustBillDetail.value
          'F' => 'Kubikasi', ScmCustBillDetail.text
             */
            $cust[$alias]['name'] = $D;
            $cust[$alias]['value'] = $E;
            $cust[$alias]['text'] = $F;
            $cust[$alias]['active_status'] = 1;
            $cust[$alias]['field'] = $BillDetail->fieldKeyValue;
            
            //bill amount
            $alias = 'ScmCustBillAmount';
            $cust[$alias]['id'] = isset($detail_flipped[$cust['ScmCustBill']['id']]) ? $detail_flipped[$cust['ScmCustBill']['id']] : CakeString::uuid();
            $cust[$alias]['scm_customer_id'] = $cust['ScmCustBill']['id'];
            /*
              'G' => 'RpAir', ScmCustBillAmount.name
          'H' => 'RpAdm', ScmCustBillAmount.text
          'I' => 'RpPmh', ScmCustBillAmount.menu_code
          'J' => 'RpTagihant', ScmCustBillAmount.value
             */
            $cust[$alias]['name'] = $G;
            $cust[$alias]['text'] = $H;
            $cust[$alias]['menu_code'] = $I;
            $cust[$alias]['value'] = $J;
            $cust[$alias]['active_status'] = 1;
            $cust[$alias]['field'] = $BillAmount->fieldKeyValue;
            
            
            $data[] = $cust;
        }
        #debug($data);die();
        $save = $this->saveAll($data, array('validate' => false, 'deep' => true));
        if ($save) {
            $res['save_error'] = false;
            $res['save_success'] = __d('syscm', 'Data upload successfully.');
            if ($new > 0) {
                $res['save_status'][0]['css_class'] = 'alert-success';
                $res['save_status'][0]['status_text'] = __d('syscm', '%d new records.', $new);
            }
            if ($replace > 0) {
                $res['save_status'][1]['css_class'] = 'alert-success';
                $res['save_status'][1]['status_text'] = __d('syscm', '%d records were replaced.', $replace);
            }
        }
        //./
        return $res;
    }

    public function validateExcelFile($request_file_data = array(), $return_file = false) {
        if (!$request_file_data) {
            return false;
        }
        extract($request_file_data);
        if ($size <= 0) {
            return false;
        }
        if ($error) {
            return false;
        }
        $file = new File($tmp_name);
        $fileinfo = $file->info();

        if (!in_array($fileinfo['mime'], array_keys($this->excelMime))) {
            return false;
        }
        if (!$return_file) {
            return true;
        }

        return $file;
    }

}
