<?php

App::uses('SyscmAppModel', 'Syscm.Model');

class Pendaftaran extends SyscmAppModel {
	 /**
     *
     * @var type 
     */
    public $useTable = 'pendaftaran';
    public $displayField = "kode_pendaftaran";
}