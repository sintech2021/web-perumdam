<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('SyscmAppModel', 'Syscm.Model');

/**
 * CakePHP FrontContent
 * @author ino
 */
class FrontContent extends SyscmAppModel
{
    /*
     * 
     */

    public $useTable             = "scm_contents";
    public $displayField         = "name";
    public $alias                = "FrontContent";
    public $published            = "Published";
    public $content_web          = "content_web";
    public $mainlistParentName   = "/topik/berita";
    public $featContName         = "berita_utama";
    public $announcementContName = "pengumuman";
    public $videoGalleryCatName  = "gallery_video";
    public $photoGalleryCatName  = "photo_gallery";
    public $homepagePopupCatName  = "homepage_popup";

    /**
     * Assoc
     */
    public $hasOne    = array(
        'ScmContentContent'   => array(
            'className'  => 'Syscm.ScmContentContent',
            'foreignKey' => 'scm_content_id',
            'conditions' => array(
                'ScmContentContent.active_status' => '1',
                'ScmContentContent.field'         => 'ScmContentContent',
            ),
            'fields'     => '',
        ),
        'ScmContentPublished' => array(
            'className'  => 'Syscm.ScmContentPublished',
            'foreignKey' => 'scm_content_id',
            'conditions' => array(
                'ScmContentPublished.active_status' => '1',
                'ScmContentPublished.field'         => 'ScmContentPublished',
            ),
            'fields'     => '',
        ),
        'ScmContentMeta'      => array(
            'className'  => 'Syscm.ScmContentMeta',
            'foreignKey' => 'scm_content_id',
            'conditions' => array(
                'ScmContentMeta.active_status' => '1',
                'ScmContentMeta.field'         => 'ScmContentMeta',
            ),
            'fields'     => '',
        ),
    );
    public $belongsTo = array(
        'ParentContent' => array(
            'className'  => 'Syscm.FrontContent',
            'foreignKey' => 'parent_id',
            'conditions' => array(
                'ParentContent.active_status' => '1',
                'ParentContent.menu_code'     => 'content_web',
            ),
            'fields'     => '',
            'order'      => ''
        )
    );
    public $hasMany   = array(
        'ChildContent' => array(
            'className'  => 'Syscm.FrontContent',
            'foreignKey' => 'parent_id',
            'conditions' => array(
                'ChildContent.active_status' => '1',
                'ChildContent.menu_code'     => 'content_web',
            ),
            'fields'     => '',
            'order'      => ''
        )
    );

    /**
     * 
     */
    public function findFirstByName($name, $deep_assoc = false)
    {
        $options                                                        = array();
        $options['conditions']['AND'] [$this->alias . '.active_status'] = '1';
        $options['conditions']['AND'] [$this->alias . '.name']          = $name;
        if (!$deep_assoc) {
            $this->recursive = -1;
        }
        return $this->find('first', $options);
    }

    /**
     * 
     */
    public function getContentsByCatname($cat_names = array(), $recursive = 0,
            $find_method = 'all')
    {
        if (!$cat_names) {
            return array();
        }

        $Category = ClassRegistry::init('Syscm.ScmCategory');
        if ($recursive) {
            $Category->recursive = $recursive;
        }
        $options = array();


        $options['conditions']['AND']['ScmCategory.active_status'] = '1';
        $options['conditions']['AND']['ScmCategory.name']          = $cat_names;
        return $Category->find($find_method, $options);
    }

    /**
     * 
     */
    public function getExcerpt($contents, $word_count = 35, $more = "...",
            $options = array())
    {
        $stripped     = filter_var($contents, FILTER_SANITIZE_STRING,
                FILTER_SANITIZE_STRIPPED);
        $words        = str_word_count($stripped, 2);
        $words_indexs = array_keys($words);

        if (isset($words_indexs[$word_count])) {
            $sub_max = $words_indexs[$word_count];
        }
        else {
            $sub_max = end($words_indexs);
            $more    = "";
        }
        return substr($stripped, 0, $sub_max) . $more;
    }

    public function getFeaturedContents()
    {
        $res = array();
        $rs  = $this->getContentsByCatname($this->featContName, 2);
        if ($rs) {
            $i = 0;
            foreach ($rs as $v) {


                if (isset($v['ScmContent'])) {

                    foreach ($v['ScmContent'] as $cont) {
                        $contents = isset($cont['ScmContentContent']) ? $cont['ScmContentContent']['value']
                                    : "";

                        $res[$i] = array(
                            'url'     => $cont['name'],
                            'title'   => $cont['text'],
                            'summary' => $this->getExcerpt($contents, 45),
                            'img_url' => $this->getFirstImg($contents,
                                    '/pdamtkr/sysadmin-logo.png'),
                        );
                        $i++;
                    }
                }
            }
        }
        return $res;
    }

    public function getFirstImg($the_content, $default = "/pdamtkr/logo.png")
    {
        $img_src     = $default;
        $startstrpos = '<img';
        $stoptstrpos = '/>';
        $src         = 'src="';

        $foundpos = strpos($the_content, $startstrpos);
        if ($foundpos):
            $next_content = substr($the_content, $foundpos);
            $foundsrc     = strpos($next_content, $src);
            $next_content = substr($next_content, $foundsrc + strlen($src));
            $foundquoted  = strpos($next_content, '"');
            $img_src      = substr($next_content, 0, $foundquoted);

        endif;
        return $img_src;
    }

    /**
     * 
     */
    public function getFormattedArticle($syscm_contents)
    {

        extract($syscm_contents);
        $article             = array();
        $article['title']    = isset($ScmContent['text']) ? $ScmContent['text'] : "";
        $article['author']   = isset($ScmContentPublished['name']) ? $ScmContentPublished['name']
                    : "";
        $article['subtitle'] = "";
        $article['date']     = isset($ScmContentPublished['text']) ? $ScmContentPublished['text']
                    : "";
        $article['time']     = isset($ScmContentPublished['value']) ? $ScmContentPublished['value']
                    : "";

        $article['url']     = isset($ScmContent['name']) ? $ScmContent['name'] : "";
        $article['content'] = isset($ScmContentContent['value']) ? $ScmContentContent['value']
                    : "";
        return $article;
    }

    /**
     * 
     */
    public function getFormattedMainlist($rs)
    {
        $list = array();
        foreach ($rs as $i => $v) {
            $list[$i] ['url']     = $v[$this->alias]['name'];
            $list[$i] ['title']   = $v[$this->alias]['text'];
            $list[$i] ['content'] = $v['ScmContentContent']['value'];
            $list[$i] ['date']    = $v['ScmContentPublished']['text'];
            $list[$i] ['time']    = $v['ScmContentPublished']['value'];
            $list[$i] ['author']  = $v['ScmContentPublished']['name'];
        }
        return array('list' => $list);
    }

    /**
     * 
     */
    public function getAnnouncements()
    {
        $res = array('view_element' => 'sidelist-announcement');
        $rs  = $this->getContentsByCatname($this->announcementContName, 0,
                'first');

        if (!$rs) {
            return $res;
        }
        if (!isset($rs['ScmContent'])) {
            return $res;
        }
        $res ['title'] = Inflector::humanize($this->announcementContName);
        $res ['url']   = Router::url(array('plugin'     => 'syscm', 'controller' => 'syscm_front',
                    'action'     => 'front_feat_categories', $this->announcementContName),
                        true);
        $res ['list']  = array();
        foreach ($rs['ScmContent'] as $i => $v) {
            $res ['list'][$i]['title'] = $v['text'];
            $res ['list'][$i]['url']   = $v['name'];
            $res ['list'][$i]['id']    = $v['id'];
        }

        return $res;
    }

    /**
     * 
     */
    public function getCatlistPaginateConditions($catname)
    {
        $conditions = array();

        $conditions['AND'][$this->alias . '.active_status'] = '1';
        $conditions['AND'][$this->alias . '.menu_code']     = $this->content_web;
        $conditions['AND']['ParentContent.name']            = $catname;

        return $conditions;
    }

    /**
     * 
     */
    public function getCatlistPaginateSettings()
    {
        return array('limit' => 10, 'order' => array(
                'ScmContentPublished.text'   => 'desc',
                'ScmContentPublished.value'  => 'desc',
                $this->alias . '.menu_order' => 'asc',
        ));
    }

    /**
     * 
     */
    public function getIframeSrc($the_content, $default = "")
    {
        $iframe_src     = $default;
        $startstrpos = '<iframe';
        $stoptstrpos = '/>';
        $src         = 'src="';

        $foundpos = strpos($the_content, $startstrpos);
        if ($foundpos){
            $next_content = substr($the_content, $foundpos);
            $foundsrc     = strpos($next_content, $src);
            $next_content = substr($next_content, $foundsrc + strlen($src));
            $foundquoted  = strpos($next_content, '"');
            $iframe_src      = substr($next_content, 0, $foundquoted);
        }
        return $iframe_src;
    }

    /**
     * 
     */
    public function getHomepagePopup()
    {
        $res = null;
        $content = $this->getContentsByCatname($this->homepagePopupCatName, 0,
                'first');
        if(!$content){
            return $res;
        }
        if(!isset($content['ScmContent'])){
            return $res;
        }
        if(!isset($content['ScmContent'][0])){
            return $res;
        }
        if(!isset($content['ScmContent'][0]['ScmContentContent'])){
            return $res;
        }
        if(!isset($content['ScmContent'][0]['ScmContentContent']['value'])){
            return $res;
        }
        $res = $content['ScmContent'][0]['ScmContentContent']['value'];
        
        return $res;
    }

    /**
     * 
     */
    public function getImageGallery()
    {
        $res = array(
            'view_element' => 'gallerylist-right',
            'url'          => '#',
            'title'        => '',
            'fa_icon'        => 'fa-image',
            'list'         => array()
        );
        $ls  = $this->getContentsByCatname($this->photoGalleryCatName, 0,
                'first');
        if (!$ls) {
            return $res;
        }
        $res['title'] = $ls['ScmCategory']['text'];
        if (!isset($ls['ScmContent'])) {
            return $res;
        }

        if (!$ls['ScmContent']) {
            return $res;
        }
        foreach ($ls['ScmContent'] as $i => $v) {
            $img_src = $this->getFirstImg($v['ScmContentContent']['value'], false);
            if(!$img_src){
                continue;
            }
            $img = sprintf('<img src="%1$s" class="image 12u" />', $img_src);
            $res['list'][$i]['content'] = $img;
        }
        shuffle($res['list']);

        return $res;
    }
    /**
     * 
     */
    public function getMainlistPaginateConditions()
    {
        $conditions = array();

        $conditions['AND'][$this->alias . '.active_status'] = '1';
        $conditions['AND'][$this->alias . '.menu_code']     = $this->content_web;
        $conditions['AND']['ParentContent.name']            = $this->mainlistParentName;

        return $conditions;
    }

    /**
     * 
     */
    public function getMainlistPaginateSettings()
    {
        return array('limit' => 5, 'order' => array(
                'ScmContentPublished.text'   => 'desc',
                'ScmContentPublished.value'  => 'desc',
                $this->alias . '.menu_order' => 'asc',
        ));
    }

    /**
     * 
     */
    public function getVideoGallery()
    {
        $res = array(
            'view_element' => 'gallerylist-right',
            'url'          => '#',
            'title'        => '',
            'fa_icon'        => 'fa-film',
            'list'         => array()
        );
        $ls  = $this->getContentsByCatname($this->videoGalleryCatName, 0,
                'first');
        if (!$ls) {
            return $res;
        }
        $res['title'] = $ls['ScmCategory']['text'];
        if (!isset($ls['ScmContent'])) {
            return $res;
        }

        if (!$ls['ScmContent']) {
            return $res;
        }
        foreach ($ls['ScmContent'] as $i => $v) {
            $iframe_src = $this->getIframeSrc($v['ScmContentContent']['value']);
            $iframe = sprintf('<iframe src="%1$s" class="12u image" allowfullscreen="allowfullscreen"><a href="%1$s" target="_blank">View Video</a></iframe>', $iframe_src);
            $res['list'][$i]['content'] = $iframe;
        }
        shuffle($res['list']);

        return $res;
    }

    /**
     * 
     */
    public function searchContent($keyword)
    {
        $options = array();
        if (!$keyword) {
            return $options;
        }

        // content: menu_code = content_web
        // Content.text
        // Content.name
        // Content.annotation
        // ContentContent.annotation
        // ContentDetail.value
        // ContentDetail.text

        $options['conditions']['AND'][$this->alias . '.active_status']          = '1';
        $options['conditions']['AND'][$this->alias . '.menu_code']              = $this->content_web;
        $options['conditions']['AND']['OR'][$this->alias . '.text LIKE ']       = sprintf('%%%s%%',
                $keyword);
        $options['conditions']['AND']['OR'][$this->alias . '.name LIKE ']       = sprintf('%%%s%%',
                $keyword);
        $options['conditions']['AND']['OR'][$this->alias . '.annotation LIKE '] = sprintf('%%%s%%',
                $keyword);
        $options['conditions']['AND']['OR']['ScmContentContent.value LIKE ']    = sprintf('%%%s%%',
                $keyword);

        #$log = $this->getDataSource()->getLog(false, false);
#debug($log);
        #debug($data);die();
        return $options;
    }

}
