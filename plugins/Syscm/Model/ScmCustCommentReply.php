<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('SyscmAppModel', 'Syscm.Model');

/**
 * CakePHP ScmMenuListGroup
 * @author ino
 */
class ScmCustCommentReply extends SyscmAppModel {

    /**
     *
     * @var type 
     */
    public $useTable = 'scm_customers';
    public $threadReplyMenuCode = 'CommentThreadReply';
    public $displayField = "name";
    public $threadLimit = 10;

    /**
     * Association
     */
    public $belongsTo = array(
        'ScmCustComment' => array(
            'className' => 'Syscm.ScmCustComment',
            'foreignKey' => 'parent_id',
            'conditions' => array(
                'ScmCustComment.active_status' => '1',
                'ScmCustComment.menu_code' => 'CommentThread',
            ),
            'fields' => '',
        ),
    );
    public $hasOne = array(
        'ScmCustCommentPhone' => array(
            'className' => 'Syscm.ScmCustCommentPhone',
            'foreignKey' => 'scm_customer_id',
            'conditions' => array(
                'ScmCustCommentPhone.active_status' => '1',
                'ScmCustCommentPhone.field' => 'ScmCustCommentPhone',
            ),
            'fields' => '',
        ),
        'ScmCustCommentEmail' => array(
            'className' => 'Syscm.ScmCustCommentEmail',
            'foreignKey' => 'scm_customer_id',
            'conditions' => array(
                'ScmCustCommentEmail.active_status' => '1',
                'ScmCustCommentEmail.field' => 'ScmCustCommentEmail',
            ),
            'fields' => '',
        ),
        'ScmCustCommentIp' => array(
            'className' => 'Syscm.ScmCustCommentIp',
            'foreignKey' => 'scm_customer_id',
            'conditions' => array(
                'ScmCustCommentIp.active_status' => '1',
                'ScmCustCommentIp.field' => 'ScmCustCommentIp',
            ),
            'fields' => '',
        ),
    );


      /**
     * Delete
     */
    public function dropStatus($id) {

        $result = false;
        if ($this->exists($id)) {

            $this->id = $id;
            $data = array(
                $this->alias => array(
                    'active_status' => '0'
                )
            );
            $result = $this->save($data, false);
        }

        return $result;
    }


}
