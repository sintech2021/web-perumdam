<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('SyscmAppModel', 'Syscm.Model');

/**
 * CakePHP FrontModel
 * @author ino
 */
class FrontModel extends SyscmAppModel
{

    /**
     *
     * @var type 
     */
    public $useTable         = "scm_menus";
    public $displayField     = "name";
    public $alias            = "FrontModel";
    public $menuListName     = "menu_list";
    public $menuGroupName    = "menu_group";
    public $footerIconsGroupName    = "social_media";
    public $mainNavGroupName = "main_nav";
    public $bannerHomeName   = "banner_home";
    public $menuWebLinkName  = "link_url_instansi";

    /**
     * Associations
     */
    public $belongsTo = array(
        'ParentMenu' => array(
            'className'  => 'Syscm.FrontModel',
            'foreignKey' => 'parent_id',
            'conditions' => array(
                'ParentMenu.active_status' => '1',
                'ParentMenu.menu_code'     => 'menu_list',
            ),
            'fields'     => '',
            'order'      => ''
        )
    );
    public $hasOne    = array(
        'ScmMenuSlug' => array(
            'className'  => 'Syscm.ScmMenuSlug',
            'foreignKey' => 'scm_menu_id',
            'conditions' => array('ScmMenuSlug.menu_code' => 'menu_slug'),
        ),
    );
    public $hasMany   = array(
        'ChildMenu' => array(
            'className'  => 'Syscm.FrontModel',
            'foreignKey' => 'parent_id',
            'conditions' => array(
                'ChildMenu.active_status' => '1',
                'ChildMenu.menu_code'     => 'menu_list',
            ),
            'order'      => 'ChildMenu.menu_order ASC',
        )
    );

    /**
     * 
     */
    public function formatSlug($request_pass = array(), $prefix = '/web/')
    {

        $slug           = $request_pass   = implode("/", $request_pass);
        $formatted_slug = $prefix . $slug;
        return $formatted_slug;
    }

    /**
     * 
     */
    public function getFooterIcons()
    {
        $res = array();
        $ids = $this->getMenuListByGroup($this->footerIconsGroupName);
        if(!$ids){
            return $res;
        }
        $options = array(
            'conditions'=> array(
                'FrontModel.id' => $ids,
                'FrontModel.active_status' => '1',
            ),
            'order'=> array(
                'FrontModel.menu_order' => 'asc'
            )
        );
        $this->recursive = 1;
        $ls = $this->find('all', $options);
        if(!$ls){
            return $res;
        }
        foreach($ls as $i => $v){
            $res[$i]['url'] = $v['ScmMenuSlug']['value'];
            $res[$i]['text'] = $v['FrontModel']['text'];
            $res[$i]['content'] = $v['FrontModel']['annotation'];
        }
        return $res;
    }
    /**
     * 
     */
    public function getListByParentName_v2($name, $exclude_ids = array())
    {
        $Content = ClassRegistry::init('Syscm.ScmContent');
        $list    = array(
            'title'       => '',
            'url'         => '',
            'parent_name' => null,
            'id'          => null,
            'list'        => array()
        );

        $options               = array();
        $options['conditions'] = array(
            'AND' => array(
                'ScmContent.active_status'      => '1',
                'ScmContent.name'               => $name,
                'ScmContentPublished.menu_code' => 'Published',
            )
        );

        $Content->recursive = 2;
        $content            = $Content->find('first', $options);

        print_r($content);
        $dbo = $this->getDatasource();
        $logs = $dbo->getLog();
        $lastLog = end($logs['log']);
        print_r($dbo);
        exit();
        if (!$content) {
            return $list;
        }
        

        $list['title'] = $content['ScmContent']['text'];
        $list['url']   = $content['ScmContent']['name'];
        $list['id']    = $content['ScmContent']['id'];
        if (isset($content['ParentContent']['name'])) {
            if ($content['ParentContent']['name']) {
                $list['parent_name'] = $content['ParentContent']['name'];
            }
        }

        if (!$content['ChildContent']) {
            return $list;
        }

        foreach ($content['ChildContent'] as $i => $v) {

            if (in_array($v['id'], $exclude_ids)) {
                continue;
            }

            if (!$v['ScmContentPublished']) {
                continue;
            }
            if (!isset($v['ScmContentPublished']['menu_code'])) {
                continue;
            }
            if ($v['ScmContentPublished']['menu_code'] != 'Published') {
                continue;
            }
            $item = array();
            $item['title']       = $v['text'];
            $item['subtitle']    = "";
            $item['meta_author'] = $v['ScmContentPublished']['name'];
            $item['date']        = $v['ScmContentPublished']['text'];
            $item['time']        = $v['ScmContentPublished']['value'];
            $item['url']         = $v['name'];
            $item['content']     = $this->fixSnippet(trim(str_replace('&nbsp;',' ', substr(strip_tags($v['ScmContentContent']['value']),0,150)))) . '...';//$v['ScmContentContent']['value'];
            $item['id']     = $v['ScmContentContent']['scm_content_id'];
            $item['pk']     = $v['ScmContentContent']['id'];
            $item['dt']    = $this->format_dt($v['ScmContentPublished']['text'], $v['ScmContentPublished']['value'] );
            $item['bgurl']    = $this->getFirstImg($v['ScmContentContent']['value']);

            $list['list'][] = $item;
        }
        #debug($list);die();
        return $list;
    }
    public function getListByParentName($name, $exclude_ids = array())
    {
        $Content = ClassRegistry::init('Syscm.ScmContent');
        $list    = array(
            'title'       => '',
            'url'         => '',
            'parent_name' => null,
            'id'          => null,
            'list'        => array()
        );

        $options               = array();
        $options['conditions'] = array(
            'AND' => array(
                'ScmContent.active_status'      => '1',
                'ScmContent.name'               => $name,
                'ScmContentPublished.menu_code' => 'Published',
            )
        );

        $Content->recursive = 2;
        $content            = $Content->find('first', $options);
        if (!$content) {
            return $list;
        }
        

        $list['title'] = $content['ScmContent']['text'];
        $list['url']   = $content['ScmContent']['name'];
        $list['id']    = $content['ScmContent']['id'];
        if (isset($content['ParentContent']['name'])) {
            if ($content['ParentContent']['name']) {
                $list['parent_name'] = $content['ParentContent']['name'];
            }
        }

        if (!$content['ChildContent']) {
            return $list;
        }

        foreach ($content['ChildContent'] as $i => $v) {

            if (in_array($v['id'], $exclude_ids)) {
                continue;
            }

            if (!$v['ScmContentPublished']) {
                continue;
            }
            if (!isset($v['ScmContentPublished']['menu_code'])) {
                continue;
            }
            if ($v['ScmContentPublished']['menu_code'] != 'Published') {
                continue;
            }
            $item = array();
            $item['title']       = $v['text'];
            $item['subtitle']    = "";
            $item['meta_author'] = $v['ScmContentPublished']['name'];
            $item['date']        = $v['ScmContentPublished']['text'];
            $item['time']        = $v['ScmContentPublished']['value'];
            $item['url']         = $v['name'];
            $item['content']     = $this->fixSnippet(trim(str_replace('&nbsp;',' ', substr(strip_tags($v['ScmContentContent']['value']),0,150)))) . '...';//$v['ScmContentContent']['value'];
            $item['id']     = $v['ScmContentContent']['scm_content_id'];
            $item['pk']     = $v['ScmContentContent']['id'];
            $item['dt']    = $this->format_dt($v['ScmContentPublished']['text'], $v['ScmContentPublished']['value'] );
            $item['bgurl']    = $this->getFirstImg($v['ScmContentContent']['value']);

            $list['list'][] = $item;
        }
        #debug($list);die();
        return $list;
    }
    public function format_dt($dt,$tt)
    {
        $hari = array ( 1 =>    'Senin',
                'Selasa',
                'Rabu',
                'Kamis',
                'Jumat',
                'Sabtu',
                'Minggu'
            );
            
        $bulan = array (1 =>   'Januari',
                    'Februari',
                    'Maret',
                    'April',
                    'Mei',
                    'Juni',
                    'Juli',
                    'Agustus',
                    'September',
                    'Oktober',
                    'November',
                    'Desember'
                );
        $strpubdate = strtotime($dt);
        $postday    =  date("d", $strpubdate) ;
        $postdate_l   = date("N", $strpubdate);
        $postdate   = $hari[$postdate_l];
        $postmonth_n = date('n',$strpubdate);
        $postmonth  = $bulan[$postmonth_n];//__d('syscm', date("F", $strpubdate));
        $postyear   = date("Y", $strpubdate);
        $posttime   = isset($tt) ? sprintf('%s ',
                        $tt) : '';
        return $post_date  = sprintf(
                '%s ' .
               
                '%s ' .
                '%s, ' .
                '%s ' ,  $postday,  $postmonth, $postyear, $posttime);
    }
    function fixSnippet($text){
        $words = explode(' ',$text);
        array_pop($words);
        return implode(' ', $words);
    }
    public function getFirstImg($the_content, $default = "/pdamtkr/logo.png")
    {
        $img_src     = $default;
        $startstrpos = '<img';
        $stoptstrpos = '/>';
        $src         = 'src="';

        $foundpos = strpos($the_content, $startstrpos);
        if ($foundpos):
            $next_content = substr($the_content, $foundpos);
            $foundsrc     = strpos($next_content, $src);
            $next_content = substr($next_content, $foundsrc + strlen($src));
            $foundquoted  = strpos($next_content, '"');
            $img_src      = substr($next_content, 0, $foundquoted);

        endif;
        return $img_src;
    }
    /**
     * 
     */
    public function getNewsWidgetHome($options = array())
    {
        /**
         * 
         * [title] => String,
         * [list] => 
         *      [] =>
         *          [title] => String,
         *          [content] => String,
         *          [url] => CakeUrl
         * 
         */
        $widget = array(
            'title' => 'Test',
            'list'  => array(
                array(
                    'title'   => 'sub head',
                    'content' => 'Phasellus quam turpis, feugiat sit amet ornare in, hendrerit in lectus. Praesent semper mod quis eget mi. Etiam eu ante risus. Aliquam erat volutpat. Aliquam luctus et mattis lectus sit amet pulvinar. Nam turpis nisi consequat etiam lorem ipsum dolor sit amet nullam.',
                    'url'     => '#',
                ),
                array(
                    'title'   => 'sub head 32',
                    'content' => 'Phasellus quam turpis, feugiat sit amet ornare in, hendrerit in lectus. Praesent semper mod quis eget mi. Etiam eu ante risus. Aliquam erat volutpat. Aliquam luctus et mattis lectus sit amet pulvinar. Nam turpis nisi consequat etiam lorem ipsum dolor sit amet nullam.',
                    'url'     => '#',
                )
            ),
        );
        return $widget;
    }

    /**
     * 
     */
    public function getSyscmBannerHome($options = array())
    {
        $Album            = ClassRegistry::init('Syscm.ScmContentMediaGroup');
        $options          = array(
            'conditions' => array(
                'AND' => array(
                    'ScmContentMediaGroup.active_status' => '1',
                    'ScmContentMediaGroup.name'          => $this->bannerHomeName,
                ),
            ),
        );
        #$Album->recursive = 2;
        # debug($Album->find('first', $options));die();
        $Album->recursive = 2;
        $album            = $Album->find('first', $options);
        return $album;
    }

    /**
     * 
     */
    public function getMenuListByGroup($group_name = "")
    {   

        $res = array();
        if (!$group_name) {
            return $res;
        }

        $options         = array('conditions' => array(
                $this->alias . '.active_status' => '1',
                $this->alias . '.menu_code'     => $this->menuGroupName,
                $this->alias . '.parent_id'     => NULL,
                $this->alias . '.name'          => $group_name,
            ),
        );
        $this->recursive = -1;

        $group = $this->find('first', $options);
       

        if (!$group) {
            return $res;
        }
        $gid = $group['FrontModel']['id'];
        if (!$gid) {
            return $res;
        }
        //field = $gid
        $MenuGroup            = ClassRegistry::init('Syscm.ScmMenuListGroup');
        $MenuGroup->recursive = -1;

        $MenuGroup->displayField = 'value';

        $options = array(
            'conditions' => array(
                'ScmMenuListGroup.field' => $gid,
            )
        );
        $rs      = $MenuGroup->find('list', $options);
        if (!$rs) {
            return $res;
        }
       

        $res = array_values($rs);
 // print_r($res);
        return $res;
    }

    /**
     * 
     */
    public function getMenuWebLink($group_name = "", $view_element = 'web_link', $title = 'Web Link', $fa_icon = "fa-external-link", $target = "_blank")
    {
        $res = array(
            'view_element'=>$view_element,
            'title'=>__d('syscm',$title),
            'fa_icon'=> $fa_icon,
            'list'=>array()
            );
        $group_name = !$group_name ? $this->menuWebLinkName : $group_name;
        $ids = $this->getMenuListByGroup($group_name);
        if(!$ids){
            return $res;
        }
        $options = array(
            'conditions'=> array(
                'FrontModel.id' => $ids
            ),
            'order'=> array(
                'FrontModel.menu_order' => 'asc'
            )
        );
        $this->recursive = 1;
        $ls = $this->find('all', $options);
        #debug($ls);die();
        if(!$ls){
            return $res;
        }
        $FrontContent = ClassRegistry::init('Syscm.FrontContent');
        foreach($ls as $i=>$v){
            $res['list'][$i]['title'] = $v['FrontModel']['text'];
            $res['list'][$i]['id'] = $v['FrontModel']['id'];
            $res['list'][$i]['alt'] = $v['FrontModel']['text'];
            $res['list'][$i]['url'] = $v['ScmMenuSlug']['value'];
            $res['list'][$i]['img_url'] = $FrontContent->getFirstImg($v['FrontModel']['annotation']);
            $res['list'][$i]['target'] = $target;
        }
        return $res;
    }

    /**
     * 
     */
    public function getSyscmMainNav()
    {
        $ids = $this->getMenuListByGroup($this->mainNavGroupName);

        $options = array(
            'conditions' => array(
                $this->alias . '.active_status' => '1',
                $this->alias . '.menu_code'     => $this->menuListName,
                $this->alias . '.parent_id'     => NULL,
            ),
            'order'      => $this->alias . '.menu_order',
        );
        if ($ids) {
            $options['conditions'][$this->alias . '.id'] = $ids;
        }
        $this->recursive = 2;
        $rs              = $this->find('all', $options);
        return $rs;
    }

}
