<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('SyscmAppModel', 'Syscm.Model');

/**
 * CakePHP ScmMenuListGroup
 * @author ino
 */
class ScmCustFeedbackView extends SyscmAppModel {

    /**
     *
     * @var type 
     */
    public $useTable      = 'scm_feedback';
    public $menuCode      = 'CustomerFeedback';
    public $displayField  = "name";
    public $topMenuRevoke = array('add');
    public $feedbackLimit = 10;

    /**
     * Association
     */
    public $hasOne = array(
        'ScmCustFeedbackContact' => array(
            'className'  => 'Syscm.ScmCustFeedbackContact',
            'foreignKey' => 'scm_customer_id',
            'conditions' => array(
                'ScmCustFeedbackContact.active_status' => '1',
                'ScmCustFeedbackContact.field'         => 'ScmCustFeedbackContact',
            ),
            'fields'     => '',
        ),
        'ScmCustFeedbackStatus' => array(
            'className'  => 'Syscm.ScmCustFeedbackStatus',
            'foreignKey' => 'scm_customer_id',
            'conditions' => array(
                'ScmCustFeedbackStatus.active_status' => '1',
                'ScmCustFeedbackStatus.field'         => 'ScmCustFeedbackStatus',
            ),
            'fields'     => '',
        ),
    );

    /* public $hasAndBelongsToMany = array(
      'ScmCategory' =>
      array(
      'className' => 'Syscm.ScmCategory',
      'joinTable' => 'scm_contents_categories',
      'foreignKey' => 'scm_content_id',
      'associationForeignKey' => 'scm_category_id',
      'unique' => 'keepExisting',
      'conditions' => '',
      'fields' => '',
      'order' => '',
      'limit' => '',
      'offset' => '',
      'finderQuery' => '',
      'with' => null
      )
      );

      public $belongsTo = array(
      'ParentContent' => array(
      'className'  => 'Syscm.ScmFeedback',
      'foreignKey' => 'parent_id',
      'conditions' => array(
      'ParentContent.active_status'=> '1',
      'ParentContent.menu_code'=> 'content_feedback',
      ),
      'fields'     => '',
      'order'      => ''

      )
      );

      public $hasMany = array(
      'ChildContent' => array(
      'className'  => 'Syscm.ScmFeedback',
      'foreignKey' => 'parent_id',
      'conditions' => array(
      'ChildContent.active_status'=> '1',
      'ChildContent.menu_code'=> 'content_feedback',
      ),
      'fields'     => '',
      'order'      => ''

      )
      );
     */

    /**
     * Validations
     */
    public $validate = array(
        'text' => array(
            'notBlank' => array(
                'rule'    => 'notBlank',
                'message' => 'This value may not be left empty!'
            ),
        ),
        'name' => array(
            'notBlank' => array(
                'rule'    => 'notBlank',
                'message' => 'This value may not be left empty!'
            ),
        )
    );

    /**
     * Delete
     */
    public function dropStatus($id) {

        $result = false;
        if ($this->exists($id)) {

            $this->id = $id;
            $data     = array(
                $this->alias => array(
                    'active_status' => '0'
                )
            );
            $result   = $this->save($data, false);
        }

        return $result;
    }

    /**
     * 
     * @param type $id
     */
//    public function getCategories()
//    {
//        $ScmCategory = ClassRegistry::init('Syscm.ScmCategory');
//        $conditions = array(
//            'AND' => array(
//                'ScmCategory.active_status' => '1',
//                'ScmCategory.menu_code' => $ScmCategory->menuCodeContentCategory,
//            )
//            
//        );
//        $ScmCategory->virtualFields = array('display'=> "CONCAT(ScmCategory.text, ' (', ScmCategory.name, ') ')");
//        $ScmCategory->displayField = 'display';
//        $options = array('conditions'=> $conditions);
//        $options['order'] = array('ScmCategory.text'=>'asc');
//        return $ScmCategory->find('list', $options);
//    }
//    
//    /**
//     * 
//     * @param type $id
//     */
//    public function getParents($exclude_ids = array())
//    {
//        
//        $conditions = array(
//            'AND' => array(
//                $this->alias.'.active_status' => '1',
//                $this->alias.'.menu_code' => $this->menuCode,
//            )
//            
//        );
//        if($exclude_ids){
//            $conditions['AND']['NOT'][$this->alias.'.id'] = $exclude_ids;
//        }
//        $this->Behaviors->load('Tree');
//        $this->recover();
//        return $this->generateTreeList($conditions);
//    }
//   
    public function findById($id, $find = 'first', $order = array()) {
        $options = array(
            'conditions' => array(
                $this->alias . '.id'          => $id,
                $this->alias . '.active_status' => '1',
                $this->alias . '.menu_code'     => $this->menuCode,
            )
        );
        if ($order) {
            $options['order'] = $order;
        }
        return $this->find($find, $options);
    }
    
    public function findByNoSl($nosl, $find = 'first', $order = array()) {
        $options = array(
            'conditions' => array(
                $this->alias . '.text'          => $nosl,
                $this->alias . '.active_status' => '1',
                $this->alias . '.menu_code'     => $this->menuCode,
            )
        );
        if ($order) {
            $options['order'] = $order;
        }
        return $this->find($find, $options);
    }

    public function findByNoslFormatted($nosl) {
        $res = array();
        $rs  = $this->findByNoSl($nosl, 'all', array($this->alias . '.created' => 'desc'));
        if (!$rs) return $res;
        foreach ($rs as $key => $val) {
            $res['list'][$key]['post_feedback'] = $val['ScmCustFeedback']['annotation'];
            $res['list'][$key]['author']        = $val['ScmCustFeedback']['name'];
            $res['list'][$key]['date']        = $val['ScmCustFeedback']['created'];
            $res['list'][$key]['time']        = date('H:i:s',  strtotime($val['ScmCustFeedback']['created']));
        }
        return $res;
    }

    public function formatCustHistory($rs){
        $res = array();
        
        if (!$rs) return $res;
        foreach ($rs as $key => $val) {
            $res['list'][$key]['scm_customer_id'] = $val['ScmCustFeedback']['id'];
            $res['list'][$key]['post_feedback'] = $val['ScmCustFeedback']['annotation'];
            $res['list'][$key]['author']        = $val['ScmCustFeedback']['name'];
            $res['list'][$key]['date']        = $val['ScmCustFeedback']['created'];
            $res['list'][$key]['time']        = date('H:i:s',  strtotime($val['ScmCustFeedback']['created']));
            
        }
        return $res;
    }
    
    public function getFeedbackHistoryPaginateSettings(){
        $settings = array(
            $this->alias => array(
				'fields' => '*',
                'order' => array('created' => 'desc'),
                'limit' => $this->feedbackLimit,
            )
                
        );
        return $settings;
    }
    
    public function invokeTopMenuOnDetail(){
        $invoke = array();
        $invoke[1] = array(
            'perm' => 'edit',
            'alias' => 'detail',
            'index' => 1,
            'menu' => array(
                'title' => '<span class="fa fa-table"></span> ' . __d('syscm', 'Detail'),
                'url' => array('action' => 'detail'),
                'options' => array('escape' => false),
                'active' => false,
            ),
        );
        $invoke[2] = array(
            'perm' => 'edit',
            'alias' => 'add',
            'index' => 2,
            'menu' => array(
                'title' => '<span class="fa fa-reply fa-flip-horizontal"></span> ' . __d('syscm', 'Add respon'),
                'url' => array('action' => 'add'),
                'options' => array('escape' => false),
                'active' => false,
            ),
        );
        ksort($invoke, SORT_NUMERIC);
        return $invoke;
    }
    
    public function invokeTopMenuOnEdit(){
        $invoke = $this->invokeTopMenuOnDetail();
        
        $invoke[3] = array(
            'perm' => 'edit',
            'alias' => 'edit',
            'index' => 3,
            'menu' => array(
                'title' => '<span class="fa fa-pencil"></span> ' . __d('syscm', 'Edit respon'),
                'url' => array('action' => 'edit'),
                'options' => array('escape' => false),
                'active' => false,
            ),
        );
        ksort($invoke, SORT_NUMERIC);
        return $invoke;
    }
    /**
     * 
     */
    public function saveFeedback($posted_data) {
        if (!$posted_data) {
            return false;
        }
        extract($posted_data);
        /**
         * array(
          'cust_feedback_name' => 'WALUYO',
          'cust_feedback_nosl' => 'H022361',
          'cust_feedback_email' => 'email@email',
          'cust_feedback_phone' => 'asdasd',
          'cust_feedback_cell' => '123456',
          'cust_feedback_msg' => 'test'
          )
         */
        $data                                     = array();
        $feedback_id                              = CakeString::uuid();
        $data['ScmCustFeedback']['id']            = $feedback_id;
        $data['ScmCustFeedback']['active_status'] = '1';
        $data['ScmCustFeedback']['menu_code']     = $this->menuCode;
        $data['ScmCustFeedback']['name']          = $cust_feedback_name;
        $data['ScmCustFeedback']['text']          = $cust_feedback_nosl;
        $data['ScmCustFeedback']['annotation']    = $cust_feedback_msg;

        $data['ScmCustFeedbackContact']['id']              = CakeString::uuid();
        $data['ScmCustFeedbackContact']['scm_customer_id'] = $feedback_id;
        $data['ScmCustFeedbackContact']['name']            = $cust_feedback_email;
        $data['ScmCustFeedbackContact']['text']            = $cust_feedback_phone;
        $data['ScmCustFeedbackContact']['field']           = 'ScmCustFeedbackContact';
        $data['ScmCustFeedbackContact']['value']           = $cust_feedback_cell;
        $data['ScmCustFeedbackContact']['menu_code']       = $client_ip;
        $data['ScmCustFeedbackContact']['active_status']   = 1;

        $options = array('validate' => false);
        return $this->saveAssociated($data, $options);
    }
	
	function paginate($conditions, $fields, $order, $limit, $page = 1, $recursive = null, $extra = array()){
			$row_start = ($page-1) * $limit;
			$order_by = "";
			
			foreach($order as $orderBy=>$val){
				$orders[] = $orderBy." ".$val;
			}
			
			foreach($conditions["AND"] as $filtered=>$val){
				$filter[] = " (".$filtered." = '".$val."') ";
			}
			
			if($limit==1){
				$limiter = "";
			}else{
				$limiter = " LIMIT $row_start, $limit";
			}
			$order_by = implode(",",$orders);
			$filters = implode(" AND ",$filter);
			$query 	   =  "SELECT $fields FROM sys_scm_feedback AS ScmCustFeedbackView
			LEFT JOIN( SELECT scm_customer_id,COUNT(*) AS `count` FROM sys_scm_customers_details AS ScmCustFeedbackRespon WHERE ((active_status = 1) AND (field = 'ScmCustFeedbackRespon')) GROUP BY scm_customer_id ) AS ScmCustFeedbackViewReplied ON ( ScmCustFeedbackViewReplied.scm_customer_id=ScmCustFeedbackView.id) WHERE ($filters) ORDER BY $order_by  $limiter";
			
			return $this->query($query);
	}
}



/**
 * Feedback main schema desc
 * 
 * name := feedback sender
 * text := no sl
 * annotation := feedback content
 * active_status := 1
 * menu_code := CustomerFeedback
 * 
 * ScmCustFeedbackContact
 * ['ScmCustFeedbackContact']['scm_customer_id'] = ['ScmCustFeedback']['id'],
 * ['ScmCustFeedbackContact']['name'] = 'sender@email',
 * ['ScmCustFeedbackContact']['text'] = 'sender phone',
 * ['ScmCustFeedbackContact']['value'] = 'sender phone',
 * ['ScmCustFeedbackContact']['menu_code'] = client_ip,
 * 
 */
