<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('SyscmAppModel', 'Syscm.Model');

/**
 * CakePHP ScmMenuListGroup
 * @author ino
 */
class ScmCustComment extends SyscmAppModel {

    /**
     *
     * @var type 
     */
    public $useTable                 = 'scm_customers';
    public $threadMenuCode           = 'CommentThread';
    public $threadReplyMenuCode      = 'CommentThreadReply';
    public $threadReplyAdminMenuCode = 'CommentThreadReplyAdmin';
    public $displayField             = "name";
    public $threadLimit              = 5;
    public $topMenuRevoke            = array('add');

    /**
     * Association
     */
    public $belongsTo = array(
        'ScmContent' => array(
            'className'  => 'Syscm.ScmContent',
            'foreignKey' => 'text',
            'conditions' => array(
                'ScmContent.active_status' => '1',
                'ScmContent.menu_code'     => 'content_web',
            ),
            'fields'     => '',
        ),
    );
    public $hasMany   = array(
        'ScmCustCommentReply' => array(
            'className'  => 'Syscm.ScmCustCommentReply',
            'foreignKey' => 'parent_id',
            'conditions' => array(
                'ScmCustCommentReply.active_status' => '1',
                'ScmCustCommentReply.menu_code'     => 'CommentThreadReply',
            ),
            'fields'     => '',
            'order'      => ''
        )
    );
    public $hasOne    = array(
        'ScmCustCommentPhone' => array(
            'className'  => 'Syscm.ScmCustCommentPhone',
            'foreignKey' => 'scm_customer_id',
            'conditions' => array(
                'ScmCustCommentPhone.active_status' => '1',
                'ScmCustCommentPhone.field'         => 'ScmCustCommentPhone',
            ),
            'fields'     => '',
        ),
        'ScmCustCommentEmail' => array(
            'className'  => 'Syscm.ScmCustCommentEmail',
            'foreignKey' => 'scm_customer_id',
            'conditions' => array(
                'ScmCustCommentEmail.active_status' => '1',
                'ScmCustCommentEmail.field'         => 'ScmCustCommentEmail',
            ),
            'fields'     => '',
        ),
        'ScmCustCommentIp'    => array(
            'className'  => 'Syscm.ScmCustCommentIp',
            'foreignKey' => 'scm_customer_id',
            'conditions' => array(
                'ScmCustCommentIp.active_status' => '1',
                'ScmCustCommentIp.field'         => 'ScmCustCommentIp',
            ),
            'fields'     => '',
        ),
        'ScmCustCommentRead'    => array(
            'className'  => 'Syscm.ScmCustCommentRead',
            'foreignKey' => 'scm_customer_id',
            'conditions' => array(
                'ScmCustCommentRead.active_status' => '1',
                'ScmCustCommentRead.field'         => 'ScmCustCommentRead',
            ),
            'fields'     => '',
        ),
    );

    /**
     * Delete
     */
    public function dropStatus($id) {

        $result = false;
        if ($this->exists($id)) {

            $this->id = $id;
            $data     = array(
                $this->alias => array(
                    'active_status' => '0'
                )
            );
            $result   = $this->save($data, false);
        }
        if($result){
            $CustDetail = ClassRegistry::init('Syscm.ScmCustDetail');
            $CustDetail->dropStatusByCustId($id);
        }
        return $result;
    }

    
    public function findById($id){
        $options = array();
        $conditions = array(
            'ScmCustComment.id' => $id,
            'ScmCustComment.active_status' => 1,
            'ScmCustComment.menu_code' => $this->threadMenuCode,
        );
        $options = compact('conditions');
        
        return $this->find('first', $options);
    }
    /**
     * 
     * @param type $id
     */
    public function getCategories() {
        $ScmCategory                = ClassRegistry::init('Syscm.ScmCategory');
        $conditions                 = array(
            'AND' => array(
                'ScmCategory.active_status' => '1',
                'ScmCategory.menu_code'     => $ScmCategory->menuCodeContentCategory,
            )
        );
        $ScmCategory->virtualFields = array('display' => "CONCAT(ScmCategory.text, ' (', ScmCategory.name, ') ')");
        $ScmCategory->displayField  = 'display';
        $options                    = array('conditions' => $conditions);
        $options['order']           = array('ScmCategory.text' => 'asc');
        return $ScmCategory->find('list', $options);
    }

    public function getIndexPaginationSetting() {
        $settings                                  = array();
        $settings['order'][$this->alias . '.text'] = 'asc';
//        debug($settings);die();
        return $settings;
    }

    /**
     * 
     * @param type $id
     */
    public function getParents($exclude_ids = array()) {

        $conditions = array(
            'AND' => array(
                $this->alias . '.active_status' => '1',
                $this->alias . '.menu_code'     => $this->menuCode,
            )
        );
        if ($exclude_ids) {
            $conditions['AND']['NOT'][$this->alias . '.id'] = $exclude_ids;
        }
        $this->Behaviors->load('Tree');
        $this->recover();
        return $this->generateTreeList($conditions);
    }

    /**
     * 
     * @param type $content_id
     * @param type $comment_page
     */
    public function getThreadByContent($content_id, $comment_page, $reply_cid, $reply_page) {
        $res   = array(
            'count'          => 0,
            'comment_paging' => array(
                'count'        => 0,
                'comment_page' => $comment_page,
                'limit'        => $this->threadLimit,
            ),
            'comments'       => array()
        );
        $limit = $this->threadLimit;

        $options = array(
            'conditions' => array(
                'AND' => array(
                    'ScmCustComment.text'          => $content_id,
                    'ScmCustComment.active_status' => '1',
                    'ScmCustComment.menu_code'     => $this->threadMenuCode,
                ),
            ),
        );

        $res['comment_paging']['count'] = $this->find('count', $options);
        if ($res['comment_paging']['count'] <= $this->threadLimit) {
            $comment_page = 1;
        }

        $last_page = ceil($res['comment_paging']['count'] / $this->threadLimit);
        if ($comment_page > $last_page) {
            $comment_page = $last_page;
        }

        $res['comment_paging']['comment_page'] = $comment_page;

        $offset = (($comment_page - 1) * $limit);
        if ($offset < 0) {
            $offset = 0;
        }

        $options['order']  = array('ScmCustComment.modified' => 'desc');
        $options['limit']  = $limit;
        $options['offset'] = $offset;

        $res['count'] = $res['comment_paging']['count'];
        $comments     = $this->find('all', $options);
        if (!$comments) {
            return $res;
        }

        foreach ($comments as $i => $v) {
            $res['comments'][$i]['cid']       = $v['ScmCustComment']['id'];
            $res['comments'][$i]['name']      = $v['ScmCustComment']['name'];
            $res['comments'][$i]['message']   = $v['ScmCustComment']['annotation'];
            $res['comments'][$i]['post_date'] = $v['ScmCustComment']['created'];
            $re_page                          = $reply_cid == $v['ScmCustComment']['id'] ? $reply_page : 1;
            $re_page                          = $re_page < 1 ? 1 : $re_page;
            $res['comments'][$i]['replies']   = $this->getThreadReplies($content_id, $v['ScmCustComment']['id'], $re_page, $comment_page);
        }

        return $res;
    }

    public function getThreadReplies($content_id, $comment_id, $re_page, $content_page) {
        $res   = array(
            'count'          => 0,
            'comment_paging' => array(
                'count'        => 0,
                'comment_page' => $re_page,
                'limit'        => $this->threadLimit,
            ),
            'comments'       => array()
        );
        $limit = $this->threadLimit;

        $options                        = array(
            'conditions' => array(
                'AND' => array(
                    'ScmCustComment.text'          => $content_id,
                    'ScmCustComment.active_status' => '1',
                    'ScmCustComment.parent_id'     => $comment_id,
                    'ScmCustComment.menu_code'     => array(
                        $this->threadReplyMenuCode,
                        $this->threadReplyAdminMenuCode,
                    ),
                ),
            ),
        );
        $res['comment_paging']['count'] = $this->find('count', $options);
        if ($res['comment_paging']['count'] <= $this->threadLimit) {
            $re_page = 1;
        }

        $last_page = ceil($res['comment_paging']['count'] / $this->threadLimit);
        if ($re_page > $last_page) {
            $re_page = $last_page;
        }

        $res['comment_paging']['comment_page'] = $re_page;
        $res['comment_paging']['reply_cid']    = $comment_id;
        $res['comment_paging']['content_page'] = $content_page;

        $offset = (($re_page - 1) * $limit);
        if ($offset < 0) {
            $offset = 0;
        }

        $options['order']  = array('ScmCustComment.modified' => 'asc');
        $options['limit']  = $limit;
        $options['offset'] = $offset;

        $res['count'] = $res['comment_paging']['count'];

        $replies = $this->find('all', $options);
        if (!$replies) {
            return $res;
        }

        foreach ($replies as $i => $v) {
            $res['replies'][$i]['cid']       = $v['ScmCustComment']['id'];
            $res['replies'][$i]['name']      = $v['ScmCustComment']['name'];
            $res['replies'][$i]['message']   = $v['ScmCustComment']['annotation'];
            $res['replies'][$i]['post_date'] = $v['ScmCustComment']['created'];
            $res['replies'][$i]['account_official_text'] = $v['ScmCustCommentIp']['name'] ? __d('syscm', '(Official Account)') : '';
        }

        return $res;
    }

    /**
     * 
     */
    public function saveThread($posted_data) {
        if (!$posted_data) {
            return false;
        }
        extract($posted_data);
        $data                                    = array();
        $comment_id                              = CakeString::uuid();
        $data['ScmCustComment']['id']            = $comment_id;
        $data['ScmCustComment']['active_status'] = '1';
        $data['ScmCustComment']['menu_code']     = $this->threadMenuCode;
        $data['ScmCustComment']['name']          = $cust_name_data;
        $data['ScmCustComment']['text']          = $scm_content_id;
        $data['ScmCustComment']['annotation']    = $cust_message_data;

        $data['ScmCustCommentEmail']['scm_customer_id'] = $comment_id;
        $data['ScmCustCommentEmail']['active_status']   = '1';
        $data['ScmCustCommentEmail']['field']           = 'ScmCustCommentEmail';
        $data['ScmCustCommentEmail']['value']           = $cust_email_data;

        $data['ScmCustCommentPhone']['scm_customer_id'] = $comment_id;
        $data['ScmCustCommentPhone']['active_status']   = '1';
        $data['ScmCustCommentPhone']['field']           = 'ScmCustCommentPhone';
        $data['ScmCustCommentPhone']['value']           = $cust_telp_data;

        $data['ScmCustCommentIp']['scm_customer_id'] = $comment_id;
        $data['ScmCustCommentIp']['active_status']   = '1';
        $data['ScmCustCommentIp']['field']           = 'ScmCustCommentIp';
        $data['ScmCustCommentIp']['value']           = $client_ip;

        $options = array('validate' => false);
        return $this->saveAssociated($data, $options);
        debug($posted_data);
        debug($data);
        die();
    }

    public function getThreadReplyLastPage($content_id, $comment_id) {
        $res     = 1;
        if (!$content_id || $comment_id) return $res;
        $options = array(
            'conditions' => array(
                'AND' => array(
                    'ScmCustComment.text'          => $content_id,
                    'ScmCustComment.active_status' => '1',
                    'ScmCustComment.menu_code'     => $this->threadReplyMenuCode,
                    'ScmCustComment.parent_id'     => $comment_id,
                ),
            ),
        );
        $count   = $this->find('count', $options);
        if (!$count) return $res;
        return ceil($count / $this->threadLimit);
    }

    /**
     * invokeTopMenu
     */
    public function invokeTopMenuOnReplyList($request_params) {
        $invoke    = array();
        /* if (in_array($request_params['action'],
          array('album_list', 'album_list_add'))) {
          if (!isset($request_params['pass'][0])) {
          return array();
          }
          }
          if (in_array($request_params['action'],
          array('album_edit', 'album_list', 'album_list_add'))) {

          $invoke = $this->invokeTopMenuOnEdit($request_params);
          } */
        $invoke[1] = array(
            'perm'  => 'edit',
            'alias' => 'reply_list',
            'index' => 1,
            'menu'  => array(
                'title'   => '<span class="fa fa-comments"></span> ' . __d('syscm', 'Comment List'),
                'url'     => array('action' => 'reply_list', $request_params['pass'][0]),
                'options' => array('escape' => false),
                'active'  => false,
            ),
        );
        $invoke[2] = array(
            'perm'  => 'edit',
            'alias' => 'reply_add',
            'index' => 2,
            'menu'  => array(
                'title'   => '<span class="fa fa-reply"></span> ' . __d('syscm', 'Reply'),
                'url'     => array('action' => 'reply_add', $request_params['pass'][0]),
                'options' => array('escape' => false),
                'active'  => false,
            ),
        );
        $invoke[4] = array(
            'perm'  => 'delete',
            'alias' => 'delete',
            'index' => 4,
            'menu'  => array(
                'link_type' => 'postLink',
                'title'     => '<span class=" text-danger"><span class="fa fa-trash text-danger"></span>  ' . __d('syscm', 'Delete Thread') . '</span>',
                'url'       => array(
                    "action" => "delete",
                    $request_params['pass'][0]),
                'options'   => array(
                    'confirm' => __d('sysadmin', 'Are you sure you want to delete this thread ?'),
                    'escape'  => false
                ),
                'active'    => false,
            ),
        );
        if ($request_params['action'] == 'reply_edit') {
            $invoke[3] = array(
                'perm'  => 'edit',
                'alias' => 'reply_edit',
                'index' => 3,
                'menu'  => array(
                    'title'   => '<span class="fa fa-reply"></span> ' . __d('syscm', 'Edit Reply'),
                    'url'     => array('action' => 'reply_edit', $request_params['pass'][0],
                        $request_params['pass'][1]),
                    'options' => array('escape' => false),
                    'active'  => false,
                ),
            );
        }

        ksort($invoke, SORT_NUMERIC);
        return $invoke;
    }

    /**
     * 
     */
    public function saveThreadReply($posted_data) {
        if (!$posted_data) {
            return false;
        }

        extract($posted_data);

        $data                                    = array();
        $comment_id                              = CakeString::uuid();
        $data['ScmCustComment']['id']            = $comment_id;
        $data['ScmCustComment']['active_status'] = '1';
        $data['ScmCustComment']['parent_id']     = $reply_cid;
        $data['ScmCustComment']['menu_code']     = $this->threadReplyMenuCode;
        $data['ScmCustComment']['name']          = $cust_name_data;
        $data['ScmCustComment']['text']          = $scm_content_id;
        $data['ScmCustComment']['annotation']    = $cust_message_data;

        $data['ScmCustCommentEmail']['scm_customer_id'] = $comment_id;
        $data['ScmCustCommentEmail']['active_status']   = '1';
        $data['ScmCustCommentEmail']['field']           = 'ScmCustCommentEmail';
        $data['ScmCustCommentEmail']['value']           = $cust_email_data;

        $data['ScmCustCommentPhone']['scm_customer_id'] = $comment_id;
        $data['ScmCustCommentPhone']['active_status']   = '1';
        $data['ScmCustCommentPhone']['field']           = 'ScmCustCommentPhone';
        $data['ScmCustCommentPhone']['value']           = $cust_telp_data;

        $data['ScmCustCommentIp']['scm_customer_id'] = $comment_id;
        $data['ScmCustCommentIp']['active_status']   = '1';
        $data['ScmCustCommentIp']['field']           = 'ScmCustCommentIp';
        $data['ScmCustCommentIp']['value']           = $client_ip;

        $options = array('validate' => false);
        return $this->saveAssociated($data, $options);
        debug($posted_data);
        debug($data);
        die();
    }

}
