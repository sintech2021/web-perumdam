<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('SyscmAppModel', 'Syscm.Model');

/**
 * CakePHP ScmMenuListGroup
 * @author ino
 */
class ScmCategory extends SyscmAppModel
{
    /**
     *
     * @var type 
     */
    public $useTable = 'scm_categories';
    public $displayField = "name";
    public $menuCodeContentCategory = "ContentCategory";
    
    /**
     * Associations
     * @var type 
     */
    public $hasAndBelongsToMany = array(
        'ScmContent' =>
            array(
                'className' => 'Syscm.ScmContent',
                'joinTable' => 'scm_contents_categories',
                'foreignKey' => 'scm_category_id',
                'associationForeignKey' => 'scm_content_id',
                'unique' => true,
                'conditions' => '',
                'fields' => '',
                'order' => '',
                'limit' => '',
                'offset' => '',
                'finderQuery' => '',
                'with' => null
            )
    );
    /**
     * Validations
     */
    public $validate = array(
        'text' => array(
            'notBlank' => array(
                'rule'    => 'notBlank',
                'message' => 'This value may not be left empty!'
            ),
        ),
        'name' => array(
            'notBlank' => array(
                'rule'    => 'notBlank',
                'message' => 'This value may not be left empty!'
            ),
        )
    );
    /**
     * Delete
     */
    public function dropStatus($id)
    {

        $result = false;
        if ($this->exists($id)) {
           
            $this->id       = $id;
            $data           = array(
                $this->alias => array(
                    'active_status' => '0'
                )
            );
            $result         = $this->save($data, false);
        }

        return $result;
    }
    
    /**
     * 
     */
    public function getPaginateConditions()
    {
      
        return array(
            'AND'=>array(
                $this->alias. '.active_status'=> '1',
                $this->alias. '.menu_code'=> $this->menuCodeContentCategory,
            )
        );
    }

    public function getAnaoucementPaginateConditions()
    {
        $paginated_condition =  $this->getPaginateConditions();
        $paginated_condition['AND']['ScmCategory.name']          = 'pengumuman';

        return $paginated_condition;
    }
    public function getAnaoucementPaginateSettings()
    {
        return array('limit' => 5, 'order' => array(
                'ScmCategory.name'   => 'desc',
                // 'ScmContentPublished.value'  => 'desc',
                // $this->alias . '.menu_order' => 'desc',
        ));
    }
    public function getFormattedAnaoucementList_v2($rs)
    {
      
        $list = array();
        foreach ($rs as $i => $v) {
            $list[$i] ['url']     = $v[$this->alias]['name'];
            $list[$i] ['title']   = $v[$this->alias]['text'];
            // $list[$i] ['content'] = $this->fixSnippet(trim(str_replace('&nbsp;',' ', substr(strip_tags($v['ScmContentContent']['value']),0,150)))) . '...';
            $list[$i] ['date']    = $v['ScmContentPublished']['text'];
            $list[$i] ['time']    = $v['ScmContentPublished']['value'];
            // $list[$i] ['bgurl']    = $this->getFirstImg($v['ScmContentContent']['value']);
            // $list[$i] ['dt']      = $this->format_dt($list[$i] ['date'] ,$list[$i] ['time'] );
            $list[$i] ['author']  = $v['ScmContentPublished']['name'];
        }
        return array('list' => $list);
    }
}
