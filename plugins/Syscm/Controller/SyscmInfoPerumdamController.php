<?php
require_once  APP .'/webroot/xcrud/xcrud.php';
App::uses('SyscmAppController', 'Syscm.Controller');

/**
 * 
 */
class SyscmInfoPerumdamController extends SyscmAppController  
{
	
	public function index($value='')
	{
		$browser_menu_title = __d('syscm', 'Info Perumdam');
		$sub_title = 'Info Perumdam';
        $this->set(compact('browser_menu_title','sub_title'));

		$this->SysAcl->isAcc();
		$xcrud = Xcrud::get_instance();

		$xcrud->table('sys_info_perumdam');
		$xcrud->columns(['path','url']);

		$xcrud->unset_title();
		$xcrud->show_primary_ai_column(true);

		$xcrud->fields(['path','url']);
		// $xcrud->button(BASE_URL.'/syscm/syscm_galery/browse/{tipe}/{id}/{title}');
		
		// $xcrud->pass_var('created', date('Y-m-d H:i:s'), 'create');
		// $xcrud->pass_var('edited', date('Y-m-d H:i:s'), 'create');
		// $xcrud->pass_var('edited', date('Y-m-d H:i:s'), 'edit');
		$xcrud->unset_print();
		$xcrud->unset_csv();
		$xcrud->unset_search();
		$xcrud->label('path','Gambar');
		$xcrud->label('url','Link');
		
		$xcrud->change_type('path','image','',array('path'=>'../uploads/info_perumdam','url'=>BASE_URL.'/uploads/info_perumdam')); // crop it as you want

		$this->set('xcrud',$xcrud->render()) ;


	}
}