<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('SyscmAppController', 'Syscm.Controller');

/**
 * CakePHP SyscmCategories
 * @author ino
 */
class SyscmFeedbackController  extends SyscmAppController 
{
    /**
     * 
     */
    public $uses = array('Syscm.ScmCustFeedback','Syscm.ScmCustFeedbackRespon');
    public $topMenuByModelSetup = array(
        'setTopMenu' => true,
        'model_alias' => "ScmCustFeedback",
        'set' => true,
        'invoke' => array()
    );

    public function add($id)
    {
        //add reply
        $this->SysAcl->isAcc('add');
        $data_row=array();
        $data_row = $this->ScmCustFeedback->findById($id, 'first');
        if(!$data_row){
            return $this->redirect(array('action'=> 'index'));
        }
        $this->set('browser_menu_title', __d('syscm', 'Add Respon :: Customer Feedback'));
        $this->set(compact('data_row'));
        $this->topMenuByModelSetup['model_alias'] = 'ScmCustFeedback';
        $topmenu = $this->ScmCustFeedback->invokeTopMenuOnDetail($this->request->params);
        $topmenu[1]['menu']['url'][] = $id;
        $topmenu[2]['menu']['url'][] = $id;
        $this->topMenuByModelSetup['invoke'] = $topmenu;
        
        if($this->request->is('post')){
            $data = $this->request->data;
            /**
             INSERT INTO `sys_scm_customers_details` (`id`, `name`, `scm_customer_id`, `field`, `value`, `text`, `annotation`, `active_status`, `menu_code`, `menu_order`, `created`, `modified`) VALUES
('56ae7d6e-a020-12e2-b33b-ef4dc0b9c32c',	'Admin',	'56ae7d6e-a020-4f42-b33b-ef4dc0b9c32c',	'ScmCustFeedbackRespon',	'Testing diterima. Trims',	NULL,	NULL,	1,	'127.0.0.1',	NULL,	'2016-01-31 15:35:58',	'2016-01-31 15:35:58');
 
             * 
             `id`	'56ae7d6e-a020-12e2-b33b-ef4dc0b9c32c'
 `name`	"	'Admin'"
 `scm_customer_id`	"	'56ae7d6e-a020-4f42-b33b-ef4dc0b9c32c'"
 `field`	"	'ScmCustFeedbackRespon'"
 `value`	"	'Testing diterima. Trims'"
 `text`	"	NULL"
 `annotation`	"	NULL"
 `active_status`	"	1"
 `menu_code`	"	'127.0.0.1'"
 `menu_order`	"	NULL"
 `created`	"	'2016-01-31 15:35:58'"
 `modified`	"	'2016-01-31 15:35:58'"

             */
            $usr = $this->SysAcl->getUserInfo($this->Session->read('AppUsrLog.uid'));
            
            $data['ScmCustFeedbackRespon']['id'] = CakeString::uuid();
            $data['ScmCustFeedbackRespon']['name'] = $usr['SysAclUser']['name'];
            //scm_customer_id : form provided
            $data['ScmCustFeedbackRespon']['field'] = $this->ScmCustFeedbackRespon->fieldKeyValue;
            //value : form provided
            $data['ScmCustFeedbackRespon']['active_status'] = 1;
            $data['ScmCustFeedbackRespon']['menu_code'] = $this->request->clientIp();
            if(!$this->ScmCustFeedbackRespon->save($data)){
               return $this->Flash->error(__d('syscm', 'Respon save has been failed. Please try again.'));
            }
            $this->Flash->success(__d('syscm', 'Respon has been saved.'));
                return $this->redirect(array('action' => 'edit', $data['ScmCustFeedbackRespon']['scm_customer_id'], $data['ScmCustFeedbackRespon']['id']));
        }
    }
    
    public function detail($id)
    {
        //list
        $this->SysAcl->isAcc();
        $data_row=array();
        $data_row = $this->ScmCustFeedback->findById($id, 'first');
        if(!$data_row){
            return $this->redirect(array('action'=> 'index'));
        }
        $this->set('browser_menu_title', __d('syscm', 'Customer Feedback Detail'));
        $this->Paginator->settings = $this->ScmCustFeedbackRespon->getFeedbackResponPaginateSettings();
        $conditions                = array(
            'AND' => array(
                'ScmCustFeedbackRespon.active_status' => '1',
                'ScmCustFeedbackRespon.field'     => $this->ScmCustFeedbackRespon->fieldKeyValue,
                'ScmCustFeedbackRespon.scm_customer_id'     => $id,
            )
        );
        try {
            $data_list = $this->Paginator->paginate('ScmCustFeedbackRespon', $conditions);
        } catch (NotFoundException $e) {
            $this->request->params['named']['page'] = 1;
            return $this->redirect(Router::reverse($this->request, true));
        }

        $this->set(compact('data_list', 'data_row'));
        
        $this->topMenuByModelSetup['model_alias'] = 'ScmCustFeedback';
        $topmenu = $this->ScmCustFeedback->invokeTopMenuOnDetail($this->request->params);
        $topmenu[1]['menu']['url'][] = $id;
        $topmenu[2]['menu']['url'][] = $id;
        $this->topMenuByModelSetup['invoke'] = $topmenu;
    }
    
    public function edit($id, $respon_id)
    {
        //edit reply
        $this->SysAcl->isAcc('add');
        $data_row=array();
        $data_row = $this->ScmCustFeedback->findById($id, 'first');
        if(!$data_row){
            return $this->redirect(array('action'=> 'index'));
        }
        $this->set('browser_menu_title', __d('syscm', 'Edit Respon :: Customer Feedback'));
        $this->topMenuByModelSetup['model_alias'] = 'ScmCustFeedback';
        $topmenu = $this->ScmCustFeedback->invokeTopMenuOnEdit($this->request->params);
        $topmenu[1]['menu']['url'][] = $id;
        $topmenu[2]['menu']['url'][] = $id;
        $data_row=array();
        $data_row = $this->ScmCustFeedback->findById($id, 'first');
        if(!$data_row){
            return $this->redirect(array('action'=> 'index'));
        }
        $this->set(compact('data_row'));
        
        $this->topMenuByModelSetup['invoke'] = $topmenu;
        $this->ScmCustFeedback->topMenuRevoke = array('add','edit', 'delete');
        
         if($this->request->is('post')){
            $data = $this->request->data;
            /**
             INSERT INTO `sys_scm_customers_details` (`id`, `name`, `scm_customer_id`, `field`, `value`, `text`, `annotation`, `active_status`, `menu_code`, `menu_order`, `created`, `modified`) VALUES
('56ae7d6e-a020-12e2-b33b-ef4dc0b9c32c',	'Admin',	'56ae7d6e-a020-4f42-b33b-ef4dc0b9c32c',	'ScmCustFeedbackRespon',	'Testing diterima. Trims',	NULL,	NULL,	1,	'127.0.0.1',	NULL,	'2016-01-31 15:35:58',	'2016-01-31 15:35:58');
 
             * 
             `id`	'56ae7d6e-a020-12e2-b33b-ef4dc0b9c32c'
 `name`	"	'Admin'"
 `scm_customer_id`	"	'56ae7d6e-a020-4f42-b33b-ef4dc0b9c32c'"
 `field`	"	'ScmCustFeedbackRespon'"
 `value`	"	'Testing diterima. Trims'"
 `text`	"	NULL"
 `annotation`	"	NULL"
 `active_status`	"	1"
 `menu_code`	"	'127.0.0.1'"
 `menu_order`	"	NULL"
 `created`	"	'2016-01-31 15:35:58'"
 `modified`	"	'2016-01-31 15:35:58'"

             */
            $usr = $this->SysAcl->getUserInfo($this->Session->read('AppUsrLog.uid'));
            
            //id : form provided
            $data['ScmCustFeedbackRespon']['text'] = $usr['SysAclUser']['name'];
            
            //value : form provided
            $data['ScmCustFeedbackRespon']['menu_code'] = $this->request->clientIp();
            if(!$this->ScmCustFeedbackRespon->save($data)){
               return $this->Flash->error(__d('syscm', 'Respon update has been failed. Please try again.'));
            }
            $this->Flash->success(__d('syscm', 'Respon has been updated.'));
                return $this->redirect(array('action' => 'edit', $id, $data['ScmCustFeedbackRespon']['id']));
        }
        $conditions                = array(
            'AND' => array(
                'ScmCustFeedbackRespon.active_status' => '1',
                'ScmCustFeedbackRespon.field'     => $this->ScmCustFeedbackRespon->fieldKeyValue,
                'ScmCustFeedbackRespon.id'     => $respon_id,
                'ScmCustFeedbackRespon.scm_customer_id'     => $id,
            )
        );
        
        $this->request->data = $this->ScmCustFeedbackRespon->find('first', compact('conditions'));
    }
    public function index()
    {
        $this->SysAcl->isAcc();
        $this->set('browser_menu_title', __d('syscm', 'Customer Feedback'));
        $this->Paginator->settings = $this->ScmCustFeedback->getFeedbackHistoryPaginateSettings();
        $conditions                = array(
            'AND' => array(
                'ScmCustFeedback.active_status' => '1',
                'ScmCustFeedback.menu_code'     => $this->ScmCustFeedback->menuCode,
            )
        );
        try {
            $data_list = $this->Paginator->paginate('ScmCustFeedback', $conditions);
            $respon_count = array();
            if($data_list){
                foreach($data_list as $key => $val){
                    $respon_count[$val['ScmCustFeedback']['id']] = $this->ScmCustFeedbackRespon->getCountResponsByCustId($val['ScmCustFeedback']['id']);
                }
            }
        } catch (NotFoundException $e) {
            $this->request->params['named']['page'] = 1;
            return $this->redirect(Router::reverse($this->request, true));
        }

        $this->set(compact('data_list', 'respon_count'));
        
    }

}
