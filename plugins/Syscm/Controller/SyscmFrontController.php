<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('SyscmAppController', 'Syscm.Controller');

/**
 * CakePHP SyscmFrontController
 * @author ino
 */
class SyscmFrontController extends SyscmAppController {

    public $layout           = 'Syscm.bootflat';
    public $printLayout      = 'Syscm.bootflat-print';
    public $theme_alias      = 'bootflat';
    public $icon_path        = '/pdamtkr/favicon.png';
    public $logo_path        = '/pdamtkr/logo.png';
    public $static_header_id = 'static_header';
    public $enqueued_css     = array('/pdamtkr/css/main');
    public $useSearchform    = true;
    public $useBelowheader   = false;
    public $belowHeader      = array();
    public $searchq          = "";
    public $uses             = array(
        'Syscm.Galery',
        'Syscm.GaleryItem',
        'Syscm.FrontModel',
        'Syscm.Pendaftaran',
        'Syscm.ScmContent',
        'Syscm.ScmContentContent',
        'Syscm.ScmContentPublished',
        'Syscm.ScmContentMeta',
        'Syscm.FrontContent',
        'Syscm.ScmCustComment',
        'Syscm.ScmCustReg',
        'Syscm.ScmContentMedia',
        'Syscm.ScmCustBill',
        'Syscm.ScmCustFeedback',
        'Syscm.ScmCustFeedbackRespon',
        'Syscm.ScmCustInfo',
        'Syscm.ScmCategory',
        'Syscm.Consultation'
    );
    public function pojok_konsultasi($value='')
    {
        $this->set('segment_key', $this->getSegmentKey());
        // $breadcrumbs            = array();
        // $breadcrumbs[]['title'] = '';
        $title_for_layout       = 'FAQ';
        // $enqueued_script        = array(Configure::read('Sysconfig.recaptcha.api_script'));
        $cond = array('limit' => 100, 'order' => array(
                'ScmConsultation.tgl_buat'   => 'desc',
        ));
        $this->Paginator->settings = $cond;
        $list = $this->Paginator->paginate('Consultation',[]);

        $this->set(compact('title_for_layout' ,'list'));
        if($this->request->is('ajax')){
            // $view = new View($this,false);
            $this->layout=false; 
            return $this->render("Syscm.SyscmFront/pojok_konsultasi_ajax");
        }else{
            return $this->render("Syscm.SyscmFront/pojok_konsultasi");
        }
    }
    public function register_check($kode_pendaftaran="")
    {
        $this->set('segment_key', $this->getSegmentKey());
        $this->set('kode_pendaftaran',$kode_pendaftaran);
        // $breadcrumbs            = array();
        // $breadcrumbs[]['title'] = '';
        $title_for_layout       = 'Status Registrasi Pelanggan';
        // $enqueued_script        = array(Configure::read('Sysconfig.recaptcha.api_script'));
        $this->set(compact('title_for_layout' ));
        if($this->request->is('ajax')){
            // $view = new View($this,false);
            $this->layout=false; 
            return $this->render("Syscm.SyscmFront/register_check_ajax");
        }
    }
    public function tunggakan()
    {
        $this->set('segment_key', $this->getSegmentKey());
        // $breadcrumbs            = array();
        // $breadcrumbs[]['title'] = '';
        $title_for_layout       = 'Tuggakan Pelanggan';
        // $enqueued_script        = array(Configure::read('Sysconfig.recaptcha.api_script'));
        $this->set(compact('title_for_layout' ));
        if($this->request->is('ajax')){
            // $view = new View($this,false);
            $this->layout=false; 
            return $this->render("Syscm.SyscmFront/tunggakan_ajax");
        }
    }
    public function galeri($cmd="",$id="",$slug="")
    {
       $this->set('segment_key', $this->getSegmentKey());
        // $breadcrumbs            = array();
        // $breadcrumbs[]['title'] = '';
        $title_for_layout       = 'Galeri';
        // $enqueued_script        = array(Configure::read('Sysconfig.recaptcha.api_script'));
        $this->set(compact('title_for_layout' ));
        if($cmd=='items'){
            $galery_id = $id;
            $galery = $this->Galery->findById($id);
            $this->set(compact('galery_id','galery'));

            if($this->request->is('ajax')){
                // $view = new View($this,false);
                $this->layout=false; 
                return $this->render("Syscm.SyscmFront/galeri_items_ajax");
            }else{
                 return $this->render("Syscm.SyscmFront/galeri_items");
            }
        }
        if($cmd=='detail'){
            $item_id = $id;
            $detail = $this->GaleryItem->findById($item_id);
            $this->set(compact('item_id','detail'));

            if($this->request->is('ajax')){
                // $view = new View($this,false);
                $this->layout=false; 
                return $this->render("Syscm.SyscmFront/galeri_detail_ajax");
            }else{
                 return $this->render("Syscm.SyscmFront/galeri_detail");
            }
        }
        if($this->request->is('ajax')){
            // $view = new View($this,false);
            $this->layout=false; 
            return $this->render("Syscm.SyscmFront/galeri_ajax");
        }
    }
    public function arsip($cat='berita',$y='',$mm='',$slug='')
    {
        $m = preg_replace('/^0/','',$mm);
        // die($m);
        $this->set('segment_key', $this->getSegmentKey());
        // $breadcrumbs            = array();
        // $breadcrumbs[]['title'] = '';
        $title_for_layout       = 'Arsip Berita '. ucfirst($slug) . ' ' . $y;
        // $enqueued_script        = array(Configure::read('Sysconfig.recaptcha.api_script'));
        // GET KATEGORY PENGUMUMAN
        // $options['conditions']['AND']['ScmCategory.active_status'] = '1';
        $options['conditions']['AND']['ParentContent.name']          = '/topik/berita';
        // $this->ScmCategory->recursive = -1;
        // $scm_category = $this->ScmCategory->find('first', $options);

        // print_r($scm_category['ScmCategory']['id']);

        ////////////////////////////////////////////////
        $cond = array('limit' => 10, 'order' => array(
                'ScmContentPublished.text'   => 'desc',
                'ScmContentPublished.value'  => 'desc',
                'FrontContent.menu_order' => 'desc',
        ));
        $this->Paginator->settings = $this->FrontContent->getMainlistPaginateSettings_v2($cond);
        try {
            // $this->FrontContent->appendScmContentsCategory();
            // $this->FrontContent->includeContent(true);
            
            $paginated_condition = [
                'AND'=>[
                    'FrontContent.active_status' => '1',
                    'FrontContent.menu_code'=> 'content_web',
                    'YEAR(ScmContentPublished.text)'   => $y,
                    'MONTH(ScmContentPublished.text)'   => $m,

                ]
            ];
            $top_mainlist_rs = $this->Paginator->paginate('FrontContent', $this->FrontContent->getMainlistPaginateConditions_v2($paginated_condition));
        } catch (NotFoundException $e) {
            $this->request->params['named']['page'] = 1;
            return $this->redirect(Router::reverse($this->request, true));
        }


        #debug($top_mainlist_rs);die();
        $mainlist = $this->FrontContent->getFormattedMainlist_v2($top_mainlist_rs );
        // $mainlist = 
        ///////////////////////////////////////////////

        $this->set(compact('title_for_layout' ,'mainlist'));
        if($this->request->is('ajax')){
            // $view = new View($this,false);
            $this->layout=false; 
            return $this->render("Syscm.SyscmFront/arsip_ajax");
        }
    }
    public function read($id='',$cat_name='',$slug='')
    {
        $category_name = ucfirst($cat_name);

        $segment_key = $this->getSegmentKey();
        $segment_keys = explode('.', $segment_key);
        // print_r($segment_keys);
        // die();
        if(count($segment_keys)==4)
            $this->set('segment_key', $segment_keys[2]);
        else
            $this->set('segment_key', $segment_key);

        $this->helpers[] = 'Syscm.SyscmArticle';
        if (!$slug) {
            return $this->redirect(array('action' => 'index'));
        }

        $formatted_slug = $this->FrontModel->formatSlug($this->request->pass);
        $options        = array(
            'conditions' => array(
                'AND' => array(
                    'ScmContent.id'          => $id,
                    'ScmContent.active_status' => '1',
                )
            )
        );


        $notPreview     = false;
        if (!$this->request->named) {
            $notPreview = true;
        }
        if (!isset($this->request->named['cid'])) {
            $notPreview = true;
        }
        if (!$notPreview) {
            if (!isset($this->request->named[md5($this->request->named['cid'] . 'id')])) {
                $notPreview = true;
            }
        }
        if (!$notPreview) {
            if ($this->request->named[md5($this->request->named['cid'] . 'id')] != md5($this->request->named['cid'] . date('d'))) {
                $notPreview = true;
            }
        }
        if (!$notPreview) {

            $options['conditions']['AND'] ['ScmContent.id'] = $this->request->named['cid'];
        } else {
            $options['conditions']['AND'] ['ScmContentPublished.menu_code'] = $this->ScmContentPublished->fieldMenuCodePublished;
        }
        $this->ScmContent->recursive = 1;
        $syscm_contents              = $this->ScmContent->find('first', $options);
        // print_r($syscm_contents);
        // die();
        $title_for_layout = __d('syscm', 'Web Page');


        $cats        = array();
        $breadcrumbs = array();
        if ($syscm_contents) {
            $title_for_layout = __d('syscm', $syscm_contents['ScmContent']['text']);
            $cats             = $syscm_contents['ScmCategory'];
            $article = $this->FrontContent->getFormattedArticle($syscm_contents);
            
           
            $article_breadcrumbs = [
                ['title'=>'Beranda', 'url'=>BASE_URL],
                ['title'=> $category_name, 'url' => BASE_URL.'/topik/berita'],
                ['title'=> strip_title(6,$article['title']),'url'=>false]
            ];
            $this->set('article_breadcrumbs',$article_breadcrumbs);

            $i                = 0;
            if (isset($syscm_contents['ParentContent'])) {
                if ($syscm_contents['ParentContent']['name']) {
                    $par = $this->ScmContent->findById($syscm_contents['ParentContent']['id']);
                    if (isset($par['ParentContent']['id'])) {
                        $breadcrumbs [$i]['title'] = $par['ParentContent']['text'];
                        $breadcrumbs [$i]['url']   = $par['ParentContent']['name'];
                        $i++;
                    }
                    $breadcrumbs [$i]['title'] = $syscm_contents['ParentContent']['text'];
                    $breadcrumbs [$i]['url']   = $syscm_contents['ParentContent']['name'];
                    $i++;
                }
            }

            $breadcrumbs [$i]['title']          = $syscm_contents['ScmContent']['text'];
            //fb_share
            $fb_share                           = array();
            $fb_share['meta']['og:url']         = Router::url($syscm_contents['ScmContent']['name'], true);
            $fb_share['meta']['og:type']        = "website";
            $fb_share['meta']['og:title']       = $syscm_contents['ScmContent']['text'];
            $fb_share['meta']['og:description'] = $this->FrontContent->getExcerpt($syscm_contents['ScmContentContent']['value'], 50);
            $fb_share['meta']['og:image']       = Router::url($this->FrontContent->getFirstImg($syscm_contents['ScmContentContent']['value']), true);
            // $this->set(compact('fb_share'));
        }

        $syscm_template = "single";
        $cat_names      = array();
        $sidelist       = array();
        if ($cats) {
            foreach ($cats as $v) {
                $cat_names[] = $v['name'];
            }
        }
        $article['comment_enabled'] = false;
        // die();
        if ($cat_names) {
            //comment
            if (in_array('comment_enable', $cat_names)) {
                // $enqueued_script = array(Configure::read('Sysconfig.recaptcha.api_script'));
                $this->_custCommentByContent($syscm_contents['ScmContent']['id']);
                $content_id = $syscm_contents['ScmContent']['id'];
                $this->set(compact('content_id'));
                $article['comment_enabled'] = true;

            }
            //./comment


            if (in_array('parent', $cat_names)) {
                $featured_heading          = array(
                    'title'   => $syscm_contents['ScmContent']['text'],
                    'summary' => $syscm_contents['ScmContentContent']['value'],
                );
                $syscm_template            = 'parent';
                /**
                 * cat Main list
                 */
                $this->Paginator->settings = $this->FrontContent->getCatlistPaginateSettings();
                $this->FrontContent->includeContent(true);
                try {
                    $catlist_rs = $this->Paginator->paginate('FrontContent', $this->FrontContent->getCatlistPaginateConditions($syscm_contents['ScmContent']['name']));
                } catch (NotFoundException $e) {
                    $this->request->params['named']['page'] = 1;
                    return $this->redirect(Router::reverse($this->request, true));
                }
                #debug($top_mainlist_rs);die();
                $this->set('mainlist', $this->FrontContent->getFormattedMainlist($catlist_rs));
            } else {
                //single post
                //$fb_share['meta']['og:description'] = $syscm_contents['ScmContent']['text'];
                /**
                 * <meta property="og:type"          content="website" />
                  <meta property="og:title"         content="Your Website Title" />
                  <meta property="og:description"   content="Your description" />
                  <meta property="og:image"         content="http://www.your-domain.com/path/image.jpg" />;
                 */
                $this->set(compact('fb_share'));
                $this->Paginator->settings = $this->FrontContent->getCatlistPaginateSettings();
                try {
                    // $catlist_rs = $this->Paginator->paginate('FrontContent', $this->FrontContent->getCatlistPaginateConditions('/topik/berita'));
                } catch (NotFoundException $e) {
                    $this->request->params['named']['page'] = 1;
                    return $this->redirect(Router::reverse($this->request, true));
                }
                #debug($top_mainlist_rs);die();
                // $this->set('mainlist', $this->FrontContent->getFormattedMainlist($catlist_rs));

            }
            if ($syscm_contents) {
                if (isset($syscm_contents['ParentContent'])) {
                    $par_list = [];//$this->FrontModel->getListByParentName($syscm_contents['ParentContent']['name'], array(
                        // $syscm_contents['ScmContent']['id']));

                    if ($par_list['url']) {
                        $sidelist[] = $par_list;
                        while ($par_list['parent_name']) {
                            $par_list_x = $par_list;
                            $par_list   = [];//$this->FrontModel->getListByParentName($par_list_x['parent_name'], array(
                                // $par_list_x['id']));
                            if ($par_list['url']) {
                                $sidelist[] = [];//$par_list;
                            }
                        }
                    }
                }
            }
        }


        $this->set('article', $article);

        // parent to search


        $sidelist[] =[];// $this->FrontModel->getListByParentName('/topik/berita');

        $this->set(compact('syscm_contents', 'title_for_layout', 'featured_heading', 'breadcrumbs', 'sidelist', 'syscm_template'));
        $tpl = "Syscm.SyscmFront/read";
        if($segment_keys[2]=='tentang_kami'){
            $tpl = "Syscm.SyscmFront/Templates/tentang_kami";
            if($segment_keys[3]=='jajaran-direksi'){
                $tpl = "Syscm.SyscmFront/Templates/jajaran_direksi";
            }
        }
        if($segment_keys[2]=='layanan_informasi'){
            $tpl = "Syscm.SyscmFront/Templates/layanan_informasi";
            if($segment_keys[3]=='kantor-pelayanan'){
                $tpl = "Syscm.SyscmFront/Templates/kantor_layanan";
            }
        }
        // print_r($tpl);
        if($this->request->is('ajax')){
            if($segment_keys[2]=='tentang_kami'){

                $tpl = "Syscm.SyscmFront/Templates/ajax/tentang_kami";
                if($segment_keys[3]=='jajaran-direksi'){
                    $tpl = "Syscm.SyscmFront/Templates/ajax/jajaran_direksi";
                }
            }
            else if($segment_keys[2]=='layanan_informasi'){

                $tpl = "Syscm.SyscmFront/Templates/ajax/layanan_informasi";
                if($segment_keys[3]=='kantor-pelayanan'){
                    $tpl = "Syscm.SyscmFront/Templates/ajax/kantor_layanan";
                }
            }else{
                $tpl .= '_ajax';
            }
            $this->layout=false; 
            return $this->render($tpl);
        }else{
            return $this->render($tpl);
        }
    
    }
    public function pengumuman()
    {
        $this->set('segment_key', $this->getSegmentKey());
        // $breadcrumbs            = array();
        // $breadcrumbs[]['title'] = '';
        $title_for_layout       = 'Pengumuman';
        // $enqueued_script        = array(Configure::read('Sysconfig.recaptcha.api_script'));
        // GET KATEGORY PENGUMUMAN
        $options['conditions']['AND']['ScmCategory.active_status'] = '1';
        $options['conditions']['AND']['ScmCategory.name']          = 'pengumuman';
        $this->ScmCategory->recursive = -1;
        $scm_category = $this->ScmCategory->find('first', $options);

        // print_r($scm_category['ScmCategory']['id']);

        ////////////////////////////////////////////////
        $cond = array('limit' => 10, 'order' => array(
                'ScmContentPublished.text'   => 'desc',
                'ScmContentPublished.value'  => 'desc',
                'FrontContent.menu_order' => 'desc',
        ));
        $this->Paginator->settings = $this->FrontContent->getMainlistPaginateSettings_v2($cond);
        try {
            $this->FrontContent->appendScmContentsCategory();
            // $this->FrontContent->includeContent(true);
            
            $paginated_condition = [
                'AND'=>[
                    'FrontContent.active_status' => '1',
                    'FrontContent.menu_code'=> 'content_web',
                    'ScmContentsCategory.scm_category_id' => $scm_category['ScmCategory']['id']
                ]
            ];
            $top_mainlist_rs = $this->Paginator->paginate('FrontContent', $this->FrontContent->getMainlistPaginateConditions_v2($paginated_condition));
        } catch (NotFoundException $e) {
            $this->request->params['named']['page'] = 1;
            return $this->redirect(Router::reverse($this->request, true));
        }


        #debug($top_mainlist_rs);die();
        $mainlist = $this->FrontContent->getFormattedMainlist_v2($top_mainlist_rs );
        // $mainlist = 
        ///////////////////////////////////////////////

        $this->set(compact('title_for_layout' ,'mainlist'));
        if($this->request->is('ajax')){
            // $view = new View($this,false);
            $this->layout=false; 
            return $this->render("Syscm.SyscmFront/pengumuman_ajax");
        }
    }

    public function _get_berita()
    {
        // GET KATEGORY PENGUMUMAN
                $options['conditions']['AND']['ScmCategory.active_status'] = '1';
                $options['conditions']['AND']['ScmCategory.name']          = 'berita';
                $this->ScmCategory->recursive = -1;
                $scm_category = $this->ScmCategory->find('first', $options);

                // print_r($scm_category['ScmCategory']['id']);

                ////////////////////////////////////////////////
                // $this->request->params['named']['page'] = 
                $cond = array('limit' => 12, 'order' => array(
                        'ScmContentPublished.text'   => 'desc',
                        'ScmContentPublished.value'  => 'desc',
                        'FrontContent.menu_order' => 'desc',
                ));
                $this->Paginator->settings = $this->FrontContent->getMainlistPaginateSettings_v2($cond);
                try {
                    $this->FrontContent->appendScmContentsCategory();
                    // $this->FrontContent->includeContent(true);
                    
                    $paginated_condition = [
                        'AND'=>[
                            'FrontContent.active_status' => '1',
                            'FrontContent.menu_code'=> 'content_web',
                            'ScmContentsCategory.scm_category_id' => $scm_category['ScmCategory']['id']
                        ]
                    ];
                    $top_mainlist_rs = $this->Paginator->paginate('FrontContent', $this->FrontContent->getMainlistPaginateConditions_v2($paginated_condition));
                } catch (NotFoundException $e) {
                    $this->request->params['named']['page'] = 1;
                    return $this->redirect(Router::reverse($this->request, true));
                }


                #debug($top_mainlist_rs);die();
                $list =  $this->FrontContent->getFormattedMainlist_v2($top_mainlist_rs ); 
                $this->set('list',$list);
                $this->layout = false;
                $this->response->type('json');   
                return $this->render('Syscm.SyscmFront/Templates/ajax/berita'); 
    }
    public function detail_reg($id='',$slug='')
    {
        $data = $this->Pendaftaran->findById($id);
        $this->set('segment_key', $this->getSegmentKey());
        // $breadcrumbs            = array();
        // $breadcrumbs[]['title'] = '';
        if(empty($data)){
            die('record doesnt exists');
        }
        $title_for_layout       = 'Detail Pendaftaran';
        $this->layout = "print";
        $this->set(compact('data','title_for_layout'));
         if ($this->request->is('ajax')) {
            return $this->render('Syscm.SyscmFront/Templates/ajax/detail_reg'); 
         }
        return $this->render('Syscm.SyscmFront/Templates/detail_reg'); 

    }
    public function data($what='',$parent='',$c='')
    {
        switch ($what) {
            case 'web-link':
                $data = $this->FrontModel->getMenuWebLink();
                
                break;
            case 'office-service':
                $data = $this->FrontModel->getListByParentName('/web/layanan_informasi/kantor_layanan');
                # code...
                break;
            case 'caraousel':
                $data =  $this->FrontContent->getFeaturedContents();
                # code...
                break;
            case 'related_content':
                $exclude_id = $c;
                if($parent=='tentang_kami'){
                    $data = $this->FrontModel->getListByParentName('/web/tentang_kami',[$exclude_id]);
                }
                else if($parent=='layanan_informasi'){
                    $data = $this->FrontModel->getListByParentName('/web/layanan_informasi',[$exclude_id]);
                }
                
                else if($parent=='laboratorium'){
                    $data = $this->FrontModel->getListByParentName('/web/laboratorium',[$exclude_id]);
                }else{
                    return $this->_get_berita();
                }
            break;
            case 'berita':
                return $this->_get_berita(); 
                // print_r($data);    
                # code...
                break; 
            case 'menu':
                $data = $this->_setMainNav();
                # code...
                break; 
            case 'comment':
                $content_id      = $this->params['url']['content_id'];
              
                $named           = $this->request->param('named');
                $comment_page    = isset($named['comment_page']) ? (int) $named['comment_page'] : 1;
                $comment_page    = $comment_page < 1 ? 1 : $comment_page;
                $reply_cid    = isset($named['reply_cid']) ? $named['reply_cid'] : null;
                $reply_page    = isset($named['reply_page']) ? $named['reply_page'] : null;
        
                $syscm_comment = array('ls' => array());
                // ts
                
                //reply
                
        
                #$options = $this->ScmCustComment->getByContentOptions();
                $syscm_comment['ls']  = $this->ScmCustComment->getThreadByContent($content_id, $comment_page, $reply_cid, $reply_page);
                $data  = $syscm_comment;   
                $view = new View($this,false);
                $view->layout=false; 
                $view->set(compact('syscm_comment'));

                $data = $view->render('Syscm.SyscmFront/Templates/comments');
            break;   
            case 'send_comment':
                  $content_id = $this->request->data('content_id');
                  if ($this->request->is('post')) {
                    $this->_custCommentPut($content_id);
                    exit();
                  }
                  break; 
            case 'reply_comment':
                  $content_id = $this->request->data('content_id');
                 if ($this->request->is('post')) {
                    $this->_custCommentReply($content_id);
                    exit();
                 }

                 break; 
            default:
                $connection = ConnectionManager::getDataSource('sys_');
                //$results = $connection->execute('SELECT * FROM articles')->fetchAll('assoc');
                
                $data = $connection->execute("SELECT * FROM sys_scm_menus")->fetchAll('assoc');;
                break;
        }
        header('Content-Type: application/json');
        // echo $what . "\n";
        echo json_encode($data,JSON_PRETTY_PRINT);
        die();
    }
    public function getSegmentKey()
    {
        parent::__construct();
        $url_path = preg_replace('/(\/+)/','/',$_SERVER['REQUEST_URI']) ;
        $segment_keys = explode("/", parse_url($url_path, PHP_URL_PATH));
        // print_r($url_path);
        // die();
        $segment_key  = '';
        $max_segment  = count($segment_keys)-1;
        foreach ($segment_keys as $i => $seg) {
            if(!empty($seg)){
                $segment_key .= $seg . ($i == $max_segment ? '':'.');
            }
        }

       return rtrim($segment_key,'.');
    }
    /**
     * 
     * @param type $content_id
     */
    protected function _custCommentByContent($content_id) {
        $enqueued_script = array(Configure::read('Sysconfig.recaptcha.api_script'));
        $named           = $this->request->param('named');
        $comment_page    = isset($named['comment_page']) ? (int) $named['comment_page'] : 1;
        $comment_page    = $comment_page < 1 ? 1 : $comment_page;
        $reply_cid    = isset($named['reply_cid']) ? $named['reply_cid'] : null;
        $reply_page    = isset($named['reply_page']) ? $named['reply_page'] : null;

        $syscm_comment = array('ls' => array());
        // ts
        if ($this->request->is('put')) {
            $this->_custCommentPut($content_id);
        }
        //reply
        if ($this->request->is('post')) {
            $this->_custCommentReply($content_id);
        }

        #$options = $this->ScmCustComment->getByContentOptions();
        // $syscm_comment['ls'] = $this->ScmCustComment->getThreadByContent($content_id, $comment_page, $reply_cid, $reply_page);
        
        $this->set(compact('syscm_comment', 'enqueued_script'));
    }

    /**
     * 
     * @return boolean
     */
    protected function _custCommentPut($content_id) {
        $request = $this->request;
        $data    = $request->data;

        $error = array();
        if (Configure::read('useRecaptcha')) {
            $recaptchaResponse = $request->data('g-recaptcha-response');
            $recaptchaCallenge = $this->_getRecaptcha($recaptchaResponse);
            if (!$recaptchaCallenge->success) {

                $error['recaptcha'] = array(
                    'message' => __d('syscm', 'Please check you are not a robot!'),
                );
            }
        }

        $syscm_comment_data                   = array('scm_content_id' => $content_id,
            'client_ip'      => $request->clientIp());
        extract($data);
        $cust_name                            = trim(filter_var($cust_name, FILTER_SANITIZE_STRING));
        $syscm_comment_data['cust_name_data'] = $cust_name;
        if (!$cust_name) {
            $error['cust_name'] = array(
                'message' => __d('syscm', 'Required!'),
            );
        }
        $cust_email                            = trim(filter_var($cust_email, FILTER_SANITIZE_EMAIL));
        $syscm_comment_data['cust_email_data'] = $cust_email;
        if (!$cust_email) {
            $error['cust_email'] = array(
                'message' => __d('syscm', 'Required a valid email!'),
            );
        }
        $cust_telp                            = trim(filter_var($cust_telp, FILTER_SANITIZE_NUMBER_INT));
        $syscm_comment_data['cust_telp_data'] = $cust_telp;
        if (!$cust_telp) {
            $error['cust_telp'] = array(
                'message' => __d('syscm', 'Required a valid phone number!'),
            );
        }
        $cust_message                            = trim(filter_var($cust_message, FILTER_SANITIZE_FULL_SPECIAL_CHARS));
        $syscm_comment_data['cust_message_data'] = $cust_message;
        if (!$cust_message) {
            $error['cust_message'] = array(
                'data'    => $cust_message,
                'message' => __d('syscm', 'Required!'),
            );
        }
        $this->set(compact('syscm_comment_data'));
        if ($error) {
            $this->Flash->frontError(__d('syscm', 'Failed to add comment'));
            $this->set('syscm_comment_error', $error);
                echo json_encode(['success'=>false,'error'=>$error]);

            return;
        }

        if (!$this->ScmCustComment->saveThread($syscm_comment_data)) {
            $this->Flash->frontError(__d('syscm', 'Failed to add your comment right now. Please try again later.'));
            if ($this->request->is('ajax')) {
                $m = __d('syscm', 'Failed to add your comment right now. Please try again later.');
                echo json_encode(['success'=>false,'message'=>$m]);
             }
            return;
        } else {

            $this->Flash->frontSuccess(__d('syscm', 'Your comment has been saved. Thank you.'));
            $this->request->params['named']['comment_page'] = 1;
            if ($this->request->is('ajax')) {
                $m = 'Sukses';
                echo json_encode(['success'=>true,'message'=>$m]);
             }
            return $this->redirect(Router::reverse($this->request, true));
        }
    }
    
    protected function _custCommentReply($content_id) {
        $request = $this->request;
        $data    = $request->data;
        //return $this->redirect(Router::reverse($this->request, true));
        
        $error = array();
        if (Configure::read('useRecaptcha')) {
            $recaptchaResponse = $request->data('g-recaptcha-response');
            $recaptchaCallenge = $this->_getRecaptcha($recaptchaResponse);
            if (!$recaptchaCallenge->success) {

                $error['recaptcha'] = array(
                    'message' => __d('syscm', 'Please check you are not a robot!'),
                );
            }
        }

        extract($data);
        $syscm_comment_data                   = array('scm_content_id' => $content_id,
            'client_ip'      => $request->clientIp(), 'reply_cid' =>$reply_cid);
        
        $reply_name                            = trim(filter_var($reply_name, FILTER_SANITIZE_STRING));
        $syscm_comment_data['cust_name_data'] = $reply_name;
        if (!$reply_name) {
            $error['reply_name'] = array(
                'message' => __d('syscm', 'Required!'),
            );
        }
        $reply_email                            = trim(filter_var($reply_email, FILTER_SANITIZE_EMAIL));
        $syscm_comment_data['cust_email_data'] = $reply_email;
        if (!$reply_email) {
            $error['reply_email'] = array(
                'message' => __d('syscm', 'Required a valid email!'),
            );
        }
        $reply_telp                            = trim(filter_var($reply_telp, FILTER_SANITIZE_NUMBER_INT));
        $syscm_comment_data['cust_telp_data'] = $reply_telp;
        if (!$reply_telp) {
            $error['reply_telp'] = array(
                'message' => __d('syscm', 'Required a valid phone number!'),
            );
        }
        $reply_message                            = trim(filter_var($reply_message, FILTER_SANITIZE_FULL_SPECIAL_CHARS));
        $syscm_comment_data['cust_message_data'] = $reply_message;
        if (!$reply_message) {
            $error['reply_message'] = array(
                'data'    => $reply_message,
                'message' => __d('syscm', 'Required!'),
            );
        }
        $syscm_comment_reply_data = array($reply_cid => $syscm_comment_data);
        $this->set(compact('syscm_comment_reply_data'));
        if ($error) {
            if ($this->request->is('ajax')) {
                $this->response->disableCache();
                echo json_encode(['success'=>false,'error'=>$error]);
            }
            $this->Flash->frontError(__d('syscm', 'Failed to add reply'));
            $this->set('syscm_comment_error', $error);
            return;
        }
        $lasttime = 0;
        if($this->Session->check(md5($this->request->clientIp().'lastcommentreplied'.$reply_cid))){
            $lasttime = $this->Session->read(md5($this->request->clientIp().'lastcommentreplied'.$reply_cid));
            
        }
        $lastdiff = time()-$lasttime;
        if( $lastdiff <= 3600){
            $this->Flash->frontError(__d('syscm', 'You have replied this comment just a few minutes ago. Please try again later in one hour.'));
             if ($this->request->is('ajax')) {
                $m = __d('syscm', 'You have replied this comment just a few minutes ago. Please try again later in one hour.');
                echo json_encode(['success'=>false,'message'=>$m]);
             }
               
            return;
        }

        if (!$this->ScmCustComment->saveThreadReply($syscm_comment_data)) {
            $this->Flash->frontError(__d('syscm', 'Failed to add your reply right now. Please try again later.'));
            return;
        } else {
            $this->Session->write(md5($this->request->clientIp().'lastcommentreplied'.$reply_cid), time());

            $this->Flash->frontSuccess(__d('syscm', 'Your reply has been saved. Thank you.'));
            $this->request->params['named']['reply_cid'] = $reply_cid;
            $this->request->params['named']['reply_page'] = $this->ScmCustComment->getThreadReplyLastPage($content_id,$reply_cid);
            $this->request->params['named']['comment_page'] = $comment_page;
            if ($this->request->is('ajax')) {
                $m = 'Sukses';
                echo json_encode(['success'=>true,'message'=>$m]);
             }
            return $this->redirect(Router::reverse($this->request, true));
        }
    }

    /**
     * 
     * @return boolean
     */
    protected function _setbelowheader() {
        if (!$this->useBelowheader) {
            return false;
        }
        $this->set('below_header', $this->belowHeader);
    }

    /**
     * 
     */
    protected function _custBillCheck() {
        $request  = $this->request;
        $req_data = $request->data;
        $error    = array();
        if (Configure::read('useRecaptcha')) {
            $recaptchaResponse = $request->data('g-recaptcha-response');
            $recaptchaCallenge = $this->_getRecaptcha($recaptchaResponse);
            if (!$recaptchaCallenge->success) {
                $error['recaptcha_error'] = array(
                    'message' => __d('syscm', 'Please check you are not a robot!'),
                );
            }
        }
        extract($req_data);
        //empty
        if (!$cust_bill_nosl)
                return $this->Flash->frontError(__d('syscm', 'Billing checking failed. Please try again later.'));

        $bill_period = sprintf('%d-%d-01', $cust_bill_period['year'], $cust_bill_period['month']);
        $bill_info   = $this->ScmCustBill->findBillByNosl($cust_bill_nosl, $cust_bill_period['year'], $cust_bill_period['month']);
        if (!$bill_info)
                $this->Flash->frontError(__d('syscm', 'Billing unavailable. Please specify valid input.'));
        /*
         * 'name' => '2',
          'parent_id' => '569bb480-712c-411a-82ce-1064ded5206b',
          'lft' => null,
          'rght' => null,
          'text' => 'H022035',
          'annotation' => '2015',
         */

        //passing output
        $this->set(compact('bill_period', 'cust_bill_nosl', 'bill_info'));
    }
    
    public function _custFeedbackCheckNosl(){
        if ($this->request->is('post')) {
            $this->_custFeedbackCheckNoslPost();
        }
        $nosl = $this->Session->read('AppFront.nosl');
        if(!$nosl) return;
        $cust_feedback_profile = $this->ScmCustInfo->findByNoSl($nosl);
        if($cust_feedback_profile){
            return $this->set(compact('cust_feedback_profile'));
        }
    }

    public function _custFeedbackCheckNoslPost(){
        $request  = $this->request;
        $req_data = $request->data;
        extract($req_data);
        $cust_feedback_clear_nosl = isset($cust_feedback_clear_nosl) ? filter_var($cust_feedback_clear_nosl,FILTER_SANITIZE_NUMBER_INT) : false;
        
        if($cust_feedback_clear_nosl){
            return $this->Session->delete('AppFront.nosl');
            
        }
        
        $cust_feedback_check_nosl = isset($cust_feedback_clear_nosl) ? filter_var($cust_feedback_check_nosl,FILTER_SANITIZE_FULL_SPECIAL_CHARS) : '';
        $count = $this->ScmCustInfo->countByNoSl($cust_feedback_check_nosl);
        if($count <= 0){
            $cust_feedback_check_nosl_error = __d('syscm', 'Invalid Customer ID');
            $cust_feedback_check_nosl_posted = compact('cust_feedback_check_nosl_error', 'cust_feedback_check_nosl');
            return $this->set(compact('cust_feedback_check_nosl_posted'));
            
        }
        $this->Session->write('AppFront.nosl', $cust_feedback_check_nosl);
        return $this->redirect(Router::reverse($this->request, true));
            
    }
    
    public function _custFeedbackHistory(){
        $nosl = $this->Session->read('AppFront.nosl');
        if(!$nosl) return;
        $this->Paginator->settings = $this->ScmCustFeedback->getFeedbackHistoryPaginateSettings();
        
        
        $conditions = array(
                $this->ScmCustFeedback->alias . '.text'          => $nosl,
                $this->ScmCustFeedback->alias . '.active_status' => '1',
                $this->ScmCustFeedback->alias . '.menu_code'     => $this->ScmCustFeedback->menuCode,
            );
        
        try {
            $cust_feedback_history = $this->Paginator->paginate('ScmCustFeedback', $conditions);
        } catch (NotFoundException $e) {
            $this->request->params['named']['page'] = 1;
            return $this->redirect(Router::reverse($this->request, true));
        }
        if(!$cust_feedback_history) return;        
        $this->set('cust_feedback_history', $this->ScmCustFeedback->formatCustHistory($cust_feedback_history));
        $this->set('cust_feedback_respon', $this->ScmCustFeedbackRespon->findByFeedbackRsFormatted($cust_feedback_history));
        
        
        
        
    }

    /**
     * 
     */
    protected function _custFeedbackPut() {
        $request  = $this->request;
        $req_data = $request->data;
        $error    = array();
        if (Configure::read('useRecaptcha') && $_SERVER['HTTP_HOST'] != 'localhost') {
            $recaptchaResponse = $request->data('g-recaptcha-response');
            $recaptchaCallenge = $this->_getRecaptcha($recaptchaResponse);
            if (!$recaptchaCallenge->success) {
                $error['recaptcha_error'] =  __d('syscm', 'Please check you are not a robot!');
                if($request->is('ajax')){
                    echo json_encode(['success'=>false,'msg'=>$error['recaptcha_error']]);
                }
                return;
            }


        }

        extract($req_data);
        /**
         * array(
          'cust_feedback_name' => 'tes',
          'cust_feedback_nosl' => 'tes',
          'cust_feedback_email' => 'tes@tes',
          'cust_feedback_phone' => 'sfdf',
          'cust_feedback_cell' => 'sdf',
          'cust_feedback_msg' => 'sdf',
          'g-recaptcha-response' => ''
          )
         */
        $keys = [
          'cust_feedback_name_error' => 'Nama Lengkap',
          'cust_feedback_nosl_error' => 'ID Pelanggan',
          'cust_feedback_email_error' => 'Email',
          'cust_feedback_phone_error' => 'Nomor Telepon',
          'cust_feedback_cell_error' => 'Nomor HP',
          'cust_feedback_msg_error' => 'Pesan Pengaduan',
        ];
        $cust_feedback_data = array();
        $cust_feedback_data['client_ip'] = $this->request->clientIp();

        $req_alias                      = 'cust_feedback_name';
        ${$req_alias}                   = trim(filter_var(${$req_alias}, FILTER_SANITIZE_STRING));
        $cust_feedback_data[$req_alias] = ${$req_alias};
        if (!${$req_alias}) {
            $error[$req_alias . '_error'] = __d('syscm', 'Required!');
        }

        $req_alias                      = 'cust_feedback_nosl';
        ${$req_alias}                   = trim(filter_var(${$req_alias}, FILTER_SANITIZE_STRING));
        $cust_feedback_data[$req_alias] = ${$req_alias};
        if (!${$req_alias}) {
            $error[$req_alias . '_error'] = __d('syscm', 'Required!');
        }
        // check valid no sl 
        if (!$this->ScmCustInfo->findByNoSl(${$req_alias})) {
            $error[$req_alias . '_error'] =  __d('syscm', 'Invalid!');
        }
        
        $req_alias                      = 'cust_feedback_email';
        ${$req_alias}                   = trim(filter_var(${$req_alias}, FILTER_SANITIZE_EMAIL));
        $cust_feedback_data[$req_alias] = ${$req_alias};
        if (!${$req_alias}) {
            $error[$req_alias . '_error'] = __d('syscm', 'Required a valid email!');
        }

        $req_alias                      = 'cust_feedback_phone';
        ${$req_alias}                   = trim(filter_var(${$req_alias}, FILTER_SANITIZE_STRING));
        $cust_feedback_data[$req_alias] = ${$req_alias};
        
        $req_alias                      = 'cust_feedback_cell';
        ${$req_alias}                   = trim(filter_var(${$req_alias}, FILTER_SANITIZE_STRING));
        $cust_feedback_data[$req_alias] = ${$req_alias};
        if (!${$req_alias}) {
            $error[$req_alias . '_error'] = __d('syscm', 'Required!');
        }
       
        $req_alias                      = 'cust_feedback_msg';
        ${$req_alias}                   = trim(filter_var(${$req_alias}, FILTER_SANITIZE_STRING));
        $cust_feedback_data[$req_alias] = ${$req_alias};
        if (!${$req_alias}) {
            $error[$req_alias . '_error'] = __d('syscm', 'Required!');
        }

        
        $this->set('cust_feedback_data', $cust_feedback_data);
        if ($error) {
            $this->set('cust_feedback_error', $error);
            if($request->is('ajax')){
                $msg = '';
                foreach ($error as $key => $value) {
                    $msg .= '<p>'.$keys[$key].' '.$value.'</p>';
                }
                echo json_encode(['success'=>false,'error'=>$error,'msg'=>$msg]);
            }else{
            $this->Flash->frontError(__d('syscm', 'Feedback sending failed. Please check your input.'));
                
            }
            return;
        }
        
        if (!$this->ScmCustFeedback->saveFeedback($cust_feedback_data)) {
            if($request->is('ajax')){
                $msg = __d('syscm', 'Feedback sending failed. Please try again later.');
               
                echo json_encode(['success'=>false, 'msg'=>$msg]);
            }    else{
            $this->Flash->frontError(__d('syscm', 'Feedback sending failed. Please try again later.'));

            }
            return;
        } else {
            $this->Session->write('AppFront.nosl', $cust_feedback_nosl);
            if($request->is('ajax')){
                $msg = __d('syscm', 'Thank you for your feedback. We will contact you soon.');
               
                echo json_encode(['success'=>true, 'msg'=>$msg]);
                return;
            } else{
            $this->Flash->frontSuccess(__d('syscm', 'Thank you for your feedback. We will contact you soon.'));

            }
            return $this->redirect(Router::reverse($this->request, true));
        }
    }

    /**
     * 
     */
    protected function _custRegisterPut() {
        $request  = $this->request;
        $req_data = $request->data;
        $error    = array();
        if (Configure::read('useRecaptcha')) {
            $recaptchaResponse = $request->data('g-recaptcha-response');
            $recaptchaCallenge = $this->_getRecaptcha($recaptchaResponse);
            if (!$recaptchaCallenge->success) {
                $error['recaptcha_error'] = array(
                    'message' => __d('syscm', 'Please check you are not a robot!'),
                );
            }
        }
        extract($req_data);
        $cust_reg_name                  = trim(filter_var($cust_reg_name, FILTER_SANITIZE_STRING));
        $cust_reg_data['cust_reg_name'] = $cust_reg_name;
        if (!$cust_reg_name) {
            $error['cust_reg_name_error'] = array(
                'message' => __d('syscm', 'Required!'),
            );
        }
        $cust_reg_phone                  = trim(filter_var($cust_reg_phone, FILTER_SANITIZE_STRING));
        $cust_reg_data['cust_reg_phone'] = $cust_reg_phone;
        if (!$cust_reg_phone) {
            $error['cust_reg_phone_error'] = array(
                'message' => __d('syscm', 'Required!'),
            );
        }
        $cust_reg_cell                  = trim(filter_var($cust_reg_cell, FILTER_SANITIZE_STRING));
        $cust_reg_data['cust_reg_cell'] = $cust_reg_cell;
        if (!$cust_reg_cell) {
            $error['cust_reg_cell_error'] = array(
                'message' => __d('syscm', 'Required a valid cellular number!'),
            );
        }
        $cust_reg_email                  = trim(filter_var($cust_reg_email, FILTER_SANITIZE_EMAIL));
        $cust_reg_data['cust_reg_email'] = $cust_reg_email;

        if (!$cust_reg_email) {
            $error['cust_reg_email_error'] = array(
                'message' => __d('syscm', 'Required a valid email!'),
            );
        }
        $cust_reg_addr                  = trim(nl2br(filter_var($cust_reg_addr, FILTER_SANITIZE_STRING)));
        $cust_reg_data['cust_reg_addr'] = $cust_reg_addr;
        $breaks                         = array("<br />", "<br>", "<br/>");
        if (!trim(str_ireplace($breaks, "\r\n", $cust_reg_addr))) {
            $error['cust_reg_addr_error'] = array(
                'message' => __d('syscm', 'Required!'),
            );
        }
        $reg_data                  = $cust_reg_data;
        $reg_data['cust_reg_addr'] = str_ireplace($breaks, "\r\n", $reg_data['cust_reg_addr']);
        $this->set('cust_reg_data', $reg_data);

        if ($this->ScmCustReg->checkDuplicatedEmailCell($cust_reg_email, $cust_reg_cell)) {
            $error['cust_reg_email_error'] = array(
                'message' => __d('syscm', 'Duplicated!'),
            );
            $error['cust_reg_cell_error']  = array(
                'message' => __d('syscm', 'Duplicated!'),
            );
        }

        if ($error) {
            $this->Flash->frontError(__d('syscm', 'Registration failed. Please check your input.'));
            $this->set('cust_reg_error', $error);
            return;
        }
        $cust_reg_data['client_ip'] = $this->request->clientIp();

        $dt     = date('Ymd-Hi');
        $gename = strtoupper(substr(str_replace(" ", "", $cust_reg_name), 0, 5));
        $gename = str_pad($gename, 5, "0", STR_PAD_LEFT);

        $cust_reg_data['cust_reg_code'] = "WEB-" . $dt . "-" . $gename;

        if (!$this->ScmCustReg->saveReg($cust_reg_data)) {
            $this->Flash->frontError(__d('syscm', 'Registration failed. Please try again later.'));
            return;
        } else {

            $this->Flash->frontSuccess(__d('syscm', 'Thank you for your registration. We will contact you soon.'));
            $this->request->params['named']['reg_code'] = $cust_reg_data['cust_reg_code'];
            return $this->redirect(Router::reverse($this->request, true));
        }
    }

    /**
     * 
     * @param type $response
     * @return type
     */
    protected function _getRecaptcha($response) {
        /**
         * secret 	Required. The shared key between your site and ReCAPTCHA.
          response 	Required. The user response token provided by the reCAPTCHA to the user and provided to your site on.
          remoteip 	Optional. The user's IP address.
         * 'https://www.google.com/recaptcha/api/siteverify';
         * '6LcYdRMTAAAAAPfVVt7_HDwUBsVKEAgO7lBxS6h7';
         */
        $url                    = Configure::read('Sysconfig.recaptcha.api_url');
        $parameters['secret']   = Configure::read('Sysconfig.recaptcha.api_secret');
        $parameters['response'] = $response;
        $data                   = http_build_query($parameters);

        // Initialize the PHP curl agent
        $ch     = curl_init();
        curl_setopt($ch, CURLOPT_USERAGENT, "curl");
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result);
    }

    /**
     * 
     */
    protected function _setFooterIcons() {
        $footer_icons = $this->FrontModel->getFooterIcons();
        $this->set(compact('footer_icons'));
    }

    /**
     * 
     */
    protected function _setHeaderImage() {
        $static_header_bg = $this->ScmContentMedia->getImageById($this->static_header_id);
        if ($static_header_bg) {
            $this->set(compact('static_header_bg'));
        }
    }

    /**
     * 
     */
    protected function _setMainNav() {
        $syscm_main_nav = $this->FrontModel->getSyscmMainNav();
        $selected_slug  = $this->request->here;
        return $syscm_main_nav;
        // $this->set(compact('syscm_main_nav', 'selected_slug'));
    }

    protected function _setSearchform() {
        if (!$this->useSearchform) {
            return false;
        }
        $this->useBelowheader            = true;
        $this->belowHeader['searchform'] = array(
            'searchq' => $this->searchq,
            'url'     => array('plugin'     => 'syscm', 'controller' => 'syscm_front',
                'action'     => 'front_search'),
        );
    }

    public function beforeFilter() {
        parent::beforeFilter();

        $this->set('theme_alias', $this->theme_alias);
        $this->set('icon_path', $this->icon_path);
        $this->set('logo_path', $this->logo_path);
        $this->set('enqueued_css', $this->enqueued_css);
        $this->set('title_for_layout', '');

        if ($this->request->is('ajax')) {
            $this->response->disableCache();
        }

    }

    public function beforeRender() {
        parent::beforeRender();

        // $this->_setMainNav();
        // $this->_setSearchform();
        // $this->_setbelowheader();
        // $this->_setFooterIcons();
        // $this->_setHeaderImage();
    }

    /**
     * 
     * @param type $slug
     * @param type $id
     * @return type
     */
    public function cust_bill($slug = null, $id = null) {
        $this->set('segment_key', $this->getSegmentKey());
        
        $breadcrumbs            = array();
        $breadcrumbs[]['title'] = __d('syscm', 'Customer Billing Info');
        $title_for_layout       = __d('syscm', 'Customer Billing Info');
        $enqueued_script        = array(Configure::read('Sysconfig.recaptcha.api_script'));
        $bill_period            = date('Y-m-01');
        $this->set(compact('breadcrumbs', 'enqueued_script', 'title_for_layout', 'bill_period'));

        if ($this->request->is('post')) {
            $this->_custBillCheck();
        }

        if($this->request->is('ajax')){
            // $view = new View($this,false);
            $this->layout=false; 
            return $this->render("Syscm.SyscmFront/cust_bill_ajax");
        }
    }

    /**
     * 
     * @param type $slug
     * @param type $id
     * @return type
     */
    public function cust_feedback($slug = null, $id = null) {
        $this->set('segment_key', $this->getSegmentKey());

        $breadcrumbs            = array();
        $breadcrumbs[]['title'] = __d('syscm', 'Customer Feed Back');
        $title_for_layout       = __d('syscm', 'Customer Feed Back');
        $enqueued_script        = array(Configure::read('Sysconfig.recaptcha.api_script'));
        $this->set(compact('breadcrumbs', 'enqueued_script', 'title_for_layout'));
        if ($this->request->is('post')) {
            $this->_custFeedbackPut();
            exit();
        }
        $this->_custFeedbackCheckNosl();
        $this->_custFeedbackHistory();
        if($this->request->is('ajax')){
            // $view = new View($this,false);
            $this->layout=false; 
            return $this->render("Syscm.SyscmFront/cust_feedback_ajax");
        }
    }

    /**
     * 
     * @param type $slug
     * @param type $id
     * @return type
     */
    public function cust_register($slug = null, $id = null) {
        $this->set('segment_key', $this->getSegmentKey());

        $breadcrumbs             = array();
        $breadcrumbs[0]['title'] = __d('syscm', 'Customer Registration');
        $title_for_layout        = __d('syscm', 'Customer Registration');
        $is_reg_code             = true;
        $named                   = $this->request->param('named');
        if (!$named) {
            $is_reg_code = false;
            $is_reg_print = false;
            
            $named       = array();
        }
        if (!isset($named['reg_code'])) {
            $is_reg_code = false;
        }
        $is_reg_print = isset($named['print']) ? intval($named['print']) : false;

        if ($is_reg_code) {
            if (!$this->ScmCustReg->checkRegCode($named['reg_code'])) {
                $is_reg_code = false;
            }
        }
        $reg_content = array();
        

        if ($is_reg_code) {
            $reg_content = $this->ScmCustReg->getRegByCode($named['reg_code']);
        }

        if ($reg_content) {
            //print
            $breadcrumbs[0]['url']   = "/pendaftaran";
            $breadcrumbs[1]['title'] = $named['reg_code'];
            $breadcrumbs[1]['url']   = "#";

            $title_for_layout = __d('syscm', 'Detail Registration') . ":" . $named['reg_code'] . " :: " . $title_for_layout;
            $cust_reg_content = $reg_content;
            $this->set(compact('breadcrumbs', 'title_for_layout', 'cust_reg_content'));
            $view_page = 'cust_register_detail';
            if($is_reg_print){
                $view_page .= '_print';
                $this->layout = $this->printLayout;
            }
            return $this->render('Syscm.SyscmFront/'.$view_page);
        }


        $enqueued_script = array(Configure::read('Sysconfig.recaptcha.api_script'));
        $this->set(compact('breadcrumbs', 'enqueued_script', 'title_for_layout'));

        if ($this->request->is('put')) {
            $this->_custRegisterPut();
        }
        if($this->request->is('ajax')){
            // $view = new View($this,false);
            $this->layout=false; 
            return $this->render("Syscm.SyscmFront/cust_register_ajax");
        }
    }

    /**
     * 
     * @param type $slug
     * @param type $id
     * @return type
     */
    public function front_categories($slug = null, $id = null) {

        $this->helpers[] = 'Syscm.SyscmArticle';
        if (!$slug) {
            return $this->redirect(array('action' => 'index'));
        }

        $formatted_slug = $this->FrontModel->formatSlug($this->request->pass, '/topik/');
        // die($formatted_slug);
        $title_for_layout = __d('syscm', 'Topic');
        $options          = array(
            'conditions' => array(
                'AND' => array(
                    'ScmContent.name'          => $formatted_slug,
                    'ScmContent.active_status' => '1',
                )
            )
        );
        $notPreview       = false;
        if (!$this->request->named) {
            $notPreview = true;
        }
        if (!isset($this->request->named['cid'])) {
            $notPreview = true;
        }
        if (!$notPreview) {
            if (!isset($this->request->named[md5($this->request->named['cid'] . 'id')])) {
                $notPreview = true;
            }
        }
        if (!$notPreview) {
            if ($this->request->named[md5($this->request->named['cid'] . 'id')] != md5($this->request->named['cid'] . date('d'))) {
                $notPreview = true;
            }
        }
        if (!$notPreview) {

            $options['conditions']['AND'] ['ScmContent.id'] = $this->request->named['cid'];
        } else {
            $options['conditions']['AND'] ['ScmContentPublished.menu_code'] = $this->ScmContentPublished->fieldMenuCodePublished;
        }
        $this->ScmContent->recursive = 1;
        $syscm_contents              = $this->ScmContent->find('first', $options);

        $cats        = array();
        $breadcrumbs = array();
        if ($syscm_contents) {

            $title_for_layout = __d('syscm', $syscm_contents['ScmContent']['text']);
            $cats             = $syscm_contents['ScmCategory'];
            $article = $this->FrontContent->getFormattedArticle($syscm_contents);
            $i                = 0;
            if (isset($syscm_contents['ParentContent'])) {
                if ($syscm_contents['ParentContent']['id']) {
                    $par = $this->ScmContent->findById($syscm_contents['ParentContent']['id']);
                    if (isset($par['ParentContent']['id'])) {
                        $breadcrumbs [$i]['title'] = $par['ParentContent']['text'];
                        $breadcrumbs [$i]['url']   = $par['ParentContent']['name'];
                        $i++;
                    }
                    $breadcrumbs [$i]['title'] = $syscm_contents['ParentContent']['text'];
                    $breadcrumbs [$i]['url']   = $syscm_contents['ParentContent']['name'];
                    $i++;
                }
            }

            $breadcrumbs [$i]['title']          = $syscm_contents['ScmContent']['text'];
            //fb_share
            /*
            $fb_share                           = array();
            $fb_share['meta']['og:url']         = Router::url($syscm_contents['ScmContent']['name'], true);
            $fb_share['meta']['og:type']        = "website";
            $fb_share['meta']['og:title']       = $syscm_contents['ScmContent']['text'];
            $fb_share['meta']['og:description'] = $this->FrontContent->getExcerpt($syscm_contents['ScmContentContent']['value'], 50);
            $fb_share['meta']['og:image']       = Router::url($this->FrontContent->getFirstImg($syscm_contents['ScmContentContent']['value']), true);


            $this->set(compact('fb_share'));
            */
        }

        $cat_names = array();
        if ($cats) {
            foreach ($cats as $v) {
                $cat_names[] = $v['name'];
            }
        }
        // print_r($cat_names);
        // die();
        $article['comment_enabled'] = false;
        $syscm_template = "single-post";
        $singlepost     = true;
        $related_list   = array();
        if ($cat_names) {
            //comment
            if (in_array('comment_enable', $cat_names)) {
                $article['comment_enabled'] = true;
                $content_id = $syscm_contents['ScmContent']['id'];
                $this->_custCommentByContent($syscm_contents['ScmContent']['id']);
            }
            //./comment

            if (in_array('parent', $cat_names)) {
                $featured_heading          = array(
                    'title'   => $syscm_contents['ScmContent']['text'],
                    'summary' => $syscm_contents['ScmContentContent']['value'],
                );
                $syscm_template            = "parent-category";
                /**
                 * cat Main list
                 */
                // $this->Paginator->settings = $this->FrontContent->getCatlistPaginateSettings();
                try {
                    // $catlist_rs = $this->Paginator->paginate('FrontContent', $this->FrontContent->getCatlistPaginateConditions('/topik/berita'));
                } catch (NotFoundException $e) {
                    $this->request->params['named']['page'] = 1;
                    return $this->redirect(Router::reverse($this->request, true));
                }
                #debug($top_mainlist_rs);die();
                // $this->set('mainlist', $this->FrontContent->getFormattedMainlist($catlist_rs));
            } else {
                //single post
                // $related_list = $this->FrontModel->getListByParentName($syscm_contents['ParentContent']['name'], array(
                    // $syscm_contents['ScmContent']['id']));
                // $this->Paginator->settings = $this->FrontContent->getCatlistPaginateSettings();
                try {
                    // $catlist_rs = $this->Paginator->paginate('FrontContent', $this->FrontContent->getCatlistPaginateConditions('/topik/berita'));
                } catch (NotFoundException $e) {
                    $this->request->params['named']['page'] = 1;
                    return $this->redirect(Router::reverse($this->request, true));
                }
                #debug($top_mainlist_rs);die();
                // $this->set('mainlist', $this->FrontContent->getFormattedMainlist($catlist_rs));
            }
        }
        $sidelist = array();
        if ($related_list) {
            $sidelist[] = $related_list;
        }
        $sidelist[] = [];//$this->FrontModel->getListByParentName('/web/tentang_kami');
        $sidelist[] = [];//$this->FrontModel->getListByParentName('/web/layanan_informasi');

        $this->set(compact('formatted_slug','syscm_contents', 'syscm_template', 'title_for_layout', 'sidelist', 'featured_heading', 'breadcrumbs','article','content_id'));
        if($this->request->is('ajax')){
            // $view = new View($this,false);
            $this->layout=false; 
            return $this->render("Syscm.SyscmFront/front_pages_ajax");
        }else{
            return $this->render('Syscm.SyscmFront/front_pages');
        }
        
    }

    /**
     * 
     * @param type $slug
     * @param type $id
     * @return type
     */
    public function front_feat_categories($slug = null, $id = null) {
        $this->helpers[] = 'Syscm.SyscmArticle';
        if (!$slug) {
            return $this->redirect(array('action' => 'index'));
        }

        return $this->render('Syscm.SyscmFront/front_pages');
    }

    public function front_pages($slug = null, $id = null) {

        $segment_key = $this->getSegmentKey();

        if(preg_match('/web\.organisasi/', $segment_key)){
            $segment_key = 'organisasi';
        }
        
        $this->set('segment_key', $segment_key);

        $this->helpers[] = 'Syscm.SyscmArticle';
        if (!$slug) {
            return $this->redirect(array('action' => 'index'));
        }

        $formatted_slug = $this->FrontModel->formatSlug($this->request->pass);
        $options        = array(
            'conditions' => array(
                'AND' => array(
                    'ScmContent.name'          => $formatted_slug,
                    'ScmContent.active_status' => '1',
                )
            )
        );


        $notPreview     = false;
        if (!$this->request->named) {
            $notPreview = true;
        }
        if (!isset($this->request->named['cid'])) {
            $notPreview = true;
        }
        if (!$notPreview) {
            if (!isset($this->request->named[md5($this->request->named['cid'] . 'id')])) {
                $notPreview = true;
            }
        }
        if (!$notPreview) {
            if ($this->request->named[md5($this->request->named['cid'] . 'id')] != md5($this->request->named['cid'] . date('d'))) {
                $notPreview = true;
            }
        }
        if (!$notPreview) {

            $options['conditions']['AND'] ['ScmContent.id'] = $this->request->named['cid'];
        } else {
            $options['conditions']['AND'] ['ScmContentPublished.menu_code'] = $this->ScmContentPublished->fieldMenuCodePublished;
        }
        $this->ScmContent->recursive = 1;
        
        // if(in_array($slug, ['layanan_informasi','tentang_kami','layanan_informasi','ppid']) && !empty($id)){
        //     $this->ScmContent->recursive = 1;
        // }
        if(in_array($slug, ['layanan_informasi','tentang_kami','layanan_informasi','ppid','organisasi']) && empty($id)){
            return $this->redirect(array('action' => 'index'));
        }
        $syscm_contents              = $this->ScmContent->find('first', $options);
       
        $title_for_layout = __d('syscm', 'Web Page');


        $cats        = array();
        $breadcrumbs = array();
        if ($syscm_contents) {
            $title_for_layout = __d('syscm', $syscm_contents['ScmContent']['text']);
            $cats             = $syscm_contents['ScmCategory'];
            $article = $this->FrontContent->getFormattedArticle($syscm_contents);
            $i                = 0;
            if (isset($syscm_contents['ParentContent'])) {
                if ($syscm_contents['ParentContent']['name']) {
                    $par = $this->ScmContent->findById($syscm_contents['ParentContent']['id']);
                    if (isset($par['ParentContent']['id'])) {
                        $breadcrumbs [$i]['title'] = $par['ParentContent']['text'];
                        $breadcrumbs [$i]['url']   = $par['ParentContent']['name'];
                        $i++;
                    }
                    $breadcrumbs [$i]['title'] = $syscm_contents['ParentContent']['text'];
                    $breadcrumbs [$i]['url']   = $syscm_contents['ParentContent']['name'];
                    $i++;
                }
            }
            $article['comment_enabled'] = false;
            $breadcrumbs [$i]['title']          = $syscm_contents['ScmContent']['text'];
            //fb_share
            $fb_share                           = array();
            $fb_share['meta']['og:url']         = Router::url($syscm_contents['ScmContent']['name'], true);
            $fb_share['meta']['og:type']        = "website";
            $fb_share['meta']['og:title']       = $syscm_contents['ScmContent']['text'];
            $fb_share['meta']['og:description'] = $this->FrontContent->getExcerpt($syscm_contents['ScmContentContent']['value'], 50);
            $fb_share['meta']['og:image']       = Router::url($this->FrontContent->getFirstImg($syscm_contents['ScmContentContent']['value']), true);
            // $this->set(compact('fb_share'));
        }

        $syscm_template = "single";
        $cat_names      = array();
        $sidelist       = array();
        if ($cats) {
            foreach ($cats as $v) {
                $cat_names[] = $v['name'];
            }
        }
        if ($cat_names) {
            //comment
            if (in_array('comment_enable', $cat_names)) {
                $enqueued_script = array(Configure::read('Sysconfig.recaptcha.api_script'));
                $syscm_comment   = array('count' => 20, 'ls' => array());
                $article['comment_enabled'] = true;

                // $this->set(compact('syscm_comment', 'enqueued_script'));
            }
            //./comment


            if (in_array('parent', $cat_names)) {
                $featured_heading          = array(
                    'title'   => $syscm_contents['ScmContent']['text'],
                    'summary' => $syscm_contents['ScmContentContent']['value'],
                );
                $syscm_template            = 'parent';
                /**
                 * cat Main list
                 */
                $this->Paginator->settings = $this->FrontContent->getCatlistPaginateSettings();
                $this->FrontContent->includeContent(true);
                try {
                    $catlist_rs = $this->Paginator->paginate('FrontContent', $this->FrontContent->getCatlistPaginateConditions($syscm_contents['ScmContent']['name']));
                } catch (NotFoundException $e) {
                    $this->request->params['named']['page'] = 1;
                    return $this->redirect(Router::reverse($this->request, true));
                }
                #debug($top_mainlist_rs);die();
                $this->set('mainlist', $this->FrontContent->getFormattedMainlist($catlist_rs));
            } else {
                //single post
                //$fb_share['meta']['og:description'] = $syscm_contents['ScmContent']['text'];
                /**
                 * <meta property="og:type"          content="website" />
                  <meta property="og:title"         content="Your Website Title" />
                  <meta property="og:description"   content="Your description" />
                  <meta property="og:image"         content="http://www.your-domain.com/path/image.jpg" />;
                 */
                $this->set(compact('fb_share'));
                $this->Paginator->settings = $this->FrontContent->getCatlistPaginateSettings();
                try {
                    // $catlist_rs = $this->Paginator->paginate('FrontContent', $this->FrontContent->getCatlistPaginateConditions('/topik/berita'));
                } catch (NotFoundException $e) {
                    $this->request->params['named']['page'] = 1;
                    return $this->redirect(Router::reverse($this->request, true));
                }
                #debug($top_mainlist_rs);die();
                // $this->set('mainlist', $this->FrontContent->getFormattedMainlist($catlist_rs));

            }
            if ($syscm_contents) {
                if (isset($syscm_contents['ParentContent'])) {
                    $par_list = [];//$this->FrontModel->getListByParentName($syscm_contents['ParentContent']['name'], array(
                        // $syscm_contents['ScmContent']['id']));

                    if ($par_list['url']) {
                        $sidelist[] = $par_list;
                        while ($par_list['parent_name']) {
                            $par_list_x = $par_list;
                            $par_list   = [];//$this->FrontModel->getListByParentName($par_list_x['parent_name'], array(
                                // $par_list_x['id']));
                            if ($par_list['url']) {
                                $sidelist[] = [];//$par_list;
                            }
                        }
                    }
                }
            }
        }




        // parent to search


        $sidelist[] =[];// $this->FrontModel->getListByParentName('/topik/berita');

       $this->set(compact('syscm_contents', 'title_for_layout', 'featured_heading', 'breadcrumbs', 'sidelist', 'syscm_template','article'));

        if($this->request->is('ajax')){
            // $view = new View($this,false);
            $this->layout=false; 
            return $this->render("Syscm.SyscmFront/front_pages_ajax");
        }
    }

    public function front_search($searchq = false) {
        $this->helpers[]  = 'Syscm.SyscmArticle';
        $title_for_layout = __d('syscm', 'Search');
        $syscm_template   = 'search';

        $breadcrumbs            = array(['title'=>'Beranda']);
        // $breadcrumbs[]['title'] = __d('syscm', 'Search');

        $data             = false;
        $data_list        = array();
        $sidelist_right   = array();
        $sidelist_right[] = [];//$this->FrontModel->getListByParentName('/web/layanan_informasi');
        $sidelist_right[] = [];//$this->FrontModel->getListByParentName('/web/tentang_kami');
        $sidelist_right[] = [];//$this->FrontModel->getListByParentName('/topik/berita');

        $this->set(compact('sidelist_right', 'syscm_template', 'search'));


        if ($this->request->is('post')) {
            $data = $this->request->data;
            if (isset($data['searchq'])) {
                return $this->redirect(array('action' => 'front_search', $data['searchq']));
            }
        }

        if (!$searchq) {
            if($this->request->is('ajax')){
                // $view = new View($this,false);
                $this->layout=false; 
                return $this->render("Syscm.SyscmFront/front_pages_ajax");
            }
            return $this->render("Syscm.SyscmFront/front_pages");
        }
        $this->searchq = $searchq;
        $options       = $this->FrontContent->searchContent($searchq);
        if ($options) {
            $this->Paginator->settings['limit'] = 5;
            try {
                $data_list = $this->Paginator->paginate('FrontContent', $options['conditions']);
            } catch (NotFoundException $e) {
                $this->request->params['named']['page'] = 1;
                return $this->redirect(Router::reverse($this->request, true));
            }
        }

        $title_for_layout .= ": " . h($searchq);
        $breadcrumbs[] = ['title' => $title_for_layout];
        $this->set(compact('data_list', 'searchq', 'title_for_layout', 'breadcrumbs'));
        if($this->request->is('ajax')){
            // $view = new View($this,false);
            $this->layout=false; 
            return $this->render("Syscm.SyscmFront/front_pages_ajax");
        }

        return $this->render("Syscm.SyscmFront/front_pages");
    }
    
    public function index() {
        $this->helpers[]  = 'Syscm.SyscmArticle';
        $title_for_layout = '';


        $enqueued_script   = array(
            'jssor/jssor.slider.mini',
            'jssor_init'
        );
        $enqueued_css      = array(
            'jssor'
        );
        /* $featured_banner           = $this->FrontModel->getSyscmBannerHome(); */
        $featured_contents = [];//$this->FrontContent->getFeaturedContents();

        $sidelist         = array();
        $sidelist[]       = [];//$this->FrontModel->getMenuWebLink('call_center', 'web_link', 'Call Center', 'fa-phone', '_self');
        $sidelist[]       = [];//$this->FrontModel->getMenuWebLink();
        $sidelist[]       = [];//$this->FrontModel->getListByParentName('/web/tentang_kami');
        //announcement
        $sidelist[]       = [];//$this->FrontContent->getAnnouncements();
        #debug($announcement_list);die();
        $sidelist_right   = array();
        $this->set('use_sss', 1);
        $sidelist_right['video_galery'] =[];// $this->FrontContent->getVideoGallery_v2();
        $sidelist_right['image_galery'] =[]; //$this->FrontContent->getImageGallery_v2();
        // $sidelist_right[] = $this->FrontModel->getListByParentName('/web/layanan_informasi/kantor_layanan');
        // $sidelist_right[] = $this->FrontModel->getListByParentName('/web/layanan_informasi');

        $this->Paginator->settings = $this->FrontContent->getMainlistPaginateSettings();
        try {
            $top_mainlist_rs = [];//$this->Paginator->paginate('FrontContent', $this->FrontContent->getMainlistPaginateConditions());
        } catch (NotFoundException $e) {
            $this->request->params['named']['page'] = 1;
            return $this->redirect(Router::reverse($this->request, true));
        }


        #debug($top_mainlist_rs);die();
        $top_mainlist = [];//$this->FrontContent->getFormattedMainlist_v2($top_mainlist_rs);
        $mainparentrs = [];//$this->FrontContent->findFirstByName($this->FrontContent->mainlistParentName);

        $top_mainlist['title'] = isset($mainparentrs['FrontContent']['text']) ? $mainparentrs['FrontContent']['text'] : "";

        $title_for_layout = 'Beranda';
        $this->set(compact('page_title','enqueued_css', 'enqueued_script', 'title_for_layout', 'featured_contents', 'sidelist', 'top_mainlist', 'sidelist_right'));
        // $this->Session->delete('homepage_popup_content');
        if(!$this->Session->check('homepage_popup_content')){
            $this->Session->write('homepage_popup_content', 1);
            $homepage_popup_content = $this->FrontContent->getHomepagePopup();
            $this->set(compact('homepage_popup_content'));
        }
        // 
        if($this->request->is('ajax')){
            // $view = new View($this,false);
            $this->layout=false; 
            return $this->render("Syscm.SyscmFront/index_ajax");
        }
        
    }

}
