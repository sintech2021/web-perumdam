<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('SyscmAppController', 'Syscm.Controller');

/**
 * CakePHP SyscmCategories
 * @author ino
 */
class SyscmLayoutsController  extends SyscmAppController 
{
    public $uses = array(
        'ScmLayout'
    );

    public function add()
    {
        $this->SysAcl->isAcc('add');
        $this->_setTopMenuByModel('ScmLayout');
         $this->set('browser_menu_title', __d('syscm', 'Add Layouts'));
    }
    
    public function index($id=null)
    {
        $this->SysAcl->isAcc();
        $this->_setTopMenuByModel('ScmLayout');
         $this->set('browser_menu_title', __d('syscm', 'Layouts'));
    }

}
