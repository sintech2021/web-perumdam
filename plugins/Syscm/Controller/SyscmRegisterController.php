<?php
require_once  APP .'/webroot/xcrud/xcrud.php';
App::uses('SyscmAppController', 'Syscm.Controller');

/**
 * 
 */
class SyscmRegisterController extends SyscmAppController  
{
	public $uses             = ['Syscm.Pendaftaran'];

	public function index($value='')
	{
		$browser_menu_title = __d('syscm', 'Registrasi Pelanggan');
		$sub_title = 'Registrasi Pelanggan';
        $this->set(compact('browser_menu_title','sub_title'));

		$this->SysAcl->isAcc();
		$xcrud = Xcrud::get_instance();

		$xcrud->table('sys_pendaftaran');
		$xcrud->columns(['kode_pendaftaran','nama_pemohon','alamat','kabupaten','kecamatan','kelurahan','kode_pos','nomor_hp','foto_ktp','status']);
		$xcrud->change_type('foto_ktp','image','',array('path'=>'../uploads/pendaftaran','url'=>BASE_URL.'/uploads/pendaftaran')); // crop it as you want
		$xcrud->unset_title();
		// $xcrud->show_primary_ai_column(true);

		$xcrud->fields(['kode_pendaftaran','nama_pemohon','nama_pemilik','nomor_ktp','foto_ktp','nomor_hp','email','kabupaten','kecamatan','kelurahan','alamat','kode_pos','status']);
		$xcrud->button(BASE_URL.'/syscm/syscm_register/detail/{id}/{kode_pendaftaran}');
		
		// $xcrud->pass_var('created', date('Y-m-d H:i:s'), 'create');
		// $xcrud->pass_var('edited', date('Y-m-d H:i:s'), 'create');
		// $xcrud->pass_var('edited', date('Y-m-d H:i:s'), 'edit');
		// $xcrud->links_label('<i class="fa fa-home"></i> Home');
		// $xcrud->unset_print();
		// $xcrud->unset_csv();
		$xcrud->unset_add();
		// $xcrud->unset_search();
		$xcrud->change_type('status','select','0',['0'=>'Belum Proses','1'=>'Proses']);

		$this->set('xcrud',$xcrud->render()) ;


	}
	public function _getToken($client)
	{
		$end_point =  'https://api-web.perumdamtkr.com/';
        $token_end_point = $end_point . 'authtimeout/token';
        $response = $client->request('GET', $token_end_point,[]);
		if( $response->getStatusCode() == 200){
            $body = $response->getBody();
            $obj = json_decode($body);
            return $obj->token;
        }
        return '';
	}
	/*
	 Pendaftaran	POSThttps://api-web.perumdamtkr.com/register
	Form Data
	pemohon:pemohon
	pemilik:pemilik
	almtPasang:alamtPasang
	noHp:097893838638
	kdPos:1
	kdProp:2
	kdKab:3
	kdKec:4
	kdKel:5
	photoName:base64photo
	email:email.com
	noKtp:01389265	HEADER :
	Key : Authorization
	value: token
	$nama_pemohon, $nama_pemilik, $alamat, $nomor_hp, $kode_pos, $id_prop, $id_kab, $id_kec, $id_kel, $foto64, $email, $nik
    */
    public function _sendRegister($form_data, $token, $client)
    {
    	$end_point =  'https://api-web.perumdamtkr.com/register';
      
        $response = $client->request('GET', $end_point,[]);
		if( $response->getStatusCode() == 200){
            $body = $response->getBody();
            $obj = json_decode($body);
            return $obj;
        }
        return '';
    }
	public function proses()
	{
		$this->SysAcl->isAcc();
		if ($this->request->is('ajax')) {
		    $this->response->disableCache();
			// print_r($_POST);
			$photoName = $_POST['photoName'];
			$id = $_POST['id'];
			unset($_POST['id']);

			$photo_path =  APP.'/webroot/uploads/pendaftaran/'.$photoName;

			$buffer = file_get_contents($photo_path);
			$buffer64 = base64_encode($buffer);
			$_POST['photoName'] = $buffer64;
		    // echo json_encode(['success'=>false,'error'=>$error]);
		    $client = new \GuzzleHttp\Client();
		 	$token  = $this->_getToken($client);

		 	if( !empty($token) ){
		 		$obj = $this->_sendRegister($_POST, $token, $client);
		 		// echo json_encode($obj);
		        $this->Pendaftaran->read(null,$id);
		        $this->Pendaftaran->set(['status'=>1]);
		        if($this->Pendaftaran->save()){
		        	echo json_encode(['success'=>true,'obj'=>$obj]);
		        }
		 	}

        	// get token
        	// send registration data
		}
		exit();
	}
	public function detail($id='',$kode_pendaftaran="")
	{
		$this->SysAcl->isAcc();
		$reg = $this->Pendaftaran->findById($id);
		$reg_json = [];
		if(!empty($reg)){
			$reg = $reg['Pendaftaran'];
			$reg_json = [
				'pemohon' => $reg['nama_pemohon'],
				'pemilik' => $reg['nama_pemilik'],
				'almtPasang' => $reg['alamat'],
				'noHp' => $reg['nomor_hp'],
				'kdPos' => $reg['kode_pos'],
				'kdProp' => $reg['id_prov'],
				'kdKab' => $reg['id_kab'],
				'kdKec' => $reg['id_kec'],
				'kdKel' => $reg['id_kel'],
				'photoName' => $reg['foto_ktp'],
				'email' => $reg['email'],
				'noKtp' => $reg['nomor_ktp'] ,
				'noHp' => $reg['nomor_hp'] 
			];
		}
		$this->set('reg',$reg);
		$this->set('id',$id);
		$this->set('reg_json',$reg_json);
	}
}