<?php

/*
 * The MIT License
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


App::uses('SyscmAppController', 'Syscm.Controller');

/**
 * CakePHP SyscmCustController
 * @author ino
 */
class SyscmCustCommentController extends SyscmAppController {

    public $uses       = array(
        'Syscm.ScmCustComment',
        'Syscm.ScmCustCommentReply',
    );
    public $components = array('Sysadmin.SysAcl', 'Sysadmin.SysLog', 'Sysadmin.SysAdmUserRoles',
        'Session', 'Paginator', 'Flash');

    /**
     * 
     */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->topMenuByModelSetup['model_alias'] = 'ScmCustComment';
    }

    public function delete($id) {
        $this->SysAcl->isAcc('delete');
        if (!$id) {
            $this->Flash->error(__d('syscm', 'Missing request parameter! Your requested page/action has been redirected.'));
            return $this->redirect(array('action' => 'index'));
        }
        $cust = $this->ScmCustComment->findById($id);
        if (!$cust) {
            $this->Flash->error(__d('syscm', 'Invalid Comment requested!'));
            return $this->redirect(array('action' => 'index'));
        }
        if (!$this->ScmCustComment->dropStatus($id)) {
            $this->Flash->error(__d('syscm', 'Comment delete failed!'));
            return $this->redirect(array('action' => 'index'));
        } else {
            $this->Flash->success(__d('syscm', 'Comment has been deleted!'));
        }
        
        return $this->redirect(array('action' => 'index'));
    }
    
    public function delete_reply($parent_id, $id) {
        $this->SysAcl->isAcc('delete');
        if (!$id || !$parent_id) {
            $this->Flash->error(__d('syscm', 'Missing request parameter! Your requested page/action has been redirected.'));
            return $this->redirect(array('action' => 'reply_list', $parent_id));
        }
        $cust = $this->ScmCustComment->findById($id);
        if (!$cust) {
            $this->Flash->error(__d('syscm', 'Invalid Comment requested!'));
        }
        if (!$this->ScmCustComment->dropStatus($id)) {
            $this->Flash->error(__d('syscm', 'Comment delete failed!'));
            return $this->redirect(array('action' => 'reply_list', $parent_id));
        } else {
            $this->Flash->success(__d('syscm', 'Comment has been deleted!'));
        }
        
        return $this->redirect(array('action' => 'reply_list', $parent_id));
    }

    public function index($id = null) {
        $this->SysAcl->isAcc();
        $this->set('browser_menu_title', __d('syscm', 'Visitor Comments'));
        $this->Paginator->settings = $this->ScmCustComment->getIndexPaginationSetting();
        $conditions                = array(
            'AND' => array(
                'ScmCustComment.active_status' => '1',
                'ScmCustComment.menu_code'     => $this->ScmCustComment->threadMenuCode,
            )
        );
        try {
            $data_list = $this->Paginator->paginate('ScmCustComment', $conditions);
        } catch (NotFoundException $e) {
            $this->request->params['named']['page'] = 1;
            return $this->redirect(Router::reverse($this->request, true));
        }

        $this->set(compact('data_list'));
    }

    public function reply_add($id) {

        $this->SysAcl->isAcc('edit');
        if ($this->request->is(array('post', 'put'))) {
            $data                                           = $this->request->data;
            $comment_id                                     = CakeString::uuid();
            $data['ScmCustComment']['id']                   = $comment_id;
            $data['ScmCustComment']['active_status']        = 1;
            // $data['ScmCustComment']['parent_id']     = $reply_cid;1 posted
            $data['ScmCustComment']['menu_code']            = $this->ScmCustComment->threadReplyAdminMenuCode;
            $appUsrInfo                                     = $this->SysAcl->getUserInfo($this->Session->read('AppUsrLog.uid'));
            $data['ScmCustComment']['name']                 = $appUsrInfo['SysAclUser']['name'];
            // $data['ScmCustComment']['text']          = $scm_content_id;1 posted
            // $data['ScmCustComment']['annotation']    = $cust_message_data;1 posted
            // email
            $data['ScmCustCommentEmail']['scm_customer_id'] = $comment_id;
            $data['ScmCustCommentEmail']['active_status']   = '1';
            $data['ScmCustCommentEmail']['field']           = 'ScmCustCommentEmail';
            $data['ScmCustCommentEmail']['value']           = $appUsrInfo['SysAclUser']['email'];
            //add username on email.name
            $data['ScmCustCommentEmail']['name']           = $appUsrInfo['SysAclUser']['username'];

            //phone
            $data['ScmCustCommentPhone']['scm_customer_id'] = $comment_id;
            $data['ScmCustCommentPhone']['active_status']   = '1';
            $data['ScmCustCommentPhone']['field']           = 'ScmCustCommentPhone';
            $data['ScmCustCommentPhone']['value']           = '-';

            //ip
            $data['ScmCustCommentIp']['scm_customer_id'] = $comment_id;
            $data['ScmCustCommentIp']['active_status']   = '1';
            $data['ScmCustCommentIp']['field']           = 'ScmCustCommentIp';
            $data['ScmCustCommentIp']['value']           = $this->request->clientIp();
            //add user id on ip.name
            $data['ScmCustCommentIp']['name']           = $appUsrInfo['SysAclUser']['id'];

            /**
              $data['ScmCustComment']['id']            = $comment_id;
              $data['ScmCustComment']['active_status'] = '1';1
              $data['ScmCustComment']['parent_id']     = $reply_cid;1
              $data['ScmCustComment']['menu_code']     = $this->threadReplyMenuCode;1
              $data['ScmCustComment']['name']          = $cust_name_data;1
              $data['ScmCustComment']['text']          = $scm_content_id;1
              $data['ScmCustComment']['annotation']    = $cust_message_data;1

              $data['ScmCustCommentEmail']['scm_customer_id'] = $comment_id;
              $data['ScmCustCommentEmail']['active_status']   = '1';
              $data['ScmCustCommentEmail']['field']           = 'ScmCustCommentEmail';
              $data['ScmCustCommentEmail']['value']           = $cust_email_data;

              $data['ScmCustCommentPhone']['scm_customer_id'] = $comment_id;
              $data['ScmCustCommentPhone']['active_status']   = '1';
              $data['ScmCustCommentPhone']['field']           = 'ScmCustCommentPhone';
              $data['ScmCustCommentPhone']['value']           = $cust_telp_data;

              $data['ScmCustCommentIp']['scm_customer_id'] = $comment_id;
              $data['ScmCustCommentIp']['active_status']   = '1';
              $data['ScmCustCommentIp']['field']           = 'ScmCustCommentIp';
              $data['ScmCustCommentIp']['value']           = $client_ip;
             */
            if ($this->ScmCustComment->saveAssociated($data)) {
                $this->Flash->success(__d('syscm', 'Reply has been saved!'));
                return $this->redirect(array('action' => 'reply_edit', $id, $comment_id));
            } else {
                $this->Flash->error(__d('syscm', 'Customer add failed. Please try again!'));
            }
        }
        $browser_menu_title = __d('syscm', 'Add Reply');
        $this->set(compact('browser_menu_title'));

        $this->topMenuByModelSetup['invoke'] = $this->ScmCustComment->invokeTopMenuOnReplyList($this->request->params);

        //

        $this->ScmCustComment->recursive = 0;
        $data_row                        = $this->ScmCustComment->find('first', array(
            'conditions' => array('ScmCustComment.id' => $id)));
        $this->set(compact('data_row'));
    }

    public function reply_edit($id, $repy_id) {
        $this->SysAcl->isAcc('edit');
        
        //
        if ($this->request->is(array('post', 'put'))) {
            $data                                           = $this->request->data;
            
            $data['ScmCustComment']['active_status']        = 1;
            
            $appUsrInfo                                     = $this->SysAcl->getUserInfo($this->Session->read('AppUsrLog.uid'));
            $data['ScmCustComment']['name']                 = $appUsrInfo['SysAclUser']['name'];
            
            //add username on email.name
            $data['ScmCustCommentEmail']['name']           = $appUsrInfo['SysAclUser']['username'];

            $data['ScmCustCommentIp']['value']           = $this->request->clientIp();
            //add user id on ip.name
            $data['ScmCustCommentIp']['name']           = $appUsrInfo['SysAclUser']['id'];

            if ($this->ScmCustComment->saveAssociated($data)) {
                $this->Flash->success(__d('syscm', 'Reply has been saved!'));
                return $this->redirect(array('action' => 'reply_edit', $id, $repy_id));
            } else {
                $this->Flash->error(__d('syscm', 'Customer add failed. Please try again!'));
            }
        }
        
        //
        
        $browser_menu_title = __d('syscm', 'Edit Reply');
        $this->set(compact('browser_menu_title'));

        $this->topMenuByModelSetup['invoke'] = $this->ScmCustComment->invokeTopMenuOnReplyList($this->request->params);

        //
        
        $this->request->data         = $this->ScmCustComment->find('first', array(
            'conditions' => array('ScmCustComment.id' => $repy_id)));
        $this->ScmCustComment->recursive = 0;
        $data_row                        = $this->ScmCustComment->find('first', array(
            'conditions' => array('ScmCustComment.id' => $id)));
        $this->set(compact('data_row'));
    }
    public function reply_list($id) {
        $this->topMenuByModelSetup['invoke'] = $this->ScmCustComment->invokeTopMenuOnReplyList($this->request->params);
        $this->SysAcl->isAcc('edit');
        $this->set('browser_menu_title', __d('syscm', 'Visitor Comments'));
        $this->Paginator->settings           = array(
            'order' => array('ScmCustComment.created_dt' => 'asc'),
        );
        $conditions                          = array(
            'AND' => array(
                'ScmCustComment.parent_id'     => $id,
                'ScmCustComment.active_status' => '1',
                'ScmCustComment.menu_code'     => array(
                    $this->ScmCustComment->threadReplyMenuCode,
                    $this->ScmCustComment->threadReplyAdminMenuCode,
                ),
            )
        );
        try {
            $data_list = $this->Paginator->paginate('ScmCustComment', $conditions);
        } catch (NotFoundException $e) {
            $this->request->params['named']['page'] = 1;
            return $this->redirect(Router::reverse($this->request, true));
        }
        $this->ScmCustComment->recursive = 0;
        $data_row                        = $this->ScmCustComment->find('first', array(
            'conditions' => array('ScmCustComment.id' => $id)));
        $this->set(compact('data_list', 'data_row'));
    }

}
