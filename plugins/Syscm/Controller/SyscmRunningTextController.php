<?php
require_once  APP .'/webroot/xcrud/xcrud.php';
App::uses('SyscmAppController', 'Syscm.Controller');

/**
 * 
 */
class SyscmRunningTextController extends SyscmAppController  
{
	
	public function index($value='')
	{
		$browser_menu_title = __d('syscm', 'Running Text');
		$sub_title = 'Running Text';
        $this->set(compact('browser_menu_title','sub_title'));

		$this->SysAcl->isAcc();
		$xcrud = Xcrud::get_instance();

		$xcrud->table('sys_running_text');
		$xcrud->columns(['content']);

		$xcrud->unset_title();
		$xcrud->show_primary_ai_column(true);

		$xcrud->fields(['content']);
		$xcrud->label('content','Isi');

		// $xcrud->button(BASE_URL.'/syscm/syscm_galery/browse/{tipe}/{id}/{title}');
		
		// $xcrud->pass_var('created', date('Y-m-d H:i:s'), 'create');
		// $xcrud->pass_var('edited', date('Y-m-d H:i:s'), 'create');
		// $xcrud->pass_var('edited', date('Y-m-d H:i:s'), 'edit');
		$xcrud->unset_print();
		$xcrud->no_editor('content');
		$xcrud->unset_csv();
		$xcrud->unset_search();
		// $xcrud->change_type('tipe','select','0',['0'=>'Image','1'=>'Video']);

		$this->set('xcrud',$xcrud->render()) ;


	}
}