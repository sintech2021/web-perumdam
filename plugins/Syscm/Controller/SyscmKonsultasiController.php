<?php
require_once  APP .'/webroot/xcrud/xcrud.php';
App::uses('SyscmAppController', 'Syscm.Controller');

/**
 * 
 */
class SyscmKonsultasiController extends SyscmAppController  
{
	
	public function index($value='')
	{
		$browser_menu_title = __d('syscm', 'Pojok Konsultasi');
		$sub_title = 'Pojok Konsultasi';
        $this->set(compact('browser_menu_title','sub_title'));

		$this->SysAcl->isAcc();
		$xcrud = Xcrud::get_instance();

		$xcrud->table('sys_konsultasi');
		$xcrud->columns(['title','keterangan']);

		$xcrud->unset_title();
		$xcrud->show_primary_ai_column(true);

		$xcrud->fields(['title','keterangan']);
		$xcrud->unset_print();
		$xcrud->unset_csv();
		$xcrud->unset_search();

		$this->set('xcrud',$xcrud->render()) ;


	}
}