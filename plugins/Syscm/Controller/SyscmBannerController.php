<?php
require_once  APP .'/webroot/xcrud/xcrud.php';
App::uses('SyscmAppController', 'Syscm.Controller');

/**
 * 
 */

class SyscmBannerController extends SyscmAppController  
{
	
	public function index($value='')
	{
		$browser_menu_title = __d('syscm', 'Homepage Banner');
		$sub_title = 'Homepage Banner';
        $this->set(compact('browser_menu_title','sub_title'));

		$this->SysAcl->isAcc();
		$xcrud = Xcrud::get_instance();

		$xcrud->table('sys_banner');
		$xcrud->columns(['path','url']);

		$xcrud->unset_title();
		$xcrud->show_primary_ai_column(true);

		$xcrud->fields(['path','url']);
		$xcrud->label('path','Gambar');
		$xcrud->label('url','Link');

		$xcrud->unset_print();
		$xcrud->unset_csv();
		$xcrud->unset_search();
		$xcrud->change_type('path','image','',array('path'=>'../uploads/banner_images','url'=>BASE_URL.'/uploads/banner_images')); // crop it as you want
		$this->set('xcrud',$xcrud->render()) ;


	}
}