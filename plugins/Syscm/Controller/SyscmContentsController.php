<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('SyscmAppController', 'Syscm.Controller');

/**
 * CakePHP SyscmContentsController
 * @author ino
 */
class SyscmContentsController extends SyscmAppController {
    public $components = array('Paginator');
    public $paginate = array(
        'limit' => 20,
        'order' => array(
            'ScmContentPublished.text' => 'desc'
        ),
        'group' => 'ScmContent.id'
    );
    public $uses = array(
        'Syscm.ScmContent',
        'Syscm.ScmContentContent',
        'Syscm.ScmContentPublished',
        'Syscm.ScmContentMeta',
        'Syscm.ScmCategory',
        'Syscm.ScmContentsCategory',
    );

    public function add() {
        $this->SysAcl->isAcc('add');
		$is_publisher = $this->SysAcl->isAcc('execute', 'sysadmin.publish_content.index','index', false, null);
		
        $this->helpers[] = 'Tinymce';
        /**
         * save
         */
        if ($this->request->is('post')) {
            $data                                = $this->request->data;
            //ScmContent
            $data['ScmContent']['id']            = CakeString::uuid();
            $data['ScmContent']['active_status'] = '1';
            $data['ScmContent']['menu_code']     = $this->ScmContent->menuCode;

            //ScmContentContent
            $data['ScmContentContent']['scm_content_id'] = $data['ScmContent']['id'];
            $data['ScmContentContent']['active_status']  = '1';
            $data['ScmContentContent']['field']          = $this->ScmContentContent->fieldKeyValue;

            //ScmContentPublished
            $data['ScmContentPublished']['scm_content_id'] = $data['ScmContent']['id'];
            $data['ScmContentPublished']['value']          = implode(':', $data['ScmContentPublished']['value']);
            $data['ScmContentPublished']['active_status']  = '1';
            $data['ScmContentPublished']['field']          = $this->ScmContentPublished->fieldKeyValue;

            //ScmContentMeta
            $data['ScmContentMeta']['scm_content_id'] = $data['ScmContent']['id'];
            $data['ScmContentMeta']['active_status']  = '1';
            $data['ScmContentMeta']['field']          = $this->ScmContentMeta->fieldKeyValue;

            # debug($data);die();
            if ($this->ScmContent->saveAssociated($data)) {
                $this->Flash->success(__d('syscm', 'Content has been saved.'));
                return $this->redirect(array('action' => 'edit', $data['ScmContent']['id']));
            } else {
                $this->Flash->error(__d('syscm', 'Failed to save the content. Please try again later'));
            }
        }
        /**
         * 
         */
        $published_options  = $this->ScmContentPublished->fieldMenuCodeOptions;
        $browser_menu_title = __d('syscm', 'Add Content');
        $scm_categories     = $this->ScmContent->getCategories();
        $parents            = $this->ScmContent->getParents();

        $enqueued_css    = array('Sysadmin.zebra_datepicker/zebra_datepicker-bootstrap');
        $enqueued_script = array('Sysadmin.zebra_datepicker');

        $this->set(compact('browser_menu_title', 'is_publisher', 'published_options', 'scm_categories', 'parents', 'enqueued_css', 'enqueued_script'));
    }

    public function delete($id) {
        $this->SysAcl->isAcc('delete');
        if (!$id) {
            $this->Flash->error(__d('syscm', 'Missing request parameter! Your requested page/action has been redirected.'));
        }

        if ($this->ScmContent->dropStatus($id)) {
            $this->Flash->success(__d('syscm', 'Content has been deleted.'));
        } else {
            $this->Flash->error(__d('syscm', 'Cannot delete content. Please try again alter'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function edit($id) {
        $this->SysAcl->isAcc('edit');
		$is_publisher = $this->SysAcl->isAcc('execute', 'sysadmin.publish_content.index','index', false, null);

        $this->helpers[] = 'Tinymce';
        if (!$id) {
            $this->Flash->error(__d('syscm', 'Missing request parameter! Your requested page/action has been redirected.'));
            return $this->redirect(array('action' => 'index'));
        }
        if (!$this->ScmContent->exists($id)) {
            $this->Flash->error(__d('syscm', 'Invalid request parameter! Your requested page/action has been redirected.'));
            return $this->redirect(array('action' => 'index'));
        }
        /**
         * save
         */
        if ($this->request->is(array('put', 'post'))) {
            $data                                 = $this->request->data;
            $data['ScmContentPublished']['value'] = implode(':', $data['ScmContentPublished']['value']);
            if ($this->ScmContent->saveAssociated($data)) {
                $this->Flash->success(__d('syscm', 'Content has been saved.'));
                return $this->redirect(array('action' => 'edit', $id));
            } else {
                $this->Flash->error(__d('syscm', 'Failed to save the content. Please try again later'));
            }
        }
        /**
         * 
         */
        $this->ScmContent->recursive = 1;
        $options                     = array(
            'conditions' => array(
                'ScmContent.id'            => $id,
                'ScmContent.active_status' => '1',
            )
        );
        $this->request->data         = $this->ScmContent->find('first', $options);

        $scm_categories     = $this->ScmContent->getCategories();
        $parents            = $this->ScmContent->getParents($id);
        $published_options  = $this->ScmContentPublished->fieldMenuCodeOptions;
        $browser_menu_title = __d('syscm', 'Edit Content');
        $enqueued_css       = array('Sysadmin.zebra_datepicker/zebra_datepicker-bootstrap');
        $enqueued_script    = array('Sysadmin.zebra_datepicker');
        $this->set(compact('browser_menu_title','is_publisher',  'published_options', 'scm_categories', 'parents', 'enqueued_css', 'enqueued_script'));

        $this->topMenuByModelSetup['invoke'] = $this->ScmContent->invokeTopMenuOnEdit($this->request->data);
    }

    public function index($id = null) {
        $this->SysAcl->isAcc();
        $gsparams = array(
            'ScmContent.text'          => __d('syscm', 'Title'),
            'ScmContent.name'          => __d('syscm', 'Slug'),
            'ParentContent.text'       => __d('syscm', 'Parent'),
            'ScmContentPublished.text' => __d('syscm', 'Post Date'),
            'ScmContentPublished.name' => __d('syscm', 'Post By'),
            'ScmContentPublished.menu_code' => __d('syscm', 'Status Publish'),
            'ScmCategory.text' => __d('syscm', 'Kategori'),
        );

        $this->_setGlobalSearch($gsparams);

        $this->set('browser_menu_title', __d('syscm', 'Content'));
        $conditions = array(
            'ScmContent.active_status' => '1',
            'ScmContent.menu_code'     => $this->ScmContent->menuCode,
        );
        if ($this->isGs) {
            $gs = $this->_getGlobalSearch();
            if($gs['global_search_field'] == 'ScmCategory.text'){
                
                $sikon = [
                    'ScmCategory.text LIKE' =>  "%" . $gs['global_search_keyword'] . '%'
                ];
                $this->ScmCategory->recursive = -1;
                $category = $this->ScmCategory->find('first',['conditions'=>$sikon]);
                // print_r($category);
                // die();
                if(!empty($category)){
                    $conditions['AND']['ScmContentsCategory.scm_category_id'] = $category['ScmCategory']['id'];
                }
                        
                unset($gs['global_search_field']);
                unset($gs['global_search_keyword']);
                unset($gs['isMulti']);
            }
            // print_r($gs);
            if ($gs['global_search_field']) {
                if ($gs['global_search_field'] == $this->allSearchKey) {
                    foreach ($gsparams as $i => $v) {
                        if($i!='ScmCategory.text'){
                        $conditions['AND']['OR'][$i . " LIKE "] = "%" . $gs['global_search_keyword'] . '%';
                            
                        }

                    }
                } else {
                    if($gs['global_search_field']!='ScmCategory.text'){
                        $conditions['AND'][$gs['global_search_field'] . " LIKE "] = "%" . $gs['global_search_keyword'] . '%';        
                    }else{

                    }
                    
                }
            }
        }
        try {
            // print_r($conditions);
            $this->Paginator->settings = $this->paginate;
            if(!isset($this->paginate['order'])){
                // die();
                // $this->paginate['order']  = 'DATE(ScmContentPublished.text) DESC';
            }
            $data_list = $this->Paginator->paginate('ScmContent', $conditions);
        } catch (NotFoundException $e) {
            $this->request->params['named']['page'] = 1;
            return $this->redirect(Router::reverse($this->request, true));
        }


        $this->set(compact('data_list'));
    }

}
