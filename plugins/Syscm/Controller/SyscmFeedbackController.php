<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('SyscmAppController', 'Syscm.Controller');

/**
 * CakePHP SyscmCategories
 * @author ino
 */
class SyscmFeedbackController  extends SyscmAppController 
{
    /**
     * 
     */
    public $uses = array('Syscm.ScmCustFeedbackView','Syscm.ScmCustFeedback','Syscm.ScmCustFeedbackRespon','Syscm.ScmCustFeedbackContact','Syscm.ScmCustFeedbackStatus');
	
	public $components = array('Sysadmin.SysAcl', 'Sysadmin.SysLog', 'Sysadmin.SysAdmUserRoles',
        'Session', 'Paginator', 'Flash', 'Syscm.PhpExcel');
		
    public $topMenuByModelSetup = array(
        'setTopMenu' => true,
        'model_alias' => "ScmCustFeedbackView",
        'set' => true,
        'invoke' => array()
    );

    public function add($id)
    {
        //add reply
        $this->SysAcl->isAcc('add');
        $data_row=array();
        $data_row = $this->ScmCustFeedback->findById($id, 'first');
        if(!$data_row){
            return $this->redirect(array('action'=> 'index'));
        }
        $this->set('browser_menu_title', __d('syscm', 'Add Respon :: Customer Feedback'));
        $this->set(compact('data_row'));
        $this->topMenuByModelSetup['model_alias'] = 'ScmCustFeedback';
        $topmenu = $this->ScmCustFeedback->invokeTopMenuOnDetail($this->request->params);
        $topmenu[1]['menu']['url'][] = $id;
        $topmenu[2]['menu']['url'][] = $id;
        $this->topMenuByModelSetup['invoke'] = $topmenu;
        
        if($this->request->is('post')){
            $data = $this->request->data;
            /**
             INSERT INTO `sys_scm_customers_details` (`id`, `name`, `scm_customer_id`, `field`, `value`, `text`, `annotation`, `active_status`, `menu_code`, `menu_order`, `created`, `modified`) VALUES
('56ae7d6e-a020-12e2-b33b-ef4dc0b9c32c',	'Admin',	'56ae7d6e-a020-4f42-b33b-ef4dc0b9c32c',	'ScmCustFeedbackRespon',	'Testing diterima. Trims',	NULL,	NULL,	1,	'127.0.0.1',	NULL,	'2016-01-31 15:35:58',	'2016-01-31 15:35:58');
 
             * 
             `id`	'56ae7d6e-a020-12e2-b33b-ef4dc0b9c32c'
 `name`	"	'Admin'"
 `scm_customer_id`	"	'56ae7d6e-a020-4f42-b33b-ef4dc0b9c32c'"
 `field`	"	'ScmCustFeedbackRespon'"
 `value`	"	'Testing diterima. Trims'"
 `text`	"	NULL"
 `annotation`	"	NULL"
 `active_status`	"	1"
 `menu_code`	"	'127.0.0.1'"
 `menu_order`	"	NULL"
 `created`	"	'2016-01-31 15:35:58'"
 `modified`	"	'2016-01-31 15:35:58'"

             */
            $usr = $this->SysAcl->getUserInfo($this->Session->read('AppUsrLog.uid'));
            
            $data['ScmCustFeedbackRespon']['id'] = CakeString::uuid();
            $data['ScmCustFeedbackRespon']['name'] = $usr['SysAclUser']['name'];
            //scm_customer_id : form provided
            $data['ScmCustFeedbackRespon']['field'] = $this->ScmCustFeedbackRespon->fieldKeyValue;
            //value : form provided
            $data['ScmCustFeedbackRespon']['active_status'] = 1;
            $data['ScmCustFeedbackRespon']['menu_code'] = $this->request->clientIp();
            if(!$this->ScmCustFeedbackRespon->save($data)){
               return $this->Flash->error(__d('syscm', 'Respon save has been failed. Please try again.'));
            }
            $this->Flash->success(__d('syscm', 'Respon has been saved.'));
                return $this->redirect(array('action' => 'edit', $data['ScmCustFeedbackRespon']['scm_customer_id'], $data['ScmCustFeedbackRespon']['id']));
        }
    }
    
    public function detail($id)
    {
        //list
        $this->SysAcl->isAcc();
        $data_row=array();
        $data_row = $this->ScmCustFeedback->findById($id, 'first');
        if(!$data_row){
            return $this->redirect(array('action'=> 'index'));
        }
        $this->set('browser_menu_title', __d('syscm', 'Customer Feedback Detail'));
        $this->Paginator->settings = $this->ScmCustFeedbackRespon->getFeedbackResponPaginateSettings();
        $conditions                = array(
            'AND' => array(
                'ScmCustFeedbackRespon.active_status' => '1',
                'ScmCustFeedbackRespon.field'     => $this->ScmCustFeedbackRespon->fieldKeyValue,
                'ScmCustFeedbackRespon.scm_customer_id'     => $id,
            )
        );
        try {
            $data_list = $this->Paginator->paginate('ScmCustFeedbackRespon', $conditions);
        } catch (NotFoundException $e) {
            $this->request->params['named']['page'] = 1;
            return $this->redirect(Router::reverse($this->request, true));
        }

        $this->set(compact('data_list', 'data_row'));
        
        $this->topMenuByModelSetup['model_alias'] = 'ScmCustFeedback';
        $topmenu = $this->ScmCustFeedback->invokeTopMenuOnDetail($this->request->params);
        $topmenu[1]['menu']['url'][] = $id;
        $topmenu[2]['menu']['url'][] = $id;
        $this->topMenuByModelSetup['invoke'] = $topmenu;
    }
    
    public function edit($id, $respon_id)
    {
        //edit reply
        $this->SysAcl->isAcc('add');
        $data_row=array();
        $data_row = $this->ScmCustFeedback->findById($id, 'first');
        if(!$data_row){
            return $this->redirect(array('action'=> 'index'));
        }
        $this->set('browser_menu_title', __d('syscm', 'Edit Respon :: Customer Feedback'));
        $this->topMenuByModelSetup['model_alias'] = 'ScmCustFeedback';
        $topmenu = $this->ScmCustFeedback->invokeTopMenuOnEdit($this->request->params);
        $topmenu[1]['menu']['url'][] = $id;
        $topmenu[2]['menu']['url'][] = $id;
        $data_row=array();
        $data_row = $this->ScmCustFeedback->findById($id, 'first');
        if(!$data_row){
            return $this->redirect(array('action'=> 'index'));
        }
        $this->set(compact('data_row'));
        
        $this->topMenuByModelSetup['invoke'] = $topmenu;
        $this->ScmCustFeedback->topMenuRevoke = array('add','edit', 'delete');
        
         if($this->request->is('post')){
            $data = $this->request->data;
            /**
             INSERT INTO `sys_scm_customers_details` (`id`, `name`, `scm_customer_id`, `field`, `value`, `text`, `annotation`, `active_status`, `menu_code`, `menu_order`, `created`, `modified`) VALUES
('56ae7d6e-a020-12e2-b33b-ef4dc0b9c32c',	'Admin',	'56ae7d6e-a020-4f42-b33b-ef4dc0b9c32c',	'ScmCustFeedbackRespon',	'Testing diterima. Trims',	NULL,	NULL,	1,	'127.0.0.1',	NULL,	'2016-01-31 15:35:58',	'2016-01-31 15:35:58');
 
             * 
             `id`	'56ae7d6e-a020-12e2-b33b-ef4dc0b9c32c'
 `name`	"	'Admin'"
 `scm_customer_id`	"	'56ae7d6e-a020-4f42-b33b-ef4dc0b9c32c'"
 `field`	"	'ScmCustFeedbackRespon'"
 `value`	"	'Testing diterima. Trims'"
 `text`	"	NULL"
 `annotation`	"	NULL"
 `active_status`	"	1"
 `menu_code`	"	'127.0.0.1'"
 `menu_order`	"	NULL"
 `created`	"	'2016-01-31 15:35:58'"
 `modified`	"	'2016-01-31 15:35:58'"

             */
            $usr = $this->SysAcl->getUserInfo($this->Session->read('AppUsrLog.uid'));
            
            //id : form provided
            $data['ScmCustFeedbackRespon']['text'] = $usr['SysAclUser']['name'];
            
            //value : form provided
            $data['ScmCustFeedbackRespon']['menu_code'] = $this->request->clientIp();
            if(!$this->ScmCustFeedbackRespon->save($data)){
               return $this->Flash->error(__d('syscm', 'Respon update has been failed. Please try again.'));
            }
            $this->Flash->success(__d('syscm', 'Respon has been updated.'));
                return $this->redirect(array('action' => 'edit', $id, $data['ScmCustFeedbackRespon']['id']));
        }
        $conditions                = array(
            'AND' => array(
                'ScmCustFeedbackRespon.active_status' => '1',
                'ScmCustFeedbackRespon.field'     => $this->ScmCustFeedbackRespon->fieldKeyValue,
                'ScmCustFeedbackRespon.id'     => $respon_id,
                'ScmCustFeedbackRespon.scm_customer_id'     => $id,
            )
        );
        
        $this->request->data = $this->ScmCustFeedbackRespon->find('first', compact('conditions'));
    }
    public function index()
    {
		
		$string  	= 'FeedbackFilter.%s.keyword';
		if ($this->request->is('post')) {

            $posted 	= $this->request->data;
			$action 	= $this->request->data('ScmCustFeedbackView.action');
            if (isset($posted['form_filter'])) {
                $posted = $posted['form_filter'];
            }
			
			if (isset($posted['do'])) {
                $this->Session->write(sprintf($string, "year"), $posted['year']);
                $this->Session->write(sprintf($string, "month"), $posted['month']);
                $this->Session->write(sprintf($string, "filtered"), 1);
            }
            if (isset($posted['clear'])) {
				$this->Session->write(sprintf($string, "year"), null);
                $this->Session->write(sprintf($string, "month"), null);
				$this->Session->delete(sprintf($string, "filtered"));
            }
		}
		
        $this->SysAcl->isAcc();
        $this->set('browser_menu_title', __d('syscm', 'Customer Feedback'));
        $this->Paginator->settings = $this->ScmCustFeedbackView->getFeedbackHistoryPaginateSettings();
		if(isset($action) && !empty($action)){
			$this->Paginator->settings["ScmCustFeedbackView"]["limit"]=false;
		}
        $conditions                = array(
            'AND' => array(
                'ScmCustFeedbackView.active_status' => '1',
                'ScmCustFeedbackView.menu_code'     => $this->ScmCustFeedbackView->menuCode,
            )
        );
		if ($this->Session->check(sprintf($string,"filtered"))) {
			$year = $this->Session->read(sprintf($string,"year"));
			$month = $this->Session->read(sprintf($string,"month"));
			if(!empty($year)){
				$conditions['AND']['YEAR(ScmCustFeedbackView.created)'] = $year;
				$form_filter["year_select"]=$year; 
			}
			if(!empty($month)){
				$conditions['AND']['MONTH(ScmCustFeedbackView.created)'] = $month;
				$form_filter["month_select"]=$month; 
			}
			
        }
		
        try {
			$this->ScmCustFeedbackView->recursive = -1;
            $data_list = $this->Paginator->paginate('ScmCustFeedbackView', $conditions);
			$form_filter["year"] 	= range(date("Y")-4,date("Y")+4);
			$form_filter["month"] 	= array("January","February","March","April","May","June","July","August","September","October","November","December");
            
        } catch (NotFoundException $e) {
            $this->request->params['named']['page'] = 1;
            return $this->redirect(Router::reverse($this->request, true));
        }
		//var_dump($data_list);
        $this->set(compact('data_list', 'respon_count','form_filter'));
		
		if ($this->request->is('post')) {
            $action = $this->request->data('ScmCustFeedbackView.action');
            if (!$action) {
                return;
            }
            $method = '_do_' . $action;
            if (!method_exists($this, $method)) {
                return;
            }
            return $this->{$method}($data_list);
        }
        
    }
	
	protected function _do_export_xls($data) {
		//init
        $usr = $this->SysAcl->getUserInfo($this->Session->read('AppUsrLog.uid'));

        $init_options = array(
            'currentUser' => $usr['SysAclUser']['name'],
            'worksheets'  => array(
                __d('syscm', 'Customer Registration List'),
            )
        );
        $this->PhpExcel->initWriterExcel($init_options);
        //.header
        //      write values

        $cell_options = array(
            'activeSheetIndex' => 0,
            'cells'            => array(
                'A1' => __d('syscm', 'No'),
                'B1' => __d('syscm', 'NoSL'),
                'C1' => __d('syscm', 'Name'),
                'D1' => __d('syscm', 'Feedback Content'),
                'E1' => __d('syscm', 'Cellular Number'),
                'F1' => __d('syscm', 'Email'),
                'G1' => __d('syscm', 'Phone'),
                'H1' => __d('syscm', 'Post Date'),
                'I1' => __d('syscm', 'Client IP')
            ),
        );

        // Nama 	Alamat 	Email 	Telp 	Cellular 	IP Klien
        $this->PhpExcel->setValuesExcel($cell_options);


        //styling
        $styleArray                                   = array(
            'borders' => array(
                'allborders' => array(
                    'style' => $this->PhpExcel->getDefined('PHPExcel_Style_Border', 'BORDER_THIN'),
                )
            ),
            'fill'    => array(
                'type'  => $this->PhpExcel->getDefined('PHPExcel_Style_Fill', 'FILL_SOLID'),
                'color' => array('rgb' => 'FCF8E3'),
            ),
            'font'    => array(
                'bold' => true,
            )
        );
        $style_options                                = array(
            'activeSheetIndex' => 0,
            'range'            => 'A1:I1',
            'style'            => $styleArray,
        );
        $this->PhpExcel->applyStyleFromArray($style_options);
        //.header
        //data
        
        $cell_options                                 = array(
            'activeSheetIndex' => 0,
                /* 'cells'            => array(
                  'A1' => __d('syscm', 'Registration ID'),
                  'B1' => __d('syscm', 'Status'),
                  'C1' => __d('syscm', 'Name'),
                  'D1' => __d('syscm', 'Address'),
                  'E1' => __d('syscm', 'Cellular Number'),
                  'F1' => __d('syscm', 'Phone Number'),
                  'G1' => __d('syscm', 'Email'),
                  'H1' => __d('syscm', 'Registration Date'),
                  'I1' => __d('syscm', 'Last Update'),
                  'J1' => __d('syscm', 'Client IP'),
                  ), */
        );
        if (!$data || empty($data)) {
            return $this->Flash->error(__d('syscm', 'Empty row. Please try again!'));
        }
		
        $row = 2;
        $no  = 1;

        foreach ($data as $key => $val) {
            $cell_options['cells']['A' . $row] = $no;
            $cell_options['cells']['B' . $row] = h($val['ScmCustFeedbackView']['text']);
            $cell_options['cells']['C' . $row] = h($val['ScmCustFeedbackView']['name']);
            $cell_options['cells']['D' . $row] = h($val['ScmCustFeedbackView']['annotation']);
            $cell_options['cells']['E' . $row] = h($val['ScmCustFeedbackView']['mobile']);
            $cell_options['cells']['F' . $row] = h($val['ScmCustFeedbackView']['email']);
            $cell_options['cells']['G' . $row] = strval(h($val['ScmCustFeedbackView']['phone']));
            $cell_options['cells']['H' . $row] = h($val['ScmCustFeedbackView']['created']);
            $cell_options['cells']['I' . $row] = h($val['ScmCustFeedbackView']['ip_address']);
            
            $row++;
            $no++;
        }
        

        // Nama 	Alamat 	Email 	Telp 	Cellular 	IP Klien
        $this->PhpExcel->setValuesExcel($cell_options);
        //.data
        
//      set auto size columns
        $autosize_options = array(
            'activeSheetIndex' => 0,
            'columns'          => range('A', 'I'),
        );
        $this->PhpExcel->setAutoSize($autosize_options);
        
        //styling
        $styleArray                                   = array(
            'borders' => array(
                'allborders' => array(
                    'style' => $this->PhpExcel->getDefined('PHPExcel_Style_Border', 'BORDER_THIN'),
                )
            ),
            
        );
        $style_options                                = array(
            'activeSheetIndex' => 0,
            'range'            => 'A2:I'.($row-1),
            'style'            => $styleArray,
        );
        $this->PhpExcel->applyStyleFromArray($style_options);
		//$this->PhpExcel->getActiveSheet(0)->getStyle('A2:I'.($row-1).')->setQuotePrefix(true);
        //output
        $time         = time();
        $datetime     = date('Y-m-d_H-i', $time);
        $save_options = array(
            'filename' => __d('syscm', 'Customer_Feedback_' . $datetime . '_' . $time),
        );
        $this->PhpExcel->saveExcel($save_options);
	}

}
