<?php

/*
 * The MIT License
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


App::uses('SyscmAppController', 'Syscm.Controller');

/**
 * CakePHP SyscmCustController
 * @author ino
 */
class SyscmCustController extends SyscmAppController
{

    public $uses       = array(
        'Syscm.ScmCustInfo',
        'Syscm.ScmCustInfoDetail',
        );
    public $components = array('Sysadmin.SysAcl', 'Sysadmin.SysLog', 'Sysadmin.SysAdmUserRoles',
        'Session', 'Paginator', 'Flash', 'Syscm.PhpExcel');

    public function add()
    {
        $this->SysAcl->isAcc('add');
        if($this->request->is(array('post','put'))){
            $data = $this->request->data;
            $id = CakeString::uuid();
            $data['ScmCustInfo']['id'] = $id;
            $data['ScmCustInfo']['active_status'] = 1;
            $data['ScmCustInfo']['menu_code'] = $this->ScmCustInfo->listMenuCode;
            $data['ScmCustInfoDetail']['scm_customer_id'] = $id;
            $data['ScmCustInfoDetail']['active_status'] = 1;
            $data['ScmCustInfoDetail']['field'] = $this->ScmCustInfoDetail->fieldKeyValue;
            if($this->ScmCustInfo->saveAssociated($data)){
                $this->Flash->success(__d('syscm', 'Customer has been added!'));
                return $this->redirect(array('action'=>'edit',$id));
            }else{
                $this->Flash->error(__d('syscm', 'Customer add failed. Please try again!'));
            }
        }
        $browser_menu_title = __d('syscm', 'Add Customer');
        $this->set(compact('browser_menu_title'));
    }

    /**
     * 
     */
    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->topMenuByModelSetup['invoke'] = $this->ScmCustInfo->invokeTopMenu($this->request->params);
    }

    public function customer_list_template($id = null)
    {
        //init
        $usr = $this->SysAcl->getUserInfo($this->Session->read('AppUsrLog.uid'));

        $init_options = array(
            'currentUser' => $usr['SysAclUser']['name'],
            'worksheets'  => array(
                __d('syscm', 'rek_info'),
            )
        );
        $this->PhpExcel->initWriterExcel($init_options);

//      write values
        $cell_options = array(
            'activeSheetIndex' => 0,
            'cells'            => array(
                'A1' => __d('syscm', 'NoSL'),
                'B1' => __d('syscm', 'Name'),
                'C1' => __d('syscm', 'Address'),
                'D1' => __d('syscm', 'Gol'),
                'E1' => __d('syscm', 'Wil'),
            ),
        );

        $this->PhpExcel->setValuesExcel($cell_options);

//      set auto size columns
        $autosize_options = array(
            'activeSheetIndex' => 0,
            'columns'          => range('A', 'E'),
        );
        $this->PhpExcel->setAutoSize($autosize_options);

        //styling
        $styleArray    = array(
            'borders' => array(
                'allborders' => array(
                    'style' => $this->PhpExcel->getDefined('PHPExcel_Style_Border',
                            'BORDER_THIN'),
                )
            ),
            'fill'    => array(
                'type'  => $this->PhpExcel->getDefined('PHPExcel_Style_Fill',
                        'FILL_SOLID'),
                'color' => array('rgb' => 'FCF8E3'),
            ),
            'font'    => array(
                'bold' => true,
            )
        );
        $style_options = array(
            'activeSheetIndex' => 0,
            'range'            => 'A1:E1',
            'style'            => $styleArray,
        );
        $this->PhpExcel->applyStyleFromArray($style_options);

        //output
        $save_options = array(
            'filename' => __d('syscm', 'Customer List Template')
        );
        $this->PhpExcel->saveExcel($save_options);

        // $objExcel->initWriterExcel($init_options);
    }

    public function delete ($id){
        $this->SysAcl->isAcc('delete');
        if (!$id) {
            $this->Flash->error(__d('syscm',
                            'Missing request parameter! Your requested page/action has been redirected.'));
            
        }
        $cust = $this->ScmCustInfo->findCLById($id);
        if(!$cust){
            $this->Flash->error(__d('syscm', 'Invalid NoSL requested!'));
            
        }
        if(!$this->ScmCustInfo->dropStatus($id)){
            $this->Flash->error(__d('syscm', 'Customer delete failed!'));
            return $this->redirect(array('action'=>'index'));
        }else{
            $this->Flash->success(__d('syscm', 'Customer has been deleted!'));
        }
        if(!isset($cust['ScmCustInfoDetail'])){
            return $this->redirect(array('action'=>'index'));
        }
        if(!isset($cust['ScmCustInfoDetail']['id'])){
            return $this->redirect(array('action'=>'index'));
        }
        $this->ScmCustInfoDetail->dropStatus($cust['ScmCustInfoDetail']['id']);
        return $this->redirect(array('action'=>'index'));
        
    }
    public function edit($id = null){
        
        $this->SysAcl->isAcc('edit');
        if(!$id){
            $this->Flash->error(__d('syscm', 'Missing request parameter'));
            return $this->redirect(array('action'=> 'index'));
        }
        
        $cust = $this->ScmCustInfo->findCLById($id);
        if(!$cust){
            $this->Flash->error(__d('syscm', 'Invalid NoSL requested!'));
            return $this->redirect(array('action'=> 'index'));
        }
        if($this->request->is(array('post','put'))){
            $data = $this->request->data;
            $data['ScmCustInfoDetail']['scm_customer_id'] = $id;
            $data['ScmCustInfoDetail']['active_status'] = 1;
            $data['ScmCustInfoDetail']['field'] = 'ScmCustInfoDetail';
            if($this->ScmCustInfo->saveAssociated($data)){
                $this->Flash->success(__d('syscm', 'Customer has been updated!'));
                return $this->redirect(array($id));
            }else{
                $this->Flash->error(__d('syscm', 'Customer update failed. Please try again!'));
            }
        }
        $this->request->data         = $cust;
        
        $browser_menu_title = __d('syscm', 'Edit Customer');
        $this->set(compact('browser_menu_title'));
    }

    public function index($id = null)
    {
        $this->SysAcl->isAcc();
        $this->set('browser_menu_title', __d('syscm', 'Customer List'));
        $this->Paginator->settings = $this->ScmCustInfo->getIndexPaginationSetting();
        
        //
        $gsparams = array(
            'ScmCustInfo.text' => __d('syscm', 'No.SL'),
            'ScmCustInfo.name' => __d('syscm', 'Name'),
            'ScmCustInfo.annotation' => __d('syscm', 'Address'),
        );

        $this->_setGlobalSearch($gsparams);


        $this->Paginator->settings = $this->ScmCustInfo->getIndexPaginationSetting();
        $conditions                = array(
            'AND' => array(
                'ScmCustInfo.active_status' => '1',
                'ScmCustInfo.menu_code'     => $this->ScmCustInfo->listMenuCode,
            )
        );

        if ($this->isGs) {
            $gs = $this->_getGlobalSearch();
            if ($gs['global_search_field']) {
                if ($gs['global_search_field'] == $this->allSearchKey) {
                    foreach ($gsparams as $i => $v) {
                        
                            $conditions['AND']['OR'][$i . " LIKE "] = "%" . $gs['global_search_keyword'] . '%';
                        
                        
                    }
                }
                else {
                    
                            $conditions['AND'][$gs['global_search_field'] . " LIKE "] = "%" . $gs['global_search_keyword'] . '%';
                    
                    
                }
            }
        }
        //
        try {
            $this->ScmCustInfo->recursive = -1;
            $data_list = $this->Paginator->paginate('ScmCustInfo', $conditions);
        }
        catch (NotFoundException $e) {
            $this->request->params['named']['page'] = 1;
            return $this->redirect(Router::reverse($this->request, true));
        }
        
        $cust_ids = array();
        foreach($data_list as $key => $val){
            $cust_ids[] = $val['ScmCustInfo']['id'];
        }
        $options = array('conditions'=>array('AND'=>array('ScmCustInfoDetail.active_status'=> 1)));
        $options['conditions']['AND']['ScmCustInfoDetail.scm_customer_id'] = $cust_ids;
        $this->ScmCustInfoDetail->recursive = -1;
        $rs  = $this->ScmCustInfoDetail->find('all', $options);
        
        $data_detail  = array();
        if($rs){
            foreach($rs as $key=>$val){
                $data_detail[$val['ScmCustInfoDetail']['scm_customer_id']]['name'] = $val['ScmCustInfoDetail']['name'];
                $data_detail[$val['ScmCustInfoDetail']['scm_customer_id']]['text'] = $val['ScmCustInfoDetail']['text'];
            }
        }
        
        $this->set(compact('data_list', 'data_detail'));
    }

    public function upload_cl($id = null)
    {
        $enqueued_css    = array('Sysadmin.zebra_datepicker/zebra_datepicker-bootstrap');
        $enqueued_script = array('Sysadmin.zebra_datepicker', 'Sysadmin.bootstrap.file-input');
        $this->set(compact('browser_menu_title', 'enqueued_script',
                        'enqueued_css', 'published_options'));

        if ($this->request->is('post')) {
            $data          = $this->request->data;
            $uploaded_path = isset($data['ScmCustInfo']['the_file_path']) ? $data['ScmCustInfo']['the_file_path']
                        : false;
            $confirmed     = $uploaded_path ? true : false;
            if (!$uploaded_path) {
                $the_file = $data['ScmCustInfo']['the_file'];
                #unset($data['ScmCustInfo']['the_file']);
                if (!$the_file) {
                    $this->Flash->error(__d('syscm', 'Please upload the file'));
                    return false;
                }

                //set true to return the file instance
                $file = $this->ScmCustInfo->validateExcelFile($the_file, true);

                if (!$file) {
                    $this->Flash->error(__d('syscm',
                                    'Please upload a valid excel file.'));
                    return false;
                }
                //generate filename
                $dest          = WWW_ROOT . 'files' . DS . CakeString::uuid();
                $file->copy($dest);
                $uploaded_path = $dest;
                $this->Flash->success(__d('syscm',
                                'Please confirm to save the customer list.'));
                //clone
            }

            if (!file_exists($uploaded_path)) {
                $this->Flash->error(__d('syscm',
                                'File does not exists. Please try again.'));
                return false;
            }

            $this->PhpExcel->loadExcel($uploaded_path);
            $sheetData   = $this->PhpExcel->getSheetData(0, null, false, false,
                    true);
            $save_status = array();
            if ($confirmed) {

                $file          = new File($uploaded_path);
                $file->delete();
                $uploaded_path = null;
                $save_list     = $this->ScmCustInfo->saveCL($sheetData);
                if ($save_list) {
                    extract($save_list);
                    $save_error = isset($save_error) ? $save_error : false;
                    if ($save_error) {
                        $this->Flash->error($save_error);
                    }

                    $save_success = isset($save_success) ? $save_success : false;
                    if ($save_success) {
                        $this->Flash->success($save_success);
                    }
                    $sheetData     = array();
                    $uploaded_path = null;
                }
            }
            $this->set(compact('sheetData', 'uploaded_path', 'save_status'));
        }
    }

}
