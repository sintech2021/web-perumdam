<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('SyscmAppController', 'Syscm.Controller');

/**
 * CakePHP SyscmMenusController
 * @author ino
 */
class SyscmMenusController extends SyscmAppController {

    public $scmConf             = array(
        'MenuGroupName' => 'menu_group',
        'MenuListName'  => 'menu_list',
    );
    public $uses                = array(
        'Syscm.ScmMenuGroup',
        'Syscm.ScmMenuList',
    );
    public $topMenuByModelSetup = array(
        'setTopMenu'  => false,
        'model_alias' => "SyscmAppModel",
        'set'         => true,
        'invoke'      => array()
    );

    /**
     * Set up the top menu for each rendered page
     * @visibility protected
     * @param void
     * @return void
     */
    protected function _setTopMenu($set = TRUE, $invoke = array()) {
        $top_menu = $this->ScmMenuGroup->SetTopMenu($this->request->params, $this->SysAclView, $invoke);
        if ($set) {
            $this->set(compact('top_menu'));
        } else {
            return $top_menu;
        }
    }

    public function add() {
        $this->SysAcl->isAcc('add');
        $this->set('browser_menu_title', __d('syscm', 'Add menu group'));

        /**
         * do save
         */
        if ($this->request->is('post')) {
            $data                                  = $this->request->data;
            $data['ScmMenuGroup']['menu_code']     = $this->scmConf['MenuGroupName'];
            $data['ScmMenuGroup']['id']            = CakeString::uuid();
            $data['ScmMenuGroup']['active_status'] = '1';
            if ($this->ScmMenuGroup->save($data)) {
                $this->Flash->success(__d('syscm', 'Menu group has been added.'));
                return $this->redirect(array('action' => 'edit', $data['ScmMenuGroup']['id']));
            } else {
                $this->Flash->error(__d('syscm', 'Menu group has not been added. Please try again'));
            }
        }
        /**
         * ./do save
         */
    }

    /**
     * Delete Menu group
     */
    public function delete($id = null) {
        $this->SysAcl->isAcc('delete');
        if (!$id) {
            return $this->redirect(array('action' => 'index'));
        }

        if (!$this->ScmMenuGroup->exists($id)) {
            return $this->redirect(array('action' => 'index'));
        }

        if ($this->ScmMenuGroup->dropStatus($id)) {
            $this->Flash->success(__d('syscm', 'Menu group has been deleted.'));
        } else {
            $this->Flash->error(__d('syscm', 'Menu group has not been deleted. Please try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    /**
     * Edit Menu group
     */
    public function edit($id) {
        /**
         * Edit Menu group
         */
        $this->SysAcl->isAcc('edit');
        if (!$id) {
            return $this->redirect(array('action' => 'index'));
        }

        if (!$this->ScmMenuGroup->exists($id)) {
            return $this->redirect(array('action' => 'index'));
        }

        /**
         * update
         */
        if ($this->request->is('put')) {
            if ($this->ScmMenuGroup->save($this->request->data)) {
                $this->Flash->success(__d('syscm', 'Menu group has been updated.'));
                return $this->redirect(array($id));
            } else {
                $this->Flash->error(__d('syscm', 'Menu group has not been updated. Please try again.'));
            }
        }

        /**
         * ./update
         */
        $invoke              = $this->ScmMenuGroup->getInvokeTopMenuEdit($id);
        $this->_setTopMenu(true, $invoke);
        $this->request->data = $this->ScmMenuGroup->findFirstById($id);
    }

    /**
     * 
     */
    public function index() {
        $this->SysAcl->isAcc();

        $this->set('browser_menu_title', __d('syscm', 'Menu Group'));

        $conditions                    = array(
            'AND' => array(
                'ScmMenuGroup.active_status' => '1',
                'ScmMenuGroup.menu_code'     => $this->scmConf['MenuGroupName'],
            )
        );
        $this->ScmMenuGroup->recursive = 1;
        $scm_menu_groups               = $this->Paginator->paginate('ScmMenuGroup', $conditions);

        $this->set(compact('scm_menu_groups'));
    }

    /**
     * 
     */
    public function menu_add() {
        $this->SysAcl->isAcc('add');
        $this->helpers[] = 'Tinymce';
        $this->set('browser_menu_title', __d('syscm', 'Add new menu'));

        /**
         * save
         */
        if ($this->request->is('post')) {
            $data                                 = $this->request->data;
            $data['ScmMenuList']['id']            = CakeString::uuid();
            $data['ScmMenuList']['menu_code']     = $this->scmConf['MenuListName'];
            $data['ScmMenuList']['active_status'] = '1';
            if (!$data['ScmMenuList']['parent_id']) {
                $data['ScmMenuList']['parent_id'] = null;
            }
            $data['ScmMenuSlug']['id']            = CakeString::uuid();
            $data['ScmMenuSlug']['scm_menu_id']   = $data['ScmMenuList']['id'];
            $data['ScmMenuSlug']['field']         = 'menu_slug';
            $data['ScmMenuSlug']['menu_code']     = 'menu_slug';
            $data['ScmMenuSlug']['active_status'] = '1';

            #debug($data);die();
            if ($this->ScmMenuList->saveAssociated($data)) {

                $this->Flash->success(__d('syscm', 'Menu has been saved.'));
                return $this->redirect(array('action' => 'menu_edit', $data['ScmMenuList']['id']));
            } else {
                $this->Flash->success(__d('syscm', 'Menu has not been saved. Please try again.'));
            }
        }
        /**
         * ./save
         */
        $parents         = $this->ScmMenuList->findActiveList(null, true);
        $scm_menu_groups = $this->ScmMenuGroup->findActiveList();
        $this->set(compact('scm_menu_groups', 'parents'));
    }

    /**
     * Delete Menu List
     */
    public function menu_delete($id = null) {
        $this->SysAcl->isAcc('delete');
        if (!$id) {
            return $this->redirect(array('action' => 'menu_list'));
        }

        if (!$this->ScmMenuList->exists($id)) {
            return $this->redirect(array('action' => 'menu_list'));
        }

        if ($this->ScmMenuList->dropStatus($id)) {
            $this->Flash->success(__d('syscm', 'Menu has been deleted.'));
        } else {
            $this->Flash->error(__d('syscm', 'Menu has not been deleted. Please try again.'));
        }
        return $this->redirect(array('action' => 'menu_list'));
    }

    /**
     * 
     */
    public function menu_edit($id) {
        $this->SysAcl->isAcc('edit');
        $this->set('browser_menu_title', __d('sysadmin', 'Menu Edit'));

        if (!$id) {
            return $this->redirect(array('action' => 'menu_list'));
        }

        if (!$this->ScmMenuList->exists($id)) {
            return $this->redirect(array('action' => 'menu_list'));
        }
        /**
         * update
         */
        if ($this->request->is(array('put', 'post'))) {
            $data                                 = $this->request->data;
            $data['ScmMenuList']['menu_code']     = $this->scmConf['MenuListName'];
            $data['ScmMenuList']['active_status'] = '1';
            if (!$data['ScmMenuList']['parent_id']) {
                $data['ScmMenuList']['parent_id'] = null;
            }

            $data['ScmMenuSlug']['field']         = 'menu_slug';
            $data['ScmMenuSlug']['menu_code']     = 'menu_slug';
            $data['ScmMenuSlug']['active_status'] = '1';
            if ($this->ScmMenuList->saveAssociated($data)) {


                $this->Flash->success(__d('syscm', 'Menu has been saved.'));
                return $this->redirect(array('action' => 'menu_edit', $data['ScmMenuList']['id']));
            } else {
                $this->Flash->success(__d('syscm', 'Menu has not been saved. Please try again.'));
            }
        }
        /**
         * ./update
         */
        $this->request->data = $this->ScmMenuList->findFirstById($id);
        $invoke              = $this->ScmMenuList->getInvokeTopMenuEdit($id);
        $this->_setTopMenu(true, $invoke);
        $parents             = $this->ScmMenuList->findActiveList($id, true);
        $scm_menu_groups     = $this->ScmMenuGroup->findActiveList();
        $this->set(compact('scm_menu_groups', 'parents'));
    }

    /**
     * 
     */
    public function menu_list() {
        $this->SysAcl->isAcc('browse');
        $gsparams = array(
            'ScmMenuList.name'       => __d('syscm', 'Menu ID'),
            'ScmMenuSlug.value'      => __d('syscm', 'Slug'),
            'ScmMenuList.text'       => __d('syscm', 'Text'),
            'ParentMenu.text'        => __d('syscm', 'Parent'),
           // 'ScmMenuGroup.text'      => __d('syscm', 'Groups'),
            'ScmMenuList.annotation' => __d('syscm', 'Annotation'),
        );

        $this->_setGlobalSearch($gsparams);
        $this->set('browser_menu_title', __d('sysadmin', 'Menu List'));
        $conditions = array(
            'AND' => array(
                'ScmMenuList.active_status' => '1',
                'ScmMenuList.menu_code'     => $this->scmConf['MenuListName'],
            )
        );
        if ($this->isGs) {
            $gs = $this->_getGlobalSearch();
            if ($gs['global_search_field']) {
                if ($gs['global_search_field'] == $this->allSearchKey) {
                    foreach ($gsparams as $i => $v) {

                        $conditions['AND']['OR'][$i . " LIKE "] = "%" . $gs['global_search_keyword'] . '%';
                    }
                } else {

                    $conditions['AND'][$gs['global_search_field'] . " LIKE "] = "%" . $gs['global_search_keyword'] . '%';
                }
            }
        }
        try {
            $data_list = $this->Paginator->paginate('ScmMenuList', $conditions);
        } catch (NotFoundException $e) {
            $this->request->params['named']['page'] = 1;
            return $this->redirect(Router::reverse($this->request, true));
        }


        $menu_id_list = $this->ScmMenuList->getTreeByRs($data_list);


        $this->set(compact('data_list', 'menu_id_list'));
    }

}
