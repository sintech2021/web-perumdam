<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppController', 'Controller');

/**
 * CakePHP SyscmAppController
 * @author ino
 */
class SyscmAppController extends AppController
{

    public $layout     = "Sysadmin.layout_bootstrap";
    public $components = array('Sysadmin.SysAcl', 'Sysadmin.SysLog', 'Sysadmin.SysAdmUserRoles',
        'Session', 'Paginator', 'Flash');
    public $helpers    = array('Sysadmin.SysHtml');
    public $topMenuByModelSetup = array(
        'setTopMenu' => true,
        'model_alias' => "SyscmAppModel",
        'set' => true,
        'invoke' => array()
    );
    public $allSearchKey = "--all--";
    public $isGs         = false;
    public $isMulti      = false;

    /**
     * Set up the global search form
     * @visibility protected
     * @param array
     * @return void
     */
    protected function _getGlobalSearch($params = array())
    {
        $global_search = array(
            'plugin'     => $this->request->params['plugin'],
            'controller' => $this->request->params['controller'],
            'action'     => $this->request->params['action'],
        );

        $result      = array('global_search_field' => '', 'global_search_keyword' => '');
        $stringfield = 'GlobalSearch.%s.%s.%s.field';
		$multifield  = 'GlobalSearch.%s.%s.%s.isMulti';
		
		if ($this->Session->check(sprintf($multifield,
                                $global_search['plugin'],
                                $global_search['controller'],
                                $global_search['action']))) {
            $result['isMulti'] = $this->Session->read(sprintf($multifield,
                            $global_search['plugin'],
                            $global_search['controller'],
                            $global_search['action']));
        }
		
		
		if(!isset($result['isMulti']) || !$result['isMulti']){
			if ($this->Session->check(sprintf($stringfield,
									$global_search['plugin'],
									$global_search['controller'],
									$global_search['action']))) {
				$result['global_search_field'] = $this->Session->read(sprintf($stringfield,
								$global_search['plugin'],
								$global_search['controller'],
								$global_search['action']));
			}

			$string = 'GlobalSearch.%s.%s.%s.keyword';
			if ($this->Session->check(sprintf($string, $global_search['plugin'],
									$global_search['controller'],
									$global_search['action']))) {
				$result['global_search_keyword'] = $this->Session->read(sprintf($string,
								$global_search['plugin'],
								$global_search['controller'],
								$global_search['action']));
			}
		}else if($result['isMulti']){
			$string 		= 'GlobalSearch.%s.%s.%s.%s.keyword';
			$stringfield 	= 'GlobalSearch.%s.%s.%s.%s.field';
			foreach($params as $key=>$val){
				if ($this->Session->check(sprintf($string, $global_search['plugin'],
									$global_search['controller'],
									$global_search['action'],$key))) {
				$result['global_search_keyword'][$key] = $this->Session->read(sprintf($string,
								$global_search['plugin'],
								$global_search['controller'],
								$global_search['action'],$key));
				}
				
				if ($this->Session->check(sprintf($stringfield,
									$global_search['plugin'],
									$global_search['controller'],
									$global_search['action'],$key))) {
				$result['global_search_field'][$key] = $this->Session->read(sprintf($stringfield,
								$global_search['plugin'],
								$global_search['controller'],
								$global_search['action'],$key));
				}
			}
		}
        return $result;
    }

    /**
     * Set up the global search form
     * @visibility protected
     * @param array
     * @return void
     */
    protected function _setGlobalSearch($params = array())
    {
        $this->isGs   	= true;
		$this->isMulti 	= false;
        $allSearchKey 	= array($this->allSearchKey => __d('sysadmin', 'All'));
        
		$stringMulti 	= 'GlobalSearch.%s.%s.%s.isMulti';

        $global_search = array(
            'plugin'     => $this->request->params['plugin'],
            'controller' => $this->request->params['controller'],
            'action'     => $this->request->params['action'],
        );
        $field         = null;
        $stringfield   = 'GlobalSearch.%s.%s.%s.field';
        if ($this->Session->check(sprintf($stringfield,
                                $global_search['plugin'],
                                $global_search['controller'],
                                $global_search['action']))) {
            $field = $this->Session->read(sprintf($stringfield,
                            $global_search['plugin'],
                            $global_search['controller'],
                            $global_search['action']));
        }

        $keyword = null;
        $string  = 'GlobalSearch.%s.%s.%s.keyword';
		
		
        if ($this->Session->check(sprintf($string, $global_search['plugin'],
                                $global_search['controller'],
                                $global_search['action']))) {
            $keyword = $this->Session->read(sprintf($string,
                            $global_search['plugin'],
                            $global_search['controller'],
                            $global_search['action']));
        }
        
        if ($this->request->is('post')) {

            $posted = $this->request->data;
         
            if (isset($posted['global_search'])) {
                $posted = $posted['global_search'];
            }
			
			if(isset($posted["field"]) && is_array($posted["field"])){
				$this->isMulti = true;
				$stringfield = 'GlobalSearch.%s.%s.%s.%s.field';
				$string		 = 'GlobalSearch.%s.%s.%s.%s.keyword';
			}
			
			if(!isset($posted["field"]) || !is_array($posted["field"])){
				if (isset($posted['do'])) {
					$this->Session->write(sprintf($string, $global_search['plugin'],
									$global_search['controller'],
									$global_search['action']), $posted['keyword']);
					$this->Session->write(sprintf($stringfield,
									$global_search['plugin'],
									$global_search['controller'],
									$global_search['action']), $posted['field']);
					
					$this->Session->write(sprintf($stringMulti,
									$global_search['plugin'],
									$global_search['controller'],
									$global_search['action']), $this->isMulti);
									
				}
				if (isset($posted['clear'])) {
					$this->Session->write(sprintf($string, $global_search['plugin'],
									$global_search['controller'],
									$global_search['action']), null);
					$this->Session->write(sprintf($stringfield,
									$global_search['plugin'],
									$global_search['controller'],
									$global_search['action']), null);
					
					$this->Session->write(sprintf($stringMulti,
									$global_search['plugin'],
									$global_search['controller'],
									$global_search['action']), null);
				}
				
				$keyword = $this->Session->read(sprintf($string,
                            $global_search['plugin'],
                            $global_search['controller'],
                            $global_search['action']));
				$field   = $this->Session->read(sprintf($stringfield,
								$global_search['plugin'],
								$global_search['controller'],
								$global_search['action']));
				
			}else{
				if (isset($posted['do'])) {
					foreach($posted['field'] as $k=>$v){
							$this->Session->write(sprintf($string, $global_search['plugin'],
											$global_search['controller'],
											$global_search['action'],$k), $k);
							$this->Session->write(sprintf($stringfield,
											$global_search['plugin'],
											$global_search['controller'],
											$global_search['action'],$k), $v);
					}
					
					$this->Session->write(sprintf($stringMulti,
									$global_search['plugin'],
									$global_search['controller'],
									$global_search['action']), $this->isMulti);				
				}
				
				if (isset($posted['clear'])) {
					foreach($posted['field'] as $k=>$v){
						$this->Session->write(sprintf($string, $global_search['plugin'],
										$global_search['controller'],
										$global_search['action'],$k), null);
						$this->Session->write(sprintf($stringfield,
										$global_search['plugin'],
										$global_search['controller'],
										$global_search['action'],$k), null);
					}
					
					$this->Session->write(sprintf($stringMulti,
									$global_search['plugin'],
									$global_search['controller'],
									$global_search['action']), null);
				}
				
				foreach($params as $key=>$val){
					$keyword[$key] = $this->Session->read(sprintf($string,
								$global_search['plugin'],
								$global_search['controller'],
								$global_search['action'],$key));
					$field[$key]   = $this->Session->read(sprintf($stringfield,
									$global_search['plugin'],
									$global_search['controller'],
									$global_search['action'],$key));
				}
				
				
			}
        }else{
			
			if($this->Session->read(sprintf($stringMulti,
                            $global_search['plugin'],
                            $global_search['controller'],
                            $global_search['action']))){
								$stringfield = 'GlobalSearch.%s.%s.%s.%s.field';
								$string		 = 'GlobalSearch.%s.%s.%s.%s.keyword';
				foreach($params as $key=>$val){
					$keyword[$key] = $this->Session->read(sprintf($string,
								$global_search['plugin'],
								$global_search['controller'],
								$global_search['action'],$key));
					$field[$key]   = $this->Session->read(sprintf($stringfield,
									$global_search['plugin'],
									$global_search['controller'],
									$global_search['action'],$key));
				}
								
			}
			
		}

        $global_search['allSearchKey'] = $allSearchKey;
        $global_search['params']       = $params;
        $global_search['field']        = $field;
        $global_search['keyword']      = $keyword;
        $global_search['isMulti']      = $this->Session->read(sprintf($stringMulti,
                            $global_search['plugin'],
                            $global_search['controller'],
                            $global_search['action']));
        $this->set(compact('global_search'));
    }

    /**
     * Set up the top menu for each rendered page
     * @visibility protected
     * @param void
     * @return void
     */
    protected function _setTopMenuByModel($model_alias, $set = TRUE,
            $invoke = array())
    {
        if(!$model_alias){
            return true;
        }
        
        if(!$this->loadModel('Syscm.' . $model_alias)){
            return true;
        }
        if(!$this->{$model_alias}->hasMethod('setTopMenu')){
            return true;
        }
        $top_menu = $this->{$model_alias}->setTopMenu($this->request->params,
                $this->SysAclView, $invoke);
        if ($set) {
            $this->set(compact('top_menu'));
            $this->set('noPageTitle', true);
        }
        else {
            return $top_menu;
        }
    }

    public function afterFilter()
    {
        parent::afterFilter();
        $this->SysLog->writeSysLog();
    }

    public function beforeFilter()
    {
        parent::beforeFilter();

        $this->setAclForView($this->request->controller);
        $appMenu     = $this->SysAcl->getAppMenu();
        $profileMenu = $this->SysAcl->getProfileMenu();
        $this->set(compact('appMenu', 'profileMenu'));
        #$this->SysAcl->isAcc();
        if (method_exists($this, '_setTopMenu')) {
            $this->_setTopMenu();
            $this->set('noPageTitle', true);
        }
    }

    /**
     * before render
     */
    public function beforeRender()
    {
        parent::beforeRender();
        extract($this->topMenuByModelSetup);
        if($setTopMenu){
            $this->_setTopMenuByModel($model_alias, $set, $invoke);
        }
    }
    /**
     * 
     * @param string $controller
     * @param string $action
     * @param string $uid
     */
    public function setAclForView($controller, $action = 'index', $uid = null,
            $plugin = 'sysadmin')
    {
        $this->SysAclView = $this->SysAcl->getAclForView($controller, $action,
                $uid, $plugin);
        $this->set('SysAcl', $this->SysAclView);
    }

}
