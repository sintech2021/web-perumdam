<?php
require_once  APP .'/webroot/xcrud/xcrud.php';
App::uses('SyscmAppController', 'Syscm.Controller');

/**
 * 
 */
class SyscmGaleryController extends SyscmAppController  
{
	
	public function index($value='')
	{
		$browser_menu_title = __d('syscm', 'Galery');
		$sub_title = 'Galery Images & Videos';
        $this->set(compact('browser_menu_title','sub_title'));

		$this->SysAcl->isAcc();
		$xcrud = Xcrud::get_instance();

		$xcrud->table('sys_galeries');
		$xcrud->columns(['title','keterangan','tipe']);

		$xcrud->unset_title();
		$xcrud->show_primary_ai_column(true);

		$xcrud->fields(['title','keterangan','tipe']);
		$xcrud->button(BASE_URL.'/syscm/syscm_galery/browse/{tipe}/{id}/{title}');
		
		$xcrud->pass_var('tgl_buat', date('Y-m-d H:i:s'), 'create');
		$xcrud->pass_var('id', 'UUID()', 'create');
		$xcrud->pass_var('tgl_edit', date('Y-m-d H:i:s'), 'create');
		$xcrud->pass_var('tgl_edit', date('Y-m-d H:i:s'), 'edit');
		$xcrud->unset_print();
		$xcrud->unset_csv();
		$xcrud->unset_search();
		$xcrud->change_type('tipe','select','0',['0'=>'Image','1'=>'Video']);
		$xcrud->before_insert('create_uuid'); 
		
		$this->set('xcrud',$xcrud->render()) ;


	}
	public function browse($tipe='',$id="",$title="")
	{
		$this->SysAcl->isAcc();
		$browser_menu_title = __d('syscm', 'Galery '.($tipe==0?'Images':'Videos'));
		$sub_title = $title;
        $this->set(compact('browser_menu_title','sub_title'));
		$xcrud = Xcrud::get_instance();

		$xcrud->table('sys_galeries_items');
		$xcrud->columns(['path','title','keterangan']);
		$xcrud->where('tipe',$tipe);
		$xcrud->where('galery_id',$id);

		$xcrud->unset_title();
		$xcrud->show_primary_ai_column(true);
		$xcrud->unset_print();
		$xcrud->unset_csv();
		$xcrud->unset_search();
		$xcrud->fields(['title','keterangan','path']);

		$xcrud->label('path','Url');
		
		$xcrud->pass_var('tgl_buat', date('Y-m-d H:i:s'), 'create');
		$xcrud->pass_var('tipe', $tipe, 'create');
		$xcrud->pass_var('galery_id', $id, 'create');
		$xcrud->pass_var('tgl_edit', date('Y-m-d H:i:s'), 'create');
		$xcrud->pass_var('tgl_edit', date('Y-m-d H:i:s'), 'edit');
		$xcrud->before_insert('create_uuid'); 
		if($tipe==0){
			$xcrud->label('path','File');

			$xcrud->change_type('path','image','',array('path'=>'../uploads/galery_images','url'=>BASE_URL.'/uploads/galery_images')); // crop it as you want
		}
		// $xcrud->button(BASE_URL.'/syscm/syscm_galery/browse/{tipe}/{id}');
		$this->set('xcrud',$xcrud->render()) ;
	}
	

}