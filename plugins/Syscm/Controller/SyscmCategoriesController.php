<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('SyscmAppController', 'Syscm.Controller');

/**
 * CakePHP SyscmCategories
 * @author ino
 */
class SyscmCategoriesController  extends SyscmAppController 
{
    /**
     * 
     * @param type $id
     */
    public $uses = array(
        'Syscm.ScmCategory',
    );

    public function index($id=null)
    {
        $this->SysAcl->isAcc();
        
        $data_list = $this->Paginator->paginate('ScmCategory', $this->ScmCategory->getPaginateConditions());
        
        $browser_menu_title = __d('syscm', 'Category');
        $this->set(compact('browser_menu_title','data_list'));
    }
    
    /**
     * 
     * @return type
     */
    public function add()
    {
        $this->SysAcl->isAcc('add');
        /**
         * save
         */
        if ($this->request->is('post')) {
            $data                                = $this->request->data;
            //ScmCategory
            $data['ScmCategory']['id']            = CakeString::uuid();
            $data['ScmCategory']['active_status'] = '1';
            $data['ScmCategory']['menu_code']     = $this->ScmCategory->menuCodeContentCategory;
            if ($this->ScmCategory->save($data)) {
                $this->Flash->success(__d('syscm', 'Category has been saved.'));
                return $this->redirect(array('action' => 'edit', $data['ScmCategory']['id']));
            }
            else {
                $this->Flash->error(__d('syscm',
                                'Failed to save the category. Please try again later'));
            }
        }
        /**
         * 
         */
        $browser_menu_title = __d('syscm', 'Add Category');
        $this->set(compact('browser_menu_title'));
        
    }

    /**
     * 
     * @return type
     */
    public function delete($id)
    {
        $this->SysAcl->isAcc('delete');
        if (!$id) {
            $this->Flash->error(__d('syscm',
                            'Missing request parameter! Your requested page/action has been redirected.'));
        }
        
        if($this->ScmCategory->dropStatus($id)){
            $this->Flash->success(__d('syscm',
                            'Category has been deleted.'));
        }else{
            $this->Flash->error(__d('syscm',
                            'Cannot delete Category. Please try again alter'));
        }

        return $this->redirect(array('action' => 'index'));
    }
    
    /**
     * 
     * @param type $id
     * @return type
     */
    public function edit($id)
    {
        $this->SysAcl->isAcc('edit');
        if (!$id) {
            $this->Flash->error(__d('syscm',
                            'Missing request parameter! Your requested page/action has been redirected.'));
            return $this->redirect(array('action' => 'index'));
        }
        if (!$this->ScmCategory->exists($id)) {
            $this->Flash->error(__d('syscm',
                            'Invalid request parameter! Your requested page/action has been redirected.'));
            return $this->redirect(array('action' => 'index'));
        }
        /**
         * save
         */
        if ($this->request->is('post')) {
            $data = $this->request->data;
            if ($this->ScmCategory->save($data)) {
                $this->Flash->success(__d('syscm', 'Content has been saved.'));
                return $this->redirect(array('action' => 'edit', $id));
            }
            else {
                $this->Flash->error(__d('syscm',
                                'Failed to save the content. Please try again later'));
            }
        }
        /**
         * 
         */
        $options                     = array(
            'conditions' => array(
                'ScmCategory.id'            => $id,
                'ScmCategory.active_status' => '1',
                'ScmCategory.menu_code' => $this->ScmCategory->menuCodeContentCategory,
            )
        );
        $this->request->data         = $this->ScmCategory->find('first', $options);
        
        $browser_menu_title = __d('syscm', 'Edit Content');
        $this->set(compact('browser_menu_title'));
    }
}
