<?php

/*
 * The MIT License
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


App::uses('SyscmAppController', 'Syscm.Controller');

/**
 * CakePHP SyscmCustController
 * @author ino
 */
class SyscmCustRegController extends SyscmAppController {

    public $uses       = array(
        'Syscm.ScmCustReg',
        'Syscm.ScmCustRegDetail',
    );
    public $components = array('Sysadmin.SysAcl', 'Sysadmin.SysLog', 'Sysadmin.SysAdmUserRoles',
        'Session', 'Paginator', 'Flash', 'Syscm.PhpExcel');

    protected function _do_export_xls() {
        $data       = $this->request->data;
        $checked_id = isset($data['ScmCustReg']['checked_id']) ? $data['ScmCustReg']['checked_id'] : array();
        if (!$checked_id) {
            return $this->Flash->error(__d('syscm', 'Select at least 1 row, you have select none. Please try again!'));
        }

        //init
        $usr = $this->SysAcl->getUserInfo($this->Session->read('AppUsrLog.uid'));

        $init_options = array(
            'currentUser' => $usr['SysAclUser']['name'],
            'worksheets'  => array(
                __d('syscm', 'Customer Registration List'),
            )
        );
        $this->PhpExcel->initWriterExcel($init_options);
        //.header
        //      write values

        $cell_options = array(
            'activeSheetIndex' => 0,
            'cells'            => array(
                'A1' => __d('syscm', 'No'),
                'B1' => __d('syscm', 'Registration ID'),
                'C1' => __d('syscm', 'Status'),
                'D1' => __d('syscm', 'Name'),
                'E1' => __d('syscm', 'Address'),
                'F1' => __d('syscm', 'Cellular Number'),
                'G1' => __d('syscm', 'Phone Number'),
                'H1' => __d('syscm', 'Email'),
                'I1' => __d('syscm', 'Registration Date'),
                'J1' => __d('syscm', 'Last Update'),
                'K1' => __d('syscm', 'Client IP'),
            ),
        );

        // Nama 	Alamat 	Email 	Telp 	Cellular 	IP Klien
        $this->PhpExcel->setValuesExcel($cell_options);


        //styling
        $styleArray                                   = array(
            'borders' => array(
                'allborders' => array(
                    'style' => $this->PhpExcel->getDefined('PHPExcel_Style_Border', 'BORDER_THIN'),
                )
            ),
            'fill'    => array(
                'type'  => $this->PhpExcel->getDefined('PHPExcel_Style_Fill', 'FILL_SOLID'),
                'color' => array('rgb' => 'FCF8E3'),
            ),
            'font'    => array(
                'bold' => true,
            )
        );
        $style_options                                = array(
            'activeSheetIndex' => 0,
            'range'            => 'A1:K1',
            'style'            => $styleArray,
        );
        $this->PhpExcel->applyStyleFromArray($style_options);
        //.header
        //data
        
        $cell_options                                 = array(
            'activeSheetIndex' => 0,
                /* 'cells'            => array(
                  'A1' => __d('syscm', 'Registration ID'),
                  'B1' => __d('syscm', 'Status'),
                  'C1' => __d('syscm', 'Name'),
                  'D1' => __d('syscm', 'Address'),
                  'E1' => __d('syscm', 'Cellular Number'),
                  'F1' => __d('syscm', 'Phone Number'),
                  'G1' => __d('syscm', 'Email'),
                  'H1' => __d('syscm', 'Registration Date'),
                  'I1' => __d('syscm', 'Last Update'),
                  'J1' => __d('syscm', 'Client IP'),
                  ), */
        );
        $conditions                                    = array('AND' => array());
        $conditions['AND']['ScmCustReg.id']            = $checked_id;
        $conditions['AND']['ScmCustReg.active_status'] = '1';
        $conditions['AND']['ScmCustReg.menu_code']     = $this->ScmCustReg->regMenuCode;
        $order                                        = array('ScmCustReg.text' => 'desc');
        $data_list                                    = $this->ScmCustReg->find('all', compact('conditions', 'order'));
        if (!$data_list) {
            return $this->Flash->error(__d('syscm', 'Empty row. Please try again!'));
        }
        $row = 2;
        $no  = 1;

        foreach ($data_list as $key => $val) {
            $cell_options['cells']['A' . $row] = $no;
            $cell_options['cells']['B' . $row] = h($val['ScmCustReg']['text']);
            $cell_options['cells']['C' . $row] = h($val['ScmCustReg']['menu_order']);
            $cell_options['cells']['D' . $row] = h($val['ScmCustReg']['name']);
            $cell_options['cells']['E' . $row] = strip_tags($val['ScmCustRegDetail']['value']);
            $cell_options['cells']['F' . $row] = h($val['ScmCustRegDetail']['text']);
            $cell_options['cells']['G' . $row] = h($val['ScmCustRegDetail']['annotation']);
            $cell_options['cells']['H' . $row] = h($val['ScmCustRegDetail']['name']);
            $cell_options['cells']['I' . $row] = h($val['ScmCustReg']['created']);
            $cell_options['cells']['J' . $row] = h($val['ScmCustReg']['modified']);
            $cell_options['cells']['K' . $row] = h($val['ScmCustRegDetail']['menu_code']);
            
            $row++;
            $no++;
        }
        

        // Nama 	Alamat 	Email 	Telp 	Cellular 	IP Klien
        $this->PhpExcel->setValuesExcel($cell_options);
        //.data
        
//      set auto size columns
        $autosize_options = array(
            'activeSheetIndex' => 0,
            'columns'          => range('A', 'K'),
        );
        $this->PhpExcel->setAutoSize($autosize_options);
        
        //styling
        $styleArray                                   = array(
            'borders' => array(
                'allborders' => array(
                    'style' => $this->PhpExcel->getDefined('PHPExcel_Style_Border', 'BORDER_THIN'),
                )
            ),
            
        );
        $style_options                                = array(
            'activeSheetIndex' => 0,
            'range'            => 'A2:K'.($row-1),
            'style'            => $styleArray,
        );
        $this->PhpExcel->applyStyleFromArray($style_options);
        //output
        $time         = time();
        $datetime     = date('Y-m-d_H-i', $time);
        $save_options = array(
            'filename' => __d('syscm', 'Customer_Registration_' . $datetime . '_' . $time),
        );
        $this->PhpExcel->saveExcel($save_options);
    }

    protected function _do_mark_delete() {
        $data       = $this->request->data;
        $checked_id = isset($data['ScmCustReg']['checked_id']) ? $data['ScmCustReg']['checked_id'] : array();
        if (!$checked_id) {
            return $this->Flash->error(__d('syscm', 'Select at least 1 row, you have select none. Please try again!'));
        }
        $set    = array('ScmCustReg.active_status' => 0, 'ScmCustReg.modified' => date("'Y-m-d H:i:s'"));
        $where  = array('ScmCustReg.id' => $checked_id);
        $update = $this->ScmCustReg->updateAll($set, $where);
        if (!$update) {
            return $this->Flash->error(__d('syscm', 'An error happened during update. Please try again!'));
        }
        $this->Flash->success(count($checked_id) . ' ' . __d('syscm', 'record(s) has been deleted!'));
        return $this->redirect(Router::reverse($this->request, true));
    }

    protected function _do_mark_done() {
        $data       = $this->request->data;
        $checked_id = isset($data['ScmCustReg']['checked_id']) ? $data['ScmCustReg']['checked_id'] : array();
        if (!$checked_id) {
            return $this->Flash->error(__d('syscm', 'Select at least 1 row, you have select none. Please try again!'));
        }
        $set    = array('ScmCustReg.menu_order' => 1, 'ScmCustReg.modified' => date("'Y-m-d H:i:s'"));
        $where  = array('ScmCustReg.id' => $checked_id);
        $update = $this->ScmCustReg->updateAll($set, $where);
        if (!$update) {
            return $this->Flash->error(__d('syscm', 'An error happened during update. Please try again!'));
        }
        $this->Flash->success(count($checked_id) . ' ' . __d('syscm', 'record(s) has been marked as done!'));
        return $this->redirect(Router::reverse($this->request, true));
    }

    protected function _do_mark_undone() {
        $data       = $this->request->data;
        $checked_id = isset($data['ScmCustReg']['checked_id']) ? $data['ScmCustReg']['checked_id'] : array();
        if (!$checked_id) {
            return $this->Flash->error(__d('syscm', 'Select at least 1 row, you have select none. Please try again!'));
        }
        $set    = array('ScmCustReg.menu_order' => null, 'ScmCustReg.modified' => date("'Y-m-d H:i:s'"));
        $where  = array('ScmCustReg.id' => $checked_id);
        $update = $this->ScmCustReg->updateAll($set, $where);
        if (!$update) {
            return $this->Flash->error(__d('syscm', 'An error happened during update. Please try again!'));
        }
        $this->Flash->success(count($checked_id) . ' ' . __d('syscm', 'record(s) has been marked as undone!'));
        return $this->redirect(Router::reverse($this->request, true));
    }

    public function add() {
        $this->SysAcl->isAcc('add');
        if ($this->request->is(array('post', 'put'))) {
            $data                                        = $this->request->data;
            $id                                          = CakeString::uuid();
            $data['ScmCustReg']['id']                    = $id;
            $data['ScmCustReg']['active_status']         = 1;
            $data['ScmCustReg']['menu_code']             = $this->ScmCustInfo->listMenuCode;
            $data['ScmCustRegDetail']['scm_customer_id'] = $id;
            $data['ScmCustRegDetail']['active_status']   = 1;
            $data['ScmCustRegDetail']['field']           = $this->ScmCustInfoDetail->fieldKeyValue;
            if ($this->ScmCustReg->saveAssociated($data)) {
                $this->Flash->success(__d('syscm', 'Customer has been added!'));
                return $this->redirect(array('action' => 'edit', $id));
            } else {
                $this->Flash->error(__d('syscm', 'Customer add failed. Please try again!'));
            }
        }
        $browser_menu_title = __d('syscm', 'Add Customer');
        $this->set(compact('browser_menu_title'));
    }

    /**
     * 
     */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->topMenuByModelSetup['model_alias'] = 'ScmCustReg';
        #$this->topMenuByModelSetup['invoke'] = $this->ScmCustReg->invokeTopMenu($this->request->params);
    }

    public function customer_list_template($id = null) {
        //init
        $usr = $this->SysAcl->getUserInfo($this->Session->read('AppUsrLog.uid'));

        $init_options = array(
            'currentUser' => $usr['SysAclUser']['name'],
            'worksheets'  => array(
                __d('syscm', 'rek_info'),
            )
        );
        $this->PhpExcel->initWriterExcel($init_options);

//      write values
        $cell_options = array(
            'activeSheetIndex' => 0,
            'cells'            => array(
                'A1' => __d('syscm', 'NoSL'),
                'B1' => __d('syscm', 'Name'),
                'C1' => __d('syscm', 'Address'),
                'D1' => __d('syscm', 'Gol'),
                'E1' => __d('syscm', 'Wil'),
            ),
        );

        $this->PhpExcel->setValuesExcel($cell_options);

//      set auto size columns
        $autosize_options = array(
            'activeSheetIndex' => 0,
            'columns'          => range('A', 'E'),
        );
        $this->PhpExcel->setAutoSize($autosize_options);

        //styling
        $styleArray    = array(
            'borders' => array(
                'allborders' => array(
                    'style' => $this->PhpExcel->getDefined('PHPExcel_Style_Border', 'BORDER_THIN'),
                )
            ),
            'fill'    => array(
                'type'  => $this->PhpExcel->getDefined('PHPExcel_Style_Fill', 'FILL_SOLID'),
                'color' => array('rgb' => 'FCF8E3'),
            ),
            'font'    => array(
                'bold' => true,
            )
        );
        $style_options = array(
            'activeSheetIndex' => 0,
            'range'            => 'A1:E1',
            'style'            => $styleArray,
        );
        $this->PhpExcel->applyStyleFromArray($style_options);

        //output
        $save_options = array(
            'filename' => __d('syscm', 'Customer List Template')
        );
        $this->PhpExcel->saveExcel($save_options);

        // $objExcel->initWriterExcel($init_options);
    }

    public function delete($id) {
        $this->SysAcl->isAcc('delete');
        if (!$id) {
            $this->Flash->error(__d('syscm', 'Missing request parameter! Your requested page/action has been redirected.'));
        }
        $cust = $this->ScmCustInfo->findCLById($id);
        if (!$cust) {
            $this->Flash->error(__d('syscm', 'Invalid NoSL requested!'));
        }
        if (!$this->ScmCustInfo->dropStatus($id)) {
            $this->Flash->error(__d('syscm', 'Customer delete failed!'));
            return $this->redirect(array('action' => 'index'));
        } else {
            $this->Flash->success(__d('syscm', 'Customer has been deleted!'));
        }
        if (!isset($cust['ScmCustInfoDetail'])) {
            return $this->redirect(array('action' => 'index'));
        }
        if (!isset($cust['ScmCustInfoDetail']['id'])) {
            return $this->redirect(array('action' => 'index'));
        }
        $this->ScmCustInfoDetail->dropStatus($cust['ScmCustInfoDetail']['id']);
        return $this->redirect(array('action' => 'index'));
    }

    public function edit($id = null) {

        $this->SysAcl->isAcc('edit');
        if (!$id) {
            $this->Flash->error(__d('syscm', 'Missing request parameter'));
            return $this->redirect(array('action' => 'index'));
        }

        $cust = $this->ScmCustInfo->findCLById($id);
        if (!$cust) {
            $this->Flash->error(__d('syscm', 'Invalid NoSL requested!'));
            return $this->redirect(array('action' => 'index'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $data                                         = $this->request->data;
            $data['ScmCustInfoDetail']['scm_customer_id'] = $id;
            $data['ScmCustInfoDetail']['active_status']   = 1;
            $data['ScmCustInfoDetail']['field']           = 'ScmCustInfoDetail';
            if ($this->ScmCustInfo->saveAssociated($data)) {
                $this->Flash->success(__d('syscm', 'Customer has been updated!'));
                return $this->redirect(array($id));
            } else {
                $this->Flash->error(__d('syscm', 'Customer update failed. Please try again!'));
            }
        }
        $this->request->data = $cust;

        $browser_menu_title = __d('syscm', 'Edit Customer');
        $this->set(compact('browser_menu_title'));
    }

    public function index($id = null) {
        $this->SysAcl->isAcc();
        $this->set('browser_menu_title', __d('syscm', 'Registrant List'));
        $this->Paginator->settings = $this->ScmCustReg->getIndexPaginationSetting();
        $conditions                = array(
            'AND' => array(
                'ScmCustReg.active_status' => '1',
                'ScmCustReg.menu_code'     => $this->ScmCustReg->regMenuCode,
            )
        );
        $gsparams = array(
            'ScmCustReg.text' => __d('syscm', 'Registration ID'),
            'ScmCustReg.menu_order' => __d('syscm', 'Status'),
            'ScmCustReg.name' => __d('syscm', 'Name'),
            'ScmCustRegDetail.value' => __d('syscm', 'Address'),
            
            'ScmCustRegDetail.name' => __d('syscm', 'Email'),
            'ScmCustRegDetail.annotation' => __d('syscm', 'Phone Number'),
            'ScmCustRegDetail.text' => __d('syscm', 'Cellular Number'),
            'ScmCustReg.created' => __d('syscm', 'Registration Date'),
            'ScmCustRegDetail.menu_code' => __d('syscm', 'Client IP'),
        );

        $this->_setGlobalSearch($gsparams);
        if ($this->isGs) {
            $gs = $this->_getGlobalSearch();
            if ($gs['global_search_field']) {
                if ($gs['global_search_field'] == $this->allSearchKey) {
                    foreach ($gsparams as $i => $v) {
                        
                            $conditions['AND']['OR'][$i . " LIKE "] = "%" . $gs['global_search_keyword'] . '%';
                        
                        
                    }
                }
                else {
                    
                            $conditions['AND'][$gs['global_search_field'] . " LIKE "] = "%" . $gs['global_search_keyword'] . '%';
                    
                    
                }
            }
        }
        try {
            $this->ScmCustReg->recursive = 0;
            $data_list = $this->Paginator->paginate('ScmCustReg', $conditions);
        } catch (NotFoundException $e) {
            $this->request->params['named']['page'] = 1;
            return $this->redirect(Router::reverse($this->request, true));
        }

        //if($data_list){
            //foreach($data_list as $key=>$value){
            
            //}
        //}
        $this->set(compact('data_list'));
        //
        if ($this->request->is('post')) {
            $action = $this->request->data('ScmCustReg.action');
            if (!$action) {
                return;
            }
            $method = '_do_' . $action;
            if (!method_exists($this, $method)) {
                return;
            }
            return $this->{$method}();
        }
    }

    public function upload_cl($id = null) {
        $enqueued_css    = array('Sysadmin.zebra_datepicker/zebra_datepicker-bootstrap');
        $enqueued_script = array('Sysadmin.zebra_datepicker', 'Sysadmin.bootstrap.file-input');
        $this->set(compact('browser_menu_title', 'enqueued_script', 'enqueued_css', 'published_options'));

        if ($this->request->is('post')) {
            $data          = $this->request->data;
            $uploaded_path = isset($data['ScmCustInfo']['the_file_path']) ? $data['ScmCustInfo']['the_file_path'] : false;
            $confirmed     = $uploaded_path ? true : false;
            if (!$uploaded_path) {
                $the_file = $data['ScmCustInfo']['the_file'];
                #unset($data['ScmCustInfo']['the_file']);
                if (!$the_file) {
                    $this->Flash->error(__d('syscm', 'Please upload the file'));
                    return false;
                }

                //set true to return the file instance
                $file = $this->ScmCustInfo->validateExcelFile($the_file, true);

                if (!$file) {
                    $this->Flash->error(__d('syscm', 'Please upload a valid excel file.'));
                    return false;
                }
                //generate filename
                $dest          = WWW_ROOT . 'files' . DS . CakeString::uuid();
                $file->copy($dest);
                $uploaded_path = $dest;
                $this->Flash->success(__d('syscm', 'Please confirm to save the customer list.'));
                //clone
            }

            if (!file_exists($uploaded_path)) {
                $this->Flash->error(__d('syscm', 'File does not exists. Please try again.'));
                return false;
            }

            $this->PhpExcel->loadExcel($uploaded_path);
            $sheetData   = $this->PhpExcel->getSheetData(0, null, false, false, true);
            $save_status = array();
            if ($confirmed) {

                $file          = new File($uploaded_path);
                $file->delete();
                $uploaded_path = null;
                $save_list     = $this->ScmCustInfo->saveCL($sheetData);
                if ($save_list) {
                    extract($save_list);
                    $save_error = isset($save_error) ? $save_error : false;
                    if ($save_error) {
                        $this->Flash->error($save_error);
                    }

                    $save_success = isset($save_success) ? $save_success : false;
                    if ($save_success) {
                        $this->Flash->success($save_success);
                    }
                    $sheetData     = array();
                    $uploaded_path = null;
                }
            }
            $this->set(compact('sheetData', 'uploaded_path', 'save_status'));
        }
    }

}
