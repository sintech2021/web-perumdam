<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('SyscmAppController', 'Syscm.Controller');

/**
 * CakePHP SyscmCategories
 * @author ino
 */
class SyscmMediaController extends SyscmAppController
{

    /**
     * 
     */
    public $uses = array(
        'Syscm.ScmContentMedia',
        'Syscm.ScmContentMediaGroup',
        'Syscm.ScmContentPublished',
    );

    /**
     * 
     * @return boolean
     */
    public function add()
    {
        $this->SysAcl->isAcc('add');
        $published_options  = $this->ScmContentPublished->fieldMenuCodeOptions;
        $browser_menu_title = __d('syscm', 'Add Media');
        $enqueued_css       = array('Sysadmin.zebra_datepicker/zebra_datepicker-bootstrap');
        $enqueued_script    = array('Sysadmin.zebra_datepicker', 'Sysadmin.bootstrap.file-input');
        $this->set(compact('browser_menu_title', 'enqueued_script',
                        'enqueued_css', 'published_options'));

        if ($this->request->is('post')) {
            $data     = $this->request->data;
            $the_file = $data['ScmContentMedia']['the_file'];
            unset($data['ScmContentMedia']['the_file']);
            if (!$the_file) {
                $this->Flash->error(__d('syscm', 'Please upload the file'));
                return false;
            }

            if (!$this->ScmContentMedia->validateImageFile($the_file)) {
                $this->Flash->error(__d('syscm',
                                'Please upload a valid image file.'));
                return false;
            }

            $id                                       = CakeString::uuid();
            $data['ScmContentMedia']['id']            = $id;
            $data['ScmContentMedia']['active_status'] = '1';
            $data['ScmContentMedia']['menu_code']     = $this->ScmContentMedia->menuCode;

            $data['ScmContentPublished']['scm_content_id'] = $id;
            $data['ScmContentPublished']['active_status']  = '1';
            $data['ScmContentPublished']['field']          = 'ScmContentPublished';

            $data['ScmContentContent']['scm_content_id'] = $id;
            $data['ScmContentContent']['active_status']  = '1';
            $data['ScmContentContent']['field']          = 'ScmContentMedia';
            $data['ScmContentContent']['value']          = $this->ScmContentMedia->saveImageFile($the_file);
            if (!$data['ScmContentContent']['value']) {
                $this->Flash->error(__d('syscm', 'Error uploading the file.'));
                return false;
            }

            if (!$this->ScmContentMedia->saveAssociated($data)) {
                $this->Flash->error(__d('syscm',
                                'Media save error. Please try again.'));
                return false;
            }
            else {
                $this->Flash->success(__d('syscm', 'Media has been saved.'));
                return $this->redirect(array('action' => 'edit', $id));
            }
        }
    }

    /**
     * 
     * @param type $id
     */
    public function album_add($id = null)
    {
        $this->SysAcl->isAcc('add');
        /**
         * save
         */
        if ($this->request->is('post')) {
            $data                                          = $this->request->data;
            //ScmCategory
            $data['ScmContentMediaGroup']['id']            = CakeString::uuid();
            $data['ScmContentMediaGroup']['active_status'] = '1';
            $data['ScmContentMediaGroup']['menu_code']     = $this->ScmContentMediaGroup->menuCode;
            if ($this->ScmContentMediaGroup->save($data)) {
                $this->Flash->success(__d('syscm', 'Album has been saved.'));
                return $this->redirect(array('action' => 'album_edit', $data['ScmContentMediaGroup']['id']));
            }
            else {
                $this->Flash->error(__d('syscm',
                                'Failed to save the album. Please try again later'));
            }
        }
        /**
         * 
         */
        $browser_menu_title = __d('syscm', 'Add Album');
        $this->set(compact('browser_menu_title'));
    }

    /**
     * 
     * @return type
     */
    public function album_delete($id)
    {
        $this->SysAcl->isAcc('delete');
        if (!$id) {
            $this->Flash->error(__d('syscm',
                            'Missing request parameter! Your requested page/action has been redirected.'));
        }

        if ($this->ScmContentMediaGroup->dropStatus($id)) {
            $this->Flash->success(__d('syscm', 'Category has been deleted.'));
        }
        else {
            $this->Flash->error(__d('syscm',
                            'Cannot delete album. Please try again alter'));
        }

        return $this->redirect(array('action' => 'album_index'));
    }

    /**
     * 
     * @param type $id
     */
    public function album_edit($id = null)
    {
        $this->SysAcl->isAcc('edit');
        if (!$id) {
            $this->Flash->error(__d('syscm',
                            'Missing request parameter! Your requested page/action has been redirected.'));
            return $this->redirect(array('action' => 'album_index'));
        }

        if (!$this->ScmContentMediaGroup->exists($id)) {
            $this->Flash->error(__d('syscm',
                            'Invalid request parameter! Your requested page/action has been redirected.'));
            return $this->redirect(array('action' => 'album_index'));
        }
        /**
         * save
         */
        if ($this->request->is(array('post', 'put'))) {
            $data = $this->request->data;

            if ($this->ScmContentMediaGroup->save($data)) {
                $this->Flash->success(__d('syscm', 'Album has been saved.'));
                return $this->redirect(array('action' => 'album_edit', $data['ScmContentMediaGroup']['id']));
            }
            else {
                $this->Flash->error(__d('syscm',
                                'Failed to save the album. Please try again later'));
            }
        }

        $this->request->data = $this->ScmContentMediaGroup->findById($id);
        /**
         * 
         */
        $browser_menu_title  = __d('syscm', 'Edit Album');
        $this->set(compact('browser_menu_title'));
    }

    /**
     * 
     * @param type $id
     */
    public function album_index($id = null)
    {
        $this->SysAcl->isAcc('browse');
        $browser_menu_title = __d('syscm', 'Album List');

        try {
            $data_list = $this->Paginator->paginate('ScmContentMediaGroup',
                    $this->ScmContentMediaGroup->getPaginateConditions());
        }
        catch (NotFoundException $e) {
            $this->request->params['named']['page'] = 1;
            return $this->redirect(Router::reverse($this->request, true));
        }
        $this->set(compact('browser_menu_title', 'data_list'));
    }

    /**
     * 
     */
    public function album_list($id = null)
    {
        $this->SysAcl->isAcc();
        if (!$id) {
            $this->Flash->error(__d('syscm',
                            'Missing request parameter! Your requested page/action has been redirected.'));
            return $this->redirect(array('action' => 'album_index'));
        }

        if (!$this->ScmContentMediaGroup->exists($id)) {
            $this->Flash->error(__d('syscm',
                            'Invalid request parameter! Your requested page/action has been redirected.'));
            return $this->redirect(array('action' => 'album_index'));
        }

        $this->helpers[] = 'ZeroClipboard';

        $album = $this->ScmContentMediaGroup->findById($id);

        $browser_menu_title = __d('syscm', 'Media List of Album: %s (%s)',
                $album['ScmContentMediaGroup']['text'],
                $album['ScmContentMediaGroup']['name']);

        try {
            $data_list = $this->Paginator->paginate('ScmContentMedia',
                    $this->ScmContentMedia->getPaginateConditionsByAlbum($album));
        }
        catch (NotFoundException $e) {
            $this->request->params['named']['page'] = 1;
            return $this->redirect(Router::reverse($this->request, true));
        }

        $this->set(compact('browser_menu_title', 'data_list', 'album'));
    }

    /**
     * 
     */
    public function album_list_add($id = null)
    {
        $this->SysAcl->isAcc();
        if (!$id) {
            $this->Flash->error(__d('syscm',
                            'Missing request parameter! Your requested page/action has been redirected.'));
            return $this->redirect(array('action' => 'album_index'));
        }

        if (!$this->ScmContentMediaGroup->exists($id)) {
            $this->Flash->error(__d('syscm',
                            'Invalid request parameter! Your requested page/action has been redirected.'));
            return $this->redirect(array('action' => 'album_index'));
        }

        //add
        if ($this->request->is('post')) {
            $req_params = $this->request->params;
            
            $cids = array(
                $req_params['pass'][1]=> $req_params['pass'][1]
            );
            $album = $this->ScmContentMediaGroup->findById($id);
            if($album){
                if($album['ScmContentsCategory']){
                    foreach($album['ScmContentsCategory'] as $v){
                        $cids[$v['scm_content_id']] = $v['scm_content_id'];
                    }
                }
            }
            
            $cids = array_keys($cids);
            #debug($cids);die();
            $data = array(
                'ScmContentMediaGroup' => array(
                    'id' => $id,
                ),
                'ScmContentMedia' => array(
                    'ScmContentMedia' =>  $cids,
                    ),
            );
#            debug($data);die();
#           if($this->ScmContentMediaGroup->saveAll($data, array('validate'=>false))){
           if($this->ScmContentMediaGroup->saveAssociated($data)){
#           if($this->ScmContentMediaGroup->save($data, false)){
               $this->Flash->success(__d('syscm', 'Media has been added.'));
           }else{
               $this->Flash->error(__d('syscm', 'Media has not been added.'));
           }
           
           
            return $this->redirect(array(
                $id,
                'page' => $req_params['named']['page']
            ));
        }

        //./add

        $this->helpers[] = 'ZeroClipboard';

        $album = $this->ScmContentMediaGroup->findById($id);
        

        $browser_menu_title = __d('syscm', 'Add Media List for Album: %s (%s)',
                $album['ScmContentMediaGroup']['text'],
                $album['ScmContentMediaGroup']['name']);

        
        try {
            $data_list = $this->Paginator->paginate('ScmContentMedia',
                $this->ScmContentMedia->getPaginateConditionsByAlbum($album,
                        true));
        }
        catch (NotFoundException $e) {
            $this->request->params['named']['page'] = 1;
            return $this->redirect(Router::reverse($this->request, true));
        }

        $this->set(compact('browser_menu_title', 'data_list', 'album'));
    }

    /**
     * 
     */
    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->topMenuByModelSetup['invoke'] = $this->ScmContentMedia->invokeTopMenu($this->request->params);
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function delete($id = null)
    {
        if (!$id) {
            $this->Flash->error(__d('syscm', 'Invalid media.'));
            return $this->redirect(array('action' => 'index'));
        }

        if (!$this->ScmContentMedia->deleteImage($id)) {
            $this->Flash->error(__d('syscm', 'Failed to delete the file.'));
            return $this->redirect(array('action' => 'edit', $id));
        }
        if (!$this->ScmContentMedia->dropStatus($id)) {
            $this->Flash->error(__d('syscm', 'Failed to delete the media.'));
            return $this->redirect(array('action' => 'edit', $id));
        }
        $this->Flash->success(__d('syscm', 'File has been deleted.'));
        return $this->redirect(array('action' => 'index'));
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function edit($id = null)
    {
        $this->SysAcl->isAcc('edit');
        $this->helpers[]    = 'ZeroClipboard';
        $browser_menu_title = __d('syscm', 'Edit Media');
        $published_options  = $this->ScmContentPublished->fieldMenuCodeOptions;
        $enqueued_css       = array('Sysadmin.zebra_datepicker/zebra_datepicker-bootstrap');
        $enqueued_script    = array('Sysadmin.zebra_datepicker', 'Sysadmin.bootstrap.file-input');
        $this->set(compact('browser_menu_title', 'enqueued_script',
                        'enqueued_css', 'published_options'));
        if (!$id) {
            return $this->redirect(array('action' => 'index'));
        }
        if (!$this->ScmContentMedia->exists($id)) {
            return $this->redirect(array('action' => 'index'));
        }

        /**
         * update
         */
        if ($this->request->is(array('put', 'post'))) {
            $data                               = $this->request->data;
            $the_file                           = $data['ScmContentMedia']['the_file'];
            unset($data['ScmContentMedia']['the_file']);
            $data['ScmContentContent']['value'] = $this->ScmContentMedia->saveImageFile($the_file,
                    $data['ScmContentContent']['value']);
            if (!$this->ScmContentMedia->saveAssociated($data)) {
                $this->Flash->error(__d('syscm',
                                'Update media has failed. Please try again.'));
            }
            else {
                $this->Flash->success(__d('syscm', 'Media has been updated.'));
            }
            return $this->redirect(array($id));
        }
        /**
         * ./ update
         */
        $this->request->data = $this->ScmContentMedia->findById($id);
    }

    /**
     * 
     * @param type $id
     */
    public function index($id = null)
    {
        $this->SysAcl->isAcc();
        $this->helpers[]    = 'ZeroClipboard';
        $browser_menu_title = __d('syscm', 'Media List');

        $gsparams = array(
            'ScmContentMedia.name'          => __d('syscm', 'ID'),
            'ScmContentMedia.text'          => __d('syscm', 'Name'),
            'ScmContentContent.value'          => __d('syscm', 'Source'),
            'ScmContentMedia.annotation'          => __d('syscm', 'Annotation'),
        );

        $this->_setGlobalSearch($gsparams);
        
        $conditions = $this->ScmContentMedia->getPaginateConditions();
        if ($this->isGs) {
            $gs = $this->_getGlobalSearch();
            if ($gs['global_search_field']) {
                if ($gs['global_search_field'] == $this->allSearchKey) {
                    foreach ($gsparams as $i => $v) {

                        $conditions['AND']['OR'][$i . " LIKE "] = "%" . $gs['global_search_keyword'] . '%';
                    }
                } else {

                    $conditions['AND'][$gs['global_search_field'] . " LIKE "] = "%" . $gs['global_search_keyword'] . '%';
                }
            }
        }

        try {
            $data_list = $this->Paginator->paginate('ScmContentMedia', $conditions);
        }
        catch (NotFoundException $e) {
            $this->request->params['named']['page'] = 1;
            return $this->redirect(Router::reverse($this->request, true));
        }

        $this->set(compact('browser_menu_title', 'data_list'));
    }

}
