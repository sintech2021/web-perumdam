<?php
#debug($data_list);
$model_alias = "ScmCustInfo";
?><div class="">
    <table class="table table-bordered table-condensed table-striped">
        <thead>
            <tr>
                <?php
                if (isset($SysAcl)):
                    if ($SysAcl['read'] || $SysAcl['edit'] || $SysAcl['delete']):
                        ?>
                        <th class="actions col-lg-1 col-sm-1"><?php echo __d('sysadmin', 'Actions'); ?></th>
                        <?php
                    endif;
                endif;
                ?>
                <th>#</th>
                <th><?php echo $this->Paginator->sort($model_alias.'.text',__d('syscm', 'NoSL'));?></th>                
                <th><?php echo $this->Paginator->sort($model_alias.'.name',__d('syscm', 'Name'));?></th>
                <th><?php echo $this->Paginator->sort($model_alias.'.annotation',__d('syscm', 'Address'));?></th>
                <th><?php echo $this->Paginator->sort($model_alias.'Detail.name',__d('syscm', 'Gol'));?></th>
                <th><?php echo $this->Paginator->sort($model_alias.'Detail.text',__d('syscm', 'Wil'));?></th>
            </tr>
        </thead>
        <tbody>
            <?php
            if($data_list):
                $counter = $this->Paginator->counter('{:start}');
                foreach($data_list as $i => $v):
                    ?>
            <tr>
                <?php
                if (isset($SysAcl)):
                    if ($SysAcl['read'] || $SysAcl['edit'] || $SysAcl['delete']):
                        ?> <td class="actions" nowrap="nowrap">
                            <?php
                            
                            if ($SysAcl['edit']):
                                echo $this->Html->link(
                                        '<span class="fa fa-edit fa-border text-warning"></span>',
                                        array(
                                    'action' => 'edit', $v[$model_alias]['id']
                                        ), array('escape' => false)
                                );
                            endif;
                            if ($SysAcl['delete'] ):
                                echo $this->Form->postLink(
                                        '<span class="fa fa-trash fa-border text-danger"></span>',
                                        array(
                                    'action' => 'delete', $v[$model_alias]['id']
                                        ),
                                        array(
                                    'confirm' => __d('sysadmin',
                                            'Are you sure you want to delete # %s?',
                                            h($v[$model_alias]['name'])),
                                    'escape'  => false
                                        )
                                );
                            endif;
                            ?>
                            </td><?php
                        endif;
                    endif;
                    ?>
                <td><?php
                echo $counter+$i;
                ?></td>
                <td><?php echo h($v[$model_alias]['text']);?></td>
                <td><?php echo h($v[$model_alias]['name']);?></td>
                <td><?php echo h($v[$model_alias]['annotation']);?></td>
                <td><?php 
                
                if(isset($data_detail[$v[$model_alias]['id']]['name'])):
                    echo h($data_detail[$v[$model_alias]['id']]['name']);
                endif;
                ?></td>
                <td><?php 
                
                if(isset($data_detail[$v[$model_alias]['id']]['text'])):
                    echo h($data_detail[$v[$model_alias]['id']]['text']);
                endif;
                ?></td>
            </tr>
                        <?php
                endforeach;
            endif;
            ?>
        </tbody>
    </table>
    
    <?php echo $this->element('paginator', array(), array('plugin' => 'sysadmin')); ?>
</div>