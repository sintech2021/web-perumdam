<?php
$model_alias = "ScmCustComment";

?>
<div class="">
    <table class="table table-condensed table-striped">
        <tbody>
            <tr>
                <td class="col-sm-2 col-lg-2 text-info"><?php echo __d('syscm', 'Content');
                $v = $data_row;
                ?></td>
                <td><?php echo $this->Html->link($v['ScmContent']['text'], $this->Html->url($v['ScmContent']['name'], true), array('target' => '_blank'));?></td>
                <!-- -->
                <td class="col-sm-2 col-lg-2 text-info"><?php echo __d('syscm', 'Name');?></td>
                <td><?php echo h($v[$model_alias]['name']); // Name ?></td>
            </tr>
            <tr>
                <td class="col-sm-2 col-lg-2 text-info"><?php echo __d('syscm', 'Thread Content');?></td>
                <td><?php echo h($v[$model_alias]['annotation']); // Thread ?></td>
                <!-- -->
                <td class="col-sm-2 col-lg-2 text-info"><?php echo __d('syscm', 'Email');?></td>
                <td><?php echo h($v['ScmCustCommentEmail']['value']);// Email ?></td>
            </tr>
            <tr>
                <td class="col-sm-2 col-lg-2 text-info"><?php echo __d('syscm', 'Post Date');?></td>
                <td><?php echo h($v[$model_alias]['created']); // Post Date ?></td>
                <!-- -->
                <td class="col-sm-2 col-lg-2 text-info"><?php echo __d('syscm', 'Phone');?></td>
                <td><?php echo h($v['ScmCustCommentPhone']['value']);// Phone ?></td>
            </tr>
            <tr>
                <td class="col-sm-2 col-lg-2 text-info"></td>
                <td></td>
                <!-- -->
                <td class="col-sm-2 col-lg-2 text-info"><?php echo __d('syscm', 'Client IP');?></td>
                <td><?php echo h($v['ScmCustCommentIp']['value']);// IP ?></td>
            </tr>
            
        </tbody>
    </table>
</div>
<div class="post form">
    <?php
    echo $this->element('Sysadmin.HtmlForm/form_create',
                    array('model_alias' => 'Syscm.' . $model_alias));
    ?>
    <fieldset>
        <?php
        echo $this->Form->input($model_alias.'.parent_id', array('type'=> 'hidden', 'value'=> $data_row[$model_alias]['id']));
        echo $this->Form->input($model_alias.'.text', array('type'=> 'hidden', 'value'=> $data_row[$model_alias]['text']));
        echo $this->Form->input($model_alias.'.annotation', array(
            'type' => 'textarea',
            'required' => true,
            'label'=> array(
                'text'  => __d('syscm', 'Reply')." *)",
                'class' => 'col-lg-2 col-sm-2 control-label text-warning',
            )
            ));   
        
        ?>
    </fieldset>
    <?php
    echo $this->element('Sysadmin.HtmlForm/form_end',
                    array(
                'submit_label'  => __d('sysadmin', 'Post Reply')
            ));
    ?>
</div>