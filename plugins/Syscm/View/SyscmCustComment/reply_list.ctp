<?php
#debug($data_list);
$model_alias = "ScmCustComment";

?>
<div class="">
    <table class="table table-condensed table-striped">
        <tbody>
            <tr>
                <td class="col-sm-2 col-lg-2 text-info"><?php echo __d('syscm', 'Content');
                $v = $data_row;
                ?></td>
                <td><?php echo $this->Html->link($v['ScmContent']['text'], $this->Html->url($v['ScmContent']['name'], true), array('target' => '_blank'));?></td>
                <!-- -->
                <td class="col-sm-2 col-lg-2 text-info"><?php echo __d('syscm', 'Name');?></td>
                <td><?php echo h($v[$model_alias]['name']); // Name ?></td>
            </tr>
            <tr>
                <td class="col-sm-2 col-lg-2 text-info"><?php echo __d('syscm', 'Thread Content');?></td>
                <td><?php echo h($v[$model_alias]['annotation']); // Thread ?></td>
                <!-- -->
                <td class="col-sm-2 col-lg-2 text-info"><?php echo __d('syscm', 'Email');?></td>
                <td><?php echo h($v['ScmCustCommentEmail']['value']);// Email ?></td>
            </tr>
            <tr>
                <td class="col-sm-2 col-lg-2 text-info"><?php echo __d('syscm', 'Post Date');?></td>
                <td><?php echo h($v[$model_alias]['created']); // Post Date ?></td>
                <!-- -->
                <td class="col-sm-2 col-lg-2 text-info"><?php echo __d('syscm', 'Phone');?></td>
                <td><?php echo h($v['ScmCustCommentPhone']['value']);// Phone ?></td>
            </tr>
            <tr>
                <td class="col-sm-2 col-lg-2 text-info"></td>
                <td></td>
                <!-- -->
                <td class="col-sm-2 col-lg-2 text-info"><?php echo __d('syscm', 'Client IP');?></td>
                <td><?php echo h($v['ScmCustCommentIp']['value']);// IP ?></td>
            </tr>
            
        </tbody>
    </table>
</div>
<div class="">
    <table class="table table-bordered table-condensed table-striped">
        <thead>
            <tr>
                <?php
                if (isset($SysAcl)):
                    if ($SysAcl['read'] || $SysAcl['edit'] || $SysAcl['delete']):
                        ?>
                        <th class="actions col-lg-1 col-sm-1"><?php echo __d('sysadmin', 'Actions'); ?></th>
                        <?php
                    endif;
                endif;
                
                ?>
                <th>#</th>
                <th><?php echo $this->Paginator->sort('ScmCustComment.annotation',__d('syscm', 'Comment'));?></th>
                <th><?php echo $this->Paginator->sort($model_alias.'.text',__d('syscm', 'Name'));?></th>                
                <th><?php echo $this->Paginator->sort('ScmCustCommentEmail.value',__d('syscm', 'Email'));?></th>
                <th><?php echo $this->Paginator->sort('ScmCustCommentPhone.value',__d('syscm', 'Phone'));?></th>
                
                <th><?php echo $this->Paginator->sort('ScmCustInfoDetail.name',__d('syscm', 'Post Date'));?></th>
                <th><?php echo $this->Paginator->sort('ScmCustCommentIp.value',__d('syscm', 'Client IP'));?></th>
                
            </tr>
        </thead>
        <tbody>
            <?php
            
            if($data_list):
                $counter = $this->Paginator->counter('{:start}');
                foreach($data_list as $i => $v):
                    ?>
            <tr>
                <?php
                if (isset($SysAcl)):
                    if ($SysAcl['read'] || $SysAcl['edit'] || $SysAcl['delete']):
                        ?> <td class="actions" nowrap="nowrap">
                            <?php
                            $reply_edit = $this->Html->link(
                                        '<span class="fa fa-edit fa-border text-warning"></span>',
                                        array(
                                    'action' => 'reply_edit', $v[$model_alias]['parent_id'],$v[$model_alias]['id']
                                        ), array('escape' => false)
                                );
                            $isEdit = true;
                            if (!$SysAcl['edit']):
                                 $isEdit = false;
                            endif;
                            if (!$v['ScmCustCommentIp']['name'] != $this->Session->read('AppUsrLog.id')):
                                 $isEdit = false;
                            endif;
                            if($isEdit):
                                echo $reply_edit;
                            endif;
                            if ($SysAcl['delete'] ):
                                echo $this->Form->postLink(
                                        '<span class="fa fa-trash fa-border text-danger"></span>',
                                        array(
                                    'action' => 'delete_reply', $v[$model_alias]['parent_id'], $v[$model_alias]['id']
                                        ),
                                        array(
                                    'confirm' => __d('sysadmin',
                                            'Are you sure you want to delete # %s?',
                                            h($v[$model_alias]['annotation'])),
                                    'escape'  => false
                                        )
                                );
                            endif;
                            ?>
                            </td><?php
                        endif;
                    endif;
                    ?>
                <td><?php
                echo $counter+$i;
                ?></td>
                <td><?php echo h($v[$model_alias]['annotation']); // comment ?></td>
                <td><?php echo h($v[$model_alias]['name']); //  ?></td>
                <td><?php echo h($v['ScmCustCommentEmail']['value']);// email ?></td>
                <td><?php echo h($v['ScmCustCommentPhone']['value']);// phone ?></td>
                
                <td><?php echo h($v[$model_alias]['created']); // post date ?></td>
                <td><?php echo h($v['ScmCustCommentIp']['value']);// ip ?></td>
            </tr><?php
                endforeach;
            endif;
            ?>
        </tbody>
    </table>
    
    <?php echo $this->element('paginator', array(), array('plugin' => 'sysadmin')); 
    debug($data_list);
    ?>
</div>