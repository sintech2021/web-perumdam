<?php
#debug($data_list);
$model_alias = "ScmCustComment";
?><div class="">
    <table class="table table-bordered table-condensed table-striped">
        <thead>
            <tr>
                <?php
                if (isset($SysAcl)):
                    if ($SysAcl['read'] || $SysAcl['edit'] || $SysAcl['delete']):
                        ?>
                        <th class="actions col-lg-1 col-sm-1"><?php echo __d('sysadmin', 'Actions'); ?></th>
                        <?php
                    endif;
                endif;
                
                ?>
                <th>#</th>

                <th><?php echo $this->Paginator->sort('ScmContent.text',__d('syscm', 'Content'));?></th>               
                <th><?php echo $this->Paginator->sort('ScmCustComment.annotation',__d('syscm', 'Thread Content'));?></th>
                <th><?php echo $this->Paginator->sort($model_alias.'.text',__d('syscm', 'Name'));?></th>                
                <th><?php echo $this->Paginator->sort('ScmCustCommentEmail.value',__d('syscm', 'Email'));?></th>
                <th><?php echo $this->Paginator->sort('ScmCustCommentPhone.value',__d('syscm', 'Phone'));?></th>
                
                <th><?php echo $this->Paginator->sort('ScmCustInfoDetail.name',__d('syscm', 'Post Date'));?></th>
                <th><?php echo $this->Paginator->sort('ScmCustCommentIp.value',__d('syscm', 'Client IP'));?></th>
                
            </tr>
        </thead>
        <tbody>
            <?php
            
            if($data_list):
                $counter = $this->Paginator->counter('{:start}');
                foreach($data_list as $i => $v):
                    ?>
            <tr>
                <?php
                if (isset($SysAcl)):
                    if ($SysAcl['read'] || $SysAcl['edit'] || $SysAcl['delete']):
                        ?> <td class="actions" nowrap="nowrap">
                            <?php
                            
                            if ($SysAcl['edit']):
                                echo $this->Html->link(
                                        '<span class="fa fa-list fa-border text-warning"></span>',
                                        array(
                                    'action' => 'reply_list', $v[$model_alias]['id']
                                        ), array('escape' => false)
                                );
                            endif;
                            if ($SysAcl['delete'] ):
                                echo $this->Form->postLink(
                                        '<span class="fa fa-trash fa-border text-danger"></span>',
                                        array(
                                    'action' => 'delete', $v[$model_alias]['id']
                                        ),
                                        array(
                                    'confirm' => __d('sysadmin',
                                            'Are you sure you want to delete # %s?',
                                            h($v[$model_alias]['annotation'])),
                                    'escape'  => false
                                        )
                                );
                            endif;
                            ?>
                            </td><?php
                        endif;
                    endif;
                    ?>
                <td><?php
                echo $counter+$i;
                ?></td>
                <td><?php echo $this->Html->link($v['ScmContent']['text'], $this->Html->url($v['ScmContent']['name'], true), array('target' => '_blank'));?></td>
                <td><?php echo h($v[$model_alias]['annotation']); // NoSL ?></td>
                <td><?php echo h($v[$model_alias]['name']); // NoSL ?></td>
                <td><?php echo h($v['ScmCustCommentEmail']['value']);// Nama ?></td>
                <td><?php echo h($v['ScmCustCommentPhone']['value']);// Alamat ?></td>
                <td><?php echo h($v[$model_alias]['created']); // NoSL ?></td>
                <td><?php echo h($v['ScmCustCommentIp']['value']);// Alamat ?></td>
            </tr><?php
                endforeach;
            endif;
            ?>
        </tbody>
    </table>
    
    <?php echo $this->element('paginator', array(), array('plugin' => 'sysadmin')); 
    debug($data_list);
    ?>
</div>