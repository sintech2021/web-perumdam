<?php
$model_alias = "ScmCustReg";
?><div class="">
    <form  class="form-horizontal" role="form" id="ScmCustRegForm" method="post" accept-charset="utf-8"><div style="display:none;"><input name="_method" value="POST" type="hidden"></div>
        <table class="table table-bordered table-condensed table-striped">
            <thead>
                <tr>
                    <?php
                    if (isset($SysAcl)):
                        if ($SysAcl['read'] || $SysAcl['edit'] || $SysAcl['delete']):
                            ?>
                            <th>
                                <input type="checkbox" onclick="$('.input-checkbox').prop('checked', this.checked);" />
                            </th>
                            <?php
                        endif;
                    endif;
                    ?>
                    <th>#</th>
                    <th><?php
                        echo $this->Paginator->sort($model_alias . '.menu_order', '<span class="fa fa-check"></span>', array(
                            'escape' => false));
                        ?></th>                
                    <th><?php echo $this->Paginator->sort($model_alias . '.text', __d('syscm', 'Registration ID')); ?></th>                
                    <th><?php echo $this->Paginator->sort($model_alias . '.name', __d('syscm', 'Name')); ?></th>
                    <th><?php echo $this->Paginator->sort($model_alias . 'Detail.value', __d('syscm', 'Address')); ?></th>
                    <th><?php echo $this->Paginator->sort($model_alias . 'Detail.name', __d('syscm', 'Email')); ?></th>
                    <th><?php echo $this->Paginator->sort($model_alias . 'Detail.annotation', __d('syscm', 'Phone Number')); ?></th>
                    <th><?php echo $this->Paginator->sort($model_alias . 'Detail.text', __d('syscm', 'Cellular Number')); ?></th>
                    
                    <th><?php echo $this->Paginator->sort($model_alias . '.created', __d('syscm', 'Registration Date')); ?></th>
                    <th><?php echo $this->Paginator->sort($model_alias . '.modified', __d('syscm', 'Last Update')); ?></th>
                    <th><?php echo $this->Paginator->sort($model_alias . 'Detail.menu_code', __d('syscm', 'Client IP')); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if ($data_list):
                    $counter = $this->Paginator->counter('{:start}');
                    foreach ($data_list as $i => $v):
                        ?>
                        <tr>
                            <?php
                            if (isset($SysAcl)):
                                if ($SysAcl['read'] || $SysAcl['edit'] || $SysAcl['delete']):
                                    ?> <td class="actions" nowrap="nowrap">
                                    <input type="checkbox" name="data[<?php echo $model_alias; ?>][checked_id][]" value="<?php echo $v[$model_alias]['id'];?>" class="input-checkbox"/>
                                    </td><?php
                                endif;
                            endif;
                            ?>
                            <td><?php
                                echo $counter + $i;
                                ?></td>
                            <td nowrap='nowrap'><?php
                                if ($v[$model_alias]['menu_order']):
                                    ?><span class="fa fa-check-square-o text-success"><?php
                                    else:
                                        ?><div class="text-danger fa-border">
                                        <?php echo __d('syscm', 'New') . ' *'; ?>
                                        </div><?php
                                    endif;
                                    ?></td>
                            <td><?php echo h($v[$model_alias]['text']); ?></td>
                            <td><?php echo h($v[$model_alias]['name']); ?></td>
                            <td><?php echo $v[$model_alias . 'Detail']['value']; ?></td>
                            <td><?php echo h($v[$model_alias . 'Detail']['name']); ?></td>
                            <td><?php echo h($v[$model_alias . 'Detail']['annotation']); ?></td>
                            <td><?php echo h($v[$model_alias . 'Detail']['text']); ?></td>
                            
                            <td><?php echo h($v[$model_alias ]['created']); ?></td>
                            <td><?php echo h($v[$model_alias ]['modified']); ?></td>
                            <td><?php echo h($v[$model_alias . 'Detail']['menu_code']); ?></td>
                            
                        </tr>
                        <?php
                    endforeach;
                endif;
                ?>
            </tbody>
        </table>
        <div class="form-group">
            <?php
            if (isset($SysAcl)):
                if ($SysAcl['read'] || $SysAcl['edit'] || $SysAcl['delete']):
                    ?>
                    <label class="col-lg-2 col-sm-2 control-label text-warning"><?php echo __d('syscm', 'With selected'); ?></label>
                    <div class="col-lg-10 col-sm-10">
                        <?php
                        if ($SysAcl['edit']):
                            ?>

                            <button type="submit" class="btn btn-primary" name="data[<?php echo $model_alias ?>][action]" value='mark_done'>
                                <span class="fa fa-check-square"></span>
            <?php echo __d('syscm', 'Mark done'); ?>
                            </button>
                            <button type="submit" class="btn btn-warning" name="data[<?php echo $model_alias ?>][action]" value='mark_undone'>
                                <span class="fa fa-ban"></span>
                            <?php echo __d('syscm', 'Mark undone'); ?>
                            </button>
                            <?php
                        endif;
                        ?>
                        <?php
                        if ($SysAcl['read']):
                            ?>
                        <button type="submit" class="btn btn-success" name="data[<?php echo $model_alias ?>][action]" value='export_xls'>
                                <span class="fa fa-file-excel-o"></span>
                            <?php echo __d('syscm', 'Export Excel'); ?>
                            </button>
                            <?php
                        endif;
                        ?>
                        <?php
                        if ($SysAcl['delete']):
                            ?>

                            <button type="submit" class="btn btn-danger" name="data[<?php echo $model_alias ?>][action]" value='mark_delete' onclick="return confirm('<?php echo __d('syscm', 'Delete selected?'); ?>');">
                                <span class="fa fa-trash"></span>
                            <?php echo __d('syscm', 'Delete'); ?>
                            </button>
                            <?php
                        endif;
                        ?>
                    </div><?php
                endif;
            endif;
            ?>
        </div>
    </form>
<?php echo $this->element('paginator', array(), array('plugin' => 'sysadmin')); ?>
</div>
<?php
debug($data_list);
