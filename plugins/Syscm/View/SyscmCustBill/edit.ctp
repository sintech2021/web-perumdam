<?php
$model_alias   = "ScmCustInfo";
?>
<div class="post form">
    <?php
    echo $this->element('Sysadmin.HtmlForm/form_create',
                    array('model_alias' => 'Syscm.' . $model_alias, 'form_type' => 'file'));
    ?>
    <fieldset>
        <?php
        echo $this->Form->input($model_alias.'.id');
        
        echo $this->Form->input($model_alias.'.text', array(
            'type' => 'text',
            'label'=> array(
                'text'  => __d('syscm', 'NoSL')." *)",
                'class' => 'col-lg-2 col-sm-2 control-label text-warning',
            ),
            'maxlength' => '255',
        ));   
        echo $this->Form->input($model_alias.'.name', array(
            'type' => 'text',
            'label'=> array(
                'text'  => __d('syscm', 'Name')." *)",
                'class' => 'col-lg-2 col-sm-2 control-label text-warning',
            ),
            'maxlength' => '255',
        ));   
        echo $this->Form->input($model_alias.'.annotation', array(
            'type' => 'textarea',
            'label'=> array(
                'text'  => __d('syscm', 'Address')." *)",
                'class' => 'col-lg-2 col-sm-2 control-label text-warning',
            )
            ));   
        echo $this->Form->input('ScmCustInfoDetail.id');
        echo $this->Form->input('ScmCustInfoDetail.name', array(
            'type' => 'text',
            'label'=> array(
                'text'  => __d('syscm', 'Gol')." *)",
                'class' => 'col-lg-2 col-sm-2 control-label text-warning',
            ),
            'maxlength' => '255',
        )); 
        echo $this->Form->input('ScmCustInfoDetail.text', array(
            'type' => 'text',
            'label'=> array(
                'text'  => __d('syscm', 'Wil')." *)",
                'class' => 'col-lg-2 col-sm-2 control-label text-warning',
            ),
            'maxlength' => '255',
        )); 
        ?>
    </fieldset>
    <?php
    echo $this->element('Sysadmin.HtmlForm/form_end',
                    array(
                'submit_label'  => __d('sysadmin', 'Save Customer')
            ));
    ?>
</div>