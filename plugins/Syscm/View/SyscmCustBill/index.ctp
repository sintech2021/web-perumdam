<?php
#debug($data_list);
$model_alias = "ScmCustBill";
?><div class="">
    <table class="table table-bordered table-condensed table-striped">
        <thead>
            <tr>
                <?php
                if (isset($SysAcl)):
                    if ($SysAcl['read'] || $SysAcl['edit'] || $SysAcl['delete']):
                        ?>
                        <th class="actions col-lg-1 col-sm-1"><?php echo __d('sysadmin', 'Actions'); ?></th>
                        <?php
                    endif;
                endif;
                /**
                 *  * 'A' => 'NoSL', ScmCustInfo.text ScmCustBill.text
                  'B' => 'BlnPakai', ScmCustBill.name
                  'C' => 'ThnPakai', ScmCustBill.annotation
                  'D' => 'StdAwal', ScmCustBillDetail.name
                  'E' => 'StdAkhir', ScmCustBillDetail.value
                  'F' => 'Kubikasi', ScmCustBillDetail.text
                  'G' => 'RpAir', ScmCustBillAmount.name
                  'H' => 'RpAdm', ScmCustBillAmount.text
                  'I' => 'RpPmh', ScmCustBillAmount.menu_code
                  'J' => 'RpTagihant', ScmCustBillAmount.value
                 */
                ?>
                <th>#</th>
                <th><?php echo $this->Paginator->sort($model_alias . '.text', __d('syscm', 'NoSL')); ?></th>                
                <th><?php
                    //echo $this->Paginator->sort('ScmCustInfo.name',__d('syscm', 'Name'));
                    echo __d('syscm', 'Name');
                    ?></th>
                <th><?php
                    //echo $this->Paginator->sort('ScmCustInfo.annotation',__d('syscm', 'Address'));
                    echo __d('syscm', 'Address');
                    ?></th>
                <th><?php
                    //echo $this->Paginator->sort('ScmCustInfoDetail.name',__d('syscm', 'Gol'));
                    echo __d('syscm', 'Gol');
                    ?></th>
                <th><?php
                    //echo $this->Paginator->sort('ScmCustInfoDetail.text',__d('syscm', 'Wil'));
                    echo __d('syscm', 'Wil');
                    ?></th>
                <th><?php echo $this->Paginator->sort($model_alias . '.name', __d('syscm', 'BlnPakai')); ?></th>
                <th><?php echo $this->Paginator->sort($model_alias . '.annotation', __d('syscm', 'ThnPakai')); ?></th>
                <th><?php
//                echo $this->Paginator->sort($model_alias.'Detail.name',__d('syscm', 'StdAwal'));
                    echo __d('syscm', 'StdAwal');
                    ?></th>
                <th><?php
//                echo $this->Paginator->sort($model_alias.'Detail.value',__d('syscm', 'StdAkhir'));
                    echo __d('syscm', 'StdAkhir');
                    ?></th>
                <th><?php
//                echo $this->Paginator->sort($model_alias.'Detail.text',__d('syscm', 'Kubikasi'));
                    echo __d('syscm', 'Kubikasi');
                    ?></th>
                <th><?php
//                echo $this->Paginator->sort($model_alias.'Amount.name',__d('syscm', 'RpAir'));
                    echo __d('syscm', 'RpAir');
                    ?></th>
                <th><?php
//                echo $this->Paginator->sort($model_alias.'Amount.text',__d('syscm', 'RpAdm'));
                    echo __d('syscm', 'RpAdm');
                    ?></th>
                <th><?php
//                echo $this->Paginator->sort($model_alias.'Amount.menu_code',__d('syscm', 'RpPmh'));
                    echo __d('syscm', 'RpPmh');
                    ?></th>
                <th><?php
                    //echo $this->Paginator->sort($model_alias.'Amount.value',__d('syscm', 'RpTagihan'));
                    echo __d('syscm', 'RpTagihan');
                    ?></th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ($data_list):
                $counter = $this->Paginator->counter('{:start}');
                foreach ($data_list as $i => $v):
                    ?>
                    <tr>
                        <?php
                        if (isset($SysAcl)):
                            if ($SysAcl['read'] || $SysAcl['edit'] || $SysAcl['delete']):
                                ?> <td class="actions" nowrap="nowrap">
                                <?php
                                /* if ($SysAcl['edit']):
                                  echo $this->Html->link(
                                  '<span class="fa fa-edit fa-border text-warning"></span>',
                                  array(
                                  'action' => 'edit', $v[$model_alias]['id']
                                  ), array('escape' => false)
                                  );
                                  endif; */
                                if ($SysAcl['delete']):
                                    echo $this->Form->postLink(
                                            '<span class="fa fa-trash fa-border text-danger"></span>', array(
                                        'action' => 'delete', $v[$model_alias]['id']
                                            ), array(
                                        'confirm' => __d('sysadmin', 'Are you sure you want to delete # %s?', h($v[$model_alias]['text'])),
                                        'escape'  => false
                                            )
                                    );
                                endif;
                                ?>
                                </td><?php
                            endif;
                        endif;
                        ?>
                        <td><?php
                            echo $counter + $i;
                            ?></td>
                        <td><?php echo h($v[$model_alias]['text']); // NoSL    ?></td>
                        <td><?php
                            $cust_name      = isset($data_cust[$v[$model_alias]['parent_id']]['name']) ? $data_cust[$v[$model_alias]['parent_id']]['name'] : '';
                            echo h($cust_name); // Nama 
                            ?></td>
                        <td><?php
                            $cust_address   = isset($data_cust[$v[$model_alias]['parent_id']]['annotation']) ? $data_cust[$v[$model_alias]['parent_id']]['annotation'] : '';
                            echo h($cust_address); // alamat
                            ?></td>
                        <td><?php
                            $cust_gol       = isset($data_detail[$v[$model_alias]['parent_id']]['name']) ? $data_detail[$v[$model_alias]['parent_id']]['name'] : '';
                            echo h($cust_gol); //  Gol 
                            ?></td>
                        <td><?php
                            $cust_gol       = isset($data_detail[$v[$model_alias]['parent_id']]['text']) ? $data_detail[$v[$model_alias]['parent_id']]['text'] : '';
                            echo h($cust_gol); //  Gol 
                            ?></td>
                        <td class="text-right"><?php echo h($v[$model_alias]['name']); // BlnPakai    ?></td>
                        <td class="text-right"><?php echo h($v[$model_alias]['annotation']); // ThnPakai   ?></td>
                        <td class="text-right"><?php
                            $cust_std_awal  = isset($data_bill[$v[$model_alias]['id']]['name']) ? $data_bill[$v[$model_alias]['id']]['name'] : '';
                            echo h($cust_std_awal); //  stdawal 
                            ?></td>
                        <td class="text-right"><?php
                            $cust_std_akhir = isset($data_bill[$v[$model_alias]['id']]['value']) ? $data_bill[$v[$model_alias]['id']]['value'] : '';
                            echo h($cust_std_akhir); //  atdakhir
                            ?></td>
                        <td class="text-right"><?php
                            $cust_kubikasi  = isset($data_bill[$v[$model_alias]['id']]['text']) ? $data_bill[$v[$model_alias]['id']]['text'] : '';
                            echo h($cust_kubikasi); //  kubikasi
                            ?></td>
                        <?php
                        /**
                         * amount
                         * name rpair
                         * text rpadm
                         * menu_code rppmh
                         * value tagihan
                         * <td class="text-right"><?php echo number_format($v[$model_alias . 'Amount']['name'], '2', ',', '.'); // RpAir   ?></td>
                         */
                        $amounts        = array('name', 'text', 'menu_code', 'value');
                        foreach ($amounts as $amt):
                            $amttext = isset($data_amount[$v[$model_alias]['id']][$amt]) ? number_format($data_amount[$v[$model_alias]['id']][$amt], '2', ',', '.') : '';
                            if ($amt == 'value'):
                                $amttext = sprintf('<strong>%s</strong>', $amttext);
                            endif;
                            printf('<td class="text-right">%s</td>', $amttext);
                        endforeach;
                        ?>


                    </tr><?php
                endforeach;
            endif;
            /**
             *  * 'A' => 'NoSL', ScmCustInfo.text ScmCustBill.text
              'B' => 'BlnPakai', ScmCustBill.name
              'C' => 'ThnPakai', ScmCustBill.annotation
              'D' => 'StdAwal', ScmCustBillDetail.name
              'E' => 'StdAkhir', ScmCustBillDetail.value
              'F' => 'Kubikasi', ScmCustBillDetail.text
              'G' => 'RpAir', ScmCustBillAmount.name
              'H' => 'RpAdm', ScmCustBillAmount.text
              'I' => 'RpPmh', ScmCustBillAmount.menu_code
              'J' => 'RpTagihant', ScmCustBillAmount.value
             */
            ?>
        </tbody>
    </table>

    <?php
    echo $this->element('paginator', array(), array('plugin' => 'sysadmin'));
    debug($data_list);
    ?>
</div>