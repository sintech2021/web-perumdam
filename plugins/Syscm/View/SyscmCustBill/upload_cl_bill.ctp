<?php
$model_alias   = "ScmCustBill";
$sheetData     = $this->get('sheetData', array());
$the_file_path = $this->get('uploaded_path', false);
$save_status   = $this->get('save_status', array());
?>
<?php
if ($sheetData) :
    ?>
    <div class="posts form">
        <h4><?php echo __d('sysadmin', 'Data Preview'); ?></h4>
        <?php
        if ($the_file_path):
            echo $this->element('Sysadmin.HtmlForm/form_create',
                    array('model_alias' => 'Syscm.' . $model_alias, 'form_type' => 'file'));
            echo $this->Form->input($model_alias . '.the_file_path',
                    array(
                'type'  => 'hidden',
                'value' => $the_file_path));
            echo $this->element('Sysadmin.HtmlForm/form_end',
                    array(
                'submit_label'  => __d('sysadmin', 'Save Customer Bill'),
                'submit_class'  => 'btn btn-warning',
                'submit_before' => '<div class="col-sm-10 col-lg-10">',
            ));
        endif;
        ?>
        <div class="panel panel-info">
            <div class="panel-body" style="overflow: scroll; max-height:500px; ">
                <table class="table table-condensed table-bordered table-striped">
                    <?php
                    foreach ($sheetData as $i => $v):
                        $celltype = $i == 1 ? 'th' : 'td';
                        echo $i == 1 ? '<thead>' : '';
                        echo $i == 2 ? '<tbody>' : '';
                        echo '<tr>';
                        foreach ($v as $cell):
                            printf('<%s>%s</%1$s>', $celltype, $cell);
                        endforeach;
                        echo '</tr>';
                        echo $i == 1 ? '</thead>' : '';
                    endforeach;
                    echo $i == 2 ? '</tbody>' : '';
                    ?>
                    <thead>
                        <tr>

                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <br>
        <!-- Confirmed button-->
        <?php
        if ($the_file_path):
            echo $this->element('Sysadmin.HtmlForm/form_create',
                    array('model_alias' => 'Syscm.' . $model_alias, 'form_type' => 'file'));
            echo $this->Form->input($model_alias . '.the_file_path',
                    array(
                'type'  => 'hidden',
                'value' => $the_file_path));
            echo $this->element('Sysadmin.HtmlForm/form_end',
                    array(
                'submit_label'  => __d('sysadmin', 'Save Customer Bill'),
                'submit_class'  => 'btn btn-warning',
                'submit_before' => '<div class="col-sm-10 col-lg-10">',
            ));
        endif;
        
        ?>
        <hr>
    </div>
    <?php
endif;
if ($save_status):
            echo '<ul class="list list-group"><li class="list-group-item">';
            foreach ($save_status as $v):
                if ($v):
                    extract($v);
                    printf('<div class="alert %s">%s</div>', $css_class, $status_text);
                endif;

            endforeach;
            echo "</li></ul>";
        endif;
?>
<div class="posts form">
    <?php
    echo $this->element('Sysadmin.HtmlForm/form_create',
            array('model_alias' => 'Syscm.' . $model_alias, 'form_type' => 'file'));
    ?>
    <fieldset>
        <?php
        echo $this->Form->input('the_file',
                array(
            'type'    => 'file',
            'title'   => __d('syscm', 'Choose file..'),
            'label'   => array(
                'text'  => __d('syscm', 'File') . " *)",
                'class' => 'col-lg-2 col-sm-2 control-label text-warning',
            ),
            'between' => '<div class="col-lg-2 col-sm-2">',
        ));
        ?>
    </fieldset>
    <?php
    echo $this->element('Sysadmin.HtmlForm/form_end',
            array('submit_label' => __d('sysadmin', 'Submit')));
    ?>
</div>