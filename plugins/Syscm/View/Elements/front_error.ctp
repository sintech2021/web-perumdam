<?php
if(!isset($msg)): return; endif;
?>
<div class="front-alert front-alert-danger"><span class="fa fa-exclamation-triangle"></span> <?php
echo $msg;
?></div>