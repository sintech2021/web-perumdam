<nav id="nav">
    <?php
    $selected = array(false,false,false,false);
    if(isset($selected_index)):
    if(isset($selected[$selected_index])):
        $selected[$selected_index] = true;
    endif;
    endif;
    $syscm_main_nav = array(
        array(
            'text' => __d('syscm','Home'),
            'url' => array('plugin' => 'syscm', 'action'=> 'index'),
            'active' => $selected[0]
        ),
        array(
            'text' => __d('syscm','My Billing'),
            'url' => array('plugin' => 'syscm', 'action'=> 'my_billing'),
            'active' => $selected[1]
        ),
        array(
            'text' => __d('syscm','About Us'),
            'url' => array('plugin' => 'syscm', 'action'=> 'about'),
            'active' => $selected[2]
        ),
        array(
            'text' => __d('syscm','Contact Us'),
            'url' => array('plugin' => 'syscm', 'action'=> 'contact'),
            'active' => $selected[3]
        )
    );
    ?>
    <ul>
        <?php
        foreach($syscm_main_nav as $i => $v):
            $li_class = '';// current ::> active
           if(isset($v['active'])):
               $li_class = $v['active'] ? 'current':'';
           endif;
            
            ?>
        <li class="<?php
        echo $li_class;?>">
            <?php
            $text = isset($v['text'])? h($v['text']):'';
            $url = isset($v['url'])?$v['url']:'#';
            echo $this->Html->link($text, $url);
            ?>
        </li>
                <?php
        endforeach;
        ?>
        
        <!--<li>
            <a href="#">Dropdown</a>
            <ul>
                <li><a href="#">Lorem ipsum dolor</a></li>
                <li><a href="#">Magna phasellus</a></li>
                <li>
                    <a href="#">Phasellus consequat</a>
                    <ul>
                        <li><a href="#">Lorem ipsum dolor</a></li>
                        <li><a href="#">Phasellus consequat</a></li>
                        <li><a href="#">Magna phasellus</a></li>
                        <li><a href="#">Etiam dolore nisl</a></li>
                    </ul>
                </li>
                <li><a href="#">Veroeros feugiat</a></li>
            </ul>
        </li>
        <li><a href="left-sidebar.html">Left Sidebar</a></li>
        <li><a href="right-sidebar.html">Right Sidebar</a></li>
        <li><a href="no-sidebar.html">No Sidebar</a></li>-->
    </ul>
</nav>