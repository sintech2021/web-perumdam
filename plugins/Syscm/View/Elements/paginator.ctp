<ul class="news_pagination"><?php echo $this->Paginator->prev('<span aria-hidden="true">&laquo;</span>' , array('escape'=>false, 'tag'=> 'li'), null,
        array('class' => 'prev disabled')); 
            echo $this->Paginator->numbers(array('modulus'=>(is_mobile()?2:5),'separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
            echo $this->Paginator->next('<span aria-hidden="true">&raquo;</span>', array('escape'=>false, 'tag'=> 'li'), null,
        array('class' => 'next disabled'));
            ?></ul>