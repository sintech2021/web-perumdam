<div id="main-wrapper">
    <div class="container">
        <div class="row 200%">
            <div class="4u 12u(medium)">
                <!-- Sidebar -->
                <?php echo $this->element('Syscm.main-sidebar');?>
                <!-- ./Sidebar -->
            </div>
            <div class="8u 12u(medium) important(medium)">
                <!-- Content -->
                <div id="content">
                    <section class="last">
                        <?php echo $this->fetch('content');?>                        
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>