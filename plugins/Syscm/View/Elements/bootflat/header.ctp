
	 <div class="header-button-sm row clearfix">
		<div class="col-xs-12">
		<button type="button" class="navbar-toggle offcanvas-toggle" data-toggle="offcanvas" data-target="#nav" style="float:left;">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
		<h6 class='visible-xs'><a href="<?php echo $this->Html->url(array(
    'plugin' => 'syscm', 'controller' => 'syscm_front', 'action' => 'index')); ?>">&nbsp;<?php echo Configure::read('Sysconfig.company.name'); ?></a></h6>
		</div>
	 </div>
    <!-- Logo -->
    <div id="logo" class="row">
	
		<div class="col-md-12" id="header">
			 <div class="col-xs-4 col-xs-offset-4 col-sm-2 col-sm-offset-0 col-lg-2 col-lg-offset-0">
				<?php
                $logo_path = isset($logo_path) ? $logo_path : '';
                echo $this->Html->image($logo_path,array("class"=>"img-responsive"));
                ?>
			</div>
			<div class="col-xs-12 col-sm-10 col-sm-offset-0 col-lg-10 col-lg-offset-0 hidden-xs">
			<?php
				$footer_icons = $this->get('footer_icons', array());
				if (!$footer_icons): return;
				endif;
				?><!-- Icons -->
					<?php foreach ($footer_icons as $v): ?>
						<?php if (!isset($v['content'])): continue; endif; ?>
						<?php if (!isset($v['url'])): continue; endif; ?>
						<?php if (!isset($v['text'])): $v['text'] = $v['url']; endif; ?>
					<?php 
					$link = str_replace(array('{:url}', '{:text}'), '%s', $v['content']); 
					//var_dump($v);
					//printf($link, $v['url'], $v['text']);
					echo  '<a href="'.$v['url'].'" target="_BLANK"><span class="fa-stack fa-lg pull-right trans">
							  <i class="fa fa-circle fa-stack-2x"></i>
							  <i class="fa fa-'.strtolower($v['text']).' fa-stack-1x fa-inverse"></i>
						  </span></a>';
					?>
				<?php endforeach; ?>
			<h1 style="vertical-align: top;" class="brand"><a href="<?php echo $this->Html->url(array(
    'plugin' => 'syscm', 'controller' => 'syscm_front', 'action' => 'index')); ?>"><?php echo Configure::read('Sysconfig.company.name'); ?></a><br><small class="text-primary">KABUPATEN TANGERANG</small></h1>
			</div>
		</div>
        
    </div>
    <!-- Nav -->
    <?php
    echo $this->element('Syscm.' . $theme_alias . '/nav');
    ?>

    <!-- ./Nav -->
<?php if (isset($featured_contents) || 1==1) :
    echo $this->element('Syscm.' . $theme_alias . '/featured-contents');
    endif; ?>
