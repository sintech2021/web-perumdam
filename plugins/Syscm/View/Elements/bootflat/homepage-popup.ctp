<?php
$homepage_popup_content = $this->get('homepage_popup_content', null);
if(!$homepage_popup_content) return;
?><div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		  <div class="modal-dialog modal-lg">
			<div class="modal-content">
			  <?php echo $homepage_popup_content;?>
			</div>
		  </div>
		</div>