 <header class="clearfix fit-margin">
<nav class="navbar navbar-default navbar-offcanvas navbar-offcanvas-touch navbar-offcanvas-fade no-radius" role="navigation" id="nav">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
		<a class="navbar-brand" href="#">
				<?php
					$logo_path = isset($logo_path) ? $logo_path : '';
					echo $this->Html->image($logo_path,array("class"=>"img-responsive"));
                ?>
		</a>
		  <button type="button" class="navbar-toggle offcanvas-toggle pull-right" data-toggle="offcanvas" data-target="#nav" style="float:left;">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="glyphicon glyphicon-remove"></i>
                    </button>
		</div>
    <?php
    //$syscm_main_nav = $this->fetch('syscm_main_nav', array());
    ?>
	<div>
    <ul class="nav navbar-nav" >
        <?php
        if (isset($syscm_main_nav)) {
            foreach ($syscm_main_nav as $i => $v):

                $li_class        = ''; // current ::> active
                $parent_selected = false;
                $childs          = "";

                if ($v['ChildMenu']) {
					$li_class        = 'dropdown-menu ';
                    $childs          = $this->element('Syscm.' . $theme_alias . '/sub-nav',
                            array('syscm_sub_main_nav' => $v['ChildMenu']));
                    $parent_selected = $this->get('parent_selected');
                }
                $text = isset($v['FrontModel']['text']) ? h($v['FrontModel']['text'])
                            : '';
                $url  = isset($v['ScmMenuSlug']['value']) ? $v['ScmMenuSlug']['value']
                            : '#';
                if (isset($v['ScmMenuSlug']['value'])):

                    $li_class = $this->html->url($url) == $selected_slug || $parent_selected
                                ? 'current' : '';
                $parent_selected = false;
                endif;
                ?>
                <li class="<?php echo $li_class; ?> <?php echo ($v['ChildMenu']?'dropdown':'');?>">
                        <?php
						if($v['ChildMenu']){
							$classA =array('class' => 'dropdown-toggle','data-toggle'=>'dropdown');
						}else{
							$classA ="";
						}
                        echo $this->Html->link(__d('syscm', $text), $url,$classA);
                        echo $childs;
                        ?>
                </li>
                <?php
            endforeach;
        }
        ?></ul>
		<?php
                echo isset($below_header['searchform']) ?  $this->element('Syscm.' . $theme_alias . '/searchform', array('searchform' => $below_header['searchform'])):"";
                ?>
		
		</div>
</nav>
</header>