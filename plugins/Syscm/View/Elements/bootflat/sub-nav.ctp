<?php if ($syscm_sub_main_nav): ?>

    <ul class="dropdown-menu" role="menu">
        <?php 
        $parent_selected = false;
        foreach ($syscm_sub_main_nav as $ii => $vv): ?>
            <?php
            
            $text            = isset($vv['text']) ? h($vv['text']) : '';
            $url             = isset($vv['ScmMenuSlug']['value']) ? $vv['ScmMenuSlug']['value']
                        : '#';
            $li_class_sub    = ''; // current ::> active
            if (isset($vv['ScmMenuSlug']['value'])):
                if ($this->html->url($url) == $selected_slug):
                    $li_class_sub    = 'current';
                    $parent_selected = true;

                endif;

            endif;
            
            ?>
            <li class="<?php echo $li_class_sub; ?>">
                <?php
                echo $this->Html->link(__d('syscm', $text), $url);
                if (isset($vv['ChildMenu'])) {
                    echo $this->element('Syscm.' . $theme_alias . '/sub-nav',
                            array('syscm_sub_main_nav' => $vv['ChildMenu']));
                }
                ?>
            </li>

        <?php endforeach; 
        $this->set(compact('parent_selected'));
        ?>
    </ul>
<?php endif; ?>