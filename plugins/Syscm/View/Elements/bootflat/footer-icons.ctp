<div class="clearfix text-center">
<?php
$footer_icons = $this->get('footer_icons', array());
if (!$footer_icons): return;
endif;
?><!-- Icons -->
    <?php foreach ($footer_icons as $v): ?>
        <?php if (!isset($v['content'])): continue; endif; ?>
        <?php if (!isset($v['url'])): continue; endif; ?>
        <?php if (!isset($v['text'])): $v['text'] = $v['url']; endif; ?>
    <?php 
    $link = str_replace(array('{:url}', '{:text}'), '%s', $v['content']); 
	//var_dump($v);
    //printf($link, $v['url'], $v['text']);
	echo  '<a href="'.$v['url'].'" target="_BLANK"><span class="fa-stack fa-lg text-info trans">
			  <i class="fa fa-circle fa-stack-2x"></i>
			  <i class="fa fa-'.strtolower($v['text']).' fa-stack-1x fa-inverse"></i>
		  </span></a>';
    ?>
<?php endforeach; ?>
</div>