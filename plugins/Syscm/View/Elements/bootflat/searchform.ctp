<?php
extract($searchform);
?>
<!--
<div id="searchform" class="style1">

    <form method="post" action="<?php echo $this->Html->url($url);?>">
        <div class="row">
            <div class="8u 12u(mobilep)">
                <input type="text" maxlength="200" value="<?php echo h($searchq);?>" name="searchq" class="fit" placeholder="<?php
                echo __d('syscm', 'Search...');
                ?>" />
            </div>
            <div class="1u 12u(mobilep)">
                <button class="button fit" type="submit"><span class='fa fa-search'></span></button>
            </div>
        </div>
    </form>
</div>
-->
<form class="navbar-form navbar-right" role="search" action="<?php echo $this->Html->url($url);?>" method="post">
	   <div class="input-group form-search">
                          <input type="text" maxlength="200" value="<?php echo h($searchq);?>" name="searchq" class="form-control search-query" placeholder="<?php
                echo __d('syscm', 'Search...');
                ?>" />
                  <span class="input-group-btn">
                    <button type="submit" class="btn btn-primary btn-search" data-type="last"><i class="fa fa-search"></i></button>
                  </span>
       </div>
</form>