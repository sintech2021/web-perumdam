<?php if($this->request->action == 'index'):return;endif;?>
<section>
    <div class="row">
	<br>
		<div class="col-md-12">
        <ol class="breadcrumb breadcrumb-arrow">
            <li><?php
        echo $this->Html->link('<i class="fa fa-home"></i>',$this->Html->url(array('action'=> 'index'), true), array('escape'=> false, 'title' => __d('syscm','Home')));
        ?></li>
            <?php
            $breadcrumbs = $this->get('breadcrumbs', array());
            if($breadcrumbs):
                foreach($breadcrumbs as $v):
                    extract($v);
                    ?><li<?php echo isset($v['url']) ? '':" class='active'";?>><?php
                    $url = isset($v['url']) ?  $v['url'] : "#";
                    $title = isset($title) ?  h($title) : "";
                    $text = $title;
                    $len = 30;
                    if(strlen($title) > $len):
                        $title = substr($title,0,$len);
                        $title .= '...';
                    endif;
                    
					echo isset($v['url'])  ? $this->Html->link($title,$this->Html->url($url,true),array('escape'=> false)) : "<span>".$title."</span>";?></li>
					<?php
                endforeach;
            endif;
            ?>
        </ol>
        
		</div>
    </div>
        
</section>