
	 <div class="header-button-sm row clearfix">
		<div class="col-xs-12">
		<button type="button" class="navbar-toggle offcanvas-toggle" data-toggle="offcanvas" data-target="#nav" style="float:left;">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
		<h6 class='visible-xs'><a href="<?php echo $this->Html->url(array(
    'plugin' => 'syscm', 'controller' => 'syscm_front', 'action' => 'index')); ?>">&nbsp;<?php echo Configure::read('Sysconfig.company.name'); ?></a></h6>
		</div>
	 </div>
    <!-- Logo -->
    <div id="logo" class="row">
	
		<div class="col-md-12" id="header">
			 <div class="col-xs-4 col-xs-offset-4 col-sm-2 col-sm-offset-0 col-lg-2 col-lg-offset-0">
				<?php
                $logo_path = isset($logo_path) ? $logo_path : '';
                echo $this->Html->image($logo_path,array("class"=>"img-responsive"));
                ?>
			</div>
			<div class="col-xs-12 col-sm-10 col-sm-offset-0 col-lg-10 col-lg-offset-0 hidden-xs">
			
			<h1 style="vertical-align: top;" class="brand"><a><?php echo Configure::read('Sysconfig.company.name'); ?></a><br><small class="text-primary">KABUPATEN TANGERANG</small></h1>
			</div>
		</div>
        
    </div>
    