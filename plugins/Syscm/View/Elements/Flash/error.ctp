<div id="flash-<?php echo isset($key) ? h($key) : CakeString::uuid(); ?>" class="alert alert-danger">
    <?php echo h($message) ?>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>