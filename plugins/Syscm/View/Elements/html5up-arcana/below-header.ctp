<?php
if(!isset($below_header)):
    ?><div id="below-header"></div><?php
    return;
endif;
?>
<div id="below-header">
    
    <section class="wrapper style3" style="padding:10px 10px 10px 10px;">
        <div class="row">
            
            <section class="6u 12u(narrower)">
                <div class="row">
                    <div class="2u"></div>
                    <div class="5u 12u(narrower)">
                        <a class="button fit" style="font-size:0.8em;" href="<?php echo $this->Html->url(array('action'=>'cust_register'), true);?>"><span class="fa fa-pencil-square-o"></span> <?php echo __d('syscm','Customer Registration');?></a>
                    </div>
                    <div class="5u 12u(narrower)">
                        <a class="button fit red-blink" style="font-size:0.8em;" href="<?php echo $this->Html->url(array('action'=>'cust_bill'), true);?>"><span class="fa fa-info-circle"></span> <?php echo __d('syscm','Billing Info');?></a>
                    </div>
                </div>
            </section>
            <section class="6u 12u(narrower)">
            <?php
                echo isset($below_header['searchform']) ?  $this->element('Syscm.' . $theme_alias . '/searchform', array('searchform' => $below_header['searchform'])):"";
                ?>
            </section>
        </div>
    </section>
</div>