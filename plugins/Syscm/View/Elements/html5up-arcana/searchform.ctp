<?php
extract($searchform);
?>
<div id="searchform" class="style1">

    <form method="post" action="<?php echo $this->Html->url($url);?>">
        <div class="row uniform 50%">
            <div class="8u 12u(mobilep)">
                <input type="text" maxlength="100" value="<?php echo h($searchq);?>" name="searchq" class="fit" placeholder="<?php
                echo __d('syscm', 'Search...');
                ?>" />
            </div>
            <div class="1u 12u(mobilep)">
                <button class="button fit" type="submit"><span class='fa fa-search'></span></button>
            </div>
        </div>
    </form>
</div>