<?php if($this->request->action == 'index'):return;endif;?>
<section class="wrapper style1 padding-0">
    <div class="container container-compact">
        <ul class="breadcrumb">
            <li> <span class="fa fa-hand-o-up fa-rotate-90"></span> </li>
            <li> <span class="fa fa-angle-right"></span> <?php
        echo $this->Html->link('<span class="fa fa-home"></span> ',$this->Html->url(array('action'=> 'index'), true), array('escape'=> false, 'title' => __d('syscm','Home')));
        ?></li>
            <?php
            $breadcrumbs = $this->get('breadcrumbs', array());
            if($breadcrumbs):
                
                foreach($breadcrumbs as $v):
                    extract($v);
                    ?><li> <span class="fa fa-angle-right"></span> <?php
                    $url = isset($url) ?  $url : "#";
                    $title = isset($title) ?  h($title) : "";
                    $text = $title;
                    $len = 30;
                    if(strlen($title) > $len):
                        $title = substr($title,0,$len);
                        $title .= '...';
                    endif;
                    echo $url ? $this->Html->link($title, $this->Html->url($url, true), array('title'=> $text)) : $title;
                    ?> </li><?php
                endforeach;
            endif;
            ?>
        </ul>
        
    </div>
        
</section>