<title>
    <?php echo h($title_for_layout); ?> :: <?php echo Configure::read('Sysconfig.app.desc'); ?>
</title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<?php
$syscm_meta_desc    = isset($syscm_meta_desc) ? h($syscm_meta_desc) : Configure::read('Sysconfig.app.desc');
$syscm_meta_author  = isset($syscm_meta_author) ? h($syscm_meta_author) : Configure::read('Sysconfig.author.name');
$syscm_meta_keyword = isset($syscm_meta_keyword) ? h($syscm_meta_keyword) : Configure::read('Sysconfig.meta.keyword');
echo $this->Html->meta(null, null,
        array('name' => 'author', 'content' => $syscm_meta_author));
echo $this->Html->meta('description', $syscm_meta_desc);
echo $this->Html->meta(null, null,
        array('name' => 'keyword', 'content' => $syscm_meta_keyword));
$icon_path          = isset($icon_path) ? $icon_path : '/favicon.ico';
echo $this->Html->meta('favicon.ico', $icon_path, array('type' => 'icon'));
echo $this->fetch('meta');
$fb_share           = $this->get('fb_share', array());
if ($fb_share):
    if (isset($fb_share['meta'])):
        foreach ($fb_share['meta'] as $i => $v):
            printf('<meta property="%s" content="%s" />', $i, $v);
        endforeach;
    endif;
endif;
?>
<!--[if lte IE 8]><?php
echo $this->Html->script('Syscm./' . $theme_alias . '/assets/js/ie/html5shiv');
echo $this->fetch('script');
?><![endif]-->
<?php
echo $this->Html->css(array('Syscm./' . $theme_alias . '/assets/css/main'));
?>
<!--[if lte IE 8]><?php
echo $this->Html->css(array('Syscm./' . $theme_alias . '/assets/css/ie8'));
?><![endif]-->
<!--[if lte IE 9]><?php
echo $this->Html->css(array('Syscm./' . $theme_alias . '/assets/css/ie8'));
?><![endif]-->
<?php
echo $this->Html->css(array('Syscm./' . $theme_alias . '/assets/css/main-override'));
if ($this->get('use_sss', false)):
    echo $this->Html->css(array('sss', 'simplemodal'));
endif;
if (isset($enqueued_css)):
    echo $this->Html->css(array($enqueued_css));

endif;
echo $this->fetch('css');
$static_header_bg = $this->get('static_header_bg', false);
if ($static_header_bg):
    ?><style type="text/css">
        #header {
            background-image: url("<?php echo $this->Html->url($static_header_bg); ?>");
           /* background-image: -moz-linear-gradient(center top , transparent, rgba(0, 0, 0, 0.15)), url("<?php echo $this->Html->url($static_header_bg); ?>");*/
        }

    </style><?php
endif;
/**
 * background-image: url("images/banner_pdam.png");
  background-image: -moz-linear-gradient(center top , transparent, rgba(0, 0, 0, 0.15)), url("images/banner_pdam.png");
 */
?>