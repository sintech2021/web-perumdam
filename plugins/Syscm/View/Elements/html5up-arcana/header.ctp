<div id="header">
    
    <!-- Logo -->
    <div id="logo">
        <h1 style="vertical-align: top;"><a href="<?php echo $this->Html->url(array(
    'plugin' => 'syscm', 'controller' => 'syscm_front', 'action' => 'index')); ?>"><?php
                $logo_path = isset($logo_path) ? $logo_path : '';
                echo $this->Html->image($logo_path);
                ?>&nbsp;<?php echo Configure::read('Sysconfig.company.name'); ?></a></h1>
    </div>
    <!-- Nav -->
    <?php
    echo $this->element('Syscm.' . $theme_alias . '/nav');
    ?>

    <!-- ./Nav -->


</div>
<?php if (isset($featured_contents) || 1==1) :
    echo $this->element('Syscm.' . $theme_alias . '/featured-contents');
    endif; ?>
