<nav id="nav">
    <?php
    //$syscm_main_nav = $this->fetch('syscm_main_nav', array());
    ?>
    <ul>
        <?php
        if (isset($syscm_main_nav)) {
            foreach ($syscm_main_nav as $i => $v):

                $li_class        = ''; // current ::> active
                $parent_selected = false;
                $childs          = "";

                if ($v['ChildMenu']) {
                    $childs          = $this->element('Syscm.' . $theme_alias . '/sub-nav',
                            array('syscm_sub_main_nav' => $v['ChildMenu']));
                    $parent_selected = $this->get('parent_selected');
                }
                $text = isset($v['FrontModel']['text']) ? h($v['FrontModel']['text'])
                            : '';
                $url  = isset($v['ScmMenuSlug']['value']) ? $v['ScmMenuSlug']['value']
                            : '#';
                if (isset($v['ScmMenuSlug']['value'])):

                    $li_class = $this->html->url($url) == $selected_slug || $parent_selected
                                ? 'current' : '';
                $parent_selected = false;
                endif;
                ?>
                <li class="<?php echo $li_class; ?>">
                        <?php
                        echo $this->Html->link(__d('syscm', $text), $url);
                        echo $childs;
                        ?>
                </li>
                <?php
            endforeach;
        }
        ?></ul>
</nav>