<?php
$footer_icons = $this->get('footer_icons', array());
if (!$footer_icons): return;
endif;
?><!-- Icons -->
<ul class="icons">
    <?php foreach ($footer_icons as $v): ?>
        <?php if (!isset($v['content'])): continue; endif; ?>
        <?php if (!isset($v['url'])): continue; endif; ?>
        <?php if (!isset($v['text'])): $v['text'] = $v['url']; endif; ?>
    <li><?php 
    $link = str_replace(array('{:url}', '{:text}'), '%s', $v['content']); 
    printf($link, $v['url'], $v['text']);
    ?></li>
<?php endforeach; ?>
</ul>