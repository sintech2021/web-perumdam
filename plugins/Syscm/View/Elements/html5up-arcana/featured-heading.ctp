<?php 
if(!isset($featured_heading)): return; endif;
?>

<section id="featured-heading" class="wrapper style1">
    <div class="container">
        <h3><?php echo isset($featured_heading['title']) ? h($featured_heading['title']) : "";?></h3>
        <p><?php echo isset($featured_heading['summary']) ? filter_var($featured_heading['summary'], FILTER_SANITIZE_STRIPPED) : "";?></p>
    </div>
</section>
