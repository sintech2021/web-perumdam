<?php
$featured_contents = $this->get('featured_contents', array());
if (!$featured_contents): return;
endif;
?>
<section id="cta" class="wrapper style3" style="padding-top:0px;padding-bottom:10px;">
    <div class="container">
        <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:400px;overflow:hidden;visibility:hidden;background:url('img/main_bg.jpg') 50% 50% no-repeat;">
            <!-- Loading Screen -->
            <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
                <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
                <div style="position:absolute;display:block;background:url('img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
            </div>
            <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 980px; height: 400px; overflow: hidden;">
                <?php /**/ ?>
                <?php
                foreach ($featured_contents as $v):
                    $content_url = $this->Html->url($v['url'], true);
                    $img_url     = $this->Html->url($v['img_url'], true);
                    ?>
                    <div data-p="172.50" style="display: none;">

                        <img  data-u="image"  src="<?php echo $img_url; ?>" class="12u image"  />
                        <div style=" font-family: verdana;font-weight: normal;position: absolute;width: 100%;height: 45px;bottom: 50px;left: 0px;color: #FFF;line-height: 45px;font-size: 20px;padding-left: 10px; background: rgba(0,0,0,0.60);"><?php echo $this->Html->link($v['title'], $this->Html->url($content_url, true)); ?></div>
                        <img data-u="thumb" src="<?php echo $img_url; ?>" />
                    </div>
                    <?php /*
                      <div data-p="172.50" style="display: none;" style="">

                      <div class="row 200% text-left" style="">

                      <section class="12u">
                      <header>
                      <h2  class="text-center"><?php echo $v['title'];?></h2>

                      </header>
                      <div class="row 200%">
                      <section class="4u">
                      <a href="<?php echo $content_url;?>"><img class="image left 12u" src="<?php echo $img_url;?>"></a>
                      </section>

                      <section class="8u">
                      <strong>
                      <p class="12u " style="color:#fff;font-weight: bolder;"><?php echo $v['summary'];?></p>
                      </strong>
                      <a href="<?php echo $content_url;?>" class="button"><?php echo strtoupper(__d('syscm','read more'));?></a>
                      </section>
                      </div>
                      </section>


                      </div>

                      <img data-u="thumb" src="<?php echo $img_url;?>" />
                      </div> */ ?>
<?php endforeach; ?>
<?php /**/ ?>
            </div>
            <!-- Thumbnail Navigator -->
            <div data-u="thumbnavigator" class="jssort04" style="position:absolute;left:0px;bottom:0px;width:980px;height:60px;" data-autocenter="1">
                <!-- Thumbnail Item Skin Begin -->
                <div data-u="slides" style="cursor: default;">
                    <div data-u="prototype" class="p">
                        <div class="w">
                            <div data-u="thumbnailtemplate" class="t"></div>
                        </div>
                        <div class="c"></div>
                    </div>
                </div>
                <!-- Thumbnail Item Skin End -->
            </div>
            <!-- Arrow Navigator -->
            <span data-u="arrowleft" class="jssora07l" style="top:0px;left:8px;width:50px;height:50px;" data-autocenter="2"></span>
            <span data-u="arrowright" class="jssora07r" style="top:0px;right:8px;width:50px;height:50px;" data-autocenter="2"></span>
            <a href="http://www.jssor.com" style="display:none">Bootstrap Carousel</a>
        </div>

        <!-- #endregion Jssor Slider End -->
    </div>
</section>