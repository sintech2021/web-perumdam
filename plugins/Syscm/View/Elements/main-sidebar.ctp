<div id="sidebar">
    <section class="widget thumbnails">
        <h3><?php echo __d('syscm', 'Check My Billing')?>:</h3>
        <div class="grid">
            <div class="row 50%">
                <input placeholder="<?php echo __d('syscm', 'Customer ID');?>">
            </div>
        </div>
        <a href="#" class="button icon fa-file-text-o">Submit</a>
    </section>
</div>