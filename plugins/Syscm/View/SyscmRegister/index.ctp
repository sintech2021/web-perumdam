<h4 class="sub_title"><?= $this->get('sub_title')?></h4>

<?= $this->get('xcrud')?>


<style type="text/css">
	a[data-task=create]{
		display: none;
	}
	ul.nav.nav-tabs{
		display: none;
	}
	h4.sub_title{
		position: absolute;
		font-weight: bold;
		margin-top: 5px;
	}
	.xcrud-nav{
		padding-left: 0 !important;
		padding-top: 2px !important;
	}
</style>

<script type="text/javascript">
	$('li[role=presentation] > a[href*=add]').hide().click(()=>{
		$('a[data-task=create]').click();
		event.preventDefault();
		return false;
	})
</script>