<?php

$reg=$this->get('reg');
$id=$this->get('id');
$reg_json=$this->get('reg_json');
// print_r($reg_json);

?>
<script type="text/javascript">
	function send_register() {
		let data = <?=json_encode($reg_json)?>;
		 data.id = '<?=$id?>';
		// console.log(data);
		const url = '/syscm/syscm_register/proses';
		if(confirm('Apakah Anda yakin ingin memproses?'))
		$.post(url,data,(r)=>{
			console.log(r);
			document.location=document.location
		},'json');
	}
</script>
<h4>Detail Registrasi</h4>
<table class="table table-hover">
	<tbody>
		<tr><th>Kode Registrasi</th><td>: <?=$reg['kode_pendaftaran']?></td></tr>
		<tr><th>Nama Pemohon</th><td>: <?=$reg_json['pemohon']?></td></tr>
		<tr><th>Nama Pemilik</th><td>: <?=$reg_json['pemilik']?></td></tr>
		<tr><th>Nomor KTP</th><td>: <?=$reg_json['noKtp']?></td></tr>
		<tr><th>Nomor HP</th><td>: <?=$reg_json['noHp']?></td></tr>
		<tr><th>Foto KTP</th><td>: <img src="/uploads/pendaftaran/<?=$reg['foto_ktp']?>"/></td></tr>
		<tr><th>Email</th><td>: <?=$reg['email']?></td></tr>
		<tr><th>Kabupaten</th><td>: <?=$reg['kabupaten']?></td></tr>
		<tr><th>Kecamatan</th><td>: <?=$reg['kecamatan']?></td></tr>
		<tr><th>Kelurahan</th><td>: <?=$reg['kelurahan']?></td></tr>
		<tr><th>Alamat</th><td>: <?=$reg_json['almtPasang']?></td></tr>
		<tr><th>Kode Pos</th><td>: <?=$reg_json['kdPos']?></td></tr>
		<tr><th>Status Registrasi</th><td>: <?=$reg['status']=='0'?'<small>Belum</small> <button class="btn btn-primary" onclick="send_register()"><i class="fa fa-arrow-up"></i> Proses</button>':'Sudah Terkirim'?></td></tr>

	</tbody>
</table>
<style type="text/css">
	a[data-task=create]{
		display: none;
	}
	ul.nav.nav-tabs{
		display: none;
	}
</style>