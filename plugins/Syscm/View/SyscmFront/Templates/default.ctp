<?php if (isset($syscm_widgets)): ?>
    <section class="wrapper style1">
        <div class="container">
            <div class="row 200%">
                <div class="8u 12u(narrower)">
                    <div id="content">
                        <!-- Content -->

                        <?php echo $this->fetch('syscm_contents',
                                __d('syscm', 'Nothing to display.'));
                        ?>
                        <!-- ./Content -->
                    </div>
                </div>
                <div class="4u 12u(narrower)">
                    <div id="sidebar">

                        <!-- Sidebar -->
                        <!-- ./Sidebar -->

                    </div>
                </div>
            </div>
        </div>
    </section>
<?php else: ?>
    <section class="wrapper style1">
        <div class="container">
            <div id="content">

                <!-- Content -->
                <?php
                $nothing = true;
                if (isset($syscm_contents)):
                    if ($syscm_contents):
                        $nothing = false;
                    endif;
                endif;
                if ($nothing):
                    ?><article>
                        <header>
                            <h2><?php echo __d('syscm', 'Content Unavailable.'); ?></h2>
                            <p><?php echo __d('syscm', 'Nothing to display.'); ?></p>
                        </header>
                    </article><?php
                else:
                    echo $this->SyscmArticle->display($syscm_contents);
                endif;
                ?>

                <!-- ./Content -->

            </div>
        </div>
    </section>
<?php endif; ?>