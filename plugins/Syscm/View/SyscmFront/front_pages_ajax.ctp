<?php
$segment_key = $this->get('segment_key');



switch ($segment_key) {
	case 'web.tentang_kami.jajaran_direksi':
		$syscm_template = 'jajaran_direksi';

		break;
	case 'web.kontak_kami':
	case 'kontak_kami':
		$syscm_template = 'kontak_kami';
		break;
	case 'web.tentang_kami.struktur_organisasi':
		$syscm_template = 'struktur_organisasi';
		break;
	case 'web.layanan_informasi.kantor_layanan':
		$syscm_template = 'kantor_layanan';
		break;	
	case 'organisasi':
		$syscm_template = 'organisasi';
		break;		
	default:
		if(preg_match('/web\.tentang_kami/',$segment_key)){
			$syscm_template = 'tentang_kami';
			$this->set('segment_key','tentang_kami');
		}else if(preg_match('/web\.layanan_informasi/',$segment_key)){
			$syscm_template = 'layanan_informasi';
			$this->set('segment_key','layanan_informasi');
		}else if(preg_match('/web\.laboratorium/',$segment_key)){
			$syscm_template = 'laboratorium';
			$this->set('segment_key','laboratorium');
		}else if(preg_match('/web\.ppid/',$segment_key)){
			$syscm_template = 'ppid';
			$this->set('segment_key','ppid');
		}

		else{ 
			$syscm_template = $this->get('syscm_template', 'default');
		}

		break;
}
// die($syscm_template);
echo $this->element('Syscm.../SyscmFront/Templates/ajax/'.$syscm_template);
