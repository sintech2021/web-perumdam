<div class="container">
    <div class="row"><h2>Kantor Wilayah Pelayanan</h2></div>
                    <div class="row">
                        <section class="3u 6u(narrower) 12u(mobilep)">
                            <h3>
Kantor Wilayah Pelayanan I
</h3>
    <ul>
    <li>
Jl. Ki Samaun No. 204 Tangerang
    </li>
    <li>
Telp. (021) 5523338 & 55794863
    </li>
    </ul>
                            
                            <h3>
Kantor Wilayah Pelayanan II
</h3>
    <ul>
    <li>
Jl. M. Toha Pasar Baru Tangerang
    </li>
    <li>
Telp. (021) 5512298
    </li>
    </ul>
                            
                            <h3>
Kantor Wilayah Pelayanan III
</h3>
    <ul>
    <li>
Jl. Cendrawasih Raya No. 76 Perumnas Tng
    </li>
    <li>
Telp. (021) 5536814
    </li>
    </ul>
                        </section>
                        <section class="3u 6u(narrower) 12u(mobilep)">
                            <h3>
Kantor Cabang Serpong
</h3>
    <ul>
    <li>
Jl. Raya Puspiptek No. 1-2 Serpong Tangerang
    </li>
    <li>
Telp. (021) 7563129
    </li>
    </ul>
<h3>
Kantor Cabang Tigaraksa
</h3>
    <ul>
    <li>
Kp. Panggang No. 3 Selapajang Cisoka
    </li>
    <li>
Telp. (021) 59450706
    </li>
    </ul>
<h3>
Kantor Cabang Teluknaga
</h3>
    <ul>
    <li>
Jl. Raya Kampung Melayu Gg. Teladan VII
    </li>
    <li>
Kp. Melayu Timur Teluk Naga
    </li>
    <li>
Telp. (021) 55931947 & 55953696
    </li>
    </ul>
                        </section>
                        <section class="6u 12u(narrower)">
                            <h3>Hubungi Kami</h3>
                            <form method="post">
                                <div class="row 50%">
                                    <div class="6u 12u(mobilep)">
                                        <input type="text" name="name" id="name" placeholder="Name" />
                                    </div>
                                    <div class="6u 12u(mobilep)">
                                        <input type="email" name="email" id="email" placeholder="Email" />
                                    </div>
                                </div>
                                <div class="row 50%">
                                    <div class="12u">
                                        <textarea name="message" id="message" placeholder="Message" rows="5"></textarea>
                                    </div>
                                </div>
                                <div class="row 50%">
                                    <div class="12u">
                                        <ul class="actions">
                                            <li><input type="submit" class="button alt" value="Send Message" /></li>
                                        </ul>
                                    </div>
                                </div>
                            </form>
                        </section>
                    </div>
                </div>

