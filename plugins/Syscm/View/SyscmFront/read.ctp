<?=$this->element("Syscm.../SyscmFront/Widgets/root_with_progress_bar");?>

    
        <script type="text/javascript">
        site_url = ()=>{
            return '<?=BASE_URL?>';
    
        }
        <? 
        $article = $this->get('article',array());
        $featured_contents = $this->get('featured_contents');
        $sidelist_right = $this->get('sidelist_right');
        $top_mainlist = $this->get('top_mainlist');
        $article = $this->get('article');
        $article_breadcrumbs = $this->get('article_breadcrumbs');
        $data = $this->get('data');
        // print_r(json_encode($featured_contents));
        ?>
        app_data = {
            menu : <?=json_encode($syscm_main_nav,JSON_PRETTY_PRINT)?>,
            carousel : <?=json_encode($featured_contents,JSON_PRETTY_PRINT)?>,
            sidelist_right : <?=json_encode($sidelist_right,JSON_PRETTY_PRINT)?>,
            top_mainlist : <?=json_encode($mainlist,JSON_PRETTY_PRINT)?>,
            article : <?=json_encode($article,JSON_PRETTY_PRINT)?>,
            article_breadcrumbs : <?=json_encode($article_breadcrumbs,JSON_PRETTY_PRINT)?>,
            data : <?=json_encode($data,JSON_PRETTY_PRINT)?>,
            formatted_slug : '<?=empty($article)?'404': $this->get('segment_key')?>',
        };
    </script>
        <!-- Bootstrap core CSS -->
    <link href="<?=BASE_URL.'/'?>node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"  crossorigin="anonymous">
    <link href="<?=BASE_URL.'/'?>node_modules/opensans-npm-webfont/style.css" rel="stylesheet"  crossorigin="anonymous">
     <link href="<?=BASE_URL.'/'?>node_modules/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet"  crossorigin="anonymous">
    <link href="<?=BASE_URL.'/'?>node_modules/nunito-fontface/css/nunito/nunito-fontface.css" rel="stylesheet"  crossorigin="anonymous">
    <!-- Custom styles for this template -->
        <link href="<?=BASE_URL.'/'?>styles.css?v=<?=gen_uuid()?>" rel="stylesheet">
        <link href="<?=BASE_URL.'/'?>bootstrap-overide.css" rel="stylesheet">
    
       <script type="text/babel">
          class SinglePost extends React.Component{
            
            render(){
              // ////////////////////////////
              let post = '';
              <?php
                $_data = [
                  't' => 'post',
                  'content' => ''
                ];
                if($article):
                    $this->set('article',$article);
                    $_data['content'] = $this->element("Syscm.../SyscmFront/Widgets/article");
                else:
                ?>
              post = `<article>
                    <header>
                        <h2><?php echo __d('syscm', 'Content Unavailable.'); ?></h2>
                        <p><?php echo __d('syscm', 'Nothing to display.'); ?></p>
                    </header>
                </article>`;
                <?php        
                endif;
                $_data['content'] .= $this->element("Syscm.../SyscmFront/Widgets/article_comment");
                ?>
              const data = <?=json_encode($_data)?>  
              if(data.content == ''){
                data.content = post;
              }
              //////////////////////////////
              const content_id = '<?=$this->get('content_id')?>';
              let comment_enabled = <?=isset($article['comment_enabled'])?($article['comment_enabled']?1:0):0;?>;
              comment_enabled = comment_enabled == 1 ? true : false;
              return (
                <div className="container">
                  <div className="content content-main" style={{padding:'0'}}>
                    <div className="article" dangerouslySetInnerHTML={{__html:data.content}}></div>
                    <div className="article-comment-cnt">
                    {comment_enabled && (<CommentBox id={content_id} ref={(element) => {window.comentBoxComponent = element}}/>)}
                    </div>
                  </div>
                </div>
              )
            }
          }
           </script>  
    <?if(ENABLE_CDN):?>
       
    <script src="https://code.jquery.com/jquery-3.1.0.min.js"  crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" crossorigin="anonymous"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.min.js" crossorigin="anonymous"></script> 
    <script crossorigin src="https://unpkg.com/react@16/umd/react.production.min.js"></script>
    <script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.production.min.js"></script>
    <script crossorigin src="https://cdn.jsdelivr.net/npm/@babel/standalone@7.11.3/babel.min.js"></script>
    <?else:?>
     <script src="<?=BASE_URL.'/'?>jquery-3.5.1.slim.min.js"  crossorigin="anonymous"></script>
  <script src="<?=BASE_URL.'/'?>node_modules/bootstrap/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script> 
  <script src="<?=BASE_URL.'/'?>node_modules/axios/dist/axios.min.js" crossorigin="anonymous"></script> 
  <script src="<?=BASE_URL.'/'?>node_modules/react/umd/react.production.min.js" crossorigin="anonymous"></script> 
  <script src="<?=BASE_URL.'/'?>node_modules/react-dom/umd/react-dom.production.min.js" crossorigin="anonymous"></script> 
  <script crossorigin src="<?=BASE_URL.'/'?>node_modules/@babel/standalone/babel.min.js"></script>
    <?endif?>
      <script  type="text/javascript" src="<?=BASE_URL.'/'?>com/app/Proxy.js"></script> 
      <script  type="text/javascript" src="<?=BASE_URL.'/'?>com/app/Store.js"></script> 
      <script  type="text/babel" src="<?=BASE_URL.'/'?>com/MyNews.js" crossorigin="anonymous"></script> 
      <script  type="text/babel" src="<?=BASE_URL.'/'?>com/MyCompany.js" crossorigin="anonymous"></script> 
      <script  type="text/babel" src="<?=BASE_URL.'/'?>com/Breadcrumbs.js?v=<?gen_uuid()?>" crossorigin="anonymous"></script> 
      <script  type="text/babel" src="<?=BASE_URL.'/'?>com/SearchBar.js" crossorigin="anonymous"></script> 
      <script  type="text/babel" src="<?=BASE_URL.'/'?>com/Footer.js" crossorigin="anonymous"></script> 
      <script  type="text/babel" src="<?=BASE_URL.'/'?>com/MenuItem.js" crossorigin="anonymous"></script> 
      <script  type="text/babel" src="<?=BASE_URL.'/'?>com/Header.js" crossorigin="anonymous"></script> 
      <script  type="text/babel" src="<?=BASE_URL.'/'?>com/CommentBox.js?v=<?=gen_uuid()?>" crossorigin="anonymous"></script> 
      <script  type="text/babel" src="<?=BASE_URL.'/'?>com/PerumdamtkrWebSingle.js" crossorigin="anonymous"></script> 
      <script  type="text/babel" src="<?=BASE_URL.'/'?>com/index.web.js" crossorigin="anonymous"></script> 
    
      <script type="text/babel">
        ReactDOM.render(
            <React.StrictMode>
                <PerumdamtkrWebSingle/>
            </React.StrictMode>, 
        document.getElementById('root'));
      </script>