<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="generator" content="">
    <link rel="icon" href="/favicon.ico" type="image/icon"/>
    <title><?= h($title_for_layout); ?> <?= isset($subtitle_for_layout) ? '-' .h($subtitle_for_layout):'';?></title>
    <?php if(defined('ENABLE_G_CAPTCHA')):?>
      <?php if(ENABLE_G_CAPTCHA):?>
        <script src="https://www.google.com/recaptcha/api.js?hl=id"></script>
      <?php endif?>  
    <?php endif?>  
<?if(!ENABLE_CDN):?>  
  <script src="<?=BASE_URL.'/'?>jquery-3.5.1.slim.min.js"  crossorigin="anonymous"></script>
  <script type="text/javascript" src="<?=BASE_URL.'/'?>fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
  <script type="text/javascript" src="<?=BASE_URL.'/'?>fancybox/jquery.fancybox-1.3.4.js"></script>
  <link rel="stylesheet" type="text/css" href="<?=BASE_URL.'/'?>fancybox/jquery.fancybox-1.3.4.css" media="screen" />

<?else:?>
<script src="https://code.jquery.com/jquery-3.1.0.min.js"  crossorigin="anonymous"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css" media="screen" />
<?endif?>

  </head>
  <body>

  <div class="modal fade" id="popup_modal" role="dialog"> <div class="modal-dialog modal-lg"> <div class="modal-content"> <div class="modal-body"> </div> </div> </div> </div>
    <?= $this->fetch('content'); ?>

    </body>
</html>
 