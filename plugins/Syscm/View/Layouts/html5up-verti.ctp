<!DOCTYPE HTML>
<!--
        Verti by HTML5 UP
        html5up.net | @n33co
        Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
    <head>
        <title>
            <?php echo h($title_for_layout); ?> :: <?php echo Configure::read('Sysconfig.app.desc'); ?>
        </title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <?php
         echo $this->Html->meta('description',
                Configure::read('Sysconfig.app.desc'));
        echo $this->Html->meta(null, null,
                array('name' => 'author', 'content' => Configure::read('Sysconfig.author.name')));
        $icon_path = isset($icon_path) ? $icon_path : '/favicon.ico';
        echo $this->Html->meta(
                'favicon.ico', $icon_path, array('type' => 'icon')
        );
        echo $this->fetch('meta');
        ?>
        <!--[if lte IE 8]><?php
        echo $this->Html->script('Syscm./html5up-verti/assets/js/ie/html5shiv');
        echo $this->fetch('script');
        ?><![endif]-->
        <?php
        echo $this->Html->css(array('Syscm./html5up-verti/assets/css/main'));
        echo $this->fetch('css');
        ?>
        <!--[if lte IE 8]><?php
        echo $this->Html->css(array('Syscm./html5up-verti/assets/css/ie8'));
        echo $this->fetch('css');
        ?><![endif]-->
        <?php
        if(isset($enqueued_css)):
            echo $this->Html->css(array($enqueued_css));
            echo $this->fetch('css');
        endif;
        ?>
    </head>
    <body class="homepage">
        <div id="page-wrapper">

            <!-- Header -->
            <div id="header-wrapper">
                <header id="header" class="container">

                    <!-- Logo -->
                    <div id="logo"><span><a href="/"><?php 
                    $logo_path = isset ($logo_path) ? $logo_path : '';
                    echo $this->Html->image($logo_path, array('width'=>'70px'));?></a><?php echo Configure::read('Sysconfig.company.name');?></span>
                    </div>

                    <!-- Nav -->
                    <?php
                    echo $this->element('Syscm.nav');
                    ?>
                   
                    <!-- ./Nav -->
                    

                </header>
            </div>

            <!-- Banner -->
            <!-- ./Banner -->
            
            <!-- Features -->
            <!-- ./Features -->
            

            <!-- Main -->
            <?php echo $this->element('Syscm.main');?>
            <!-- ./Main -->
            

            <!-- Footer -->
            <?php echo $this->element('Syscm.footer');?>
            <!-- ./Footer -->
            

        </div>

        <!-- Scripts -->
<?php
        $script_ls = array(
            'Syscm./html5up-verti/assets/js/jquery.min',
            'Syscm./html5up-verti/assets/js/jquery.dropotron.min',
            'Syscm./html5up-verti/assets/js/skel.min',
            'Syscm./html5up-verti/assets/js/util',
        );
        echo $this->Html->script($script_ls);
        echo $this->fetch('script');
        ?><!--[if lte IE 8]><?php
        echo $this->Html->script(array('Syscm./html5up-verti/assets/js/ie/respond.min'));
        echo $this->fetch('script');
        ?><![endif]--><?php
        echo $this->Html->script(array('Syscm./html5up-verti/assets/js/main'));
        echo $this->fetch('script');
        if (isset($enqueued_script)) {
            echo $this->Html->script($enqueued_script);
            echo $this->fetch('script');
        }
        
        ?></body>
</html>