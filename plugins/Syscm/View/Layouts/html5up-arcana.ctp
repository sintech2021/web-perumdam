<?php
/**
 * 
 */
?><!DOCTYPE HTML>
<!--
        Arcana by HTML5 UP
        html5up.net | @n33co
        Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
    <head>
        <?php
        echo $this->element('Syscm.' . $theme_alias . '/html-head');
        ?>
    </head>
    <body><?php
    $fb_share = $this->get('fb_share', array());
    if($fb_share):
        ?><!-- Load Facebook SDK for JavaScript -->
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/<?php echo Configure::read('Sysconfig.share.fb.appLang');?>/sdk.js#appId=<?php echo Configure::read('Sysconfig.share.fb.appId');?>&xfbml=1&version=<?php echo Configure::read('Sysconfig.share.fb.version');?>";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
        
        <script async defer src="//assets.pinterest.com/js/pinit.js"></script>
        <link rel="canonical" href="<?php echo $fb_share['meta']['og:url']; ?>" />
        
        <script>
      window.___gcfg = {
        lang: 'id-ID',
        parsetags: 'onload'
      };
    </script>

        <script src="https://apis.google.com/js/platform.js" async defer></script>
            <?php
    endif;
    ?>
        <div id="page-wrapper" style="min-height: 830px;">
            <!-- Header -->
            <?php
            echo $this->element('Syscm.' . $theme_alias . '/header');
            ?>
            <!-- ./Header -->
            <!-- below-header -->
            <?php
            echo $this->element('Syscm.' . $theme_alias . '/below-header');
            ?>
            <!-- ./below-header -->
            
            <!-- featured-header -->
            <?php
            echo $this->element('Syscm.' . $theme_alias . '/featured-heading');
            ?>
            <!-- ./featured-header -->
            <?php echo $this->Flash->render(); ?>
            <!-- breadcrumbs -->
            <?php
            echo $this->element('Syscm.' . $theme_alias . '/breadcrumbs');
            ?>
            <!-- ./breadcrumbs -->
            
            <!-- content -->
            
            <?php echo $this->fetch('content'); ?>
            <!-- ./content -->
            <!-- Footer -->
            <?php
            echo $this->element('Syscm.' . $theme_alias . '/footer');
            ?>
            <!-- ./Footer -->
        </div>
        <!-- Scripts -->
        <?php
        $script_ls = array(
            'Syscm./' . $theme_alias . '/assets/js/jquery.min',
            'Syscm./' . $theme_alias . '/assets/js/jquery.dropotron.min',
            'Syscm./' . $theme_alias . '/assets/js/skel.min',
            'Syscm./' . $theme_alias . '/assets/js/util',
        );
        if($this->get('use_sss', false)):
            $script_ls[] = 'sss/sss.min';
            $script_ls[] = 'sss_init';
            $script_ls[] = 'simplemodal/jquery.simplemodal.1.4.4.min';
            $script_ls[] = 'simplemodal_init';
        endif;
        echo $this->Html->script($script_ls);
        echo $this->fetch('script');
        ?><!--[if lte IE 8]><?php
        echo $this->Html->script(array('Syscm./' . $theme_alias . '/assets/js/ie/respond.min'));
        echo $this->fetch('script');
        ?><![endif]--><?php
        echo $this->Html->script(array('Syscm./' . $theme_alias . '/assets/js/main'));
        
        
        echo $this->Html->script(array('Syscm./' . $theme_alias . '/assets/js/main-override'));
        echo $this->fetch('script');
        if (isset($enqueued_script)) {
            echo $this->Html->script($enqueued_script);
            echo $this->fetch('script');
        }
        ?>
    </body>
</html>
<?php
$syscm_contents = $this->get('syscm_contents', 'No Content');
#debug($syscm_contents);
?>