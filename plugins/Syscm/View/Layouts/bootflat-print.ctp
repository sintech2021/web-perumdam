<?php
/**
 * 
 */
?><!DOCTYPE HTML>
<!--
        Arcana by HTML5 UP
        html5up.net | @n33co
        Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
    <head>
        <?php
        echo $this->element('Syscm.' . $theme_alias . '/html-head');
        ?>
    </head>
    <body class="body-print">
        <div id="page-wrapper" class="container page-boxed">
            <!-- Header -->
            <?php
            echo $this->element('Syscm.' . $theme_alias . '/header-print');
            ?>
            <!-- ./Header -->
            
            
            <!-- content -->
            
            <?php echo $this->fetch('content'); ?>
            <!-- ./content -->
            <!-- Footer -->
            <?php
            #echo $this->element('Syscm.' . $theme_alias . '/footer');
            ?>
            <!-- ./Footer -->
        </div>
		
		
		
		
        <!-- Scripts -->
        <?php
        $script_ls = array(
            'Syscm./' . $theme_alias . '/js/jquery.min',
            'Syscm./' . $theme_alias . '/js/jquery.dropotron.min',
            
        );
		
		
		?>
		<?php
        
        echo $this->Html->script($script_ls);
        echo $this->fetch('script');
        ?><!--[if lte IE 8]><?php
        echo $this->Html->script(array('Syscm./' . $theme_alias . '/assets/js/ie/respond.min'));
        echo $this->fetch('script');
        ?><![endif]--><?php
        //echo $this->Html->script(array('Syscm./' . $theme_alias . '/assets/js/main'));
        echo $this->Html->script(array('Syscm./' . $theme_alias . '/js/icheck.min'));
        echo $this->Html->script(array('Syscm./' . $theme_alias . '/js/jquery.fs.selecter.min'));
        echo $this->Html->script(array('Syscm./' . $theme_alias . '/js/jquery.fs.stepper.min'));
		echo $this->Html->script(array('Syscm./' . $theme_alias . '/js/offcanvas'));
        
        
        echo $this->Html->script(array('Syscm./' . $theme_alias . '/js/main-override'));
        echo $this->fetch('script');
        if (isset($enqueued_script)) {
            echo $this->Html->script($enqueued_script);
            echo $this->fetch('script');
        }
		echo $this->Html->script(array('/bootstrap-3.3.5-dist/js/bootstrap.min'));
		echo $this->Html->script(array('/js/jquery.blueimp-gallery.min'));
		echo $this->Html->script(array('/js/bootstrap-image-gallery.min'));
        ?>
    </body>
</html>
<?php
$syscm_contents = $this->get('syscm_contents', 'No Content');
#debug($syscm_contents);
?>