<?php
/**
 * 
 */
?><!DOCTYPE HTML>
<!--
        Arcana by HTML5 UP
        html5up.net | @n33co
        Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
    <head>
        <?php
        echo $this->element('Syscm.' . $theme_alias . '/html-head');
        ?>
    </head>
    <body class="body-offcanvas"><?php
    $fb_share = $this->get('fb_share', array());
    if($fb_share):
        ?><!-- Load Facebook SDK for JavaScript -->
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/<?php echo Configure::read('Sysconfig.share.fb.appLang');?>/sdk.js#appId=<?php echo Configure::read('Sysconfig.share.fb.appId');?>&xfbml=1&version=<?php echo Configure::read('Sysconfig.share.fb.version');?>";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
        
        <script async defer src="//assets.pinterest.com/js/pinit.js"></script>
        <link rel="canonical" href="<?php echo $fb_share['meta']['og:url']; ?>" />
        
        <script>
      window.___gcfg = {
        lang: 'id-ID',
        parsetags: 'onload'
      };
    </script>

        <script src="https://apis.google.com/js/platform.js" async defer></script>
            <?php
    endif;
    ?>
        <div id="page-wrapper" class="container page-boxed">
            <!-- Header -->
            <?php
            echo $this->element('Syscm.' . $theme_alias . '/header');
            ?>
            <!-- ./Header -->
            <!-- below-header -->
            <?php
            echo $this->element('Syscm.' . $theme_alias . '/below-header');
            ?>
            <!-- ./below-header -->
            
            <!-- featured-header -->
            <?php
            echo $this->element('Syscm.' . $theme_alias . '/featured-heading');
            ?>
            <!-- ./featured-header -->
            <?php echo $this->Flash->render(); ?>
            <!-- breadcrumbs -->
            <?php
            echo $this->element('Syscm.' . $theme_alias . '/breadcrumbs');
            ?>
            <!-- ./breadcrumbs -->
            
            <!-- content -->
            
            <?php echo $this->fetch('content'); ?>
            <!-- ./content -->
            <!-- Footer -->
            <?php
            echo $this->element('Syscm.' . $theme_alias . '/footer');
            ?>
            <!-- ./Footer -->
        </div>
		
		
		<?php
            echo $this->element('Syscm.' . $theme_alias . '/homepage-popup');
            ?>	
		
		
		<!-- The Bootstrap Image Gallery lightbox, should be a child element of the document body -->
		<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-use-bootstrap-modal="false">
			<!-- The container for the modal slides -->
			<div class="slides"></div>
			<!-- Controls for the borderless lightbox -->
			<h3 class="title"></h3>
			<a class="prev">‹</a>
			<a class="next">›</a>
			<a class="close">×</a>
			<a class="play-pause"></a>
			<ol class="indicator"></ol>
			<!-- The modal dialog, which will be used to wrap the lightbox content -->
			<div class="modal fade">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" aria-hidden="true">&times;</button>
							<h4 class="modal-title"></h4>
						</div>
						<div class="modal-body next"></div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default pull-left prev">
								<i class="glyphicon glyphicon-chevron-left"></i>
								Previous
							</button>
							<button type="button" class="btn btn-primary next">
								Next
								<i class="glyphicon glyphicon-chevron-right"></i>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		
        <!-- Scripts -->
        <?php
        $script_ls = array(
            'Syscm./' . $theme_alias . '/js/jquery.min',
            'Syscm./' . $theme_alias . '/js/jquery.dropotron.min',
            
        );
		
		
		?>
		<?php
        if($this->get('use_sss', false)):
            $script_ls[] = 'sss/sss.min';
            $script_ls[] = 'sss_init';
        endif;
        echo $this->Html->script($script_ls);
        echo $this->fetch('script');
        ?><!--[if lte IE 8]><?php
        echo $this->Html->script(array('Syscm./' . $theme_alias . '/assets/js/ie/respond.min'));
        echo $this->fetch('script');
        ?><![endif]--><?php
        //echo $this->Html->script(array('Syscm./' . $theme_alias . '/assets/js/main'));
        echo $this->Html->script(array('Syscm./' . $theme_alias . '/js/icheck.min'));
        echo $this->Html->script(array('Syscm./' . $theme_alias . '/js/jquery.fs.selecter.min'));
        echo $this->Html->script(array('Syscm./' . $theme_alias . '/js/jquery.fs.stepper.min'));
		echo $this->Html->script(array('Syscm./' . $theme_alias . '/js/offcanvas'));
        
        
        echo $this->Html->script(array('Syscm./' . $theme_alias . '/js/main-override'));
        echo $this->fetch('script');
        if (isset($enqueued_script)) {
            echo $this->Html->script($enqueued_script);
            echo $this->fetch('script');
        }
		echo $this->Html->script(array('/bootstrap-3.3.5-dist/js/bootstrap.min'));
		echo $this->Html->script(array('/js/jquery.blueimp-gallery.min'));
		echo $this->Html->script(array('/js/bootstrap-image-gallery.min'));
        ?>
    </body>
</html>
<?php
$syscm_contents = $this->get('syscm_contents', 'No Content');
#debug($syscm_contents);
?>