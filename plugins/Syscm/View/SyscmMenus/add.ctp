<div class="scm_menu form">
    <?php 
    echo $this->element('Sysadmin.HtmlForm/form_create', array('model_alias'=>'Syscm.ScmMenuGroup'));
    
    ?>
    <fieldset>
        <?php 
        echo $this->Form->input('name',
                        array(
                    'label' => array(
                        'text'  => __d('sysadmin', 'Group Name')." *)",
                        'class' => 'col-lg-2 col-sm-2 control-label text-warning',
                        'maxlength'=> '100',
                    )
                ));
         
         echo $this->Form->input('text',
                        array(
                            'type' => 'text',
                    'label' => array(
                        'text'  => __d('sysadmin', 'Location'),
                        'class' => 'col-lg-2 col-sm-2 control-label',
                        'maxlength'=> '100',
                    )
                ));
         echo $this->Form->input('annotation',
                        array(
                    'label' => array(
                        'text'  => __d('sysadmin', 'Annotation'),
                        'class' => 'col-lg-2 col-sm-2 control-label',
                    )
                ));
          
        ?>
    </fieldset>
    <?php
    echo $this->element('Sysadmin.HtmlForm/form_end', array('submit_label'=>__d('sysadmin', 'Submit')));
     ?>
</div>