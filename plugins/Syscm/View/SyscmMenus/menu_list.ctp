<div class="">
    <?php
    $main_model_alias = "ScmMenuList";
    ?>
    <table class="table table-bordered table-condensed table-striped">
        <thead>
            <tr>
                <?php
                if (isset($SysAcl)):
                    if ($SysAcl['read'] || $SysAcl['edit'] || $SysAcl['delete']):
                        ?>
                        <th class="actions col-lg-1 col-sm-1"><?php echo __d('sysadmin',
                        'Actions'); ?></th>
                        <?php
                    endif;
                endif;
                ?>
                <th>#</th>
                <th><?php echo $this->Paginator->sort($main_model_alias . '.name',
                        __d('syscm', 'Menu ID')); ?></th>
                <th><?php echo $this->Paginator->sort('ScmMenuSlug.value',
                        __d('syscm', 'Slug')); ?></th>
                <th><?php echo $this->Paginator->sort($main_model_alias . '.text',
                        __d('syscm', 'Text')); ?></th>
                <th><?php echo $this->Paginator->sort('ParentMenu.text',
                        __d('syscm', 'Parent')); ?></th>
                <th><?php echo __d('syscm', 'Groups'); ?></th>
                <th><?php echo $this->Paginator->sort($main_model_alias . '.menu_order',
                        __d('syscm', 'Ordering')); ?></th>
                <th><?php echo $this->Paginator->sort($main_model_alias . '.annotation',
                    __d('syscm', 'Annotation')); ?></th>
            </tr>
        </thead>
        <tbody>
                <?php
                if ($data_list):
                    $counter = $this->Paginator->counter('{:start}');
                    foreach ($data_list as $i => $v):
                        ?>
                    <tr>
                            <?php
                            if (isset($SysAcl)):
                                if ($SysAcl['read'] || $SysAcl['edit'] || $SysAcl['delete']):
                                    ?> <td class="actions" nowrap="nowrap">
                                    <?php
                                    if ($SysAcl['edit']):
                                        echo $this->Html->link(
                                                '<span class="fa fa-edit fa-border text-warning"></span>',
                                                array(
                                            'action' => 'menu_edit', $v[$main_model_alias]['id']
                                                ), array('escape' => false)
                                        );
                                    endif;
                                    if ($SysAcl['delete']):
                                        echo $this->Form->postLink(
                                                '<span class="fa fa-trash fa-border text-danger"></span>',
                                                array(
                                            'action' => 'menu_delete', $v[$main_model_alias]['id']
                                                ),
                                                array(
                                            'confirm' => __d('sysadmin',
                                                    'Are you sure you want to delete # %s?',
                                                    h($v[$main_model_alias]['name'])),
                                            'escape'  => false
                                                )
                                        );
                                    endif;
                                    ?>
                                </td><?php
                endif;
            endif;
            ?>
                        <td><?php
                            echo $counter + $i;
                            ?></td>
                        <td><?php echo isset($menu_id_list[$v[$main_model_alias]['id']])? $menu_id_list[$v[$main_model_alias]['id']] : h($v[$main_model_alias]['name']); ?></td>
                        <td><?php echo h($v['ScmMenuSlug']['value']); ?></td>
                        <td><?php echo h($v[$main_model_alias]['text']); ?></td>
                        <td><?php if ($v['ParentMenu']['text'] || $v['ParentMenu']['name']): ?>
                                <?php echo h($v['ParentMenu']['text']); ?> (<?php echo h($v['ParentMenu']['name']); ?>)
        <?php endif; ?>
                        </td>
                        <td><?php if ($v['ScmMenuGroup']): ?>
            <?php foreach ($v['ScmMenuGroup'] as $vv): ?>
                            <?php echo $vv['text'];?>
                            (<?php echo $vv['name'];?>)
                        <?php endforeach; ?>
                    <?php endif; ?>
                        </td>

                        <td><?php echo h($v[$main_model_alias]['menu_order']); ?></td>
                        <td><?php echo h($v[$main_model_alias]['annotation']); ?></td>
                    </tr>
            <?php
        endforeach;
    endif;
    ?>
        </tbody>
    </table>

<?php echo $this->element('paginator', array(),
        array('plugin' => 'sysadmin')); ?>
</div>
<?php
debug($data_list);
?>