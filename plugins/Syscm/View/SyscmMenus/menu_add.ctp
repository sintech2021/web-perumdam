<div class="scm_menu form">
    <?php 
    echo $this->element('Sysadmin.HtmlForm/form_create', array('model_alias'=>'Syscm.ScmMenuList'));
    
    ?>
    <fieldset>
        <?php 
        echo $this->Form->input('ScmMenuList.name',
                        array(
                    'label' => array(
                        'text'  => __d('sysadmin', 'Menu ID')." *)",
                        'class' => 'col-lg-2 col-sm-2 control-label text-warning',
                        'maxlength'=> '100',
                    )
                ));
         
         echo $this->Form->input('ScmMenuSlug.value',
                        array(
                            'type' => 'text',
                    'label' => array(
                        'text'  => __d('sysadmin', 'Menu Slug'),
                        'class' => 'col-lg-2 col-sm-2 control-label',
                        'maxlength'=> '255',
                    )
                ));
         echo $this->Form->input('ScmMenuList.text',
                        array(
                            'type' => 'text',
                    'label' => array(
                        'text'  => __d('sysadmin', 'Menu Text'),
                        'class' => 'col-lg-2 col-sm-2 control-label',
                        'maxlength'=> '100',
                    )
                ));
         echo $this->Form->input('ScmMenuList.parent_id',
                        array(
                           'empty' => true,
               
                    'label' => array(
                        'text'  => __d('sysadmin', 'Menu Parent'),
                        'class' => 'col-lg-2 col-sm-2 control-label',
                        'maxlength'=> '100',
                    )
                ));
         echo $this->Form->input('ScmMenuGroup',
                        array(
                            'multiple' => 'multiple',
                'type'     => 'select',
                'options'  => $scm_menu_groups,
                    'label' => array(
                        'text'  => __d('sysadmin', 'Menu Group'),
                        'class' => 'col-lg-2 col-sm-2 control-label',
                        'maxlength'=> '100',
                    )
                ));
         echo $this->Form->input('ScmMenuList.menu_order',
                        array(
                    'label' => array(
                        'text'  => __d('sysadmin', 'Ordering'),
                        'class' => 'col-lg-2 col-sm-2 control-label',
                    )
                ));
         echo $this->Tinymce->input('ScmMenuList.annotation',
                        array(
                    'label' => array(
                        'text'  => __d('syscm', 'Annotation/Widget'),
                        'class' => 'col-lg-2 col-sm-2 control-label',
                    )
                ), array('selector'=> '#ScmMenuListAnnotation'), 'full-4x');
         
          
        ?>
    </fieldset>
    <?php
    echo $this->element('Sysadmin.HtmlForm/form_end', array('submit_label'=>__d('sysadmin', 'Submit')));
     ?>
</div>