<div class="">
    <table class="table table-bordered table-condensed table-striped">
        <thead>
            <tr>
                <?php
                if (isset($SysAcl)):
                    if ($SysAcl['read'] || $SysAcl['edit'] || $SysAcl['delete']):
                        ?>
                        <th class="actions col-lg-1 col-sm-1"><?php echo __d('sysadmin', 'Actions'); ?></th>
                        <?php
                    endif;
                endif;
                ?>
                <th>#</th>
                <th><?php echo $this->Paginator->sort('ScmMenu.name',__d('syscm', 'Group Name'));?></th>
                <th><?php echo $this->Paginator->sort('ScmMenu.text',__d('syscm', 'Location'));?></th>
                <th><?php echo $this->Paginator->sort('ScmMenu.annotation',__d('syscm', 'Annotation'));?></th>
            </tr>
        </thead>
        <tbody>
            <?php
            if($scm_menu_groups):
                $counter = $this->Paginator->counter('{:start}');
                foreach($scm_menu_groups as $i => $v):
                    ?>
            <tr>
                <?php
                if (isset($SysAcl)):
                    if ($SysAcl['read'] || $SysAcl['edit'] || $SysAcl['delete']):
                        ?> <td class="actions" nowrap="nowrap">
                            <?php
                            
                            if ($SysAcl['edit']):
                                echo $this->Html->link(
                                        '<span class="fa fa-edit fa-border text-warning"></span>',
                                        array(
                                    'action' => 'edit', $v['ScmMenuGroup']['id']
                                        ), array('escape' => false)
                                );
                            endif;
                            if ($SysAcl['delete'] ):
                                echo $this->Form->postLink(
                                        '<span class="fa fa-trash fa-border text-danger"></span>',
                                        array(
                                    'action' => 'delete', $v['ScmMenuGroup']['id']
                                        ),
                                        array(
                                    'confirm' => __d('sysadmin',
                                            'Are you sure you want to delete # %s?',
                                            h($v['ScmMenuGroup']['name'])),
                                    'escape'  => false
                                        )
                                );
                            endif;
                            ?>
                            </td><?php
                        endif;
                    endif;
                    ?>
                <td><?php
                echo $counter+$i;
                ?></td>
                <td><?php echo h($v['ScmMenuGroup']['name']);?></td>
                <td><?php echo h($v['ScmMenuGroup']['text']);?></td>
                <td><?php echo h($v['ScmMenuGroup']['annotation']);?></td>
            </tr>
                        <?php
                endforeach;
            endif;
            ?>
        </tbody>
    </table>
    
    <?php echo $this->element('paginator', array(), array('plugin' => 'sysadmin')); ?>
</div>