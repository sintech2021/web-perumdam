<?php
#debug($data_list);
$model_alias = "ScmContent";
?><div class="">
    <table class="table table-bordered table-condensed table-striped">
        <thead>
            <tr>
                <?php
                if (isset($SysAcl)):
                    if ($SysAcl['read'] || $SysAcl['edit'] || $SysAcl['delete']):
                        ?>
                        <th class="actions col-lg-1 col-sm-1"><?php echo __d('sysadmin', 'Actions'); ?></th>
                        <?php
                    endif;
                endif;
                ?>
                <th>#</th>
                <th><?php echo $this->Paginator->sort($model_alias.'.text',__d('syscm', 'Title'));?></th>                
                <th><?php echo $this->Paginator->sort($model_alias.'.name',__d('syscm', 'Slug'));?></th>
                <th><?php echo $this->Paginator->sort('ParentContent.name',__d('syscm', 'Parent'));?></th>
                <th><?php echo __d('syscm', 'Category');?></th>
                <th><?php echo $this->Paginator->sort('ScmContentPublished.text',__d('syscm', 'Post Date'));?></th>
                <th><?php echo $this->Paginator->sort('ScmContentPublished.name',__d('syscm', 'Post By'));?></th>
            </tr>
        </thead>
        <tbody>
            <?php
            if($data_list):
                $counter = $this->Paginator->counter('{:start}');
                foreach($data_list as $i => $v):
                    ?>
            <tr>
                <?php
                if (isset($SysAcl)):
                    if ($SysAcl['read'] || $SysAcl['edit'] || $SysAcl['delete']):
                        ?> <td class="actions" nowrap="nowrap">
                            <?php
                            
                            if ($SysAcl['edit']):
                                echo $this->Html->link(
                                        '<span class="fa fa-edit fa-border text-warning"></span>',
                                        array(
                                    'action' => 'edit', $v[$model_alias]['id']
                                        ), array('escape' => false)
                                );
                            endif;
                            if ($SysAcl['delete'] ):
                                echo $this->Form->postLink(
                                        '<span class="fa fa-trash fa-border text-danger"></span>',
                                        array(
                                    'action' => 'delete', $v[$model_alias]['id']
                                        ),
                                        array(
                                    'confirm' => __d('sysadmin',
                                            'Are you sure you want to delete # %s?',
                                            h($v[$model_alias]['name'])),
                                    'escape'  => false
                                        )
                                );
                            endif;
                            ?>
                            </td><?php
                        endif;
                    endif;
                    ?>
                <td><?php
                echo $counter+$i;
                ?></td>
                <td><?php echo h($v[$model_alias]['text']);?><sup class="sup<?php echo h($v['ScmContentPublished']['menu_code']);?>"><em><?php echo h($v['ScmContentPublished']['menu_code']);?></em></sup></td>
                <td><?php echo h($v[$model_alias]['name']);?></td>
                <td><?php echo isset($v['ParentContent']['name']) ? h($v['ParentContent']['text']." (".$v['ParentContent']['name'].")" ): "";?></td>
                <td><?php 
                if($v['ScmCategory']):
                    ?><ul>
                        <?php
                        foreach($v['ScmCategory'] as $cat):
                            ?><li>
                                <?php
                                echo h($cat['text']." (".$cat['name'].")");
                                ?>
</li><?php
                        endforeach;
                        ?>
                    </ul><?php
                endif;
                ?></td>
                <td><?php echo h($v['ScmContentPublished']['text']);?></td>
                <td><?php echo h($v['ScmContentPublished']['name']);?></td>
            </tr>
                        <?php
                endforeach;
            endif;
            ?>
        </tbody>
    </table>
    
    <?php echo $this->element('paginator', array(), array('plugin' => 'sysadmin')); ?>
</div>