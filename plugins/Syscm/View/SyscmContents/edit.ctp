<div class="posts form">
<?php 
    echo $this->element('Sysadmin.HtmlForm/form_create', array('model_alias'=>'Syscm.ScmContent'));
    
    ?>
    <fieldset>
            
    <?php
          
        echo $this->Form->input('ScmContent.id');
         echo $this->Form->input('ScmContent.text', array(
            'type' => 'text',
            'label'=> array(
                'text'  => __d('syscm', 'Title')." *)",
                'class' => 'col-lg-2 col-sm-2 control-label text-warning',
            ),
            'maxlength' => '255',
        ));   
        echo $this->Form->input('ScmContent.name', array(
            'label'=> array(
                'text'  => __d('syscm', 'Slug')." *)",
                'class' => 'col-lg-2 col-sm-2 control-label text-warning',
            )
        )); 
        echo $this->Form->input('ScmContent.parent_id', array(
            'empty' => true,
            'label'=> array(
                'text'  => __d('syscm', 'Parent'),
                'class' => 'col-lg-2 col-sm-2 control-label',
            )
        )); 
        echo $this->Form->input('ScmCategory', array(
            'type' => 'select',
            'options' => $scm_categories,
            'multiple' => true,
            'label'=> array(
                'text'  => __d('syscm', 'Category'),
                'class' => 'col-lg-2 col-sm-2 control-label',
            ),
            'between' => '<div class="col-lg-4 col-sm-4">',
        )); 
        echo $this->Form->input('ScmContentContent.id');
        echo $this->Tinymce->input('ScmContentContent.value',
                        array(
                    'label' => array(
                        'text'  => __d('syscm', 'Content'),
                        'class' => 'col-lg-2 col-sm-2 control-label',
                    )
                ), array('selector'=> '#ScmContentContentValue'), 'full-4x');
        echo $this->Form->input('ScmContentContent.annotation', array(
            'label'=> array(
                'text'  => __d('syscm', 'Data'),
                'class' => 'col-lg-2 col-sm-2 control-label',
            )
        )); 
        echo $this->Form->input('ScmContentPublished.id');
		$ScmContentPublished_input = array(
            'type'=>'select',
            'options'=> $published_options,
            'label'=> array(
                'text'  => __d('syscm', 'Publish Status'),
                'class' => 'col-lg-2 col-sm-2 control-label',
            ),
            'between' => '<div class="col-lg-2 col-sm-2">'
        );
		if(!$is_publisher) {
		 $ScmContentPublished_input['disabled'] = 'disabled';
		}
        echo $this->Form->input('ScmContentPublished.menu_code', $ScmContentPublished_input); 
        echo $this->Form->input('ScmContentPublished.text', array(
             'class'   => 'form-control form-date-input',
            'label'=> array(
                'text'  => __d('syscm', 'Publish Date'),
                'class' => 'col-lg-2 col-sm-2 control-label',
            ),
                    'type'    => 'text',
                    'between' => '<div class="col-lg-2 col-sm-2">'
        )); 
         echo $this->Form->input('ScmContentPublished.value', array(
                    'type'       => 'time',
                    'timeFormat' => '24',
                    'label'      => array(
                        'text'  => __d('sysadmin', 'Publish Time'),
                        'class' => 'col-lg-2 col-sm-2 control-label',
                    ),
                    'between'    => '<div class="col-lg-10 col-sm-10"><div class="form-inline">',
                    'after'      => '</div></div>',
        )); 
        echo $this->Form->input('ScmContentPublished.name', array(
            'label'=> array(
                'text'  => __d('syscm', 'Published By'),
                'class' => 'col-lg-2 col-sm-2 control-label',
            )
        )); 
        
        echo $this->Form->input('ScmContentMeta.id');
        echo $this->Form->input('ScmContentMeta.value', array(
            'label'=> array(
                'text'  => __d('syscm', 'Meta Description'),
                'class' => 'col-lg-2 col-sm-2 control-label',
            )
        )); 
        echo $this->Form->input('ScmContentMeta.text', array(
            'type'=> 'text',
            'label'=> array(
                'text'  => __d('syscm', 'Meta Keyword'),
                'class' => 'col-lg-2 col-sm-2 control-label',
            ),
            'maxlength'=> '500',
        )); 
        echo $this->Form->input('ScmContentMeta.name', array(
            'label'=> array(
                'text'  => __d('syscm', 'Author'),
                'class' => 'col-lg-2 col-sm-2 control-label',
            )
        )); 
        echo $this->Form->input('ScmContentMeta.menu_order', array(
            'label'=> array(
                'text'  => __d('syscm', 'Ordering'),
                'class' => 'col-lg-2 col-sm-2 control-label',
            ),
            'between' => '<div class="col-lg-2 col-sm-2">',
        )); 
        //
    ?>
    </fieldset>
<?php
    echo $this->element('Sysadmin.HtmlForm/form_end', array('submit_label'=>__d('sysadmin', 'Submit')));
    
     ?>
</div>