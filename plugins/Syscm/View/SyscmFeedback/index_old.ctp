<?php
#debug($data_list);
$model_alias = "ScmCustFeedback";
?><div class="">
    <table class="table table-bordered table-condensed table-striped">
        <thead>
            <tr>
                <?php
                if (isset($SysAcl)):
                    if ($SysAcl['read'] || $SysAcl['edit'] || $SysAcl['delete']):
                        ?>
                        <th class="actions col-lg-1 col-sm-1"><?php echo __d('sysadmin', 'Actions'); ?></th>
                        <?php
                    endif;
                endif;
                ?>
                <th>#</th>
                <th><?php echo $this->Paginator->sort($model_alias.'.text',__d('syscm', 'NoSL'));?></th>                
                <th><?php echo $this->Paginator->sort($model_alias.'.name',__d('syscm', 'Name'));?></th>
                <th><?php echo $this->Paginator->sort($model_alias.'.annotation',__d('syscm', 'Feedback Content'));?></th>
                <th><?php echo $this->Paginator->sort($model_alias.'Contact.value',__d('syscm', 'Cellular Number'));?></th>
                <th><?php echo $this->Paginator->sort($model_alias.'Contact.name',__d('syscm', 'Email'));?></th>
                <th><?php echo $this->Paginator->sort($model_alias.'Contact.text',__d('syscm', 'Phone'));?></th>
                <th><?php echo $this->Paginator->sort($model_alias.'.created',__d('syscm', 'Post Date'));?></th>
                <th><?php echo $this->Paginator->sort($model_alias.'Contact.menu_code',__d('syscm', 'Client IP'));?></th>
            </tr>
        </thead>
        <tbody>
            <?php
            if($data_list):
                $counter = $this->Paginator->counter('{:start}');
                foreach($data_list as $i => $v):
                    ?>
            <tr>
                <?php
                if (isset($SysAcl)):
                    if ($SysAcl['read'] || $SysAcl['edit'] || $SysAcl['delete']):
                        ?> <td class="actions" nowrap="nowrap">
                            <?php
                            
                            if ($SysAcl['edit']):
                                echo $this->Html->link(
                                        '<span class="fa fa-table fa-border text-warning"></span>',
                                        array(
                                    'action' => 'detail', $v[$model_alias]['id']
                                        ), array('escape' => false, 'title'=> __d('syscm','View detail'))
                                );
                            endif; 
                            if ($SysAcl['delete'] ):
                                echo $this->Form->postLink(
                                        '<span class="fa fa-trash fa-border text-danger"></span>',
                                        array(
                                    'action' => 'delete', $v[$model_alias]['id']
                                        ),
                                        array(
                                    'confirm' => __d('sysadmin',
                                            'Are you sure you want to delete # %s?',
                                            h($v[$model_alias]['name'])),
                                    'escape'  => false
                                        )
                                );
                            endif;
                            ?>
                            </td><?php
                        endif;
                    endif;
                    ?>
                <td><?php
                echo $counter+$i;
                ?></td>
                <td><?php echo h($v[$model_alias]['text']);?>
                    <?php
                    if(isset($respon_count[$v[$model_alias]['id']])):
                        if($respon_count[$v[$model_alias]['id']] == 0):
                            printf('<sup class="text-success">%s %s</sup>', '<span class="fa fa-asterisk"></span>', __d('syscm','New'));
                        else:
                            printf('<sup class="text-info">%s %s (%s)</sup>', '<span class="fa fa-check"></span>', __d('syscm','Replied'), $respon_count[$v[$model_alias]['id']]);
                    endif;
                    endif;
                    ?>
                </td>
                <td><?php echo h($v[$model_alias]['name']);?></td>
                <td><?php echo h($v[$model_alias]['annotation']);?></td>
                <td><?php echo h($v[$model_alias.'Contact']['value']);?></td>
                <td><?php echo h($v[$model_alias.'Contact']['name']);?></td>
                <td><?php echo h($v[$model_alias.'Contact']['text']);?></td>
                <td><?php echo h($v[$model_alias]['created']);?></td>
                <td><?php echo h($v[$model_alias.'Contact']['menu_code']);?></td>
            </tr>
                        <?php
                endforeach;
            endif;
            ?>
        </tbody>
    </table>
    
    <?php echo $this->element('paginator', array(), array('plugin' => 'sysadmin')); ?>
</div>
<?php
debug($respon_count);