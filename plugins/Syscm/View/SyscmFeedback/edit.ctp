<?php
$model_alias   = "ScmCustFeedbackRespon";
/**
 * foreach ($rs as $key => $val) {
            $cust_id                                  = $val[$this->alias]['scm_customer_id'];
            $res [$cust_id][$key]['responder']        = $val[$this->alias]['name'];
            $res [$cust_id][$key]['respon']           = $val[$this->alias]['value'];
            $res [$cust_id][$key]['respon_time']      = $val[$this->alias]['created'];
            $res [$cust_id][$key]['responder_update'] = $val[$this->alias]['text'];
            $res [$cust_id][$key]['respon_update']    = $val[$this->alias]['modified'];
        }
 */
echo $this->element('Syscm./../SyscmFeedback/elements/feedback-detail');
?>
<div class="post form">
    <?php
    echo $this->element('Sysadmin.HtmlForm/form_create',
                    array('model_alias' => 'Syscm.' . $model_alias));
    ?>
    <fieldset>
        <?php
        echo $this->Form->input($model_alias.'.id');
        
        echo $this->Form->input($model_alias.'.value', array(
            'type' => 'textarea',
            'required' => true,
            'label'=> array(
                'text'  => __d('syscm', 'Respon')." *)",
                'class' => 'col-lg-2 col-sm-2 control-label text-warning',
            )
            ));   
        
        ?>
    </fieldset>
    <?php
    echo $this->element('Sysadmin.HtmlForm/form_end',
                    array(
                'submit_label'  => __d('sysadmin', 'Save respon')
            ));
    ?>
</div>