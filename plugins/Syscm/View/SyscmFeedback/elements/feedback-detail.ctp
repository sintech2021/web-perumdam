<div class="">
    <table class="table table-condensed table-striped">
        <tbody>
            <tr>
                <td class="col-sm-2 col-lg-2 text-info"><?php echo __d('syscm', 'NoSL');
                $v = $data_row;
                ?></td>
                <td><?php echo $v['ScmCustFeedback']['text'];?></td>
                <!-- -->
                <td class="col-sm-2 col-lg-2 text-info"><?php echo __d('syscm', 'Name');?></td>
                <td><?php echo h($v['ScmCustFeedback']['name']); // Name ?></td>
            </tr>
            <tr>
                <td class="col-sm-2 col-lg-2 text-info"><?php echo __d('syscm', 'Feedback Content');?></td>
                <td><?php echo h($v['ScmCustFeedback']['annotation']); // Thread ?></td>
                <!-- -->
                <td class="col-sm-2 col-lg-2 text-info"><?php echo __d('syscm', 'Email');?></td>
                <td><?php echo h($v['ScmCustFeedbackContact']['name']);// Email ?></td>
            </tr>
            <tr>
                <td class="col-sm-2 col-lg-2 text-info"><?php echo __d('syscm', 'Post Date');?></td>
                <td><?php echo h($v['ScmCustFeedback']['created']); // Post Date ?></td>
                <!-- -->
                <td class="col-sm-2 col-lg-2 text-info"><?php echo __d('syscm', 'Cellular');?></td>
                <td><?php echo h($v['ScmCustFeedbackContact']['value']);// Cell ?></td>
            </tr>
            <tr>
                <td class="col-sm-2 col-lg-2 text-info"><?php echo __d('syscm', 'Client IP');?></td>
                <td><?php echo h($v['ScmCustFeedbackContact']['menu_code']);// IP ?></td>
                <!-- -->
                <td class="col-sm-2 col-lg-2 text-info"><?php echo __d('syscm', 'Phone');?></td>
                <td><?php echo h($v['ScmCustFeedbackContact']['value']);// Phone ?></td>
                
            </tr>
            
        </tbody>
    </table>
</div>