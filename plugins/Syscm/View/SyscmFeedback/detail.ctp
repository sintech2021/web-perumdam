<?php
#debug($data_list);
$model_alias = "ScmCustFeedbackRespon";
echo $this->element('Syscm./../SyscmFeedback/elements/feedback-detail');
?>

<div class="">
    <table class="table table-bordered table-condensed table-striped">
        <thead>
            <tr>
                <?php
                if (isset($SysAcl)):
                    if ($SysAcl['read'] || $SysAcl['edit'] || $SysAcl['delete']):
                        ?>
                        <th class="actions col-lg-1 col-sm-1"><?php echo __d('sysadmin', 'Actions'); ?></th>
                        <?php
                    endif;
                endif;
                ?>
                <th>#</th>
                
                <th><?php echo $this->Paginator->sort($model_alias.'.name',__d('syscm', 'Responder'));?></th>
                <th><?php echo $this->Paginator->sort($model_alias.'.value',__d('syscm', 'Respon'));?></th>
                
                
                <th><?php echo $this->Paginator->sort($model_alias.'.created',__d('syscm', 'Post Date'));?></th>
                <th><?php echo $this->Paginator->sort($model_alias.'.modified',__d('syscm', 'Last Update'));?></th>
                <th><?php echo $this->Paginator->sort($model_alias.'.text',__d('syscm', 'Last Update By'));?></th>
                
            </tr>
        </thead>
        <tbody>
            <?php
            if($data_list):
                $counter = $this->Paginator->counter('{:start}');
                foreach($data_list as $i => $v):
                    ?>
            <tr>
                <?php
                if (isset($SysAcl)):
                    if ($SysAcl['read'] || $SysAcl['edit'] || $SysAcl['delete']):
                        ?> <td class="actions" nowrap="nowrap">
                            <?php
                            
                            if ($SysAcl['edit']):
                                echo $this->Html->link(
                                        '<span class="fa fa-edit fa-border text-warning"></span>',
                                        array(
                                    'action' => 'edit', $v[$model_alias]['scm_customer_id'], $v[$model_alias]['id']
                                        ), array('escape' => false, 'title'=> __d('syscm','Add respon'))
                                );
                            endif; 
                            if ($SysAcl['delete'] ):
                                echo $this->Form->postLink(
                                        '<span class="fa fa-trash fa-border text-danger"></span>',
                                        array(
                                    'action' => 'delete', $v[$model_alias]['id']
                                        ),
                                        array(
                                    'confirm' => __d('sysadmin',
                                            'Are you sure you want to delete # %s?',
                                            h($v[$model_alias]['name'])),
                                    'escape'  => false
                                        )
                                );
                            endif;
                            ?>
                            </td><?php
                        endif;
                    endif;
                    ?>
                <td><?php
                echo $counter+$i;
                ?></td>
                <td><?php echo h($v[$model_alias]['name']);?></td>
                <td><?php echo h($v[$model_alias]['value']);?></td>
                <td><?php echo h($v[$model_alias]['created']);?></td>
                <td><?php echo h($v[$model_alias]['modified']);?></td>
                <td><?php echo h($v[$model_alias]['text']);?></td>
            </tr>
                        <?php
                endforeach;
            endif;
            ?>
        </tbody>
    </table>
    
    <?php echo $this->element('paginator', array(), array('plugin' => 'sysadmin')); ?>
</div>
<?php
debug($data_row);