<?php
#debug($data_list);
$model_alias 	 	 = "ScmCustFeedbackView";
$model_alias_count 	 = "ScmCustFeedbackViewReplied";
#debug($form_filter);
?><div class="">

	<div>
	
	<div class="btn-group" role="group" aria-label="...">
	  <button type="button" class="btn btn-info" id="filter-grid"><span class="fa fa-calendar"></span> Filter</button>
	  <?php if ($SysAcl['read']): ?>
			<button type="submit" form="form-filter" class="btn btn-success" id="export-grid" name="data[<?php echo $model_alias ?>][action]" value='export_xls'>
					<span class="fa fa-file-excel-o"></span>
				<?php echo __d('syscm', 'Export Excel'); ?>
			</button>
			<?php
		endif;
	?>
	</div>
	<hr>
	<div class="panel panel-info" style="display:none;" id="filter-panel">
	  <div class="panel-heading">Filter Data</div>
	  <div class="panel-body">
		<form class="form-inline" id="form-filter" method="post">
			  <div class="form-group">
				<label for="exampleInputName2">Tahun</label>
				<select class="form-control" name="form_filter[year]">
					<option value=""> - </option>
				  <?php foreach($form_filter["year"] as $tahun){
					if($tahun == $form_filter["year_select"]){$selected = "selected";}else{$selected= "";}
					echo '<option '.$selected.'>'.$tahun.'</option>';
				  }?>
				</select>
			  </div>
			  <div class="form-group">
				<label for="exampleInputEmail2">Bulan</label>
				<select class="form-control" name="form_filter[month]">
				<option value=""> - </option>
				  <?php foreach($form_filter["month"] as $key=>$month){
					if(($key+1) == $form_filter["month_select"]){$selected = "selected";}else{$selected= "";}
					echo '<option value="'.($key+1).'"  '.$selected.'>'.$month.'</option>';
				  }?>
				</select>
			  </div>
			  <button class="btn btn-info" id="submit-filter" name="form_filter[do]">Submit</button>
			  <button class="btn btn-success" id="clear-filter" name="form_filter[clear]">Clear</button>
		</form>
	  </div>
	</div>
	</div>
	<hr>
    <table class="table table-bordered table-condensed table-striped">
        <thead>
            <tr>
                <?php
                if (isset($SysAcl)):
                    if ($SysAcl['read'] || $SysAcl['edit'] || $SysAcl['delete']):
                        ?>
                        <th class="actions col-lg-1 col-sm-1"><?php echo __d('sysadmin', 'Actions'); ?></th>
                        <?php
                    endif;
                endif;
                ?>
                <th>#</th>
                <th><?php echo $this->Paginator->sort($model_alias.'.text',__d('syscm', 'NoSL'));?></th>                
                <th><?php echo $this->Paginator->sort($model_alias.'.name',__d('syscm', 'Name'));?></th>
                <th><?php echo $this->Paginator->sort($model_alias.'.annotation',__d('syscm', 'Feedback Content'));?></th>
                <th><?php echo $this->Paginator->sort($model_alias.'.mobile',__d('syscm', 'Cellular Number'));?></th>
                <th><?php echo $this->Paginator->sort($model_alias.'.email',__d('syscm', 'Email'));?></th>
                <th><?php echo $this->Paginator->sort($model_alias.'.phone',__d('syscm', 'Phone'));?></th>
                <th><?php echo $this->Paginator->sort($model_alias.'.created',__d('syscm', 'Post Date'));?></th>
                <th><?php echo $this->Paginator->sort($model_alias.'.ip_address',__d('syscm', 'Client IP'));?></th>
            </tr>
        </thead>
        <tbody>
            <?php
            if($data_list):
                $counter = $this->Paginator->counter('{:start}');
                foreach($data_list as $i => $v):
                    ?>
            <tr>
                <?php
                if (isset($SysAcl)):
                    if ($SysAcl['read'] || $SysAcl['edit'] || $SysAcl['delete']):
                        ?> <td class="actions" nowrap="nowrap">
                            <?php
                            
                            if ($SysAcl['edit']):
                                echo $this->Html->link(
                                        '<span class="fa fa-table fa-border text-warning"></span>',
                                        array(
                                    'action' => 'detail', $v[$model_alias]['id']
                                        ), array('escape' => false, 'title'=> __d('syscm','View detail'))
                                );
                            endif; 
                            if ($SysAcl['delete'] ):
                                echo $this->Form->postLink(
                                        '<span class="fa fa-trash fa-border text-danger"></span>',
                                        array(
                                    'action' => 'delete', $v[$model_alias]['id']
                                        ),
                                        array(
                                    'confirm' => __d('sysadmin',
                                            'Are you sure you want to delete # %s?',
                                            h($v[$model_alias]['name'])),
                                    'escape'  => false
                                        )
                                );
                            endif;
                            ?>
                            </td><?php
                        endif;
                    endif;
                    ?>
                <td><?php
                echo $counter+$i;
                ?></td>
                <td> <?php echo h($v[$model_alias]['text']);?>
                    <?php
                    if(isset($v[$model_alias_count]['count'])):
                        if($v[$model_alias_count]['count'] >= 1 ):
							printf('<sup class="text-info">%s %s (%s)</sup>', '<span class="fa fa-check"></span>', __d('syscm','Replied'), $v[$model_alias_count]['count']);
                        else:
                            printf('<sup class="text-success">%s %s</sup>', '<span class="fa fa-asterisk"></span>', __d('syscm','New'));
						endif;
					else:
						printf('<sup class="text-success">%s %s</sup>', '<span class="fa fa-asterisk"></span>', __d('syscm','New'));
                    endif;
                    ?>
                </td>
                <td><?php echo h($v[$model_alias]['name']);?></td>
                <td><?php echo h($v[$model_alias]['annotation']);?></td>
                <td><?php echo h($v[$model_alias]['mobile']);?></td>
                <td><?php echo h($v[$model_alias]['email']);?></td>
                <td><?php echo h($v[$model_alias]['phone']);?></td>
                <td><?php echo h($v[$model_alias]['created']);?></td>
                <td><?php echo h($v[$model_alias]['ip_address']);?></td>
            </tr>
                        <?php
                endforeach;
            endif;
            ?>
        </tbody>
    </table>
    
    <?php echo $this->element('paginator', array(), array('plugin' => 'sysadmin')); ?>
</div>

<?php
#debug($respon_count);