<?php
$model_alias = "ScmCategory";
?>
<div class="posts form">
<?php 
    echo $this->element('Sysadmin.HtmlForm/form_create', array('model_alias'=>'Syscm.'.$model_alias));
    ?>
    <fieldset>
        <?php
          
        echo $this->Form->input($model_alias.'.name', array(
            'type' => 'text',
            'label'=> array(
                'text'  => __d('syscm', 'ID')." *)",
                'class' => 'col-lg-2 col-sm-2 control-label text-warning',
            ),
            'maxlength' => '255',
        ));   
        echo $this->Form->input($model_alias.'.text', array(
            'type' => 'text',
            'label'=> array(
                'text'  => __d('syscm', 'Name')." *)",
                'class' => 'col-lg-2 col-sm-2 control-label text-warning',
            ),
            'maxlength' => '255',
        ));   
        echo $this->Form->input($model_alias.'.annotation', array(
            
            'label'=> array(
                'text'  => __d('syscm', 'Annotation'),
                'class' => 'col-lg-2 col-sm-2 control-label',
            ),
        ));   
        
    ?>
    </fieldset>
<?php
    echo $this->element('Sysadmin.HtmlForm/form_end', array('submit_label'=>__d('sysadmin', 'Submit')));
     ?>
</div>
