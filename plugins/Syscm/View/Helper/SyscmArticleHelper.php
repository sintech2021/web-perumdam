<?php

App::uses('HtmlHelper', 'View/Helper');

class SyscmArticleHelper extends HtmlHelper
{

    public $helpers = array('Js', 'Html');

    protected function _getFirstImg($the_content, $default = "/pdamtkr/logo.png" )
    {
        $img_src     = $default;
        $startstrpos = '<img';
        $stoptstrpos = '/>';
        $src         = 'src="';

        $foundpos = strpos($the_content, $startstrpos);
        if ($foundpos):
            $next_content = substr($the_content, $foundpos);
            $foundsrc = strpos($next_content, $src);
            $next_content = substr($next_content, $foundsrc+strlen($src));
            $foundquoted = strpos($next_content, '"');
            $img_src = substr($next_content, 0,$foundquoted);
            
        endif;
        return $img_src;
    }

    protected function _invokeSpanImg($the_content)
    {

        $parsed_content = "";
        $startstrpos    = '<img';
        $stoptstrpos    = '/>';
        $spanopen       = '<span class="image">';
        $spanclose      = '</span>';
        $foundpos       = strpos($the_content, $startstrpos);
        if ($foundpos !== false) {
            $parsed_content .= substr($the_content, 0, $foundpos);
            $next_content = substr($the_content, $foundpos);
            $parsed_content .= $spanopen;
            $next_post    = strpos($next_content, $stoptstrpos);
            $parsed_content .= substr($next_content, 0,
                    $next_post + strlen($stoptstrpos));
            $parsed_content .= $spanclose;
            $next_content = substr($next_content,
                    $next_post + strlen($stoptstrpos));
            $foundpos     = strpos($next_content, $startstrpos);

            while ($foundpos !== false) {
                $parsed_content .= substr($next_content, 0, $foundpos);
                $next_content = substr($next_content, $foundpos);
                $parsed_content .= $spanopen;
                $next_post    = strpos($next_content, $stoptstrpos);
                $parsed_content .= substr($next_content, 0,
                        $next_post + strlen($stoptstrpos));
                $parsed_content .= $spanclose;
                $next_content = substr($next_content,
                        $next_post + strlen($stoptstrpos));
                $foundpos     = strpos($next_content, $startstrpos);
            }
            $parsed_content .= $next_content;
        }
        else {
            $parsed_content = $the_content;
        }
        return $parsed_content;
    }

    /**
     * 
     * @param type $contents_rs
     * @return type
     */
    public function display($contents_rs)
    {
        $ob = "";
        ob_start();
        echo "<article>";
        if (isset($contents_rs['ScmContent'])):
            echo "<header>";
            echo "<h2>";
            echo $contents_rs['ScmContent']['text'];
            echo "</h2>";
            if (isset($contents_rs['ScmContentPublished'])):
                echo "<p><small>";
                echo __d('syscm', "Written by ");
                echo $contents_rs['ScmContentPublished']['name'];
                $pubdate = $contents_rs['ScmContentPublished']['text'];
                if ($pubdate):
                    $pubtime  = strtotime($pubdate);
                    $pubday   = __d('syscm', date("l", $pubtime));
                    $pubdate  = date("j", $pubtime);
                    $pubmonth = __d('syscm', date("F", $pubtime));
                    $pubyear  = date("Y", $pubtime);
                    echo __d('syscm', " on %s, %s %s %s", $pubday, $pubdate,
                            $pubmonth, $pubyear);
                endif;
                echo "<p><small>";
            endif;


            echo "<small><p>";
            echo "</header>";
        endif;
        if (isset($contents_rs['ScmContentContent'])):
            $the_content    = $contents_rs['ScmContentContent']['value'];
            $parsed_content = "";
            $startstrpos    = '<img';
            $stoptstrpos    = '/>';
            $spanopen       = '<span class="image maxten">';
            $spanclose      = '</span>';
            $foundpos       = strpos($the_content, $startstrpos);
            if ($foundpos !== false) {
                $parsed_content .= substr($the_content, 0, $foundpos);
                $next_content = substr($the_content, $foundpos);
                $parsed_content .= $spanopen;
                $next_post    = strpos($next_content, $stoptstrpos);
                $parsed_content .= substr($next_content, 0,
                        $next_post + strlen($stoptstrpos));
                $parsed_content .= $spanclose;
                $next_content = substr($next_content,
                        $next_post + strlen($stoptstrpos));
                $foundpos     = strpos($next_content, $startstrpos);

                while ($foundpos !== false) {
                    $parsed_content .= substr($next_content, 0, $foundpos);
                    $next_content = substr($next_content, $foundpos);
                    $parsed_content .= $spanopen;
                    $next_post    = strpos($next_content, $stoptstrpos);
                    $parsed_content .= substr($next_content, 0,
                            $next_post + strlen($stoptstrpos));
                    $parsed_content .= $spanclose;
                    $next_content = substr($next_content,
                            $next_post + strlen($stoptstrpos));
                    $foundpos     = strpos($next_content, $startstrpos);
                }
                $parsed_content .= $next_content;
            }
            else {
                $parsed_content = $the_content;
            }


            echo $parsed_content;
        endif;
        echo "</article>";
        $ob = ob_get_contents();
        ob_end_clean();
        return $ob;
    }

    public function displaySummary($contents_rs)
    {

        $ob = "";
        ob_start();
        
         $ctitle = "";
        if (isset($contents_rs['text'])):
            $ctitle = h($contents_rs['text']);
        endif;
        $clink = "";
        if (isset($contents_rs['name'])):
            $clink = h($contents_rs['name']);
        endif;
        
        $cvalue = "";
        if(isset($contents_rs['ScmContentContent']['value'])):
            $cvalue = $contents_rs['ScmContentContent']['value'];
        endif;
        $imUrl = "";
        if($cvalue):
            $imUrl = $this->_getFirstImg($cvalue);
        endif;
        if($imUrl):
            
                echo $this->Html->link(
                        $this->Html->image(
                                $imUrl
                                ),$this->Html->url($clink, true),array('escape'=>false, 'class'=>'image left'));
        endif;

        echo '<div class="inner">';
       
        if ($ctitle):
            echo '<h3>';
            $text = $ctitle;
            if ($clink):
                $text = $this->Html->link($ctitle,
                        $this->Html->url($clink, true));
            endif;
            echo $text;
            echo '</h3>';
        endif;
        echo '<p>';
        $text = filter_var($cvalue, FILTER_SANITIZE_STRIPPED);
        echo substr($text, 0, 220);
        if (strlen($text) > 220):
            echo "...";
        endif;
        echo '</p>';
        echo '<p>';
        echo $this->Html->link(__d('syscm', 'read more'),
                $this->Html->url($clink, true), array('class'=>'button'));
        echo '</p>';


        echo '</div>';
        $ob = ob_get_contents();
        ob_end_clean();
        return $ob;
    }
    
    public function excerpt($contents, $word_count = 35, $more = "...", $options=array())
    {
        $stripped = filter_var($contents, FILTER_SANITIZE_STRING, FILTER_SANITIZE_STRIPPED);
        $words = str_word_count($stripped, 2);
        $words_indexs = array_keys($words);
       
        if(isset($words_indexs[$word_count])){
             $sub_max = $words_indexs[$word_count];
        }else{
            $sub_max =  end($words_indexs);
            $more = "";
        }
        return substr($stripped,0,$sub_max).$more;
    }
    
    public function getFirstImg($the_content, $default = "/pdamtkr/logo.png")
    {
        return $this->_getFirstImg($the_content, $default);
    }

    public function invokeSpanImg($the_content)
    {
        return $this->_invokeSpanImg($the_content);
    }
}
