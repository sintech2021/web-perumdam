<h4><?= $this->get('sub_title')?></h4>
<?= $this->get('xcrud')?>
<style type="text/css">
	a[data-task=create]{
		display: none;
	}
</style>

<script type="text/javascript">
	$('li[role=presentation] > a[href*=add]').click(()=>{
		$('a[data-task=create]').click();
		event.preventDefault();
		return false;
	})
</script>