<?php
$model_alias = "ScmContentMedia";
?><div class="">
    <table class="table table-bordered table-condensed table-striped">
        <thead>
            <tr>
                <?php
                if (isset($SysAcl)):
                    if ($SysAcl['read'] || $SysAcl['edit'] || $SysAcl['delete']):
                        ?>
                        <th class="actions col-lg-1 col-sm-1"><?php echo __d('sysadmin', 'Actions'); ?></th>
                        <?php
                    endif;
                endif;
                ?>
                <th>#</th>
                <th><?php echo __d('syscm', 'Image');?></th>
                            
                <th><?php echo $this->Paginator->sort($model_alias.'.name',__d('syscm', 'ID'));?> / <?php echo $this->Paginator->sort($model_alias.'.text',__d('syscm', 'Name'));?> / <?php echo $this->Paginator->sort('ScmContentContent.value',__d('syscm', 'Source'));?></th>
                <th><?php echo __d('syscm', 'Album');?></th>
                <th><?php echo $this->Paginator->sort($model_alias.'.annotation',__d('syscm', 'Annotation'));?></th>
            </tr>
        </thead>
        <tbody>
            <?php
            if($data_list):
                $counter = $this->Paginator->counter('{:start}');
                foreach($data_list as $i => $v):
                    ?>
            <tr>
                <?php
                if (isset($SysAcl)):
                    if ($SysAcl['read'] || $SysAcl['edit'] || $SysAcl['delete']):
                        ?> <td class="actions" nowrap="nowrap">
                            <?php
                            
                            if ($SysAcl['edit']):
                                echo $this->Html->link(
                                        '<span class="fa fa-edit fa-border text-warning"></span>',
                                        array(
                                    'action' => 'edit', $v[$model_alias]['id']
                                        ), array('escape' => false)
                                );
                            endif;
                            if ($SysAcl['delete'] ):
                                echo $this->Form->postLink(
                                        '<span class="fa fa-trash fa-border text-danger"></span>',
                                        array(
                                    'action' => 'delete', $v[$model_alias]['id']
                                        ),
                                        array(
                                    'confirm' => __d('sysadmin',
                                            'Are you sure you want to delete # %s?',
                                            h($v[$model_alias]['name'])),
                                    'escape'  => false
                                        )
                                );
                            endif;
                            ?>
                            </td><?php
                        endif;
                    endif;
                    ?>
                <td><?php
               
                echo $counter+$i;
                ?></td>
                
                <td><?php echo $this->Html->image($this->Html->url($v['ScmContentContent']['value'], true), array('class'=>"col-lg-12 col-sm-12"));?></td>
                <td>
                    <ul class="list-group">
                        <li class="list-group-item"><?php echo h($v[$model_alias]['name']);?></li>
                        <li class="list-group-item"><?php echo h($v[$model_alias]['text']);?></li>
                    </ul>
                    <?php
                    $zcoptions = array(
                        '_id' => $i,
                        
                    );
                    echo $this->ZeroClipboard->buildZC($zcoptions);
                    ?>
                    <div class="panel panel-warning">
                        <div class="panel-body">
                            <button class="btn btn-default pull-right" id="zc-btn-<?php echo $i; ?>" data-clipboard-target="src-input-id-<?php echo $i; ?>" title="Copy to Clipboard">
    <span class="fa fa-clipboard"></span>
</button>
                            <div class="col-lg-8 col-sm-8" id="src-input-id-<?php echo $i; ?>"><?php echo $this->Html->url($v['ScmContentContent']['value'], true);?></div>
                            
                        </div>
                    </div>
                    
                    

</td>
                <td><?php if($v['ScmContentMediaGroup']):
                    printf('<ul>');
                    foreach($v['ScmContentMediaGroup'] as $g):
                        printf('<li>%s</li>', h($g['text']));
                    endforeach;
                    printf('</ul>');
                endif;
?></td>
                <td><?php echo h($v[$model_alias]['annotation']);?></td>
            </tr>
                        <?php
                endforeach;
            endif;
            ?>
        </tbody>
    </table>
    
    <?php echo $this->element('paginator', array(), array('plugin' => 'sysadmin')); ?>
</div>
<?php
debug($data_list);
?>