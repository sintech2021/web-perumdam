<?php
$model_alias = "ScmContentMedia";

?>
<div class="posts form">
<?php 

    echo $this->element('Sysadmin.HtmlForm/form_create', array('model_alias'=>'Syscm.'.$model_alias, 'form_type'=>'file'));
    ?>
    <fieldset>
        <?php
        echo $this->Form->input($model_alias.'.id');
        echo $this->Form->input($model_alias.'.name', array(
            'type' => 'text',
            'label'=> array(
                'text'  => __d('syscm', 'ID')." *)",
                'class' => 'col-lg-2 col-sm-2 control-label text-warning',
            ),
            'maxlength' => '255',
        ));   
        echo $this->Form->input($model_alias.'.text', array(
            'type' => 'text',
            'label'=> array(
                'text'  => __d('syscm', 'Name')." *)",
                'class' => 'col-lg-2 col-sm-2 control-label text-warning',
            ),
            'maxlength' => '255',
            
        ));   
        echo $this->Form->input('ScmContentContent.id');
        echo $this->Form->input('ScmContentContent.value', array('type'=> 'hidden'));
        $imUrl = $this->Html->url($this->request->data['ScmContentContent']['value'], true);
        $after = '</div>';
        $after .= '<div class="col-lg-3 col-sm-3">';
        ob_start();
        
                    $zcoptions = array(
                        '_id' => rand(912, 1981),
                        
                    );
                    echo $this->ZeroClipboard->buildZC($zcoptions);
                    ?>
                    <div class="panel panel-warning">
                        <div class="panel-body">
                            <button type="button" class="btn btn-default pull-right" id="zc-btn-<?php echo $zcoptions['_id']; ?>" data-clipboard-target="src-input-id-<?php echo $zcoptions['_id']; ?>" title="Copy to Clipboard">
    <span class="fa fa-clipboard"></span>
</button>
                            <div class="col-lg-8 col-sm-8" id="src-input-id-<?php echo $zcoptions['_id']; ?>"><?php echo $imUrl;?></div>
                            
                        </div>
                    </div><?php
        $after .= ob_get_contents();
        ob_end_clean();
        $after .= '</div>';
        $after .= '<div class="col-lg-5 col-sm-5">';
        $after .= $this->Html->image($imUrl, array('class'=> 'col-lg-12 col-sm-12'));
        $after .= '</div>';
        echo $this->Form->input('the_file', array(
            'type' => 'file',
            'title' => __d('syscm', 'Choose file..'),
            'label'=> array(
                'text'  => __d('syscm', 'File')." *)",
                'class' => 'col-lg-2 col-sm-2 control-label text-warning',
            ),
                    'between' => '<div class="col-lg-2 col-sm-2">',
            'after' => $after,
        ));   
        echo $this->Form->input($model_alias.'.annotation', array(
            
            'label'=> array(
                'text'  => __d('syscm', 'Annotation'),
                'class' => 'col-lg-2 col-sm-2 control-label',
            ),
        ));   
        
        echo $this->Form->input('ScmContentPublished.id');
        echo $this->Form->input('ScmContentPublished.menu_code', array(
            'type'=>'select',
            'options'=> $published_options,
            'label'=> array(
                'text'  => __d('syscm', 'Publish Status'),
                'class' => 'col-lg-2 col-sm-2 control-label',
            )
        )); 
        echo $this->Form->input('ScmContentPublished.text', array(
             'class'   => 'form-control form-date-input',
            'label'=> array(
                'text'  => __d('syscm', 'Publish Date'),
                'class' => 'col-lg-2 col-sm-2 control-label',
            ),
                    'type'    => 'text',
                    'between' => '<div class="col-lg-2 col-sm-2">'
        )); 
        
        echo $this->Form->input('ScmContentPublished.name', array(
            
            'label'=> array(
                'text'  => __d('syscm', 'Uploaded By'),
                'class' => 'col-lg-2 col-sm-2 control-label',
            )
        ));
    ?>
    </fieldset>
<?php
    echo $this->element('Sysadmin.HtmlForm/form_end', array('submit_label'=>__d('sysadmin', 'Submit')));
     ?>
</div>
