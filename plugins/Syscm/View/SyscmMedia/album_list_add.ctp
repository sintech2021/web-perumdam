<?php
$model_alias = "ScmContentMedia";
?><div class="">
    <h2><?php echo $browser_menu_title; ?></h2>
    <table class="table table-bordered table-condensed table-striped">
        <thead>
            <tr>
                <?php
                if (isset($SysAcl)):
                    if ($SysAcl['read'] || $SysAcl['edit'] || $SysAcl['delete']):
                        ?>
                        <th class="actions col-lg-1 col-sm-1"><?php echo __d('sysadmin',
                        'Actions'); ?></th>
                        <?php
                    endif;
                endif;
                ?>
                <th>#</th>
                <th><?php echo __d('syscm', 'Image'); ?></th>

                <th><?php echo $this->Paginator->sort($model_alias . '.name',
                        __d('syscm', 'ID')); ?> / <?php echo $this->Paginator->sort($model_alias . '.text',
                        __d('syscm', 'Name')); ?> / <?php echo $this->Paginator->sort($model_alias . '.name',
                        __d('syscm', 'Source')); ?></th>
                
                <th><?php echo $this->Paginator->sort($model_alias . '.annotation',
                        __d('syscm', 'Annotation')); ?></th>
            </tr>
        </thead>
        <tbody>
                <?php
                if ($data_list):
                    $counter = $this->Paginator->counter('{:start}');
                    $current_page = $this->Paginator->current();
                    foreach ($data_list as $i => $v):
                        ?>
                    <tr>
                        <?php
                        if (isset($SysAcl)):
                            if ($SysAcl['read'] || $SysAcl['edit'] || $SysAcl['delete']):
                                ?> <td class="actions" nowrap="nowrap">
                                <?php
                                if ($SysAcl['edit']):
                                    /*echo $this->Html->link(
                                            '<span class="fa fa-edit fa-border text-warning"></span>',
                                            array(
                                        'action' => 'edit', $v[$model_alias]['id']
                                            ), array('escape' => false)
                                    );*/
                                endif;
                                if ($SysAcl['add']):
                                    echo $this->Form->postLink(
                                            '<span class="fa fa-plus fa-border text-info"></span>',
                                            array(
                                        'action' => 'album_list_add', 
                                                $album['ScmContentMediaGroup']['id'], 
                                                $v[$model_alias]['id'],
                                                'page'=> $current_page,
                                            ),
                                            array(
                                        
                                        'escape'  => false
                                            )
                                    );
                                endif;
                                ?>
                                </td><?php
                                endif;
                            endif;
                            ?>
                        <td><?php
                    echo $counter + $i;
                    ?></td>

                        <td><?php echo $this->Html->image($this->Html->url($v['ScmContentContent']['value'],
                                    true), array('class' => "col-lg-12 col-sm-12")); ?></td>
                        <td>
                            <ul class="list-group">
                                <li class="list-group-item"><?php echo h($v[$model_alias]['name']); ?></li>
                                <li class="list-group-item"><?php echo h($v[$model_alias]['text']); ?></li>
                            </ul>
                            <?php
                            $zcoptions = array(
                                '_id' => $i,
                            );
                            echo $this->ZeroClipboard->buildZC($zcoptions);
                            ?>
                            <div class="panel panel-warning">
                                <div class="panel-body">
                                    <button class="btn btn-default pull-right" id="zc-btn-<?php echo $i; ?>" data-clipboard-target="src-input-id-<?php echo $i; ?>" title="Copy to Clipboard">
                                        <span class="fa fa-clipboard"></span>
                                    </button>
                                    <div class="col-lg-8 col-sm-8" id="src-input-id-<?php echo $i; ?>"><?php echo $this->Html->url($v['ScmContentContent']['value'],
                                    true); ?></div>

                                </div>
                            </div>



                        </td>
                        <td><?php echo h($v[$model_alias]['annotation']); ?></td>
                    </tr>
        <?php
    endforeach;
endif;
?>
        </tbody>
    </table>

<?php echo $this->element('paginator', array(),
        array('plugin' => 'sysadmin')); ?>
</div>