<?php
$model_alias = "ScmContentMedia";

?>
<div class="posts form">
<?php 

    echo $this->element('Sysadmin.HtmlForm/form_create', array('model_alias'=>'Syscm.'.$model_alias, 'form_type'=>'file'));
    ?>
    <fieldset>
        <?php
        echo $this->Form->input($model_alias.'.name', array(
            'type' => 'text',
            'label'=> array(
                'text'  => __d('syscm', 'ID')." *)",
                'class' => 'col-lg-2 col-sm-2 control-label text-warning',
            ),
            'maxlength' => '255',
        ));   
        echo $this->Form->input($model_alias.'.text', array(
            'type' => 'text',
            'label'=> array(
                'text'  => __d('syscm', 'Name')." *)",
                'class' => 'col-lg-2 col-sm-2 control-label text-warning',
            ),
            'maxlength' => '255',
        ));   
        echo $this->Form->input('the_file', array(
            'type' => 'file',
            'title' => __d('syscm', 'Choose file..'),
            'label'=> array(
                'text'  => __d('syscm', 'File')." *)",
                'class' => 'col-lg-2 col-sm-2 control-label text-warning',
            ),
                    'between' => '<div class="col-lg-2 col-sm-2">',
        ));   
        echo $this->Form->input($model_alias.'.annotation', array(
            
            'label'=> array(
                'text'  => __d('syscm', 'Annotation'),
                'class' => 'col-lg-2 col-sm-2 control-label',
            ),
        ));   
        echo $this->Form->input('ScmContentPublished.menu_code', array(
            'type'=>'select',
            'options'=> $published_options,
            'label'=> array(
                'text'  => __d('syscm', 'Publish Status'),
                'class' => 'col-lg-2 col-sm-2 control-label',
            )
        )); 
        echo $this->Form->input('ScmContentPublished.text', array(
             'class'   => 'form-control form-date-input',
            'label'=> array(
                'text'  => __d('syscm', 'Publish Date'),
                'class' => 'col-lg-2 col-sm-2 control-label',
            ),
                    'type'    => 'text',
                    'between' => '<div class="col-lg-2 col-sm-2">'
        )); 
        
        echo $this->Form->input('ScmContentPublished.name', array(
            
            'label'=> array(
                'text'  => __d('syscm', 'Uploaded By'),
                'class' => 'col-lg-2 col-sm-2 control-label',
            )
        ));
    ?>
    </fieldset>
<?php
    echo $this->element('Sysadmin.HtmlForm/form_end', array('submit_label'=>__d('sysadmin', 'Submit')));
     ?>
</div>
