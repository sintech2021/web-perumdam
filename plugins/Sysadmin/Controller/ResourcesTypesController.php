<?php
App::uses('SysadminAppController', 'Sysadmin.Controller');
/**
 * ResourcesTypes Controller
 *
 * @property ResourcesType $ResourcesType
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class ResourcesTypesController extends SysadminAppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ResourcesType->recursive = 0;
		$this->set('resourcesTypes', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ResourcesType->exists($id)) {
			throw new NotFoundException(__('Invalid resources type'));
		}
		$options = array('conditions' => array('ResourcesType.' . $this->ResourcesType->primaryKey => $id));
		$this->set('resourcesType', $this->ResourcesType->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ResourcesType->create();
			if ($this->ResourcesType->save($this->request->data)) {
				$this->Flash->success(__('The resources type has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The resources type could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ResourcesType->exists($id)) {
			throw new NotFoundException(__('Invalid resources type'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ResourcesType->save($this->request->data)) {
				$this->Flash->success(__('The resources type has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The resources type could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ResourcesType.' . $this->ResourcesType->primaryKey => $id));
			$this->request->data = $this->ResourcesType->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ResourcesType->id = $id;
		if (!$this->ResourcesType->exists()) {
			throw new NotFoundException(__('Invalid resources type'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ResourcesType->delete()) {
			$this->Flash->success(__('The resources type has been deleted.'));
		} else {
			$this->Flash->error(__('The resources type could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
