<?php

App::uses('SysadminAppController', 'Sysadmin.Controller');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class UsersController extends SysadminAppController
{

    /**
     * Components
     *
     * @var array
     */
    public $uses = array('Sysadmin.User');

    /**
     * Set up the top menu for each rendered page
     * @visibility protected
     * @param void
     * @return void
     */
    protected function _setTopMenu($set = TRUE)
    {
        $index = array();
        $add   = array();
        if ($this->SysAclView['browse']) {
            $index = array(
                'title'   => '<span class="fa fa-list fa-fw"></span> ' . __d('sysadmin',
                        'Index'),
                'url'     => $this->SysAcl->routeToCakeUrl('sysadmin.users.index'),
                'options' => array('escape' => false),
                'active'  => false,
            );
            if ($this->request->action == 'index') {
                $index['active'] = true;
            }
        }
        if ($this->SysAclView['add']) {
            $add = array(
                'title'   => '<span class="fa fa-plus fa-fw"></span> ' . __d('sysadmin',
                        'Add user'),
                'url'     => $this->SysAcl->routeToCakeUrl('sysadmin.users.add'),
                'options' => array('escape' => false),
                'active'  => false,
            );
            if ($this->request->action == 'add') {
                $add['active'] = true;
            }
        }
        $top_menu = array(
            $index,
            $add
        );
        if ($set) {
            $this->set(compact('top_menu'));
        }
        else {
            return $top_menu;
        }
    }

    /**
     * index method
     *
     * @return void
     */
    public function index()
    {

        $gsparams = array(
            'User.name'          => __d('sysadmin', 'Name'),
            'User.username'      => __d('sysadmin', 'Username'),
            'User.email'         => __d('sysadmin', 'Email'),
            'Role.name'          => __d('sysadmin', 'User role'),
            'User.registered_dt' => __d('sysadmin', 'Registered date'),
            'User.active_dt'     => __d('sysadmin', 'Active date'),
        );

        $this->_setGlobalSearch($gsparams);
        $this->User->recursive = 0;
        $conditions            = array('AND' => array('User.active_status' => 1));
        if ($this->isGs) {
            $gs = $this->_getGlobalSearch();
            if ($gs['global_search_field']) {
                if ($gs['global_search_field'] == $this->allSearchKey) {
                    foreach ($gsparams as $i => $v) {
                        if($i == 'Role.name'){
                             $conditions['AND']['OR']['User.id'] = $this->User->getIdLikeRoleName("%".$gs['global_search_keyword']."%");
                        }else{
                            $conditions['AND']['OR'][$i . " LIKE "] = "%" . $gs['global_search_keyword'] . '%';
                        }
                        
                    }
                }
                else {
                    if($gs['global_search_field'] == 'Role.name'){
                             $conditions['AND']['User.id'] = $this->User->getIdLikeRoleName("%".$gs['global_search_keyword']."%");
                        }else{
                            $conditions['AND'][$gs['global_search_field'] . " LIKE "] = "%" . $gs['global_search_keyword'] . '%';
                        }
                    
                }
            }
        }
        $this->User->recursive=1;
        $users    = $this->Paginator->paginate('User', $conditions);
        
        
        $user_ids = array();
        if ($users) {
            foreach ($users as $i => $user) {
                $user_ids[] = $user['User']['id'];
            }
        }
        $this->set('users_roles', $this->getUserRoles($user_ids));
        $this->set('users', $users);
        $this->set('SysAclUserActivation',
                $this->SysAcl->getAclForView('users', 'user_activation'));
        $this->set('protectedUsr', $this->getProtected('user'));
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null)
    {
        $this->SysAcl->isAcc('read');
        if (!$this->User->exists($id)) {
            $this->redirect($this->SysAcl->routeToCakeUrl('sysadmin.users.index'));
        }
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $id,
                'User.active_status'              => 1));
        $this->set('user', $this->User->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add()
    {

        $this->SysAcl->isAcc('add');
        if ($this->request->is('post')) {
            $this->User->create();
            $this->request->data['User']['active_status'] = 1;
            $this->request->data['User']['registered_dt'] = DboSource::expression('NOW()');
            //debug($this->request->data); die();
            if ($this->User->save($this->request->data)) {
                $id = $this->User->getLastInsertId();
                $this->Flash->success(__d('sysadmin', 'The user has been saved.'));
                return $this->redirect(array('action' => 'edit', $id));
            }
            else {
                $this->Flash->error(__d('sysadmin',
                                'The user could not be saved. Please, try again.'));
            }
        }
        $roles = $this->User->Role->find('list');

        $this->set(compact('roles'));
    }

    /**
     * before filter
     */
    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->_setTopMenu();
        $this->set('noPageTitle', true);
    }

    /**
     * 
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null)
    {
        $this->SysAcl->isAcc('edit', true);
        if (!$this->User->exists($id)) {
            $this->Flash->error(__d('sysadmin', 'User not found.'));
            return $this->redirect(array('action' => 'index'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->User->save($this->request->data)) {
                $this->Flash->success(__d('sysadmin', 'The user has been saved.'));
                return $this->redirect(array('action' => 'edit', $id));
            }
            else {
                $this->Flash->error(__d('sysadmin',
                                'The user could not be saved. Please, try again.'));
            }
        }
        else {
            $options             = array('conditions' => array('User.' . $this->User->primaryKey => $id));
            $this->request->data = $this->User->find('first', $options);
        }
        $user       = $this->request->data;
        $user_roles = array();
        if ($user) {
            if (isset($user['Role'])) {
                if ($user['Role']) {
                    foreach ($user['Role'] as $i => $role) {
                        $user_roles[] = $role['id'];
                    }
                }
            }
        }
        $this->User->Role->displayField = 'text';
        $roles                          = $this->User->Role->find('list');
        $top_menu                       = $this->_setTopMenu(false);
        $top_menu[]                     = array('title'   => '<span class="fa fa-edit fa-fw"></span> ' . __d('sysadmin',
                    'Edit User'), 'url'     => '#', 'options' => array('escape' => false),
            'active'  => true);

        $role_input = array();
        if ($this->SysAclView['delete'] && !in_array($id,
                        $this->SysAdmUserRoles->getProtected('user'))) {
            $top_menu[] = array(
                'link_type' => 'postLink',
                'title'     => '<span class="fa fa-trash fa-fw "></span> ' . __d('sysadmin',
                        'Delete User'), 'url'       => array(
                    'action' => 'delete', $user['User']['id']
                ), 'options'   => array(
                    'confirm' => __d('sysadmin',
                            'Are you sure you want to delete # %s?',
                            $user['User']['name']),
                    'class'   => 'text-danger',
                    'escape'  => false
                ), 'active'    => false);
            $role_input = array(
                'multiple' => 'multiple',
                'type'     => 'select',
                'options'  => $roles,
                'selected' => $user_roles
            );
        }
        $this->set(compact('roles', 'top_menu', 'role_input', 'user',
                        'user_roles'));
    }

    /**
     * 
     * get_protected method
     *
     */
    public function getProtected($var = "")
    {
        $ls = array();
        if ($var) {
            $ResDetail               = ClassRegistry::init('ResDetail');
            $ResDetail->useDbConfig  = 'sys_';
            $ResDetail->useTable     = "resources_details";
            $ResDetail->displayField = "value";
            $options                 = array('conditions' => array(
                    'name'          => "protected_{$var}_id",
                    'field'         => "{$var}_id",
                    'active_status' => 1
            ));
            $ls                      = $ResDetail->find('list', $options);
            $ResDetail->clear();
        }

        return $ls;
    }

    /**
     * 
     * getUserRoles method
     *
     */
    public function getUserRoles($user_ids = array())
    {
        $ls                      = array();
        $ResDetail               = ClassRegistry::init('UsrRole');
        $ResDetail->useDbConfig  = 'sys_';
        $ResDetail->useTable     = "users_roles";
        $ResDetail->displayField = "id";

        $options = array(
            'joins'      => array(
                array('table'      => 'roles',
                    'alias'      => 'Role',
                    'type'       => 'LEFT',
                    'conditions' => array(
                        'Role.id = UsrRole.role_id',
                    )
                )),
            'conditions' => array(
                'UsrRole.active_status' => 1,
                'UsrRole.user_id'       => $user_ids,
            ), 'fields'     => '*'
        );
        $rs      = $ResDetail->find('all', $options);
        if ($rs) {
            foreach ($rs as $i => $v) {
                $ls [$v['UsrRole']['user_id']][] = $v['Role'];
            }
        }
        return $ls;
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null)
    {
        $this->SysAcl->isAcc('delete');
        if (!in_array($id, $this->getProtected('user'))) {
            $this->User->id = $id;
            if (!$this->User->exists()) {
                $this->Flash->error(__d('sysadmin', 'User not found.'));
            }
            $this->request->allowMethod('post', 'delete');
            $this->User->set('active_status', DboSource::expression('NULL'));
            if ($this->User->save()) {
                $this->Flash->success(__d('sysadmin',
                                'The user has been deleted.'));
            }
            else {
                $this->Flash->error(__d('sysadmin',
                                'The user could not be deleted. Please, try again.'));
            }
        }
        else {
            $this->Flash->error(__d('sysadmin',
                            'The protected user could not be deleted.'));
        }

        return $this->redirect(array('action' => 'index'));
    }

    public function user_activation($id = null, $activate = 1)
    {
        $this->SysAcl->isAcc('execute', 'Sysadmin.users.user_activation',
                'user_activation');
        $this->User->id = $id;
        if (!$this->User->exists()) {
            $this->Flash->error(__d('sysadmin', 'User not found.'));
        }
        $this->request->allowMethod('post');
        $expr = $activate ? 'NOW()' : 'NULL';
        $msg  = $activate ? 'activated' : 'de-activated';
        $this->User->set('active_dt', DboSource::expression($expr));
        if ($this->User->save()) {
            $this->Flash->success(__d('sysadmin', 'The user has been ' . $msg));
        }
        else {
            $this->Flash->error(__d('sysadmin',
                            'Could not proceed, the user could not be ' . $msg));
        }
        return $this->redirect(array('action' => 'index'));
    }

}
