<?php
App::uses('SysadminAppController', 'Sysadmin.Controller');
/**
 * RolesPreferences Controller
 *
 * @property RolesPreference $RolesPreference
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class RolesPreferencesController extends SysadminAppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->RolesPreference->recursive = 0;
		$this->set('rolesPreferences', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->RolesPreference->exists($id)) {
			throw new NotFoundException(__('Invalid roles preference'));
		}
		$options = array('conditions' => array('RolesPreference.' . $this->RolesPreference->primaryKey => $id));
		$this->set('rolesPreference', $this->RolesPreference->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->RolesPreference->create();
			if ($this->RolesPreference->save($this->request->data)) {
				$this->Flash->success(__('The roles preference has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The roles preference could not be saved. Please, try again.'));
			}
		}
		$roles = $this->RolesPreference->Role->find('list');
		$this->set(compact('roles'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->RolesPreference->exists($id)) {
			throw new NotFoundException(__('Invalid roles preference'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->RolesPreference->save($this->request->data)) {
				$this->Flash->success(__('The roles preference has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The roles preference could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('RolesPreference.' . $this->RolesPreference->primaryKey => $id));
			$this->request->data = $this->RolesPreference->find('first', $options);
		}
		$roles = $this->RolesPreference->Role->find('list');
		$this->set(compact('roles'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->RolesPreference->id = $id;
		if (!$this->RolesPreference->exists()) {
			throw new NotFoundException(__('Invalid roles preference'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->RolesPreference->delete()) {
			$this->Flash->success(__('The roles preference has been deleted.'));
		} else {
			$this->Flash->error(__('The roles preference could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
