<?php
App::uses('SysadminAppController', 'Sysadmin.Controller');
/**
 * UsersPreferences Controller
 *
 * @property UsersPreference $UsersPreference
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class UsersPreferencesController extends SysadminAppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->UsersPreference->recursive = 0;
		$this->set('usersPreferences', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->UsersPreference->exists($id)) {
			throw new NotFoundException(__('Invalid users preference'));
		}
		$options = array('conditions' => array('UsersPreference.' . $this->UsersPreference->primaryKey => $id));
		$this->set('usersPreference', $this->UsersPreference->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->UsersPreference->create();
			if ($this->UsersPreference->save($this->request->data)) {
				$this->Flash->success(__('The users preference has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The users preference could not be saved. Please, try again.'));
			}
		}
		$users = $this->UsersPreference->User->find('list');
		$this->set(compact('users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->UsersPreference->exists($id)) {
			throw new NotFoundException(__('Invalid users preference'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->UsersPreference->save($this->request->data)) {
				$this->Flash->success(__('The users preference has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The users preference could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('UsersPreference.' . $this->UsersPreference->primaryKey => $id));
			$this->request->data = $this->UsersPreference->find('first', $options);
		}
		$users = $this->UsersPreference->User->find('list');
		$this->set(compact('users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->UsersPreference->id = $id;
		if (!$this->UsersPreference->exists()) {
			throw new NotFoundException(__('Invalid users preference'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->UsersPreference->delete()) {
			$this->Flash->success(__('The users preference has been deleted.'));
		} else {
			$this->Flash->error(__('The users preference could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
