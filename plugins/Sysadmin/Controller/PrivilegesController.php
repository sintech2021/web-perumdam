<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('SysadminAppController', 'Sysadmin.Controller');

/**
 * CakePHP PrivilegesController
 * @author ino
 */
class PrivilegesController extends SysadminAppController
{

    public $uses       = array(
        /* 'Sysadmin.Resource',
          'Sysadmin.ResourcesGroup',
          'Sysadmin.ResourcesType', */
        'Sysadmin.Proceeding',
        'Sysadmin.ResourcesProceeding', /*
          'Sysadmin.SysAclResGr',
          'Sysadmin.SysAclResType', */
        'Sysadmin.PrivilegesResource',
        'Sysadmin.PrivilegesResourcesGroup',
        'Sysadmin.PrivilegesResourcesType',
        'Sysadmin.PrivilegesResourcesDetail',
    );
    public $components = array(
        'Sysadmin.SysAcl',
        'Sysadmin.SysAdmPrivileges',
        'Sysadmin.SysAdmResources',
        'Paginator',
        'Flash',
        'Session'
    );

    /**
     * Set up the top menu for each rendered page
     * @visibility protected
     * @param void
     * @return void
     */
    protected function _setTopMenu($set = TRUE)
    {
        $index = array();
        $add   = array();
        if ($this->SysAclView['browse']) {
            $index = array(
                'title'   => '<span class="fa fa-list fa-fw"></span> ' . __d('sysadmin',
                        'Index'),
                'url'     => $this->SysAcl->routeToCakeUrl('sysadmin.privileges.index'),
                'options' => array('escape' => false),
                'active'  => false,
            );
            if ($this->request->action == 'index') {
                $index['active'] = true;
            }
        }
        if ($this->SysAclView['add']) {
            $add = array(
                'title'   => '<span class="fa fa-plus fa-fw"></span> ' . __d('sysadmin',
                        'Add privileges'),
                'url'     => $this->SysAcl->routeToCakeUrl('sysadmin.privileges.add'),
                'options' => array('escape' => false),
                'active'  => false,
            );
            if ($this->request->action == 'add') {
                $add['active'] = true;
            }
        }
        $top_menu = array(
            $index,
            $add
        );
        if ($set) {
            $this->set(compact('top_menu'));
        }
        else {
            return $top_menu;
        }
    }

    public function add()
    {
        $this->SysAcl->isAcc('add', true);
        $resgroupids = $this->PrivilegesResourcesGroup->getIdByName(array('sysadmin_app_menu'));
        if ($this->request->is('post')) {
            $data                                                  = $this->request->data;
            $resgrkeys                                             = array_keys($resgroupids);
            $data['PrivilegesResource']['resources_group_id']      = $resgrkeys[0];
            $data['PrivilegesResource']['active_status']           = 1;
            $id                                                    = CakeString::uuid();
            $data['PrivilegesResource']['id']                      = $id;
            $this->PrivilegesResource->validate['name']['message'] = __d('sysadmin',
                    'Required and unique value');
            if ($this->PrivilegesResource->save($data)) {
                $this->Flash->success(__d('sysadmin',
                                'The privileges has been saved.'));
                $this->PrivilegesResource->Behaviors->load('Tree');
                $this->PrivilegesResource->recover();
                $this->redirect(array('action' => 'edit', $id, $resgrkeys[0]));
            }
            else {
                $this->Flash->error(__d('sysadmin',
                                'The privileges could not be saved. Please, try again.'));
            }
        }

        $resources_types = $this->PrivilegesResourcesType->getListByExcludedNames(array(
            'bs_dropdown_sep',
            'preferences'));
        $parents         = $this->SysAdmResources->getParents(
                array(),
                array(
            "SysAclRes.resources_group_id " => $resgroupids,
                )
        );

        $this->set(compact('parents', 'resources_types'));
    }

    public function add_detail($id = null, $group_id = null)
    {
        if (!$id || !$group_id) {
            return $this->redirect(array('action' => 'index'));
        }

        $options  = array(
            'conditions' => array(
                'PrivilegesResource.id'                 => $id,
                'PrivilegesResource.resources_group_id' => $group_id,
                'PrivilegesResource.active_status'      => 1
            )
        );
        $resource = $this->PrivilegesResource->find('first', $options);
        if (!$resource) {
            $this->Flash->error(__d('sysadmin', 'Privilege not found.'));
            return $this->redirect(array('action' => 'index'));
        }

        $this->SysAcl->isAcc('edit', true);
        if ($this->request->is('post')) {
            $data                                             = $this->request->data;
            $detail_id                                        = CakeString::uuid();
            $data['PrivilegesResourcesDetail']['resource_id'] = $id;
            $data['PrivilegesResourcesDetail']['id']          = $detail_id;

            if ($this->PrivilegesResourcesDetail->save($data)) {
                $this->Flash->success(__d('sysadmin', 'Detail has ben saved.'));
                return $this->redirect(array('action' => 'edit_detail', $id, $group_id,
                            $detail_id));
            }
        }
        $top_menu   = $this->_setTopMenu(false);
        $top_menu[] = array('title'   => '<span class="fa fa-edit fa-fw"></span> ' . __d('sysadmin',
                    'Edit Privilege'), 'url'     => array('action' => 'edit', $id,
                $group_id), 'options' => array('escape' => false),
            'active'  => false);
        $top_menu[] = array('title'   => '<span class="fa fa-cog fa-fw"></span> ' . __d('sysadmin',
                    'Detail Privilege'), 'url'     => array('action' => 'detail',
                $id, $group_id), 'options' => array('escape' => false),
            'active'  => false);
        $top_menu[] = array('title'   => '<span class="fa fa-plus fa-fw"></span> ' . __d('sysadmin',
                    'Add Detail'), 'url'     => '#',
            'options' => array('escape' => false),
            'active'  => true);

        $this->set(compact('top_menu', 'resource'));
    }

    public function delete($id, $group_id)
    {
        $this->SysAcl->isAcc('delete', true);
        $err = false;

        $options = array(
            'conditions' => array(
                'PrivilegesResource.id'                 => $id,
                'PrivilegesResource.resources_group_id' => $group_id,
                'PrivilegesResource.active_status'      => 1
            )
        );

        if (!$this->PrivilegesResource->find('first', $options)) {
            $this->Flash->error(__d('sysadmin', 'Privilege not found.'));
            $err = true;
        }
        if (!$err) {
            $this->PrivilegesResource->id                     = $id;
            $this->PrivilegesResource->validate['name']['on'] = 'create';
            if ($this->PrivilegesResource->save(array('PrivilegesResource' => array(
                            'active_status' => 0)))) {
                $this->Flash->success(__d('sysadmin',
                                'Privilege has been deleted.'));
            }
            else {

                $this->Flash->error(__d('sysadmin', 'Privilege action error.'));
            }
            $this->PrivilegesResource->validate['name']['on'] = null;
        }

        return $this->redirect(array('action' => 'index'));
    }

    /**
     * 
     * @param type $id
     * @param type $group_id
     * @param type $detail_id
     */
    public function delete_detail($id, $group_id, $detail_id)
    {
        if (!$id || !$group_id) {
            return $this->redirect(array('action' => 'index'));
        }
        $this->SysAcl->isAcc('delete', true);
        $options = array(
            'conditions' => array(
                'PrivilegesResource.id'                 => $id,
                'PrivilegesResource.resources_group_id' => $group_id,
                'PrivilegesResource.active_status'      => 1
            )
        );
        if (!$this->PrivilegesResource->find('first', $options)) {
            $this->Flash->error(__d('sysadmin', 'Privilege not found.'));
            return $this->redirect(array('action' => 'index'));
        }        
        if (!$this->PrivilegesResourcesDetail->exists($detail_id)) {
            $this->Flash->error('sysadmin', 'Detail not found.');
            return $this->redirect(array('action'=> 'detail', $id, $group_id));
        }
        
        $this->PrivilegesResourcesDetail->id                     = $detail_id;
        if ($this->PrivilegesResourcesDetail->save(array('PrivilegesResourcesDetail' => array(
                        'active_status' => 0)))) {
            $this->Flash->success(__d('sysadmin',
                            'Privilege Detail has been deleted.'));
        } else {
            $this->Flash->error(__d('sysadmin', 'Privilege Detail action error.'));
        }
        return $this->redirect(array('action'=> 'detail', $id, $group_id));
    }
    /**
     * Edit Privileges
     * @param type $id
     * @param type $group_id
     * @return type
     */
    public function edit($id, $group_id)
    {
        if (!$id || !$group_id) {
            return $this->redirect(array('action' => 'index'));
        }
        $this->SysAcl->isAcc('edit', true);
        $options = array(
            'conditions' => array(
                'PrivilegesResource.id'                 => $id,
                'PrivilegesResource.resources_group_id' => $group_id,
                'PrivilegesResource.active_status'      => 1
            )
        );

        if (!$this->PrivilegesResource->find('first', $options)) {
            $this->Flash->error(__d('sysadmin', 'Privilege not found.'));
            return $this->redirect(array('action' => 'index'));
        }

        if ($this->request->is(array('post', 'put'))) {
            if ($this->PrivilegesResource->save($this->request->data)) {
                $this->Flash->success(__d('sysadmin',
                                'Privilege has been saved.'));
                return $this->redirect(array('action' => 'edit', $id, $group_id));
            }
            else {
                $this->Flash->error(__d('sysadmin',
                                'Privilege could not be saved. Please, try again.'));
            }
        }
        else {
            $options             = array('conditions' => array('PrivilegesResource.' . $this->PrivilegesResource->primaryKey => $id));
            $this->request->data = $this->PrivilegesResource->find('first',
                    $options);
        }
        $resource   = $this->request->data;
        $top_menu   = $this->_setTopMenu(false);
        $top_menu[] = array('title'   => '<span class="fa fa-edit fa-fw"></span> ' . __d('sysadmin',
                    'Edit Privilege'), 'url'     => '#', 'options' => array('escape' => false),
            'active'  => true);
        $top_menu[] = array('title'   => '<span class="fa fa-cog fa-fw"></span> ' . __d('sysadmin',
                    'Detail Privilege'), 'url'     => array('action' => 'detail',
                $id, $group_id), 'options' => array('escape' => false),
            'active'  => false);

        $resgroupids     = $this->PrivilegesResourcesGroup->getIdByName(array('sysadmin_app_menu'));
        $resources_types = $this->PrivilegesResourcesType->getListByExcludedNames(array(
            'bs_dropdown_sep',
            'preferences'));
        $parents         = $this->SysAdmResources->getParents(
                array(),
                array(
            "SysAclRes.resources_group_id " => $resgroupids,
                )
        );
        
        $proceedings = $this->Proceeding->find('list', array('conditions'=>array('active_status'=>'1')));
        $this->set(compact('resource', 'top_menu', 'parents', 'resources_types', 'proceedings'));
    }

    public function detail($id = null, $group_id = null)
    {
        if (!$id || !$group_id) {
            return $this->redirect(array('action' => 'index'));
        }

        $this->SysAcl->isAcc('edit', true);
        $options  = array(
            'conditions' => array(
                'PrivilegesResource.id'                 => $id,
                'PrivilegesResource.resources_group_id' => $group_id,
                'PrivilegesResource.active_status'      => 1
            )
        );
        $resource = $this->PrivilegesResource->find('first', $options);
        if (!$resource) {
            $this->Flash->error(__d('sysadmin', 'Privilege not found.'));
            return $this->redirect(array('action' => 'index'));
        }

        $top_menu   = $this->_setTopMenu(false);
        $top_menu[] = array('title'   => '<span class="fa fa-edit fa-fw"></span> ' . __d('sysadmin',
                    'Edit Privilege'), 'url'     => array('action' => 'edit', $id,
                $group_id), 'options' => array('escape' => false),
            'active'  => false);
        $top_menu[] = array('title'   => '<span class="fa fa-cog fa-fw"></span> ' . __d('sysadmin',
                    'Detail Privilege'), 'url'     => '#', 'options' => array('escape' => false),
            'active'  => true);
        $top_menu[] = array('title'   => '<span class="fa fa-plus fa-fw"></span> ' . __d('sysadmin',
                    'Add Detail'), 'url'     => array('action' => 'add_detail', $id,
                $group_id),
            'options' => array('escape' => false),
            'active'  => false);

        $conditions        = array(
            'PrivilegesResourcesDetail.resource_id'   => $resource['PrivilegesResource']['id'],
            'PrivilegesResourcesDetail.active_status' => 1,
        );
        $resources_details = $this->Paginator->paginate('PrivilegesResourcesDetail',
                $conditions);
        $this->set(compact('resource', 'top_menu', 'resources_details',
                        'group_id'));
    }

    public function edit_detail($id, $group_id, $detail_id)
    {
        $this->SysAcl->isAcc('edit', true);
        $options  = array(
            'conditions' => array(
                'PrivilegesResource.id'                 => $id,
                'PrivilegesResource.resources_group_id' => $group_id,
                'PrivilegesResource.active_status'      => 1
            )
        );
        $resource = $this->PrivilegesResource->find('first', $options);
        if (!$resource) {
            $this->Flash->error(__d('sysadmin', 'Privilege not found.'));
            return $this->redirect(array('action' => 'index'));
        }

        if (!$this->PrivilegesResourcesDetail->exists($detail_id)) {
            $this->Flash->error('sysadmin', 'Detail not found.');
            return $this->redirect(array('action'=> 'detail', $id, $group_id));
        }
        
        if ($this->request->is(array('post', 'put'))) {
            if ($this->PrivilegesResourcesDetail->save($this->request->data)) {
                $this->Flash->success(__d('sysadmin',
                                'Privilege detail has been saved.'));
                return $this->redirect(array('action' => 'edit_detail', $id, $group_id, $detail_id));
            }
            else {
                $this->Flash->error(__d('sysadmin',
                                'Privilege detail could not be saved. Please, try again.'));
            }
        }
        
        $options             = array('conditions' => array('PrivilegesResourcesDetail.' . $this->PrivilegesResourcesDetail->primaryKey => $detail_id));
        $this->request->data = $this->PrivilegesResourcesDetail->find('first',
                    $options);
//fatty
        $top_menu   = $this->_setTopMenu(false);
        $top_menu[] = array('title'   => '<span class="fa fa-edit fa-fw"></span> ' . __d('sysadmin',
                    'Edit Privilege'), 'url'     => array('action' => 'edit', $id,
                $group_id), 'options' => array('escape' => false),
            'active'  => false);
        $top_menu[] = array('title'   => '<span class="fa fa-cog fa-fw"></span> ' . __d('sysadmin',
                    'Detail Privilege'), 'url'     => array('action' => 'detail',
                $id, $group_id), 'options' => array('escape' => false),
            'active'  => false);
        $top_menu[] = array('title'   => '<span class="fa fa-plus fa-fw"></span> ' . __d('sysadmin',
                    'Add Detail'), 'url'     => array('action' => 'add_detail', $id,
                $group_id),
            'options' => array('escape' => false),
            'active'  => false);
        $top_menu[] = array('title'   => '<span class="fa fa-edit fa-fw"></span> ' . __d('sysadmin',
                    'Edit Detail'), 'url'     => '#',
            'options' => array('escape' => false),
            'active'  => true);
        // ./fatty
        
        $this->set(compact('resource', 'top_menu', 'group_id'));
    }

    public function index()
    {
        $this->SysAcl->isAcc();
        $gsparams = array(
            'PrivilegesResource.name'=> __d('sysadmin', 'ID'),
            'PrivilegesResource.text'=> __d('sysadmin', 'Privileges'),
            'ParentResource.text'=> __d('sysadmin', 'Parent'),
        );
        $this->_setGlobalSearch($gsparams);

        $redir = false;
        // update permission
        if ($this->SysAcl->isAcc('edit', 'sysadmin.privileges.index', 'index',
                        false)) {
            extract($this->ResourcesProceeding->privilegesSetPermission($this->request->params));
            if ($this->ResourcesProceeding->error) {
                $this->Flash->error($this->ResourcesProceeding->error);
            }
            if ($this->ResourcesProceeding->successMsg) {
                $this->Flash->success($this->ResourcesProceeding->successMsg);
            }
        }
        if (isset($redirect_url) && $redir) {
            return $this->redirect($redirect_url);
        }
        //./update permission
        $options['AND']            = array(
            'PrivilegesResource.active_status' => 1,
                # 'Resource.id' => $res_ids,
                # 'Resource.resources_group_id' => $app_resgroup_id,
        );
        if($this->isGs){
            $gs = $this->_getGlobalSearch();
            if($gs['global_search_field']){
                if($gs['global_search_field'] == $this->allSearchKey){
                    foreach($gsparams as $i => $v){
                        $options['AND']['OR'][$i." LIKE "] = "%".$gs['global_search_keyword'].'%';
                    }
                } else {
                    $options['AND'][$gs['global_search_field']." LIKE "] = "%".$gs['global_search_keyword'].'%';
                }
            }
        }
        
        
        $options['OR']             = array('PrivilegesResourcesGroup.name' => 'sysadmin_app_menu',
            'PrivilegesResourcesType.name'  => 'sys_action',);
        $this->Paginator->settings = array(
            // 'limit' => 20,
            'order' => array(
                #'ParentResource.lft' => 'asc',
                'PrivilegesResource.lft'        => 'asc',
                'PrivilegesResource.menu_order' => 'asc',
            )
        );
        # $this->Resource->recursive=0;

        $resources = $this->Paginator->paginate('PrivilegesResource', $options);

        // loop the paging
        // fetch corresponding res_id::proc_id
        $res_ids2 = array();

        if ($resources) {
            foreach ($resources as $i => $v) {
                $res_ids2[] = $v['PrivilegesResource']['id'];
            }
        }
        //get parents
        $parents    = array();
        $privileges = array();
        if ($res_ids2) {
            //$this->ResourcesProceeding->clear();
            $res_proc_ls = $this->ResourcesProceeding->find('all',
                    array(
                'conditions' => array(
                    'ResourcesProceeding.resource_id'   => $res_ids2,
                    'ResourcesProceeding.active_status' => 1,
                )
                    )
            );
            #debug($res_proc_ls);die();
            if ($res_proc_ls) {
                foreach ($res_proc_ls as $i => $v) {
                    $privileges[$v['ResourcesProceeding']['resource_id']][$v['ResourcesProceeding']['proceeding_id']]
                            = true;
                }
            }
        }

        //

        $priv_names  = $this->PrivilegesResource->getTreeListTextAndNameByRS($resources);
        $proceedings = $this->Proceeding->find('all');
        $this->set(compact('resources', 'privileges', 'proceedings',
                        'priv_names'));
    }

}
