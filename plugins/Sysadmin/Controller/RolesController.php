<?php

App::uses('SysadminAppController', 'Sysadmin.Controller');

/**
 * Roles Controller
 *
 * @property Role $Role
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class RolesController extends SysadminAppController
{

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Flash', 'Session', 'Sysadmin.SysAdmUserRoles');
    public $uses       = array(
        'Sysadmin.Role',
        'Sysadmin.RolesProceeding',
        'Sysadmin.ResourcesProceeding',
        'Sysadmin.Resource',
        'Sysadmin.Proceeding',
        'Sysadmin.ResourcesGroup',
        'Sysadmin.SysAclRes',
        'Sysadmin.SysAclResGr',
        'Sysadmin.SysAclResType',
    );

    /**
     * Set up the top menu for each rendered page
     * @visibility protected
     * @param void
     * @return void
     */
    protected function _setTopMenu($set = TRUE)
    {
        $index = array();
        $add   = array();
        if ($this->SysAclView['browse']) {
            $index = array(
                'title'   => '<span class="fa fa-list fa-fw"></span> ' . __d('sysadmin',
                        'Index'),
                'url'     => $this->SysAcl->routeToCakeUrl('sysadmin.roles.index'),
                'options' => array('escape' => false),
                'active'  => false,
            );
            if ($this->request->action == 'index') {
                $index['active'] = true;
            }
        }
        if ($this->SysAclView['add']) {
            $add = array(
                'title'   => '<span class="fa fa-plus fa-fw"></span> ' . __d('sysadmin',
                        'Add role'),
                'url'     => $this->SysAcl->routeToCakeUrl('sysadmin.roles.add'),
                'options' => array('escape' => false),
                'active'  => false,
            );
            if ($this->request->action == 'add') {
                $add['active'] = true;
            }
        }
        $top_menu = array(
            $index,
            $add
        );
        if ($set) {
            $this->set(compact('top_menu'));
        }
        else {
            return $top_menu;
        }
    }

    /**
     * index method
     *
     * @return void
     */
    public function index()
    {
        $this->Role->recursive = 0;

        $gsparams = array(
            'Role.name'       => __d('sysadmin', 'Name'),
            'Role.text'       => __d('sysadmin', 'Full Text'),
            'Role.annotation' => __d('sysadmin', 'Description'),
        );
         $this->_setGlobalSearch($gsparams);

        $conditions = array(
            'AND' => array(
                'Role.active_status' => 1
            )
        );
        if ($this->isGs) {
            $gs = $this->_getGlobalSearch();
            if ($gs['global_search_field']) {
                if ($gs['global_search_field'] == $this->allSearchKey) {
                    foreach ($gsparams as $i => $v) {

                        $conditions['AND']['OR'][$i . " LIKE "] = "%" . $gs['global_search_keyword'] . '%';
                    }
                }
                else {

                    $conditions['AND'][$gs['global_search_field'] . " LIKE "] = "%" . $gs['global_search_keyword'] . '%';
                }
            }
        }
        $this->set('roles', $this->Paginator->paginate($conditions));
        $this->set('SysAclPrivilegesManagement',
                $this->SysAcl->getAclForView('roles', 'privileges_management'));
        $this->set('protected_roles',
                $this->SysAdmUserRoles->getProtected('role'));
    }

    /**
     * 
     */
    public function privileges_management($id)
    {
        $this->SysAcl->isAcc('browse', 'sysadmin.roles.privileges_management',
                'privileges_management');
        if (!$this->Role->exists($id)) {
            $this->redirect(array('action' => 'index'));
            throw new NotFoundException(__d('sysadmin', 'Invalid role'));
        }

        
        
        // update privileges
        if ($this->SysAcl->isAcc('edit', 'sysadmin.roles.privileges_management',
                        'privileges_management', false)) {
            $req_params = $this->request->params;
            $named      = isset($req_params['named']) ? $req_params['named'] : array();

            if ($named) {
                $pageno = isset($named['page']) ? $named['page'] : 1;

                if (isset($named['priv_act'])) {
                    $act_status = $named['priv_act'] == md5('set' . date('j')) ? 1
                                : NULL;
                    $options    = array(
                        'conditions' => array(
                            'resource_id'   => $named['res_id'],
                            'proceeding_id' => $named['proc_id'],
                        )
                    );
                    #debug($act_status);die();
                    //fetch res proc id
                    $r          = $this->ResourcesProceeding->find('first',
                            $options);
                    #debug($r);die();
                    $err        = false;
                    if ($r) {
                        if (isset($r['ResourcesProceeding']['id'])) {
                            $rid     = $r['ResourcesProceeding']['id'];
                            $options = array(
                                'conditions' => array(
                                    'role_id'                 => $id,
                                    'resources_proceeding_id' => $rid,
                                )
                            );
                            $r       = $this->RolesProceeding->find('first',
                                    $options);
                            #debug($r);die();
                            if ($r) {
                                $rid2 = $r['RolesProceeding']['id'];
                                $this->RolesProceeding->clear();
                                if (!$this->RolesProceeding->exists($rid2)) {
                                    $this->RolesProceeding->create();
                                }
                                else {
                                    $this->RolesProceeding->id = $rid2;
                                }
                            }

                            $update_data = array(
                                'RolesProceeding' => array('role_id'                 => $id,
                                    'resources_proceeding_id' => $rid,
                                    'active_status'           => $act_status,
                            ));
                            if ($this->RolesProceeding->save($update_data)) {
                                
                            }
                        }
                    }
                    $this->redirect(array('action' => 'privileges_management', $id,
                        'page'   => $pageno));
                }
            }
        }
        // ./update privileges
        // get permissions
        $role                                = $this->Role->find('first',
                array('conditions' => array('Role.active_status' => 1, 'Role.id' => $id)));
        $this->RolesProceeding->displayField = 'resources_proceeding_id';
        $res_proc_ids                        = $this->RolesProceeding->find('list',
                array('conditions' => array('RolesProceeding.role_id' => $id, 'RolesProceeding.active_status' => 1)));
        //

        $res_procs   = $this->ResourcesProceeding->find('all',
                array('conditions' => array('ResourcesProceeding.id' => $res_proc_ids)));
        $permissions = array();
        $res_ids     = array();
        if ($res_procs) {
            foreach ($res_procs as $res_proc) {
                $res_id                         = $res_proc['ResourcesProceeding']['resource_id'];
                $proc_id                        = $res_proc['ResourcesProceeding']['proceeding_id'];
                $res_ids[$res_id]               = $res_id;
                $permissions[$res_id][$proc_id] = $res_proc['ResourcesProceeding']['active_status'];
            }
        }
        // ./ get permissions
        // get resources privileges
        #$app_resgroup = $this->ResourcesGroup->find('first', array('conditions'=> array('name'=>'sysadmin_app_menu')));
        #$app_resgroup_id = isset($app_resgroup['ResourcesGroup']) ? $app_resgroup['ResourcesGroup']['id'] : null;
        $app_resgroup_id = $this->SysAclResGr->getIdByName(array('sysadmin_app_menu'));
        $app_restype_id  = $this->SysAclResType->getIdByName(array('sys_action'));
        # debug($app_resgroup);die();
        $options         = array();
        
        $options['AND']  = array(
            'Resource.active_status' => 1,
        );

        $options['OR'] = array(
            'Resource.resources_group_id' => $app_resgroup_id,
            'Resource.resources_type_id'  => $app_restype_id,
        );

        //global search
        $gsparams = array(
            'Resource.name'=> __d('sysadmin', 'ID'),
            'Resource.text'=> __d('sysadmin', 'Privileges'),
            'ParentResource.text'=> __d('sysadmin', 'Parent'),
        );
        $this->_setGlobalSearch($gsparams);
        if($this->isGs){
            $gs = $this->_getGlobalSearch();
            if($gs['global_search_field']){
                if($gs['global_search_field'] == $this->allSearchKey){
                    foreach($gsparams as $i => $v){
                        $options['AND']['OR'][$i." LIKE "] = "%".$gs['global_search_keyword'].'%';
                    }
                } else {
                    $options['AND'][$gs['global_search_field']." LIKE "] = "%".$gs['global_search_keyword'].'%';
                }
            }
        }
        //./global search
        

        $this->Paginator->settings = array(
            //'limit' => 10,
            'order' => array(
                'Resource.lft' => 'asc')
        );

        // resource as privileges

        $resources = $this->Paginator->paginate('Resource', $options);

        //./get resources privileges
        // loop the paging
        // fetch corresponding res_id::proc_id
        $res_ids2 = array();
        if ($resources) {
            foreach ($resources as $i => &$v) {
                $res_ids2[] = $v['Resource']['id'];
            }
        }
        $priv_names = $this->SysAclRes->getTreeListTextAndName($res_ids2);

        $privileges = array();
        if ($res_ids2) {
            $this->ResourcesProceeding->clear();
            $res_proc_ls = $this->ResourcesProceeding->find('all',
                    array(
                'conditions' => array(
                    'ResourcesProceeding.resource_id'   => $res_ids2,
                    'ResourcesProceeding.active_status' => 1,
                )
                    )
            );
            if ($res_proc_ls) {
                foreach ($res_proc_ls as $i => $v) {
                    $privileges[$v['Resource']['id']][$v['Proceeding']['id']] = true;
                }
            }
        }

        //

        $top_menu    = $this->_setTopMenu(false);
        $top_menu[]  = array('title'   => '<span class="fa fa-cog fa-fw"></span> ' . __d('sysadmin',
                    'Privileges Management'), 'url'     => '#', 'options' => array(
                'escape' => false),
            'active'  => true);
        #die();
        $proceedings = $this->Proceeding->find('all');

        $this->set(compact('top_menu', 'role', 'resources', 'permissions',
                        'proceedings', 'privileges', 'priv_names'));
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null)
    {
        $this->SysAcl->isAcc('read');
        if (!$this->Role->exists($id)) {
            throw new NotFoundException(__d('sysadmin', 'Invalid role'));
        }
        $options = array('conditions' => array('Role.' . $this->Role->primaryKey => $id));
        $this->set('role', $this->Role->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add()
    {
        $this->SysAcl->isAcc('add');
        if ($this->request->is('post')) {
            $this->Role->create();
            $this->request->data['Role']['active_status'] = 1;
            if ($this->Role->save($this->request->data)) {
                $this->Flash->success(__d('sysadmin', 'The role has been saved.'));
                return $this->redirect(array('action' => 'index'));
            }
            else {
                $this->Flash->error(__d('sysadmin',
                                'The role could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null)
    {
        if (!$this->Role->exists($id)) {
            throw new NotFoundException(__('Invalid role'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Role->save($this->request->data)) {
                $this->Flash->success(__d('sysadmin', 'The role has been saved.'));
                return $this->redirect(array('action' => 'index'));
            }
            else {
                $this->Flash->error(__d('sysadmin',
                                'The role could not be saved. Please, try again.'));
            }
        }
        else {
            $options             = array('conditions' => array('Role.' . $this->Role->primaryKey => $id));
            $this->request->data = $this->Role->find('first', $options);
        }
        $role       = $this->request->data;
        $top_menu   = $this->_setTopMenu(false);
        $top_menu[] = array('title'   => '<span class="fa fa-edit fa-fw"></span> ' . __d('sysadmin',
                    'Edit Role'), 'url'     => '#', 'options' => array('escape' => false),
            'active'  => true);
        if ($this->SysAclView['delete'] && !in_array($id,
                        $this->SysAdmUserRoles->getProtected('role'))) {
            $top_menu[] = array(
                'link_type' => 'postLink',
                'title'     => '<span class="fa fa-trash fa-fw "></span> ' . __d('sysadmin',
                        'Delete Role'), 'url'       => array(
                    'action' => 'delete', $id
                ), 'options'   => array(
                    'confirm' => __d('sysadmin',
                            'Are you sure you want to delete # %s?',
                            $role['Role']['name']),
                    'class'   => 'text-danger',
                    'escape'  => false
                ), 'active'    => false);
        }
        $this->set(compact('top_menu'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null)
    {
        $this->Role->id = $id;
        if (!$this->Role->exists()) {
            throw new NotFoundException(__d('sysadmin', 'Invalid role'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Role->delete()) {
            $this->Flash->success(__d('sysadmin', 'The role has been deleted.'));
        }
        else {
            $this->Flash->error(__d('sysadmin',
                            'The role could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

}
