<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('SysadminAppController', 'Sysadmin.Controller');

/**
 * CakePHP AppMenusController
 * @author ino
 */
class ProfileMenusController extends SysadminAppController
{
    public $components = array('Sysadmin.SysAcl','Paginator', 'Flash', 'Session');
    public $uses = array('Resource');

    
    
    public function index()
    {
       
        $this->Resource->recursive = 0;
       
        $this->Resource->belongsTo = array(
		'ResourcesGroup' => array(
			'className' => 'ResourcesGroup',
			'foreignKey' => 'resources_group_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ResourcesType' => array(
			'className' => 'ResourcesType',
			'foreignKey' => 'resources_type_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ParentResource' => array(
			'className' => 'Resource',
			'foreignKey' => 'parent_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
        $this->Paginator->settings['order'] = array('Resource.lft' => 'asc');
        /*$this->Paginator->settings['fields'] = array('Resource.*','resources_group','resources_type');
        $this->Paginator->settings['Resource']['joins'] = array(
            array(
                'table' => 'resources_groups',
        'alias' => 'ResourcesGroup',
        'type' => 'LEFT',
        'conditions' => array(
            'ResourcesGroup.id = Resource.resources_group_id',
        )
            ),
            array(
                'table' => 'resources_types',
        'alias' => 'ResourcesType',
        'type' => 'LEFT',
        'conditions' => array(
            'ResourcesType.id = Resource.resources_type_id',
        )
            ),
        );*/
        $resources = $this->Paginator->paginate('Resource', array('ResourcesGroup.name' => 'sysadmin_profile_menu'));
        
        $this->set(compact('resources'));
    }

}
