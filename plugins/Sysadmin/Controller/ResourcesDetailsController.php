<?php
App::uses('SysadminAppController', 'Sysadmin.Controller');
/**
 * ResourcesDetails Controller
 *
 * @property ResourcesDetail $ResourcesDetail
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class ResourcesDetailsController extends SysadminAppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ResourcesDetail->recursive = 0;
		$this->set('resourcesDetails', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ResourcesDetail->exists($id)) {
			throw new NotFoundException(__('Invalid resources detail'));
		}
		$options = array('conditions' => array('ResourcesDetail.' . $this->ResourcesDetail->primaryKey => $id));
		$this->set('resourcesDetail', $this->ResourcesDetail->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ResourcesDetail->create();
			if ($this->ResourcesDetail->save($this->request->data)) {
				$this->Flash->success(__('The resources detail has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The resources detail could not be saved. Please, try again.'));
			}
		}
		$resources = $this->ResourcesDetail->Resource->find('list');
		$this->set(compact('resources'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ResourcesDetail->exists($id)) {
			throw new NotFoundException(__('Invalid resources detail'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ResourcesDetail->save($this->request->data)) {
				$this->Flash->success(__('The resources detail has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The resources detail could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ResourcesDetail.' . $this->ResourcesDetail->primaryKey => $id));
			$this->request->data = $this->ResourcesDetail->find('first', $options);
		}
		$resources = $this->ResourcesDetail->Resource->find('list');
		$this->set(compact('resources'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ResourcesDetail->id = $id;
		if (!$this->ResourcesDetail->exists()) {
			throw new NotFoundException(__('Invalid resources detail'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ResourcesDetail->delete()) {
			$this->Flash->success(__('The resources detail has been deleted.'));
		} else {
			$this->Flash->error(__('The resources detail could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
