<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('SysadminAppController', 'Sysadmin.Controller');

/**
 * CakePHP AppMenusController
 * @author ino
 */
class AppMenusController extends SysadminAppController
{
    public $components = array('Sysadmin.SysAcl','Paginator', 'Flash', 'Session');
    public $uses = array('Resource');

    public function index()
    {
       
        $this->Resource->recursive = 0;
       
        $this->Resource->belongsTo = array(
		'ResourcesGroup' => array(
			'className' => 'ResourcesGroup',
			'foreignKey' => 'resources_group_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ResourcesType' => array(
			'className' => 'ResourcesType',
			'foreignKey' => 'resources_type_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ParentResource' => array(
			'className' => 'Resource',
			'foreignKey' => 'parent_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
        $this->Paginator->settings['order'] = array('Resource.lft' => 'asc');
        
        $resources = $this->Paginator->paginate('Resource', array('ResourcesGroup.name' => 'sysadmin_app_menu'));
        
        $this->set(compact('resources'));
    }

}
