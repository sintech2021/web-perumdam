<?php
App::uses('SysadminAppController', 'Sysadmin.Controller');
/**
 * ActivitiesLogs Controller
 *
 * @property ActivitiesLog $ActivitiesLog
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class ActivitiesLogsController extends SysadminAppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ActivitiesLog->recursive = 0;
		$this->set('activitiesLogs', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ActivitiesLog->exists($id)) {
			throw new NotFoundException(__('Invalid activities log'));
		}
		$options = array('conditions' => array('ActivitiesLog.' . $this->ActivitiesLog->primaryKey => $id));
		$this->set('activitiesLog', $this->ActivitiesLog->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ActivitiesLog->create();
			if ($this->ActivitiesLog->save($this->request->data)) {
				$this->Flash->success(__('The activities log has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The activities log could not be saved. Please, try again.'));
			}
		}
		$users = $this->ActivitiesLog->User->find('list');
		$this->set(compact('users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ActivitiesLog->exists($id)) {
			throw new NotFoundException(__('Invalid activities log'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ActivitiesLog->save($this->request->data)) {
				$this->Flash->success(__('The activities log has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The activities log could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ActivitiesLog.' . $this->ActivitiesLog->primaryKey => $id));
			$this->request->data = $this->ActivitiesLog->find('first', $options);
		}
		$users = $this->ActivitiesLog->User->find('list');
		$this->set(compact('users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ActivitiesLog->id = $id;
		if (!$this->ActivitiesLog->exists()) {
			throw new NotFoundException(__('Invalid activities log'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ActivitiesLog->delete()) {
			$this->Flash->success(__('The activities log has been deleted.'));
		} else {
			$this->Flash->error(__('The activities log could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
