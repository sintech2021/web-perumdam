<?php

App::uses('Component', 'Controller');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * CakePHP SysLogComponent
 * @author ino
 */
class SysLogComponent extends Component
{

    public $components  = array();
    public $dbConfig    = 'sys_';
    public $useDbConfig = 'sys_';

    protected function _loadActivitesLog($alias = 'Sysadmin.ActLog')
    {
        $Res              = ClassRegistry::init($alias);
        $Res->useDbConfig = $this->dbConfig;
        $Res->useTable    = "activities_logs";
        return $Res;
    }

    public function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function writeSysLog($options = array())
    {
        $values              = array(
            'name'          => NULL,
            'user_id'       => NULL,
            'active_status' => 1,
            'text'          => NULL,
            'annotation'    => NULL,
            'menu_code'     => NULL,
            'menu_order'    => 0
        );
        $values['user_id']   = $this->controller->Session->read('AppUsrLog.uid');
        $values['menu_code'] = $this->controller->request->clientIp();
        if ($options) {

            if (isset($options['messages'])) {
                
                $values['annotation'] = json_encode($options['messages']);
            }
            if (isset($options['values'])) {
                if (is_array($options['values'])) {
                    foreach ($options['values'] as $i => $v) {
                        $values[$i] = $v;
                    }
                }
            }
        }
        if (!$values['user_id']) {
            $values['user_id'] = $this->controller->request->clientIp();
        }
        if (!$values['name']) {
            $values['name'] = 'Access Log';
        }
        if (!$values['text']) {
            $values['text'] = $this->controller->request->here;
        }
        if (!$values['annotation']) {
            $annot = array();
            $annot['request_params'] = $this->controller->request->params;
            $annot['session'] = $this->controller->Session->read();
            $annot['request_data'] = $this->controller->request->data;
            
            /*to-do*/
            /*
             * remove sensitive data on login
             */
            
            $values['annotation'] = json_encode($annot);
        }
        
        $ActLog = $this->_loadActivitesLog();
        $ActLog->create();
        $ActLog->save($values);
        
    }

}
