<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * CakePHP SysAdmUserRoles
 * @author ino
 */
class SysAdmUserRolesComponent extends Component
{

   /* public $components = array();

    public function initialize($controller)
    {
        
    }

    public function startup($controller)
    {
        
    }

    public function beforeRender($controller)
    {
        
    }

    public function shutDown($controller)
    {
        
    }

    public function beforeRedirect($controller, $url, $status = null,
            $exit = true)
    {
        
    }
    * 
    */
    /**
     * 
     * get_protected method
     *
     */
    public function getProtected($var = "")
    {
        $ls                      = array();
        if($var){
            $ResDetail               = ClassRegistry::init('Sysadmin.ResDetail');
            $ResDetail->useDbConfig  = 'sys_';
            $ResDetail->useTable     = "resources_details";
            $ResDetail->displayField = "value";
            $options                 = array('conditions' => array(
                    'name'          => "protected_{$var}_id",
                    'field'         => "{$var}_id",
                    'active_status' => 1
            ));
            $ls                      = $ResDetail->find('list', $options);
            $ResDetail->clear();
        }
        
        return $ls;
    }
    

}
