<?php

App::uses('Component', 'Controller');
App::uses('SessionComponent', 'Controller/Component');
App::uses('FlashComponent', 'Controller/Component');
App::uses('CakeString', 'Utility');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

/**
 * @name SysadminAppController::$SysAcl
 * @author ino <inoshadi@gmail.com>
 * 
 */
class SysAclComponent extends Component
{

    protected $profileMenu      = array();
    protected $appMenu          = array();
    public $Session;
    public $Flash;
    protected $requestRoute;
    protected $signInRedirRoute = array();
    protected $signInRoute      = "sysadmin.sysadmin_app.login";
    protected $signOutRoute     = "sysadmin.sysadmin_app.signout";
    protected $profileRoute     = "sysadmin.sysadmin_app.my_profile";
    protected $homeRoute        = "syscm.syscm_front.index";
    public $dbConfig            = 'sys_';
    public $useDbConfig         = 'sys_';
    /*

     * array $usersAllowed
     * List of allowed resources for signed users
     * consist of plugin.controller.action
     *      */
    public $usersAllowed        = array(
        'sysadmin.sysadmin_app.my_profile',
        'sysadmin.sysadmin_app.change_password',
        'sysadmin.sysadmin_app.signout',
        'sysadmin.sysadmin_app.user_preferences',
        'sysadmin.sysadmin_app.help',
        'sysadmin.sysadmin_app.my_activities',
    );
    public $Resource;

    /* attr end */
    /* function */

    public function __construct(\ComponentCollection $collection,
            $settings = array())
    {
        parent::__construct($collection, $settings);

        if (Configure::check('Sysconfig.signInRedirRoute')) {
            $this->signInRedirRoute = Configure::read('Sysconfig.signInRedirRoute');
        }
        $this->Session = new SessionComponent(new ComponentCollection());
        $this->Flash   = new FlashComponent(new ComponentCollection());
    }

    protected function _getRecaptcha($response)
    {
        /**
         * secret 	Required. The shared key between your site and ReCAPTCHA.
          response 	Required. The user response token provided by the reCAPTCHA to the user and provided to your site on.
          remoteip 	Optional. The user's IP address.
         * 'https://www.google.com/recaptcha/api/siteverify';
         * '6LcYdRMTAAAAAPfVVt7_HDwUBsVKEAgO7lBxS6h7';
         */
        $url                    = Configure::read('Sysconfig.recaptcha.api_url');
        $parameters['secret']   = Configure::read('Sysconfig.recaptcha.api_secret');
        $parameters['response'] = $response;
        $data                   = http_build_query($parameters);

        // Initialize the PHP curl agent
        $ch     = curl_init();
        curl_setopt($ch, CURLOPT_USERAGENT, "curl");
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result);
    }

    protected function _getUsr($id, $unbindActLog = true)
    {
        $User = ClassRegistry::init('Sysadmin.SysAclUser');

        if ($unbindActLog) {
            $User->unbindModel(array('hasMany' => array('ActivitiesLog')));
        }
        $Usr = $User->find('first',
                array(
            'conditions' => array(
                'SysAclUser.id'            => $id,
                'SysAclUser.active_status' => 1,
                'UNIX_TIMESTAMP(SysAclUser.active_dt) <= UNIX_TIMESTAMP()',
            )
                )
        );
        if ($Usr) {
            if (isset($Usr['SysAclUser']['password'])) {
                unset($Usr['SysAclUser']['password']);
            }
            if (isset($Usr['Role'])) {
                if (isset($Usr['Role'][0])) {
                    if (isset($Usr['Role'][0]['id'])) {
                        $Usr['SysAclUser']['role_id'] = $Usr['Role'][0]['id'];
                    }
                }
            }
        }
        $User->clear();
        return $Usr;
    }

    protected function _loadResources()
    {
        $Res              = ClassRegistry::init('Sysadmin.SysAclRes');
        $Res->useDbConfig = $this->dbConfig;
        $Res->useTable    = "resources";
        $Res->actsAs      = array('Containable');
        $params           = array(
            'belongsTo' => array(
                'ResourcesGroup' => array(
                    'className'  => 'Sysadmin.ResourcesGroup',
                    'foreignKey' => 'resources_group_id',
                ),
                'ResourcesType'  => array(
                    'className'  => 'Sysadmin.ResourcesType',
                    'foreignKey' => 'resources_type_id',
                ),
                'ParentResource' => array(
                    'className'  => 'Sysadmin.Resource',
                    'foreignKey' => 'parent_id',
                )
            ),
            'hasMany'   => array(
                'ChildResource'   => array(
                    'className'  => 'Sysadmin.Resource',
                    'foreignKey' => 'parent_id',
                    'dependent'  => false,
                ),
                'ResourcesDetail' => array(
                    'className'  => 'Sysadmin.ResourcesDetail',
                    'foreignKey' => 'resource_id',
                    'dependent'  => false,
                )
            )
        );
        $Res->bindModel($params, false);
        return $Res;
    }

    protected function _loadResourcesDetail()
    {

        $Res = ClassRegistry::init(array('class' => 'Sysadmin.ResourcesDetail', 'alias' => 'SysAclResDetail'),
                        true);

        if ($Res) {
            $Res->virtualFields = array(
                'val' => 'REPLACE(`value`, \' \',\'\')'
            );
            $params             = array(
                'belongsTo' => array(
                    'AclRes' => array(
                        'className'  => 'Sysadmin.Resources',
                        'foreignKey' => 'resource_id',
                    ),
                )
            );
            $Res->bindModel($params, false);
        }

        return $Res;
    }

    protected function _loadResourcesProceedings()
    {
        $Res = ClassRegistry::init(array('class' => 'Sysadmin.ResourcesProceeding',
                    'alias' => 'SysAclResProc'), true);
        #$Res              = ClassRegistry::init('SysAclResProc');
        /* $Res->useDbConfig = $this->dbConfig;
          $Res->useTable    = "resources_proceedings"; */
        return $Res;
    }

    protected function _loadRolesProceedings()
    {
        $Res              = ClassRegistry::init('Sysadmin.SysAclRolesProc');
        $Res->useDbConfig = $this->dbConfig;
        $Res->useTable    = "roles_proceedings";
        return $Res;
    }

    protected function _isAcc($proceeding = 'browse', $resRoute = false,
            $actionSubstitute = 'index')
    {
        $acc                = false;
        $reqroute           = $this->routeToCakeUrl($resRoute);
        $reqroute['action'] = $actionSubstitute;
        krsort($reqroute);
        $reqroutejson       = json_encode($reqroute);
        $json_url           = $reqroutejson;
        $ResDetail          = $this->_loadResourcesDetail();
       
        $options              = array(
            'conditions' => array(
                'SysAclResDetail.active_status' => 1,
                'field' => 'json_url',
                'val'   => $json_url,
            )
        );
        $res_id               = false;
        // debug($this->dbConfig);die();
        //$ResDetail->useDbConfig = $this->dbConfig;
        // debug($ResDetail);die();
        $rs_resources_details = $ResDetail->find('first', $options);

        #debug($rs_resources_details);die();
        if ($rs_resources_details) {
            #resource detail found
            if (isset($rs_resources_details['AclRes'])) {
                if (isset($rs_resources_details['AclRes']['id'])) {
                    $res_id = $rs_resources_details['AclRes']['id'];
                }
                if ($res_id) {
                    $ResProc = ClassRegistry::init('Sysadmin.SysAclResProc'); //$this->_loadResourcesProceedings();

                    $options     = array(
                        'joins'      => array(
                            array(
                                'table'      => 'proceedings',
                                'alias'      => 'AclProc',
                                'type'       => 'LEFT',
                                'conditions' => array(
                                    'AclProc.id = SysAclResProc.proceeding_id',
                                )
                            )
                        ),
                        'conditions' => array(
                            'AclProc.name'              => $proceeding,
                            'SysAclResProc.resource_id' => $res_id,
                        )
                    );
                    $res_proc_id = false;
                    $rs_res_proc = $ResProc->find('first', $options);

                    if ($rs_res_proc) {
                        if (isset($rs_res_proc['SysAclResProc'])) {
                            if (isset($rs_res_proc['SysAclResProc']['id'])) {
                                $res_proc_id = $rs_res_proc['SysAclResProc']['id'];
                            }
                        }
                        if ($res_proc_id) {
                            //find user
                            $id      = $this->Session->read('AppUsrLog.uid');
                            $usr     = $this->_getUsr($id);
                            $role_id = false;
                            
                            if ($usr) {
                                if (isset($usr['SysAclUser'])) {
                                    if (isset($usr['SysAclUser']['role_id'])) {
                                        $role_id = $usr['SysAclUser']['role_id'];
                                    }
                                    if ($role_id) {

                                        $RolesProc  = $this->_loadRolesProceedings();

                                        

                                        $conditions = array(
                                            'role_id'                 => $role_id,
                                            'resources_proceeding_id' => $res_proc_id,
                                            'active_status'           => 1
                                        );

                                        
                                        $acc        = (bool) $RolesProc->field('active_status',
                                                        $conditions);

                                        if(!$acc){

                                            $acc  = $RolesProc->find('first',
                                                        $conditions);
                                            if(is_array($acc['SysAclRolesProc'])){
                                                return (bool)$acc['SysAclRolesProc']['active_status'];
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return $acc;
    }

    public function _isSignedIn()
    {
        $signedIn = false;
        $uid      = $this->Session->read('AppUsrLog.uid');
        if ($uid) {
            $signedIn = true;
        }
        return $signedIn;
    }

    public function beforeRedirect(\Controller $controller, $url,
            $status = null, $exit = true)
    {
        parent::beforeRedirect($controller, $url, $status, $exit);

        $controller->SysLog->writeSysLog();
    }

    /**
     * get Access Control List for View
     * 
     * @param string $controller controller name
     * @param string $action action name
     * @param string $uid user id
     * @return array list of BREADX ACL in boolean
     * @depends SessionComponent::read
     * @author ino <inoshadi@gmail.com>
     * @since version 1.0.0
     */
    public function getAclForView($controller, $action = 'index', $uid = null,
            $plugin = 'sysadmin')
    {

        if (!$uid) {
            $uid = $this->Session->read('AppUsrLog.uid');
        }
        $acl         = array(
            'browse'  => false,
            'read'    => false,
            'edit'    => false,
            'add'     => false,
            'delete'  => false,
            'execute' => false
        );
        $proceedings = array(
            'browse',
            'read',
            'edit',
            'add',
            'delete',
            'execute'
        );

        foreach ($acl as $i => &$v) {

            $v = $this->isAcc($i, false, $action, false, null);
        }
        return $acl;
    }

    /**
     * 
     * @return object Resource instance
     */
    public function getAppMenu()
    {
        $id      = $this->Session->read('AppUsrLog.uid');

        $usr     = $this->_getUsr($id);
        $res_ids = array();
        if ($usr) {
            if (isset($usr['SysAclUser']['role_id'])) {
                // fetch role res proc

                $res_ids = $this->getResIdsByRoleId($usr['SysAclUser']['role_id'],
                        true);
            }
        }
        $rs = array();
        // print_r($usr);
        // print_r($res_ids);
        if ($res_ids) {
            $Resource = ClassRegistry::init('Sysadmin.SysAclRes'); //$this->_loadResources();

            $options = array(
                'conditions' => array(
                    'ResourcesGroup.name LIKE ' => 'sysadmin_app_menu',
                    'SysAclRes.active_status'   => 1,
                    'SysAclRes.id'              => $res_ids,
                ),
                'order'      => array(
                    'SysAclRes.menu_order' => 'asc',
                    'SysAclRes.lft'        => 'asc'
                ),
                    /*  'joins'      => array(
                      array(
                      'table'      => 'resources_groups',
                      'alias'      => 'ResourcesGroup',
                      'type'       => 'LEFT',
                      'conditions' => array(
                      'ResourcesGroup.id = SysAclRes.resources_group_id'
                      )
                      )
                      ) */
            );
            $rs      = $Resource->find('threaded', $options);
            $Resource->clear();
        }

        // print_r($rs);
        return $rs;
    }

    public function getProfileMenu()
    {
        $Resource = ClassRegistry::init('Sysadmin.SysAclRes'); //$this->_loadResources();
        $options  = array(
            'conditions' => array(
                'SysAclRes.active_status' => 1,
                'ResourcesGroup.name'     => 'sysadmin_profile_menu',
            ),
            'order'      => array(
                'SysAclRes.menu_order' => 'asc'
            ),
                /* 'joins'      => array(
                  array(
                  'table'      => 'resources_groups',
                  'alias'      => 'ResourcesGroup',
                  'type'       => 'LEFT',
                  'conditions' => array(
                  'ResourcesGroup.id = SysAclRes.resources_group_id'
                  )
                  )
                  ) */
        );
        $rs       = $Resource->find('all', $options);
        $Resource->clear();
        $res      = array();
        if ($rs) {
            foreach ($rs as $i => $v) {
                $res[$i]['Resource'] = $v['SysAclRes'];
                if (isset($v['ResourcesType'])) {
                    $res[$i]['resources_type'] = $v['ResourcesType']['name'];
                }
                if (isset($v['ResourcesDetail'])) {
                    if ($v['ResourcesDetail']) {
                        foreach ($v['ResourcesDetail'] as $id => $vd) {
                            $res[$i][$vd['field']] = $vd['value'];
                        }
                    }
                }
            }
        }
        return $res;
    }

    /**
     * Get the corresponding resource_ids identified by the role_id
     * @param string $role_id
     * @param bool $res_id_only default false
     * @return array of resource_ids if $res_id_only is set to true
     */
    public function getResIdsByRoleId($role_id)
    {
        $result                  = array();
        $RolesProc               = ClassRegistry::init(array('class' => 'Sysadmin.RolesProceeding',
                    'alias' => 'SysAclRolesProc'));
        /* $RolesProc               = ClassRegistry::init('SysAclRolesProc');
          $RolesProc->useDbConfig  = $this->dbConfig;
          $RolesProc->useTable     = 'roles_proceedings'; */
        $RolesProc->displayField = 'resources_proceeding_id';
        $options                 = array(
            'conditions' => array(
                'SysAclRolesProc.role_id'       => $role_id,
                'SysAclRolesProc.active_status' => 1,
            )
        );
        $res_proc_ids            = $RolesProc->find('list', $options);
        $RolesProc->clear();
        if ($res_proc_ids) {
            $res_proc_ids          = array_values($res_proc_ids);
            $ResProc               = ClassRegistry::init(array('class' => 'Sysadmin.ResourcesProceeding',
                        'alias' => 'SysAclResProc'));
            /* $ResProc               = ClassRegistry::init('SysAclResProc');
              $ResProc->useDbConfig  = $this->dbConfig;
              $ResProc->useTable     = 'resources_proceedings'; */
            $ResProc->displayField = "resource_id";
            $res_ids               = $ResProc->find('list',
                    array('conditions' => array('SysAclResProc.id' => $res_proc_ids)));
            if ($res_ids) {
                $result = array_values($res_ids);
            }
        }

        return $result;
    }

    /**
     * 
     * @param type $id
     */
    public function getUserInfo($id)
    {
        return $this->_getUsr($id);
    }

    public function initialize(Controller $controller)
    {

        $this->controller   = $controller;
        $request            = $this->controller->request;
        $this->requestRoute = sprintf("%s.%s.%s", $request->plugin,
                $request->controller, $request->action);
        if (!$this->Session->check('AppUsrLog')) {
            $this->Session->write('AppUsrLog.nextReferer', null);
        }

        $this->Session->write('AppUsrLog.referer',
                $this->Session->read('AppUsrLog.nextReferer'));
        if ($this->requestRoute != $this->signInRoute) {
            $write = true;
            if (Configure::check('Sysconfig.signInActionAlias')) {
                if (Configure::read('Sysconfig.signInActionAlias') == $request->action) {
                    $write = false;
                }
            }
            if ($request->action == 'login') {
                $write = false;
            }
            if ($write) {
                $this->Session->write('AppUsrLog.nextReferer',
                        $this->controller->request->params);
            }
        }


        if (!$this->Session->check('AppUsrToken')) {
            $appToken = CakeString::uuid();
            $this->Session->write('AppUsrToken', $appToken);
            $this->Session->write('AppUsrLog.token', $appToken);
        }

//debug($this->controller->request->plugin);
    }

    /*
     * function SysAcl::isAcc
     * 
     * params 0 : (string) proceeding list (breadx)| default browse
     * params 2 : (bool) force redirect to login page| default false
     * 
     * 
     */

    public function isAcc($proceeding = 'browse', $resRoute = false,
            $actionSubstitute = 'index', $forceRedirect = true)
    {
        $arg_num       = func_num_args();
        $alertMsg      = $arg_num >= 5 ? func_get_arg(4) : "You don't have permission to access the requested page. Please contact your System Administrator.";
        $accessOptions = $arg_num >= 6 ? func_get_arg(5) : array();
        $resRoute      = !$resRoute ? $this->requestRoute : $resRoute;
        $acc           = false;
        if ($this->_isSignedIn()) {

            if (in_array($resRoute, $this->usersAllowed)) {
                $acc = true;
            }
            else {
                $acc = $this->_isAcc($proceeding, $resRoute, $actionSubstitute);
                
                
            }
        }
        if (!$acc) {
            if ($resRoute != $this->signInRoute) {
                if ($alertMsg) {

                    $this->Flash->error($alertMsg);
                }
                if ($forceRedirect) {

                    $redir = $this->signInRedirRoute ? $this->signInRedirRoute : $this->routeToCakeUrl($this->signInRoute);

                    $this->controller->redirect($redir);
                }
            }
        }

        return $acc;
    }

    /**
     * Format from route (plugin.controller.action) to cake HtmlHelper url param
     * @param string $route plugin.controller.action
     * @return array
     */
    public function routeToCakeUrl($route)
    {
        $url = array();
        list($url['plugin'], $url['controller'], $url['action']) = explode('.',
                $route);
        krsort($url);
        return $url;
    }

    /**
     * Format from route (plugin.controller.action) to json
     * @param string $route plugin.controller.action
     * @depends SysAclCoomponent::routeToCakeUrl
     * @return json
     */
    public function routeToJsonUrl($route)
    {
        $url = $this->routeToCakeUrl($route);
        krsort($url);
        return json_encode($url);
    }

    /**
     * @return void
     */
    public function signin()
    {
        $this->controller->set('page','login_page');
        if ($this->_isSignedIn()) {

            $route = $this->routeToCakeUrl($this->profileRoute);
            return $this->controller->redirect($route);
        }
        if ($this->controller->request->is('post')) {

            $request           = $this->controller->request;
            if(defined('ENABLE_G_CAPTCHA') && $_SERVER['HTTP_HOST'] != 'localhost'){
                if(ENABLE_G_CAPTCHA){
                    $recaptchaResponse = $request->data('g-recaptcha-response');
            
                    $recaptchaCallenge = $this->_getRecaptcha($recaptchaResponse);
                    if(!$recaptchaCallenge->success && Configure::read('useRecaptcha')){
                       $this->Flash->error(__d('sysadmin',
                                                    'Please check you are not a robot'),
                                            array('plugin' => 'Sysadmin'));
                       return;
                    }
                }
            }
            
            $postedUname = $request->data(md5($this->Session->read('AppUsrToken') . 'username'));
            $postedPsswd = $request->data(md5($this->Session->read('AppUsrToken') . 'password'));
            if ($postedUname && $postedPsswd) {
                $User    = ClassRegistry::init('Sysadmin.SysAclUser');
                /* $User->useDbConfig = 'sys_';
                  $User->useTable    = 'users'; */
                $options = array(
                    'conditions' => array(
                        'SysAclUser.username'      => $postedUname,
                        'SysAclUser.active_status' => 1,
                        'UNIX_TIMESTAMP(SysAclUser.active_dt) <= UNIX_TIMESTAMP()',
                    ), 'fields'     => array('username', 'id')
                );
                $User->unbindModel(array('hasMany' => array('ActivitiesLog')));
                $usr     = $User->find('first', $options);
                $User->clear();
                if ($usr) {
                    $pwd = $User->read('password', $usr['SysAclUser']['id']);
                    $User->clear();
                    if ($pwd) {
//password check
                        $passworHasher = new BlowfishPasswordHasher();
                        if ($passworHasher->check($postedPsswd,
                                        $pwd['SysAclUser']['password'])) {
                            $this->Session->write('AppUsrLog.uid',
                                    $usr['SysAclUser']['id']);
                            $logUsr = $this->_getUsr($usr['SysAclUser']['id']);

                            $this->Session->write('AppUsrLog.usr', $logUsr);
                            $this->Flash->success(__d('sysadmin',
                                            'Welcome %s, you have logged in!',
                                            $postedUname),
                                    array('plugin' => 'Sysadmin'));
                            $referer   = $this->Session->read('AppUsrLog.referer');
                            $homeRoute = $this->routeToCakeUrl($this->homeRoute);
                            if (!$referer) {
                                $referer = $homeRoute;
                            }
                            $redirecturl = $referer;
                            if ($referer['plugin'] == $homeRoute['plugin'] && $referer['controller']
                                    == $homeRoute['controller']) {
                                $redirecturl = $this->routeToCakeUrl($this->profileRoute);
                            }
                            return $this->controller->redirect(array(
                                        'plugin'     => $redirecturl['plugin'],
                                        'controller' => $redirecturl['controller'],
                                        'action'     => $redirecturl['action'],
                            ));
                        }
                        else {
                            $this->Flash->error(__d('sysadmin',
                                            'Wrong username and password'),
                                    array('plugin' => 'Sysadmin'));
                        }
                    }
                    else {
                        $this->Flash->error(__d('sysadmin',
                                        'Wrong username and password'),
                                array('plugin' => 'Sysadmin'));
                    }
                }
                else {
                    $this->Flash->error(__d('sysadmin',
                                    'Wrong username and password'),
                            array('plugin' => 'Sysadmin'));
                }
            }
        }
    }

    public function signout()
    {
        $this->Session->delete('AppUsrLog');
        //$this->Flash->success(__d('sysadmin', 'Good bye!'));
        $redir = $this->signInRedirRoute ? $this->signInRedirRoute : $this->routeToCakeUrl($this->signInRoute);
        $this->controller->redirect($redir);
    }

}
