<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * CakePHP SysAdmReourcesComponent
 * @author ino
 */
class SysAdmResourcesComponent extends Component
{

    public $belongsTo  = array(
        'belongsTo' => array(
            'ResourcesType'  => array(
                'className'  => 'ResourcesType',
                'foreignKey' => 'resources_type_id',
                'conditions' => '',
                'fields'     => '',
                'order'      => ''
            ),
            'ResourcesGroup' => array(
                'className'  => 'ResourcesGroup',
                'foreignKey' => 'resources_group_id',
                'conditions' => '',
                'fields'     => '',
                'order'      => ''
            ),
            'ParentResource' => array(
                'className'  => 'Resource',
                'foreignKey' => 'parent_id',
                'conditions' => '',
                'fields'     => '',
                'order'      => ''
            )
        )
    );
    public $components = array();

    

    public function getParents($exclude_ids = array(), $conditions = array())
    {
        $Res              = ClassRegistry::init('Sysadmin.SysAclRes');
        $Res -> virtualFields = array(
            'full_text' => "CONCAT(SysAclRes.text, ' (', SysAclRes.name,')')",
        );
        $Res -> displayField = 'full_text';
        $conditions['SysAclRes.active_status'] = 1;
        $conditions[] = "SysAclRes.id NOT IN ('".implode("','",$exclude_ids)."')";
        
        $Res -> recursive =0;
        $Res->Behaviors->load('Tree');
        $parents = $Res->generateTreeList($conditions);
        return $parents;//$Res->find ('list', $options);
    }
    
    public function getResourcesGroupIdByName($names = array()){
        $ResGr = ClassRegistry::init('Sysadmin.SysAclResGr');
        $ResGr-> displayField = 'id';
        return $ResGr->find('list', array('conditions'=> array('active_status'=> 1, 'name' => $names)));
    }
    public function getResourcesTypeIdByName($names = array()){
        $ResType = ClassRegistry::init('Sysadmin.SysAclResType');
        $ResType-> displayField = 'id';
        return $ResType->find('list', array('conditions'=> array('active_status'=> 1, 'name' => $names)));
        
    }

}
