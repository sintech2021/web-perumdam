<?php
App::uses('SysadminAppController', 'Sysadmin.Controller');
/**
 * ResourcesActions Controller
 *
 * @property ResourcesAction $ResourcesAction
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class ResourcesActionsController extends SysadminAppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ResourcesAction->recursive = 0;
		$this->set('resourcesActions', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ResourcesAction->exists($id)) {
			throw new NotFoundException(__('Invalid resources action'));
		}
		$options = array('conditions' => array('ResourcesAction.' . $this->ResourcesAction->primaryKey => $id));
		$this->set('resourcesAction', $this->ResourcesAction->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ResourcesAction->create();
			if ($this->ResourcesAction->save($this->request->data)) {
				$this->Flash->success(__('The resources action has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The resources action could not be saved. Please, try again.'));
			}
		}
		$resources = $this->ResourcesAction->Resource->find('list');
		$actions = $this->ResourcesAction->Action->find('list');
		$this->set(compact('resources', 'actions'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ResourcesAction->exists($id)) {
			throw new NotFoundException(__('Invalid resources action'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ResourcesAction->save($this->request->data)) {
				$this->Flash->success(__('The resources action has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The resources action could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ResourcesAction.' . $this->ResourcesAction->primaryKey => $id));
			$this->request->data = $this->ResourcesAction->find('first', $options);
		}
		$resources = $this->ResourcesAction->Resource->find('list');
		$actions = $this->ResourcesAction->Action->find('list');
		$this->set(compact('resources', 'actions'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ResourcesAction->id = $id;
		if (!$this->ResourcesAction->exists()) {
			throw new NotFoundException(__('Invalid resources action'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ResourcesAction->delete()) {
			$this->Flash->success(__('The resources action has been deleted.'));
		} else {
			$this->Flash->error(__('The resources action could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
