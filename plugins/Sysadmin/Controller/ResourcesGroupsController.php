<?php
App::uses('SysadminAppController', 'Sysadmin.Controller');
/**
 * ResourcesGroups Controller
 *
 * @property ResourcesGroup $ResourcesGroup
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class ResourcesGroupsController extends SysadminAppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ResourcesGroup->recursive = 0;
		$this->set('resourcesGroups', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ResourcesGroup->exists($id)) {
			throw new NotFoundException(__('Invalid resources group'));
		}
		$options = array('conditions' => array('ResourcesGroup.' . $this->ResourcesGroup->primaryKey => $id));
		$this->set('resourcesGroup', $this->ResourcesGroup->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ResourcesGroup->create();
			if ($this->ResourcesGroup->save($this->request->data)) {
				$this->Flash->success(__('The resources group has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The resources group could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ResourcesGroup->exists($id)) {
			throw new NotFoundException(__('Invalid resources group'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ResourcesGroup->save($this->request->data)) {
				$this->Flash->success(__('The resources group has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The resources group could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ResourcesGroup.' . $this->ResourcesGroup->primaryKey => $id));
			$this->request->data = $this->ResourcesGroup->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ResourcesGroup->id = $id;
		if (!$this->ResourcesGroup->exists()) {
			throw new NotFoundException(__('Invalid resources group'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ResourcesGroup->delete()) {
			$this->Flash->success(__('The resources group has been deleted.'));
		} else {
			$this->Flash->error(__('The resources group could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
