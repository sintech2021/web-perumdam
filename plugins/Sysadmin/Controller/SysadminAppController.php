<?php

/**
 * 
 */
App::uses('AppController', 'Controller');

class SysadminAppController extends AppController
{

    public $layout       = 'layout_bootstrap';
    public $components   = array('Sysadmin.SysAcl', 'Sysadmin.SysLog', 'Sysadmin.SysAdmUserRoles',
        'Session', 'Paginator', 'Flash');
    public $helpers      = array('Sysadmin.SysHtml');
    public $sysConfig    = array();
    public $SysAcl;
    public $SysAclView   = array();
    public $allSearchKey = "--all--";
    public $isGs         = false;

    /**
     * Set up the global search form
     * @visibility protected
     * @param array
     * @return void
     */
    protected function _getGlobalSearch($params = array())
    {
        $global_search = array(
            'plugin'     => $this->request->params['plugin'],
            'controller' => $this->request->params['controller'],
            'action'     => $this->request->params['action'],
        );

        $result      = array('global_search_field' => '', 'global_search_keyword' => '');
        $stringfield = 'GlobalSearch.%s.%s.%s.field';
        if ($this->Session->check(sprintf($stringfield,
                                $global_search['plugin'],
                                $global_search['controller'],
                                $global_search['action']))) {
            $result['global_search_field'] = $this->Session->read(sprintf($stringfield,
                            $global_search['plugin'],
                            $global_search['controller'],
                            $global_search['action']));
        }

        $string = 'GlobalSearch.%s.%s.%s.keyword';
        if ($this->Session->check(sprintf($string, $global_search['plugin'],
                                $global_search['controller'],
                                $global_search['action']))) {
            $result['global_search_keyword'] = $this->Session->read(sprintf($string,
                            $global_search['plugin'],
                            $global_search['controller'],
                            $global_search['action']));
        }

        return $result;
    }

    /**
     * Set up the global search form
     * @visibility protected
     * @param array
     * @return void
     */
    protected function _setGlobalSearch($params = array())
    {
        $this->isGs   = true;
        $allSearchKey = array($this->allSearchKey => __d('sysadmin', 'All'));

        $global_search = array(
            'plugin'     => $this->request->params['plugin'],
            'controller' => $this->request->params['controller'],
            'action'     => $this->request->params['action'],
        );
        $field         = null;
        $stringfield   = 'GlobalSearch.%s.%s.%s.field';
        if ($this->Session->check(sprintf($stringfield,
                                $global_search['plugin'],
                                $global_search['controller'],
                                $global_search['action']))) {
            $field = $this->Session->read(sprintf($stringfield,
                            $global_search['plugin'],
                            $global_search['controller'],
                            $global_search['action']));
        }

        $keyword = null;
        $string  = 'GlobalSearch.%s.%s.%s.keyword';
        if ($this->Session->check(sprintf($string, $global_search['plugin'],
                                $global_search['controller'],
                                $global_search['action']))) {
            $keyword = $this->Session->read(sprintf($string,
                            $global_search['plugin'],
                            $global_search['controller'],
                            $global_search['action']));
        }

        if ($this->request->is('post')) {

            $posted = $this->request->data;
            if (isset($posted['global_search'])) {
                $posted = $posted['global_search'];
            }

            if (isset($posted['do'])) {

                $this->Session->write(sprintf($string, $global_search['plugin'],
                                $global_search['controller'],
                                $global_search['action']), $posted['keyword']);
                $this->Session->write(sprintf($stringfield,
                                $global_search['plugin'],
                                $global_search['controller'],
                                $global_search['action']), $posted['field']);
            }
            if (isset($posted['clear'])) {

                $this->Session->write(sprintf($string, $global_search['plugin'],
                                $global_search['controller'],
                                $global_search['action']), null);
                $this->Session->write(sprintf($stringfield,
                                $global_search['plugin'],
                                $global_search['controller'],
                                $global_search['action']), null);
            }

            $keyword = $this->Session->read(sprintf($string,
                            $global_search['plugin'],
                            $global_search['controller'],
                            $global_search['action']));
            $field   = $this->Session->read(sprintf($stringfield,
                            $global_search['plugin'],
                            $global_search['controller'],
                            $global_search['action']));
        }

        $global_search['allSearchKey'] = $allSearchKey;
        $global_search['params']       = $params;
        $global_search['field']        = $field;
        $global_search['keyword']      = $keyword;
        $this->set(compact('global_search'));
    }

    /**
     * Set up the top menu for each rendered page
     * @visibility protected
     * @param void
     * @return void
     */
    protected function _setTopMenu($set = TRUE)
    {
        $my_profile       = array();
        $change_password  = array();
        $user_preferences = array();
        $my_activities    = array();
        $my_profile       = array(
            'title'   => '<span class="fa fa-user fa-fw"></span> ' . __d('sysadmin',
                    'My Profile'),
            'url'     => $this->SysAcl->routeToCakeUrl('sysadmin.sysadmin_app.my_profile'),
            'options' => array('escape' => false),
            'active'  => false,
        );
        if ($this->request->action == 'my_profile') {
            $my_profile['active'] = true;
        }
        $change_password = array(
            'title'   => '<span class="fa fa-key fa-fw"></span> ' . __d('sysadmin',
                    'Change Password'),
            'url'     => $this->SysAcl->routeToCakeUrl('sysadmin.sysadmin_app.change_password'),
            'options' => array('escape' => false),
            'active'  => false,
        );
        if ($this->request->action == 'change_password') {
            $change_password['active'] = true;
        }
//        $user_preferences = array(
//            'title'   => '<span class="fa fa-cogs fa-fw"></span> ' . __d('sysadmin',
//                    'Preferences'),
//            'url'     => $this->SysAcl->routeToCakeUrl('sysadmin.sysadmin_app.user_preferences'),
//            'options' => array('escape' => false),
//            'active'  => false,
//        );
//        if ($this->request->action == 'user_preferences') {
//            $user_preferences['active'] = true;
//        }
        $my_activities = array(
            'title'   => '<span class="fa fa-exchange-alt fa-fw"></span> ' . __d('sysadmin',
                    'My Activities'),
            'url'     => $this->SysAcl->routeToCakeUrl('sysadmin.sysadmin_app.my_activities'),
            'options' => array('escape' => false),
            'active'  => false,
        );
        if ($this->request->action == 'my_activities') {
            $my_activities['active'] = true;
        }

        $top_menu = array(
            $my_profile,
            $change_password,
//            $user_preferences,
            $my_activities,
        );
        if ($set) {
            $this->set(compact('top_menu'));
        }
        else {
            return $top_menu;
        }
    }

    public function afterFilter()
    {
        parent::afterFilter();
        $this->SysLog->writeSysLog();
    }

    public function beforeFilter()
    {
        parent::beforeFilter();

        $this->setAclForView($this->request->controller);
        $appMenu     = $this->SysAcl->getAppMenu();
        $profileMenu = $this->SysAcl->getProfileMenu();
        $this->set(compact('appMenu', 'profileMenu'));
        $this->SysAcl->isAcc();
        if (method_exists($this, '_setTopMenu')) {
            $this->_setTopMenu();
            $this->set('noPageTitle', true);
        }
    }

    public function change_password()
    {
        $this->loadModel('Sysadmin.SysAclUser');
        $reqdata = array(
            'SysAclUser' => array(
                'password'         => '',
                'new_password'     => '',
                'confirm_password' => ''
            )
        );
        if ($this->request->is('post')) {
            $err     = false;
            $reqdata = $this->request->data;
            if (!isset($reqdata['SysAclUser'])) {
                $err = true;
                $this->Flash->error(__d('sysadmin', 'Invalid data provided!!'));
            }
            else {
                $posted = $reqdata['SysAclUser'];
            }

            if (!isset($posted['password']) || !isset($posted['new_password']) || !isset($posted['confirm_password'])) {
                $err = true;
                $this->Flash->error(__d('sysadmin', 'Invalid data provided!'));
            }

            if (!$err) {
                if (!$posted['password'] || !$posted['new_password'] || !$posted['confirm_password']) {
                    $err = true;
                    $this->Flash->error(__d('sysadmin',
                                    'All fields are required!'));
                }
            }
            if (!$err) {
                if ($posted['new_password'] != $posted['confirm_password']) {
                    $err = true;
                    $this->Flash->error(__d('sysadmin',
                                    'New password did not match!'));
                }
            }
            if (!$err) {

                $checkPassword = $this->SysAclUser->checkPassword($this->Session->read('AppUsrLog.uid'),
                        $posted['password']);

                $this->SysAclUser->clear();
                if (!$checkPassword) {
                    $err = true;
                    $this->Flash->error(__d('sysadmin',
                                    'Invalid password provided!'));
                }
            }

            if (!$err) {
                $this->SysAclUser->id = $this->Session->read('AppUsrLog.uid');
                if ($this->SysAclUser->save($reqdata)) {
                    $this->Flash->success(__d('sysadmin',
                                    'Your password has been changed!'));
                }
                else {
                    $this->Flash->error(__d('sysadmin',
                                    'Password update failed!'));
                }
            }
        }
        $this->set(compact('reqdata'));
    }

    public function dashboard()
    {
        $this->view = 'index';
    }

    public function index()
    {
        $page_title = "System Administration Console";

        $this->set(compact('page_title'));
    }

    public function login()
    {

        $this->layout = 'layout_full';
        $enqueued_css = array('Sysadmin./bootstrap-3.3.5-dist/css/signin');
        $this->set(compact('enqueued_css'));
        $this->SysAcl->signin();
    }

    public function my_activities()
    {
        $this->loadModel('Sysadmin.ActivitiesLog');
        $this->ActivitiesLog->unbindModel(array('belongsTo' => array('User')));
        $conditions                         = array('ActivitiesLog.user_id' => $this->Session->read('AppUsrLog.uid'));
        $this->Paginator->settings['order'] = array('ActivitiesLog.created' => 'desc');
        $activities                         = $this->Paginator->paginate('ActivitiesLog',
                $conditions);
        $this->set(compact('activities'));
    }

    public function my_profile()
    {
        $user_info = $this->SysAcl->getUserInfo($this->Session->read('AppUsrLog.uid'));
        $this->set(compact('user_info'));
    }

    public function signout()
    {
        $this->SysAcl->signout();
        $this->autoRender = false;
    }

    public function user_preferences()
    {
        
    }

    /**
     * 
     * @param string $controller
     * @param string $action
     * @param string $uid
     */
    public function setAclForView($controller, $action = 'index', $uid = null,
            $plugin = 'sysadmin')
    {
        $this->SysAclView = $this->SysAcl->getAclForView($controller, $action,
                $uid, $plugin);
        $this->set('SysAcl', $this->SysAclView);
    }

}
