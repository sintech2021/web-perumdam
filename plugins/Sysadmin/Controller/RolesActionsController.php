<?php
App::uses('SysadminAppController', 'Sysadmin.Controller');
/**
 * RolesActions Controller
 *
 * @property RolesAction $RolesAction
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class RolesActionsController extends SysadminAppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->RolesAction->recursive = 0;
		$this->set('rolesActions', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->RolesAction->exists($id)) {
			throw new NotFoundException(__('Invalid roles action'));
		}
		$options = array('conditions' => array('RolesAction.' . $this->RolesAction->primaryKey => $id));
		$this->set('rolesAction', $this->RolesAction->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->RolesAction->create();
			if ($this->RolesAction->save($this->request->data)) {
				$this->Flash->success(__('The roles action has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The roles action could not be saved. Please, try again.'));
			}
		}
		$usersRoles = $this->RolesAction->UsersRole->find('list');
		$resourcesActions = $this->RolesAction->ResourcesAction->find('list');
		$this->set(compact('usersRoles', 'resourcesActions'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->RolesAction->exists($id)) {
			throw new NotFoundException(__('Invalid roles action'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->RolesAction->save($this->request->data)) {
				$this->Flash->success(__('The roles action has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The roles action could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('RolesAction.' . $this->RolesAction->primaryKey => $id));
			$this->request->data = $this->RolesAction->find('first', $options);
		}
		$usersRoles = $this->RolesAction->UsersRole->find('list');
		$resourcesActions = $this->RolesAction->ResourcesAction->find('list');
		$this->set(compact('usersRoles', 'resourcesActions'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->RolesAction->id = $id;
		if (!$this->RolesAction->exists()) {
			throw new NotFoundException(__('Invalid roles action'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->RolesAction->delete()) {
			$this->Flash->success(__('The roles action has been deleted.'));
		} else {
			$this->Flash->error(__('The roles action could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
