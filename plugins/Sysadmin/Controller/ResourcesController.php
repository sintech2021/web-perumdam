<?php
App::uses('SysadminAppController', 'Sysadmin.Controller');
/**
 * Resources Controller
 *
 * @property Resource $Resource
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class ResourcesController extends SysadminAppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Sysadmin.SysAcl','Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Resource->recursive = 0;
		$this->set('resources', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Resource->exists($id)) {
			throw new NotFoundException(__('Invalid resource'));
		}
		$options = array('conditions' => array('Resource.' . $this->Resource->primaryKey => $id));
		$this->set('resource', $this->Resource->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Resource->create();
			if ($this->Resource->save($this->request->data)) {
				$this->Flash->success(__('The resource has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The resource could not be saved. Please, try again.'));
			}
		}
		$resourcesGroups = $this->Resource->ResourcesGroup->find('list');
		$resourcesTypes = $this->Resource->ResourcesType->find('list');
		$parentResources = $this->Resource->ParentResource->find('list');
		$actions = $this->Resource->Action->find('list');
		$this->set(compact('resourcesGroups', 'resourcesTypes', 'parentResources', 'actions'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Resource->exists($id)) {
			throw new NotFoundException(__('Invalid resource'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Resource->save($this->request->data)) {
				$this->Flash->success(__('The resource has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The resource could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Resource.' . $this->Resource->primaryKey => $id));
			$this->request->data = $this->Resource->find('first', $options);
		}
		$resourcesGroups = $this->Resource->ResourcesGroup->find('list');
		$resourcesTypes = $this->Resource->ResourcesType->find('list');
		$parentResources = $this->Resource->ParentResource->find('list');
		$actions = $this->Resource->Action->find('list');
		$this->set(compact('resourcesGroups', 'resourcesTypes', 'parentResources', 'actions'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Resource->id = $id;
		if (!$this->Resource->exists()) {
			throw new NotFoundException(__('Invalid resource'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Resource->delete()) {
			$this->Flash->success(__('The resource has been deleted.'));
		} else {
			$this->Flash->error(__('The resource could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
