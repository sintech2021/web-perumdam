<h1 class="page-header"><?php 
echo h(__d('sysadmin', $resource['PrivilegesResource']['text'])),
        ' (', 
        h(__d('sysadmin', $resource['PrivilegesResource']['name'])),
                ')';
?></h1>
<div class="">
    <table class="table table-bordered table-condensed table-striped table-responsive">
        <thead>
            <tr>
                <?php
                if (isset($SysAcl)):
                    if ($SysAcl['read'] || $SysAcl['edit'] || $SysAcl['delete']):
                        ?>
                        <th class="actions"><?php echo __d('sysadmin', 'Actions'); ?></th>
                        <?php
                    endif;
                endif;
                ?>
                        <th>#</th>
                        <th><?php
                    echo $this->Paginator->sort('PrivilegesResourcesDetail.name', __d('sysadmin', 'Name'));
                    ?></th>
                        <th><?php
                    echo $this->Paginator->sort('PrivilegesResourcesDetail.field', __d('sysadmin', 'Key'));
                    ?></th>
                        <th><?php
                    echo $this->Paginator->sort('PrivilegesResourcesDetail.value', __d('sysadmin', 'Value'));
                    ?></th>
                        <th><?php
                    echo $this->Paginator->sort('PrivilegesResourcesDetail.text', __d('sysadmin', 'Text'));
                    ?></th>
                        <th><?php
                    echo $this->Paginator->sort('PrivilegesResourcesDetail.annotation', __d('sysadmin', 'Annotation'));
                    ?></th>
                        <th><?php
                    echo $this->Paginator->sort('PrivilegesResourcesDetail.menu_code', __d('sysadmin', 'Code'));
                    ?></th>
                        <th><?php
                    echo $this->Paginator->sort('PrivilegesResourcesDetail.menu_order', __d('sysadmin', 'Ordering'));
                    ?></th>
            </tr>
        </thead>
        <tbody>
            <?php
            //init
            $page_current = $this->Paginator->current();
            $counter      = $this->Paginator->counter('{:start}');
            
            //loop
            foreach($resources_details as $i=> $v):
                ?>
            <tr>
                
                <?php
                    if (isset($SysAcl)):
                        if ($SysAcl['read'] || $SysAcl['edit'] || $SysAcl['delete']):
                            ?> <td class="actions" nowrap="nowrap">
                            <?php
                            if ($SysAcl['edit']):
                                echo $this->Html->link(
                                        '<span class="fa fa-edit fa-border fa-fw text-warning"></span>',
                                        array(
                                    'action' => 'edit_detail',
                                    $v['PrivilegesResourcesDetail']['resource_id'],
                                    $group_id,
                                    $v['PrivilegesResourcesDetail']['id'],
                                        ), array('title'=> __d('sysadmin', 'edit'),'escape' => false)
                                );
                            endif;
                            if ($SysAcl['delete']):
                                echo $this->Form->postLink(
                                        '<span class="fa fa-trash fa-border fa-fw text-danger"></span>',
                                        array( 
                                            'action' => 'delete_detail', 
                                            $v['PrivilegesResourcesDetail']['resource_id'],
                                            $group_id,
                                            $v['PrivilegesResourcesDetail']['id'],
                                        ),
                                        array(
                                    'confirm' => __d('sysadmin',
                                            'Are you sure you want to delete detail privileges for # %s (%s)?',
                                            h($v['PrivilegesResourcesDetail']['name']),
                                            h($v['PrivilegesResourcesDetail']['field'])
                                            ),
                                            'title'=> __d('sysadmin', 'delete'),
                                    'escape'  => false
                                        )
                                );
                            endif;
                            ?>
                            </td><?php
                        endif;
                    endif;
                    ?>
                <td><?php
                        echo $counter++;
                        ?></td>
                <td><?php echo h($v['PrivilegesResourcesDetail']['name']);?></td>
                <td><?php echo h($v['PrivilegesResourcesDetail']['field']);?></td>
                <td><?php echo h($v['PrivilegesResourcesDetail']['value']);?></td>
                <td><?php echo h($v['PrivilegesResourcesDetail']['text']);?></td>
                <td><?php echo h($v['PrivilegesResourcesDetail']['annotation']);?></td>
                <td><?php echo h($v['PrivilegesResourcesDetail']['menu_code']);?></td>
                <td><?php echo h($v['PrivilegesResourcesDetail']['menu_order']);?></td>
            </tr>
                    <?php
            endforeach;
            ?>
            
        </tbody>
    </table>
    