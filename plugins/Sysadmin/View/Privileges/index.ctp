<div class="">
    <table class="table table-bordered table-condensed table-striped table-responsive">
        <thead>
            <tr>
                <?php
                if (isset($SysAcl)):
                    if ($SysAcl['read'] || $SysAcl['edit'] || $SysAcl['delete']):
                        ?>
                        <th class="actions"><?php echo __d('sysadmin', 'Actions'); ?></th>
                        <?php
                    endif;
                endif;
                ?>
                <th>#</th>
                <th><?php
                    echo $this->Paginator->sort('text',
                            __d('sysadmin', 'Privileges'));
                    ?> (<?php
                    echo $this->Paginator->sort('PrivilegesResource.name', __d('sysadmin', 'ID'));
                    ?>)</th>
                <th><?php
                    echo $this->Paginator->sort('PrivilegesResourcesType.name', __d('sysadmin', 'Type'));
                    ?></th>
                <th><?php
                    echo $this->Paginator->sort('ParentResource.name', __d('sysadmin', 'Parent'));
                    ?></th>
                <?php
                foreach ($proceedings as $ip => $proc):
                    ?><th><?php echo $proc['Proceeding']['menu_code']; ?></th><?php
                    endforeach;
                    ?>
            </tr>
        </thead>
        <tbody>
            <?php
            //init
            $page_current = $this->Paginator->current();
            $counter      = $this->Paginator->counter('{:start}');
            $url_drop     = array('priv_act' => md5('drop' . date('j')),
                'page'     => $page_current);
            $url_set      = array('priv_act' => md5('set' . date('j')),
                'page'     => $page_current);
            $checklists   = array(
                array(
                    'title'   => '<i class="fa fa-remove"></i>&nbsp;',
                    'url'     => $url_drop,
                    'options' => array(
                        'class'  => 'btn btn-sm',
                        'escape' => false)
                ),
                array(
                    'title'   => '<i class="fa fa-check"></i>&nbsp;',
                    'url'     => $url_set,
                    'options' => array(
                        'class'  => 'btn btn-sm btn-success',
                        'escape' => false)
                ),
            );
            foreach ($resources as $i => $resource):
                ?><tr>
                    <?php
                    if (isset($SysAcl)):
                        if ($SysAcl['read'] || $SysAcl['edit'] || $SysAcl['delete']):
                            ?> <td class="actions" nowrap="nowrap">
                            <?php
                            if ($SysAcl['edit']):
                                echo $this->Html->link(
                                        '<span class="fa fa-edit fa-border fa-fw text-warning"></span>',
                                        array(
                                    'action' => 'edit',
                                    $resource['PrivilegesResource']['id'],
                                    $resource['PrivilegesResource']['resources_group_id'],
                                        ), array('title'=> __d('sysadmin', 'edit'),'escape' => false)
                                );
                            endif;
                            if ($SysAcl['delete']):
                                echo $this->Form->postLink(
                                        '<span class="fa fa-trash fa-border fa-fw text-danger"></span>',
                                        array( 
                                            'action' => 'delete', 
                                            $resource['PrivilegesResource']['id'],
                                            $resource['PrivilegesResource']['resources_group_id'],
                                            
                                        ),
                                        array(
                                    'confirm' => __d('sysadmin',
                                            'Are you sure you want to delete the privileges for # %s?',
                                            h($priv_names[$resource['PrivilegesResource']['id']])
                                            ),
                                            'title'=> __d('sysadmin', 'delete'),
                                    'escape'  => false
                                        )
                                );
                            endif;
                            if ($SysAcl['edit']):
                                echo $this->Html->link(
                                        '<span class="fa fa-cog fa-border fa-fw text-info"></span>',
                                        array(
                                    'action' => 'detail',
                                    $resource['PrivilegesResource']['id'],
                                    $resource['PrivilegesResource']['resources_group_id'],
                                        ), array('title'=> __d('sysadmin', 'privileges detail'),'escape' => false)
                                );
                            endif;
                            ?>
                            </td><?php
                        endif;
                    endif;
                    ?>
                    <td><?php
                        echo $counter++;
                        ?></td>
                    <td>
                        <a class="btn" title="<?php echo h($resource['PrivilegesResource']['annotation']); ?>"><span class="fa fa-info-circle"></span></a>&nbsp;<?php 
                        echo h($priv_names[$resource['PrivilegesResource']['id']]); 
                        ?>
                    </td>
                    <td>
                        <?php echo h($resource['PrivilegesResourcesType']['text']); ?>
                    </td>
                    <td>
                        <?php echo h($resource['ParentResource']['text']); ?>
                    </td>
                    <?php
                    foreach ($proceedings as $ip => $proc):
                        ?><td>
                            <div class="btn-group">
                                <?php
                                $selected = isset($privileges[$resource['PrivilegesResource']['id']][$proc['Proceeding']['id']])
                                            ? 1 : 0;

                                $opposed        = $selected ? 0 : 1;
                                $btnclass       = $selected ? 'success' : '';
                                ?>
                                <button type="button" class="btn btn-default dropdown-toggle  btn-sm btn-<?php echo $btnclass; ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <?php
                                    echo $checklists[$selected]['title'];
                                    ?>
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><?php
                                        $options        = $checklists[$opposed]['options'];
                                        $url            = $checklists[$opposed]['url'];
                                        $url['res_id']  = $resource['PrivilegesResource']['id'];
                                        $url['proc_id'] = $proc['Proceeding']['id'];
                                        echo $this->Html->link($checklists[$opposed]['title'],
                                                $url, $options);
                                        ?></li>
                                </ul>
                            </div>
                        </td><?php
                    endforeach;
                    ?>

                </tr>
                <?php
            endforeach;
            ?>
        </tbody>
    </table>
    <?php
    echo $this->element('paginator', array(), array('plugin' => 'sysadmin'));
    ?>
</div>