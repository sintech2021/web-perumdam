<div class="row">
    <div class="col-lg-12 col-sm-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <span class="fa fa-user"></span>&nbsp;
                    <?php echo __d('sysadmin', 'User Info');?>
                </h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-2 col-sm-2">
                        <span class="fa fa-user fa-4x fa-fw"></span>
                    </div>
                    <div class="col-lg-8 col-sm-2">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td class="col-lg-2 cols-sm-2"><?php echo __d('sysadmin', 'Username');?></td>
                                    <td class="col-lg-1 col-sm-1">:</td>
                                    <td><?php echo $user_info['SysAclUser']['username'];?></td>
                                </tr>
                                <tr>
                                    <td class="col-lg-2 cols-sm-2"><?php echo __d('sysadmin', 'Name');?></td>
                                    <td class="col-lg-1 col-sm-1">:</td>
                                    <td><?php echo $user_info['SysAclUser']['name'];?></td>
                                </tr>
                                <tr>
                                    <td class="col-lg-2 cols-sm-2"><?php echo __d('sysadmin', 'Email');?></td>
                                    <td class="col-lg-1 col-sm-1">:</td>
                                    <td><?php echo $user_info['SysAclUser']['email'];?></td>
                                </tr>
                                <tr>
                                    <td class="col-lg-2 cols-sm-2"><?php echo __d('sysadmin', 'User Roles');?></td>
                                    <td class="col-lg-1 col-sm-1">:</td>
                                    <td><?php 
                                    $roles = isset($user_info['Role']) ? $user_info['Role'] : array();
                                    if($roles):
                                        ?><ul class="list-group"><?php
                                        foreach($roles as $role):
                                            ?><li class="list-group-item">
                                                <?php
                                                echo h($role['annotation']);
                                                echo ' (', h($role['name']),')';
                                                ?>
                                            </li><?php
                                        endforeach;
                                        ?></ul><?php
                                    endif;
                                    ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                
                <?php
                #debug($user_info);
                ?>
            </div>
            <div class="panel-footer">
                <span class="fa fa-clock-o"></span>&nbsp;
                <?php 
                
                $latest_activities = isset($user_info['ActivitiesLog']) ? $user_info['ActivitiesLog'] : array();
                $log = isset($latest_activities[0]) ? $latest_activities[0] : array();
                    
                if(isset($log['created'])):
                    $logtime = strtotime($log['created']);
                    echo __d('sysadmin', 'Latest activity on'), '&nbsp;';
                    echo __d('sysadmin', h(date('l', $logtime))),', ',date('j/n/Y H:i', $logtime);
                else:
                    echo __d('sysadmin', 'No latest activity.');
                endif;
                ?>
            </div>
        </div>
    </div>
</div>