<div class="container">
    <div class="" style="padding-top: 40px;">&nbsp;</div>

    <form class="form-signin" method="post">
        <h2 class="form-signin-heading"><?php echo __d('sysadmin', 'Please sign in');?></h2>
        <label for="username" class="sr-only"><?php echo __d('sysadmin', 'Username');?></label>
        <input type="text" maxlength="100" id="username" name="<?php echo md5($this->Session->read('AppUsrToken').'username');?>" class="form-control" placeholder="<?php echo __d('sysadmin', 'Username');?>" required autofocus>
        <label for="inputPassword" class="sr-only"><?php echo __d('sysadmin', 'Password');?></label>
        <input type="password" maxlength="100" id="inputPassword" name="<?php echo md5($this->Session->read('AppUsrToken').'password');?>"  class="form-control" placeholder="<?php echo __d('sysadmin', 'Password');?>" required>
        <!--div class="checkbox">
          <label>
            <input type="checkbox" name="rememberme" value="remember-me"> <?php echo __d('sysadmin', 'Remember me');?>
          </label>
        </div-->
        <button class="btn btn-lg btn-primary btn-block" type="submit"><?php echo __d('sysadmin', 'Sign in');?></button>
      </form>
</div> <!-- /container -->