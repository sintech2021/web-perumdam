<div class="">
    <table class="table table-bordered table-condensed table-striped table-responsive">
        <thead>
            <tr>
                <th>#</th>
                <th><?php echo $this->Paginator->sort('created',
        __d('sysadmin', 'Access Time')); ?></th>
                <th><?php echo $this->Paginator->sort('name',
        __d('sysadmin', 'Action')); ?></th>
                <th><?php echo $this->Paginator->sort('menu_code',
        __d('sysadmin', 'Client IP')); ?></th>
                <th><?php echo $this->Paginator->sort('text',
        __d('sysadmin', 'Query String')); ?></th>
                <th><?php echo $this->Paginator->sort('annotation',
        __d('sysadmin', 'Logs')); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ($activities):
                $counter = $this->Paginator->counter('{:start}');
                foreach ($activities as $i => $v):
                    ?><tr>
                        <td><?php echo $i + $counter; ?>&nbsp;</td>
                    <td><?php echo h($v['ActivitiesLog']['created']); ?>&nbsp;</td>
                    <td><?php echo h($v['ActivitiesLog']['name']); ?>&nbsp;</td>
                    <td><?php echo h($v['ActivitiesLog']['menu_code']); ?>&nbsp;</td>
                    <td><?php echo h($v['ActivitiesLog']['text']); ?>&nbsp;</td>
                    <td><textarea><?php echo h($v['ActivitiesLog']['annotation']); ?></textarea>&nbsp;</td>
                    </tr><?php
        endforeach;
    endif;
    ?>
        </tbody>
    </table>
<?php echo $this->element('paginator', array(),
        array('plugin' => 'sysadmin')); ?>
</div>
<?php
#debug($activities);
?>