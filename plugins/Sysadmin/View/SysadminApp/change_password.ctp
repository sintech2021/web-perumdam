<div class="cahnge-password form">
<?php
echo $this->Form->create('Sysadmin.SysAclUser',
            array(
        'class'         => 'form-horizontal',
        'role'          => 'form',
        'inputDefaults' => array(
            'format'  => array('before', 'label', 'between', 'input', 'error', 'after'),
            'div'     => array('class' => 'form-group'),
            'class'   => 'form-control',
            'label'   => array('class' => 'col-lg-2 col-sm-2 control-label'),
            'between' => '<div class="col-lg-10 col-sm-10">',
            'after'   => '</div>',
            'error'   => array(
                'attributes' => array(
                    'wrap'   => 'div',
                    'class'  => 'alert alert-danger help-inline alert-dismissible alert-auto-dismiss',
                    'escape' => false
                )
            ),
        )
            )
    );
?><fieldset>
    <?php
    
    echo $this->Form->input('password',
                        array(
                             'type'=> 'password',
                    'label'       => array(
                        'text'  => __d('sysadmin', 'Current Password'),
                        'class' => 'col-lg-2 col-sm-2 control-label',
                    ),
                    'value'       => $reqdata['SysAclUser']['password'],
                    'placeholder' => '*****Hidden*****',
                ));
    echo $this->Form->input('new_password',
                        array(
                            'type'=> 'password',
                    'label'       => array(
                        'text'  => __d('sysadmin', 'New Password'),
                        'class' => 'col-lg-2 col-sm-2 control-label',
                    ),
                    'value'       => $reqdata['SysAclUser']['new_password'],
                    'placeholder' => 'New Password',
                ));
    echo $this->Form->input('confirm_password',
                        array(
                             'type'=> 'password',
                    'label'       => array(
                        'text'  => __d('sysadmin', 'Re-type'),
                        'class' => 'col-lg-2 col-sm-2 control-label',
                    ),
                    'value'       => $reqdata['SysAclUser']['confirm_password'],
                    'placeholder' => 'New Password',
                ));
    ?>
</fieldset>
    <?php echo $this->Form->end(array(
        'label'  => __d('sysadmin', 'Submit'),
        'class'  => 'btn btn-default',
        'div'    => 'form-group',
        'before' => '<div class="col-sm-offset-2 col-sm-10 col-lg-offset-2 col-lg-10">',
        'after'  => '</div>'
    )); ?>
</div>