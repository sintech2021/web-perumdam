<?php
echo $this->Html->docType('html5');
?>
<html lang="en">
  <head>
    <?php echo $this->Html->charset(); ?>
    <?php
    echo $this->Html->meta(null, null, array('http-equiv' => 'X-UA-Compatible', 'content' => 'IE=edge'));
    echo $this->Html->meta(null, null, array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1'));    
    echo $this->Html->meta('description', Configure::read('Sysconfig.app.desc'));
    echo $this->Html->meta(null, null, array('name' => 'author', 'content' => Configure::read('Sysconfig.author.name')));
    echo $this->Html->meta(
    'favicon.ico',
    '/favicon.ico',
    array('type' => 'icon')
);
    echo $this->fetch('meta');
    ?>
	<title>
		<?php echo $this->fetch('title'); ?> ~ <?php echo Configure::read('Sysconfig.app.name') ?>
	</title>

    <!-- Bootstrap core CSS -->
    <?php
    $css_ls = array(
        'Sysadmin./bootstrap-3.3.5-dist/css/bootstrap.min',
        'Sysadmin./font-awesome-4.4.0/css/font-awesome.min',
        'Sysadmin./bootstrap-3.3.5-dist/css/layout-full'
    );
    if(isset($enqueued_css)){
        $css_ls = array_merge($css_ls, $enqueued_css);
    }
    echo $this->Html->css($css_ls);
    echo $this->fetch('css');
    ?>
     
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <?php
      echo $this->Html->script('Sysadmin./bootstrap-3.3.5-dist/js/html5shiv.min'); ?>
      <?php
    echo $this->Html->script('Sysadmin./bootstrap-3.3.5-dist/js/respond.min');
    echo $this->fetch('script');
    
      ?>
    <![endif]-->
    <?php
    echo $this->Html->script(
    array(
        'Sysadmin./bootstrap-3.3.5-dist/js/jquery.min', 
        'Sysadmin./bootstrap-3.3.5-dist/js/bootstrap.min', 
       
        'Sysadmin./bootstrap-3.3.5-dist/js/ie10-viewport-bug-workaround', 
    ), 
    array(
    'block' => 'scriptBottom'
    ));
    ?>
  </head>

  <body>
      <?php echo $this->Flash->render(); ?>
      <?php echo $this->fetch('content'); ?>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <?php
    echo $this->fetch('scriptBottom');
    ?>
    
  </body>
</html>
