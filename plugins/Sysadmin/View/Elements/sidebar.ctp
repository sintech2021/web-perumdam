<?php
$list_sidebar_menu = isset($appMenu) ? $appMenu : array();
if ($list_sidebar_menu):
    ?><ul id="side-menu" class="sm sm-simple-clean sm-vertical ">
        <?php
        foreach ($list_sidebar_menu as $i):
            
            ?><li>
                <?php
                echo $this->element('sidebar/i', array('i'=>$i), array('plugin' => 'Sysadmin'));
                ?>
            </li><?php
        endforeach;
        ?>
    </ul><?php
endif;
?>