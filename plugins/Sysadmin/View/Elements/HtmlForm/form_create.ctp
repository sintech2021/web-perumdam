<?php
$form_method = 'post';
 
if(isset($form_type)){
   
    if(in_array($form_type, array('file', 'post', 'get'))){
        $form_method = $form_type;
    }
}
echo $this->Form->create($model_alias,
            array(
        'class'         => 'form-horizontal',
        'role'          => 'form',
        'type'          => $form_method,
        'inputDefaults' => array(
            'format'  => array('before', 'label', 'between', 'input', 'error', 'after'),
            'div'     => array('class' => 'form-group'),
            'class'   => 'form-control',
            'label'   => array('class' => 'col-lg-2 col-sm-2 control-label'),
            'between' => '<div class="col-lg-10 col-sm-10">',
            'after'   => '</div>',
            'error'   => array(
                'attributes' => array(
                    'wrap'   => 'div',
                    'class'  => 'alert alert-danger help-inline alert-dismissible alert-auto-dismiss',
                    'escape' => false
                )
            ),
        )
            )
    ); 
?>