<?php
$submit_label = isset($submit_label) ? $submit_label : __d('sysadmin', 'Submit');
$submit_class = isset($submit_class) ? $submit_class : 'btn btn-default';
$submit_div = isset($submit_div) ? $submit_div : 'form-group';
$submit_before = isset($submit_before) ? $submit_before : '<div class="col-sm-offset-2 col-sm-10 col-lg-offset-2 col-lg-10">';
$submit_after = isset($submit_after) ? $submit_after : '</div>';
echo $this->Form->end(array(
        'label'  => $submit_label,
        'class'  => $submit_class,
        'div'    => $submit_div,
        'before' => $submit_before,
        'after'  => $submit_after
    ));
?>