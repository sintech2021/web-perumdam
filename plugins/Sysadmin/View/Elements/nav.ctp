<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?php
            $home_link_text = Configure::read('Sysconfig.app.name')
                    . ' '
                    . Configure::read('Sysconfig.app.build')
                    . ' '
                    . Configure::read('Sysconfig.app.ver');
            $home_link_url  = array('plugin'     => 'sysadmin', 'controller' => 'sysadmin_app',
                'action'     => 'index');
            echo $this->Html->link($home_link_text, $home_link_url,
                    array('class' => 'navbar-brand'));
            ?>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <?php
                        if (isset($profileMenu)):
                            if ($profileMenu):
                                foreach ($profileMenu as $i => $v):
                                    if (isset($v['resources_type'])):
                                        if ($v['resources_type'] == 'cake_anchor'):
                                            ?><li>
                                                <?php
                                                echo $this->SysHtml->resToCakeUrl($v);
                                                ?>
                                            </li><?php
                                        endif;
                                        if ($v['resources_type'] == 'bs_dropdown_sep'):
                                            ?><li role="separator" class="divider"></li><?php
                                        endif;
                                    endif;
                                endforeach;
                            endif;
                        endif;
                        ?>
                    </ul>
                </li>
            </ul>
            <form class="navbar-form navbar-right">
                <input type="text" class="form-control" placeholder="Search...">
            </form>
        </div>
    </div>
</nav>