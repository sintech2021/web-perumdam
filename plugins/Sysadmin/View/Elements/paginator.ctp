
    <p><?php
        
        
        echo $this->Paginator->counter(array(
            'format' => __d('sysadmin','Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
        ));
        ?>	</p>
    <nav>
        <ul class="pagination">
            <?php echo $this->Paginator->prev('<span aria-hidden="true">&laquo;</span>' , array('escape'=>false, 'tag'=> 'li'), null,
        array('class' => 'prev disabled')); 
            echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
            echo $this->Paginator->next('<span aria-hidden="true">&raquo;</span>', array('escape'=>false, 'tag'=> 'li'), null,
        array('class' => 'next disabled'));
            ?>
        </ul>
    </nav>