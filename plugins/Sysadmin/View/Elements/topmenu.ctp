        <ul class="nav nav-tabs" style="margin-bottom:1em">
            <?php
            if (isset($topmenuls)):
                if ($topmenuls):
                    foreach ($topmenuls as $i => $v):
                        if ($v):
                            ?><li role="presentation" class="<?php echo $v['active']
                            ? 'active' : '';
                            ?>"><?php
                            $link_type = isset($v['link_type']) ? $v['link_type'] : 'link';
                            switch ($link_type):
                                case 'postLink':
                                    echo $this->Form->postLink($v['title'],
                                            $v['url'], $v['options']);
                                    break;
                                default:
                                    echo $this->Html->link($v['title'],
                                            $v['url'], $v['options']);
                                    break;
                            endswitch;
                                    
                                    ?></li><?php
                        endif;
                    endforeach;
                endif;
            endif;
            ?>
        </ul>
        
