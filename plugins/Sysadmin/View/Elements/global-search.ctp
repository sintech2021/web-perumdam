<?php
            if (isset($global_search)):
                if ($global_search):
                    if (isset($global_search['form'])):
                        echo $global_search['form'];
						var_dump($global_search);
                    endif;
                    ?><form class="navbar-form navbar-right " method="post">
                        <input name="_method" value="POST" type="hidden">
                        <input type="hidden" name="global_search[plugin]" value="<?php echo $global_search['plugin']; ?>">
                        <input type="hidden" name="global_search[controller]" value="<?php echo $global_search['controller']; ?>">
                        <input type="hidden" name="global_search[action]" value="<?php echo $global_search['action']; ?>">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-group">
								<?php if(!$global_search['isMulti']){?>
                                   <span class="form-group">
                                        <select class="form-control" name="global_search[field]">
                                            <?php
                                            if ($global_search['allSearchKey']):
                                                foreach ($global_search['allSearchKey'] as $i => $v):
                                                    ?><option value="<?php echo $i; ?>"><?php echo $v; ?></option><?php
                                                endforeach;
                                            endif;
                                            
                                            ?>
                                            <?php
                                            if ($global_search['params']):
                                                foreach ($global_search['params'] as $i => $v):
                                                    ?><option value="<?php echo $i; ?>"<?php 
                                                    if($i==$global_search['field']):
                                                        ?> selected="selected"<?php
                                                    endif;
                                                    ?>><?php echo $v; ?></option><?php
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    
                                    <input type="text" name="global_search[keyword]"  maxlength="300" class="form-control"  id="global_search_keyword"  placeholder="<?php echo __d('sysadmin',
                                                    'Search...');
                                            ?>" value="<?php echo h(trim($global_search['keyword'])); ?>" >
                                    </span>
									<?php } ?>
									
                                    <span class="input-group-btn">
										<?php if(!$global_search['isMulti']){ ?>
                                        <button class="btn btn-default" value="1" name="global_search[do]" type="submit"><span class="fa fa-search"></span></button>
										<?php } ?>
										
                                        <button class="btn btn-warning" value="1" name="global_search[clear]"  type="submit"><span class="fa fa-eraser"></span></button>
										<button class="btn btn-success" value="1" name="global_search[all_keywords]" id="expand_global_search"><span class="fa fa-arrows-alt" data-toggle="modal" data-target="#myModal"></span></button>
                                    </span>
                                </div><!-- /input-group -->
                            </div><!-- /.col-lg-12 -->
                        </div><!-- /.row -->

                    </form><?php
                    
                endif;
            endif;
            ?>
			
			
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Filter Data</h4>
      </div>
      <div class="modal-body">
			<form class="form-horizontal" id="form1" method="post">
						<input type="hidden" name="global_search[plugin]" value="<?php echo $global_search['plugin']; ?>">
                        <input type="hidden" name="global_search[controller]" value="<?php echo $global_search['controller']; ?>">
                        <input type="hidden" name="global_search[action]" value="<?php echo $global_search['action']; ?>">
			   <?php
				if ($global_search['params']):
					foreach ($global_search['params'] as $i => $v):
			   ?>
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label"><?php echo $v; ?></label>
						<div class="col-sm-10">
						
						<?php 
							if(strtolower(trim($v)) =="bulan"){ 
							$month = array(
										'1'=>"Januari",
										'2'=>"Februari",
										'3'=>"Maret",
										'4'=>"April",
										'5'=>"Mei",
										'6'=>"Juni",
										'7'=>"Juli",
										'8'=>"Agustus",
										'9'=>"September",
										'10'=>"Oktober",
										'11'=>"November",
										'12'=>"Desember");
						?>
							<select class="form-control" name="global_search[field][<?php echo $i;?>]">
								<option value=""> - </option>
								<?php foreach($month as $numb=>$string){ ?>
									<option value = "<?php echo $numb;?>" <?php echo (trim($global_search['field'][$i])==$numb)?"selected":"";?> > <?php echo $string; ?> </option>
								<?php } ?>
							
							</select>
						
						 <?php }elseif(strtolower(trim($v)) =="tahun"){
							$years = range((date("Y")-3) , (date("Y")+4));
						 ?>
						 
							<select class="form-control" name="global_search[field][<?php echo $i;?>]">
								<option value=""> - </option>
								<?php foreach($years as $year){ ?>
									<option value = "<?php echo $year;?>" <?php echo (trim($global_search['field'][$i])==$year)?"selected":"";?> > <?php echo $year; ?> </option>
								<?php } ?>
							
							</select>
				
						 <?php
						 } else {
						 ?>
						 
						 <input class="form-control" name="global_search[field][<?php echo $i;?>]" value="<?php echo trim($global_search['field'][$i]);?>">
						 
						 <?php } ?>
						 
						 
						</div>
					</div>
			  <?php
					endforeach;
				endif;
			  ?>
			  
			</form>		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class='fa fa-times'></i></button>
        <button type="submit" class="btn btn-warning" value="1" name="global_search[clear]" form="form1"><span class="fa fa-eraser"></span></button>
        <button type="submit" class="btn btn-primary" name="global_search[do]" form="form1"><i class='fa fa-search'></i></button>
      </div>
    </div>
  </div>
</div>