<div class="resourcesDetails view">
<h2><?php echo __('Resources Detail'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($resourcesDetail['ResourcesDetail']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($resourcesDetail['ResourcesDetail']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Resource'); ?></dt>
		<dd>
			<?php echo $this->Html->link($resourcesDetail['Resource']['name'], array('controller' => 'resources', 'action' => 'view', $resourcesDetail['Resource']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Field'); ?></dt>
		<dd>
			<?php echo h($resourcesDetail['ResourcesDetail']['field']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Value'); ?></dt>
		<dd>
			<?php echo h($resourcesDetail['ResourcesDetail']['value']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Active Status'); ?></dt>
		<dd>
			<?php echo h($resourcesDetail['ResourcesDetail']['active_status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Text'); ?></dt>
		<dd>
			<?php echo h($resourcesDetail['ResourcesDetail']['text']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Annotation'); ?></dt>
		<dd>
			<?php echo h($resourcesDetail['ResourcesDetail']['annotation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($resourcesDetail['ResourcesDetail']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($resourcesDetail['ResourcesDetail']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Resources Detail'), array('action' => 'edit', $resourcesDetail['ResourcesDetail']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Resources Detail'), array('action' => 'delete', $resourcesDetail['ResourcesDetail']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $resourcesDetail['ResourcesDetail']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Resources Details'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resources Detail'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Resources'), array('controller' => 'resources', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resource'), array('controller' => 'resources', 'action' => 'add')); ?> </li>
	</ul>
</div>
