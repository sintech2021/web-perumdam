<div class="resourcesDetails index">
	<h2><?php echo __('Resources Details'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('resource_id'); ?></th>
			<th><?php echo $this->Paginator->sort('field'); ?></th>
			<th><?php echo $this->Paginator->sort('value'); ?></th>
			<th><?php echo $this->Paginator->sort('active_status'); ?></th>
			<th><?php echo $this->Paginator->sort('text'); ?></th>
			<th><?php echo $this->Paginator->sort('annotation'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($resourcesDetails as $resourcesDetail): ?>
	<tr>
		<td><?php echo h($resourcesDetail['ResourcesDetail']['id']); ?>&nbsp;</td>
		<td><?php echo h($resourcesDetail['ResourcesDetail']['name']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($resourcesDetail['Resource']['name'], array('controller' => 'resources', 'action' => 'view', $resourcesDetail['Resource']['id'])); ?>
		</td>
		<td><?php echo h($resourcesDetail['ResourcesDetail']['field']); ?>&nbsp;</td>
		<td><?php echo h($resourcesDetail['ResourcesDetail']['value']); ?>&nbsp;</td>
		<td><?php echo h($resourcesDetail['ResourcesDetail']['active_status']); ?>&nbsp;</td>
		<td><?php echo h($resourcesDetail['ResourcesDetail']['text']); ?>&nbsp;</td>
		<td><?php echo h($resourcesDetail['ResourcesDetail']['annotation']); ?>&nbsp;</td>
		<td><?php echo h($resourcesDetail['ResourcesDetail']['created']); ?>&nbsp;</td>
		<td><?php echo h($resourcesDetail['ResourcesDetail']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $resourcesDetail['ResourcesDetail']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $resourcesDetail['ResourcesDetail']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $resourcesDetail['ResourcesDetail']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $resourcesDetail['ResourcesDetail']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Resources Detail'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Resources'), array('controller' => 'resources', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resource'), array('controller' => 'resources', 'action' => 'add')); ?> </li>
	</ul>
</div>
