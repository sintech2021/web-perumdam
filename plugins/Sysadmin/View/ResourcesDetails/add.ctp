<div class="resourcesDetails form">
<?php echo $this->Form->create('ResourcesDetail'); ?>
	<fieldset>
		<legend><?php echo __('Add Resources Detail'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('resource_id');
		echo $this->Form->input('field');
		echo $this->Form->input('value');
		echo $this->Form->input('active_status');
		echo $this->Form->input('text');
		echo $this->Form->input('annotation');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Resources Details'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Resources'), array('controller' => 'resources', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resource'), array('controller' => 'resources', 'action' => 'add')); ?> </li>
	</ul>
</div>
