<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

__d('sysadmin', 'Content Management');
__d('sysadmin', 'Contents');
__d('sysadmin', 'Media');
__d('sysadmin', 'Categories');
__d('sysadmin', 'Menus');
__d('sysadmin', 'Customers');
__d('sysadmin', 'Customer Registration');
__d('sysadmin', 'Customers List');
__d('sysadmin', 'Customers Billing');
__d('sysadmin', 'Customer Feedback');
__d('sysadmin', 'Visitor Comments');
__d('sysadmin', 'System Administration');
__d('sysadmin', 'Users Management');
__d('sysadmin', 'Users');
__d('sysadmin', 'Roles');
__d('sysadmin', 'Resources Management');
__d('sysadmin', 'Resources Privileges');