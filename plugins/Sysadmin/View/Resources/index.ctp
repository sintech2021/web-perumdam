<div class="">
    <table class="table table-bordered table-condensed table-striped table-responsive">
        <thead>
            <tr>
                <th class="actions"><?php echo __('Actions'); ?></th>
                                    <th><?php echo $this->Paginator->sort('id'); ?></th>
                                    <th><?php echo $this->Paginator->sort('name'); ?></th>
                                    <th><?php echo $this->Paginator->sort('resources_group_id'); ?></th>
                                    <th><?php echo $this->Paginator->sort('resources_type_id'); ?></th>
                                    <th><?php echo $this->Paginator->sort('parent_id'); ?></th>
                                    <th><?php echo $this->Paginator->sort('lft'); ?></th>
                                    <th><?php echo $this->Paginator->sort('rght'); ?></th>
                                    <th><?php echo $this->Paginator->sort('active_status'); ?></th>
                                    <th><?php echo $this->Paginator->sort('text'); ?></th>
                                    <th><?php echo $this->Paginator->sort('annotation'); ?></th>
                                    <th><?php echo $this->Paginator->sort('menu_code'); ?></th>
                                    <th><?php echo $this->Paginator->sort('menu_order'); ?></th>
                                    <th><?php echo $this->Paginator->sort('created'); ?></th>
                                    <th><?php echo $this->Paginator->sort('modified'); ?></th>
                
            </tr>
        </thead>
        <tbody>
            <?php foreach ($resources as $resource): ?>
	<tr>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $resource['Resource']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $resource['Resource']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $resource['Resource']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $resource['Resource']['id']))); ?>
		</td>
		<td><?php echo h($resource['Resource']['id']); ?>&nbsp;</td>
		<td><?php echo h($resource['Resource']['name']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($resource['ResourcesGroup']['name'], array('controller' => 'resources_groups', 'action' => 'view', $resource['ResourcesGroup']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($resource['ResourcesType']['name'], array('controller' => 'resources_types', 'action' => 'view', $resource['ResourcesType']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($resource['ParentResource']['name'], array('controller' => 'resources', 'action' => 'view', $resource['ParentResource']['id'])); ?>
		</td>
		<td><?php echo h($resource['Resource']['lft']); ?>&nbsp;</td>
		<td><?php echo h($resource['Resource']['rght']); ?>&nbsp;</td>
		<td><?php echo h($resource['Resource']['active_status']); ?>&nbsp;</td>
		<td><?php echo h($resource['Resource']['text']); ?>&nbsp;</td>
		<td><?php echo h($resource['Resource']['annotation']); ?>&nbsp;</td>
		<td><?php echo h($resource['Resource']['menu_code']); ?>&nbsp;</td>
		<td><?php echo h($resource['Resource']['menu_order']); ?>&nbsp;</td>
		<td><?php echo h($resource['Resource']['created']); ?>&nbsp;</td>
		<td><?php echo h($resource['Resource']['modified']); ?>&nbsp;</td>
	</tr>
<?php endforeach; ?>
        </tbody>
    </table>
    <p>
        <?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>    </p>
    <div class="paging">
        <?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
    </div>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('New Resource'), array('action' => 'add')); ?></li>
        		<li><?php echo $this->Html->link(__('List Resources Groups'), array('controller' => 'resources_groups', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resources Group'), array('controller' => 'resources_groups', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Resources Types'), array('controller' => 'resources_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resources Type'), array('controller' => 'resources_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Resources'), array('controller' => 'resources', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Parent Resource'), array('controller' => 'resources', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proceedings'), array('controller' => 'proceedings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proceedings'), array('controller' => 'proceedings', 'action' => 'add')); ?> </li>
    </ul>
</div>
