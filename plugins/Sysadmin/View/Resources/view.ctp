<div class="resources view">
<h2><?php echo __('Resource'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($resource['Resource']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($resource['Resource']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Resources Group'); ?></dt>
		<dd>
			<?php echo $this->Html->link($resource['ResourcesGroup']['name'], array('controller' => 'resources_groups', 'action' => 'view', $resource['ResourcesGroup']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Resources Type'); ?></dt>
		<dd>
			<?php echo $this->Html->link($resource['ResourcesType']['name'], array('controller' => 'resources_types', 'action' => 'view', $resource['ResourcesType']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Parent Resource'); ?></dt>
		<dd>
			<?php echo $this->Html->link($resource['ParentResource']['name'], array('controller' => 'resources', 'action' => 'view', $resource['ParentResource']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Lft'); ?></dt>
		<dd>
			<?php echo h($resource['Resource']['lft']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Rght'); ?></dt>
		<dd>
			<?php echo h($resource['Resource']['rght']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Active Status'); ?></dt>
		<dd>
			<?php echo h($resource['Resource']['active_status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Text'); ?></dt>
		<dd>
			<?php echo h($resource['Resource']['text']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Annotation'); ?></dt>
		<dd>
			<?php echo h($resource['Resource']['annotation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Menu Code'); ?></dt>
		<dd>
			<?php echo h($resource['Resource']['menu_code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Menu Order'); ?></dt>
		<dd>
			<?php echo h($resource['Resource']['menu_order']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($resource['Resource']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($resource['Resource']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Resource'), array('action' => 'edit', $resource['Resource']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Resource'), array('action' => 'delete', $resource['Resource']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $resource['Resource']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Resources'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resource'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Resources Groups'), array('controller' => 'resources_groups', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resources Group'), array('controller' => 'resources_groups', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Resources Types'), array('controller' => 'resources_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resources Type'), array('controller' => 'resources_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Resources'), array('controller' => 'resources', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Parent Resource'), array('controller' => 'resources', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proceedings'), array('controller' => 'proceedings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proceedings'), array('controller' => 'proceedings', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Resources'); ?></h3>
	<?php if (!empty($resource['ChildResource'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Resources Group Id'); ?></th>
		<th><?php echo __('Resources Type Id'); ?></th>
		<th><?php echo __('Parent Id'); ?></th>
		<th><?php echo __('Lft'); ?></th>
		<th><?php echo __('Rght'); ?></th>
		<th><?php echo __('Active Status'); ?></th>
		<th><?php echo __('Text'); ?></th>
		<th><?php echo __('Annotation'); ?></th>
		<th><?php echo __('Menu Code'); ?></th>
		<th><?php echo __('Menu Order'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($resource['ChildResource'] as $childResource): ?>
		<tr>
			<td><?php echo $childResource['id']; ?></td>
			<td><?php echo $childResource['name']; ?></td>
			<td><?php echo $childResource['resources_group_id']; ?></td>
			<td><?php echo $childResource['resources_type_id']; ?></td>
			<td><?php echo $childResource['parent_id']; ?></td>
			<td><?php echo $childResource['lft']; ?></td>
			<td><?php echo $childResource['rght']; ?></td>
			<td><?php echo $childResource['active_status']; ?></td>
			<td><?php echo $childResource['text']; ?></td>
			<td><?php echo $childResource['annotation']; ?></td>
			<td><?php echo $childResource['menu_code']; ?></td>
			<td><?php echo $childResource['menu_order']; ?></td>
			<td><?php echo $childResource['created']; ?></td>
			<td><?php echo $childResource['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'resources', 'action' => 'view', $childResource['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'resources', 'action' => 'edit', $childResource['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'resources', 'action' => 'delete', $childResource['id']), array('confirm' => __('Are you sure you want to delete # %s?', $childResource['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Child Resource'), array('controller' => 'resources', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Proceedings'); ?></h3>
	<?php if (!empty($resource['Proceedings'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Active Status'); ?></th>
		<th><?php echo __('Text'); ?></th>
		<th><?php echo __('Annotation'); ?></th>
		<th><?php echo __('Menu Code'); ?></th>
		<th><?php echo __('Menu Order'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($resource['Proceedings'] as $proceedings): ?>
		<tr>
			<td><?php echo $proceedings['id']; ?></td>
			<td><?php echo $proceedings['name']; ?></td>
			<td><?php echo $proceedings['active_status']; ?></td>
			<td><?php echo $proceedings['text']; ?></td>
			<td><?php echo $proceedings['annotation']; ?></td>
			<td><?php echo $proceedings['menu_code']; ?></td>
			<td><?php echo $proceedings['menu_order']; ?></td>
			<td><?php echo $proceedings['created']; ?></td>
			<td><?php echo $proceedings['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'proceedings', 'action' => 'view', $proceedings['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'proceedings', 'action' => 'edit', $proceedings['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'proceedings', 'action' => 'delete', $proceedings['id']), array('confirm' => __('Are you sure you want to delete # %s?', $proceedings['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Proceedings'), array('controller' => 'proceedings', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
