<div class="resources form">
<?php echo $this->Form->create('Resource'); ?>
	<fieldset>
		<legend><?php echo __('Add Resource'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('resources_group_id');
		echo $this->Form->input('resources_type_id');
		echo $this->Form->input('parent_id');
		echo $this->Form->input('lft');
		echo $this->Form->input('rght');
		echo $this->Form->input('active_status');
		echo $this->Form->input('text');
		echo $this->Form->input('annotation');
		echo $this->Form->input('menu_code');
		echo $this->Form->input('menu_order');
		echo $this->Form->input('Proceedings');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Resources'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Resources Groups'), array('controller' => 'resources_groups', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resources Group'), array('controller' => 'resources_groups', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Resources Types'), array('controller' => 'resources_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resources Type'), array('controller' => 'resources_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Resources'), array('controller' => 'resources', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Parent Resource'), array('controller' => 'resources', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proceedings'), array('controller' => 'proceedings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proceedings'), array('controller' => 'proceedings', 'action' => 'add')); ?> </li>
	</ul>
</div>
