<?php

App::uses('HtmlHelper', 'View/Helper');

class SysHtmlHelper extends HtmlHelper
{

    public function rsToCakeUrl($v)
    {
        $res = array();
        if ($v) {
            $res['Resource'] = $v['SysAclRes'];
            if (isset($v['ResourcesType'])) {
                $res['resources_type'] = $v['ResourcesType']['name'];
            }
            if (isset($v['ResourcesDetail'])) {
                if ($v['ResourcesDetail']) {
                    foreach ($v['ResourcesDetail'] as $id => $vd) {
                        if($vd['active_status'] == '1'){
                            $res[$vd['field']] = $vd['value'];
                        }
                        
                    }
                }
            }
        }
        return $this->resToCakeUrl($res);
    }

    public function resToCakeUrl($res)
    {

        //link : {pre_text,text_domain,text,post_text, json_url, json_options}
        if (isset($res['resources_type'])) {
            if ($res['resources_type'] == 'cake_anchor') {
                $text = isset($res['Resource']['text']) ? $res['Resource']['text'] : "";
                
                if (isset($res['text'])) {
                    $text = $res['text'];
                }
                $text_domain = 'sysadmin';
                if (isset($res['text_domain'])) {
                    if ($res['text_domain']) {
                        $text_domain = $res['text_domain'];
                    }
                }
                $text = __d($text_domain, $text);

                if (isset($res['pre_text'])) {
                    $text = $res['pre_text'] . $text;
                }
                if (isset($res['post_text'])) {
                    $text .= $res['post_text'];
                }


                $url = "#";
                if (isset($res['url'])) {
                    $url = $res['url'];
                }
                if (isset($res['json_url'])) {
                    $url = (array) json_decode($res['json_url']);
                }
                $options = array('escape' => false);
                if (isset($res['json_options'])) {
                    $options = (array) json_decode($res['json_options']);
                }
                if(is_array($url)){
                    if(isset($url['plugin']) && isset($url['controller']) && isset($url['action'])){
                        
                        $class = "current";
                        if($url['plugin']==$this->request->params['plugin'] && $url['controller']==$this->request->params['controller']){
                            $options['class'] = isset($options['class']) ? $options['class'].' '.$class: $class;
                        }
                    }
                }
                return $this->link($text, $url, $options);
            }
        }
    }

}
