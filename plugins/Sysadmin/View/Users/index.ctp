
<div class="">
    <?php
    # debug($users_roles);
    #debug($users);
    ?>
    <table class="table table-bordered table-condensed table-striped table-responsive">
        <thead>
            <tr>
                <?php
                if (isset($SysAcl) || isset($SysAclUserActivation)):
                    if ($SysAcl['read'] || $SysAcl['edit'] || $SysAcl['delete'] || $SysAclUserActivation['execute']):
                        ?>
                        <th class="actions"><?php echo __('Actions'); ?></th>
                        <?php
                    endif;
                endif;
                ?>

                <th>#</th>
                <th><?php echo $this->Paginator->sort('name'); ?></th>
                <th><?php echo $this->Paginator->sort('username'); ?></th>
                <th><?php echo $this->Paginator->sort('email'); ?></th>
                <th><?php echo $this->Paginator->sort('user roles'); ?></th>
                <th><?php echo $this->Paginator->sort('registered date'); ?></th>
                <th><?php echo $this->Paginator->sort('active since'); ?></th>

            </tr> 
        </thead>
        <tbody>
<?php
$counter = $this->Paginator->counter('{:start}');
foreach ($users as $i => $user):
    $uid = $user['User']['id'];
    ?>
                <tr>
                <?php
                if (isset($SysAcl) || isset($SysAclUserActivation)):
                    if ($SysAcl['read'] || $SysAcl['edit'] || $SysAcl['delete'] || $SysAclUserActivation['execute']):
                        ?> <td class="actions" nowrap="nowrap">
                            <?php
                            if ($SysAcl['read']):
                            /*
                             * echo $this->Html->link(
                              '<span class="fa fa-search-plus fa-border fa-fw"></span>',
                              array(
                              'action' => 'view',
                              $user['User']['id']
                              ),
                              array('escape'=>false)
                              );
                             * 
                             */
                            endif;
                            if ($SysAcl['edit']):
                                echo $this->Html->link(
                                        '<span class="fa fa-edit fa-border fa-fw text-warning"></span>',
                                        array(
                                    'action' => 'edit', $user['User']['id']
                                        ), array('escape' => false)
                                );
                            endif;
                            if ($SysAcl['delete'] && !in_array($user['User']['id'],
                                            $protectedUsr)):
                                echo $this->Form->postLink(
                                        '<span class="fa fa-trash fa-border fa-fw text-danger"></span>',
                                        array(
                                    'action' => 'delete', $user['User']['id']
                                        ),
                                        array(
                                    'confirm' => __d('sysadmin',
                                            'Are you sure you want to delete # %s?',
                                            $user['User']['name']),
                                    'escape'  => false
                                        )
                                );
                            endif;
                            if ($SysAclUserActivation['execute'] && !in_array($user['User']['id'],
                                            $protectedUsr)):
                                if ($user['User']['active_dt']):
                                    echo $this->Form->postLink(
                                            '<span class="fa fa-ban fa-border fa-fw text-warning"></span>',
                                            array(
                                        'action' => 'user_activation', $user['User']['id'],
                                        0
                                            ),
                                            array(
                                        'confirm' => __d('sysadmin',
                                                'User could not be able to login anymore. Are you sure you want to de-activate # %s?',
                                                $user['User']['name']),
                                        'escape'  => false
                                            )
                                    );
                                else:
                                    echo $this->Form->postLink(
                                            '<span class="fa fa-check-circle-o fa-border fa-fw text-info"></span>',
                                            array(
                                        'action' => 'user_activation', $user['User']['id'],
                                        1
                                            ),
                                            array(
                                        'confirm' => __d('sysadmin',
                                                'Are you sure you want to activate # %s?',
                                                $user['User']['name']),
                                        'escape'  => false
                                            )
                                    );
                                endif;

                            endif;
                            ?>
                            </td><?php
                        endif;
                    endif;
                    ?>

                    <td><?php echo $i + $counter; ?>&nbsp;</td>
                    <td><?php echo h($user['User']['name']); ?>&nbsp;</td>
                    <td><?php echo h($user['User']['username']); ?>&nbsp;</td>
                    <td><?php echo h($user['User']['email']); ?>&nbsp;</td>
                    <td><?php
                        if (isset($users_roles[$uid])):
                            foreach ($users_roles[$uid] as $role):
                                ?><a title="<?php echo h($role['text']); ?>"><?php echo h($role['text']); ?></a><br><?php
                            endforeach;

                        endif;
                        ?>&nbsp;</td>
                    <td><?php echo h($user['User']['registered_dt']); ?>&nbsp;</td>
                    <td><?php echo h($user['User']['active_dt']); ?>&nbsp;</td>
                </tr>
<?php endforeach; ?>
        </tbody>
    </table>
<?php echo $this->element('paginator', array(), array('plugin' => 'sysadmin')); ?>

</div>
