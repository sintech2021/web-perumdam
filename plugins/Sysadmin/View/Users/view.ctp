<div class="users view">
<h2><?php echo __('User'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($user['User']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($user['User']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Username'); ?></dt>
		<dd>
			<?php echo h($user['User']['username']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Password'); ?></dt>
		<dd>
			<?php echo h($user['User']['password']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Token'); ?></dt>
		<dd>
			<?php echo h($user['User']['token']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($user['User']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Registered Dt'); ?></dt>
		<dd>
			<?php echo h($user['User']['registered_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Active Dt'); ?></dt>
		<dd>
			<?php echo h($user['User']['active_dt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Active Status'); ?></dt>
		<dd>
			<?php echo h($user['User']['active_status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Text'); ?></dt>
		<dd>
			<?php echo h($user['User']['text']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Annotation'); ?></dt>
		<dd>
			<?php echo h($user['User']['annotation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($user['User']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($user['User']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit User'), array('action' => 'edit', $user['User']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete User'), array('action' => 'delete', $user['User']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $user['User']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Activities Logs'), array('controller' => 'activities_logs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Activities Log'), array('controller' => 'activities_logs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Roles'), array('controller' => 'roles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Role'), array('controller' => 'roles', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Activities Logs'); ?></h3>
	<?php if (!empty($user['ActivitiesLog'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Active Status'); ?></th>
		<th><?php echo __('Text'); ?></th>
		<th><?php echo __('Annotation'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($user['ActivitiesLog'] as $activitiesLog): ?>
		<tr>
			<td><?php echo $activitiesLog['id']; ?></td>
			<td><?php echo $activitiesLog['name']; ?></td>
			<td><?php echo $activitiesLog['user_id']; ?></td>
			<td><?php echo $activitiesLog['active_status']; ?></td>
			<td><?php echo $activitiesLog['text']; ?></td>
			<td><?php echo $activitiesLog['annotation']; ?></td>
			<td><?php echo $activitiesLog['created']; ?></td>
			<td><?php echo $activitiesLog['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'activities_logs', 'action' => 'view', $activitiesLog['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'activities_logs', 'action' => 'edit', $activitiesLog['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'activities_logs', 'action' => 'delete', $activitiesLog['id']), array('confirm' => __('Are you sure you want to delete # %s?', $activitiesLog['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Activities Log'), array('controller' => 'activities_logs', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Roles'); ?></h3>
	<?php if (!empty($user['Role'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Active Status'); ?></th>
		<th><?php echo __('Text'); ?></th>
		<th><?php echo __('Annotation'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($user['Role'] as $role): ?>
		<tr>
			<td><?php echo $role['id']; ?></td>
			<td><?php echo $role['name']; ?></td>
			<td><?php echo $role['active_status']; ?></td>
			<td><?php echo $role['text']; ?></td>
			<td><?php echo $role['annotation']; ?></td>
			<td><?php echo $role['created']; ?></td>
			<td><?php echo $role['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'roles', 'action' => 'view', $role['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'roles', 'action' => 'edit', $role['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'roles', 'action' => 'delete', $role['id']), array('confirm' => __('Are you sure you want to delete # %s?', $role['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Role'), array('controller' => 'roles', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
