<h1 class="page-header"><?php echo h(__d('sysadmin', $role['Role']['text'])); ?></h1>
<div class="">
    <?php
    //init
    $page_current = $this->Paginator->current();
    ?>
    <table class="table table-bordered table-condensed table-striped table-responsive">
        <thead>
            <tr>
                <th>#</th>
                <th><?php
                    echo $this->Paginator->sort('text',
                            __d('sysadmin', 'Privileges')), 
                            " (",
                            $this->Paginator->sort('name', __d('sysadmin', 'ID')),
                            ")";
                    ?></th>       
                <th><?php echo $this->Paginator->sort('ParentResource.text', __d('sysadmin', 'Parent'));?></th>
                <?php
                    foreach ($proceedings as $ip => $proc):
                        ?><th><?php echo $proc['Proceeding']['menu_code']; ?></th><?php
                    endforeach;
                    ?>
            </tr>
        </thead>
        <tbody>
            <?php
            $counter    = $this->Paginator->counter('{:start}');
            $url_drop   = array($role['Role']['id'], 'priv_act' => md5('drop' . date('j')),
                'page'     => $page_current);
            $url_set    = array($role['Role']['id'], 'priv_act' => md5('set' . date('j')),
                'page'     => $page_current);
            $checklists = array(
                array(
                    'title'   => '<i class="fa fa-times"></i>&nbsp;',
                    'url'     => $url_drop,
                    'options' => array(
                        'class'  => 'btn btn-sm btn-danger',
                        'escape' => false)
                ),
                array(
                    'title'   => '<i class="fa fa-check"></i>&nbsp;',
                    'url'     => $url_set,
                    'options' => array(
                        'class'  => 'btn btn-sm btn-primary',
                        'escape' => false)
                ),
            );
            //loop
            foreach ($resources as $i => $resource):
                ?><tr>
                    <td><?php
                        echo $counter++;
                        
                        ?></td>
                    <td><a class="btn" title="<?php echo h($resource['Resource']['annotation']); ?>"><span class="fa fa-info-circle"></span></a>&nbsp;
                        <?php 
                        echo h($priv_names[$resource['Resource']['id']]); 
                        ?></td>
                    <td><?php 
                        echo isset($resource['ParentResource']['text']) ? h($resource['ParentResource']['text']) : ""; 
                        ?></td>
                    <?php
                    foreach ($proceedings as $ip => $proc):
                        ?><td>
                            <?php
                            if (isset($privileges[$resource['Resource']['id']][$proc['Proceeding']['id']])):
                                ?><div class="btn-group">
                                <?php
                                $selected = 0;
                                if (isset($permissions[$resource['Resource']['id']][$proc['Proceeding']['id']])) :
                                    $selected = $permissions[$resource['Resource']['id']][$proc['Proceeding']['id']];
                                endif;
                                $opposed        = $selected ? 0 : 1;
                                $btnclass       = $selected ? 'primary' : 'danger';
                                ?>
                                    <button type="button" class="btn btn-default dropdown-toggle  btn-sm btn-<?php echo $btnclass; ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <?php
                                        echo $checklists[$selected]['title'];
                                        ?>
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><?php
                                            $options        = $checklists[$opposed]['options'];
                                            $url            = $checklists[$opposed]['url'];
                                            $url['res_id']  = $resource['Resource']['id'];
                                            $url['proc_id'] = $proc['Proceeding']['id'];
                                            echo $this->Html->link($checklists[$opposed]['title'],
                                                    $url, $options);
                                            ?></li>
                                    </ul>
                                </div><?php
                            else:
                            endif;
                            ?>

                        </td><?php
                    endforeach;
                    ?>
                </tr>
                <?php
            endforeach;
            ?>
        </tbody>
    </table>
    <?php
    echo $this->element('paginator', array(), array('plugin' => 'sysadmin'));
    ?>
</div>
