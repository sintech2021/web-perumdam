<div class="roles form">
<?php echo $this->Form->create('Role',
            array(
        'class'         => 'form-horizontal',
        'role'          => 'form',
        'inputDefaults' => array(
            'format'  => array('before', 'label', 'between', 'input', 'error', 'after'),
            'div'     => array('class' => 'form-group'),
            'class'   => 'form-control',
            'label'   => array('class' => 'col-lg-2 col-sm-2 control-label'),
            'between' => '<div class="col-lg-10 col-sm-10">',
            'after'   => '</div>',
            'error'   => array(
                'attributes' => array(
                    'wrap'   => 'div',
                    'class'  => 'alert alert-danger help-inline alert-dismissible alert-auto-dismiss',
                    'escape' => false
                )
            ),
        )
            )
    ); ?>
	<fieldset>
	<?php
		echo $this->Form->input('name',
                        array(
                    'label' => array(
                        'text'  => __d('sysadmin', 'Name'),
                        'class' => 'col-lg-2 col-sm-2 control-label',
                    )
                ));
		echo $this->Form->input('text',
                        array(
                    'label' => array(
                        'text'  => __d('sysadmin', 'Full Text'),
                        'class' => 'col-lg-2 col-sm-2 control-label',
                    )
                ));
		echo $this->Form->input('annotation',
                        array(
                    'label' => array(
                        'text'  => __d('sysadmin', 'Description'),
                        'class' => 'col-lg-2 col-sm-2 control-label',
                    )
                ));
		
		echo $this->Form->input('menu_code',
                        array(
                    'label' => array(
                        'text'  => __d('sysadmin', 'Code'),
                        'class' => 'col-lg-2 col-sm-2 control-label',
                    )
                ));
	?>
	</fieldset><?php echo $this->Form->end(array(
        'label'  => __d('sysadmin', 'Submit'),
        'class'  => 'btn btn-default',
        'div'    => 'form-group',
        'before' => '<div class="col-sm-offset-2 col-sm-10 col-lg-offset-2 col-lg-10">',
        'after'  => '</div>'
    )); ?>
</div>
