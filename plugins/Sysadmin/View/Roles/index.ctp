<div class="">
    <?php
    #debug($role);
    ?>
    
    <table class="table table-bordered table-condensed table-striped table-responsive">
        <thead>
            <tr>
                <?php
                if (isset($SysAcl)  || isset($SysAclPrivilegesManagement)):
                    if ($SysAcl['read'] || $SysAcl['edit'] || $SysAcl['delete'] || $SysAclPrivilegesManagement['browse']):
                        ?>
                        <th class="actions"><?php echo __d('sysadmin','Actions'); ?></th>
                        <?php
                    endif;
                endif;
                ?>
                <th>#</th>
                <th><?php echo $this->Paginator->sort('name'); ?></th>
                <th><?php echo $this->Paginator->sort('full text'); ?></th>
                <th><?php echo $this->Paginator->sort('description'); ?></th>
                <th><?php echo $this->Paginator->sort('code'); ?></th>

            </tr>
        </thead>
        <tbody>
            <?php
            
            $counter = $this->Paginator->counter('{:start}');
            foreach ($roles as $i => $role):
                $id = $role['Role']['id'];
                ?>
                <tr>
                    <?php
                if (isset($SysAcl)  || isset($SysAclPrivilegesManagement)):
                    if ($SysAcl['read'] || $SysAcl['edit'] || $SysAcl['delete'] || $SysAclPrivilegesManagement['browse']):
                        ?> <td class="actions" nowrap="nowrap">
                            <?php
                            
                            if ($SysAcl['edit']):
                                echo $this->Html->link(
                                        '<span class="fa fa-edit fa-border text-warning"></span>',
                                        array(
                                    'action' => 'edit', $role['Role']['id']
                                        ), array('escape' => false)
                                );
                            endif;
                            if ($SysAcl['delete'] && !in_array($id, $protected_roles)):
                                echo $this->Form->postLink(
                                        '<span class="fa fa-trash fa-border text-danger"></span>',
                                        array(
                                    'action' => 'delete', $role['Role']['id']
                                        ),
                                        array(
                                    'confirm' => __d('sysadmin',
                                            'Are you sure you want to delete # %s?',
                                            h($role['Role']['name'])),
                                    'escape'  => false
                                        )
                                );
                            endif;
                            if($SysAclPrivilegesManagement['browse']  && !in_array($id, $protected_roles)):
                                echo $this->Html->link(
                                        '<span class="fa fa-cog fa-border text-success"></span>',
                                        array(
                                    'action' => 'privileges_management', $role['Role']['id']
                                        ), array('escape' => false)
                                );
                            endif;
                            
                            ?>
                            </td><?php
                        endif;
                    endif;
                    ?>
                    
                    <td><?php echo $i+$counter; ?>&nbsp;</td>
                    <td><?php echo h($role['Role']['name']); ?>&nbsp;</td>
                    <td><?php echo h($role['Role']['text']); ?>&nbsp;</td>
                    <td><?php echo h($role['Role']['annotation']); ?>&nbsp;</td>
                    <td><?php echo h($role['Role']['menu_code']); ?>&nbsp;</td>
                </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <?php echo $this->element('paginator', array(), array('plugin' => 'sysadmin')); ?>
</div>
