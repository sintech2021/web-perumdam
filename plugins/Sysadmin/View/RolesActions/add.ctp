<div class="rolesActions form">
<?php echo $this->Form->create('RolesAction'); ?>
	<fieldset>
		<legend><?php echo __('Add Roles Action'); ?></legend>
	<?php
		echo $this->Form->input('users_role_id');
		echo $this->Form->input('resources_action_id');
		echo $this->Form->input('active_status');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Roles Actions'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Users Roles'), array('controller' => 'users_roles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Users Role'), array('controller' => 'users_roles', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Resources Actions'), array('controller' => 'resources_actions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resources Action'), array('controller' => 'resources_actions', 'action' => 'add')); ?> </li>
	</ul>
</div>
