<div class="rolesActions view">
<h2><?php echo __('Roles Action'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($rolesAction['RolesAction']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Users Role'); ?></dt>
		<dd>
			<?php echo $this->Html->link($rolesAction['UsersRole']['id'], array('controller' => 'users_roles', 'action' => 'view', $rolesAction['UsersRole']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Resources Action'); ?></dt>
		<dd>
			<?php echo $this->Html->link($rolesAction['ResourcesAction']['id'], array('controller' => 'resources_actions', 'action' => 'view', $rolesAction['ResourcesAction']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Active Status'); ?></dt>
		<dd>
			<?php echo h($rolesAction['RolesAction']['active_status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($rolesAction['RolesAction']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($rolesAction['RolesAction']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Roles Action'), array('action' => 'edit', $rolesAction['RolesAction']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Roles Action'), array('action' => 'delete', $rolesAction['RolesAction']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $rolesAction['RolesAction']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Roles Actions'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Roles Action'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users Roles'), array('controller' => 'users_roles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Users Role'), array('controller' => 'users_roles', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Resources Actions'), array('controller' => 'resources_actions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resources Action'), array('controller' => 'resources_actions', 'action' => 'add')); ?> </li>
	</ul>
</div>
