<div class="rolesActions index">
	<h2><?php echo __('Roles Actions'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('users_role_id'); ?></th>
			<th><?php echo $this->Paginator->sort('resources_action_id'); ?></th>
			<th><?php echo $this->Paginator->sort('active_status'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($rolesActions as $rolesAction): ?>
	<tr>
		<td><?php echo h($rolesAction['RolesAction']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($rolesAction['UsersRole']['id'], array('controller' => 'users_roles', 'action' => 'view', $rolesAction['UsersRole']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($rolesAction['ResourcesAction']['id'], array('controller' => 'resources_actions', 'action' => 'view', $rolesAction['ResourcesAction']['id'])); ?>
		</td>
		<td><?php echo h($rolesAction['RolesAction']['active_status']); ?>&nbsp;</td>
		<td><?php echo h($rolesAction['RolesAction']['created']); ?>&nbsp;</td>
		<td><?php echo h($rolesAction['RolesAction']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $rolesAction['RolesAction']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $rolesAction['RolesAction']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $rolesAction['RolesAction']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $rolesAction['RolesAction']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Roles Action'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Users Roles'), array('controller' => 'users_roles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Users Role'), array('controller' => 'users_roles', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Resources Actions'), array('controller' => 'resources_actions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resources Action'), array('controller' => 'resources_actions', 'action' => 'add')); ?> </li>
	</ul>
</div>
