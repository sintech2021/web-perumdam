<div class="resourcesActions index">
	<h2><?php echo __('Resources Actions'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('resource_id'); ?></th>
			<th><?php echo $this->Paginator->sort('action_id'); ?></th>
			<th><?php echo $this->Paginator->sort('active_status'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($resourcesActions as $resourcesAction): ?>
	<tr>
		<td><?php echo h($resourcesAction['ResourcesAction']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($resourcesAction['Resource']['name'], array('controller' => 'resources', 'action' => 'view', $resourcesAction['Resource']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($resourcesAction['Action']['name'], array('controller' => 'actions', 'action' => 'view', $resourcesAction['Action']['id'])); ?>
		</td>
		<td><?php echo h($resourcesAction['ResourcesAction']['active_status']); ?>&nbsp;</td>
		<td><?php echo h($resourcesAction['ResourcesAction']['created']); ?>&nbsp;</td>
		<td><?php echo h($resourcesAction['ResourcesAction']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $resourcesAction['ResourcesAction']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $resourcesAction['ResourcesAction']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $resourcesAction['ResourcesAction']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $resourcesAction['ResourcesAction']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Resources Action'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Resources'), array('controller' => 'resources', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resource'), array('controller' => 'resources', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Actions'), array('controller' => 'actions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Action'), array('controller' => 'actions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Roles Actions'), array('controller' => 'roles_actions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Roles Action'), array('controller' => 'roles_actions', 'action' => 'add')); ?> </li>
	</ul>
</div>
