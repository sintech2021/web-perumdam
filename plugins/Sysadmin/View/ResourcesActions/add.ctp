<div class="resourcesActions form">
<?php echo $this->Form->create('ResourcesAction'); ?>
	<fieldset>
		<legend><?php echo __('Add Resources Action'); ?></legend>
	<?php
		echo $this->Form->input('resource_id');
		echo $this->Form->input('action_id');
		echo $this->Form->input('active_status');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Resources Actions'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Resources'), array('controller' => 'resources', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resource'), array('controller' => 'resources', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Actions'), array('controller' => 'actions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Action'), array('controller' => 'actions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Roles Actions'), array('controller' => 'roles_actions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Roles Action'), array('controller' => 'roles_actions', 'action' => 'add')); ?> </li>
	</ul>
</div>
