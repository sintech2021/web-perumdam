<div class="resourcesActions view">
<h2><?php echo __('Resources Action'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($resourcesAction['ResourcesAction']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Resource'); ?></dt>
		<dd>
			<?php echo $this->Html->link($resourcesAction['Resource']['name'], array('controller' => 'resources', 'action' => 'view', $resourcesAction['Resource']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Action'); ?></dt>
		<dd>
			<?php echo $this->Html->link($resourcesAction['Action']['name'], array('controller' => 'actions', 'action' => 'view', $resourcesAction['Action']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Active Status'); ?></dt>
		<dd>
			<?php echo h($resourcesAction['ResourcesAction']['active_status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($resourcesAction['ResourcesAction']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($resourcesAction['ResourcesAction']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Resources Action'), array('action' => 'edit', $resourcesAction['ResourcesAction']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Resources Action'), array('action' => 'delete', $resourcesAction['ResourcesAction']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $resourcesAction['ResourcesAction']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Resources Actions'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resources Action'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Resources'), array('controller' => 'resources', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resource'), array('controller' => 'resources', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Actions'), array('controller' => 'actions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Action'), array('controller' => 'actions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Roles Actions'), array('controller' => 'roles_actions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Roles Action'), array('controller' => 'roles_actions', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Roles Actions'); ?></h3>
	<?php if (!empty($resourcesAction['RolesAction'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Users Role Id'); ?></th>
		<th><?php echo __('Resources Action Id'); ?></th>
		<th><?php echo __('Active Status'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($resourcesAction['RolesAction'] as $rolesAction): ?>
		<tr>
			<td><?php echo $rolesAction['id']; ?></td>
			<td><?php echo $rolesAction['users_role_id']; ?></td>
			<td><?php echo $rolesAction['resources_action_id']; ?></td>
			<td><?php echo $rolesAction['active_status']; ?></td>
			<td><?php echo $rolesAction['created']; ?></td>
			<td><?php echo $rolesAction['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'roles_actions', 'action' => 'view', $rolesAction['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'roles_actions', 'action' => 'edit', $rolesAction['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'roles_actions', 'action' => 'delete', $rolesAction['id']), array('confirm' => __('Are you sure you want to delete # %s?', $rolesAction['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Roles Action'), array('controller' => 'roles_actions', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
