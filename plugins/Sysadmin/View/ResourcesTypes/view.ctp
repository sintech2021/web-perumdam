<div class="resourcesTypes view">
<h2><?php echo __('Resources Type'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($resourcesType['ResourcesType']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($resourcesType['ResourcesType']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Active Status'); ?></dt>
		<dd>
			<?php echo h($resourcesType['ResourcesType']['active_status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Text'); ?></dt>
		<dd>
			<?php echo h($resourcesType['ResourcesType']['text']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Annotation'); ?></dt>
		<dd>
			<?php echo h($resourcesType['ResourcesType']['annotation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($resourcesType['ResourcesType']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($resourcesType['ResourcesType']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Resources Type'), array('action' => 'edit', $resourcesType['ResourcesType']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Resources Type'), array('action' => 'delete', $resourcesType['ResourcesType']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $resourcesType['ResourcesType']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Resources Types'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resources Type'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Resources'), array('controller' => 'resources', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resource'), array('controller' => 'resources', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Resources'); ?></h3>
	<?php if (!empty($resourcesType['Resource'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Resources Group Id'); ?></th>
		<th><?php echo __('Resources Type Id'); ?></th>
		<th><?php echo __('Parent Id'); ?></th>
		<th><?php echo __('Lft'); ?></th>
		<th><?php echo __('Rght'); ?></th>
		<th><?php echo __('Active Status'); ?></th>
		<th><?php echo __('Text'); ?></th>
		<th><?php echo __('Annotation'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($resourcesType['Resource'] as $resource): ?>
		<tr>
			<td><?php echo $resource['id']; ?></td>
			<td><?php echo $resource['name']; ?></td>
			<td><?php echo $resource['resources_group_id']; ?></td>
			<td><?php echo $resource['resources_type_id']; ?></td>
			<td><?php echo $resource['parent_id']; ?></td>
			<td><?php echo $resource['lft']; ?></td>
			<td><?php echo $resource['rght']; ?></td>
			<td><?php echo $resource['active_status']; ?></td>
			<td><?php echo $resource['text']; ?></td>
			<td><?php echo $resource['annotation']; ?></td>
			<td><?php echo $resource['created']; ?></td>
			<td><?php echo $resource['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'resources', 'action' => 'view', $resource['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'resources', 'action' => 'edit', $resource['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'resources', 'action' => 'delete', $resource['id']), array('confirm' => __('Are you sure you want to delete # %s?', $resource['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Resource'), array('controller' => 'resources', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
