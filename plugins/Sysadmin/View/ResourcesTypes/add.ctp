<div class="resourcesTypes form">
<?php echo $this->Form->create('ResourcesType'); ?>
	<fieldset>
		<legend><?php echo __('Add Resources Type'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('active_status');
		echo $this->Form->input('text');
		echo $this->Form->input('annotation');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Resources Types'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Resources'), array('controller' => 'resources', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resource'), array('controller' => 'resources', 'action' => 'add')); ?> </li>
	</ul>
</div>
