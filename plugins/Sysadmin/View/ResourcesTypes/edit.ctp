<div class="resourcesTypes form">
<?php echo $this->Form->create('ResourcesType'); ?>
	<fieldset>
		<legend><?php echo __('Edit Resources Type'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('active_status');
		echo $this->Form->input('text');
		echo $this->Form->input('annotation');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('ResourcesType.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('ResourcesType.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Resources Types'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Resources'), array('controller' => 'resources', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resource'), array('controller' => 'resources', 'action' => 'add')); ?> </li>
	</ul>
</div>
