<div class="usersPreferences view">
<h2><?php echo __('Users Preference'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($usersPreference['UsersPreference']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($usersPreference['UsersPreference']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($usersPreference['User']['name'], array('controller' => 'users', 'action' => 'view', $usersPreference['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Value'); ?></dt>
		<dd>
			<?php echo h($usersPreference['UsersPreference']['value']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Active Status'); ?></dt>
		<dd>
			<?php echo h($usersPreference['UsersPreference']['active_status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Text'); ?></dt>
		<dd>
			<?php echo h($usersPreference['UsersPreference']['text']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Annotation'); ?></dt>
		<dd>
			<?php echo h($usersPreference['UsersPreference']['annotation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($usersPreference['UsersPreference']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($usersPreference['UsersPreference']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Users Preference'), array('action' => 'edit', $usersPreference['UsersPreference']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Users Preference'), array('action' => 'delete', $usersPreference['UsersPreference']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $usersPreference['UsersPreference']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Users Preferences'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Users Preference'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
