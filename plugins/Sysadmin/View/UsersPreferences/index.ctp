<div class="usersPreferences index">
	<h2><?php echo __('Users Preferences'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('value'); ?></th>
			<th><?php echo $this->Paginator->sort('active_status'); ?></th>
			<th><?php echo $this->Paginator->sort('text'); ?></th>
			<th><?php echo $this->Paginator->sort('annotation'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($usersPreferences as $usersPreference): ?>
	<tr>
		<td><?php echo h($usersPreference['UsersPreference']['id']); ?>&nbsp;</td>
		<td><?php echo h($usersPreference['UsersPreference']['name']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($usersPreference['User']['name'], array('controller' => 'users', 'action' => 'view', $usersPreference['User']['id'])); ?>
		</td>
		<td><?php echo h($usersPreference['UsersPreference']['value']); ?>&nbsp;</td>
		<td><?php echo h($usersPreference['UsersPreference']['active_status']); ?>&nbsp;</td>
		<td><?php echo h($usersPreference['UsersPreference']['text']); ?>&nbsp;</td>
		<td><?php echo h($usersPreference['UsersPreference']['annotation']); ?>&nbsp;</td>
		<td><?php echo h($usersPreference['UsersPreference']['created']); ?>&nbsp;</td>
		<td><?php echo h($usersPreference['UsersPreference']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $usersPreference['UsersPreference']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $usersPreference['UsersPreference']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $usersPreference['UsersPreference']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $usersPreference['UsersPreference']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Users Preference'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
