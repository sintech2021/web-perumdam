<div class="usersRoles form">
<?php echo $this->Form->create('UsersRole'); ?>
	<fieldset>
		<legend><?php echo __('Edit Users Role'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('user_id');
		echo $this->Form->input('role_id');
		echo $this->Form->input('active_status');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('UsersRole.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('UsersRole.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Users Roles'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Roles'), array('controller' => 'roles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Role'), array('controller' => 'roles', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Roles Actions'), array('controller' => 'roles_actions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Roles Action'), array('controller' => 'roles_actions', 'action' => 'add')); ?> </li>
	</ul>
</div>
