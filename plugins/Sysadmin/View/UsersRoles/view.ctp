<div class="usersRoles view">
<h2><?php echo __('Users Role'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($usersRole['UsersRole']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($usersRole['User']['name'], array('controller' => 'users', 'action' => 'view', $usersRole['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Role'); ?></dt>
		<dd>
			<?php echo $this->Html->link($usersRole['Role']['name'], array('controller' => 'roles', 'action' => 'view', $usersRole['Role']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Active Status'); ?></dt>
		<dd>
			<?php echo h($usersRole['UsersRole']['active_status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($usersRole['UsersRole']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($usersRole['UsersRole']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Users Role'), array('action' => 'edit', $usersRole['UsersRole']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Users Role'), array('action' => 'delete', $usersRole['UsersRole']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $usersRole['UsersRole']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Users Roles'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Users Role'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Roles'), array('controller' => 'roles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Role'), array('controller' => 'roles', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Roles Actions'), array('controller' => 'roles_actions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Roles Action'), array('controller' => 'roles_actions', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Roles Actions'); ?></h3>
	<?php if (!empty($usersRole['RolesAction'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Users Role Id'); ?></th>
		<th><?php echo __('Resources Action Id'); ?></th>
		<th><?php echo __('Active Status'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($usersRole['RolesAction'] as $rolesAction): ?>
		<tr>
			<td><?php echo $rolesAction['id']; ?></td>
			<td><?php echo $rolesAction['users_role_id']; ?></td>
			<td><?php echo $rolesAction['resources_action_id']; ?></td>
			<td><?php echo $rolesAction['active_status']; ?></td>
			<td><?php echo $rolesAction['created']; ?></td>
			<td><?php echo $rolesAction['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'roles_actions', 'action' => 'view', $rolesAction['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'roles_actions', 'action' => 'edit', $rolesAction['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'roles_actions', 'action' => 'delete', $rolesAction['id']), array('confirm' => __('Are you sure you want to delete # %s?', $rolesAction['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Roles Action'), array('controller' => 'roles_actions', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
