<div class="activitiesLogs view">
<h2><?php echo __('Activities Log'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($activitiesLog['ActivitiesLog']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($activitiesLog['ActivitiesLog']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($activitiesLog['User']['name'], array('controller' => 'users', 'action' => 'view', $activitiesLog['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Active Status'); ?></dt>
		<dd>
			<?php echo h($activitiesLog['ActivitiesLog']['active_status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Text'); ?></dt>
		<dd>
			<?php echo h($activitiesLog['ActivitiesLog']['text']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Annotation'); ?></dt>
		<dd>
			<?php echo h($activitiesLog['ActivitiesLog']['annotation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($activitiesLog['ActivitiesLog']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($activitiesLog['ActivitiesLog']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Activities Log'), array('action' => 'edit', $activitiesLog['ActivitiesLog']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Activities Log'), array('action' => 'delete', $activitiesLog['ActivitiesLog']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $activitiesLog['ActivitiesLog']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Activities Logs'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Activities Log'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
