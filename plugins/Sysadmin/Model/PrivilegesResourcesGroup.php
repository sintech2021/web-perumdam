<?php
App::uses('SysadminAppModel', 'Sysadmin.Model');
/**
 * ResourcesGroup Model
 *
 * @property Resource $Resource
 */
class PrivilegesResourcesGroup extends SysadminAppModel {

/**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'sys_';
	public $useTable = 'resources_groups';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
        
	public $hasMany = array(
		
		'PrivilegesResource' => array(
			'className' => 'Sysadmin.PrivilegesResource',
			'foreignKey' => 'resources_group_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

        public function getIdByName($names = array()){
        
        $this-> displayField = 'id';
        return $this->find('list', array('conditions'=> array('active_status'=> 1, 'name' => $names)));
    }
}
