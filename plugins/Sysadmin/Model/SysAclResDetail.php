<?php

App::uses('SysadminAppModel', 'Sysadmin.Model');

/**
 * Resource Model
 */
class SysAclResDetail extends SysadminAppModel
{

    /**
     * Use database config
     *
     * @var string
     */
    public $useDbConfig = 'sys_';
    public $useTable = 'resources_details';

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';
    

}
