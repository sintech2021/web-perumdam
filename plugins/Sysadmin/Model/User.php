<?php

App::uses('SysadminAppModel', 'Sysadmin.Model');

/**
 * User Model
 *
 * @property ActivitiesLog $ActivitiesLog
 * @property Role $Role
 */
class User extends SysadminAppModel
{

    /**
     * Use database config
     *
     * @var string
     */
    public $useDbConfig = 'sys_';

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';


    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'ActivitiesLog' => array(
            'className'    => 'Sysadmin.ActivitiesLog',
            'foreignKey'   => 'user_id',
            'dependent'    => false,
            'conditions'   => '',
            
            'fields'       => array(
                'ActivitiesLog.id',
                'ActivitiesLog.user_id',
                'ActivitiesLog.active_status',
                'ActivitiesLog.menu_code',
                'ActivitiesLog.text',
                'ActivitiesLog.menu_order',
                'ActivitiesLog.created',
                'ActivitiesLog.modified',
                ),
            'order'        => '',
            'limit'        => '20',
            'offset'       => '',
            'exclusive'    => '',
            'finderQuery'  => '',
            'counterQuery' => ''
        )
    );

    /**
     * hasAndBelongsToMany associations
     *
     * @var array
     */
    public $hasAndBelongsToMany = array(
        'Role' => array(
            'className'             => 'Sysadmin.Role',
            'joinTable'             => 'users_roles',
            'foreignKey'            => 'user_id',
            'associationForeignKey' => 'role_id',
            'unique'                => 'keepExisting',
            'conditions'            => '',
            'fields'                => '',
            'order'                 => '',
            'limit'                 => '',
            'offset'                => '',
            'finderQuery'           => '',
        )
    );

    /**
     * validation
     */
    public $validate            = array(
        'username' => array(
            'rule'    => 'usernameUnique',
            'message' => "Username already taken. please choose another."
        )
    );

    /**
     * password hasher
     *
     * @var array
     */
    public function beforeSave($options = array())
    {
        if (isset($this->data[$this->alias]['password'])) {
            if ($this->data[$this->alias]['password']) {
                $passwordHasher                       = new BlowfishPasswordHasher();
                $this->data[$this->alias]['password'] = $passwordHasher->hash(
                        $this->data[$this->alias]['password']
                );
            }
            else {
                unset($this->data[$this->alias]['password']);
            }
        }
        return true;
    }

    public function usernameUnique()
    {
        $conditions = array(
            'username'      => $this->data[$this->alias]['username'],
            'active_status' => 1,
        );
        if (isset($this->data[$this->alias]['id'])) {
            $conditions['id !='] = $this->data[$this->alias]['id'];
        }
        $existing = $this->find('first',
                array(
            'conditions' => $conditions
        ));
        return (count($existing) == 0);
    }
    
    /**
     * 
     * @param string $roleName
     */
    public function getIdLikeRoleName($roleName){
        $ids = array();
        $Role = ClassRegistry::init('Sysadmin.Role');
        $roles = $Role->find('all',array('conditions'=>array('name LIKE'=> $roleName)));
        if($roles){
            foreach($roles as $i){
                if(isset($i['User'])){
                    if($i['User']){
                        foreach($i['User'] as $usr){
                            $ids[] =$usr['id'];
                        }
                    }
                }
            }
        }
        return $ids;
    }

}
