<?php
App::uses('SysadminAppModel', 'Sysadmin.Model');
/**
 * RolesAction Model
 *
 * @property UsersRole $UsersRole
 * @property ResourcesAction $ResourcesAction
 */
class RolesAction extends SysadminAppModel {

/**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'sys_';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'UsersRole' => array(
			'className' => 'Sysadmin.UsersRole',
			'foreignKey' => 'users_role_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ResourcesAction' => array(
			'className' => 'Sysadmin.ResourcesAction',
			'foreignKey' => 'resources_action_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
