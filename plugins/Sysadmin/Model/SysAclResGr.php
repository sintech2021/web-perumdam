<?php
App::uses('SysadminAppModel', 'Sysadmin.Model');
/**
 * ResourcesGroup Model
 *
 * @property Resource $Resource
 */
class SysAclResGr extends SysadminAppModel {
    public $useDbConfig = 'sys_';
    public $useTable = 'resources_groups';
    
    public function getIdByName($names = array()){
        
        $this-> displayField = 'id';
        return $this->find('list', array('conditions'=> array('active_status'=> 1, 'name' => $names)));
    }
}
