<?php

App::uses('SysadminAppModel', 'Sysadmin.Model');

/**
 * Resource Model
 *
 * @property ResourcesGroup $ResourcesGroup
 * @property ResourcesType $ResourcesType
 * @property Resource $ParentResource
 * @property Resource $ChildResource
 * @property Proceedings $Proceedings
 */
class Resource extends SysadminAppModel
{

    /**
     * Use database config
     *
     * @var string
     */
    public $useDbConfig = 'sys_';

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';


    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'ResourcesGroup' => array(
            'className'  => 'Sysadmin.ResourcesGroup',
            'foreignKey' => 'resources_group_id',
            'conditions' => '',
            'fields'     => '',
            'order'      => ''
        ),
        'ResourcesType'  => array(
            'className'  => 'Sysadmin.ResourcesType',
            'foreignKey' => 'resources_type_id',
            'conditions' => '',
            'fields'     => '',
            'order'      => ''
        ),
        'ParentResource' => array(
            'className'  => 'Sysadmin.Resource',
            'foreignKey' => 'parent_id',
            'conditions' => '',
            'fields'     => '',
            'order'      => ''
        )
    );

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'ChildResource' => array(
            'className'    => 'Sysadmin.Resource',
            'foreignKey'   => 'parent_id',
            'dependent'    => false,
            'conditions'   => '',
            'fields'       => '',
            'order'        => '',
            'limit'        => '',
            'offset'       => '',
            'exclusive'    => '',
            'finderQuery'  => '',
            'counterQuery' => ''
        )
    );

    /**
     * hasAndBelongsToMany associations
     *
     * @var array
     */
    public $hasAndBelongsToMany = array(
        'Proceeding' => array(
            'className'             => 'Sysadmin.Proceeding',
            'joinTable'             => 'resources_proceedings',
            'foreignKey'            => 'resource_id',
            'associationForeignKey' => 'proceeding_id',
            'unique'                => 'keepExisting',
            'conditions'            => '',
            'fields'                => '',
            'order'                 => '',
            'limit'                 => '',
            'offset'                => '',
            'finderQuery'           => '',
        )
    );

    

}
