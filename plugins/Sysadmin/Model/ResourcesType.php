<?php
App::uses('SysadminAppModel', 'Sysadmin.Model');
/**
 * ResourcesType Model
 *
 * @property Resource $Resource
 */
class ResourcesType extends SysadminAppModel {

/**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'sys_';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Resource' => array(
			'className' => 'Sysadmin.Resource',
			'foreignKey' => 'resources_type_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
