<?php

App::uses('SysadminAppModel', 'Sysadmin.Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

/**
 * User Model
 *
 * @property ActivitiesLog $ActivitiesLog
 * @property Role $Role
 */
class SysAclUser extends SysadminAppModel
{

    /**
     * Use database config
     *
     * @var string
     */
    public $useDbConfig = 'sys_';
   // public $name = 'SysAclUser';
    //public $tablePrefix  = 'sys_';
    public $useTable   = 'users';

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';


    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * hasMany associations
     *
     * @var array
     */public $hasMany = array(
        'ActivitiesLog' => array(
            'className'    => 'Sysadmin.ActivitiesLog',
            'foreignKey'   => 'user_id',
            'dependent'    => false,
            'conditions'   => '',
            'fields'       => '',
            'order'        => array('created'=> 'desc'),
            'limit'        => '20',
            'offset'       => '',
            'exclusive'    => '',
            'finderQuery'  => '',
            'counterQuery' => ''
        )
    );
    /**
     * hasAndBelongsToMany associations
     *
     * @var array
     */
    public $hasAndBelongsToMany = array(
        'Role' => array(
            'className'             => 'Sysadmin.Role',
            'joinTable'             => 'users_roles',
            'foreignKey'            => 'user_id',
            'associationForeignKey' => 'role_id',
            'unique'                => 'keepExisting',
            'conditions'            => '',
            'fields'                => '',
            'order'                 => '',
            'limit'                 => '',
            'offset'                => '',
            'finderQuery'           => '',
        )
    );

    /**
     * password hasher
     *
     * @var array
     */
    public function beforeSave($options = array())
    {
        if (isset($this->data[$this->alias]['password'])) {
            if(isset($this->data[$this->alias]['new_password'])){
                $this->data[$this->alias]['password'] = $this->data[$this->alias]['new_password'];
                unset($this->data[$this->alias]['new_password']);
                unset($this->data[$this->alias]['confirm_password']);
            }
            if ($this->data[$this->alias]['password']) {
                $passwordHasher                       = new BlowfishPasswordHasher();
                $this->data[$this->alias]['password'] = $passwordHasher->hash(
                        $this->data[$this->alias]['password']
                );
            }
            else {
                unset($this->data[$this->alias]['password']);
            }
        }
        return true;
    }

    /**
     * 
     */
    public function checkPassword($id, $password){
        $this->id = $id;
        $pass = $this->field('password');
        $passwordHasher                       = new BlowfishPasswordHasher();
        return $passwordHasher->check($password, $pass);
    }
}
