<?php
App::uses('SysadminAppModel', 'Sysadmin.Model');
/**
 * ResourcesDetail Model
 *
 * @property Resource $Resource
 */
class ResourcesDetail extends SysadminAppModel {

/**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'sys_';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Resource' => array(
			'className' => 'Sysadmin.Resource',
			'foreignKey' => 'resource_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
