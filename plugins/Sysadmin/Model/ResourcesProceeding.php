<?php

App::uses('SysadminAppModel', 'Sysadmin.Model');

/**
 * ResourcesProceeding Model
 *
 * @property Resource $Resource
 * @property Proceeding $Proceeding
 * @property RolesProceeding $RolesProceeding
 */
class ResourcesProceeding extends SysadminAppModel
{

    /**
     * 
     */
    
    public $successMsg = "";
    
    /**
     * Use database config
     *
     * @var string
     */
    public $useDbConfig = 'sys_';

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'resource_id';


    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Resource'   => array(
            'className'  => 'Sysadmin.Resource',
            'foreignKey' => 'resource_id',
            'conditions' => '',
            'fields'     => '',
            'order'      => ''
        ),
        'Proceeding' => array(
            'className'  => 'Sysadmin.Proceeding',
            'foreignKey' => 'proceeding_id',
            'conditions' => '',
            'fields'     => '',
            'order'      => ''
        )
    );

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'RolesProceeding' => array(
            'className'    => 'RolesProceeding',
            'foreignKey'   => 'resources_proceeding_id',
            'dependent'    => false,
            'conditions'   => '',
            'fields'       => '',
            'order'        => '',
            'limit'        => '',
            'offset'       => '',
            'exclusive'    => '',
            'finderQuery'  => '',
            'counterQuery' => ''
        )
    );

    /**
     * 
     * @param type $request
     */
    public function privilegesSetPermission($request)
    {
        $res        = array('redir' => false);
        
        $named      = isset($request['named']) ? $request['named'] : array();
        $pageno     = 1;


        if ($named) {
            if (isset($named['page'])) {
                $pageno = (int) $named['page'];
            }
        }
        if ($pageno < 1) {
            $pageno = 1;
        }
        $res['redirect_url'] = array('page' => $pageno);
        if (!isset($named['priv_act'])) {
            #$this->error = __d('sysadmin','Missing priviliges action');
            return $res;
        }
        
        $setaction  = md5('set' . date('j'));
        $dropaction = md5('drop' . date('j'));
        if (!in_array($named['priv_act'], array($setaction, $dropaction))) {
            $this->error = __d('sysadmin','Missing action');
            return $res;
        }

        $res['redir'] = true;
        $act_status   = $named['priv_act'] == $setaction ? 1 : null;
        // do update
        //check if resproc exists
        if(!isset($named['res_id']) || !isset($named['proc_id'])){
            $this->error = __d('sysadmin','Missing ids');
            return $res;
           
        } 
        
       
        $options      = array('conditions' => array(
            'ResourcesProceeding.resource_id' => $named['res_id'],
            'ResourcesProceeding.proceeding_id' => $named['proc_id'],
        ) );
         
        $rs = $this->find('first', $options);
        $this->clear();
        if($rs){
           //update
            $this->id = $rs['ResourcesProceeding']['id'];
        }else{
            //create new
            $this->create();
        }
        $dt = array(
            'resource_id' => $named['res_id'],
            'proceeding_id' => $named['proc_id'],
            'active_status' => $act_status,
        );
        if($this->save($dt)){
            $this->successMsg = __d('sysadmin', 'Permissions has been updated.');
        }
        //$dt = array();
        //./do update



        return $res;
    }

}
