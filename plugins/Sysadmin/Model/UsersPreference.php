<?php
App::uses('SysadminAppModel', 'Sysadmin.Model');
/**
 * UsersPreference Model
 *
 * @property User $User
 */
class UsersPreference extends SysadminAppModel {

/**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'sys_';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'Sysadmin.User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
