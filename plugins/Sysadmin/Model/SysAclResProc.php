<?php

App::uses('SysadminAppModel', 'Sysadmin.Model');

/**
 * Resource Model
 */
class SysAclResProc extends SysadminAppModel
{

    /**
     * Use database config
     *
     * @var string
     */
    public $useDbConfig = 'sys_';
    public $useTable = 'resources_proceedings';

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'id';
    
    /*public $hasMany = array(
        ''
    );
    */

}