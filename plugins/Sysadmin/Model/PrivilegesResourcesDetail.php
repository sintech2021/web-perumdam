<?php
App::uses('SysadminAppModel', 'Sysadmin.Model');
/**
 * ResourcesDetail Model
 *
 * @property Resource $Resource
 */
class PrivilegesResourcesDetail extends SysadminAppModel {

/**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'sys_';
	public $useTable = 'resources_details';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'PrivilegesResource' => array(
			'className' => 'Sysadmin.PrivilegesResource',
			'foreignKey' => 'resource_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
