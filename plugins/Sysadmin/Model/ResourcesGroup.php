<?php
App::uses('SysadminAppModel', 'Sysadmin.Model');
/**
 * ResourcesGroup Model
 *
 * @property Resource $Resource
 */
class ResourcesGroup extends SysadminAppModel {

/**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'sys_';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
        
	public $hasMany = array(
		'Resource' => array(
			'className' => 'Sysadmin.Resource',
			'foreignKey' => 'resources_group_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'PrivilegesResource' => array(
			'className' => 'Sysadmin.PrivilegesResource',
			'foreignKey' => 'resources_group_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
