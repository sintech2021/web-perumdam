<?php

App::uses('SysadminAppModel', 'Sysadmin.Model');

/**
 * Resource Model
 */
class SysAclRes extends SysadminAppModel
{

    /**
     * Use database config
     *
     * @var string
     */
    public $useDbConfig = 'sys_';
    public $useTable = 'resources';
    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';
    
    /**
     * Relation
     */
    
    public $belongsTo = array(
                'ResourcesGroup' => array(
                    'className'  => 'Sysadmin.ResourcesGroup',
                    'foreignKey' => 'resources_group_id',
                ),
                'ResourcesType'  => array(
                    'className'  => 'Sysadmin.ResourcesType',
                    'foreignKey' => 'resources_type_id',
                ),
                'ParentResource' => array(
                    'className'  => 'Sysadmin.Resource',
                    'foreignKey' => 'parent_id',
                )
            );
    
     public $hasMany = array(
         'ChildResource'   => array(
                    'className'  => 'Sysadmin.Resource',
                    'foreignKey' => 'parent_id',
                    'dependent'  => false,
                ),
                'ResourcesDetail' => array(
                    'className'  => 'Sysadmin.ResourcesDetail',
                    'foreignKey' => 'resource_id',
                    'dependent'  => false,
                )
     );

    
    public function getTreeListTextAndName($ids){
        $this->recursive = 0;
        $this->virtualFields = array(
            'text_name' => "CONCAT(SysAclRes.text, ' (', SysAclRes.name,')')"
        );
        $this->displayField = 'text_name';
        $this->Behaviors->load('Tree');
        return $this->generateTreeList(array('id'=>$ids));
    }
   
}
