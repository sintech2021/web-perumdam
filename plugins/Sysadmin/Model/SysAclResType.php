<?php

App::uses('SysadminAppModel', 'Sysadmin.Model');

/**
 * ResourcesType Model
 *
 * @property Resource $Resource
 */
class SysAclResType extends SysadminAppModel
{

    /**
     * Use database config
     *
     * @var string
     */
    public $useDbConfig = 'sys_';
    public $useTable    = 'resources_types';

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * hasMany associations
     *
     * @var array
     */
    /* public $hasMany = array(
      'Resource' => array(
      'className' => 'Resource',
      'foreignKey' => 'resources_type_id',
      'dependent' => false,
      'conditions' => '',
      'fields' => '',
      'order' => '',
      'limit' => '',
      'offset' => '',
      'exclusive' => '',
      'finderQuery' => '',
      'counterQuery' => ''
      )
      );
     */
    public function getIdByName($names = array())
    {

        $this->displayField = 'id';
        return $this->find('list',
                        array('conditions' => array('active_status' => 1, 'name' => $names)));
    }
    
    public function getListByExcludedNames($exclude_names = array())
    {

        $this->virtualFields = array('name_text' => "CONCAT (`text`, ' (', `name`, ')')");
        $this->displayField = 'name_text';
        return $this->find('list',
                        array('conditions' => array('active_status' => 1, "`name` NOT IN ('".implode("','",$exclude_names)."')")));
    }

}
