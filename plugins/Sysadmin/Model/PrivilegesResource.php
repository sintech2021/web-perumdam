<?php

App::uses('SysadminAppModel', 'Sysadmin.Model');

/**
 * Resource Model
 *
 * @property ResourcesGroup $ResourcesGroup
 * @property ResourcesType $ResourcesType
 * @property Resource $ParentResource
 * @property Resource $ChildResource
 * @property Proceedings $Proceedings
 */
class PrivilegesResource extends SysadminAppModel
{

    /**
     * Use database config
     *
     * @var string
     */
    public $useDbConfig = 'sys_';
    public $useTable    = 'resources';

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';


    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'PrivilegesResourcesGroup' => array(
            'className'  => 'Sysadmin.PrivilegesResourcesGroup',
            'foreignKey' => 'resources_group_id',
            'conditions' => '',
            'fields'     => '',
            'order'      => ''
        ),
        'PrivilegesResourcesType'  => array(
            'className'  => 'Sysadmin.PrivilegesResourcesType',
            'foreignKey' => 'resources_type_id',
            'conditions' => '',
            'fields'     => '',
            'order'      => ''
        ),
        'ParentResource'           => array(
            'className'  => 'Sysadmin.PrivilegesResource',
            'foreignKey' => 'parent_id',
            'conditions' => '',
            'fields'     => '',
            'order'      => ''
        )
    );

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'ChildResource' => array(
            'className'    => 'Sysadmin.PrivilegesResource',
            'foreignKey'   => 'parent_id',
            'dependent'    => false,
            'conditions'   => '',
            'fields'       => '',
            'order'        => '',
            'limit'        => '',
            'offset'       => '',
            'exclusive'    => '',
            'finderQuery'  => '',
            'counterQuery' => ''
        )
    );

    /**
     * hasAndBelongsToMany associations
     *
     * @var array
     */
    public $hasAndBelongsToMany = array(
        'Proceeding' => array(
            'className'             => 'Sysadmin.Proceeding',
            'joinTable'             => 'resources_proceedings',
            'foreignKey'            => 'resource_id',
            'associationForeignKey' => 'proceeding_id',
            'unique'                => 'keepExisting',
            'conditions'            => array('ResourcesProceeding.active_status'=> 1),
            'fields'                => '',
            'order'                 => '',
            'limit'                 => '',
            'offset'                => '',
            'finderQuery'           => '',
        )
    );

    /**
     * Validation
     */
    public $validate = array(
        'name' => array(
            'rule' => 'nameUnique',
            'required' => true,
            'allowEmpty' => false,
            'last' => false,
        )
    );

    /**
     * 
     * @param type $ids
     * @return type
     */
    public function getTreeListTextAndName($ids)
    {
        $this->recursive     = 0;
        $this->virtualFields = array(
            'text_name' => "CONCAT(PrivilegesResource.text, ' (', PrivilegesResource.name,')')"
        );
        $this->displayField  = 'text_name';
        $this->Behaviors->load('Tree');
        return $this->generateTreeList(array('id' => $ids));
    }

    /**
     * 
     * @param type $RS
     * @return type
     */
    public function getTreeListTextAndNameByRS($RS)
    {
        $ids = array();
        if ($RS) {
            foreach ($RS as $i => $v) {
                if (isset($v['PrivilegesResource'])) {
                    if (isset($v['PrivilegesResource']['id'])) {
                        $ids [] = $v['PrivilegesResource']['id'];
                    }
                }
            }
        }
        return $this->getTreeListTextAndName($ids);
    }

    public function nameUnique()
    {
        $conditions = array(
            'PrivilegesResource.name'          => $this->data[$this->alias]['name'],
            'PrivilegesResource.active_status' => 1,
        );
        if (isset($this->data[$this->alias]['id'])) {
            $conditions['PrivilegesResource.id !='] = $this->data[$this->alias]['id'];
        }
        $existing = $this->find('first',
                array(
            'conditions' => $conditions
        ));
        #debug($this->data);die();
        return (count($existing) == 0);
    }
}
