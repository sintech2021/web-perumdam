<?php
App::uses('SysadminAppModel', 'Sysadmin.Model');
/**
 * RolesProceeding Model
 *
 * @property Role $Role
 * @property ResourcesProceeding $ResourcesProceeding
 */
class RolesProceeding extends SysadminAppModel {

    public $actsAs = array('Containable');
    
/**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'sys_';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'resources_proceeding_id';
        

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Role' => array(
			'className' => 'Sysadmin.Role',
			'foreignKey' => 'role_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ResourcesProceeding' => array(
			'className' => 'Sysadmin.ResourcesProceeding',
			'foreignKey' => 'resources_proceeding_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
