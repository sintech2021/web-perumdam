<?php

App::uses('SysadminAppModel', 'Sysadmin.Model');

/**
 * Role Model
 *
 * @property Proceedings $Proceedings
 * @property User $User
 */
class Role extends SysadminAppModel
{

    /**
     * Use database config
     *
     * @var string
     */
    public $useDbConfig = 'sys_';

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';


    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * hasAndBelongsToMany associations
     *
     * @var array
     */
    public $hasAndBelongsToMany = array(
        'ResourcesProceedings' => array(
            'className'             => 'Sysadmin.Proceedings',
            'joinTable'             => 'roles_proceedings',
            'foreignKey'            => 'role_id',
            'associationForeignKey' => 'resources_proceeding_id',
            'unique'                => 'keepExisting',
            'conditions'            => '',
            'fields'                => '',
            'order'                 => '',
            'limit'                 => '',
            'offset'                => '',
            'finderQuery'           => '',
        ),
        'User'        => array(
            'className'             => 'Sysadmin.User',
            'joinTable'             => 'users_roles',
            'foreignKey'            => 'role_id',
            'associationForeignKey' => 'user_id',
            'unique'                => 'keepExisting',
            'conditions'            => '',
            'fields'                => '',
            'order'                 => '',
            'limit'                 => '',
            'offset'                => '',
            'finderQuery'           => '',
        )
    );

    /**
     * validation
     */
    public $validate = array(
        'name' => array(
            'rule'       => 'nameUnique',
            'message'    => "Role name already taken. please choose another."
        ),
        'text' => array(
            'rule' => 'notBlank',
            'message' => 'Please fill in the full text of the role name.'
        ),
        'menu_code' => array(
            'rule' => 'notBlank',
            'message' => 'Please fill in the code of the role.'
        )
    );

    public function nameUnique()
    {
        $conditions = array(
            'name'          => $this->data[$this->alias]['name'],
            'active_status' => 1,
        );
        if (isset($this->data[$this->alias]['id'])) {
            $conditions['id !='] = $this->data[$this->alias]['id'];
        }
        $existing = $this->find('first',
                array(
            'conditions' => $conditions
        ));
        #debug($this->data);die();
        return (count($existing) == 0);
    }

}
