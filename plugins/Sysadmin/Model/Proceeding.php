<?php
App::uses('SysadminAppModel', 'Sysadmin.Model');
/**
 * Proceedings Model
 *
 * @property Resource $Resource
 * @property Role $Role
 */
class Proceeding extends SysadminAppModel {

/**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'sys_';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Resource' => array(
			'className' => 'Sysadmin.Resource',
			'joinTable' => 'resources_proceedings',
			'foreignKey' => 'proceeding_id',
			'associationForeignKey' => 'resource_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		),
		
		'Role' => array(
			'className' => 'Sysadmin.Role',
			'joinTable' => 'roles_proceedings',
			'foreignKey' => 'resources_proceeding_id',
			'associationForeignKey' => 'role_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);

}
