<?php
App::uses('SysadminAppModel', 'Sysadmin.Model');
/**
 * UsersRole Model
 *
 * @property User $User
 * @property Role $Role
 * @property RolesAction $RolesAction
 */
class UsersRole extends SysadminAppModel {

/**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'sys_';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'Sysadmin.User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Role' => array(
			'className' => 'Sysadmin.Role',
			'foreignKey' => 'role_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'RolesAction' => array(
			'className' => 'RolesAction',
			'foreignKey' => 'users_role_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
